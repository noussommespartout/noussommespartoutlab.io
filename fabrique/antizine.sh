#!/bin/bash

INPUT=$1
TEX=$2
LAST=`ls -tb ../content/francais/articles/* | head -1`

if [[ ${INPUT} = "compare" ]]; then
  ARTICLES=`ls -1q ../content/francais/articles/* | wc -l`
  PDFS=`ls -1q ../static/etc/*.pdf | wc -l`
  PDFS=$((PDFS - ARTICLES - ARTICLES))
  if [[ $ARTICLES != $PDFS ]]; then
    echo "---------------------------------"
    echo "---------------------------------"
    echo "                              "
    echo " Attention ! Il manque $((ARTICLES - PDFS)) PDF !"
    echo "                              "
    echo "---------------------------------"
    echo "---------------------------------"
  fi

elif [[ ${INPUT} = "clean" ]]; then
  cd antizines
  latexmk -c
  rm *.cb
  rm *.cb2

# elif [[ ${TEX} = "tex" ]]; then
elif [[ -n $INPUT ]]; then
  PDF=$(basename -- ${INPUT%.md}).pdf
  TEXFILE=$(basename -- ${INPUT%.md}).tex
  TEXA4FILE=$(basename -- ${INPUT%.md})-antilecture.tex
  BOOKLET=$(basename -- ${INPUT%.md})-antizine.tex
  TEXSOURCE=./antizines/$TEXFILE
  TEXA4SOURCE=./antizines/$TEXA4FILE
  pandoc -s --metadata lang:fr --lua-filter ./outils/replace.lua --lua-filter ./outils/pandoc-quotes.lua -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=outils/template.tex --toc --toc-depth=3 -o $TEXSOURCE $INPUT
  latexmk -lualatex -output-directory=./antizines $TEXSOURCE
  NBPAGES=$(pdfinfo ./antizines/$PDF | grep Pages | awk '{print $2}')
  pandoc -s --metadata lang:fr --metadata numberofpages:$NBPAGES --metadata pdfsource:./$PDF -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=outils/booklet.tex --toc --toc-depth=3 -o ./antizines/$BOOKLET $INPUT
  pandoc -s --metadata lang:fr -V a4paper --lua-filter ./outils/replace.lua --lua-filter ./outils/pandoc-quotes.lua -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=outils/template.tex --toc --toc-depth=3 -o $TEXA4SOURCE $INPUT
  latexmk -lualatex -output-directory=./antizines $TEXA4SOURCE
  cd antizines
  sed -i -- 's/{src/{\.\.\/src/g' $TEXFILE
  latexmk -c
  rm *.cb
  rm *.cb2
  vim $TEXFILE

elif [[ ${INPUT} = "last" ]]; then
  PDF=$(basename -- ${LAST%.md}).pdf
  BOOKLET=$(basename -- ${LAST%.md})-antizine.pdf
  PDFSOURCE=./antizines/$PDF
  pandoc -s --metadata lang:fr --lua-filter ./outils/replace.lua --lua-filter ./outils/pandoc-quotes.lua -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=outils/template.tex --toc --toc-depth=3 -o $PDFSOURCE $LAST
  NBPAGES=$(pdfinfo $PDFSOURCE | grep Pages | awk '{print $2}')
  pandoc -s --metadata lang:fr --metadata numberofpages:$NBPAGES --metadata pdfsource:$PDFSOURCE -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=outils/booklet.tex --toc --toc-depth=3 -o ./antizines/$BOOKLET $LAST

else
  for i in ../content/francais/articles/* ; do
    test "$i" = ../content/francais/articles/_index.md && continue
    PDF=$(basename -- ${i%.md}).pdf
    TEXFILE=$(basename -- ${i%.md}).tex
    TEXA4FILE=$(basename -- ${i%.md})-antilecture.tex
    BOOKLET=$(basename -- ${i%.md})-antizine.tex
    TEXSOURCE=./antizines/$TEXFILE
    TEXA4SOURCE=./antizines/$TEXA4FILE
    pandoc -s --metadata lang:fr --lua-filter ./outils/replace.lua --lua-filter ./outils/pandoc-quotes.lua -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=outils/template.tex --toc --toc-depth=3 -o $TEXSOURCE $i
    latexmk -lualatex -output-directory=./antizines $TEXSOURCE
    NBPAGES=$(pdfinfo ./antizines/$PDF | grep Pages | awk '{print $2}')
    pandoc -s --metadata lang:fr --metadata numberofpages:$NBPAGES --metadata pdfsource:./$PDF -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=outils/booklet.tex --toc --toc-depth=3 -o ./antizines/$BOOKLET $i
    pandoc -s --metadata lang:fr -V a4paper --lua-filter ./outils/replace.lua --lua-filter ./outils/pandoc-quotes.lua -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=outils/template.tex --toc --toc-depth=3 -o $TEXA4SOURCE $i
    latexmk -lualatex -output-directory=./antizines $TEXA4SOURCE
    sed -i -- 's/{src/{\.\.\/src/g' $TEXFILE
  done
fi

exit 0
