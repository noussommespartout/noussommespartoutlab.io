#!/bin/bash

pandoc -s --lua-filter gabarit/pandoc-quotes.lua -f markdown -t latex --file-scope --pdf-engine=lualatex --top-level-division=chapter --template=gabarit/livre-introduction.tex -o antilivres/livre-introduction.tex texte/1-001-meta.md
pandoc -s --lua-filter gabarit/pandoc-quotes.lua -f markdown -t latex --file-scope --pdf-engine=lualatex --top-level-division=chapter --template=gabarit/livre-conclusion.tex -o antilivres/livre-conclusion.tex texte/1-001-meta.md

touch antilivres/nous-sommes-partout.tex
cat antilivres/livre-introduction.tex > antilivres/nous-sommes-partout.tex

for file in texte/*.md; do
  TITLE="$(basename ${file} .md)"
pandoc --lua-filter gabarit/pandoc-quotes.lua -s -f markdown-smart -t latex --file-scope --pdf-engine=lualatex --top-level-division=chapter --template=gabarit/chapitre.tex -o antilivres/tmp/$TITLE.tex $file
  echo "" >> antilivres/tmp/$TITLE.tex
  sed -i 's/-\\\/-\\\/-/---/g' antilivres/tmp/$TITLE.tex
done
rm antilivres/tmp/1-001-meta.tex
cat antilivres/tmp/* >> antilivres/nous-sommes-partout.tex

cat antilivres/livre-conclusion.tex >> antilivres/nous-sommes-partout.tex

rm antilivres/tmp/*
rm antilivres/livre-*

cd antilivres

vim nous-sommes-partout.tex
