text = require 'text'
return {
  {
    Str = function (elem)
      --- Épigraphe
      if string.find(elem.text, "{{%%epigraphe%%}}") then
        return pandoc.RawInline('latex', '\\begin{epigraphe}')
      elseif string.find(elem.text, "{{%%/epigraphe%%}}") then
        return pandoc.RawInline('latex', '\\end{epigraphe}')

      --- gauche
      elseif string.find(elem.text, "{{%%gauche%%}}") then
        return pandoc.RawInline('latex', '\\begin{gauche}')
      elseif string.find(elem.text, "{{%%/gauche%%}}") then
        return pandoc.RawInline('latex', '\\end{gauche}')

      --- droite
      elseif string.find(elem.text, "{{%%droite%%}}") then
        return pandoc.RawInline('latex', '\\begin{droite}')
      elseif string.find(elem.text, "{{%%/droite%%}}") then
        return pandoc.RawInline('latex', '\\end{droite}')

      --- centre
      elseif string.find(elem.text, "{{%%centre%%}}") then
        return pandoc.RawInline('latex', '\\begin{nscenter}')
      elseif string.find(elem.text, "{{%%/centre%%}}") then
        return pandoc.RawInline('latex', '\\end{nscenter}')

      --- alignement droite
      elseif string.find(elem.text, "{{%%alignement-d%%}}") then
        return pandoc.RawInline('latex', '\\begin{nsright}')
      elseif string.find(elem.text, "{{%%/alignement-d%%}}") then
        return pandoc.RawInline('latex', '\\end{nsright}')

      --- cadre
      elseif string.find(elem.text, "{{%%cadre%%}}") then
        return pandoc.RawInline('latex', '\\begin{mdframed}')
      elseif string.find(elem.text, "{{%%/cadre%%}}") then
        return pandoc.RawInline('latex', '\\end{mdframed}')

      --- droitecit
      elseif string.find(elem.text, "{{%%droitecit%%}}") then
        return pandoc.RawInline('latex', '\\begin{droitecit}')
      elseif string.find(elem.text, "{{%%/droitecit%%}}") then
        return pandoc.RawInline('latex', '\\end{droitecit}')

      --- Astérisme
      elseif string.find(elem.text, "{{%%asterisme%%}}") then
        return pandoc.RawInline('latex', '\\asterisme')

      --- Ligne blanche
      elseif string.find(elem.text, "{{%%ligneblanche%%}}") then
        return pandoc.RawInline('latex', '\\vspace{1\\baselineskip}')

      else
        return elem
      end
    end,
  }
}
