---
author: "Anonyme"
title: "Cette colère immense, collective, transgénérationnelle, internationale"
subtitle: "Colères écoféministes explosantes"
info: "Texte rédigé pour le recueil"
date: "novembre 2020"
categories: ["écoféminismes", "féminismes, questions de genre"]
tags: ["index"]
lang: ["fr"]
num: "30"
quote: "devenir une tempête plus forte que le cyclone néolibéral"
---

l'une :

--- je suis en colère

l'autre :

--- moi aussi

nous sommes deux amies qui nous sommes rencontrées alors que nous nous
découvrions selon certaines évidences *écoféministes°*

*éco* comme le vivant, la vie --- *féministes* comme le vivant, la vie.

Étant donné que le terme écoféminisme pourrait nommer une forme
d'appartenance, d'inscription dans tout le reste, une notion de ne pas
se sentir toute seule, nous sommes deux pour le découdre-recoudre à
notre guise, en parler, pour le préciser jusqu'à ce qu'il nous
soutienne et que l'on puisse le soutenir aussi.

Nous sommes des filles blanches hétéras dans notre trentaine, et nous
considérons que c'est un minimum incompressible de dire ça pour situer
d'où nous parlons. Nous ne croyons pas à l'universel.

chœur/touxtes :

Ça vient du bas

De femmes\*° qui en avaient marre qu'on défonce tout autour d'elles,
qu'on défonce les femmes\* les arbres les terres les rivières les
vaches[^76].

Et qui voyaient bien que tout cela se tenait ensemble, tout ce qui se
faisait défoncer d'une part, et (ce) qui défonçait de l'autre.

Ça vient de là.

l'une dit :

Moi on m'a dit écoféministe bien avant que moi je le dise. C'est un peu
ça aussi, c'est un truc un peu *logique\index[tags]{logique}*, pas un véritable choix
politique conscient.

Je faisais un peu attention autour de moi.

chœur/les deux :

On faisait un peu attention à autour de nous.

On a eu peur\index[tags]{peur} de la "femme sauvage" cachée derrière le terme.

De l'idée d'être naturellement plus naturelle, donc plus écologiste.
D'être les filles de la terre mère\index[tags]{mère} et c'est tout. Alors on a refusé.

Ensuite on a bien senti qu'on pouvait aussi fleurir là-dedans un peu, y
germer, y composter\index[tags]{compost/composter}[^77]. Dans ce terme. Qu'il peut être fort et
porteur, si on le laisse pas juste dire femme-louve qui court sous la
lune par pulsion.

Ce ne sont pas nos pulsions qui nous font courir, ce sont tour à tour
les assauts à lancer contre ce qui nous défonce ou la joie qui nous
anime, souvent c'est mélangé d'ailleurs.

Notre féminisme est antipatriarcal donc anticapitaliste, donc
anticolonial, donc anticlassiste, antivalidiste et globalement antitout
ce qui nous sépare tant. On le veut le plus inclusif possible, le plus
radical aussi, au sens de déterminé. Fini de rire\index[tags]{rire/se marrer/rigoler}. Sauf qu'on rit tout
le temps ensemble. La chose la plus radicale qu'on fait ensemble c'est
se fendre la gueule\index[tags]{gueule} littéralement. On est pliées de rire\index[tags]{rire/se marrer/rigoler}. On dit des
bêtises absurdes et des jeux de mots magiques, on imite nos proches et
nos douleurs et on rigole à pisser dans nos culottes.

Notre féminisme est en friche, il est compliqué à surveiller comme le
lait qui bout car parfois il blesse celleux que nous aimerions embrasser
avec tendresse. Il se prête bien à la métaphore. Aussi on en a
maaaaaarrrre. Il est nourri de ça.

On fait feu\index[tags]{feu} de tout bois, on boit des tisanes parce qu'on sait les
préparer et parce que soit ça soigne, soit ça tue, et on a envie
d'avoir un spectre aussi large que cela. On fait pousser de la moutarde
parce que ça pousse partout. C'est faire avec ce qu'il y a, en
conscience et en vigilance. Ça donne l'impression d'appartenir à un
monde plus vaste quand même, faut pas oublier de le dire, on se
sent quand même vraiment en faire partie, tout en se sentant quand même
souvent moins faire partie d'une certaine version du monde plus
restreinte, le monde patriarcal qui est tout tout tout petit si on
compte en atomes de vie et qui pourtant fait si mal, comme un bris de
verre empoisonné dans notre talon qui nous TUE alors qu'il est SI
PETIT, ça devrait pas, si tout était juste logique\index[tags]{logique}.

l'une dit :

Littéralement toute la monde est oppressée par le patriarcat.

l'autre dit :

Notre féminisme en friche est un écoféminisme, parce qu'il dit que
l'oppression, la colonisation et l'exploitation de la société
occidentale et patriarcale ont créé des dommages environnementaux
irréversibles. Il visibilise l'articulation de la domination de la
nature --- au sens de tout ce qui n'a pas été créé par les êtres
humains --- et la domination de tous les groupes minorisés --- femmes\*,
LGBTQIA+, personnes racisées°, malades, enfants, pauvres, animaux. Il
n'y a pas l'un ou l'autre à combattre, c'est le tout qui défonce
tout ce que l'on combat. C'est par une révolution féministe-écologiste
impliquante décoloniale et anticapitaliste que nous bouleverserons les
violences de domination.

l'une dit :

et puis j'ai rencontré dans les témoignages et les centaines de
pratiques une radicalité encourageante.

elles rigolent

notre amie dit : Il n'y a pas de rupture\index[tags]{rupture}, il n'y aura pas de fin du
monde du jour au lendemain. Elle prendra son temps et ce n'est qu'en
créant une **solidarité** humaine et interespèce que nous la vivrons en
respectant les vulnérabilités, en cohabitant dans les ruines et en se
débarrassant de l'individualisme.

Nous devons revendiquer des changements structurels contre les
dominations, afin de prendre en compte que le niveau de violence\index[tags]{violence} variera
beaucoup avec ou sans ces changements.

l'autre dit : on a moins peur\index[tags]{peur} de la violence\index[tags]{violence} du futur que de la
violence actuelle.

l'une dit :

Les écoféministes décoloniales, trans-inclusives et anticapitalistes
n'acceptent pas le deuil de la fin du monde. Elles désespèrent peut-être
individuellement mais se mobilisent collectivement avec rage\index[tags]{rage}. Les voies
de l'écoféminisme sont tout aussi possibles que d'autres scénarii. Ces
luttes s'inscrivent dans une longue temporalité dense et multiple sans
rupture d'un effondrement\index[tags]{effondrement} préprogrammé.

l'une dit :

Ça nous porte aussi cette longue temporalité. Ce n'est pas un point
cardinal pour moi l'écoféminisme, pas un recueil de règles qui me
guident dans la vie, c'est un terme plus ou moins concis et pratique
pour mentionner de manière rapide la somme de mes émotions et mes
pratiques quotidiennes, mes réflexions critiques comme mes intuitions.
C'est pour ça qu'on passe tant de temps à y ajouter des autres termes,
à le tricoter ensemble pour pas qu'il soit comme un terme qui flotte
au-dessus de nous et nous mette mal à l'aise. La joie dans ce terme,
c'est justement qu'il était un plus, un fluidifiant, un résumé de nos
élans.

Mais dernièrement l'autre a dit : *stop ! on arrête ce terme, ça va
plus, c'est devenu trop ambigu, ça veut trop dire de choses excluantes
maintenant.*

Ça m'a questionnée, moi je préfère ne pas abandonner trop de termes,
mais plutôt les définir quand je les utilise. Il n'empêche qu'on
continuera à lutter, à taguer, à cultiver, à s'enrager et à articuler
ensemble les destructions parallèles.

l'autre dit :

Surtout que de manière générale, j'ai la sensation que certains
mouvements écologistes actuels font table rase du passé en omettant de
s'inscrire dans une continuité historique. Alors que les luttes autour
de l'écologie sont une longue tradition vivace et multiple. Ainsi, en
créant des formes de générations spontanées de mouvements, le risque, et
c'est ce qui se passe, c'est d'invisibiliser des luttes précédentes.

et notre autre amie nous rappelle que :

c'est dingue à quel point l'occident a besoin de nommer, de prendre et
de s'approprier...

l'une dit : je crois que oui, ce terme est surtout important pour nous
occidentales, pour rassembler, décrire, faciliter la construction de
ponts entre des centaines de pratiques partout sur terre et de la
théorie qui nous parle de nous. Il me permet de m'inscrire avec
humilité et joie du côté de toutes ces personnes qui défendent leurs
vies et celles de tout ce qui les entoure de manière créative, à la fois
désespérée mais pleine d'espoir et surtout de manière efficace. Comme
on n'a pas le même système de valeurs que les dominants, on n'a pas le
même point de vue sur les victoires.

l'autre dit :

Il s'agit de devenir une tempête plus forte que le cyclone néolibéral,
patriarcal, injuste, raciste. Je suis en colère\index[tags]{colère} et j'ai envie de casser
des choses. Mon genre a été socialisé° sans cette colère\index[tags]{colère} immense,
collective, transgénérationnelle, internationale[^78]. Je suis en
colère et j'ai envie de casser des choses avec une rage\index[tags]{rage} émancipatrice.

l'autre dit :

La durabilité n'est pas un luxe\index[tags]{luxe}, pub récente d'une grosse entreprise
de meubles à bas coûts : un monde meilleur commence chez soi.

Depuis, cette phrase me trotte dans la tête. J'exècre cette phrase.
C'est tout ce contre quoi j'essaie de me battre. C'est tout ce qui me
débecte et elle dit tout. Un monde meilleur\ →\ chez soi. C'est
la quête de l'absence de trouble. L'apathie avec passion. La
décoration avant tout. Tout ça, pour nous refourguer une cheap lampe en
bambou. Tout y est, la néocolonisation et l'impérialisme occidental qui
délocalisent ses troubles pour atteindre l'éden intérieur, le paradis
sur terre.

l'une dit :

le paradis\index[tags]{paradis} sur terre sous forme d'enclos privé. Un jour j'ai lu "ici
c'est la terre, ça ne sera jamais le paradis\index[tags]{paradis}" ça m'a tellement
soulagée.

l'autre dit :

Comment peut-on si éhontément écarter la production et les conditions de
travail dans lesquelles ces lampes ont été fabriquées? Je suis en colère
et j'ai envie de casser des choses.

l'autre dit :

Et après, c'est la réponse collective que je souhaite défendre,
l'organisation et l'autogestion qui consiste à prendre soin des
individus humains, non humains dans tous les temps, pandémiques ou
non. Et le renvoi de la responsabilité à l'état et au capitalisme. Il
s'agit de devenir une tempête plus forte que le cyclone néolibéral,
patriarcal, injuste, raciste. Je suis en colère\index[tags]{colère} et j'ai envie de casser
des choses. Mon genre a été socialisé sans cette colère\index[tags]{colère} immense,
collective, transgénérationnelle, internationale. Je suis en colère\index[tags]{colère} et
j'ai envie de casser des choses avec une rage\index[tags]{rage} émancipatrice.

chœur/touxtes :

Ça vient du bas

De femmes\* qui en avaient marre qu'on défonce tout autour d'elles qu'on
défonce les femmes\* et les autres, les arbres les terres les rivières
les vaches et les autres.

Se dire écoféministes ça a juste donné un nouveau nom à ce que l'on
faisait déjà, et ça a permis de se sentir plus nombreuxses, ce qui
nourrit nos vies.

Elles soupirent, s'étirent, se sourient et s'y remettent.

[^76]: *Là-haut sur la Colline*\ \[n^o^\ 51\] est le récit d'une expérience de ZAD° contre la destruction programmée d'une colline.

[^77]: Pour envisager une révolution hautement biodégradable, lire *Le compost généralisé*\ \[n^o^\ 41\].

[^78]: Pour entendre une colère non binaire, queer, trans, pédéex, grossex, hyperactivex et HP, lire *Un terreau pour les fleurs de la révolte*\ \[n^o^\ 52\].

