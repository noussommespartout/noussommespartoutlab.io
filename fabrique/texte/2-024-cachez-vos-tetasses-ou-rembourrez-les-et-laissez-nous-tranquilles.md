---
author: "S. & P."
title: "Cachez vos tétasses ou rembourrez-les et laissez-nous tranquilles"
subtitle: "Réflexions sur le concept de poitrine"
info: "Texte rédigé pour le recueil"
date: "août 2020"
categories: ["féminismes, questions de genre", "sexualités"]
tags: ["index"]
lang: ["fr"]
num: "24"
quote: "nos seins sont politiquement différents"
---

Les boobs, c'est une partie du corps\index[tags]{corps} qui se situe à la hauteur du
thorax, entre le cou et le bide. C'est la poitrine quoi. Mais
contrairement à ce qu'on peut entendre ici et là, notamment dans les
dictionnaires, tout le monde a des nichons, des miches, des michetons,
des mamelles, des mamelons, des lolos, des loches, des pecs', des
pectoraux, des tétons, des tétines, des tétés, des tchoutchs, des tits,
des boobs, des poitrines, des poitrails, des balcons, des flotteurs, des
nibards, des gougouttes, des coffres, des gorges, des eins, des nénés et
autres éléments physiques qui se situent sous les deux clavicules.

Que tu sois une femme cis, une femme trans, un mec trans, une personne
non binaire ou.............. un mec cis bien relou, ben t'as des boobs.
Mais nos seins sont politiquement différents.

On ne les regarde pas pareil.

On ne les éduque pas pareil.

On n'en parle pas pareil.

Chers mecs cis, vos tétasses on les a déjà bien assez vues. Un peu de
pudeur de bleu ! Dévoilez-les avec précaution, jouez subtilement à
cache-cache avec nous, balancez les décolletés, "tu me vois, tu me vois
pas", c'est comme ça qu'on fait askip, rendez-les censurables !
Rembourrez-vous le torse, pas au fitness, devant vos miroirs et
contemplez vos nouvelles courbes féminines avant de vous lancer dans un
concours de t-shirts mouillés. Si, si. On se lèche déjà les babines.
Soyez plus fab' quoi.

Nous, on rêve de grandes processions de nibards dans les rues, tous nus
comme des vers et cachés pour celleux qui en n'ont pas envie. Une méga
meute impubliable sur les réseaux sociaux. D'ailleurs, balançons les
restes des "ancêtres corsets", balançons nos crop-tops et nos
sweatshirts et promenons-nous dans les bois et dans les villes, allons
taffer à poil\index[tags]{poil}, buvons des verres à poil\index[tags]{poil} et monsieur Zuckerberg pliera ou
fera faillite. Organisons des blocs, pétons les caméras des journalistes
relous et scandons touxtes ensemble nos hymnes, nos kifs et nos slogans.

Ce sera une grande meute de nichons coulants comme des larmes de joie,
flous comme la brume, poilus comme des arbres en été, couverts comme en
hiver, gonflés à bloc comme en manif, fraîchement cicatrisés comme
certains de nos cœurs, petits comme des raisins\index[tags]{raisins}, accidentés comme les
forêts, rouges comme des langues chaudes, énormes comme notre
solidarité, fripés comme du papier froissé, plats comme les steppes du
nord, boursouflés comme des oranges juteuses, acnéiques comme les
astres, faits, refaits, cousus et décousus comme nos pensées, durs comme
la roche, vieux comme les sagesses de nos sœurs, laiteux comme la voie
lactée, vergeturés comme l'océan, asymétriques comme la vie, mous comme
l'écume de mer, en sueur\index[tags]{sueur} comme la rosée du matin, secs comme des
pruneaux californiens, noirs comme des pupilles, voilés comme une ville
en automne, pointus comme les aiguilles des conifères, explosifs comme
les volcans, carrés comme des pecs, débordants comme nos colères.

