---
author: "Anonyme"
title: "Spectacle nulle part. *Care* partout."
subtitle: "Discussion avec un collectif de copwatch"
info: "Discussion retranscrite"
date: "6 mai 2020"
categories: ["prison, justice, répression", "police", "violence, non-violence", "autodéfense"]
tags: ["index"]
lang: ["fr"]
num: "23"
quote: "l'institution te traverse, te fait exister"
---

*NB : Cet entretien a eu lieu près d'un mois avant la diffusion des
images du meurtre de George Floyd et de leurs répercussions politiques
mondiales et près de sept mois avant le projet de loi "Sécurité
Globale" en France\index[tags]{france} qui cherche à rendre illégale la diffusion d'images
d'un policier identifiable.*

## Est-ce que "tous les flics sont des connards" ?

\indent --- Ouais. Qui a juste envie de dire oui ?

--- Oui.

--- Oui.

--- Oui.

--- Oui.

--- Oui. (*Rires.*)

--- La réponse typique des gens au slogan ACAB°, c'est : "Moi je connais
un flic qui... blabla". La question soulève la différence entre
critique individuelle et critique institutionnelle ou systémique°, une
différence qui n'est pas toujours évidente, je trouve.

--- Perso, hors milieu militant, je réponds souvent à cette question en
mettant l'accent sur le fait qu'il s'agit d'une critique
institutionnelle et systémique, parce que c'est le cas déjà et que
c'est plus simple de convaincre comme ça, mais allons au bout de la
question. Parce que oui, l'institution organise la violence\index[tags]{violence} avec son
corps policier, mais en même temps, on a les individus flics ultra
racistes, d'extrême droite, qui vont s'encourager les uns les autres à
être des connards et à se comporter de manière violente. On est
d'accord, c'est quelque chose qu'ils ont hérité de l'institution et
du système dans lequel ils évoluent, mais c'est aussi quelque chose qui
dépend de leur "libre arbitre". Toutes ces violences, ils les font
quand même, dans une certaine mesure, en toute conscience. C'est juste
que la conscience aussi, elle est construite par l'institution, parce
que l'institution te traverse, te fait exister.

--- Tu vois, chaque année il y a deux ou trois keufs qui quittent la
police suisse en prenant parfois la parole\index[tags]{parole} sur ce qu'ils ont vécu. Le
dernier que j'ai vu, c'était un mec qui a quitté la police parce
qu'il en pouvait plus et un truc qu'il racontait, c'est un jeu entre
collègues : faire des tours en voiture\index[tags]{voiture/bagnole} et balancer des insultes racistes
par la fenêtre aux personnes racisées[^64]. C'était à Genève. Il y a
un piège à ne vouloir critiquer que l'institution policière. Quand tu
formules une critique institutionnelle, t'as un discours qui empêche
parfois de faire comprendre aux gens qu'il s'agit de vécus humains et
d'individus, autant en ce qui concerne les policiers que les personnes
qui subissent les violences policières. Quand tu racontes des trucs bien
précis comme ce "jeu", le vécu quotidien du profilage racial, les
fouilles à nu humiliantes après des manifestations pacifiques, là tu
fais comprendre aux gens le sens de "tous les flics sont des
connards". Parce que dans la pratique, c'est pas le système dans sa
globalité que tu regardes dans les yeux quand tu te fais contrôler dans
la rue\index[tags]{rue}, c'est un flic. À quel point existe-t-on en dehors des cadres
sociaux qui nous font exister ? Être dans la police, se comporter en
policier parce que l'institution est faite comme ça, c'est se
comporter en connard, donc tous les flics sont des connards en tant
qu'ils sont des flics et qu'ils ont décidé de performer ce rôle
social.

--- D'un autre côté, on peut dire ACAB si on considère le rôle de flic,
mais maintenant, je pense qu'il y a aussi la personne et...

--- Attention, la séparation de l'homme et de l'artiste\index[tags]{art/artiste} ! (*Rires.*)

--- Non mais je pense qu'il y a aussi des composantes sociologiques. Une
fois qu'on est dans un cercle social, c'est difficile d'en sortir. C'est
chaud de décrire une personne par son métier de policier, en fait c'est
chaud de le faire avec tous les métiers...

--- Je vois, mais on n'en a rien à foutre au fond, on peut pas commencer
à penser au bien-être de personnes qui perpétuent un système raciste et
qui passent leur vie à défendre les intérêts d'un état profondément
inégalitaire. Ce discours a des limites, c'est comme si on avait ce
discours avec des patronnexs d'entreprise qui licencient en masse et qui
détruisent la planète, on s'en fout, on n'a pas le temps de s'occuper de
ces gens, d'avoir de l'empathie\index[tags]{empathie} pour elleux, on a des priorités
politiques. Ces personnes se battent pour sauvegarder leurs privilèges,
elles sont violentes. On vit des situations de petites guerres avec la
police, des manifestations ultra violentes, on voit en face de nous une
ligne de front[^65]. On comprend qu'on est contrainxtes d'utiliser la
violence pour se faire entendre et pour changer quelque chose, et c'est
pas cool du tout, c'est triste. La violence\index[tags]{violence}, c'est une nécessité, c'est
pas une source de joie et on se rend bien compte que s'il n'y avait pas
des personnes détères à lancer quelques pavés, à repousser les CRS ou à
tenir des barricades, on ne pourrait rien faire. C'est un aveu d'échec
constant de l'humanité. Et c'est pareil avec la violence\index[tags]{violence} verbale, avec
la haine. Quand elle ne te bouffe\index[tags]{bouffe} pas de l'intérieur, elle aide à garder
la force\index[tags]{force} et à construire un réel contrepouvoir. Parce que face à une
ligne de CRS armés de la tête aux pieds[^66], il faut avoir le cran et
il faut montrer notre puissance, sinon c'est perdu d'avance, tu te
fais embarquer en deux secondes. La violence\index[tags]{violence} verbale du ACAB, c'est
pareil, ça fait partie du même "contrepouvoir" qu'on tente de créer.

--- Il y a souvent cet argument selon lequel les flics font face à des
situations difficiles.

--- Ouais, c'est l'interdiscours public, mais je pense que ça va plus
loin. La plupart du temps, puisque t'es un mâle, que t'es fort et que
t'as conscience de "ton devoir" et qu'on sait que l'être humain est
"une sale merde\index[tags]{merde}", ben on accepte et on héroïse les personnes qui sont
capables de mettre de côté leurs émotions pour faire leur devoir et
emprisonner les "sales merdes". C'est justement ça, j'ai
l'impression, qui est considéré comme héroïque, c'est le fait qu'ils
ont des émotions et qu'ils tuent et punissent quand même les
"méchanxtes". C'est ce qui est considéré comme "difficile".

--- Une petite histoire pour parler des "émotions" de la police. Une
fois je me baladais ici, à Genève, et vers le Grütli il y avait une
baston, deux gars qui se foutaient sur la gueule\index[tags]{gueule} grave, grave, grave. Du
sang, ils étaient physiquement épuisés. C'était deux personnes
racisées, toujours important de préciser ce genre de choses pour les
histoires qui concernent les flics... tous les gens au Grütli étaient
passifvexs bien sûr, mais le gérant d'un bar avait appelé la police.
Avec un ami on essayait de calmer le jeu puis la police est arrivée. Un
des deux mecs était parti. Et les flics étaient en mode ultra saoulés,
"encore une baston entre un Noir et un Arabe quoi", ils ont ignoré le
gars qui était encore là et qui était dans une situation ultra précaire :
mal physiquement, sans endroit où dormir, on lui avait volé son
téléphone, etc. Et il y avait un manque hallucinant d'émotions envers
cette personne. Il y a un manque d'empathie\index[tags]{empathie} incroyable envers des
personnes qui vivent d'autres réalités.

--- Oui, c'est une question d'empathie\index[tags]{empathie}. L'empathie\index[tags]{empathie} n'est pas nourrie
dans une institution policière ou alors elle est extrêmement canalisée
et "triée". L'empathie\index[tags]{empathie} est une construction sociale, elle se
soustrait pas aux problèmes liés au racisme, à la transphobie, à toutes
les oppressions sociétales, c'est ce qui fait que certaines personnes
sont complètement déshumanisées.

--- On va pas considérer une personne noire comme une personne blanche.
En Suisse, quand on amène des sans-papièrexs dans les avions pour les
renvois sous contrainte, il y a un ligotage intégral, avec menottes,
casque sur la tête, pieds attachés, des flics partout. Hamid Bakiri et
Samson Chukwu sont morts dans le cadre de ces renvois. Il ne faut pas
oublier leurs noms.

--- Si on voyait une personne suisse dans un pays étranger entrer dans un
avion dans ces conditions, ce serait le scandale de la dictature. On
imagine touxtes ce que ça donnerait dans la presse. L'empathie\index[tags]{empathie} et les
émotions sont configurées par un système policier de contrôle qui a pour
but de maintenir le pouvoir\index[tags]{pouvoir} en place et ses inégalités. C'est pas pour
rien que dans la police on vote\index[tags]{vote} beaucoup plus à l'extrême droite. Alors
savoir si les flics sont racistes et violents à la base ou s'ils le
deviennent, c'est une question intéressante à laquelle il y a sans
doute des sociologues qui consacrent du temps, mais ça ne change pas le
fait qu'ils sont racistes. Donc dire des trucs genre "j'ai un ami qui
est flic et qui a un ami noir" et je ne sais quel autre argument
pourri, c'est pas possible. (*Rires.*)

--- C'est marrant les discussions qu'on se tape avec les flics dans un lieu occupé quelque part en Suisse. Ils se sont accaparés le rôle de sauveurs,
de personnes bien qui doivent se tenir à une charte faite de règles,
donc à des lois qui évitent le chaos. Pour eux, c'est hyper construit.
Dans leurs têtes, ils ne se posent pas de questions. Ils ne viennent
jamais dans cette occupation en se demandant si ce qu'on fait c'est
bien. Non, ils viennent en nous disant des trucs genre "oui quand on va
sur des scènes de violences conjugales, les gens sont bien contents de
nous voir".

--- Sans entrer dans la discussion de la prise en charge catastrophique
des violences conjugales par la police, toute cette héroïsation nourrit
leur envie de continuer.

--- déconstruire\index[tags]{déconstruire/déconstruction} la nécessité d'une police, nécessite aussi de remettre
en question toutes les institutions qui l'entourent. Le système
carcéral, par exemple. La société s'en fout du sort des personnes qui
vont en prison, on construit la figure de lae prisonnièrex comme
"grandex méchanxte" et on laisse tout ça de côté. C'est ultra
invisibilisé, on ne sait rien des prisons. Quand tu vois un flic, tu ne
vois pas la prison derrière ses épaules, tu ne vois pas toutes les
caméras de surveillance qui l'entourent[^67].

## Pas de bienveillance pour la surveillance

\indent --- Le prochain truc dont on avait prévu de parler, c'est la répression
du copwatching.

--- Une des questions, c'est de savoir à quel point tu dois être visible
en manif. Certaines cellules militantes de copwatch s'affichent en tant
que telles. On a un brassard et on dit aux flics que c'est différent.
Le problème c'est que la plupart des personnes qui font ce choix
deviennent des cibles privilégiées de la police. Même quand tu
t'annonces et que c'est très clair dans la tête des keufs, tu fais
d'autant plus peur\index[tags]{peur} parce qu'ils savent très bien qu'ils font des
bavures et des débordements. Ils ont les boules et ils te prennent pour
cible plus que n'importe quellex militanxte.

--- Une fois, à Genève, j'étais en train de filmer, le flic m'a
directement retiré le téléphone\index[tags]{téléphone} des mains, il m'a insulté et m'a amené
de force\index[tags]{force} dans un coin avec tous ses collègues en rond autour de moi en
attendant que j'efface toutes les vidéos, alors que c'est tout à fait
légal de filmer la police dans l'exercice de ses fonctions. Ça a été
clairement perçu comme une provocation, il avait peur\index[tags]{peur} de mes images et
ça a justifié ce comportement tout à fait illégal de leur part.

--- Ça montre que le système a peur\index[tags]{peur} qu'on le voie sous un angle qu'il ne
maîtrise pas, qu'on voie autre chose que ce qu'il a décidé de montrer.
Il y a cette réaction de défense en mode "on doit être maîtres de notre
image", sinon, on est en danger. Du coup, on réprime celleux qui vont
montrer une autre image.

--- Les tribunaux français ont fermé le site Copwatch.fr, le motif
invoqué, c'était "diffamation". Est-ce que c'était le cas pour
l'ensemble des images qu'iels diffusaient ? Non, bien sûr, elles
n'étaient pas diffamantes, elles étaient juste vraies. Pour qu'il y ait
diffamation, il faut qu'il y ait diffusion de FAUSSES informations. Il a
fermé à cause d'une ligne dans sa description qui était insultante pour
la police. Tu crois vraiment que c'est normal de mobiliser des
procédures judiciaires aussi vastes juste parce qu'il y a écrit ACAB sur
un site ?

--- Voilà le fond de ce que c'est le copwatch, j'aime bien le mot
sous-veillance. Les keufs sont dans un espace public, c'est de la
surveillance mentale et physique pure et simple. À Lausanne, maintenant
ils ont des *body cameras* (des minicaméras fixées sur l'équipement des
policiers) dans le cadre d'un projet pilote qui va sûrement être
approuvé. À Zürich, le même projet pilote a eu lieu. Le Conseil cantonal
de sécurité vaudois prévoit de foutre ces caméras à tout son corps
policier.

--- Pendant les actions maintenant, il y en a toujours qui filment
continuellement les militanxtes, ça c'est pas un projet pilote par
contre, c'est systématique. Bientôt, il y aura des drones en masse, les
commandes de drones en France\index[tags]{france} pendant le confinement, c'est atroce. Il y
a la surveillance numérique, etc. Et on se rend compte que la
contre-surveillance ou la sous-veillance, ça dérange à fond, c'est de la
provoc', tout d'un coup les organes de surveillance ne se sentent plus
assez protégés.

--- C'est pour ça aussi que tu dois faire attention à ton propre
comportement quand tu filmes. Si tu gueules sur les keufs et qu'on le
remarque à l'image, ce qui est quand même très probable vu que le
téléphone est à trente centimètres de ta bouche\index[tags]{bouche}, tes images peuvent ne
plus être recevables devant un tribunal.

--- Jacqueline, c'est son gros problème, elle filme, elle met pause, elle gueule
et elle recommence à filmer.

(*Rires.*)

--- Le dernier truc dont on voulait parler, c'est "pourquoi faire du copwatch
alors que la presse est souvent présente ?".

## La presse

\indent --- Parce que les médias suisses sont une honte ?

--- La plupart du temps, quand on copwatche, il y a des médias partout et
les images de violence\index[tags]{violence} et d'abus de pouvoir\index[tags]{pouvoir} qu'on a sur nos natels et
qu'ils ont aussi, ils ne les diffusent pas. Peut-être par souci de
"neutralité" mdr.

--- Un autre truc, c'est que c'est pas pareil de filmer pour un téléjournal ou de
filmer pour un procès\index[tags]{procès}. Notre but c'est aussi de faire des vidéos
claires, de filmer le plus longtemps possible pour avoir plus
d'éléments de contexte et d'insérer des éléments écrits s'il le faut,
pour expliquer ce qu'il s'est passé. D'ailleurs, quand on monte les
vidéos qu'on envoie à la presse, on pourrait noter les articles de lois
qui ne sont pas respectés sous les images, même si à tous les coups les
journalistes les enlèveraient. Ah mais en fait la dernière fois, iels
ont effectivement enlevé les trucs qu'on avait écrits avant de diffuser
les images.

--- Ouais c'est vrai. Pff... de toute manière iels ne font pas leur
boulot, c'est ok on l'a compris. Ça filme un type qui casse une vitrine
pour faire le buzz, bravo super, ça m'énerve.

--- Je pense que toute information est la construction d'une situation
dans l'information, que ce soit le journal Le Monde ou ma chère maman
qui partage un truc sur facebook. On est toujours en train de construire
des récits pour essayer d'expliquer les choses. La question, c'est de
savoir dans quelle perspective on essaie de construire une situation.
Là, je pense que tu vas de toute façon être confrontéex aux
interprétations, même si tu fais ça dans une perspective révolutionnaire
qui est la mienne par exemple. Je filme pour montrer que l'existence de
la police est une erreur\index[tags]{erreur} politique, ben il y a tout un tas de monde qui
va me répondre que mon regard est biaisé. Je pense que la seule chose
qu'on puisse faire, c'est de donner un maximum d'éléments de contexte
et après on produit une information visuelle qui sera de toute façon
débattue et discutée, dans les tribunaux comme dans l'espace public
quand les personnes décident de la faire sortir dans la presse.
Finalement, on ne peut rien faire d'autre que d'essayer d'avoir une
éthique qui est quand même journalistique et informative, parce qu'elle
est juridique pour donner notre représentation de témoin, la plus fidèle
possible, et c'est évident pour tout le monde que toute personne qui
filme et qui prend la parole\index[tags]{parole} pour dire ce qui s'est passé parle en tant
qu'elle-même.

--- Et après, une image sur laquelle tu vois une personne à terre se
faire matraquer à répétition, c'est une information en soi. Tu peux
donner tous les éléments de contexte possibles, ça ne peut rien changer
à ces gestes. En plus, souvent, les "éléments de contexte" donnés par
la presse sont ceux de la police. Il y a eu plein d'exemples en Suisse.
L'effort de contextualisation est très soigné par la police qui fait
tout pour construire des portraits de grossexs méchanxtes militanxtes.

--- En fait, il y a une guerre de contextualisation, un combat de
narration : comment on raconte l'histoire ?

--- C'est un combat entre mondes possibles, un combat de narration.

--- C'est aussi pour ça qu'on filme en continu et que lors d'une
action par exemple on a mal aux bras. (*Rires.*) C'est pour minimiser un
maximum l'absence de contexte qui justifierait quoi que ce soit.

--- Dans la diffusion de ces images, il y a tout un tas de questions
différentes qui se posent. Il y a un mec sur youtube par exemple, il a
une carte de presse et il poste des longues vidéos dans lesquelles il se
promène en manif, il a beaucoup couvert les Gilets Jaunes. Il y a
souvent des abus de pouvoir\index[tags]{pouvoir} et des violences policières dans ses vidéos
et il fait ça dans une perspective critique vis-à-vis de la police.
Souvent, il y a une caméra\index[tags]{caméra} subjective et tu te promènes dans les rues de
Paris en pleine manif et tout d'un coup il se fait contrôler assez
violemment par la police et c'est comme si c'était toi. On ne le croit
pas quand il dit qu'il a une carte de presse, on l'empêche de filmer,
etc. C'est intéressant comme ressort je trouve. Il prend aussi le temps
de s'attarder sur les belles images, sur les couchers de soleil\index[tags]{soleil} entourés
de fumigènes et de gaz lacrymo... En fait, toutes ces questions sont
intéressantes aussi : la question de l'esthétisation, la caméra
subjective, musique ou pas, etc. Les téléjournaux *mainstream* sont aussi remplis
de musiques épiques et tragiques, c'est assez impressionnant. C'est pas
mal de faire l'exercice d'écouter constamment le *sound design* et la
création sonore des téléjournaux de temps en temps.

--- Après, nous ce qu'on fait, dans la méthode qu'on a définie, c'est que
chacunex gère et fait comme iel veut avec son image. Si toi t'es en
train de subir une violence\index[tags]{violence} policière et t'as envie de la médiatiser, on
dit ok on peut faire un montage, etc. Il s'est trouvé que la presse est
devenue un sujet parce que des militanxtes en ont exprimé l'envie, mais
notre enjeu principal n'est pas là. On s'en bat les steaks de la presse,
l'enjeu c'est d'aider pour les procès\index[tags]{procès}, de montrer à la police qu'elle
est surveillée, de soutenir les personnes qui sont en train de se faire
interpeller et d'être un soutien-témoin. Du coup, la presse dans cette
vision, c'est pas vraiment le sujet même si la question se pose une fois
que la personne concernée ou le collectif est en possession des images.
C'est aussi ça la différence avec un média\index[tags]{média} : on te dit ok, là tu peux
prouver ce qui t'est arrivé, as-tu envie ou non de montrer ça à tout le
monde ? La presse elle va pas venir te demander, elle va balancer des
images parfois hyper humiliantes sur internet\index[tags]{internet/web} sans ton accord et c'est
parfois chaud à assumer.

--- Aujourd'hui je me disais qu'il y a des lacunes dans le copwatch, en
dehors de nos buts qui ne sont pas de faire le buzz et d'envoyer tout à
la presse. D'un point de vue journalistique, il y a un moment où le
copwatching perd de la puissance ou en tout cas a des lacunes. Quand la
violence, c'est pas 50 coups de matraque par exemple. Il y a un moment
où toutes ces vidéos et le côté "spectaculaire" qu'elles ont, ben
elles viennent minimiser toute la violence\index[tags]{violence} moins spectaculaire ou tout
simplement cachée. Le profilage racial, les cellules horribles de garde
à vue dans lesquelles des personnes noires ou des manifestanxtes se
font enfermer pendant des heures pour que dalle, etc. Du coup c'est un
outil qui peine à montrer le problème systémique de la violence
d'État.

--- Il n'a pas de sens tout seul évidemment, mais de le faire en le
croisant à de l'antirep et à de l'information juridique, c'est en faire
un outil\index[tags]{outil} plus efficace. Et après sur la critique systémique de la
police, le copwatch ne se suffit pas à lui-même, c'est une pratique
militante, c'est pas une théorie\index[tags]{théorie}. C'est l'accumulation de tous les
outils qui amènent la mise en pratique, une critique systémique
offensive et révolutionnaire.

--- Mais il y a quand même quelque chose qui me dérange parfois, c'est
qu'il n'y a que les trucs les plus trash qui font de l'effet, tu vois ?
Mais je ne remets pas en question la pratique en soi, je me demande
parfois si la surmédiatisation de ces vidéos fait que les gens ne sont
plus du tout choqués des violences moins spectaculaires, mais à
répétition.

--- C'est compliqué d'échapper au spectacle général.

--- Notre logique\index[tags]{logique}, c'est la défense de personnes activistes et
militantes, le vrai sens de tout ça, c'est de faire en sorte que ces
personnes puissent continuer leurs luttes, qu'elles abandonnent pas. Ça
c'est méga important. Et c'est ce qui permet à une cellule\index[tags]{cellule} de copwatch
d'être à l'intersection des luttes et des causes dans la mesure où
l'enjeu est de protéger toutes les luttes. C'est comme faire de la
bouffe pendant des actions, c'est du *care*, et le copwatch peut aussi
se construire une forme de *care*, de soin.

--- Spectacle nulle part. *Care* partout.

[^64]: *They Don't See Us*\ \[n^o^\ 4\] témoigne des violences policières en Suisse.

[^65]: *Survivre dans un black bloc*\ \[n^o^\ 15\] aborde l'usage de la violence face à la police.

[^66]: *Drones*\ \[n^o^\ 1\] raconte un vécu affectif face aux policiers antiémeutes.

[^67]: Pour d'autres réflexions sur les prisons et le système judiciaire, lire *Abolir la prison, abolir le patriarcat*\ \[n^o^\ 33\] ou *Swiss-made prison system*\ \[n^o^\ 53\].

