---
author: "SUD Étudianxtes et Précaires"
title: "Lutter dans un syndicat étudiant"
subtitle: "Discussion entre quelques militantes du syndicat SUD Étudianxtes et Précaires"
info: "Transcription d'un entretien oral"
date: "24 septembre 2020"
categories: ["syndicalisme, luttes des travailleureuxses", "autogestion, expérimentations collectives", "relation à la militance", "combats institutionnels"]
tags: ["index"]
lang: ["fr"]
num: "19"
quote: "un horizon révolutionnaire en soi"
---

--- L'idée d'un syndicat étudiant en Suisse, c'est un peu particulier. En
France, il y a une tradition de lutte étudiante, mais en Suisse, à part
la CUAE[^53] qui se déclare syndicat étudiant, ça n'existe pas. Nous,
on s'appelle SUD Etudianxtes et Précairexs, on est un syndicat de lutte
indépendant qui existe depuis 2013. On se situe du côté des étudianxtes,
mais aussi plus largement du précariat°. On fait pas mal de défense
individuelle pour des personnes qui sont précaires. L'idée, c'est
d'aider les personnes à obtenir des moyens de se défendre, tout en
menant une bagarre juridique sur des situations d'oppression au travail
en particulier. Ce qui a lancé la création de ce syndicat, c'est le
besoin de penser sa condition en tant qu'étudianxte et on a très vite
pensé que ça pourrait servir pour touxtes les travailleureuxses en
formation. C'est pas parce qu'on est en formation qu'on ne fait pas
partie du prolétariat. La notion de travailleureuxses en formation
repose sur l'idée que les étudianxtes, même s'iels sont en formation,
sont producteurixes de valeur.

--- Tu travailles, quoi... et tu produis de la valeur. Une des
revendications qu'on a toujours portée, c'est le salaire étudiant. C'est
une revendication centrale.

--- Un syndicat étudiant a aussi du sens, parce que les étudianxtes
travaillent à côté de leurs études, iels ont souvent des jobs de merde\index[tags]{merde},
pas ou mal payés, précaires. L'idée, c'est de ne pas se limiter à la
défense individuelle, mais de créer un rapport de force\index[tags]{force} politique en
réunissant des défenses individuelles, en aidant les personnes à
construire des défenses communes, à se reconnaître dans leurs situations
mutuelles. Mais c'est dur d'y arriver, ça demande des forces.

--- Le collectif s'est construit par des combats de plus ou moins grande
envergure, situés parfois sur des lieux de formation précis comme
l'Université de Lausanne ou des campagnes transversales, comme celle sur
les bourses d'études qui touchait les apprentiexs, les étudianxtes en
haute école spécialisée et à l'université. Les défenses individuelles, c'est venu plus tard. Tout à
coup, on est devenuexs une espèce de pôle d'expertise (pour parler comme
les patronnexs) sur un certain nombre d'enjeux et de questions sociales
qui touchent aux conditions de vie, de travail et d'études.

--- Le syndicat s'est aussi créé à partir d'une déception\index[tags]{déception} fondamentale
vis-à-vis des structures institutionnelles, de la représentation
étudiante, en particulier à l'Université de Lausanne. Les
associations représentatives, qui refusent systématiquement d'intervenir
sur les questions sociales --- les conditions de travail et de vie des
étudianxtes seraient des trucs externes à l'université. Il fallait une
structure indépendante qui ne soit pas une structure liée à l'université, qui vise
la construction d'un rapport de force\index[tags]{force} politique, dans une visée de
transformation sociale. On fait de la défense individuelle, des trucs de
loi, envoyer des textes ou des lettres, mais c'est toujours inscrit dans
un horizon révolutionnaire.

--- Dans nos revendications et nos positionnements, il y a la gratuité de
toute formation, mais aussi de la bouffe\index[tags]{bouffe} et des transports, la
généralisation de l'accès aux formations pour tout le monde et on estime
aussi que tout service public doit être autogéré par les gens qui y
bossent. On veut pas de ces structures pyramidales avec toute la
violence qu'elles perpétuent pour le personnel en bout de chaîne, pour
les assistanxtes, les travailleureuxses du nettoyage externalisé qui
sont à la merci du service de sécurité de l'Unil (Université de Lausanne), du patron de la boîte
privée, mais aussi du mépris de certainexs étudianxtes.

--- La question des contenus des cours, on l'a moins traitée. Un peu, par
le biais d'une construction plus égalitaire du rapport de formation,
entre étudianxtes et personnel enseignant, où y'a des hiérarchies et une
toute-puissance dans la détention du "savoir".

## De beaux moments

**Office des bourses**. Moi j'ai envie de parler de cette occupation de
l'office des bourses en 2016, parce qu'on a réussi à créer un certain
rapport de force\index[tags]{force}. On est alléexs occuper le hall de l'office des bourses
pour exiger des négociations sur la mise en œuvre de la nouvelle loi.
C'était une période où il y avait des retards monumentaux. D'ailleurs,
notre première manif en 2013, c'était par rapport au changement de la
loi sur les bourses. C'était notre premier gros truc, en hiver, avec du
feu, des flambeaux, c'était beau. Iels avaient voté une loi, on l'a
analysée collectivement, on a dit qu'on était contre. On a fait une
coalition avec différents groupes et une manif d'environ 800 personnes.
On n'était pas 50 000, mais c'était une jolie manif. On a décidé de
faire une occupation pour repolitiser le sujet. On est arrivéexs avec du
café et des croissants pour les employéexs de l'office des bourses et on
a suspendu une banderole. On demandait, en même temps, plus de moyens
pour les employéexs, pour qu'iels puissent engager du personnel : iels
avaient, par personne, le double de dossiers que dans les bureaux de
Genève. On a distribué des salades aux députéexs pour... qu'iels
arrêtent de raconter des salades. Le problème fondamental, c'était qu'il
y avait à peu près six mois de retard dans les rendus de réponses des
bourses. Des mois pendant lesquels les étudianxtes ne percevaient aucune
thune : ces bourses sont le dernier filet social étudiant. La moitié du
bureau était en *burnout*. La loi, on l'avait perdue, mais on demandait
des négociations. Après deux ans de tractations pour dire qu'il manquait
du fric pour tout le monde, on a obtenu une rallonge de près de trois
millions sur le budget des bourses, ce qui n'est pas rien. Et puis on a
négocié ça relativement bien, dans le sens où c'était des choses qui
touchaient tout le monde et pas seulement certaines catégories de
boursièrexs, à savoir les forfaits repas, les forfaits logement, les
forfaits mobilité. Entrer dans le jeu institutionnel permet un accès
privilégié à cet office des bourses et c'est utile pour les défenses
individuelles. Ça nous a bien aidéexs pour passer certains dossiers en
accéléré, pour pouvoir\index[tags]{pouvoir} intervenir sur des situations particulières.

**Bourses d'étude --- FAE**. Même si on se considère, pour une partie en
tout cas, révolutionnaires, on a des victoires sur le terrain
institutionnel dont on est fièrexs. Un beau jour, à l'Unil,
l'association représentative (FAE) décide de baisser les montants des
aides d'urgence aux étudianxtes du fonds qu'elle a elle-même créé, alors
que c'est nos taxes étudianxtes qui alimentent ce fonds. Iels se sont
dit "il y a trop de demandes, faut que les montants qu'on donne
baissent", sans se poser une seconde la question inverse, sans
réfléchir à comment ce fonds pourrait avoir plus de thunes. On a réussi
à faire capoter ça. On a fait ça dans les règles. (*Rires.*) On a organisé
un référendum, genre le truc qui ne se fait jamais. On les a euexs sur
leur propre terrain, parce que le référendum universitaire a été créé
par les statuts de la FAE. Ce droit n'a été utilisé que deux fois depuis
qu'il existe et la FAE a perdu à chaque fois. Mobiliser ce droit a créé
un chaos assez monstrueux, les services informatiques de l'université
ont dû mettre en place une plateforme de vote\index[tags]{vote}. En fait, tu fais chier
tout le monde. C'est assez jouissif. On a mené campagne, ça nous a
conduixtes à faire notre première vidéo. Olé ! (*Rires.*) Les étudianxtes
ont voté et ont gagné. Les aides d'urgence n'ont pas été baissées.

**Harcèlement sexuel**. On a fait une campagne sur le harcèlement sexuel
dans toutes les Hautes Écoles. Il n'y avait pas que nous, c'était avec
la CUAE et le Collectif Féministe d'Etudianxtes en Lutte contre les
Violences Sexistes et le Harcèlement Sexuel (CELVS) à Genève,
l'Association Féministe Universitaire (AFU) à Lausanne et la Kritische
Politik (KriPo) à Zürich. Il y a eu une autre campagne destinée aux
apprentiexs, une campagne que le bureau de l'égalité du canton de Vaud
essayait de préparer depuis trois ou quatre ans. Le harcèlement sexuel
n'est reconnu que dans les relations de travail, salariées, ce qui est
profondément absurde\index[tags]{absurde/absurdité}. On demandait qu'il le soit aussi dans les lieux
de formation. On demandait un retrait de permis aux entreprises
formatrices en cas de harcèlement sexuel sur les apprentiexs. En 2019,
l'Unil a enfin produit le règlement sur l'égalité qu'on avait
demandé. On a ouvert une brèche\index[tags]{brèche} en 2015 et la Grève Féministe de 2019 a
conduit à l'impulsion finale. Sans la grève, on aurait sûrement attendu
encore 10 ans. L'ECAL est en train d'emboîter le pas, on verra
sûrement un règlement sur le harcèlement sexuel d'ici un ou deux ans.
On en chie longtemps, mais au final, ça paie quand même.

**Domino's pizza\index[tags]{pizza}**. C'est un exemple intéressant. On a été
contactéexs par des personnes qui bossaient là-bas, et à partir des
défenses individuelles, on a fini par avoir une petite organisation de
lutte au sein même de l'entreprise. On a réussi à faire virer plusieurs
gérants, les journaux en ont pas mal parlé, surtout parce qu'on a
beaucoup médiatisé le truc. On a gagné quelques petites victoires, elles
ont mis en lumière les conditions de travail horribles dans ces
boîtes-là. Mais au final, de nouvelles raclures remplacent les anciennes
et reprennent les licenciements abusifs. C'est la roue infinie du
travail syndical, dans toute sa splendeur déprimante.

**Les apprentiexs**. On travaille beaucoup sur les questions de
l'apprentissage, mais c'est le plus difficile. Ce qui pèche, c'est le
manque de moyens d'organisation chez les apprentiexs. Faut dire que leur
quotidien de travail est organisé de sorte à empêcher la formation
d'une conscience collective et d'intérêts communs. Iels sont
fragmentéexs sur différents lieux de travail, seulexs, et ne se
retrouvent à l'école qu'une fois par semaine. C'est presque encore
plus difficile avec ce qu'on appelle les préapprentiexs, toute la
main-d'œuvre que l'État et les institutions font bosser gratuitement.
C'est le cas aussi des personnes migrantes placées par l'État qui sont
complètement exploitées.

**La fête\index[tags]{fête} des cinq ans**. Il fallait qu'on fête\index[tags]{fête} notre survie dans un
climat particulièrement hostile, dans un paysage de luttes étudiantes
morne, voire complètement plat. Cette fête\index[tags]{fête} avait aussi une dimension
internationale, puisque nous avions des invitéexs de France\index[tags]{france} et de
Belgique qui gravitent autour de la pensée de Bernard Friot et qui
réfléchissent au salaire à vie. C'était sur deux jours, on accueillait
des camarades avec une fête\index[tags]{fête} le premier soir et ensuite y'avait une
journée de conférences. Il y avait des autoformations, une conférence
d'Aurélien Casta... le week-end de discussions avait débouché sur une
résolution internationaliste sur la nécessité d'un salaire étudiant
pour toutes les personnes en formation. Ça c'était beau, oui.

## Quel militantisme ? Quelles difficultés ?

--- La question de la relève est difficile dans ce syndicat, parce que
t'étudies pas toute ta vie. La transmission des savoirs pose aussi
problème. Ça fait deux ans que je suis là, mais je suis larguée, c'est
aussi parce que je suis pas suisse, donc y'a plein de trucs que je ne
connais pas sur les différentes formations, sur les lois, j'ai jamais
été à l'université de ma vie, etc. Il y a énormément de choses à
apprendre pour faire du travail syndical, la transmission des savoirs
est essentielle mais on est dans des cycles courts, c'est un vrai
problème.

--- Il y a aussi un truc un peu triste, il n'y a pas beaucoup de
révoltes dans les écoles. À l'ECAL (École cantonale d'art\index[tags]{art/artiste} de Lausanne)
par exemple, on a fait plein de trucs pendant une année, mais finalement
j'ai dû porter ce combat toute seule étant la seule syndiquée, ça m'a
épuisée, c'était chouette, mais l'année d'après j'étais trop fatiguée.

--- Il y a aussi la question du temps, c'est un grand engagement les
défenses, les campagnes politiques, etc. Et la plupart des gens ont
leurs études. Iels travaillent à côté et ont envie d'avoir un peu de
plaisir dans leur vie sociale, mais ça c'est comme toutes les luttes.

--- Quand il y a une super ambiance dans un collectif, ça peut devenir
méga fun les luttes, ça peut faire partie de ton temps libre, quelque
chose que tu fais avec plaisir. J'ai l'impression que ça fait longtemps
qu'il n'y a pas cet engouement de lutter ensemble, le fait de se
marrer tout en ayant la rage\index[tags]{rage}... Et c'est méga important. Je fais partie
d'un autre collectif militant avec lequel je passe neuf heures par semaine depuis
des mois et c'est un vrai plaisir.

--- Ouais mais le syndicalisme, y'a plein de moments où tu peux te
marrer, faire des manifs, des actions, c'est cool, mais y'a quand même
beaucoup de taf, faut le dire, qui est très chiant.

--- En fait, oui, à certains moments y'a des synergies et des dynamiques
collectives motivantes quand t'es dans un truc créatif, un truc qui peut
commencer à avoir une expression révolutionnaire par exemple, mais je
pense pas que tu t'engages politiquement parce que tu cherches du
plaisir.

--- Moi je le vois assez différemment. J'ai besoin de prendre plaisir à
ce que je fais même dans les luttes, le plaisir se mêle à la rage\index[tags]{rage}, la
colère, la frustration. Le plaisir est d'autant plus grand.

--- Ça peut être motivant, mais moi je suis pas pour dire que l'activité
révolutionnaire c'est ça. Le désir de changer les choses, c'est
différent que le plaisir que tu prends quand tu vas regarder un film\index[tags]{film}.

--- Ouais mais tu vois, quand tu participes à une manif que t'as
peut-être organisée, tu ressens de la joie et ce plaisir sera méga plus
puissant que quand tu regardes un film\index[tags]{film}. Et ce plaisir, cette joie est
liée à tout le travail collectif en amont, même légaliste.

--- Non mais il y a un truc qui s'est passé, elle a raison. On s'est
donné pour objectif, par exemple, la transmission des savoirs pour
mettre en place la répartition des défenses individuelles --- ce qu'on
n'a jamais réussi à faire. Nous, on veut mettre en mouvement les gens
sur des questions qui concernent leurs conditions de vie, de travail et
d'études. Il se trouve qu'historiquement on a eu un poids sur certains
sujets et qu'on appartient à une fédération syndicale "représentative"
du service public. À ce titre, on a *de facto* une raison d'être et
d'exister, y compris pour les autorités. On a réfléchi ensemble à ce
qu'on voulait faire pour mettre en marche les gens au sein d'un
mouvement d'émancipation. Et en même temps, il y a la misère du monde
qui nous arrive dans les bras. C'est hyper chaud de devoir s'occuper
d'un dossier syndical individuel. L'avenir de la personne que t'as en
face dépend de ce que tu fais, de tes décisions, de ta gestion
administrative de son dossier. C'est un truc lourd. Du coup, les gens ne
veulent pas prendre des défenses individuelles. Ce qui nous fait
"vivre", c'est pas de dire "J'ai gagné six mois de bourse pour telle
personne". Ce qui nous fait "vivre", c'est qu'on a donné les moyens ---
collectivement --- à des gens de vivre mieux, on a obtenu collectivement
une augmentation salariale pour les assistanxtes étudianxtes de l'uni,
on a obtenu que les parents en période de Covid ne soient pas traitéexs
comme de la merde\index[tags]{merde} à la Haute École de Travail Social --- c'est ça qui
nous intéresse. Et puis de faire des actions *avec* des gens, pas d'être
dans notre bureau à écrire des lettres. Alors il se trouve qu'on doit le
faire. Et on en reçoit trop pour les forces qu'on a. C'est le piège
ultime, quand on vient à manquer de propositions parce qu'on a trop à
faire, on s'institutionnalise si on ne fait plus de la politique telle
qu'on la conçoit, *avec* les gens, par en bas, contre le haut, dans la
rue.

--- Nos actions directes restent généralement dans le cadre de l'action
syndicale. On a un rapport assez bon enfant à l'illégalité. En général,
c'est des actions symboliques, comme quand on a tapissé les bâtiments
universitaires d'affiches, mais genre tapissé très abusivement, pour
faire tomber une directive qui soulignait que les affiches devaient
avoir été validées par la direction avant d'être apposées. Quand on
occupe des bâtiments, on ne demande pas d'autorisation, nos
rassemblements sont illégaux, mais comme on est une structure connue,
personne fait chier.

## Qui est dans le syndicat ?

--- Il y a des personnes qui viennent de tous les horizons. Et on essaie
d'être aussi dans les autres luttes. On porte une visée sociale et
transversale, on voit le syndicat comme un outil\index[tags]{outil}. Quand je vais à des
réunions avec des gens de la Grève du Climat, j'y vais en tant que
syndicaliste. Et en tant que syndicalistes, on essaie de porter une
politique, une analyse politique, des revendications politiques qui
viennent du fait qu'on est un outil\index[tags]{outil} prolétarien et puis que le climat,
le harcèlement, l'antisexisme, ce sont des questions prolétariennes.
C'est ça qu'il faut marteler. C'est pour ça que ça fait sens d'être dans
un syndicat et de toucher à toutes ces questions-là.

--- On fait pas de hiérarchie des luttes. On a une perspective
révolutionnaire globale.

--- L'autre jour après la réunion\index[tags]{réunion}, on discutait avec un camarade qui est
marxiste-léniniste, ce qui est quand même un mouvement très minoritaire
chez nous...

--- Insiste sur "minoritaire". (*Rires.*)

--- Ben oui, c'est le seul dans le syndicat. Mais c'est important de
confronter les différentes perspectives et expériences politiques.

--- Ici, il n'y a pas de position politique dogmatique ou de ligne à
adopter et c'est pas le but. Le but, c'est un horizon révolutionnaire en
soi, qui peut potentialiser des forces, des puissances.

--- On a des gens qui ne se disent pas révolutionnaires et qui ne le
seront jamais. On se rejoint touxtes sur un truc extrêmement flou, mais
qui n'est pas un positionnement sur un échiquier. Puisqu'il y a autant
des marxistes-léninistes que des libertaires, que des gens qui se disent
plutôt d'affinité Parti socialiste, il faut admettre qu'on essaie surtout de
s'organiser démocratiquement, en autogestion par le bas. Et
potentiellement on pourrait même avoir des gens de tout le spectre
politique, parce que personne n'est cohérent. On pourrait même avoir des
réacs qui tout à coup se mobilisent quand on touche à leur salaire.

--- On a seulement vocation à organiser les travailleureuxses et pas à
regarder qui tu es.

--- Le fondement de notre histoire est anarcho-syndicaliste. À SUD-EP, on
a établi des codes qui sont plutôt anarcho-syndicalistes, une
organisation par la base.

--- Mais c'est pas une position sur l'échiquier.

--- C'est une pratique avant tout, l'anarcho-syndicalisme.

--- On essaie, mais ça crée aussi des limitations : par exemple, personne
n'est payé, contrairement aux gros syndicats institutionnels. Alors on
manque de forces. Et en fait, on a besoin de forces... pour pas devoir
s'institutionnaliser nous aussi. C'est ça le paradoxe. Mais il y en a,
des dérives institutionnelles qu'on peut nous reprocher. On en est
pleinement conscienxtes. Quand on se fait balader, nous, à notre faible
niveau, dans une plateforme de bourse d'études : là on devient les
fameux "partenaires sociaux". On le sent, et ça nous fait mal de
l'être autour de cette table. À chaque fois qu'on en sort on se dit :
allez on arrête, on se barre de cette table. Mais pour faire quoi ?

--- La lutte !

## Qu'est-ce qui nous a poussées à militer ?

--- Je fais de la politique parce que la vie c'est de la politique, parce
qu'on se territorialise sur le champ social avant tout, et c'est
difficile de parler de motivations individuelles qui sont déjà, quelque
part, façonnées. Pour moi, l'individualité est façonnée, elle ne s'étaie
pas sur le corps\index[tags]{corps}, sur la biologie. La subjectivité est d'abord
collective, politique et sociale, on individualise dans un second temps.

--- Moi ça me dépasse tout ça, tu vois. Quand je suis arrivée en Suisse,
j'ai rencontré par hasard quelqu'un qui était chez SUD et on a commencé
à parler d'anarchisme. Je me suis dit "Tiens, c'est la première
personne que je rencontre en Suisse avec qui je connecte en termes
politiques". C'était à un moment où je vivais très mal l'école dans
laquelle j'étais, tout me semblait violent, c'était la merde\index[tags]{merde} et en fait
je voulais faire quelque chose, mais je savais pas quoi et je me sentais
seule. La personne m'a proposé de venir à une réunion\index[tags]{réunion} du syndicat. Je
suis venue et je me suis lancée là-dedans. C'était l'outil\index[tags]{outil} pour faire ce
que j'avais envie de faire à ce moment-là en termes politiques.

--- Y'a des gens qui commencent à faire de la politique institutionnelle
parce qu'ils ont envie du pouvoir\index[tags]{pouvoir}. Ce sont des gens qui veulent se faire
élire. La vraie question, c'est comment on devient militanxte politique
de base, et comment on le reste, sans commencer à servir ses intérêts
propres. En fait, c'est aussi une histoire de pouvoir\index[tags]{pouvoir}, mais différente,
c'est l'envie d'avoir un outil\index[tags]{outil} collectif pour retrouver une emprise
sur sa vie. C'est aussi simple que ça.

--- Moi je milite pour sortir de ma subjectivité triste, toute façonnée.
Individuellement, notre monde est super triste. Alors se retrouver,
créer, co-construire ensemble des espaces de convivialité, des espaces
de lutte, c'est une joie, mais c'est aussi que tu construis un rapport
de force\index[tags]{force} avec le monde autoritaire. C'est sûr, on refait de nouveaux
espaces. Quand on arrive autour de la table, qu'on fait des trucs
ensemble, on est toujours dans le même monde, avec ces mêmes rapports de
force, ces mêmes choses, et toute la question c'est comment on arrive à
sortir de ça. Parfois on y arrive, juste trois secondes. Trois secondes
où on arrive à sortir de ça, après on y replonge, mais ces trois
secondes, elles sont loin d'être dérisoires.

--- Moi, quand j'étais jeune, j'avais besoin de faire bouger les
choses, mais j'étais dans un endroit très isolé, alors je me suis
inscrit dans un parti, un peu par défaut, ce genre de conneries de
jeunesse. Et je suis arrivé à l'université où j'ai vu des gens se
mobiliser pour changer leur condition. Quand je parlais avec elleux, je
me sentais mille fois plus connecté que quand je parlais à des
politiciennexs. Et dès que j'ai pu faire des réunions, ça m'a botté. La
controverse interne, c'est aussi quelque chose qui m'a fait rester.
J'étais beaucoup plus stimulé, beaucoup plus que par les cours.
J'étais extrêmement secoué dans mes préjugés, dans mes opinions
politiques, certaines très arrêtées, je me rendais compte qu'elles
étaient peut-être un peu trop arrêtées.

--- Toi tu travailles à côté en plus.

--- Oui, je fais différents boulots. Là, je travaille principalement de
nuit comme veilleur.

--- Et t'en as jamais marre ? Y'a pas des moments où tu te dis que tu vas
abandonner le syndicalisme ? Parce que moi, ça m'arrive.

--- Ben y'a des moments où à cause du travail, j'venais plus au syndicat.
C'est surtout l'aliénation au travail qui pesait sur mon engagement en
politique. Mais je suis toujours là.

[^53]: CUAE : La Conférence Universitaire des Associations d'Étudianxtes de l'Université de Genève --- la faîtière des associations facultaires.
