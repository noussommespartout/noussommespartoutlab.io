---
author: "Texte collaboratif entre plusieurs participanxtes de cet ouvrage"
title: "Tu sais faire quoi ?"
subtitle: "Tout le monde pourrait participer"
info: "Texte rédigé pour le recueil"
date: "mars 2021"
categories: ["DIY"]
tags: ["index"]
lang: ["fr"]
num: "56"
quote: "mais la lutte a besoin de tout et de tout le monde"
---

Ça doit t'arriver, de te dire que tu n'es pas assez capable, pas assez
compétenxte, que ce que tu sais faire, ce que tu as appris à faire, ce
que tu aimes faire, ce que tu fais ne peut pas servir aux luttes qui
t'entourent et auxquelles tu aimerais participer.

Mais la lutte a besoin de tout et de tout le monde.

Alors, tu sais faire quoi ?

Si tu sais faire du graphisme, tu peux créer des affiches
pour des collectifs.

Si tu sais souder du métal, tu peux renforcer des barrières qui
protègent les lieux autogérés et construire des barricades du futur.

Si tu sais utiliser les réseaux sociaux, tu peux faire circuler au
maximum les appels à mobilisation.

Si tu sais réparer de l'électronique, tu peux bidouiller les ordinateurs
pour limiter la surveillance.

Si tu sais coudre, tu peux fabriquer des belles banderoles et des belles
cagoules pour les manifs.

Si tu sais imprimer et relier, tu peux fabriquer des brochures ou des
livres contestataires pour les infokiosques° permanents ou les
infokiosques éphémères de manifs.

Si tu sais lire, tu peux t'informer, te politiser.

Si tu sais prendre la parole\index[tags]{parole} en public, tu peux penser à parfois la
laisser aux autres.

Si tu sais faire de la chirurgie, tu peux aider celleux que la
domination abîme, tu peux aider des personnes à transitionner°.

Si tu sais faire de la plomberie, tu peux entretenir les canalisations
des centres sociaux, des refuges, des squats ou fabriquer des systèmes
de récupération d'eau\index[tags]{eau} de pluie.

Si tu sais faire à manger, tu peux organiser des cantines populaires
dans ton quartier ou faire à manger pour les fins de manifs[^166].

Si tu sais appliquer les premiers soins, tu peux devenir *street
medic°*.

Si tu sais parler plusieurs langues, tu peux servir d'interprète pour
celleux qui en ont besoin, traduire des documents administratifs et
écrire des lettres pour celleux qui subissent les oppressions
bureaucratiques de l'État.

Si tu sais travailler le bois, tu peux construire un mirador pour une
ZAD°, une terrasse pour un centre autogéré, des meubles pour un abri.

Si tu sais conduire, tu peux faire chauffeureuxse pour des récups dans
les poubelles des supermarchés.

Si tu sais faire du pain, tu peux en distribuer pour faire tenir les
blocages ou les vendre au marché pour aider à payer des amendes.

Si tu sais écrire de la poésie, tu peux créer des slogans pour les
manifs, pour des collages ou écrire des textes sur les panneaux
publicitaires.

Si tu sais faire de la musique, tu peux rejoindre une fanfare militante.

Si t'es banquier, tu peux faire la vaisselle à la fin des
réunions.

Si tu sais broder, tu peux remplacer tous les logos des t-shirts de tes
potes par des dessins ou des messages anticapitalistes.

Si tu sais réparer des vélos, tu peux lancer un atelier de réparation
collectif pour qu'il y ait un maximum de monde à la prochaine *Critical
mass°* près de chez toi.

Si t'as de la voix\index[tags]{voix}, tu peux venir à la prochaine manif et
gueuler très fort.

Si tu sais filmer, tu peux commencer à pratiquer le copwatch°
systématiquement quand tu vois une personne se faire
interpeller[^167].

Si tu sais faire du son, tu peux lancer une radio (pirate\index[tags]{pirate}) pour faire
entendre les voix\index[tags]{voix} de celleux qu'on n'entend pas.

Si tu sais couper les cheveux, tu peux faire des coupes
gratuites pour les plus précariséexs.

Si tu connais la loi, tu peux faire du conseil juridique
ou des défenses lors de procès\index[tags]{procès} de militanxtes attaquéexs ou de personnes
précarisées.

Si t'es chimiste, tu peux développer une formule pour un
cocktail molotov bio et écolo[^168].

Si t'es policier, tu peux démissionner[^169].

Si tu aimes la randonnée, tu peux faire de la cueillette et redistribuer
ta récolte.

Si tu sais faire de la boxe et que t'es pas un mec cis°,
tu peux organiser un cours d'autodéfense en mixité choisie°.

Si t'as de la thune, tu peux soutenir les luttes de ta
région.

Si tu sais faire de la danse, tu peux aider celleux qui ne
sont pas à l'aise avec leur corps\index[tags]{corps}.

Si tu sais tricoter, tu peux faire des chaussettes et des gants pour
celleux qui ont froid.

Si tu sais faire quoi que ce soit, tu peux créer un
atelier pour partager tes compétences avec les autres.

Si tu sais faire des massages, tu peux installer un stand
lors d'événements militants pour faire du care°, pour soigner les
militanxtes fatiguéexs.

Si tu penses vraiment que tu ne sais rien faire, c'est pas grave, viens
quand même.

[^166]: Pour voir à quoi ressemble une semaine type d'une cantine autogérée, lire *Le Grand Midi*\ \[n^o^\ 47\].

[^167]: Sur le copwatching, lire *Surveiller la surveillance*\ \[n^o^\ 55\]. Pour écouter plusieurs membres d'un collectif de copwatch suisse romand discuter de leurs expériences, lire *Spectacle nulle part.* Care *partout*\ \[n^o^\ 23\].

[^168]: *Piraterie ordinaire*\ \[n^o^\ 38\] et *Le compost généralisé*\ \[n^o^\ 41\] décrivent des tentatives allant dans cette direction.

[^169]: Si tu veux savoir pourquoi, tu peux lire *Jean Dutoit en lutte*\ \[n^o^\ 13\] ; *Vous détruisez une Spyre, on en reconstruira plein*\ \[n^o^\ 18\] ; *Drones*\ \[n^o^\ 1\] ; *Kill the hippie in your head*\ \[n^o^\ 48\] ; *Spectacle nulle part.* Care *partout*\ \[n^o^\ 23\]; *Surveiller la surveillance*\ \[n^o^\ 55\] ; *Faudrait pas que notre révolution ait l'air trop révolutionnaire*\ \[n^o^\ 44\] ; *Survivre dans un black bloc*\ \[n^o^\ 15\] ; *They don't see us*\ \[n^o^\ 4\] ; *Brisons l'isolement*\ \[n^o^\ 39\] ; *Swiss made prison system*\ \[n^o^\ 53\] ; *Abolir la prison, abolir le patriarcat*\ \[n^o^\ 33\] et *Arrêtons de "défendre"*\ \[n^o^\ 28\].

