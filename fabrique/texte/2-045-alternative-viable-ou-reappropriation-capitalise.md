---
author: "Anonyme"
title: "Alternative viable ou réappropriation capitaliste"
subtitle: "Expérience d'un membre d'une association permaculturelle romande"
info: "Texte rédigé pour le recueil"
date: "mars 2021"
categories: ["écologie", "autogestion, expérimentations collectives", "combats institutionnels"]
tags: ["index"]
lang: ["fr"]
num: "45"
quote: "des socioécosystèmes autosuffisants et résilients"
---

Entre 2013 et 2018, j'ai eu l'occasion de faire partie d'une association
estudiantine de permaculture en Suisse romande. Je ne parlerai que de
mon vécu au cours de cette période, car je n'y suis plus actif, et donc
ne peux ni ne veux parler au nom des personnes qui en sont actuellement
membres.

J'ai intégré l'association par le biais d'unex amiex, que je ne
remercierai jamais assez pour cette invitation, pour son
investissement et pour sa capacité de gestion lors de ces années. L'objectif
de l'association était d'amener la permaculture dans le milieu
universitaire et de pouvoir\index[tags]{pouvoir} se réapproprier une parcelle de terrain pour
y planter un jardin\index[tags]{jardin} d'expérimentation permacole. Nous mettions en avant
le fait que la permaculture est politique et avions à cœur d'organiser
des conférences et ateliers qui puissent aborder différentes
problématiques d'un point de vue global. Nous avons organisé par exemple
des conférences sur les alternatives aux pesticides, les politiques
agricoles en Suisse, la résilience de nos socioécosystèmes, les vers de
terre et les arbres face aux problématiques environnementales ou encore
le réformisme écologique. Petit à petit, nous avons tissé des liens en
dehors de l'université, notamment avec le réseau de permaculture Suisse
romande, via lesquels nous avons fait de très belles rencontres, et
d'autres qui se sont avérées problématiques, notamment parce que nous ne
partagions pas la même définition de ce qu'est la permaculture.

Lors de nos conférences d'introduction à la permaculture, nous l'avons
définie brièvement comme un ensemble d'outils visant à mettre en place
des socioécosystèmes autosuffisants et résilients. Puis, nous avons
ouvert plus largement notre point de vue et abordé le fait que ce terme
est devenu un mot fourre-tout, pouvant englober un peu tout et n'importe
quoi et facilement réapproprié à des fins capitalistes, ce qui implique
une certaine vigilance. Nous avons par exemple très rapidement remarqué
qu'une partie du milieu permacole suisse romand s'efforçait à l'époque
de présenter la permaculture comme étant apolitique, ce qui nous
semblait non seulement faux mais aussi dangereux. De nombreux survivalistes nationalistes se revendiquent de
la permaculture et savent très bien construire des microécosystèmes très
productifs, ce qui leur confère beaucoup de notoriété. Ceci va à l'encontre même
des débuts de la permaculture : les deux auteurs "officiels" du terme,
Mollison et Holmgren, se basent sur des auteurs anarchistes comme
Kropotkine pour mettre en avant la création de petites communautés
interdépendantes qui fonderaient l'idéal d'une société permacole. Nous
avons remarqué que la permaculture a été progressivement récupérée par
le capitalisme vert. Les Cours de Design en Permaculture (CDP) en sont
un exemple flagrant. Il s'agit de la première formation reconnue dans le
domaine, un cours immersif de deux semaines durant
lesquels les participanxtes vont se plonger dans ce qu'est la
permaculture et son application à plusieurs échelles, que ce soit dans
différents climats ou au sein de collectifs humains. Cette formation
existe depuis plusieurs dizaines d'années et est disponible dans le
monde entier, portée par différentes associations, universités
populaires ou individus pratiquant la permaculture. Elle veut établir
une base commune de ce que peut être la permaculture grâce à un petit
cahier des charges, mais les conditions matérielles de la formation
varient énormément selon la manière dont elle est organisée. Parmi
les variations, il y a surtout celle du prix\index[tags]{prix}. Un CDP en France\index[tags]{france}, en
Allemagne ou encore en Angleterre coûtait à l'époque entre 400 et 600
euros pour deux semaines. La Suisse, cependant, ne proposait à notre
connaissance que des CDP à partir de 1200\ CHF, un prix\index[tags]{prix} exorbitant qui
rend la permaculture inaccessible à la plupart des personnes qui en ont
le plus besoin. Un tel prix\index[tags]{prix} cantonne la permaculture aux jardins
potagers de personnes pouvant payer leur formation et qui bien souvent
ne dépendent pas de leurs cultures pour améliorer leur alimentation.
L'idée originelle était de faire en sorte que les populations les plus
démunies puissent devenir plus autosuffisantes et prendre en main leur
alimentation, tout en créant des écosystèmes naturels résilients (ce que
montre l'exemple des jardins permacoles des populations précaires dans
diverses villes des États-Unis, que la faim a poussé à squatter des
friches pour se nourrir et se rencontrer). Nous étions conscienxtes que
nous faisions aussi partie des privilégiéexs, notamment parce qu'aucunex
d'entre nous ne dépendait de notre jardin\index[tags]{jardin} pour se nourrir. Nous ne
remettions pas en question le fait que les personnes qui organisent une
telle formation souhaitent se rémunérer et nous reconnaissions que
plusieurs CDP proposaient des réductions pour des personnes qui ne
pouvaient pas payer une telle somme, mais ce prix\index[tags]{prix} nous semblait tout de
même excessif.

Nous avons donc décidé de proposer nous aussi un CDP, mais à un prix
plus accessible. Ce cours a été mis en place trois ou quatre ans après
la formation de l'association. Ça ne s'est pas fait sans accrocs,
notamment au niveau de la certification. Pour que ce cours soit reconnu,
il faut qu'il soit "certifié" par une personne qui a déjà fait le CDP
et deux ans d'immersion dans un projet permacole
préexistant pour y acquérir de la pratique. À l'époque, une seule
personne en Suisse répondait à ces conditions et se trouvait dans la
capacité de certifier les CDP, ce qui rendait sa présence essentielle
partout. En plus de découvrir qu'il existait une sorte de monopole de la
certification des CDP, nous n'avions pas beaucoup d'affinité avec cette
personne, ni dans son approche de la permaculture ni dans ses tarifs.
Nous avons donc fait appel aux groupes Permaculture Italie et
Permaculture Australie (le groupe de permaculture Australie étant
informellement l'organisme central au niveau international) qui nous ont
fourni une certification. Nous avons commencé en 2017 à proposer un CDP
à 500\ CHF. Comme il a lieu sur le site de l'Université, on nous a
demandé que la majorité des participanxtes soient des étudianxtes, ce
qui constitue une limite\index[tags]{limite} fondamentale. Cependant, à chaque édition, des
places étaient réservées pour le personnel de Parcs et Jardins de
l'Université et nous avons réussi à négocier également des places pour
des personnes extérieures au milieu universitaire, ainsi qu'une ou deux
places non payantes en échange d'une aide dans l'organisation générale.

Nous avons été contactéexs avant la première édition par la personne qui
avait le monopole de la certification. Elle a affirmé que nous n'étions
pas en droit de fournir un tel cours, non seulement sur le plan légal
mais aussi sur le plan financier, puisqu'elle contestait la possibilité
d'offrir des cours à un prix\index[tags]{prix} aussi peu élevé. Nous avons répondu que
nous avions toutes les bases légales pour le faire et que la
permaculture devait être la plus accessible possible.

Quelque temps avant mon départ de l'association, nous discutions souvent
de l'évolution du terme de "permaculture", qui devenait creux,
passe-partout, réapproprié et dépolitisé par plusieurs mouvances. Je
reste personnellement convaincu que pour que la permaculture puisse
atteindre l'objectif pour laquelle elle a été créée, c'est-à-dire
proposer des alternatives pour faire face aux problématiques
sociopolitiques et écologiques actuelles, elle ne doit pas perdre ses
bases anarchistes, ses inspirations puisées auprès de diverses
communautés à travers la planète.

Si certainexs permaculteurixes, bien plus expérimentéexs que nous, vont
dans ce sens en Suisse romande, nous ne pouvons nier qu'une part du
mouvement vrille vers un capitalisme vert, qui se dit apolitique et qui
reproduit divers schémas de domination. Nous avons aussi observé beaucoup
d'influence New Age et ésotérique, ou encore l'apparition de gourous en
permaculture, le tout souvent couronné de libéralisme. La permaculture
est encore bien loin d'être exempte des schémas de domination qui
traversent nos sociétés occidentales, mais elle propose des pistes de
réflexion qui peuvent s'avérer intéressantes, notamment au niveau de
l'autogestion. En général, les projets permacoles sont
collectifs, car il est très difficile d'assumer seulex la quantité de
travail nécessaire pour mettre en place un écosystème. Il faut donc
réapprendre à gérer la communication et l'organisation collective, ce à
quoi la permaculture répond par une recherche d'horizontalité\index[tags]{horizontal/horizontalité}, de
différents types de consensus, d'écoute et de bienveillance\index[tags]{bienveillance}. Je dois
avouer que l'association dont je faisais partie était très enrichissante
sur ce plan, non seulement grâce à l'expérience de certainex membres,
mais grâce à l'amitié qui a fini par nous lier. En travaillant à
construire des socioécosystèmes, j'ai aussi pu remettre en question
beaucoup de mes comportements, surtout des comportements masculins, et
ceci grâce au collectif. Évidemment, ce sont des outils qui ne sont pas
propres à la permaculture, elle doit les valoriser afin d'éviter les
dérives autocratiques qui sont contraires à ses fondations
antiautoritaires. Un principe essentiel de la permaculture dit que tout
élément doit assurer plusieurs fonctions et que chaque fonction doit
être assurée par plusieurs éléments afin d'aboutir à un socioécosystème
résilient où tout le monde se sent à l'aise et inclusex.

Aujourd'hui, j'observe jour après jour combien cette expérience a marqué
mon militantisme et ma manière d'être. C'est au travers de la
permaculture, et des gens qui la font vivre, que j'ai pu découvrir
l'anarchie, les rapports de domination entre les différentes sphères du
vivant et entre les êtres humains, et surtout mes privilèges. Je cherche
activement à minimiser l'oppression que j'exerce autour de moi, que ce
soit sur des humains ou non humains, et à maximiser la bienveillance\index[tags]{bienveillance},
l'écoute et le soutien aux personnes qui m'entourent, sans oublier de
réagir quand une situation ne me semble pas acceptable.

Actuellement, je m'investis principalement dans des activités et
collectifs liés à la transformation et à la distribution alimentaire,
ainsi que dans certains projets agricoles en agroforesterie et en
permaculture. Au travers de ces rencontres, j'ai eu l'occasion de
travailler dans des fermes de production de viande et de produits
laitiers, ce qui m'a fait devenir *vegan* et m'a permis d'approfondir
mes questionnements sur l'interaction entre les êtres humains et leur
environnement. Cultiver et manger restent le socle de mes interactions
sociales, dont les fonctionnements me rappellent les socioécosystèmes
qu'imaginait Kropotkine et les réseaux souterrains des hyphes de
champignons symbiotiques. Il n'y a pas de révolution sans pain, et notre
pain est politique.

