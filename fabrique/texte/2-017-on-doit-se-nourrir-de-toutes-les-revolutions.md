---
author: "Hêvi (Espoir)"
title: "On doit se nourrir de toutes les révolutions"
subtitle: "Une trajectoire kurde dans la lutte globale"
info: "Transcription d'un entretien oral"
date: "novembre 2020"
categories: ["féminismes, questions de genre", "relation à la militance", "antiracismes"]
tags: ["index"]
lang: ["fr"]
num: "17"
quote: "la volonté de mes camarades, c'est de s'ouvrir au monde et aux autres luttes"
---

Je suis une Kurde, née dans une grande ville turque et j'ai grandi en
Suisse. D'après mon passeport\index[tags]{passeport}, je suis turque. On ne peut pas être kurde
sur un passeport\index[tags]{passeport} : l'État turc nie toute existence à l'identité kurde.
Depuis 2009, je suis naturalisée suisse et aujourd'hui, je pense, je
m'exprime et je rêve en français. Le turc, je le parle avec un accent
français ; le kurde, je ne le parle pas.

J'ai grandi dans le rejet de mes origines, de mes appartenances, de la culture kurde. Mon père et ma mère\index[tags]{mère} se
sont installées en Suisse, nous laissant mon frère et moi en Turquie,
avec nos grands-parents maternels. J'avais trois ans quand mon père
s'est installé en Suisse, ma mère\index[tags]{mère} l'a rejoint deux ans plus tard. Je ne
les ai pas revues avant mes huit ans. Iels ne nous ont pas expliqué leur
départ. Ma mère\index[tags]{mère} a seulement dit "Je vais chez le médecin, je reviens",
et elle n'est pas revenue. Nous étions trop jeunes pour comprendre, du
moins c'est ce qu'iels pensaient, iels ont voulu nous épargner et ont
créé, sans le vouloir, un traumatisme profond.

Leur départ pour la Suisse était à la fois économique et politique, ce
qu'iels voulaient, c'était nous offrir "une meilleure vie". Jusqu'à
mes huit ans, j'ai vécu dans une grande ville turque, une ville où il
fallait toujours cacher nos origines pour notre sécurité. Lorsqu'on
écoutait de la musique kurde, il fallait fermer les fenêtres pour que
les voisinexs n'entendent rien. Un des rares souvenirs de mon enfance,
c'est justement le jour où nous avons pris l'avion pour la première fois
avec mon frère, pour nous rendre en Suisse. Il avait six ans. Une
hôtesse de l'air --- je me souviens qu'elle était gentille --- nous a
guidées jusqu'à nos parents. Nous restions des enfants et nous
considérions nos grands-parents comme nos véritables parents. Ces deux
personnes que nous retrouvions là, ces étrangères, c'étaient celleux qui
nous avaient obligées à quitter le seul foyer\index[tags]{foyer} que nous n'ayons jamais
connu. Alors, quand ma mère\index[tags]{mère} essayait de nous parler en kurde, nous nous
bouchions les oreilles.

Une fois en Suisse, j'ai essayé de m'intégrer, de ne pas être rejetée.
Je ne voulais pas être "l'étrangère", mais ça ne te quitte jamais.
Quand t'es migranxte, t'es étrangèrex dans le pays où tu arrives et tu
deviens inévitablement étrangèrex dans le pays que tu quittes. Le rejet
de ma culture d'origine est probablement lié à ces multiples
traumatismes : abandon, humiliation, rejet, peur\index[tags]{peur}, déracinement, volonté
d'intégration, désir d'appartenance.

Aujourd'hui, j'ai 39 ans et je suis infirmière. Mon engagement militant
est présent dans toutes les sphères de ma vie. J'ai toujours ressenti le
besoin d'infuser du sens à ce que je vis, j'ai toujours voulu construire
une cohérence entre mes valeurs, mes croyances, mes idées et mes
actions. Je suis combative, révoltée et opposée au système depuis
l'enfance. Issue d'un peuple qui subit un génocide, je n'ai pas d'autre
choix que de lutter, c'est inscrit dans mon ADN. Je suis vite entrée en
force dans le mouvement des femmes\*° kurdes. Je fais maintenant partie
de l'Assemblée des Femmes\* Kurdes et j'y suis très active.

La lutte kurde est complexe et diverse, je commence tout juste à la
comprendre réellement. Nous sommes la plus grande minorité au monde sans
État-nation, une minorité séparée de force\index[tags]{force} entre quatre nations. En
Turquie, on nous assimile de force\index[tags]{force}, ma famille en est un bon exemple. Je
pense que certainexs de mes petixtes cousinexs ont même oublié qu'iels
ont des racines kurdes. D'autres sont devenuexs des nationalistes
turcquexs. Peu de personnes réalisent l'ampleur du génocide en cours.
Dans l'activité révolutionnaire et émancipatrice du mouvement kurde, les
femmes\* jouent un rôle central. Le mouvement des femmes\* kurdes est
très bien organisé, une organisation qu'il doit à son ancienneté, aux
années passées à lutter. Le postulat de base d'Abdullah Öcalan, un des
leaders du PKK (Parti des travailleureuxses du Kurdistan), c'est
que la libération de la société passe par la libération de la femme\*. Son travail a d'ailleurs contribué à l'essor de la Jinéolojî, une science des femmes*.
L'idéologie d'Öcalan a été un outil\index[tags]{outil} très puissant pour
implémenter une nouvelle organisation politique féministe, pluraliste et
écologiste, qui plus est dans un coin du globe où ça aurait pu sembler
improbable. Öcalan a écrit des dizaines de livres, il a produit une
idéologie politique globale, mais le plus intéressant, c'est qu'une
partie du peuple kurde se l'est véritablement appropriée, a commencé un
travail de concrétisation de l'utopie.

On retrouve un souci politique égalitaire et paritaire jusque dans
l'organisation de la guérilla, ce qui n'est pas la norme dans les luttes
armées de la planète. Cette lutte, même dans la diaspora, continue à
être très active et fertile. Le fait de s'organiser et de lutter
ensemble ici, en Suisse, ça donne un sens au fait d'être ici, ça
justifie d'être tout le temps l'étrangère. Ce n'est déjà pas facile
d'être une femme\*, mais être une femme\* migrante qui vient d'un pays à
majorité musulmane ouvre à tellement de stéréotypes et de préjugés.
Faire partie d'une communauté de femmes\* en lutte donne de l'unité, du
courage, un sentiment d'appartenance, et ça c'est fort. Le fait de se
sentir faire partie de quelque chose de plus grand que soi, c'est
essentiel, et nourrissant[^49].

C'est aussi cohérent sur le plan politique, puisque l'un des fondements
intellectuels de la libération kurde, c'est qu'on n'est pas une île,
qu'on ne peut pas vivre en paix si le reste du monde est en guerre. Donc
la libération des femmes\*, elle est importante dans le monde entier et
pas seulement au Kurdistan. La lutte est globale et commune. La volonté
de mes camarades, c'est de s'ouvrir au monde et aux autres luttes. Il y
a de plus en plus de liens qui se créent. Par exemple, j'ai rejoint le
collectif de la Grève Féministe, j'y représente l'Assemblée des Femmes\*
Kurdes[^50], je soutiens la Grève du Climat et Extinction
Rebellion[^51], je suis proche des milieux squats, anarchistes,
communistes. Selon moi, au-delà de la diversité de nos idéologies et de
nos outils, nous luttons pour un horizon de transformation commun. Je
pourrais mettre mes œillères et vivre une vie relativement confortable
ici en Suisse, beaucoup de gens le pourraient et le refusent, beaucoup
de gens le font. Lutter n'a rien de facile, c'est inconfortable, ça
demande du temps et de l'énergie, des sacrifices, des remises en
question parfois douloureuses. Il faut sans cesse argumenter face à des
détracteurixes qui peuvent se montrer violenxtes pour protéger leur
vieux monde confortable. On ne le fait pas juste pour le plaisir de le
faire : il y a quelque chose de puissant qui nous pousse, une pulsion de
vie.

Le but de nos révolutions, de nos énergies révolutionnaires, c'est la
liberté. Ici en Occident, même chez certaines féministes, il y a cette
idée latente que les femmes\* ont des vies libérées, parce qu'il y a une
certaine reconnaissance au niveau des lois, des droits, des
constitutions, parce qu'on n'est pas enfermées à la maison, parce qu'on
peut conduire, parce qu'on peut voter. Mais dans les mentalités, dans la
mise en pratique des lois, il y a encore tellement de chemin à faire,
sinon comment expliquer que la révolution féministe pointe le bout de
son nez en Suisse aussi ? L'enfermement est peut-être moins visible ici,
mais comme le disait une féministe tunisienne "on est enfermées dans
l'apparence". Il faut rester jeune, belle, mince, être comme ci, être
comme ça, subir des injonctions, parfois contradictoires.

Je connais des femmes\* qui ont l'impression d'avoir perdu tout pouvoir
d'action en arrivant en Suisse. Au pays, elles étaient combatives, sans
peur, elles étaient dans la rue\index[tags]{rue}, elles ont subi les pires violences sans
sourciller, elles s'organisaient, elles luttaient. Ici, avec la
barrière de la langue, face à un système dont elles dépendent beaucoup
plus et qui leur rappelle sans cesse leur statut, elles se sentent
souvent diminuées et n'osent pas s'exprimer. Je suis rentrée
simultanément dans le mouvement kurde et dans le collectif de la Grève
Féministe. Au sein du deuxième, il existait déjà un groupe de femmes\*
migrantes qui avait été initié par des femmes\* kurdes, africaines et
chiliennes. C'est important qu'il y ait une représentation des femmes\*
migrantes ou kurdes au sein d'un mouvement comme celui de la Grève
Féministe. Un collectif aussi large donne de la visibilité. La
visibilité leur donne de la légitimité.

On m'a demandé de représenter les femmes\* kurdes dans ce groupe. Ça me
semblait aller de soi parce que j'ai plusieurs identités, plusieurs
casquettes. Pendant de nombreuses années, je luttais à mon niveau, dans
mon coin. Aujourd'hui, pour moi, la révolution ne pourra passer que par
les solidarités transnationales. Les acteurixes de ce changement, ce seront
les femmes\* et la jeune génération. La lutte des femmes\* kurdes a
commencé il y a une centaine d'années déjà, peut-être même plus, mais le
mouvement organisé tel que nous le connaissons n'existe que depuis les
années 80. La lutte doit s'organiser sur le plan international, mais on
doit accepter de s'inspirer mutuellement. Il n'y a pas de bonne façon
d'y arriver, il faut se nourrir et s'inspirer de toutes les révolutions.

[^49]: *Cette colère immense, transgénérationnelle, internationale*\ \[n^o^\ 30\] évoque aussi le sentiment d'appartenance à quelque chose qui transcende le quotidien.

[^50]: Pour faire connaissance avec un autre groupe de travail de la Grève Féministe, lire *En el feminismo, lo personal es político*\ \[n^o^\ 49\] ou *Pas de féminisme sans les putes !*\ \[n^o^\ 37\].

[^51]: Lire *Faudrait pas que notre révolution ait l'air trop révolutionnaire*\ \[n^o^\ 44\] pour une autocritique du mouvement.

