---
author: "Anonyme"
title: "Surveiller la surveillance"
subtitle: "Quelques éléments pratiques pour la mise en place d'un collectif de copwatch"
info: "Texte rédigé pour le recueil"
date: "avril 2020"
categories: ["DIY", "prison, justice, répression", "autodéfense", "police"]
tags: ["index"]
lang: ["fr"]
num: "55"
quote: "regarder la police"
---

*Ce texte a été élaboré plus d'un mois avant la diffusion des images du
meurtre de George Floyd et de leurs répercussions politiques mondiales
et près de sept mois avant le projet de loi "Sécurité globale" en
France qui cherche à rendre illégale la diffusion d'images d'un policier
identifiable.*

## À quoi sert un groupe de copwatch organisé ?

Le copwatching° (terme qui signifie simplement "regarder la police")
apparaît aux USA dans un contexte d'autodéfense populaire face à la
violence raciste et à l'impunité des forces de l'ordre, notamment dans
les quartiers les plus précaires. De ce qu'on sait, les premièrexs à
s'organiser pour filmer la police sont des brigades plus ou moins
affiliées aux Black Panthers, qui cherchaient à documenter cette
violence avec les caméras analogiques de l'époque. Le copwatching est
apparu de manière organique, dans une logique\index[tags]{logique} de résistance qui voulait
documenter le vécu.

Ensuite, ça a évolué, ça a donné des idées, ça s'est organisé. Dans
certains pays, pas seulement en Occident, des collectifs commencent à
mettre en place des rondes tous les soirs dans leur ville, dans
l'intention de surveiller l'action de la police, prêts à filmer en cas
de violence\index[tags]{violence}. Il y a aussi des réseaux internationaux, des sites
internet, des zones de mise en commun des images, de partage de
tactiques et d'expériences, qui aident à organiser des groupes de
copwatch décentralisés. Sur ces réseaux, on peut aussi apprendre à
monter une défense juridique face à la police, à constituer un dossier
de témoignages, etc. Ensuite, au début du XXI^e^\ siècle, s'inspirant de
plusieurs brigades populaires, des groupes ont commencé à se former dans
l'intention de filmer spécifiquement la répression militante :
documenter non pas la violence\index[tags]{violence} quotidienne, mais aussi la violence
politique, la répression des manifestations par exemple. C'est de ce
copwatch d'antirépression qu'on parlera ici[^162].

Au fond, l'idée est simple : personne ne surveille ceux qui nous
surveillent, alors il faut des caméras populaires. On ne peut avoir
confiance ni en l'État ni en sa justice\index[tags]{justice} pour contrôler leur principal
organe de contrôle : la police. L'impunité de la police française n'est
plus à prouver (il suffit de regarder les chiffres des condamnations de
l'IGPN --- Inspection Générale de la Police Nationale --- ou de regarder
l'ensemble des dénonciations de violences policières ces dernières
années). En Suisse, ce n'est pas mieux. Ces vingt dernières années,
l'ONU a condamné à deux reprises la Confédération au motif qu'elle ne
dispose d'aucun organe de contrôle indépendant de sa propre police. Pour
le dire vite, si tu veux porter plainte pour violence\index[tags]{violence} policière, tu dois
aller le faire... à la police. Sur le terrain, l'impunité est aussi la
loi d'une police suisse plusieurs fois meurtrière, violente au
quotidien et outrepassant souvent ses droits[^163]. Le copwatching
peut aussi être défini comme une pratique militante offensive qui entend
documenter et démontrer la nécessité de repenser, désarmer et/ou
abolir carrément la police dans sa forme actuelle.

S'organiser pour filmer les violences policières change beaucoup de
choses. Au fond, l'idée n'a rien de compliqué, mais avoir un groupe de
personnes présentes dans les moments de lutte physique donne de vrais
résultats : des images plus complètes, plus compréhensibles, qui peuvent
alimenter le débat public et/ou être utilisées plus efficacement dans la
défense juridique des militanxtes. Très souvent, en Suisse, ce sont les
images des groupes de copwatch qui sont relayées dans la presse ou
amenées devant les tribunaux.

Bien sûr, en manif, tout le monde filme, mais c'est souvent trop tard
(on a le réflexe de sortir son téléphone\index[tags]{téléphone} quand on voit une violence
mais, le temps de sortir l'appareil photo, pouf, la situation de
violence est terminée et on n'a pas d'images), les prises sont souvent
floues, incompréhensibles. Mais en mobilisant sur le terrain un petit
groupe de personnes attentives, concentrées et qui savent pourquoi elles
sont là, d'un coup, pouf, les images documentant la violence\index[tags]{violence} sont bien
plus efficaces, parce qu'on *anticipe*, on filme avant que la violence
ne se déclenche.

C'est pas très compliqué d'organiser un groupe de copwatch dans ta
ville, pour soutenir et défendre celleux qui luttent, faut juste
s'organiser et discuter un peu.

## Filmer pour qui ?

Il est important qu'un groupe se mette collectivement d'accord sur une
éthique du copwatching : pour qui filme-t-on ? Si l'on peut se
coordonner avec des groupes d'antirépression ou les associations qui
organisent des événements, il est essentiel que les militanxtes filméexs
soient prioritaires dans la gestion de ces images : dans l'idéal, iels
devraient toujours avoir leur mot à dire dans leur diffusion. Les
groupes de *copwatch* décident le plus souvent de ne publier aucune
image en leur nom propre, pour ne pas se visibiliser, mais surtout pour
ne pas interférer avec le droit à l'image de chacunex.

## Une marche à suivre

Cette démarche est une sorte de prototype, une liste de choses qui
peuvent être utiles. Évidemment, on ne peut pas tout faire pour chaque
événement. C'est aussi une marche à suivre à compléter, à remettre en
question, à modifier...

### Avant le jour de l'événement

Il peut être judicieux de se promener là où l'action aura lieu et de
faire un plan des zones d'invisibilité possibles. La police sait
qu'elle est filmée et souvent, elle va essayer d'organiser immédiatement
l'espace pour invisibiliser sa violence\index[tags]{violence} : en faisant des lignes de
fourgons, en utilisant un groupe d'agents pour en cacher d'autres, en
emmenant des militanxtes dans des ruelles préalablement bloquées, etc.
Ces zones permettent à la police d'embarquer, fouiller et de déraper en
toute tranquillité. Il s'agit souvent de lieux que l'on peut facilement
nasser°.

Le groupe de copwatch doit étudier l'espace : y a-t-il une zone en
surplomb où l'on pourrait se poster préventivement (une terrasse de
café) ? Y a-t-il un endroit relativement sûr duquel on pourrait voir
sans être vuex (filmer à travers une vitrine de magasin) ?

Si on est plusieurs, il faut aussi réfléchir à la répartition des
personnes qui filment dans l'espace, même si souvent, au moment voulu,
c'est plus compliqué que ce qu'on imaginait. Se répartir dans l'espace
évite que touxtes celleux qui filment soient nasséexs au même endroit et
ne puissent pas filmer d'autres zones critiques d'un événement par
exemple. Évidemment, le feu\index[tags]{feu} de l'action redessine l'espace et il faut
surtout observer, improviser et rester réactifvexs, mais l'anticipation
et la préparation donnent souvent de très bons résultats.

### Les rondes, le jour de l'événement

Dans les heures qui précèdent l'événement, il peut être intéressant
d'effectuer des rondes pour évaluer la mobilisation policière déjà mise
en place. Certains signes permettent d'anticiper la gestion de l'espace
comme les barrières de déviation entreposées au coin de certaines rues,
les itinéraires précis des voitures banalisées, les lieux où sont
discrètement garés les fourgons, etc. Attention à rester discrèxtes
durant ces rondes. S'il y a des flics en uniforme ou en civil, on peut
aussi essayer d'écouter discrètement leurs conversations, qui
contiennent parfois des informations précieuses sur la planification de
l'événement. À chaque groupe de trouver ensuite le meilleur moyen pour
communiquer aux militanxtes et/ou aux organisations, ces informations en
temps réel pour essayer de se protéger au maximum. Pour les
communications numériques, toujours privilégier les applications
chiffrées de bout en bout (Signal, Telegram et Riot sont parmi les plus
recommandées, Signal efface les messages après un temps imparti, ce qui
peut être bien pratique[^164]).

### Organisation des copwatcheureuxses

Il y a plusieurs tactiques, plusieurs choix possibles...

Certains groupes de copwatcheureuxses décident de se rendre visibles en
portant un brassard ou d'autres signes distinctifs, comme lorsqu'il y a
des juristes et des avocaxtes sur place ou comme la presse. Dans ce cas,
on peut amener des stabilisateurs de téléphone\index[tags]{téléphone}, des selfie sticks pour
filmer depuis le haut, des caméras à la place des smartphones, etc. On
se rend visible et on se comporte comme des journalistes. Ce choix
implique un comportement particulièrement prudent, pour éviter de
devenir une cible facile.

D'autres groupes décident de copwatcher discrètement, de se fondre
dans la masse pour éviter de montrer qu'iels sont organiséexs. À
l'intérieur d'un bloc radical, rester masquéex et ne pas afficher trop
ouvertement que l'on filme : on ne filme un bloc que si l'on est sûr
de la sécurité des images[^165].

Il est rarement utile que deux copwatcheureuxses soient au même endroit.
Les angles de vue peuvent faire une vraie différence lors d'un procès\index[tags]{procès} :
tout est affaire de lisibilité de l'image. Pour autant, les règles de
sécurité s'appliquent comme pour n'importe quelle manifestation : on
essaie de ne pas laisser unex copwatcheureuxse "hors de vue", il faut
parfois se filmer ou se protéger mutuellement.

### Matériel

#### Quelques conseils d'équipement

- Le smartphone est un bon compromis entre efficacité et discrétion.
- Si on utilise un smartphone, ce qu'il y a de mieux c'est un mot de
passe alphanumérique assez long, par exemple :
NousSommesPartout\*1312\*.
- Vérifier qu'on a assez d'espace de stockage\index[tags]{stockage}.
- Effacer tout contenu compromettant (toutes les applications non
nécessaires, les groupes de discussion non nécessaires, les images, le
répertoire, etc.).
- Il est impossible\index[tags]{impossible} de passer une journée à filmer sans une batterie
externe contenant plusieurs recharges.
- Les ordinaires de toute manifestation (sérum physiologique,
nourriture, premiers soins, masque plastique, etc.).

#### Quelques conseils d'habillement

**Visibilité** : se signaler comme copwatcheureuxse avec un brassard, un
gilet, un signe distinctif et rester très prudenxte. Avec cette
tactique, on devient une cible privilégiée qui risque de se faire
interpeller de manière prioritaire. L'interrogatoire peut être salé.

**Immersion** : s'habiller comme unex manifestanxte parmi d'autres, ne
pas être trop extravaganxte dans son apparence pour ne pas être
facilement identifiable.

**Invisibilité** : s'habiller avec des codes sociaux
dominants/normatifs pour avoir l'air d'unex bourgeoisex curieuxse qui
aurait atterri là par hasard.

#### En cas de professionnalisation

- Gérer les images en live sur un cloud autonome et chiffré (toutes les
images y sont directement envoyées et supprimées du téléphone\index[tags]{téléphone}).
- Avoir une personne en retrait avec un ordinateur et un adaptateur
microSD pour faire arriver les images.
- Filmer avec un drone\index[tags]{drone} (attention c'est généralement illégal).
- Se procurer une carte de presse et une grosse caméra\index[tags]{caméra}, s'il y a dans
ton pays des journalistes camarades.
- Piloter un hélicoptère avec une caméra\index[tags]{caméra} pour contrer les hélicoptères
de la police, sait-on jamais.

### La prise d'images

C'est le moment où c'est tout bête, mais où ça fait une vraie
différence (de nombreux cas en attestent, en Suisse comme ailleurs).

Quand on filme une scène de violence\index[tags]{violence} : il faut surveiller constamment le
cadre et la luminosité de l'image, essayer de ne pas trembler, de ne
pas se déconcentrer du cadrage pendant les situations critiques, obtenir
la prise de son la plus nette possible, du coup ne pas chanter toi-même
de slogans (oui, même si c'est frustrant), parler le moins possible, ne
pas se préoccuper du droit à l'image lorsqu'on filme (il faut filmer
clairement les visages et les matricules pour que les images soient
utiles juridiquement --- la question de l'identité se pose uniquement lors
de la diffusion des images).

On essaie d'avoir des plans larges pour comprendre tout ce qu'il se
passe et pour filmer un maximum de mouvements possibles. Essayer
d'anticiper et filmer la situation générale quand on sent que "ça
commence à chauffer" pour fabriquer le document visuel le plus
compréhensible possible.

### S'assurer de pouvoir contacter les personnes concernées et rassembler les vidéos prises par d'autres personnes

À la suite d'une interpellation ou d'une arrestation\index[tags]{arrestation} filmée, il faut
essayer d'obtenir discrètement une adresse email\index[tags]{mail/email} ou un numéro de
téléphone à contacter par la suite. Si on n'a pas l'occasion de prendre
le contact de la personne concernée, on peut demander à ses amiexs.

La plupart du temps, d'autres personnes présentes ont filmé la scène. Le
problème, c'est que ces images sont rarement rassemblées et données aux
personnes concernées. En outre, il arrive qu'elles circulent sur les
réseaux sociaux sans les précautions adéquates (floutage des militanxtes
par exemple). Un collectif organisé de copwatch peut aussi servir à ça :
dissuader les personnes d'incriminer les autres dans leurs stories
insta et centraliser les images des personnes présentes sur place.
Attention : on demande si l'on peut obtenir les images, sans dire que
l'on appartient à quelque collectif que ce soit (en manif comme
ailleurs, on ne sait jamais à qui on est en train de parler).

Quand le calme revient, on peut aussi demander aux témoins oculaires de
raconter face caméra\index[tags]{caméra} ou à visage\index[tags]{visage} couvert ce qu'iels ont vu. On ne pose
pas trop de questions, on paraphrase les affirmations et on demande aux
témoins de valider. Ce type de document pris "sur le vif" a déjà changé
le cours de certains procès\index[tags]{procès}.

## Après l'action ou l'événement

### Stockage

Il est important de stocker les images et de ne pas les effacer après
les avoir envoyées aux personnes concernées. Les images peuvent servir
des mois et des mois après l'événement et d'autres militanxtes que
celleux auxquellexs on pense sur le moment pourraient en avoir besoin
plus tard. Les militanxtes font souvent l'objet d'enquêtes approfondies
qui durent longtemps et qui regroupent plusieurs chefs d'accusation
relatifs à plusieurs événements et actions. Il vaut mieux tout archiver,
même lorsque les images n'ont pas l'air violentes, on ne sait jamais ce
qui pourra servir face aux juges.

Il ne faut jamais stocker les images en ligne, il faut utiliser des clés
USB et des disques durs externes. Pour s'assurer de ne rien perdre en
cours de route, on peut les stocker en double, sur différents supports.

### Montage

Ne pas sous-estimer le temps de travail.

Envoyer un seul fichier monté compilant l'essentiel d'un événement est
souvent d'une grande aide pour une organisation qui cherche à défendre
ses manifestanxtes ou pour les avocaxtes en charge de procès
collectifs.

Il peut s'avérer utile aussi d'inclure des panneaux de textes
explicatifs dans le montage ou de mettre en évidence des agressions qui
ne sont pas au centre de l'image, mais en arrière-plan, avec des
cercles de couleur par exemple.

Flouter l'ensemble des personnes, sur des montages qui peuvent faire
plus d'une heure, est un travail colossal et rarement nécessaire, mieux
vaut en laisser la responsabilité aux organisations tout en leur
précisant bien l'importance du floutage au moment de l'envoi des
images. Il peut toutefois être plus sûr de s'assurer que les
copwatcheureuxses sont floutéexs.

Pour l'envoi des images, se référer aussi aux différents guides
d'autodéfense numérique qui expliquent comment envoyer des fichiers de
manière sécurisée.

Tout groupe de copwatching doit aussi s'autoéduquer le plus précisément
possible sur les lois en vigueur dans son pays relatives au droit à
l'image : en général dans l'espace public et plus spécifiquement
relatives à la police (droits qui sont souvent régis par des textes de
lois complémentaires voire contradictoires).

[^162]: Pour un échange entre plusieurs membres d'un collectif de copwatch, lire *Spectacle nulle part.* Care *partout*\ \[n^o^\ 23\].

[^163]: Pour écouter une personne concernée par cette violence policière, lire *They don't see us*\ \[n^o^\ 4\].

[^164]: Pour tenter de communiquer numériquement avec un semblant de sécurité, lire *Camouflage dans l'infosphère*\ \[n^o^\ 40\].

[^165]: Pour comprendre les enjeux liés aux images dans un bloc ou une manif en général, lire : *Survivre dans un black bloc*\ \[n^o^\ 15\].
