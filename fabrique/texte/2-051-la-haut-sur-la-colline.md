---
author: "Anonyme"
title: "Là-haut sur la colline"
subtitle: "Récit d'une zadiste de la Colline, 1312 Éclépens"
info: "Transcription d'un entretien"
date: "décembre 2020"
categories: ["écologie", "autogestion, expérimentations collectives", "squat, occupations, logement", "sabotage, action directe"]
tags: ["index"]
lang: ["fr"]
num: "51"
quote: "contre toutes les formes d'oppression, de domination et d'exploitation"
---

J'ai commencé à militer au sein des récents mouvements qui luttent pour
une justice\index[tags]{justice} climatique. À travers ça, j'ai découvert d'autres luttes,
des luttes féministes, queer, antiracistes et toutes celles qui me
semblent faire partie d'un même combat.

Après avoir passé un certain temps dans les luttes écolos de Suisse
romande, j'ai senti qu'on arrivait gentiment à un point mort, une
sensation renforcée par le confinement, qui a fini par confiner aussi
nos révoltes. J'avais de plus en plus envie d'aller vers une écologie
moins consensuelle, qui revendique et assume ses conflits, qui tisse des
liens avec les autres luttes. Une écologie qui affronte les vraies
questions de nos rapports aux autres, aux milieux naturels et aux
communs, une écologie qui se bat contre toutes les formes d'oppression,
de domination et d'exploitation. Je n'étais pas du tout la seule à être
traversée par ces envies. Beaucoup de jeunes se sont politiséexs avec
ces nouveaux mouvements climatiques ces deux dernières années et au fil
du temps on s'est radicaliséexs et on a embrassé ou approfondi des
sensibilités politiques plus anarchistes. Une vision radicale de
l'écologie a émergé, qui ne cherche pas à faire de compromis
institutionnel et qui est fondamentalement anticapitaliste, parce
qu'elle voit que les problèmes sont enracinés dans ce système. Et
maintenant, une approche intersectionnelle commence à prendre forme. On
prend conscience de l'intrication de toutes ces oppressions qui sont
forgées par ce modèle capitaliste unique.

Ça, c'était un peu le fond de ce qui se passait entre nous.
Parallèlement, on a découvert la situation de la colline du Mormont,
menacée de destruction\index[tags]{détruire/destruction} complète par la cimenterie lafarge-holcim. Avec
un groupe de personnes, on a eu l'idée de cette Zone À Défendre
(ZAD)[^149] pour lutter contre l'extension de la carrière de
la cimenterie. Cette ZAD° naît d'une double envie ou d'une double
nécessité.

D'un côté, elle permet d'entrer dans une lutte très locale et hyper
concrète qui vise la préservation de l'endroit. C'est un aspect
important, parce que dans les mouvements climatiques, c'est difficile de
rendre les causes du dérèglement climatique visibles en un coup d'œil.
On comprend le problème, le réchauffement climatique et tout ça, mais il
reste souvent lointain et abstrait, comme s'il ne nous touchait pas
réellement. Avec cette ZAD, on lutte directement, *ici*, sur cette
colline qui va se faire exploser par lafarge holcim --- une de plus... et
cela permet de concrétiser l'écologie, de rendre visible la destruction\index[tags]{détruire/destruction}.
On lutte frontalement, dans le conflit, sans l'éviter.

De l'autre côté, cette ZAD nous permet aussi de dénoncer tout un
système, de lutter avec une perspective plus globale, contre une
industrie globalisée qui détruit la terre et la biodiversité, contre un
modèle capitaliste d'exploitation qui n'a pas de limites --- c'est ce
modèle que holcim symbolise. Voilà pourquoi la Colline est un symbole de
ce qu'il se passe ici et ailleurs.

Lafarge-holcim défonce des territoires entiers, pourrit la vie des
populations de plein de pays, sans qu'il n'y ait jamais aucune forme de
justice sociale ou climatique. C'est la version "XXI^e^\ siècle" du
colonialisme. Même si notre ZAD n'a pas d'impact direct sur ces
populations et ces territoires, pour nous, ce double enjeu que
représente la Colline, à la fois concret et symbolique, a vraiment du
sens. Il permet de faire front contre un système en révélant ses
oppressions et ses dominations systémiques°.

La ZAD a nécessité une longue préparation. Quand on a commencé, on
n'était vraiment pas beaucoup. On a eu des doutes au début, on ne
comprenait pas pourquoi cette stratégie de lutte qui nous paraissait si
sensée, ne rassemblait pas plus de monde[^151]. C'était dur de trouver
des personnes qui avaient envie de nous rejoindre. On ne pouvait pas en
parler publiquement, ça n'aidait pas. Mais, au
bout d'un certain temps, plein de personnes ont fini par nous
rejoindre : il y a eu une véritable dynamique collective, hyper positive
et c'était cool de constater que le projet répondait à une envie
militante partagée. Il fallait juste qu'on touche les bons cercles, on
n'avait simplement pas assez de contacts à l'extérieur de nos sphères
militantes.

En octobre, quand on a installé la ZAD, c'était une surprise générale.
En Suisse, l'État ne connaît pas vraiment ce mode d'action. C'était
marrant, parce personne ne comprenait ce qu'on allait faire, ce qu'on
voulait et combien de temps on comptait rester. Toute la machine
juridique a mis du temps à se mettre en route. Holcim n'a porté plainte
qu'au bout de deux semaines. Notre action sort des carcans d'Extinction
Rebellion[^152] ou de la Grève du Climat, donc ça prend de court. Ça a
provoqué plein de fantasmes et de mythes. Localement, il y a eu des
réactions très différentes : de nombreux soutiens, mais aussi des
réactions ridicules genre : "il y a beaucoup de déchets" ou "c'est
touxtes des étrangèrexs, on a vu des plaques belges". Souvent les gens
n'essaient même pas de comprendre de quoi on parle.

La ZAD c'est un endroit de lutte créatif, on teste des micropolitiques,
des modes d'organisation communautaires et alternatifs. On veut être un
lieu d'accueil, de rencontres et de convergence des luttes. La société
fait tout pour nous isoler, nous confiner dans nos petits espaces, parce
que c'est dangereux, une communauté politique qui s'autonomise, qui
pense et qui critique. La ZAD permet de faire se rencontrer des milieux
militants différents qui ne se parlent pas forcément. Avant, j'étais
comme dans une bulle du milieu écolo, ça me manquait de
rencontrer des personnes d'autres milieux radicaux. Ici, les visions,
les générations, les savoir-faire et les expériences se brassent, ça
donne une communauté politique hyper créative, qui se remet toujours en
question et qui ne reste jamais statique. Décloisonner les différents
espaces militants libertaires, ça donne un vrai souffle\index[tags]{souffle} à nos milieux,
c'est un enjeu primordial. Ça nous rend plus fortexs d'être ensemble, de
lutter ensemble. Je pense qu'au fond, on a touxtes envie de créer cette
communauté politique et que c'est possible, mais qu'on a besoin
d'espaces de rencontres pour le faire. Il n'y a pas besoin d'être ultra
potes, on a juste besoin de s'organiser. Mais bon, on est
conscienxtes qu'il reste encore énormément de barrières à faire tomber.
On n'est pas assez inclusivexs, on est une majorité de personnes
blanches et de personnes cishétéros. On se questionne beaucoup sur
cette réalité, sur le fait que les espaces sont toujours tenus par les
mêmes groupes sociaux et on tente de se remettre en question, de parler
avec les personnes concernées.

Pour le moment, il n'y a pas de plans concrets d'évacuation et on espère
que ça va durer ! De toute façon, même si l'État décide un jour de nous
évacuer, on continuera à lutter et on essaiera de résister. Avoir
organisé cette ZAD, c'est hyper fort, on se sent vivanxtes, ça répond à
ce besoin d'expérimenter d'autres formes d'écologie par l'action
directe°. Ce besoin ne va pas s'éteindre, car on n'a pas d'autre choix
dans la lutte écolo. Et au final, même si cette ZAD ne survit pas et
même si la convergence des luttes est loin d'être parfaite, ça me donne
de la force\index[tags]{force} et j'ai l'espoir que cette convergence continuera à se
construire à l'extérieur.

Personnellement, ce projet m'a permis d'affirmer un besoin de luttes
radicales et d'action directe. Dans mon parcours militant, je suis
passée par les partis politiques et j'ai vu de l'intérieur comment ce
milieu fonctionne. Sans critiquer le travail des individus qui font
souvent du bon boulot, avec leur cœur, les luttes institutionnelles et
réformistes font dominer une vision de l'écologie : celle du compromis.
Elles font croire que les luttes climatiques peuvent s'insérer dans le
système actuel. L'effet collatéral, c'est qu'elles étouffent les autres
voix écolos et qu'elles refusent de montrer les vraies forces qui
empêchent tout changement.

Ça ne veut pas dire que toutes ces luttes n'ont servi à rien, elles ont
fait avancer l'opinion publique par exemple. Je bosse encore dans une
ONG, je continue à voter parce que ça permet de sauver quelques meubles
pour certaines personnes, mais là, on a vraiment besoin de passer à un
autre stade, celui où on se bat frontalement contre un système qui ne
cesse de pacifier nos luttes et de détruire\index[tags]{détruire/destruction} toute possibilité de
justice.

La rupture\index[tags]{rupture} est non négociable.

[^149]: *Vous détruisez une Spyre, on en reconstruira plein*\ \[n^o^\ 18\] raconte une expérience d'occupation à Pully, dans le canton de Vaud ; *ZAB 2028*\ \[n^o^\ 43\] est une fiction décrivant une ZAD du futur.

[^151]: *Arrêtons de "défendre"*\ \[n^o^\ 28\] élabore une critique de la tactique défensive des ZAD.

[^152]: *Faudrait pas que notre révolution ait l'air trop révolutionnaire*\ \[n^o^\ 44\] est une discussion autocritique entre plusieurs membres d'Extinction Rébellion Lausanne.
