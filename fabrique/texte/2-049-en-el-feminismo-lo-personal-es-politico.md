---
author: "Antonia Undurraga"
title: "En el feminismo, lo personal es político"
subtitle: "Une trajectoire féministe internationale et révolutionnaire"
info: "Transcription d'un entretien"
date: "août 2020"
categories: ["féminismes, questions de genre", "manifestation, émeute", "relation à la militance"]
tags: ["index"]
lang: ["fr"]
num: "49"
quote: "la coupable ce n'est pas moi, ni mes fringues, ni l'endroit"
---

Je commence par me présenter. J'ai 41 ans et je suis militante féministe
depuis un an et demi. On peut dire que je suis devenue militante
féministe affichée à mes 40 ans, date qui correspond à la grande Grève
Féministe du 14 juin 2019. Je suis moitié chilienne, moitié suissesse
(tessinoise) et j'ai vécu mon enfance et ma jeunesse un peu partout dans
le monde. Ça fait treize ans que je me suis fixée à Lausanne, je me
considère comme Lausannoise d'adoption et, enfants obligent, je pense
que c'est parti pour une autre décennie. En termes professionnels, je
suis sociologue de formation et mon parcours s'est concentré sur
l'enfance, la famille et l'accueil de l'enfance (garderies, crèches,
parascolaire). D'ailleurs, aujourd'hui je travaille dans une institution
pour l'enfance en tant qu'éducatrice.

Ma famille est composée de moi, mon fils Noah qui a neuf ans et Ela qui
en a cinq. J'ai eu Noah dans le cadre d'un mariage tout à fait
traditionnel et je me suis séparée de son père lorsqu'il avait un an et
demi. Le père en question n'a ni accepté une garde partagée ni un lien
régulier avec Noah, alors je suis devenue mère\index[tags]{mère} solo.

Pour Ela, l'histoire est différente. Je voulais un deuxième enfant et
j'avais commencé à creuser différentes options. Entre-temps, je suis
tombée enceinte d'une relation purement sexuelle que j'entretenais avec
son géniteur. Il n'a pas voulu entendre parler de cet enfant en devenir
et m'a d'ailleurs demandé d'avorter. De toute évidence, il ne connaît
pas le slogan "mon corps\index[tags]{corps}, mon choix". J'ai subi une grande pression
sociale pour avorter, "parce que c'était salaud que je fasse ça à un mec
qui ne voulait pas d'enfant", "parce que c'était égoïste d'amener un
enfant au monde en sachant qu'il n'aurait pas de père", etc. J'ai décidé
de garder ma fille et aujourd'hui je pense que c'est l'acte de militance
le plus puissant que j'ai fait, même si à l'époque je ne le concevais
pas ainsi.

Bref, je suis devenue militante à l'âge de 40 ans. Je ne suis pas une
militante historique, une personne qui a consacré toute sa vie à des
engagements militants. Je pense que j'ai compris, de par mon parcours
personnel et le contexte global historique dans lequel nous vivons
aujourd'hui, que j'étais effectivement une féministe et qu'il fallait
que je m'engage. Je ressentais comme un fort besoin existentiel de
m'engager.

Jeune, j'ai traversé toute une série de souffrances
qu'aujourd'hui seulement je peux qualifier de souffrances liées à mon
"être femme" : des souffrances de genre infligées par la violence\index[tags]{violence} d'une
société patriarcale. Je pourrais te parler des poils, de combien on
s'est moqué de moi, car je suis une femme noiraude assez poilue[^146].
Des commentaires de certains partenaires sexuels, du style "tu penses
pas que ça serait plus hygiénique si tu t'épilais la chatte ?" ou de la
difficulté que j'ai eue à me mettre en maillot de bain, car je n'étais
pas parfaitement épilée (et bien sûr, aussi parce que je n'avais pas un
corps qui ressemblait à la beauté féminine véhiculée par les médias et
la société). Si on y pense, le temps et l'énergie que j'ai consacrés à
un thème aussi banal que les poils, c'est juste aberrant !

Je pourrais aussi te parler de sexualité. J'étais une fille très libre,
si j'avais envie de baiser, je baisais. À l'époque, j'étais au Chili,
dans une société qui a une mentalité rétrograde en ce qui concerne la
place de la femme. On en était encore à imposer la
virginité avant le mariage. Moi je ne comprenais pas, j'sais pas...
j'avais des copines qui me conseillaient de jouer la dure par exemple,
de ne pas céder aux premières avances, etc. Je comprenais pas pourquoi
je devais me priver de l'envie que j'avais pour le jeu de la séduction.
Une fois, mon père m'a même accusée d'être une libertine ! Je me demande
s'il aurait fait le même commentaire à un fils. Étant une fille libre et
sans peur\index[tags]{peur}, j'ai fait plein de choses dans ma jeunesse qui sont
considérées comme "dangereuses" pour une jeune femme. Me promener tard
le soir dans certaines rues, faire du stop, me bourrer la gueule\index[tags]{gueule} dans
une fête\index[tags]{fête} où je ne connais personne, m'habiller de manière "trop" dénudée
dans certains contextes, aller choper un peu de weed dans la rue\index[tags]{rue},
etc.[^147] Et nous, les femmes\*°, on a tellement intériorisé ça, que
je me grondais toute seule ! Je me disais que c'était irresponsable de
faire ceci ou cela et que c'était un miracle que rien de grave ne me
soit jamais arrivé. J'ai longtemps dit, en rigolant, que j'avais un ange
gardien qui me protégeait. Je n'arrivais pas à m'expliquer ma
"chance".

D'un côté, il y a tout ce vécu individuel, ce parcours personnel, et de
l'autre, il y a le 14 juin 2019. Je sortais d'une crise dépressive suite
à une rupture\index[tags]{rupture} amoureuse, à un déménagement et, qui sait, probablement
que fêter mes 40 ans au milieu de tout ça n'a pas aidé. Le jour de la
grève (un vendredi), c'était mon dernier jour d'arrêt maladie et j'ai
participé aux 24 heures de la grève. Et là, wow, quelle claque ! Cette
journée a marqué ma vie, comme celle de beaucoup d'autres femmes\* je
crois. Je ne me suis plus sentie seule. Pendant toute la nuit\index[tags]{nuit/obscurité} du 13 et
la journée du 14, j'ai vu et discuté avec des femmes\* qui partagent mes
souffrances, mes problèmes et qui se posent les mêmes questions que moi.
Mes souffrances passées et présentes ont pris une tout autre dimension :
d'individuelles, elles sont devenues collectives. Récemment je suis
tombée sur une phrase qui est devenue mon mantra, car elle dit tout :
"en el feminismo, lo personal es político".

Ce jour-là, mon engagement dans le mouvement féministe est devenu une
évidence. Je me suis toujours définie comme féministe, je me rends
compte aujourd'hui que c'est un peu parce que je suis une nana "de
gauche", une nana sociologue, que j'ai toujours été le type de nana qui
revendique son appétit sexuel, etc. Je pense que ça suffisait pour me
déclarer un peu féministe, un peu grande gueule\index[tags]{gueule} si tu veux. Aujourd'hui,
il y a un mouvement qui m'a permis de mettre des mots et de dire "oui,
c'est évident que je suis féministe", mais avec plus de conscience,
plus d'appuis aussi.

Donc me voilà toute fraîchement engagée dans le collectif de la Grève
Féministe vaudois, qu'au Chili éclate un énorme conflit social et que le
collectif LasTesis crée la performance "Un violador en tu camino". En
l'espace de quelques jours, cette performance est reprise dans le monde
entier, donc pourquoi pas la reprendre ici, à Lausanne ? Avec un petit
groupe de Chiliennes, on a réussi, en dix jours, à rassembler environ
200 personnes à la Place Saint-Laurent. Des jeunexs et des moins
jeunexs, des mères avec leurs filles, des Chiliennexs et des
Suissessexs, des militanxtes et des non-militanxtes. Ensuite, on a
refait la performance plusieurs fois à l'occasion du 8 mars, et à chaque
fois le résultat a été incroyable ! Cette chorégraphie et ces paroles
résonnent tellement fort chez nous touxtes ! "La coupable ce n'est pas
moi, ni mes fringues, ni l'endroit", cette phrase dit l'essentiel !

Chez moi, ces phrases ont réveillé les souvenirs de toutes ces années
passées à me dire que je devais faire gaffe si je ne voulais pas que
quelque chose de grave m'arrive. Si on regarde le nombre de fois, le
nombre de pays et le nombre de langues dans laquelle cette performance a
été reproduite, on peut bien imaginer que c'est parce qu'elle parle aux
femmes\* du monde entier.

Je sais pas si t'as vu Las Tesis senior au Chili. Elles ont fait un
appel aux femmes\* de plus de 40 ans. Elles étaient 1 000 dehors, au
stade national --- un lieu emblématique lié aux tortures de Pinochet --- 1
000 nanas à faire la choré ! Et là, tu vois des femmes\* qui ne sont pas
militanxtes. Et c'est justement ça, cette choré, elle sort des femmes\*
dans la rue\index[tags]{rue} qui ne sont pas des militanxtes, qui ne sont pas
politiséexs, qui sont touchéexs parce que ça parle de leur quotidien.

Assez vite dans mon engagement auprès du collectif, j'ai réalisé que les
mères et les thématiques autour de la maternité étaient très peu
présentes dans le mouvement. Et c'est MON thème ! Comme je disais avant,
il m'a fallu du temps, mais j'ai désormais compris que ma famille est un
acte militant. Parce qu'on peut être une famille sans appliquer la
formule "papa, maman et deux gosses (si possible une fille et un
garçon)". Et surtout, on peut être une famille heureuse.

La beauté de l'engagement dans un mouvement comme le nôtre, c'est les
rencontres. J'ai rencontré tellement de personnes incroyables ces deux
dernières années ! Au fil des rencontres et des échanges, l'idée de
créer un groupe de travail sur la maternité est née : le GT Maternités
Féministes. Depuis quelques mois, nous sommes une quinzaine de femmes\*
à travailler sur un manifeste. Parmi nous,
il y a aussi des non-mères. Chaque dimanche matin on se retrouve
virtuellement (Covid-19 oblige), mais en tant que mères, on se rend
compte de l'utilité des outils numériques. Sans ça, on n'aurait jamais
pu faire autant de réunions pour discuter de ces thématiques. Chacunex a
son parcours de vie, son parcours de couple et/ou de non-couple, son
parcours professionnel, son parcours maternel, etc. Autour de la
maternité, on se rencontre, on partage nos difficultés, nos
frustrations, nos idées pour un monde meilleur.

On partage touxtes, indépendamment de notre état civil ou de notre
situation familiale, une grande colère\index[tags]{colère} envers une société qui nous a
surresponsabiliséexs envers les enfants, une société où les mères sont
les seulexs référenxtes et donc les seulexs responsablexs et une société
qui nous a laisséexs sans aucun soutien. Nous rêvons d'une société qui
valorise le travail éducatif et le travail de soin qu'apportent les
mères. Nous luttons contre une société qui nous impose le mythe de la
mère parfaite, en nous mettant d'emblée en échec dans tous les rôles que
nous assumons. Nous militons pour une société qui fout la paix aux
femmes\*, qu'elles veulent ou non devenir mères ! Nous nous battons pour
notre santé gynécologique et la liberté\index[tags]{liberté} dans nos choix ! Nous crions
pour que la collectivité tout entière assume ses responsabilités
vis-à-vis des enfants !

## Texte de la version lausannoise bilingue de la flashmob "El violador eres tu" écrite en 2019 pour la Grève Féministe

\setlength{\parindent}{0em}

Premier son du sifflet, on met en place les rangées.

\vspace{1\baselineskip}

La musique techno commence. Les participanxtes frappent des pieds pour
marquer le rythme.

Iels pivotent sur leurs talons et se balancent d'un côté puis de l'autre

(4 fois, sans chanter).

\vspace{1\baselineskip}

Au deuxième coup de sifflet, iels chantent. Le même mouvement, sur deux
temps sans chanter, est répété entre chaque phrase.

\vspace{1\baselineskip}

El patriarcado es un juez/que nos juzga por nacer

Y nuestro castigo/es la violencia que no ves

El patriarcado es un juez/que nos juzga por nacer

Y nuestro castigo/es la violencia que ya ves.

\vspace{1\baselineskip}

Le patriarcat est un juge/qui nous juge dès la naissance

Et notre punition/c'est la violence\index[tags]{violence} que tu vois. (2x)

\vspace{1\baselineskip}

(Un *squat* les mains derrière la tête une fois qu'elles finissent de
chanter.)

\vspace{1\baselineskip}

Es femicidio

Impunidad para mi asesino

Es la desaparición

Es la violación.

\vspace{1\baselineskip}

(Les participanxtes dansent sur place en bougeant les bras.)

\vspace{1\baselineskip}

Y la culpa no era mía ni dónde estaba ni cómo vestía. (4x)

La coupable ce n'est pas moi, ni mes fringues, ni l'endroit. (4x)

\vspace{1\baselineskip}

(Index gauche pointé vers l'avant à "tú"/"toi", baissé en début de
phrase.)

\vspace{1\baselineskip}

El violador eres tú. (2x)

Le violeur c'est toi.

Le coupable c'est toi.

\vspace{1\baselineskip}

Son los pacos/c'est les flics ! (Poing en haut à gauche.)

Los jueces/c'est la justice\index[tags]{justice} ! (Poing en haut devant.)

El Estado/c'est l'État, la société, le patriarcat tout entier

\vspace{1\baselineskip}

(De leurs mains, elles dessinent un cercle au-dessus de la tête.)

\vspace{1\baselineskip}

C'est l'État, la société, le patriarcat tout entier ! (Bras en croix
au-dessus de la tête.)

\vspace{1\baselineskip}

(On lève le poing gauche en rythme.)

\vspace{1\baselineskip}

El estado opresor es un macho violador. (4x)

\vspace{1\baselineskip}

(Index gauche pointé vers l'avant à "tú"/"toi", baissé en début de
phrase.)

\vspace{1\baselineskip}

El violador eres tú. (2x)

Le violeur c'est toi.

Le coupable c'est toi.

Y la culpa no era mía ni dónde estaba ni cómo vestía. (4x)

La coupable ce n'est pas moi, ni mes fringues, ni l'endroit. (4x)

(Index gauche pointé vers l'avant à "tú"/"toi", baissé en début de
phrase.)

\vspace{1\baselineskip}

El violador eres tú. (2x)

Le violeur c'est toi.

Le coupable c'est toi.

\vspace{1\baselineskip}

(Les participantes dansent sur place en bougeant les bras.)

(Les mains en porte-voix\index[tags]{voix} de chaque côté de la bouche\index[tags]{bouche}.)

\vspace{1\baselineskip}

Patriarcat t'es foutu, les femmes\* sont dans la rue\index[tags]{rue} ! (4x)

\vspace{1\baselineskip}

El violador eres tú. (2x)

Le violeur c'est toi.

Le coupable c'est toi.

Duerme tranquila/niña inocente

Sin preocuparte del bandolero

Que por tus sueños/dulce sonriente

Vela tu amante carabinero.

\vspace{1\baselineskip}

(Index gauche pointé vers l'avant à "tú"/"toi", baissé en début de
phrase.)

\vspace{1\baselineskip}

El violador eres tú. (2x)

Le violeur c'est toi

Le coupable c'est toi.

Y la culpa no era mía, ni dónde estaba, ni cómo vestía. (4x)

La coupable ce n'est pas moi, ni mes fringues, ni l'endroit. (4x)

\vspace{1\baselineskip}

(Cris de joie.)

\setlength{\parindent}{1em}

[^146]: Nos corps, nos choix, il y a les poils et il y a les seins : *Cachez vos tétasses ou rembourrez-les et laissez-nous tranquilles*\ \[n^o^\ 24\].

[^147]: Pour réfléchir à des fêtes féministes et bienveillantes, lire *La fête est finie*\ \[n^o^\ 20\].
