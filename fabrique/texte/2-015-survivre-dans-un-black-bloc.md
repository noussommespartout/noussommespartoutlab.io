---
author: "Anonyme"
title: "Survivre dans un black bloc"
subtitle: "Théorie, arguments, lacrymos, répression"
info: "Texte rédigé pour le recueil"
date: "février 2021"
categories: ["DIY", "manifestation, émeute", "police", "violence, non-violence", "autodéfense", "sabotage, action directe"]
tags: ["index"]
lang: ["fr"]
num: "15"
quote: "voilà vingt ans qu'on annonce la mort du black bloc et vingt ans qu'il réapparaît"
---

\begin{addmargin}[0.15\textwidth]{0em}
\noindent \emph{{[}...{]} les Black Blocs sont les meilleurs philosophes
politiques du moment.}
\end{addmargin}

Nicolas Tavaglione, *Le Courrier*, Genève, 2003[^36]

\vspace{1\baselineskip}

Voilà vingt ans qu'on annonce la mort du black bloc° et vingt ans qu'il
réapparaît, partout autour du monde, y compris en Suisse. Il renaît
parce qu'il est à l'image de notre époque. Avec l'essor de la
surveillance de masse, la militarisation croissante des polices et la
coupure toujours plus visible entre le peuple et les élites qui
s'autolégitiment par les urnes, tout mouvement de transformation
sociale un peu significatif semble exiger l'anonymat, la confrontation
violente avec les autorités[^37] et une mise en scène de soi en pleine
position de pouvoir\index[tags]{pouvoir} sur la ville.

Avec la crise globale du capitalisme mondialisé et les prémices de la
crise écosociale qui s'annonce, les stratégies du bloc sont de plus en
plus adoptées, notamment dans des pays et des contextes culturels où
elles constituent une nouveauté. Ces stratégies se sont développées dans
un environnement résolument hostile. Souvent clandestines, elles sont
cantonnées à des contre-médias°, sans véritable organisation ni partage
de savoir ouvert, contre une structure de pouvoir\index[tags]{pouvoir} stable et sans
recevoir beaucoup de soutien des mouvements sociaux pacifistes comme du
reste de la population. Et c'est pourtant bien ce "reste de la
population" qui ne cesse de rejoindre les formes les plus radicales de
présence urbaine, les rangs du black bloc.

Ce texte mélange l'expérience suisse romande avec certains éléments de
*Défense du black bloc* (2010) par Harsha Walia, militante antiraciste,
et de *Les black blocs, la liberté\index[tags]{liberté} et l'égalité se manifestent* (2016)
par Francis Dupuis-Déri, théoricien et militant anarchiste.

## Survivre en théorie

À quoi sert un bloc radical ? Eh bah, à plusieurs trucs :

--- il constitue une tactique, qui peut être efficace ou non, mais dont
la légitimité n'a pas à être discutée *pour elle-même*.

--- dans son principe, il est au service de l'idée qu'on est un seul
groupe anonyme, et en pratique, il permet à touxtes d'avoir une
possibilité de manifester en échappant à la surveillance d'État,
d'éviter un fichage qui les poursuivra parfois toute leur vie.

--- très souvent, le bloc permet de désarrêter° des personnes aux mains
de la police en déconcentrant, en surprenant et en adoptant une attitude
offensive. Le bloc est l'une des meilleures formes que prend
l'expression "on ne laisse personne derrière".

--- il brise la ritualisation de la désobéissance et de la contestation,
qui tend à obéir à la gestion de l'espace autorisé, et à rester dans les
limites des conventions supposément démocratiques (manifestations,
sit-in°, pétitions, etc.)[^38].

## Survivre aux critiques

On reproche souvent au bloc radical d'ouvrir grand la porte aux
policiers-agitateurs, qui s'habillent en manifestants et perpétuent des
casses inutiles (en attaquant des cibles incohérentes, comme des petits
magasins et non des grandes banques) ou piègent d'autres manifestanxtes.
Le reproche ne tient pas, l'histoire des luttes sociales montre que ces
agitateurs ont toujours été là et que les policiers se déguisent très
souvent en journalistes ou en manifestanxtes à visage\index[tags]{visage} découvert.

On lui reproche beaucoup d'être inefficace, et c'est souvent vrai, mais
on s'intéresse trop peu à l'inefficacité d'autres tactiques, comme le
fait d'interpeller continuellement des politiciennexs avec des
pancartes. Pourtant, le bloc concentre l'essentiel des discours sur
"l'inefficacité", ce qui signifie que l'ordre du débat relève avant
tout d'une intériorisation des systèmes de valeurs dominants relatifs à
la représentation de la propriété privée et de la violence\index[tags]{violence}.

On reproche souvent au bloc sa violence\index[tags]{violence}. Mais l'on pourra toujours
répondre qu'il existe trois sortes de violence\index[tags]{violence}. D'abord la violence
première, celle du capital, de l'extorsion, de l'exploitation, de la
mise à mort, du racisme, du patriarcat. Ensuite, la violence\index[tags]{violence} défensive,
qui n'existe que parce qu'elle réagit légitimement à la première. Enfin,
la violence\index[tags]{violence} de la répression, qui se manifeste lorsque l'État veut
s'assurer que la violence\index[tags]{violence} première puisse continuer de s'exercer. Quand
on casse les vitrines de banques qui se font de l'argent grâce à des
dictatures, des réseaux néocoloniaux, parfois même des génocides, et
produisent à elles seules jusqu'à 20 fois plus d'émissions de CO~2~ que
la population suisse tout entière, n'est-ce pas la violence\index[tags]{violence} défensive du
vivant qui s'exprime ? Les cibles du bloc ne sont jamais choisies au
hasard : Organisation Mondiale du Commerce, Fonds Monétaire
International, G8, G20, WEFF, banques, assurances, multinationales. Il
ne faut jamais oublier à qui le bloc essaie de faire peur\index[tags]{peur}. Et il ne faut
pas oublier la puissance du dispositif policier. Quelques personnes avec
des cailloux et des écharpes contre des soldats surentraînés dont
l'équipement antiémeute coûte des dizaines de millions de francs. Qui
défend la police ?

Il ne faut pas non plus oublier que la violence\index[tags]{violence} contestataire devient
légitime et nécessaire aux yeux de l'essentiel de la population dès lors
qu'on l'historicise, dès lors qu'elle devient une violence\index[tags]{violence} *passée* sur
laquelle certaines avancées essentielles de notre monde se sont
construites. En France\index[tags]{france}, la décapitation du roi est un bon exemple et,
aujourd'hui, Jeff Bezos est autrement plus puissant et oppressif que ne
l'était Louis XVI. Les actions directes des grévistes, des suffragettes
ou des Black Panthers, particulièrement violentes, ont mené à un monde
plus féministe, moins raciste, un monde dans lequel on ne bosse plus 12
heures par jour, 360 jours par an. Et ces violences-là, ces violences
d'hier, sont largement acceptées, sinon justifiées par la population.
S'il est plus juste, plus antiraciste, plus féministe, plus écologique,
notre futur justifiera la violence\index[tags]{violence} des black blocs d'aujourd'hui.

On reproche au bloc de diviser les luttes, mais l'expérience montre
aussi qu'en guidant le discours public vers une division entre les
gentillexs et les méchanxtes manifestanxtes, il tient la police occupée
loin des initiatives communautaires ouvertes et alternatives, tout en
forçant les médias à considérer positivement ces dernières (par
opposition au bloc). Très souvent, presque toujours, les personnes qui
participent aux blocs revendiquent la diversité des tactiques, défendent
la nécessité d'une entraide communautaire et comprennent l'utilité de
stratégies pacifistes et non violentes.

En revanche, les porte-parole\index[tags]{parole} des mouvements réformistes qui jouent le
jeu et en profitent pour redorer leur blason dans la presse, en se
désolidarisant du bloc, celleux-là ne font que renforcer la légitimation
de la violence\index[tags]{violence} d'État. D'ailleurs, il n'est pas rare que les
représentanxtes politiques émettent des sommations, des injonctions à
condamner la violence\index[tags]{violence}. C'est un élément très important de la rhétorique
politicienne pour pacifier et diviser les luttes.

Petite pièce de théâtre\index[tags]{théâtre} réelle qui montre un exemple de bonne réaction
face à une injonction médiatique au pacifisme :

> David Pujadas : On comprend bien sûr votre désarroi, mais est-ce que
> ça ne va pas trop loin ? est-ce que vous regrettez ces violences ?
>
> Xavier Matthieu (délégué CGT de *Continental*) : Vous plaisantez
> j'espère ?
>
> (*Un moment d'interview.*)
>
> David Pujadas : Bon, on entend votre colère\index[tags]{colère}, mais est-ce que vous
> lancez un appel au calme ce soir ?
>
> Xavier Matthieu : Je lance rien du tout. J'ai pas d'appel au calme à
> lancer. Les gens sont en colère\index[tags]{colère} et la colère\index[tags]{colère} faut qu'elle s'exprime.
> Qui sème la misère, récolte la colère\index[tags]{colère}.
>
> (*Journal télévisé de France\index[tags]{france} 2, 21 avril 2009*.)

On notera au passage que Pujadas appelle "désarroi" ce que ressentent
des centaines de salariéexs licenciéexs par une multinationale qui
affiche des bénéfices indécents cette année-là. En dénigrant les
stratégies plus violentes, le discours médiatique renforce l'image
publique d'un mouvement chaotique et divisé, alors qu'il faut construire
l'image d'un mouvement cohérent acceptant des tactiques
plurielles[^39]. Plus certaines formes d'actions seront marginalisées
par les médias, les politiques et les autres mouvements, plus la police
pourra se permettre de les réprimer violemment.

On reproche souvent au bloc de constituer une minorité isolée. Or
l'histoire des luttes repose fondamentalement sur le recours à l'action
directe° par des petits groupes. C'est ce qu'il s'est passé pendant les
grèves suisses de 1917 par exemple. L'action directe émerge quand les
gens se défendent. S'il fallait toujours attendre que des millions de
personnes se mobilisent simultanément ou que les politiciennexs écoutent
les revendications des collectifs militants et des syndicats, nous
vivrions aujourd'hui dans un monde bien pire. En tant que tactique, on
pourrait même refuser l'argument de la "minorité radicale", puisqu'il
y a en réalité un continuum entre les revendications émancipatrices et
l'action du bloc : en pratique, un *green bloc°* porte souvent les
mêmes revendications que des mouvements pacifistes, comme la Grève du
Climat en Suisse.

On reproche aux black blocs leur seule envie de casser : les opposanxtes
les appellent d'ailleurs les "casseurs", on les dépolitise. On les
caractérise par "une folie destructrice apparemment sans raison \[...\]
sans aucune motivation politique ou idéologique[^40]". Le
contre-argument principal se situe sur les banderoles présentes dans les
black blocs et sur la sélectivité des cibles vandalisées.

On reproche souvent au bloc de légitimer et de renforcer la brutalité de
l'État policier. Si ce genre d'argument peut être utilisé, alors autant
abandonner d'emblée toute idée de descendre dans les rues ensemble :
l'État policier se justifie lui-même. Dès qu'une revendication prend
suffisamment d'ampleur, tout le dispositif répressif s'active, et dans
toutes les situations. Depuis quand tenons-nous nos alliéexs pour
responsable de l'augmentation croissante des violences policières ?

On reproche souvent au bloc de délégitimer les mouvements et de leur
faire perdre toute crédibilité aux yeux des médias dominants. Même
logique : depuis quand les médias dominants sont-ils du côté des luttes
sociales ?

En résumé, il n'y a aucune raison d'idéaliser le black bloc, qui n'est
qu'une tactique comme une autre (pas toujours pertinente et efficace),
mais le problème, c'est que la majorité des arguments qui s'y opposent
relèvent essentiellement d'*a priori* ayant largement intériorisé des
valeurs dominantes et oppressives (le respect de la propriété privée, la
présentabilité, la responsabilité administrative, la légitimité aux yeux
des médias de masse, etc.).

## Survivre aux lacrymos

La police antiémeute suisse recourt plus souvent aux fumées lacrymogènes
pour disperser les énormes manifestations, souvent festives.
D'expérience, elle semble plus encline à l'usage de flash-balls ou de
canons à eau\index[tags]{eau} lorsqu'il s'agit de disperser de petits groupes organisés.

Les lacrymos, ça arrive, mais on peut s'y préparer :

- ne panique pas, c'est horrible, mais ça passe en 10 à 15 minutes, ça
ira mieux quand tu seras au chaud, avec un thé\index[tags]{thé/tisane} et un câlin\index[tags]{câlin}.
- si tu as des nausées sur le moment, elles passeront.
- si tu as des crampes ou des spasmes sur le moment, ou si tu as une
gêne respiratoire qui persiste après une heure, va consulter unex médecin.
- si tu attends un enfant et que tu es exposéex à du gaz lacrymogène, il faut
aussi aller voir unex médecin.
- produire un excès de sécrétions (salive, morve) est un réflexe
biologique de défense, n'hésite pas à te moucher et à cracher autant que
possible, autrement, tu peux vite avoir le sentiment de te noyer dans
tes miasmes.
- essaie d'enrouler ta bouche\index[tags]{bouche} et ton nez dans une écharpe mouillée (y
compris sous une cagoule), si elle est mouillée de transpiration, ça
peut marcher aussi. Assure-toi de toujours avoir de l'eau\index[tags]{eau} dans ton sac.
- protège ton cuir chevelu, le gaz lacrymogène s'infiltre dans les
pores.
- de manière générale, moins ta peau est exposée, moins le gaz sera
agressif.
- évite de te raser toute partie du corps\index[tags]{corps} exposée (y compris le visage\index[tags]{visage})
juste avant une manifestation, le gaz irrite beaucoup les pores à vif.
- touche tes yeux le moins possible.
- n'oublie pas tes lunettes\index[tags]{lunettes} de piscine.
- apporte du sérum physiologique en quantité dans ton sac (ça s'achète
en pharmacie sans ordonnance et c'est pas très cher).
- si possible, privilégie les lunettes\index[tags]{lunettes} aux lentilles, même si c'est
parfois galère sous une cagoule. Le contact avec le gaz risque de les
faire fondre. Si t'as quand même tes lentilles, dès que la moindre
particule de lacrymo se pointe à l'horizon, dépêche-toi de les enlever
avant d'avoir du gaz plein les doigts et enfile tes lunettes\index[tags]{lunettes}. Si tu te
retrouves avec du lacrymo sur les lentilles, remplis tes yeux et tes
mains de sérum physiologique, enlève tes lentilles, et nettoie encore.
- sur le coup, tu peux aussi essayer d'arrêter les lacrymos, mais
attention, cela implique de se rapprocher du gaz. Il y a deux approches.
  1. Prendre une raquette de tennis, ou tout objet long et élastique,
     puis renvoyer la grenade d'un coup sec (en visant l'envoyeur, bien sûr).
  2. Une bombe lacrymogène émet du gaz sur la base d'une explosion
     pyrotechnique. En gros, quelque chose brûle à l'intérieur, et tant que
     ça brûle, du gaz sort. Tu peux donc essayer de l'éteindre comme tu
     éteindrais un feu\index[tags]{feu} (tu peux aussi allumer des feux[^42]), par exemple
     en posant un cône de chantier par-dessus et en versant beaucoup d'eau
     par l'ouverture supérieure (peu efficace, car cela demande beaucoup
     d'eau\index[tags]{eau} et qu'on en a rarement en grande quantité sous la main), ou plus
     simplement en posant par-dessus un carton, voire en jetant la grenade
     dans un conteneur poubelle (qu'il faut refermer bien sûr). Aujourd'hui,
     plusieurs modèles sont waterproofs et l'astuce risque de ne pas marcher.
- finalement, une fois rentréex à la maison, limite\index[tags]{limite} au maximum le
contact entre tes vêtements et ton intérieur (les particules des
lacrymos peuvent rester actives jusqu'à cinq jours) et prend une douche
froide d'environ 20 minutes, elle réduira l'irritation au minimum.

## Survivre à la répression policière

- d'abord, et c'est le plus important, tu peux exposer les autres à la
répression, alors RÉFLÉCHIS au mouvement que tu viens agiter : si c'est
pour mettre en danger des personnes déjà particulièrement vulnérables
face à la répression (dans un mouvement de lutte des sans-papièrexs par
exemple), autant rester chez toi. Les luttes du travail et/ou
écologiques sont souvent plus propices à des actions radicales et
autonomes.
- de manière générale, ne débarque jamais à une manifestation équipéex
comme ces gens cools sur les photos, avec tout ton matos sur toi. Déjà,
parce que tu auras pas l'air malinex dans le métro, ensuite parce que si
tu es identifiable, la police ne va pas te tirer dessus ou te gazer,
elle va très probablement juste t'encercler et t'arrêter. Pour y
échapper, mieux vaut être mobile et bien se fondre dans la masse.
- ne te lance jamais seulex, viens toujours accompagnéex d'un groupe
avec qui tu as des affinités et essaie de ne jamais le perdre de vue.
Pour se déplacer ensemble à travers une foule, toujours se tenir la
main. Si tu ne connais pas un groupe, viens au moins avec une personne
"binôme" pour pouvoir\index[tags]{pouvoir} veiller mutuellement l'unex sur l'autre.
- agis toujours au début ou au milieu d'un mouvement de masse, jamais à
la fin. D'abord pour donner un ton à la journée (qui va souvent se
radicaliser si elle est agitée dès le départ) et surtout pour éviter de
se retrouver isoléexs en fin de journée. La police est beaucoup plus
violente quand il n'y a plus un grand nombre de "gentillexs"
citoyennexs pour les regarder.
- le moment du masquage est crucial et c'est une question difficile :
trop tard, tu risques d'être identifiéex, trop tôt, tu risques de te
faire attraper. Des petits groupes masqués qui se promènent isolés avant
un événement sont des cibles idéales. Dans les grandes manifs, le mieux
est de se fondre dans la masse, avec des gens autour de toi en qui tu as
confiance, pour rejoindre un point de rendez-vous et se masquer.
- prépare plusieurs couches de vêtements pour te changer en cours de
route, et te rechanger avant de partir.
- essaie d'être toujours en lien avec une éclaireuxse (une personne qui
a envie de prendre moins de risque), qui tourne autour du bloc pour le
prévenir des mouvements de masse de la police.
- essaie de maintenir un bloc compact, pour éviter que des petits
groupes de flics ne se glissent pour saisir une personne en particulier.
De grandes bannières renforcées à l'avant et sur les flancs sont utiles
aussi pour éviter ce genre d'interpellation. Attention, il faut être
plusieurs pour les tenir.
- utilise un surnom d'un jour, dans le groupe, pour éviter de
s'adresser les unexs aux autres par des identités connues.
- communique en respectant les règles de sécurité numérique, notamment
si tu as besoin d'un canal de discussion sur smartphone[^43]. Sortir
sans smartphone est toujours plus prudent.
- une banderole ou une pancarte peut efficacement dissimuler un
bouclier plus solide.
- les protections de sport (tibias, genouillères, etc.), ça peut
servir.
- les lampes torches, ou même les lasers de loisir, ça peut aveugler.
- le plus souvent, la police disperse (avec du gaz, des flash-balls,
etc.) quand elle n'est pas préparée pour effectuer des arrestations. Une
fois prête, elle va probablement charger, en ligne et par vagues
successives (en avançant, puis en reculant). La première stratégie est
généralement d'effectuer quelques arrestations individuelles violentes
pour impressionner et décourager les autres manifestanxtes. Si la foule
est compacte, elle va probablement garder ses distances. Si la foule a
l'air évasive, chaotique ou passive, la police va essayer de former des
lignes pour découper la masse et isoler des plus petits groupes, qu'elle
pourra alors encercler (c'est ce qu'on appelle une "nasse°").
- si tu te fais prendre par la police, garde à l'esprit que même le
plus petit réflexe d'autodéfense de ta part peut considérablement
aggraver les charges (assaut sur agent). Une bonne méthode est de
devenir instantanément un poids mort, en arrêtant tout mouvement. Cela
limite les charges et complique souvent le travail des flics.
- à la fin de l'action, le dernier challenge, c'est de s'en sortir
tranquillement. Les couches de vêtements sont bien utiles : trouve un
endroit tranquille pour te changer, pourquoi pas au milieu de la foule
ou dans une rue\index[tags]{rue} parallèle, mais toujours en vérifiant la présence de
caméras. Si tu ne connais pas la ville, n'hésite pas à suivre les
groupes de gens qui s'enfuient, ou à t'installer dans un café\index[tags]{café} en
attendant que le temps passe. Mieux vaut dissoudre le groupe et rester
en binôme.
- prépare-toi bien aux aspects juridiques de la répression. Pars en
manif avec un numéro d'avocat appris par cœur, ou écrit sur ton bras, et
consulte un bon guide d'antirépression écrit par des militanxtes de ton
pays, si possible un guide récent (les lois changent vite). Ces guides
aident par exemple à comprendre ce que tu peux dire ou ne pas dire
pendant une garde à vue, etc.
- et puis la règle absolue de l'action directe : arrête quand tout va
encore bien, n'abuse pas et économise-toi pour la prochaine.

## Survivre en conclusion

L'émeute n'est pas un projet politique, elle ne doit jamais le devenir,
elle n'est qu'une forme de contestation (d'ailleurs, dans l'histoire, il
y a même eu des émeutes policières). En gardant à l'esprit qu'on agit
ainsi *parce qu'on ne nous laisse pas le choix de la violence\index[tags]{violence}*, on reste
concentréexs sur ce qui importe : construire un monde bienveillant et
fait de *care°* mutuel. Le reste est accessoire et tactique. Parfois
c'est un exutoire, parfois c'est une réparation, mais l'expérience du
bloc nous rappelle toujours *ce qui compte vraiment* : être ensemble,
résister, s'aimer et ne pas abandonner le rêve d'un monde moins
pire[^44].

[^36]: Cette citation ouvre le livre *Les black blocs, la liberté et l'égalité se manifestent* de Francis Dupuis-Déry.

[^37]: Lire *Drones*\ \[n^o^\ 1\] pour un récit critique de cette confrontation.

[^38]: *Kill the hippie in your head*\ \[n^o^\ 48\] raconte une déconstruction progressive de la non-violence.

[^39]: *Faudrait pas que notre révolution ait l'air trop révolutionnaire*\ \[n^o^\ 44\], une discussion critique à l'interne d'un grand mouvement pacifiste, offre des éclairages sur cette question.

[^40]: Office fédéral de la police, Département fédéral de Justice et Police, Service d'analyse et de prévention, \enquote{Le potentiel de violence résidant dans le mouvement antimondialisation}, Berne, juillet 2001.

[^42]: On trouvera dans *L'usure ordinaire*\ \[n^o^\ 6\] d'autres usages des feux en manifestation.

[^43]: *Camouflage dans l'infosphère*\ \[n^o^\ 40\] présente quelques éléments pratiques de sécurité numérique.

[^44]: Lire *Lendemains*\ \[n^o^\ 3\] pour se préparer à affronter la ville le jour d'après.

