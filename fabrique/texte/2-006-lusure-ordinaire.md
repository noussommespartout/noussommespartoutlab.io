---
author: "Anonyme"
title: "L'usure ordinaire"
subtitle: "Histoire de ville abîmée"
info: "Texte rédigé pour le recueil"
date: "décembre 2020"
categories: ["sabotage, action directe"]
tags: ["index"]
lang: ["fr"]
num: "6"
quote: "refuser la vie ordinaire"
---

À 17 ans, j'ai déjà grave la haine des flics. Je vais cramer la
gendarmerie de mon village.

Je décide de lancer un cocktail Molotov sur le bâtiment. J'ai pas encore
le permis voiture\index[tags]{voiture/bagnole}, je roule en mobylette, sans plaques
d'immatriculation. Un soir, je sens que c'est le moment. Je prépare mon
cocktail, sans trembler, dans une bouteille en verre, en mélangeant de
l'essence\index[tags]{essence} avec un engrais\index[tags]{engrais} chimique qui traîne dans l'atelier de jardin
de mon grand-père. Je laisse bien dépasser la mèche en tissu et j'oublie
pas de scotcher le goulot pour éviter les appels d'air. Je glisse la
bouteille dans un sac à dos, j'enfile un casque de moto intégral, pas
celui que j'utilise d'habitude. J'enfourche ma mobylette et je prends la
route. Arrivé à l'entrée de la ville, je m'arrête dans un coin, je
glisse la bouteille dans la poche de mon manteau. Il doit être une heure
du matin et la ville n'est plus complètement allumée. Tout va très vite.
Je me gare\index[tags]{gare} devant le poste. J'allume le molotov. Je le lance par-dessus
la haie. Je prends à peine le temps de le regarder s'éclater sur les
marches, un mètre devant la porte, et paf, j'appuie sur l'accélérateur.
La bouteille crame les marches devant la gendarmerie, un mètre devant la
porte. C'est carrément loupé. Il n'y a presque pas de dégâts. Personne
n'en entend parler. Je ne me fais pas choper. À partir de là, je
m'améliore.

Je continue à saboter\index[tags]{sabot/saboter/sabotage}, à créer de l'usure ordinaire. Je ne cherche pas
le truc spectaculaire, plutôt le truc qui abîme le système petit à
petit, en permanence\index[tags]{permanence}, ou alors le truc qui décore. Ça peut aller
ensemble : dégrader et décorer. J'ai envie de faire chier les flics, de
faire chier l'État. Puis je commence à traîner dans le milieu anarchiste
et je rencontre rapidement des gens qui partagent mon goût pour l'usure
ordinaire. On commence à faire des tags. Sur les murs, on laisse des
messages simples, contre le fascisme, contre le système. C'est trop
bien. On s'éclate. C'est aussi un stress, une adrénaline\index[tags]{adrénaline}, on fait un peu
n'importe quoi, on ne repère pas les zones d'action avant, on ne
s'occupe pas de savoir si on est surveilléexs. On ne se fait pas
prendre. On continue.

On s'organise mieux. On commence à faire attention. Chaque fois qu'on
traîne en ville, on note l'emplacement des caméras de surveillance. Au
fil des mois, on constitue le plan de la surveillance urbaine, en notant
toutes les caméras sur une carte. On l'imprime et on la distribue à
touxtes celleux qui veulent. Avec cette carte dans la poche, tu te sens
plus en sécurité, parce que tu sais où est-ce que tu peux taguer,
saboter, créer de l'usure. C'est pratique aussi quand tu dois t'enfuir,
pour une raison ou pour une autre, ça permet de savoir intuitivement par
où passer. Maintenant, si on refaisait le plan, y'aurait des caméras à
tous les coins de rue\index[tags]{rue}. L'objectif, ce serait plutôt de chercher
désespérément une rue\index[tags]{rue} sans caméra\index[tags]{caméra}.

Après, on commence à saboter\index[tags]{sabot/saboter/sabotage} les horodateurs, les trucs qui servent à
payer ta place de parking en ville. C'est assez facile. Il suffit
d'enlever la capsule en alu qui ferme les canettes de bière\index[tags]{bière}, de
l'enrouler dans du papier et de la glisser dans la fente pour les
pièces. Les horodateurs se bloquent instantanément. Il y a une sécurité
qui s'enclenche et ça devient impossible\index[tags]{impossible} de payer. Niveau tranquillité,
c'est peinard. Quand on fait ça, personne ne se doute\index[tags]{doute} qu'on n'est pas
juste en train de payer le parking. On prend l'habitude, dès qu'on
marche en ville, d'avoir des capsules plein les poches et de bloquer
toutes les machines qu'on croise. Grâce à ça, non seulement les gens
n'ont plus besoin de payer pour la journée, puisque les horodateurs sont
rarement réparés avant le lendemain, mais en plus ça fait un manque à
gagner pour les villes. Qui est-ce qu'on taxe avec le parking ? Toujours
les mêmes, comme si ça rapportait vraiment des thunes. On devrait
pouvoir se garer où on veut, quand on veut. Pour info, on n'a toujours
pas réussi à instaurer une taxe de 0,1\ % sur les transactions financières
internationales, une taxe qui rapporterait plus de 50 milliards par an.
Bon, aujourd'hui, il y a des horodateurs qui n'acceptent plus que les
cartes bancaires. Tu peux glisser des trucs dans la fente pour la carte bancaire,
mais c'est plus compliqué. Pour les trucs qui se paient avec un QR Code,
comme les trottinettes électriques, c'est plus simple, tu peux gratter
le code pour le rendre illisible ou coller\index[tags]{coller} du scotch à moquette dessus.

Ensuite, avec des potes, on commence à s'organiser en équipes pour aller
démonter des panneaux publicitaires. On fait des petits groupes de deux
ou trois personnes et on marche dans la ville pour les saboter\index[tags]{sabot/saboter/sabotage}. C'est
tout simple. Tu dévisses leur cadre, t'enlèves les affiches,
éventuellement tu les remplaces, ou tu tagues des trucs dessus et tu
remets tout ça en place. Tu risques pas grand-chose. Il te faut juste
une clé triangle. Remplacer tous les panneaux des arrêts de bus, c'est
quand même grave cool. Après, les gros panneaux électriques arrivent
dans les villes, ceux qui affichent en LED. Pour ceux-là, on ouvre les
boîtiers et on arrache les câbles à la main. Sur certains modèles, on
n'y arrive pas, alors on prend des pinces, mais on flippe un peu à cause
de l'électricité. Quand on arrache les câbles à la main, il faut bien
tenir la partie haute et arracher depuis le sucre\index[tags]{sucre}, c'est moins
dangereux.

Un autre truc qu'on fait beaucoup avec ces potes, c'est mettre
régulièrement de la superglu dans les serrures. On cible souvent les
serrures des grillages de chantier, les portes des banques aussi. Grâce
à ça, les gens ne peuvent pas aller bosser le matin, ça emmerde bien la
direction. Iels sont obligéexs d'appeler une serrurerie. C'est hyper
simple, t'as un tube sur toi, tu glisses l'embout dans la serrure et tu
vides le contenu. Sur les cadenas, c'est moins utile, parce que ça se
coupe plus facilement. On vise souvent les infrastructures de la ville,
parce que les gens qui viennent y bosser, les fonctionnaires, s'en
foutent particulièrement. On leur fait gagner une journée, un peu de
temps de vie. Dans les portières de voiture\index[tags]{voiture/bagnole}, la superglu marche bien
aussi, mais maintenant, il y a de plus en plus de voitures qui s'ouvrent
automatiquement. Aujourd'hui, si t'as encore une voiture\index[tags]{voiture/bagnole} qui s'ouvre
seulement avec une serrure, c'est que tu fais probablement pas partie de
la classe des gens que j'ai envie de saboter\index[tags]{sabot/saboter/sabotage}.

Tout ça, bien sûr, c'est de l'usure ordinaire, des actions de faible
intensité que tu montes à plusieurs, mais je vais aussi en manif. Je
commence à manifester dès que je passe mon permis et que je m'installe
en ville. L'avantage des manifs, c'est que t'es protégéex par le nombre.
Pour moi, c'est important d'utiliser mon privilège dans les cortèges, de
taguer, de foutre le zbeul° au nom des gens qui peuvent pas se le
permettre, mais qui le feraient s'iels le pouvaient. Il y a plein de
manières efficaces d'agiter une manif : foutre le feu\index[tags]{feu} aux poubelles, aux
arrêts de bus. En dehors des molotov, un bon plan pour faire partir un
feu, c'est d'emporter une petite bouteille en plastique d'essence\index[tags]{essence} sous
ton k-way[^15]. Tu la verses discrètement dans une poubelle, plutôt
une benne qui a l'air pleine de plastique, le plastique ça brûle bien.
Mais faut faire gaffe, si tu mets trop d'essence\index[tags]{essence}, si t'as des gouttes
qui giclent sur ta peau ou sur tes fringues, tu risques de prendre feu
aussi. Pour éviter ça, un truc pas mal c'est d'attacher plusieurs blocs
d'allume-feu\index[tags]{feu} ensemble avec du scotch. Même, si tu rajoutes une mèche à
ton bloc d'allume-feu\index[tags]{feu}, par exemple un morceau de tissu coincé dans le
scotch, tu fabriques un cocktail molotov à retardement. Quand le cortège
passe dans un endroit, tu le glisses quelque part et il s'allume
plusieurs minutes après, ce qui peut être efficace pour distraire les
flics. Si tu fais ça, privilégie une poubelle, comme ça le feu\index[tags]{feu} ne sera
pas incontrôlable. C'est efficace pour tout ce qui est manifestation
antifasciste, écologiste, pour certaines luttes sociales, pour les
droits du travail. Le principal intérêt, c'est de désorganiser le
dispositif policier, qui ne sait plus où donner de la tête, alors que
les émeutièrexs s'en foutent pas mal. Le système doit se dire que là,
c'est une poubelle qui crame, mais qu'un jour, ça pourrait être une
maison ou une usine\index[tags]{usine}. Il faut qu'iels gardent bien ça en tête, qu'iels
n'oublient pas qu'à la fin, c'est le peuple qui a raison, c'est le
peuple qui décidera. Par contre, il faut pas s'amuser à saboter\index[tags]{sabot/saboter/sabotage} et à
foutre le feu\index[tags]{feu} à n'importe quelle manif. Moi je fais très attention à ça.
Dans les manifs qui rassemblent des personnes moins privilégiées, comme
les manifs antiracistes, ou alors des personnes pour qui c'est déjà
dangereux de participer à un cortège légal, les sans-papièrexs par
exemple, alors là, il faut surtout pas agiter le cortège ou cramer des
trucs, pour pas que ça leur retombe dessus.

En parallèle des manifs, je continue toujours l'usure ordinaire et monte
un peu en intensité. Je participe à quelques sabotages de
chantiers[^16]. On traîne à droite, à gauche et dès qu'on apprend que
des engins vont venir pour raser un bout de forêt ou faire des travaux
de construction, on essaie d'aller attaquer dans les jours qui suivent.
C'est pas compliqué non plus ; faut bien étudier le terrain et il n'y a
pas besoin d'en faire des tonnes. Tu ouvres le réservoir, t'y verses du
sable ou du sucre\index[tags]{sucre} et tu sais que le moteur de la machine\index[tags]{machine} est foutu, ça
leur fait pas plaisir. Ouvrir le réservoir, c'est rarement très
difficile, même s'il y a des réservoirs à clé. Par contre, on fait
attention à ne pas forcer les bouchons, pour pas que les entreprises se
doutent de quelque chose et hésitent à démarrer les machines. Tu forces
le bouchon à la main pour les petites machines, ou avec un pied de biche
ou une pince pour les plus grosses. À choisir, mieux vaut le sucre\index[tags]{sucre}, ça
fait du caramel, ça donne un petit côté fête\index[tags]{fête} au village, ça sent bon la
barbe à papa.

Je commence aussi à déboulonner des pylônes à haute tension\index[tags]{tension}. L'idée,
c'est de faire peur\index[tags]{peur} aux entreprises qui gèrent l'électricité. La nuit\index[tags]{nuit/obscurité},
on va aux pieds des pylônes et on dévisse les boulons qui sont au niveau
du sol. On en déboulonne deux par pied, parfois un peu plus. Ensuite, on
va les poser devant la porte des bureaux des entreprises qui gèrent les
réseaux électriques. En voyant les boulons, iels savent bien qu'ils
viennent de leurs pylônes à haute tension\index[tags]{tension}, mais iels peuvent pas savoir
de quelle ligne ni de quel pylône. Par contre, iels comprennent que s'il
y a un peu trop de vent, leurs lignes pourraient bien se péter la
gueule. Alors iels sont obligéexs d'envoyer des gens inspecter toutes
les lignes, pour trouver les boulons manquants. Certes, tu ne fais pas
directement tomber les lignes, mais tu les fais bien chier. Par contre,
ces boulons-là sont bien vissés, donc il faut choper des énormes clés,
des bras de levier.

En Suisse, il faudrait plus pourrir la ville. Multiplier les collages et
les peintures urbaines. Plus il y a de sabotages, plus tu coûtes de la
thune au système[^17]. Si toutes les personnes motivées allaient faire
des rondes de collage ou de tags toutes les deux ou trois semaines, la
ville serait transformée. Il y aurait des affiches et des messages
partout, ça changerait la donne. On aurait touxtes l'impression de vivre
dans une société différente, en pleine effervescence, plus instable et
plus créative aussi, moins disciplinée. Une ville transformée en
permanence par des actions d'usure, ça inspirerait les gens, ça
changerait ton paysage visuel, ça changerait ce à quoi tu penses quand
tu prends le métro parce que tu verrais des messages peints tous les
cent mètres, des messages qui t'empêcheraient de penser à autre chose.
Moi, quand je me lève le matin, je tourne dans la ville, je vais
contempler mon travail. C'est la partie la plus cool, regarder le décor
urbain qui a changé. Le mieux, c'est de tomber sur d'autres sabotages
que des gens ont faits. Ça, ça fait plaisir. On s'inspire mutuellement.
C'est beau les villes sales, c'est important de ne jamais obéir, de
refuser la vie ordinaire, d'user un peu ce vieux monde qui fait semblant
d'être neuf.

## Quelques conseils pour mener des actions d'usure ordinaire

- Toujours agir tard dans la nuit\index[tags]{nuit/obscurité} et les jours où la vie nocturne est
la plus faible (lundi, mardi, mercredi\index[tags]{mercredi}, dimanche).
- Voyager léger, en emportant le strict minimum, si possible dans un
seul sac à dos.
- S'il y a besoin de lumière, privilégier la lumière rouge, moins
visible de loin. Tu peux peindre ta lampe torche.
- Porter des gants, y compris pendant la préparation du matériel, pour
éviter de laisser des empreintes. Pour la même raison, avant de partir,
toujours nettoyer ton matériel avec un chiffon imbibé d'essence\index[tags]{essence}. Ne pas
jeter de mégots autour du lieu de l'action.
- Agir en groupe, jamais seul. Pour la plupart des actions, quatre est
un bon nombre : deux qui sabotent, deux qui font le guet. Un petit
groupe est plus mobile, il peut agir plus vite.
- Essayer de mener des actions avec les mêmes personnes, pour qu'une
confiance s'installe progressivement.
- Agir sobre, pour réagir avec lucidité à ce qu'il peut se passer.
- Ne pas voir trop grand. Mieux vaut de petites actions d'usure
régulières qu'un gros coup. Les gros coups échouent le plus souvent.
C'est beaucoup moins risqué de coller\index[tags]{coller} dix affiches en trois nuits, que
trente affiches en une nuit\index[tags]{nuit/obscurité}.
- Bien étudier le lieu sur lequel on va agir. Prévoir d'effectuer un
repérage pendant la journée.
- Réfléchir à l'endroit par lequel on va entrer, et à l'endroit par
lequel on va sortir. Prévoir ça permet de mieux anticiper les
déplacements du groupe, en envoyant une personne préparer la sortie dès
le début du sabotage\index[tags]{sabot/saboter/sabotage} par exemple.
- Si possible, entrer et sortir de la zone par des accès différents. Si
l'on découvre l'entrée du groupe, il pourra toujours s'échapper par une
autre sortie prévue.
- Faire attention aux caméras. Si tu dois te cagouler parce que tu vas
agir sous une caméra\index[tags]{caméra}, prévois bien l'endroit où tu vas enfiler ta
cagoule, pour éviter que d'autres caméras ne permettent de remonter
jusqu'à toi.
- Il existe d'autres dispositifs de sécurité à considérer : alarmes
antivols, chiens de garde, détecteurs de mouvements à infrarouge. Si tu
veux attaquer un site occupé en permanence\index[tags]{permanence}, il faudra l'observer
longtemps pour comprendre quand et où la voie se dégage, selon les
routines du personnel de sécurité.
- Toujours essayer, dès la préparation, d'être le plus rapide possible.
- Éviter de faire des actions que la police et la justice\index[tags]{justice} pourraient
facilement lier les unes aux autres. Par exemple, en ne signant pas les
tags ou en changeant complètement le style et le contenu de ses affiches
ou de ses collages.
- En variant les types d'action, on minimise aussi la probabilité d'un
recoupage d'informations. Si tu te fais choper en train de coller\index[tags]{coller} des
affiches, difficile de prouver que tu es aussi responsable des serrures
sabotées il y a trois semaines.
- Saboter\index[tags]{sabot/saboter/sabotage} peut créer de l'euphorie, de l'adrénaline\index[tags]{adrénaline}, mais il faut la
gérer. Si tu as peur\index[tags]{peur}, si tu fatigues, rentre à la maison et fais-toi un
thé. Quand il y a de la peur\index[tags]{peur} dans le groupe, les mouvements sont moins
fluides, on se met plus en danger.
- Ne pas se mettre de pression, ne pas faire de concours, ne pas y
aller si on ne le sent pas, même si c'était prévu.
- Prendre soin les unexs des autres, ne jamais mettre de pression sur
les autres, ne jamais mépriser celleux qui le sentent pas.

## Des actions de plus grande ampleur

### Préparer

Le repérage est essentiel, ainsi que de ne pas se faire prendre en
repérant. Use de ton imagination, déguise-toi, trouve de bonnes excuses
pour t'introduire incognito et obtenir des informations.

Pose-toi les bonnes questions : à quelle distance est le commissariat le
plus proche ? où sont les entrées, les sorties, les ascenseurs ? où sont
les caméras ? combien de personnes travaillent ? quand le lieu est-il
fréquenté ?

Chaque personne impliquée doit avoir un rôle précis, quitte à oublier la
spontanéité. Même ce qui paraît dérisoire (déplier une banderole, etc.)
doit être organisé. On improvise le moins possible. Il est important de
connaître l'essentiel des personnes qui participent et de savoir qui
doit s'occuper de quoi, pour pouvoir\index[tags]{pouvoir} réagir au mieux en cas d'imprévu.
Plus l'organisation est horizontale et avec des rôles tournants, plus
les personnes se sentent impliquées dans l'action. Il faut toujours
prévoir une réunion\index[tags]{réunion} la veille de l'action avec toutes les personnes
impliquées. C'est aussi le moment de répéter les rôles et de faire
l'inventaire du matériel.

Le plan de base doit inclure un itinéraire vers la cible sans caméras de
surveillance, un point de chute ou un parking, un point d'entrée et de
sortie de la cible, l'heure et le jour de l'action, le temps que prendra
chaque opération

### Communiquer

En présence des flics, il est bon de prévoir des systèmes préétablis de
communication et des codes simples. Si certains scénarios sont
possibles, il faut se mettre d'accord pour leur donner des noms de code
(pour signaler que les flics sont arrivés, que l'une ou l'autre
éventualité se produit, etc.).

Sur des actions où les personnes sont dispersées et ne peuvent
communiquer directement, il peut être utile de mettre en place un numéro
de téléphone\index[tags]{téléphone} portable centralisé que touxtes peuvent appeler,
éventuellement un groupe Signal temporaire, avec effacement des
messages[^18]. Pour les actions les plus dangereuses, ne pas du tout
avoir de téléphone\index[tags]{téléphone} peut être une option. Mieux vaut également se mettre
d'accord au préalable sur le mode de décision durant l'action, en
accordant aux personnes une confiance\index[tags]{confiance} et une autorité ou en favorisant
la décision collective quand elle est possible, même si sur le coup,
elle ne l'est pas toujours.

### Se sécuriser

Il faut se fixer un nombre minimal de personnes nécessaires et ne pas
agir si ce nombre n'est pas là. La répression va le plus souvent faire
peser l'ensemble des charges sur quelques personnes pour faire des
exemples. Attention à bien être au courant du type de risques juridiques et des
meilleures réponses possibles en cas d'inculpation. Le mieux est de
prévoir un topo d'antirépression à communiquer à touxtes les
participanxtes. Durant la préparation, bien appliquer les conseils de
sécurité numérique habituels.

Durant l'action, il faut se concentrer sur ce que l'on fait, mais garder
quand même un œil sur les autres. Il peut être utile de prévoir des
heures précises de rassemblement. Si des personnes manquent à l'appel,
mieux vaut chercher à comprendre ce qui leur est arrivé, sans se mettre
en danger soi-même. Si l'action ne se passe pas comme prévu, il faut
suivre le plan de dispersion de secours, si possible en restant en
groupe, au moins en binômes.

### Documenter

Il est important de documenter ses actions, d'en garder des traces. Les
collages par exemple, disparaissent parfois très vite... Attention à ne
prendre aucune photo ou vidéo qui pourrait incriminer les personnes
présentes. Le mieux est de bien se renseigner sur la traçabilité des
appareils photo. Si on veut utiliser un smartphone, appliquer les règles
d'hygiène numérique. Dans le cas d'abus policiers ou de violence
contre les activistes, filmer[^19].

[^15]: *Drones*\ \[n^o^\ 1\] raconte l'usage d'un feu d'artifice détourné.

[^16]: Sur le sabotage, lire aussi *Comment bien rater un contrôle technique? *\[n^o^\ 54\].

[^17]: *Piraterie ordinaire*\ \[n^o^\ 38\] aborde également la question du sabotage.

[^18]: Pour en lire davantage sur les messageries sécurisées, voir *Camouflage dans l'infosphère*\ \[n^o^\ 40\].

[^19]: *Surveiller la surveillance*\ \[n^o^\ 55\] explique la pratique du copwatch.
