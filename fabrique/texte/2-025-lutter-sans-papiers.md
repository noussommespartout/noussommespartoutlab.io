---
author: "Faris"
title: "Lutter sans papiers"
subtitle: "Choisir ce qu'on veut être"
info: "Entretien retranscrit"
date: "juillet 2020"
categories: ["luttes migratoires", "antiracismes", "relation à la militance"]
tags: ["index"]
lang: ["fr"]
num: "25"
quote: "pour être militant, pour faire avancer les choses, il faut avoir un statut légal"
---

Je m'appelle Faris, j'ai 28 ans et je suis de nationalité
marocaine. Ça fait trois ans que je suis installé en Suisse, à
Lausanne. Avant, j'étais étudiant en France\index[tags]{france}, mais faute de moyens et de
garanxte pour renouveler mon titre de séjour, je me suis retrouvé en
situation illégale. J'ai décidé de partir en Suisse pour éviter les
contrôles systématiques, pour ne plus risquer une expulsion de France\index[tags]{france}.

Je ne me considère pas comme militant. Pour être militant, pour faire
avancer les choses, il faut avoir un statut légal. Dans l'illégalité, tu
ne peux pas faire entendre ta voix\index[tags]{voix}, parce qu'il y a toujours le risque
d'être arrêtéex, d'être amendéex. Je ne suis pas assez protégé pour
pouvoir m'exposer à de tels risques. Je n'ai pas la possibilité de
lutter à haute voix\index[tags]{voix}. Donc je suis un peu en retrait, c'est à travers
d'autres personnes que j'arrive à lutter, à donner de la force\index[tags]{force} et à
soutenir des mouvements féministes et écologistes. C'est très frustrant
parce que je ne peux pas être là à 100\ %. Je ne donne pas tout ce que je
peux donner, tout mon potentiel, toute ma force\index[tags]{force}, mais une voix\index[tags]{voix} de plus,
même en retrait, ça fait toujours une voix\index[tags]{voix} de plus.

C'est vrai que c'est frustrant, fatigant et décourageant de ne pas
pouvoir être là quand il le faut. Avant, je n'étais jamais allé aux
manifestations, j'avais un peu peur\index[tags]{peur}. Pas *un peu*, j'avais vraiment
peur. Heureusement, il y a eu des personnes du collectif dont je fais
partie aujourd'hui qui m'ont entouré. Ça m'a donné de l'assurance. Sans
elleux, j'aurais pas été présent lors de la dernière manifestation
féministe[^69]. Avec toutes les arrestations que les manifestanxtes
subissent, tout ce que la police fait pour arrêter ces phénomènes
qu'elle considère comme illégaux...

Quand j'étais encore au Maroc, j'entendais parler de féminisme, mais je
ne comprenais pas exactement ce que c'était. Je savais juste que c'était
l'égalité entre hommes et femmes, pas plus que ça. En fait, j'étais déjà
féministe, mais sans le savoir. J'ai toujours voulu l'égalité, la fin
des violences envers les femmes, la fin des maltraitances. J'avais les
mêmes idées, mais c'était général et superficiel. Je ne connaissais pas
le terme et le mouvement.

Au Maroc, je ne connaissais pas non plus le mouvement écologiste. En
France, j'étais en colocation avec une fille, c'était une Française,
elle avait une trentaine d'années, moi j'avais 20 ans. Elle fumait des
cigarettes roulées. À chaque fois qu'elle terminait sa cigarette\index[tags]{cigarette/clope}, elle
séparait le filtre du mégot, elle jetait le mégot à la poubelle et le
reste par terre. Elle disait que c'était biodégradable, alors que le
filtre non. Au Maroc, il y a juste des gens qui récupèrent les canettes
en aluminium pour les revendre. Cette fille, elle a ancré ces manières
de faire dans mon mode de vie. J'ai appris à faire attention aux
déchets, je ne jette plus mes cigarettes par terre. Il y a des lois en
France, mais c'est pas ce qui a fait que j'ai changé de comportement.
C'est elle qui m'a rendu conscient de la question du climat et de
l'environnement.

Ma colocataire en France\index[tags]{france}, elle ne me parlait pas du tout de féminisme.
Je ne sais pas si c'était à cause de la religion\index[tags]{religion}, elle savait que
j'étais musulman et pour elle les musulmans sont un peu... Enfin, en
tout cas, elle ne m'en a jamais parlé. Quand je suis arrivé en Suisse,
j'ai fait des connaissances, j'ai rencontré les personnes du collectif
dont je fais partie maintenant. Et avec le temps, en les entendant
parler, en voyant leur façon de se comporter, j'ai développé beaucoup de
connaissances. Je suis une personne différente maintenant, je ne vois
plus les choses de la même façon.

On est en Suisse, chacunex vit sa vie, chacunex fait ce qu'iel veut, on
n'est plus au Maroc. Hier j'ai croisé un gars, un musulman marocain
aussi. Moi je portais un chapeau de cow-boy et j'étais devant la maison
du collectif. Il me regarde et il me dit "c'est quoi ce chapeau, c'est
quoi ces cheveux ?". Il m'a dit : " tu deviens comme elleux". Et je
lui ai répondu "on n'est plus au Maroc, je peux faire ce que je veux de
ma vie maintenant, j'ai le droit. Ces personnes m'ont ouvert les yeux".
Avant je pensais toujours à ce que les gens allaient penser de moi.
Aujourd'hui, ça n'est plus comme ça. L'autre jour, je parlais de
féminisme et un gars a dit un truc inapproprié et je lui ai dit " t'as
pas le droit de dire ça". Sur mon téléphone\index[tags]{téléphone}, j'ai collé le logo\index[tags]{logo} de la
grève féministe et il me dit "C'est quoi ça?". Je lui ai dit "on est
en Suisse et je peux faire ce que je veux". Il me répond "l'Islam a
donné une place à la femme, c'est pour des bonnes raisons, c'est pas
maintenant qu'on va changer ça". J'ai dit "L'Islam valorise la femme,
ça ne sert à rien de déformer les choses..." En deux jours, deux
personnes m'ont dit "C'est bon, t'es en train de devenir européen."
Par certaines personnes, t'es vu comme si t'étais un traître, comme si
t'avais abandonné ta religion\index[tags]{religion}, alors que je ne bois pas d'alcool\index[tags]{alcool}, je ne
mange pas de porc, je suis à 80\ % religieux disons et ça ne m'empêche pas
de lutter pour ces causes. Et, quand j'aurai régularisé ma situation, je
m'impliquerai encore plus dans la lutte.

Au cours de cette transition, de ce changement, il y a eu plusieurs
étapes. J'apprenais, je regardais, je remarquais. Au début, j'avais des
doutes liés à la religion\index[tags]{religion}. Avant, je ne pouvais pas accepter qu'on me
traite de gay ou de bi, mais au fond de moi j'ai toujours été pour la
cause LGBTQIA+, même si je n'arrivais pas à l'exprimer. Au bout d'un
moment, j'ai commencé à me remettre en question. J'ai commencé à voir
des couples homosexuels, des choses que je n'avais jamais vues avant. Et
à me dire dans ma tête "c'est un peu contradictoire Faris quand même".
Je savais que chacunex avait le droit de vivre sa vie comme iel voulait,
d'être religieuxse ou pas, d'aller à l'encontre de ce que la société
nous impose comme le modèle familial homme-femme-enfants. J'ai commencé
à me remettre en question et à me dire que chacunex doit pouvoir\index[tags]{pouvoir} vivre
sa vie à sa guise, qu'on n'a pas à être attaché au modèle imposé par la
société, qu'on peut choisir ce qu'on veut être, qu'on peut devenir ce
qu'on a envie d'être.

En parlant avec les personnes du collectif, ça me donne de la force\index[tags]{force}, ça
m'encourage de voir qu'il y a toujours des personnes dans la lutte,
qu'elles sont là à lever le drapeau, à manifester, surtout pour les
personnes qui ne parlent pas. Je pense pouvoir\index[tags]{pouvoir} apporter des choses à ces
mouvements parce qu'étant arabe et musulman, je fais partie d'une
minorité.

La plupart des personnes qui luttent sont européennes. Rien que de voir
un arabe musulman dans une foule féministe qui lutte pour les droits des
femmes et des LGBTQIA+, ça change quelque chose. Ça ferait réagir
d'autres personnes, d'autres communautés.

Comme j'ai dit avant, heureusement qu'il y a ces gens qui m'entourent.
Parce qu'avec toutes ces contraintes, les papiers, je pense que j'aurais
été complètement découragé. Ces personnes que je vois tous les deux
jours, elles me donnent de la force\index[tags]{force}. C'est ça qui fait que je ne baisse
pas les bras.

[^69]: Manifestation pour la Grève Féministe du 14 juin 2020.

