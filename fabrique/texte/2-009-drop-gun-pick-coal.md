---
author: "Emmathegreat"
title: "Drop gun & pick coal"
subtitle: "Poème"
info: "Texte rédigé pour le recueil"
date: "18 janvier 2021"
categories: ["antiracismes", "relation à la militance", "luttes migratoires"]
tags: ["index"]
lang: ["fr"]
num: "9"
quote: "lâchez les armes et allez ramasser du charbon"
---


\setlength{\parindent}{0em}

Drop guns and pick coal. Stop shooting, but save soul.

I'm not stronger, but I've just been bold to regain my mentality that
has been sold.

And to share some piece of it with you, maybe together we can grow old.

Because you all know we've lost so many young souls, because they chose
colors, guns and not coal.

I've been to places where they have different races. Where I never
heard a gunshot in nine years.

Which got me thinking about my place, where I see blood so many times in
one year.

All by violence\index[tags]{violence}.

Many by gunshot.

I wish I had some other ways to reach you. I really wish I had some
other ways to show you.

I imagine your feet, fitting in my shoes, they can't.

So all I can do is to tell you my shoe size, maybe we can have a year
with few suicide.

By violence\index[tags]{violence} or by gunshot, at least a few lost.

“Black lives matter”, I don't know if that includes us. Or should we
say “African lives matter”, just to gain trust.

I think I'm strong enough to carry more than my own cross. You speak
for yourself, they speak for themselves, I speak for us.

So I say “Drop guns and pick coal. Stop shooting, but save soul.”

\clearpage

**Lâchez les armes et allez ramasser du charbon**

\vspace{1\baselineskip}

Lâchez les armes et allez ramasser du charbon. Cessez le feu\index[tags]{feu}, sauvez des
âmes.

Je ne suis pas plus fort, j'ai seulement eu le courage\index[tags]{courage} de retrouver ma
conscience qui avait été vendue.

Et si j'en partage un peu avec toi, peut-être qu'on pourra vieillir
ensemble.

Vous le savez touxtes, nous avons perdu tant de jeunes âmes, parce
qu'iels choisissent les couleurs, les armes et pas le charbon.

J'ai été en des lieux où il y a des races différentes. Où je n'ai
jamais entendu un coup de feu\index[tags]{feu} en neuf ans.

Ça m'a fait penser à chez moi, où je vois du sang\index[tags]{sang} si souvent.

Toujours à cause de la violence

Souvent à cause des balles.

J'aimerais pouvoir\index[tags]{pouvoir} vous atteindre autrement. J'aimerais vraiment pouvoir
vous le montrer autrement.

Je vous imagine dans mes chaussures, mais ça ne marche pas.

Tout ce que je peux faire, c'est vous donner ma pointure, il y aura
peut-être une année avec moins de suicide.

À cause de la violence\index[tags]{violence} ou des balles, toujours des vies perdues.

"Black Lives Matter", je ne sais pas si ça nous inclut. On devrait
peut-être dire "African Lives Matter", au moins pour reprendre
confiance.

Je pense que je suis assez fort pour porter plus que ma propre croix. Tu
parles pour toi, iels parlent pour elleux, je parle pour nous.

Alors je dis "Lâchez les armes et allez au charbon. Cessez le feu\index[tags]{feu},
sauvez des âmes".

\setlength{\parindent}{1em}
