---
author: "Jupiter"
title: "Brisons l'isolement"
subtitle: "Récit d'organisation contre les prisons"
info: "Texte rédigé pour le recueil"
date: "février 2021"
categories: ["police", "prison, justice, répression", "antiracismes"]
tags: ["index"]
lang: ["fr"]
num: "39"
quote: "l'isolement est l'une des armes principales de la répression"
---

Jupiter est un collectif de soutien aux prisonnièrexs victimes de la
police, la majorité des membres du collectif sont des personnes noires.

Pendant des jours, des mois, des années, nous avons vu des amiexs
disparaître pendant quelques jours, quelques mois, quelques années.
Chaque jour, des gens sont arrêtés puis emprisonnés. En fait, pour
beaucoup, nous ne les voyons même pas. Sans papiers de séjour valables
en Suisse, même si certainexs détiennent des documents délivrés par
d'autres pays européens, leur permettant de voyager, iels sont
considéréexs comme en situation illégale. C'est un moyen direct d'envoyer
une certaine catégorie de personnes en prison, une catégorie de
personnes que le système rend invisibles au point de les faire
disparaître dans des cellules.

L'isolement\index[tags]{isolement} est l'une des armes principales de la répression.

Du poste de police à la prison, tout est organisé pour que la personne
arrêtée n'ait pas de ressources. À Lausanne, cela commence toujours par
la détention dans le quartier pénitentiaire de la préfecture de police
ou au poste de gendarmerie de la Blécherette, zones de non-droit, où les
conditions de détention s'apparentent à de la torture\index[tags]{torture}. Les personnes
arrêtées sont régulièrement battues dans les cellules, humiliées,
menacées de mort et médicamentées de force\index[tags]{force}. Les cellules sont éclairées
24 heures sur 24, une caméra\index[tags]{caméra} filme en permanence\index[tags]{permanence}, pas de lumière du
jour, pas de droit de visite, pas de traduction des lettres officielles
reçues et aucune information sinon le fait qu'il n'y a pas de place
dans la prison et que tu dois attendre... La durée légale maximale de
la détention au poste de police est de 48 heures, mais beaucoup restent
entre 15 et 30 jours. Ensuite, la détention se fait en prison. À
l'intérieur, personne ne sait quand iel pourra sortir. Et les mois passent.
Les jours précédant la libération prévue, les gardiens apportent de
nouvelles lettres ajoutant des jours de peine, à la suite des décisions
que le procureur prend à volonté, alors que la personne reste
emprisonnée. Sans possibilité d'établir un contact avec l'extérieur,
les personnes arrêtées et embarquées disparaissent pour quelques jours,
quelques mois, quelques années.

Chaque jour, la police harcèle les personnes de couleur dans les
rues[^106]. Tous les jours, la police bat, frappe et torture\index[tags]{torture} des
personnes, à l'abri des regards, dans leurs voitures\index[tags]{voiture/bagnole}, dans les buissons,
dans les ruelles, à cause de la couleur de leur peau. Les flics prennent
les transports publics et ne font sortir que les Noirexs pour des
contrôles. Ils entrent dans des restaurants d'Afrique de l'Ouest,
désignent des personnes au hasard et les font sortir pour les fouiller.
Les patrouilles arrivent en grand nombre dans la ville et dans la rue\index[tags]{rue},
interpellent les Noirexs, les alignent contre le mur en les encerclant,
parfois menottéexs, leur demandent leur permis de séjour et les
fouillent. Lors de nombreux contrôles, elles prennent les papiers de
séjour, les détruisent et volent l'argent que les gens ont sur
eux[^107]. Face à cette situation, face au silence\index[tags]{silence}, face à
l'invisibilité voulue par l'État, nous voulons parler, nous voulons
montrer. Nous avons formé ce groupe en suivant ce que nous voyons, avons
vu et, pour certainexs d'entre nous, avons vécu directement. Nous avons
décidé d'organiser des événements, tout d'abord pour pouvoir\index[tags]{pouvoir} récolter
des fonds afin d'apporter un soutien financier à celleux qui sont dans
le besoin, notamment en prison. Par exemple, le simple fait de pouvoir
acheter des cartes téléphoniques permet d'avoir des contacts avec le
monde extérieur. Nous diffusons des informations et, en fin de compte,
nous prenons du temps pour nous réunir, nous rencontrer et penser à
celleux qui ne sont pas là.

L'isolement\index[tags]{isolement} est leur arme, brisons l'isolement\index[tags]{isolement}.

[^106]: Sur le harcèlement quotidien et le profilage racial, lire *They don't see us*\ \[n^o^\ 4\].

[^107]: Une réalité évoquée aussi dans *L'histoire d'une lutte*\ \[n^o^\ 16\].
