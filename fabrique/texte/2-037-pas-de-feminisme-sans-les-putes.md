---
author: "Une membre de Parapluies Rouges (Grève féministe Vaud)"
title: "Pas de féminisme sans les putes !"
subtitle: "Lutter contre l'abolitionnisme du travail du sexe et pour la défense des droits des travailleureuxses du sexe"
info: "Transcription d'un entretien"
date: "19 novembre 2020"
categories: ["féminismes, questions de genre", "syndicalisme, luttes des travailleureuxses", "travail du sexe", "sexualités"]
tags: ["index"]
lang: ["fr"]
num: "37"
quote: "Bonjour, je suis travailleureuxse du sexe"
---

Pendant la préparation de la Grève féministe du canton de Vaud de 2019,
j'ai participé à la création d'un sous-groupe qui lutte pour les droits
des travailleureuxses du sexe\index[tags]{sexe}° (ci-après TDS) et qui vise à souligner
la nécessité pour le féminisme de prendre en considération le
TDS[^100] : le collectif Parapluies Rouges.

L'histoire de la création du collectif est liée à un événement qui a eu
lieu en suisse[^101] en 2019 : une journée organisée par une
institution publique pour laquelle nombre de féministes abolitionnistes°
du TDS ont été invitéexs à parler, dont certaines des voix\index[tags]{voix} européennes
les plus importantes. Le ton global était hyper condescendant et
victimisant envers les TDS. Les discussions
étaient de véritables plaidoyers pour l'interdiction du TDS en suisse
ou, pour le dire autrement, de véritables éloges des modèles politiques de pays
basés sur la "pénalisation du client" comme ceux instaurés
par la suède ou la france\index[tags]{france}. Souvent, les féministes abolitionnistes se
présentent comme les défenseureuxses des personnes qui pratiquent le
TDS. Iels ne souhaitent donc pas les pénaliser directement pour
l'exercice de ce métier, qu'iels ne considèrent, d'ailleurs, pas comme
un métier. Iels luttent contre la prostitution en décourageant la
demande, ce qui revient au même. Les premièrexs concernéexs dénoncent
ces politiques qui précarisent leurs activités en causant une baisse des
tarifs et en réduisant la capacité de négociation\index[tags]{négociation} du service. Le
discours abolitionniste adopte une posture tutélaire et infantilisante,
étant donné qu'il est impossible\index[tags]{impossible}, si on l'écoute, de consentir à se
prostituer : toute personne qui se prostitue est une victime. Dans la
logique abolitionniste, l'autodéfinition, l'autonomie et
l'auto-organisation des TDS sont impensables et logiquement
invisibilisées sur le plan politique.

Bref, du coup, on a décidé d'y aller à cet événement, à cinq, pour faire
acte de présence et montrer qu'il existe une opposition féministe à ces
discours toujours plus influents. On avait des masques neutres blancs,
notamment pour préserver l'anonymat d'une des personnes qui est encore
TDS. On a attendu que la journée se termine pour faire notre petite
action. On n'a même pas interrompu leur colloque, on est venues à la fin
et, en silence\index[tags]{silence}, on s'est simplement mises sur le côté de la salle avec
quelques pancartes. Rien de bruyant, rien d'agressif. Ça a été
extrêmement mal reçu. On nous a reproché d'avoir été "agressives",
d'avoir gâché leur journée. Surtout, on nous a reproché d'essayer de
"faire taire leurs voix\index[tags]{voix}, des voix\index[tags]{voix} différentes et hors-normes".
Paradoxalement, de leur point de vue, c'est elleux qui portent une voix
différente et nous, on venait les censurer et les obliger à se taire.
Dans le public, on a remarqué qu'il y avait des membres de la Grève
Féministe[^102] et on s'est dit qu'il fallait absolument amener une
voix pro-pute\index[tags]{pute} dans ce mouvement qui s'annonçait historique.

Le discours abolitionniste, victimisant systématiquement les personnes
qui pratiquent le TDS, perpétue, voire renforce les conditions précaires
dans lesquelles elles travaillent. En niant la pluralité des situations
dans lesquelles peuvent se trouver les TDS, l'approche abolitionniste ne
se confronte pas à la réalité de leurs conditions de travail et empêche
tout changement. Les abolitionnistes prétendent aider les
professionnellexs, mais iels ne bataillent pas avec les autorités
lorsqu'il y a des restrictions du périmètre d'exercice, iels ne se
battent pas pour les défendre matériellement pour de meilleures
conditions de travail. Pour nous, en revanche, c'est là que se situent
les vrais enjeux : si tu veux aider, ça ne sert à rien de dire : "Oh là
là c'est trop dur, il faut arrêter de pratiquer." Le quotidien des TDS
c'est pas ça, le quotidien c'est qu'il faut bouffer, payer les factures
et qu'il n'y a plus de boulot à cause du confinement.

À la même époque, plusieurs aspects politiques du TDS étaient vivement
discutés à Lausanne, faisant suite surtout à la réduction du périmètre
d'exercice en 2018. C'était pas forcément des approches politiques
abolitionnistes, mais quand même, l'idée était de "lisser" les nuits
lausannoises, en repoussant les TDS un peu plus loin, de les éloigner
d'un quartier en cours de gentrification, pour le dire avec les mots des
autorités, d'un "site stratégique pour le développement urbain". On
sait comment ça marche, on les met toujours un peu plus loin, jusqu'à ce
qu'iels soient carrément en dehors des villes, et qu'iels ne puissent
plus travailler correctement, jusqu'à ce qu'iels soient dans des lieux
reculés et isolés où iels sont exposéexs à davantage de violences ou de
situations dangereuses.

Lorsqu'on a annoncé la création d'un sous-groupe qui s'occuperait de
toutes ces questions, on a vite remarqué que ce n'était pas évident pour
tout le monde. Tous les autres sous-groupes de la Grève féministe se
sont créés sans aucune discussion. Mais pour nous, ça a posé des
problèmes. On sentait que ça créait un malaise et que c'était une source
de tensions. Il a fallu légitimer l'existence du groupe, et c'était
vraiment délicat, mais on a fini par le créer. On s'est regroupées entre
personnes qui avaient des affinités sur ces questions-là. Des personnes
concernées, d'autres pas concernées, des universitaires, d'autres pas,
des personnes du milieu associatif. On a commencé à faire des trucs et
voilà, maintenant, on est légitimes. Personne ne va venir remettre en
cause l'existence de ce groupe. Au fil du temps, d'autres personnes sont
venues nous rejoindre, ça s'agrandit, c'est cool, et deux groupes
similaires se sont formés au sein des collectifs de la Grève féministe à
Genève et en Valais.

Nos activités, c'est surtout des tables rondes, des discussions, des
projections. On a organisé une table ronde à l'Arsenic[^103] sur
l'imbrication des questions liées au TDS et au féminisme. On organise
plutôt des événements symboliques. L'idée, c'est de ramener du monde
pour discuter en montrant qu'il y a des alternatives qui existent, en
proposant des conceptions nouvelles de la sexualité et du TDS, en
défendant et en réaffirmant que la lutte pour les droits des TDS a sa
place au sein des mouvements féministes, dans une perspective non
seulement inclusive, mais aussi révolutionnaire. Pour l'instant, on n'a
pas organisé d'actions "coups de poing" ou de trucs, disons, plus
vindicatifs. Mais c'est comme si, de toute manière, quoi qu'on fasse, on
est vu comme un collectif hyper "extrémiste". En vrai, des projections
et des tables rondes, c'est pas non plus hyper radical.

On est aussi actives sur un plan pratique. On organise des collectes,
des aides d'urgence. Le TDS dans son ensemble, en l'absence de
reconnaissance symbolique, c'est hyper précaire au niveau du statut et
du droit du travail. Pendant les périodes de crise, comme la situation
sanitaire créée par le Covid-19, ce sont toujours les boulots les plus
précaires qui sont touchés en premier. Et là, clairement, le besoin le
plus urgent, c'est une vraie protection du travail. On entend dire que
puisque c'est légal, les TDS peuvent demander des APG (Allocations pour
Pertes de Gain), alors qu'en vérité, peu de personnes peuvent vraiment
prétendre à ces aides. Dans le canton de Vaud, il n'y a pas d'obligation
d'annonce pour les TDS par exemple --- cette obligation existe dans les
autres cantons de suisse. Du coup, il y a énormément de TDS qui ne sont
pas annoncéexs et qui n'ont droit à aucune aide. Cette absence
d'obligation d'annonce est paradoxale. D'un côté, c'est une annonce au
registre du commerce et à la police qui t'oblige à accepter une forme de
surveillance, mais de l'autre, si t'es pas déclaréex et annoncéex
officiellement, t'as pas accès aux aides sociales ; je schématise mais
c'est ça l'idée. Même pour les TDS annoncéexs, les aides sociales,
pragmatiquement, c'est pas incroyable. Quand tu fais une déclaration
d'impôt, les APG sont calculées sur ta précédente déclaration. Donc si
tu viens de commencer, tu n'en as pas, de déclaration précédente, et
donc tu ne touches rien. Aussi, puisque ce sont des boulots socialement
stigmatisés, tu vas pas forcément t'annoncer, tu vas pas en parler
autour de toi, tu vas pas savoir vers qui te tourner pour demander des
aides. C'est pas facile d'aller au chômage et de dire : "Bonjour, je
suis travailleureuxse du sexe\index[tags]{sexe}". Que tu sois unex TDS qui passe par
internet, avec des ressources, ou que tu fasses du TDS dans la rue\index[tags]{rue} avec
moins de ressources, c'est hyper mal vu dans tous les cas et c'est
toujours très compliqué d'aller voir des institutions pour leur demander
de l'aide. C'est pour ça que les personnes les plus précaires se
tournent vers les aides d'urgence associatives, comme la Soupe populaire
ou les aides alimentaires d'urgence. Les associations sont débordées,
elles font ce qu'elles peuvent. Dans tout ça, la ville de Lausanne, qui
se prétend de gauche et progressiste, ne fait finalement que réduire le
périmètre d'exercice et rendre difficile le travail pour celleux qui
sont déjà les plus précaires.

Dans le milieu féministe, on pourrait être sympas et solidaires entre
nous, ça enlèverait un premier effet de stress et de stigmatisation
symbolique pour les copainexs TDS. On en connaît des TDS qui disent :
"Ah moi les féministes, j'ai pas envie de leur parler". C'est lié au
fait que les voix\index[tags]{voix} abolitionnistes sont surreprésentées dans les
mouvements féministes. Du coup, certains milieux féministes renvoient
parfois aux TDS une impression de déconsidération, de délégitimation. Il
y a vraiment du boulot là-dessus, dans les représentations, et dans la
bienveillance à avoir envers touxtes les femmes, les personnes non
binaires° et les personnes trans° ! Pour autant il y a des signaux
positifs : l'existence de nos réseaux, les succès des mobilisations de
solidarité pendant la Covid et même des collages que nous avons eu la
surprise de découvrir et qui rappellent ce slogan : "Pas de féminisme
sans les putes" !

[^100]: Pour lire d'autres témoignages de travailleureuxses du sexe : *Nous Sommes Touxtes des Putes*\ \[n^o^\ 42\] sur le statut du TDS et *Je suis une pute*\ \[n^o^\ 2\].

[^101]: L'autrice de ce texte a demandé à ce que les noms des pays soient écrits en minuscule : "Je ne mets jamais de majuscule aux pays, dans un but symbolique, la majuscule témoignant dans la langue française écrite d'une certaine valeur, valeur que je ne souhaite pas apporter au concept de nation."

[^102]: Pour d'autres témoignages issus de la Grève féministe : *En el feminismo, lo personal es político*\ \[n^o^\ 49\] aborde des questions liées à la maternité et raconte la performance de Las Tesis à Lausanne et *Il faut se nourrir de toutes les révolutions*\ \[n^o^\ 17\] évoque les luttes kurdes au sein du mouvement.

[^103]: L'Arsenic est un centre d'art dédié à la création contemporaine en danse, théâtre et performance à Lausanne.

