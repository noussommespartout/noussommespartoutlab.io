---
author: "Anonyme"
title: "L'absurdité des amendes qui permettent de socialiser un peu"
subtitle: "Travailler sans en avoir le droit"
info: "Entretien retranscrit"
date: "19 septembre 2020"
categories: ["combats institutionnels", "antiracismes", "luttes migratoires", "prison, justice, répression"]
tags: ["index"]
lang: ["fr"]
num: "34"
quote: "aucun droit de séjour"
---

En tant que collectif qui lutte dans le domaine de l'asile[^95], on
fait un travail de soutien, d'information et d'accompagnement des
personnes qui sont dans une procédure d'asile ou en dehors, mais on fait
aussi un travail de dénonciation et de revendication tant au niveau
politique que public. Et parfois, il arrive que ces deux niveaux soient
contradictoires.

Une des nombreuses absurdités du domaine de l'asile, c'est que les
réquéranxtes d'asile déboutéexs --- c'est-à-dire les personnes qui
n'obtiennent pas le droit d'asile --- doivent quitter le territoire.
Comprendre : elles n'ont pas le droit de rester en Suisse. Si les
autorités en ont les moyens, elles vont arrêter ces personnes et les
mettre dans le prochain avion pour les expulser. Mais, en pratique, les
procédures d'expulsion prennent souvent des années. D'où l'absurdité\index[tags]{absurde/absurdité} :
ces personnes restent parfois huit, neuf, dix ans en Suisse, sans avoir
le moindre droit de séjour. Elles restent toutes ces années dans des
foyers pour requéranxtes déboutéexs, dans des conditions dégueulasses,
dans des baraques pourries louées par des propriétaires qui se font
plein d'argent. Et une fois par semaine, ces personnes doivent se rendre
à la police des étrangèrexs --- maintenant c'est devenu le service de la
population, ça fait plus joli... --- pour recevoir un tampon qui permet
ensuite de recevoir l'aide d'urgence. Donc en gros, ces personnes n'ont
aucun droit de séjour en Suisse, elles sont complètement illégales, mais
leur présence est tout à fait officielle et contrôlée, donc légale quoi.
Et bien sûr, quand ces personnes se promènent en ville, elles se font
régulièrement contrôler par la police et elles reçoivent des amendes
pour séjour illégal. Ces amendes peuvent s'accumuler. On a par exemple
le cas d'une personne qui a reçu neuf amendes. Elle en a eu pour plus de
4000\ CHF l'aide d'urgence, soit 10\ CHF par jour.

Quand ces personnes reçoivent les amendes, elles s'adressent souvent
d'abord à l'ORS --- l'entreprise qui gère leurs foyers. L'ORS leur dit
d'accepter l'amende\index[tags]{amende} et de la payer petit à petit. Certaines personnes
sont venues nous voir avec leurs amendes et on a commencé à faire
opposition avec elles. Quand tu fais opposition et que tu ne paies pas
une amende\index[tags]{amende}, c'est le service des probations qui trouve d'autres manières
de te la faire "payer". En gros, tu reçois un formulaire avec deux
choix : soit tu vas en prison, soit tu travailles (des jours-amendes).
Avec la personne qu'on accompagnait, on a coché la case travail. On
s'est dit que ça n'allait pas marcher puisqu'il fallait mentionner le
permis de séjour de la personne alors qu'elle n'en avait pas, pour
prouver qu'elle pouvait bien travailler légalement en Suisse. Sauf
que... ça a marché ! Cette personne était tellement heureuse. Après des
centaines, des milliers de jours dans ce foyer\index[tags]{foyer}, c'est une mort
intellectuelle, affective. Et là, elle pouvait enfin aller travailler.
Elle était contente de se rendre utile, de pouvoir\index[tags]{pouvoir} exister aux yeux des
autres. On était très étonnéexs que ça ait marché. On s'est dit que la
personne qui gérait la demande avait dû se tromper, qu'elle n'avait
peut-être pas vu. L'amende\index[tags]{amende} suivante arrive, on tente encore le coup, on
refait opposition et bim : ça marche de nouveau. Ça a marché comme ça
quatre ou cinq fois. De cette manière, la personne qu'on accompagnait a
pu bosser dans des maisons de retraite ou dans la cuisine\index[tags]{cuisiner/cuisine} de la mensa de
l'université, par exemple. Ensuite, on s'est dit qu'il fallait vraiment
qu'on dénonce cette absurdité\index[tags]{absurde/absurdité}, qu'on fasse quelque chose pour que ces
personnes arrêtent de recevoir des amendes. Donc on a monté une petite
action, on en a parlé avec le Conseil d'État, qui voyait bien que
c'était absurde\index[tags]{absurde/absurdité}, on en a parlé avec un ancien juge aussi, qui trouvait
aussi absurde\index[tags]{absurde/absurdité} qu'on donne des amendes de 600\ CHF à des personnes qui ont
10\ CHF par jour pour vivre.

Donc voilà où on en est : on voulait dénoncer publiquement l'absurdité
de la situation de ces personnes que la Suisse refuse sur son
territoire, mais qu'elle autorise à bosser. On voulait en parler dans
les médias. Mais quand on a réalisé à quel point ça peut faire du bien
de travailler plutôt que d'être dans un foyer\index[tags]{foyer}, alors on s'est dit
"surtout pas". Et on n'a rien dit.

[^95]: *L'absurdité de devoir prouver sa vie*\ \[n^o^\ 50\] a été écrit par une membre du même collectif et donne un autre aspect des rencontres avec les autorités et des réglementations qui en émanent.

