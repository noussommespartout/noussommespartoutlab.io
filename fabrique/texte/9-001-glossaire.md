---
title: Glossaire
meta: true
withoutindent: true
num: "0-3"
---

**Abolitionnisme**\
Selon les contextes, voir "anticarcéral" ou "féminisme abolitionniste".

**ACAB, 1312**\
ACAB, acronyme de l'anglais *All Cops Are Bastards* (Tous les flics sont
des salauds) est un slogan antipolice. L'acronyme est parfois remplacé
par le nombre 1312, en référence à la position des lettres A, C, A, B
dans l'alphabet latin. Il est arrivé que des personnes l'utilisant
soient condamnées pour outrage à agent. Plusieurs déclinaisons ont été
proposées, parfois pour contourner cette censure (*All Cats Are
Beautiful*) ou inclure d'autres luttes (*All Colours Are Beautiful* ;
*All Clitoris Are Brave*, etc.).

**Action directe**\
Dans l'usage militant, l'action directe désigne le fait d'agir
immédiatement sur l'ordre des choses par opposition aux voies politiques
institutionnelles, représentatives et étatiques qui constituent des
actions "indirectes". L'action directe peut avoir une acception plus ou
moins large selon l'interprétation que l'on en fait : squat, occupation,
désobéissance civile, collages, sabotage, collectifs ouvriers et
autogérés, permanences juridiques gratuites, etc. Suivant certains
usages, on peut utiliser le terme pour désigner uniquement les actions
illégales et/ou considérées comme violentes.

**Adelphes et adelphité**\
Du grec *adelphos* (utérin), l'adelphité désigne le lien entre des
personnes qui ont les mêmes parents, indépendamment de leur genre. Le
terme est utilisé comme un mot non genré permettant de dire à la fois
"sororité°" et "fraternité". Dans le féminisme queer°, il est utilisé
pour regrouper, dans ce même sentiment d'appartenance, les personnes non
binaires.

**Alliéex**\
Les alliéexs sont les personnes qui soutiennent et parfois participent à
des luttes contre des formes de domination par lesquelles elles ne sont
pas directement concernées. Le terme "alliéex" sert à mettre en évidence
le fait que c'est aux personnes concernées par une domination de mener
une lutte et que c'est à elles de s'exprimer, de s'autodéterminer et de
décider ce dont elles ont besoin. Un homme cis°, par exemple, peut être
à l'écoute, s'informer, amorcer un processus de déconstruction, soutenir
et venir en aide selon les besoins exprimés par des groupes féministes,
mais ce ne sera pas à lui de déterminer comment les luttes féministes
doivent être menées. Il en va de même pour les personnes blanches face
aux luttes antiracistes, les personnes cishétéros face aux luttes
LGBTQIA+°, les
personnes valides face aux personnes vivant avec un handicap, etc.

**Anticarcéral**\
Désigne largement la constellation de mouvements, de textes et de
pratiques luttant pour l'abolition du système carcéral donc de la prison
ou encore des centres de renvoi dans le domaine de l'asile. Le terme
anticarcéral sert souvent à expliciter qu'un mouvement n'a pas une
approche réformiste° (améliorer les conditions d'emprisonnement), mais
révolutionnaire (détruire le principe même de l'emprisonnement). Ces
luttes sont également un vivier de réflexions sur les formes
alternatives de justice (voir dans ce glossaire : "Justice
réparatrice"). Dans l'usage, beaucoup de mouvements se désignent comme
"abolitionnistes" (sous-entendu, de la prison), ce qu'il ne faut pas
confondre avec d'autres usages du terme (abolitionniste du travail du
sexe° par exemple --- voir dans ce glossaire "Féminisme abolitionniste").

**Assemblée citoyenne**\
L'assemblée citoyenne est un concept dont la définition politique varie
selon les contextes. Le principe de base est de créer une institution
permettant aux individus de s'exprimer et d'exercer directement le
pouvoir politique, par exemple sur la gestion d'un territoire ou des
enjeux législatifs. Les applications varient grandement : regroupements
consultatifs et sans pouvoir de citoyennexs initiés par les
gouvernements ou assemblées de citoyennexs qui s'auto-organisent pour
renverser les mêmes gouvernements. Le terme a pu être utilisé pour
désigner des formes autogestionnaires comme les assemblées de lutte dans
les usines, des réformes constitutionnelles visant à l'instauration de
mécaniques démocratiques directes ou encore des propositions émanant des
théoriciennexs de l'holacratie ou de la sociocratie.

**Auto-organisation / autogestion**\
L'autogestion désigne le fait, pour un groupe de personnes ou une
structure, de se gérer collectivement, sans l'intermédiaire décisionnaire
d'un gouvernement ou d'une hiérarchie. L'autogestion est un mode
d'organisation qui implique notamment l'abolition de toute forme de
hiérarchie, la recherche du consensus comme mode de décision, la
collectivisation des richesses, des savoirs, des droits et des devoirs.
En théorie comme en pratique, l'autogestion implique souvent le refus,
ou du moins la réduction, des fonctions représentatives dans une
organisation.

**Binarité de genre**\
Voir "non-binarité, personnes non binaires".

**Black hat**\
Les *black hats* sont des hackeureuxses qui agissent dans l'illégalité.
Il est difficile d'établir d'autres traits communs liés au terme : les
*black hats* peuvent agir pour des buts et des bords politiques très
variés. Dans certaines terminologies, la notion de *black hat* désigne
des personnes agissant avec de "mauvaises intentions", une axiologie
évidemment refusée dans les cercles hacktivistes politisés : abolir la
propriété intellectuelle, est-ce une "mauvaise intention" ?

**Black bloc / bloc radical / bloc révolutionnaire**\
Le *black bloc* est une forme d'organisation collective essentiellement
mise en place durant des manifestations. Son nom vient de l'adoption de
vêtements noirs qui visent à uniformiser les participanxtes pour les
rendre plus difficiles à identifier. Fréquemment, le bloc émerge comme
une partie plus "violente" et déterminée d'un cortège pacifique. Ce
choix tactique conteste le monopole étatique de la violence légitime et
permet aux militanxtes de résister activement à la répression, par
exemple en ne se laissant pas interpeller et arrêter. Dans le
vocabulaire militant, d'autres usages comme "bloc radical" ou "bloc
révolutionnaire" désignent une idée similaire et l'on observe des
variations thématiques comme *green bloc°* (dans les manifestations
écologistes), *pink bloc* (féminisme/queer°) ou même *clown bloc*.

**Body positive**\
Les discours et les mouvements *body positive* luttent contre le *body
shaming°*, la grossophobie et toutes les formes de discrimination
causées par les normes sociales liées au corps et les canons de beauté
dominants. Les courants *body positive* permettent de montrer que la
beauté est une construction sociale et mettent en valeur la pluralité
des apparences corporelles : aisselles et jambes poilues, vergetures,
cheveux blancs, calvities, rides, bourrelets, cicatrices, etc.

**Body shaming**\
Le *body shaming* désigne le fait de moquer, critiquer ou humilier une
personne sur la base de ses caractéristiques corporelles (poids, formes,
taille, etc.), lorsque celles-ci n'obéissent pas aux critères
socialement normés de la "beauté". Ce phénomène a été exacerbé par
l'usage des réseaux sociaux.

**Care**\
*Care* est un terme anglais qui peut être traduit en français de
différentes façons : soin, attention à autrui, sollicitude, souci,
bienveillance. Issu des remises en question féministes de l'utilitarisme
en psychologie, il s'est depuis étendu à d'autres disciplines (économie
du *care*, philosophie du *care*, etc.). Il est utilisé très diversement
pour désigner, par exemple, les emplois ayant pour fonction le soin à la
personne ou le rapport à soi et aux autres dans un collectif. De manière
générale, il est souvent connoté par le désir d'un renversement de
certaines valeurs capitalistes, patriarcales° et modernistes comme
l'efficacité, la productivité, la virilité, le jusqu'au-boutisme.

**Consensus d'action**\
Un consensus d'action est un cadre posé par les organisateurixes et/ou
les participanxtes d'une action pour en définir les modalités théoriques
et pratiques. Il peut par exemple spécifier le déroulé de l'action, le
comportement souhaité des participanxtes, le recours ou non à des
actions illégales/violentes, les risques juridiques encourus par les
participanxtes ou la ligne de défense publique à adopter. Les consensus
d'action servent à constituer un cadre dans lequel chaque militanxte
sait à quoi s'attendre, afin de pouvoir se préparer pragmatiquement,
matériellement et psychologiquement à l'action.

**Contre-média**\
Terme générique utilisé pour désigner les médias alternatifs et
politisés diffusant une information différente de celle des médias
*mainstream* et institutionnels. Dans l'usage francophone, le terme de
contre-information est plutôt utilisé par les mouvements d'extrême
gauche et/ou révolutionnaires, alors que les courants réactionnaires
et/ou fascisants parlent plus volontiers de "réinformation".

**Copwatching**\
Le *copwatch* ou *copwatching* désigne l'action de filmer la police dans
l'exercice de ses fonctions, dans une perspective militante. On peut
filmer la police pour prévenir les violences policières en lui montrant
qu'elle est observée, pour créer un rapport de force avec l'autorité,
pour documenter des violences policières à des fins de publicisation et
comme arme juridique, etc.

**Critical mass**\
Une *critical mass* est une manifestation à vélo (ou tout autre moyen de
locomotion non motorisé) qui se déroule simultanément, chaque dernier
vendredi du mois, dans des centaines de villes du monde et qui vise à se
réapproprier les rues et l'espace public. Le mouvement, étroitement lié
aux luttes écologistes, a commencé en 1992 à San Francisco. Une
*critical mass* n'est pas une organisation à proprement parler, il n'y a
pas de leader et elle est généralement auto-organisée puisqu'il s'agit
simplement, grâce à un rendez-vous régulier, de constituer une masse
critique permettant d'occuper l'espace urbain et de créer des
embouteillages.

***DDoS attack* (verbe : *ddosser*)**\
*Distributed Denial Of Service*. L'acronyme désigne une forme d'attaque
numérique durant laquelle celleux qui attaquent cherchent à saturer le
serveur ciblé (souvent un site internet en particulier) en le forçant à
gérer un trop grand nombre de connexions. Par analogie, on peut
rapprocher ce type d'attaque de l'occupation d'un bâtiment par un groupe
de personnes suffisamment grand pour le rendre inutilisable. L'usage
*hacktiviste* en fait parfois un verbe : je ddosse, tu ddosses, iel
ddosse, nous ddossons, etc.

**Désarrêter**\
Néologisme désignant le fait d'intervenir pour libérer une personne qui
vient d'être arrêtée par la police, par exemple en encerclant un fourgon
jusqu'à ce que les personnes arrêtées soient libérées ou en engageant
une confrontation physique avec la police pour qu'elle relâche une
personne menottée.

**Détèrex**\
Contraction de "déterminéex". Souvent utilisé comme un synonyme de
"radical" ou de "prêxtes à militer violemment s'il le faut".

**Doxxing**\
Le *doxing* ou *doxxing* dérive du mot "docs" (documents). Il fait
référence à l'acte de rendre publics sur le net des documents qui
dévoilent une identité ou des actions préalablement inconnues. C'est
notamment une pratique visant à révéler des informations personnelles
concernant des ennemis, par exemple en affichant publiquement qu'une
personne participe à des événements néonazis, a tenu des propos racistes
ou a perpétré des agressions sexuelles.

**Dublin (Réglementation de)**\
Réglementation sur le droit d'asile au sein de "l'espace Dublin"
(composé des 27 États de l'UE plus la Suisse, le Liechtenstein, la
Norvège et l'Islande) dont la dernière version est entrée en vigueur en
2013. Les accords sont notamment critiqués parce qu'ils stipulent
"qu'une demande d'asile ne peut être examinée qu'une seule fois à
l'intérieur de l'espace Dublin : dans le pays de première
arrivée[^181]". Cela permet à un pays sans frontières maritimes comme
la Suisse, de renvoyer aisément touxte requéranxte d'asile vers le pays
par lequel iel est entréex en Europe. "Sur la période 2009-2014, la
Suisse est sortie clairement vainqueur de Dublin : elle a en effet
renvoyé 19'517 requéranxtes d'asile, mais les autres États ne lui en
ont renvoyé que 2523[^182]".

**Dumb phones**\
Par opposition au *smartphone*, le *dumb phone*, littéralement
"téléphone stupide", est un téléphone portable de première génération
ou aux fonctionnalités basiques, qui n'a pas (toujours) la possibilité
de se connecter à internet.

**Écocide / écocidaire**\
Terme désignant tout acte de destruction des écosystèmes. La formulation
en -cide, calquée sur génocide, vise à la formation d'un terme qui
connote ces actions comme explicitement criminelles et/ou meurtrières.

**Écoféminisme**\
Terme généralement utilisé pour désigner la convergence des enjeux
écologistes et féministes. Très souvent, la critique sociale
écoféministe insiste sur l'intrication de la domination patriarcale, de
la destruction des écosystèmes et du colonialisme, tant sur les plans
économiques et juridiques que dans les représentations et les
imaginaires (la masculinité dominante se définissant notamment par la
domestication de la nature sauvage). Le terme peut aussi être utilisé
pour se référer spécifiquement au mouvement qui s'est autodésigné comme
écoféministe, né aux États-Unis au milieu des années 60, lié notamment
aux luttes contre l'énergie nucléaire et la pollution industrielle de
masse.

***Empowerment* / empouvoirement**\
*Empowerment*, en français empouvoirement, encapacitation ou encore
autonomisation sont des termes qui désignent le processus par lequel une
personne ou un groupe social, par un mouvement d'affranchissement,
reprend le pouvoir sur ses conditions économiques, politiques,
identitaires, sociales ou culturelles. Dans ce terme, la notion de
pouvoir s'articule à celle d'apprentissage qui permet, justement, d'y
accéder. On peut utiliser *empowered* pour désigner une personne ou un
groupe social dont le processus d'*empowerment* aurait abouti.

**Femmes\***\
Le terme "femmes" suivi d'un astérisque est une convention
typographique notamment utilisée, dans le contexte suisse romand, par la
Grève Féministe, qui souligne la volonté d'inclure dans ce mot toute
personne qui n'est pas un homme cisgenre. Cette manière d'inclure les
identités de genre minorisées est toutefois critiquée parce que l'on
considère que cela continue d'invisibiliser les hommes trans, les
personnes non binaires et toutes les personnes qui ne veulent pas
résumer leur identité de genre à un terme issu de la binarité. À noter
qu'en anglais, le terme *femme* est parfois utilisé dans un but
similaire, considéré comme plus inclusif que *woman*.

**Féminisme abolitionniste**\
Le plus souvent, lorsqu'il n'y a pas de précision, la locution fait
référence aux courants féministes qui militent pour l'abolition du
travail du sexe° en cherchant notamment à criminaliser les
travailleureuxses et/ou les clienxtes. Fréquemment, cette position
s'étend à l'ensemble des métiers de l'industrie du sexe.

**FTP**\
Acronyme de *Fuck The Police*. Il a connu de nombreuses déclinaisons,
souvent pour contourner des procès pour insulte à agent : *Feed The
People*, *Fight The Power*, etc.

**Gentrification**\
La gentrification est un processus de transformation de la
composition sociale et démographique d'un quartier populaire urbain ou
périurbain à l'arrivée de ménages plus aisés qui y importent des modes
de vie ou de consommation différents. Le terme vient de l'anglais
*gentry*, bourgeoisie, et il est parfois traduit par "embourgeoisement"
ou encore par "boboïsation".

**Green bloc**\
Mot apparu dans les luttes écologistes pour désigner la formation de
groupes d'actions "violentes" anonymes dans les manifestations pour le
climat, formés sur le modèle du *black bloc°* (de tradition plus
généralement anarchiste, syndicaliste et anticapitaliste).

**Hackathons**\
Rencontres entre personnes de la communauté *hack*, souvent sous la
forme de festivals où sont organisés des conférences et des concours
visant à développer des connaissances et des compétences techniques dans
le domaine de la programmation informatique.

**Hackerspaces**\
Espaces ou locaux constituant des ateliers de formation au *hack* ou aux
pratiques numériques en général.

**Hommes cis / mecs cis / cisgenres**\
L'adjectif cisgenre, abrégé en "cis" est un néologisme désignant un
type d'identité de genre° où le genre ressenti d'une personne
correspond à celui qui lui a été assigné à sa naissance. Le mot est
construit par opposition à celui de transgenre°. Lorsqu'on parle de mecs
cis ou d'hommes cisgenres, on parle de personnes qui, lorsqu'elles sont
nées, ont été assignées au genre masculin et se reconnaissent dans ce
genre. S'agissant d'une position dominante dans les systèmes
patriarcaux°, les hommes cis bénéficient de privilèges par rapport aux
autres identités de genre.

**Identité de genre**\
L'identité de genre est initialement un concept sociologique qui désigne
le sentiment d'appartenance à un genre en dehors de toute considération
biologique. Ce terme permet de se défaire de la biologie, de considérer
que les genres sont des constructions sociales et donc de se baser sur
le ressenti d'une personne pour définir son genre. L'identité de genre
est à distinguer de l'orientation sexuelle qui désigne le genre
des personnes par lesquelles on est sexuellement et/ou amoureusement
attiré.

**Incels**\
Abréviation de l'anglais *Involuntary Celibate*. La sous-culture incel
est constituée de communautés en ligne misogynes et masculinistes dont
les membres se présentent comme étant incapables de trouver une
partenaire amoureuse ou sexuelle, état qu'ils décrivent comme un
célibat involontaire ou *inceldom*. Ceux qui se proclament *incels* sont
presque exclusivement des hommes cis hétérosexuels. La communauté *incel*
rassemble des profils divers, généralement unifiés derrière l'idée que
le féminisme les a privés de leur "droit naturel masculin" sur les
femmes. Ces communautés sont particulièrement présentes aux États-Unis,
où certains *incels* ont commis des actes terroristes faisant plusieurs
victimes.

**Infokiosque**\
Un infokiosque peut prendre la forme d'un étal éphémère --- voire mobile ---
de brochures, publications imprimées et de zines, ou la forme plus
organisée d'une librairie indépendante, militante et *Do It Yourself*
prônant la diffusion autonome, l'autoéducation et l'autoproduction.
L'infokiosque est un moyen d'action politique qui permet de mettre en
commun les expériences militantes, les savoirs pratiques, théoriques et
émotionnels en dehors des logiques marchandes, les brochures étant
généralement gratuites ou à prix libre, pour que l'éducation politique
fasse partie du bien commun. Les brochures des infokiosques sont
photocopiées à l'aide de matériaux les moins chers possibles. L'une des
principales ressources en ligne pour les infokiosques francophones est
le site Infokiosque.net.

**Intersectionnalité**\
L'intersectionnalité est une notion employée en sociologie et en
théorie politique pour désigner la situation de personnes subissant
simultanément plusieurs formes de dominations dans une société. Le terme a été proposé par
l'universitaire et militante afroféministe américaine Kimberlé Williams
Crenshaw en 1989 pour parler spécifiquement de l'intersection entre le
sexisme et le racisme subis par les femmes afro-américaines et expliquer
pourquoi ces femmes n'étaient pas prises en compte dans les discours
féministes de l'époque.

**IRC**\
*Internet Relay Chat*. Protocole de communication textuelle sur internet
qui désigne par extension des forums ou tchats en circuit court
utilisant ce protocole, parfois hébergés en dehors du web grand public.

**IRL**\
*In Real Life*. Acronyme utilisé dans le cyberespace pour désigner la
vie non numérique.

**Justice restaurative ou réparatrice ou transformatrice**\
Les justices restaurative, réparatrice et transformatrice proposent des
modes alternatifs de résolution de conflits. Les justices s'opposent à
la justice punitive (qui répond aux délits ou aux crimes par des
punitions : emprisonnement, amendes, etc.) par un travail impliquant
l'inclusion de toutes les parties impliquées dans le conflit. Ces
justices participatives, fondées sur le dialogue, visent la guérison, la
réparation et la réinsertion sociale. Certaines formes maintiennent
l'idée de la punition, d'autres la récusent. Elles s'accompagnent le
plus souvent d'une réflexion sur la nécessité de remplacer la justice
étatique par des formes de justice populaires et autogérées. Le Chiapas zapatiste est un exemple de société anticarcérale°
fonctionnant à large échelle sans justice punitive.

**LGBTQIA+**\
LGBTQIA+ est un acronyme rassemblant différentes orientations sexuelles,
identités de genre° et communautés qui ne sont pas incluses dans le
modèle hétéronormé et patriarcal° de nos sociétés : Lesbienne, Gay,
Bisexuelle, Transgenre, Queer, Intersexe, Asexuelle. Le signe "+" sert
à désigner la diversité des orientations sexuelles et des identités de
genre : pansexualité, personnes agenréexs, *genderqueer*, bigenres, etc.

**Mansplaining, mansplainer**\
Le *mansplaining* est un concept féministe qui désigne le comportement
d'un homme cisgenre expliquant à une femme ou à une personne non binaire
des choses sur lesquelles iel est objectivement plus compétenxte, sur un
ton condescendant ou paternaliste. Le *mansplaining* s'explique par
l'éducation et la socialisation dont bénéficient les hommes cisgenres
dans des contextes et systèmes basés sur le patriarcat°. En français
québécois, le terme est traduit par "pénispliquer".

**Mégenrer**\
Mégenrer est l'acte d'attribuer un genre à une personne dans lequel elle
ne se reconnaît pas. C'est lorsque, par exemple,
on continue d'utiliser le pronom "il" et d'accorder les verbes et les
adjectifs au masculin pour parler d'une personne qui a transitionné° et
qui parle d'elle-même au féminin.

**Mixité choisie ou non-mixité**\
La mixité choisie est une pratique qui consiste à organiser des
rassemblements réservés aux personnes appartenant à un ou
plusieurs groupes sociaux opprimés ou discriminés, en excluant la
participation de personnes appartenant à d'autres groupes
structurellement discriminants (ou oppressifs), afin de minimiser la
possibilité de reproduire les schémas de domination sociale. Cette
pratique est un outil militant notamment utilisé dans le féminisme,
l'antiracisme, les mouvements LGBTQIA+, les mouvements de minorités
de genre ou de personnes en situation de handicap. Voir aussi *safe
space°.*

**Misogynie / sexisme intégrée / intériorisée**\
La misogynie intégrée est une forme de sexisme ordinaire non idéologique
qui regroupe des pensées et des comportements intériorisés par les
femmes envers les autres femmes. Les femmes ayant très souvent intégré
le rôle que la société patriarcale° leur assigne, cette forme de sexisme
est particulièrement tenace et difficile à déconstruire. Le sexisme
intégré pousse souvent à minimiser voire à mépriser l'importance des
luttes féministes et ses actions associées et prend également des formes
plus quotidiennes comme le dégoût spontané pour des aisselles non
épilées, le mépris pour ce qui est considéré comme "trop *girly*" ou
encore des sentiments de jalousie plus exacerbés vis-à-vis d'autres
femmes. On peut également parler de racisme intégré, d'homophobie
intégrée, de transphobie intégrée, etc.

**Nasse / Nasser**\
Il s'agit à l'origine d'un terme de pêche : la nasse est un panier
d'osier ou de fil de fer en forme d'entonnoir servant à prendre
du poisson. Dans le vocabulaire militant, la nasse fait référence à
une technique policière visant à immobiliser ou à encadrer un groupe de
personnes. La police peut
nasser un groupe en utilisant de la rubalise ou en l'entourant d'un
cordon de policiers. La nasse est une pratique de maintien de l'ordre
extrêmement répandue que l'on critique pour son atteinte aux libertés
fondamentales comme le droit de manifester.

**Native**\
Terme anglais désignant des groupes culturels originaires d'un endroit
en particulier. Selon les usages activistes et les traditions
idéologiques, il est parfois traduit par "indigène" ou "autochtone" en
français. Dans l'usage militant, il est souvent utilisé pour désigner
les luttes des populations présentes sur un territoire antérieurement à
sa colonisation, qu'il s'agisse de luttes autonomistes, de récupération
du territoire traditionnel ou pour la reconnaissance des droits des
minorités.

**Non-binarité / personnes non binaires**\
La non-binarité est un concept utilisé pour désigner la catégorisation
des personnes, dites non binaires ou *genderqueer*, dont l'identité de
genre ne s'inscrit pas dans la norme binaire, c'est-à-dire qui ne se
sentent ni homme ni femme, mais entre les deux, un "mélange" des deux,
ou aucun des deux. Cette identité s'oppose à la binarité de genre et à
la hiérarchie des genres qui l'accompagne dans un système patriarcal°.
Elle remet aussi en cause l'assignation sexuelle à un genre donné.

**Papiers blancs**\
Les personnes déboutées par le système d'asile en Suisse n'ont, pour
seul papier, non pas un permis de résidence, mais une attestation de
délai de départ (appelée "papier blanc"), qu'elles doivent faire viser
régulièrement par les autorités cantonales en charge de la migration. "
Ce papier n'octroie aucune protection légale mais permet l'accès à
l'aide d'urgence. Bon prince, l'État ne laisse pas crever les
personnes qu'il a illégalisées, du moins en théorie. En attendant de les
expulser, il leur octroie l'aide d'urgence qui consiste en un
hébergement avec, soit des plateaux-repas, soit un soutien financier de
dix francs suisses par jour[^183]."

**Patriarcat**\
Utilisé autrefois pour qualifier la domination du patriarche sur le
reste de la famille (femme, enfants, esclaves, employéexs, etc.), le
patriarcat désigne aujourd'hui plus généralement l'organisation sociale,
politique et juridique basée sur la détention de l'autorité et du
pouvoir par les hommes cisgenres et les formes de discrimination qui en
découlent.\
Dans les théories féministes, ce concept a été utilisé pour désigner et
analyser la spécificité de l'oppression des femmes face à d'autres
types d'oppression (classisme, racisme, etc.). Les mouvements
féministes intersectionnels° contemporains mettent l'accent sur
l'intrication du patriarcat avec les autres formes de domination.

**Permis de réfugiéexs**\
Les personnes qui demandent l'asile en Suisse se voient accorder un statut
juridique et des droits différents selon l'issue de la procédure :\
- le permis N de requéranxtes d'asile est une confirmation que la personne
  a demandé l'asile en Suisse et qu'elle attend une décision du Secrétariat
  d'État aux Migrations (SEM) ;
- le permis B de réfugiéex reconnuex est octroyé lorsqu'une personne requérante
  d'asile a expliqué de "manière vraisemblable et justifiée qu'elle subit dans
  son pays d'origine des persécutions relevant du droit d'asile au sens de la
  Convention de Genève[^184]", elle se voit alors accorder l'asile ;
- le permis F d'admission provisoire en tant que réfugiéex est octroyé
  lorsqu'une personne remplit les conditions requises pour obtenir le statut en
  vertu du droit international, mais voit sa demande d'asile rejetée par le SEM
  en vertu de la loi suisse sur l'asile. D'un point de vue formel, le SEM
  ordonne alors l'expulsion de la Suisse. Toutefois, pour des raisons de droit
  international, l'expulsion est illicite et la personne est donc acceptée
  provisoirement comme réfugiée en Suisse ;
- le permis S est un statut juridique qui a été introduit afin de pouvoir
  réagir de manière appropriée, rapide et pragmatique à des situations d'exode
  massif. Les "personnes à protéger" reçoivent le droit à un séjour provisoire
  en Suisse. Ce statut n'a jusqu'à présent jamais été utilisé ;
- si la personne requérante d'asile ne subit pas dans son pays d'origine des
  "persécutions pertinentes en matière de droit d'asile" et aucun motif ne
  s'oppose à son renvoi dans son pays d'origine, le SEM ordonne l'expulsion.
  Elle reçoit alors une attestation de délai de départ (voir : Papiers
  blancs°).

**Phishing**\
Contraction des mots anglais phreaking, désignant le piratage de
lignes téléphoniques, et fishing, "pêche". Il s'agit d'une
technique frauduleuse utilisée pour récupérer des informations
(généralement bancaires) auprès d'internautes. Le plus souvent, le
*phishing* consiste à imiter l'identité d'une entité d'émission plus ou
moins fiable (comme une banque ou une entreprise) pour faire croire
quelque chose à la victime, la poussant à donner de l'argent ou des
informations.

**Précariat**\
Terme utilisé pour décrire la "classe" de personnes vivant en
situation précaire. On dit parfois qu'il a remplacé, dans les
démocraties contemporaines occidentales, le terme de "prolétariat".
Dans cette optique, le terme désigne une réalité spécifique : si l'on
constate un accroissement de la "classe moyenne", on assiste à
l'émergence d'une classe précaire qui n'est plus aussi homogène (lieu de
travail, conditions de vie etc.) que ne pouvait l'être la classe
"ouvrière". L'idée de précariat cherche généralement à désigner un
ensemble de réalités très diverses : étudianxtes, sans-papièrexs,
travailleureuxses ubériséexs, etc.

**Prix libre**\
À ne pas confondre avec la gratuité, le prix libre est un mode de
fixation du prix par les personnes qui achètent. L'objet ou le service
n'a pas de prix fixé par la personne qui le propose. Le prix libre
permet de donner accès à quelque chose en fonction des moyens de
l'acheteureuxse et de sortir de la logique de la valeur de marché
uniquement. Le prix de revient ou un prix conseillé peut parfois être
affiché. Le prix libre peut inclure ou non la possibilité de gratuité de
l'objet ou du service.

**Pro-sexe**\
Le féminisme pro-sexe naît aux États-Unis dans les années 80,
principalement en réponse aux féminismes abolitionnistes° du travail du
sexe° et de la pornographie. Le mouvement pro-sexe met en avant la
libération sexuelle, la déconstruction des stéréotypes de genre dans la
sexualité et la réappropriation positive de la sexualité en considérant
toute activité sexuelle consentie comme fondamentalement saine et
plaisante. Les enjeux sont de politiser la sexualité en déconstruisant
ses dynamiques patriarcales°, d'abolir la primauté de l'orgasme
masculin, les discriminations au sein des relations sexuelles et de
mettre en avant le consentement, l'acceptation des désirs et le sexe
sans risques. Voir aussi *Sex-positive*°.

**Queer**\
Queer est un anglicisme signifiant littéralement "étrange" ou
"bizarre". À l'origine injure homophobe et transphobe, les minorités
concernées se sont réapproprié ce mot au début des années 1990 en lui
attribuant une connotation positive. Il est utilisé pour désigner
l'ensemble des personnes ayant une orientation sexuelle° autre que
l'hétérosexualité ou se reconnaissant dans une identité de genre°
différente de la cisidentité°.

**Racisation / personnes racisées**\
La race n'existe pas comme essence ou nature distinctive biologique de
tel ou tel groupe humain. Par contre, des individus et des groupes de
personnes font l'objet de processus de racisation (ou racialisation),
liés à des contextes culturels, sociaux et économiques. Lorsqu'on
utilise ce terme comme un adjectif (une personne racisée), on désigne
des personnes qui subissent le racisme systémique qui découle de cette
assignation raciale, de la race en tant qu'elle existe comme
représentation culturelle partagée.

**Rooter**\
Désigne, en informatique, le fait de relier ensemble deux éléments, ou
de ramener un système à sa racine (*root*).

**Safe Space**\
Un *safe space*, littéralement "espace sûr", est un lieu qui entend
permettre à des personnes marginalisées et opprimées de se réunir dans
un environnement sûr et bienveillant, qui tente d'instaurer un cadre
dépourvu des formes de dominations habituelles. L'idée de *safe space*
désigne souvent un processus d'autocritique permanent visant à
réfléchir à la manière dont se reproduisent des oppressions dans des
espaces temporaires ou permanents. Les *safe spaces* émergent dans les
années 60 aux États-Unis au sein des milieux LGBTQIA+. Cette notion est
étroitement liée à la mixité choisie°.\
Nombre de collectifs s'accordent à dire que le *safe space* est un idéal
critique vers lequel tendre plutôt qu'une réalité objectivement
observable, c'est pourquoi on parle parfois de *safer space* (espaces
plus sûrs). Aussi, l'idée d'un *safe space* tend à universaliser
l'expérience de chacunex : un espace peut être plus *safe* pour des
femmes cis° blanches sans forcément l'être pour des femmes cis noires
par exemple.

**Sex-positive**\
Apparu dans les années 60 et lié au féminisme pro-sexe° dans le sens où
il se place également en opposition à la vague antisexe et au féminisme
abolitionniste°, le mouvement *sex-positive* vise à déconstruire les
hiérarchisations morales associées à la sexualité, souligner la
diversité des expressions sexuelles et mettre au centre de la sexualité
le droit au plaisir, le *body positivisme°* et le consentement.

**Sit-in**\
Le *sit-in* est un type de manifestation dans lequel des personnes
s'assoient pacifiquement pour bloquer un espace, comme une rue ou un
bâtiment.

**Socialisation, socialiséex**\
En sociologie, la notion de socialisation désigne les processus par
lesquels un individu acquiert et intériorise les normes, les rôles et
les valeurs qui structurent la vie sociale en construisant ainsi son
identité sociale et psychologique. La socialisation se fait à travers
des instances multiples dont l'éducation ou le contact avec les pairs.
Le concept de socialisation différenciée ou socialisation de genre
permet d'expliquer la transmission et l'intériorisation des normes et
codes sociaux relatifs au masculin et au féminin ainsi que le
développement des identités de genre qui s'y rapportent.

**Sororité**\
Le terme sororité provient du terme latin *soror*, qui signifie sœur ou
cousine. La sororité est la solidarité entre femmes. Elle désigne les
liens entre les femmes qui se sentent des affinités, ont un vécu partagé
en tant que personnes socialisées femmes et au statut social qui y est
lié. L'équivalent inclusif et non binaire° de sororité est adelphité°.

**Street medic**\
Les *street medics* sont des personnes qui fournissent une aide médicale
de premiers secours dans un contexte de lutte politique (manifestations,
squats, etc.).

**Suffragettes**\
Le terme suffragettes désigne les mouvements du début du XX^e^\ siècle en
faveur du droit de vote des femmes au Royaume-Uni. Le terme prend son
origine dans la *National Union of Woman's Suffrage Societies (NUWSS)*,
fondée en 1897 par Millicent Fawcett en Angleterre. Issu majoritairement
de classes bourgeoises blanches, le mouvement a essaimé dans des classes
sociales et pour des causes diverses, trouvant pour point commun la
lutte pour le droit de vote des femmes. Deux lignes de force principales
constituent le mouvement : l'approche dite modérée et
constitutionnaliste du *NUWSS* et l'action directe° représentée
notamment par la *Women's Social and Political Union (WSPU)*
d'Emmeline, Christabel et Sylvia Pankhurst.

**Spywares**\
Logiciels invisibles qui infectent les appareils numériques puis
enregistrent et transmettent les actions des utilisateurixes, par
exemple les mouvements de la souris et du clavier sur un ordinateur.

**Systémique**\
Terme issu des approches holistes/structuralistes des sciences sociales,
qui insistent sur l'influence des structures sociales, culturelles et
politiques sur les individus et les pratiques, mais aussi sur les
institutions et les représentations partagées. Il est souvent utilisé
dans le langage militant pour dégager une question d'un niveau de
problématisation individuel et l'inscrire dans des déterminations
sociales plus larges (systémiques, causées par un système et non par des
caractéristiques ou des dispositions individuelles).

**Test osseux**\
Datant de 1959, le test osseux est un moyen utilisé par les tribunaux et
les autorités, afin de déterminer si une personne est majeure. Dans le
domaine de l'asile, il est souvent utilisé pour accorder ou non la
protection aux mineurexs non accompagnéexs (puisque le test est supposé
permettre d'identifier l'âge réel d'une personne). Il s'agit de
radiographies de la main et du poignet qui sont ensuite comparées à des
planches d'images similaires dans un atlas de référence. La croissance
osseuse des personnes est évaluée subjectivement à partir des images,
c'est-à-dire à l'œil nu et sans quantification précise. Cette méthode
est scientifiquement très contestée, notamment parce qu'elle ne tient
pas compte des facteurs socioéconomiques et nutritionnels et que la
marge d'erreur des tests est d'un à deux ans pour les adolescenxtes
entre 16 et 18 ans. Elle reste pourtant très utilisée par les tribunaux
et l'administration en charge des questions migratoires.

**Torrent**\
Dans le système de distribution de fichiers BitTorrent, un fichier
torrent ou de méta-info est un fichier informatique qui contient des
métadonnées sur les fichiers et les dossiers à distribuer, et
généralement aussi une liste des emplacements réseau des *trackers*. Un
fichier *torrent* ne contient pas le contenu à distribuer ; il ne
contient que des informations sur ces fichiers, telles que leurs noms,
la structure des dossiers et les tailles obtenues via des valeurs de
hachage cryptographiques pour vérifier l'intégrité des fichiers. Le
terme *torrent* peut faire référence soit au fichier de métadonnées,
soit aux fichiers téléchargés, selon le contexte.

**Trans / transgenre / transidentités**\
On parle de transidentité, ou de transidentités, au pluriel, pour
désigner les identités pour lesquelles, contrairement à la
cis°-identité, l'identité de genre° vécue et le sexe assigné à la
naissance ne correspondent pas.

**Transition / transitionner**\
Le parcours de transition est le processus que traverse une personne
trans°, *genderqueer* ou non binaire, afin de changer d'identité de
genre. Le terme peut être utilisé pour tous les actes psychologiques,
administratifs, sociaux ou médicaux (hormonosubstitution, chirurgie de
réattribution de genre, chirurgie de réattribution sexuelle, etc.), qui
peuvent permettre à une personne de se sentir mieux dans son corps et de
s'identifier au genre auquel elle appartient. Il est important de
préciser qu'une transition n'implique pas toujours une réattribution
sexuelle ou une prise en charge médicale.

**Travail du sexe / travailleureuxses du sexe**\
Le travail du sexe (en anglais *sex work*) est un synonyme de
prostitution, mais il peut aussi englober d'autres métiers liés à
l'industrie du sexe : acteurixe dans l'industrie pornographique,
*escorting*, *camgirls/camboys*, assistanxtes sexuelles, etc. Il a été
utilisé pour la première fois par la travailleuse du sexe militante,
féministe et pro-sexe Carol Leigh dans le but de dédiaboliser le mot
"prostituéex". Ce terme permet aussi de mettre l'accent sur les droits,
les garanties sociales et les conditions matérielles qui devraient être
liées au travail du sexe comme ils le sont dans tous les autres corps de
métier.

**Troll (verbe : troller)**\
Lutin des légendes scandinaves qui traîne trop sur internet.

**Vénèrex**\
Verlan de "énervéex". Est fréquemment utilisé dans les slogans
militants pour désigner la colère et la détermination présente dans les
luttes, à l'instar du slogan féministe : "Fière, vénère et pas prête de
se taire".

**White hats**\
Les *white hats* sont des hackeureuxses agissant dans la légalité,
souvent experxtes en sécurité. Les *white hats* informent généralement
les détenteurixes des sites dont iels ont testé, et éventuellement
percé, les défenses. Il est difficile d'établir d'autres traits communs
liés à ce terme : les *white hats* peuvent agir pour des buts et des
bords politiques très différents.

**ZAD**\
Acronyme de Zone À Défendre, le terme ZAD désigne une tactique de lutte
qui s'oppose à des projets considérés inutiles, dangereux et nuisibles à
l'environnement en occupant la zone destinée à être aménagée pour
empêcher la réalisation du projet en question. Ce mode d'organisation
politique a notamment été utilisé dans la lutte contre l'aéroport de
Notre-Dame-des-Landes, le barrage de Sivens, la ferme des mille vaches
ou l'extension de la cimenterie holcim en Suisse.

**Zbeul**\
De l'arabe, \arb{زبل}, *zebl'*, fumier, ordure, désordre. Souvent utilisé dans
le vocabulaire militant pour désigner le fait d'agiter une manifestation
ou de mettre en place des actions de dégradation matérielle.

[^181]: *Rapport pour les droits et la mobilité des personnes noires africaines en Suisse et en Europe* du collectif Jean Dutoit.

[^182]: Duc-Quang Nguyen et Stefania Summermatter, \enquote{La Suisse défend l'accord de Dublin et ce n'est pas un hasard}, Swissinfo, 19.02.2016. URL : [*www.swissinfo.ch/fre/politique/politique-de-l-asile_la-suisse-défend-l-accordde-dublin-et-ce-n-est-pas-un-hasard/41969008*](https://www.swissinfo.ch/fre/politique/politique-de-l-asile_la-suisse-défend-l-accordde-dublin-et-ce-n-est-pas-un-hasard/41969008).

[^183]: *Sans retour papier blanc*, Collectif Sans Retour, 2018.

[^184]: Dans Osar.ch. URL : [*www.osar.ch/themes/asile-en-suisse/statut-de-sejour*](https://www.osar.ch/themes/asile-en-suisse/statut-de-sejour).
