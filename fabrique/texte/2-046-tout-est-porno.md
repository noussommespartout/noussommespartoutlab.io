---
author: "H & M"
title: "Tout est porno"
subtitle: "Discussion sur la pornographie alternative"
info: "Transcription d'un entretien"
date: "novembre 2020"
categories: ["sexualités", "transpédégouines, queer", "travail du sexe"]
tags: ["index"]
lang: ["fr"]
num: "46"
quote: "le porno, c'est un toboggan politique"
---

\indent--- Comment on pourrait définir le porno ?

--- Alors ça, c'est une grande question. Quand on a commencé à produire
des films, on a décidé d'utiliser le mot porno, mais c'était pas une
évidence, on en a discuté longtemps. C'est un *statement* politique, un
moment où tu touches à l'intouchable. C'est le moment où tu te dis "Ok,
on va prendre ce qui se fait, on va le refaire nous, et on va acter
qu'on milite pour quelque chose." Parce que, forcément, certains films
alternatifs peuvent très bien être considérés comme des objets d'art
vidéo ou d'art\index[tags]{art/artiste} performatif.

--- Si une personne me dit "regarde, j'ai fait un porno" et que son
film c'est deux fourmis\index[tags]{fourmi} qui marchent, eh bah ok, t'as fait un porno,
c'est valide. Dire "porno", c'est politique, c'est provocateur, c'est
dissident, et c'est aussi rejoindre un milieu, une famille. Si tu fais
ça comme un "art\index[tags]{art/artiste}", tu pourras peut-être exposer tes vidéos dans une
galerie suisse quelques semaines. Si tu appelles ça du porno, il y a des
répercussions, des conséquences, une visibilité, les médias qui
s'agitent et te donnent de la place pour parler de ton acte.

--- Il y a deux ans, j'étais dans un festival à Londres et j'ai assisté à
un débat sur la définition (et donc les limites) du porno. C'était une
table ronde, avec près de 300 personnes dans le public, à propos d'une
loi anglaise qui interdit certaines pratiques dans le porno, comme le
BDSM ou l'éjaculation féminine. Autour de la table, il y avait un
avocat, deux personnes qui performent dans des pornos et une qui réalise
des films. Iels auraient pu construire leur propos contre cette loi de
mille manières différentes, mais iels ont choisi de montrer des films
avec des fétiches : un film\index[tags]{film} avec le fétiche des masques à gaz, un autre
avec des meufs en méga talons qui appuient sur des pédales de voiture\index[tags]{voiture/bagnole}.
Il n'y avait pas trop de réactions dans le public, jusqu'à ce qu'une des
meufs de la table ronde se mette à rire\index[tags]{rire/se marrer/rigoler} et à essayer de dire aux gens
que si on pouvait considérer ça comme du porno, alors la loi était
absurde. Alors, tout le monde a commencé à se moquer de ces images et de
l'idée même que ça puisse être considéré comme du porno. Je me suis levé
et j'ai dit que c'était ultra-insultant : t'es une personne qui a un de
ces fétiches, tu te sens super mal devant une salle qui rit de toi. Que
ça puisse se passer dans un festival *sex-positive°* m'a choqué. Je
raconte cette histoire pour dire qu'on évite le débat sur ce qui est du
porno et ce qui n'en est pas. On est déjà une communauté bien petite,
n'essayons pas de créer des scissions entre nous.

--- Tout peut être du porno, si c'est l'intention de la personne qui le
produit ou qui le regarde. C'est juste la réalité. Au quotidien, on est
touxtes excitéexs par des choses qui ne sont pas masturbatoires. Essayer
d'exciter des gens avec des choses qui ne paraissent pas *porn*, c'est
un des trucs que le porno alternatif essaie de faire. Si on insiste et
qu'on continue d'appeler "pornographiques" nos films, alors qu'ils
échappent à l'imaginaire standardisé, il y aura peut-être une ouverture,
un développement de cet imaginaire, de la conception collective de ce
qui "est du porno".

--- Moi je vends du fromage, comme travail, et je peux te dire que je
fais du porno toute la journée. Dans mon cœur, la définition du porno,
c'est le potentiel excitateur des choses, pas la représentation de la
sexualité.

--- C'est dommage de vivre dans un monde où l'excitation n'est déclenchée
que par un imaginaire pornographique classique.

--- D'où la nécessité militante du porno alternatif ou éthique.

--- La communauté du porno alternatif, même au-delà de la Suisse romande,
n'est pas grande : il y en a beaucoup à Berlin, à Milan et en Espagne
aussi. Maintenant, on a des potes un peu partout, mais tu te rends vite
compte que tout le monde se connaît. Et aussi que les personnes qui font
ça sont très précariséexs, souvent de par leur appartenance à la
communauté queer° et *sex-positive*.

--- Le schéma classique que j'ai observé, c'est des gens queer, qui, au
sein du milieu queer, ont trouvé un environnement *sex-positive* dans
lequel iels se sont sentiexs bien. Et, dans cette communauté, certainexs
utilisent le porno comme un médium à la fois artistique et militant.
Quand iels commencent le porno queer, iels sont déjà précaires --- à cause
des oppressions subies --- et c'est ça qui les amène là, qui nous a
amenéexs là, même si toutes les situations sont différentes.

--- Le porno, c'est un médium politique, parce que ça touche à plusieurs
choses. Dans les positions où l'on est, il y a une certaine souffrance,
qui n'existait pas pleinement chez moi avant que je développe le
vocabulaire et les concepts pour la vivre. La souffrance passait presque
toujours dans mon inconscient. Et, à force\index[tags]{force} de déconstruire\index[tags]{déconstruire/déconstruction} la sexualité,
dans le porno, tu remarques davantage les oppressions, tu remarques
beaucoup plus de choses injustes.

--- Le porno, c'est un toboggan politique.

--- Et là on arrive au bout.

--- Dans l'eau\index[tags]{eau}.

--- Ou dans le bac à sable.

--- On arrive dans l'immensité de la politique quoi.

--- C'est un lancement, c'est le moment où tu prends de la vitesse.

--- Du coup, quitte à arriver à pleine vitesse, je préfère l'eau\index[tags]{eau} au bac à
sable...

--- Le porno éthique, c'est aussi une manière de découdre le sexe
scripté. Un bon exemple de sexe\index[tags]{sexe} scripté, c'est les applis, comme
*grindr*[^133] : le mec vient, tu discutes, et il y a un moment où on
sait tous les deux que ça va passer au sexe\index[tags]{sexe}. À ce moment-là, tu ne vois
plus la personne. En suivant ce script, j'ai l'impression de baiser avec
des robots. Le script qui se déroule en fait, c'est aussi celui du porno
gay classique. Et là, on se distancie de ce qu'on pourrait vraiment
ressentir, de ce qu'on pourrait vraiment vouloir. Avec le porno éthique,
tu fais un pas vers l'écoute, vers la déconstruction\index[tags]{déconstruire/déconstruction}, tu retrouves la
possibilité de s'écouter vraiment, d'envisager le sexe\index[tags]{sexe} autrement.

--- Il n'y a pas longtemps, j'ai été à la télévision pour parler de
porno, mais le contexte était spécifique : l'émission portait sur la
consommation de porno chez les adolescenxtes. On me demandait si le
porno éthique pouvait constituer un espoir, une solution\index[tags]{solution}, quelque chose
à développer pour pouvoir\index[tags]{pouvoir} leur proposer une alternative, des imaginaires
nouveaux.

--- C'est peut-être le futur.

--- Tu y crois ?

--- Non...

--- On n'a pas assez de moyens.

--- On se pose la question de la thune depuis le départ. C'est assez
complexe. On bosse beaucoup au feeling, on essaie d'avoir une éthique
commune et de la faire évoluer ensemble. Globalement, on est d'accord
pour refuser que nos films empruntent le circuit porno classique,
c'est-à-dire les *tubes*[^134], où l'on pourrait se faire de
l'argent. Il a donc fallu trouver une autre solution\index[tags]{solution} pour les diffuser.
On a décidé de les mettre sur notre site internet\index[tags]{internet/web}, dans un espace
protégé par un mot de passe qu'on donne gratuitement, si on nous en fait
la demande. Bien sûr, on suggère de verser un peu de thunes via paypal,
mais personne ne le fait. C'est pas grave, c'est pas ça qu'on veut.
Personne n'est éduquéex à payer pour du porno aujourd'hui et, même si on
milite pour ça, on ne veut forcer personne.

--- Un tournage c'est quand même des gens, des moyens, du matériel, des
ressources qui ne sont pas gratuites.

--- C'est clair, c'est le propre du porno éthique.

--- C'est difficile de faire une bonne scène, surtout si tu veux
respecter l'éthique que tu te fixes, qui inclut la sécurité sur le
tournage, la diversité des corps\index[tags]{corps}, l'absence de script qui met les
performeureuxses à l'aise. Il faut rajouter à ça le problème de la thune
évidemment, qui raccourcit le temps à disposition : en payant peu les
gens, tu ne peux pas leur demander de tourner quinze fois. Pour toutes
ces raisons, avec *Oil*[^135], on est davantage dans une position
d'archive-documentation que nous mettons en forme, plus que dans une
situation véritable d'écriture et d'engagement d'acteurixes.

--- Parce que la thune manque. Il faudrait convaincre le milieu du cinéma
suisse de donner.

--- Ouais, si on avait un revenu de base inconditionnel, ça changerait
tout. Je ne resterais pas dans les cases, je ferais tout exploser.

[^133]: Créée en 2009, grindr est une application de rencontre géolocalisée conçue pour les hommes homosexuels, bisexuels ou bicurieux.

[^134]: Sites web de diffusion en streaming ou en VOD comme pornhub, youtube, etc.

[^135]: *Oil production* est une association basée à Lausanne qui se définit comme un collectif fluide, incubateur de productions pornographiques.
