---
author: "Loïc Valley"
title: "Pronoms et langage"
subtitle: "Tuto pour un langage non binaire"
info: "Texte rédigé pour le recueil"
date: "mai 2020"
categories: ["DIY", "transpédégouines, queer", "féminismes, questions de genre"]
tags: ["index"]
lang: ["fr"]
num: "36"
quote: "n'assume pas les pronoms des gens"
---

## Quels pronoms existe-t-il ?

On a tendance à connaître les pronoms qu'on nous a appris à l'école :
"il" et "elle", peut-être que vous connaissez aussi un pronom neutre
relativement répandu "**iel**" parfois orthographié "**ielle**",
mais il existe des tas d'autres pronoms comme
par exemple : "**al/ul/ol/ael/yel/ille**" et plein d'autres encore !
Lorsqu'une personne vous dit son pronom, elle n'attend pas de commentaire
sur ce dernier, il vous suffit de faire des efforts pour l'utiliser[^99].

## Et si je me trompe ?

C'est une question qu'on me pose énormément et c'est
une question importante, parce qu'une erreur\index[tags]{erreur} peut arriver et qu'elle
peut être tout à fait minime, si derrière il n'y a pas une trentaine
d'heures de discussion à ce propos, je m'explique.

Si vous vous trompez, le mieux à faire est de s'excuser, de se
reprendre rapidement et de continuer. Il en va de même si quelqu'unex
vous reprend sur le pronom de quelqu'unex d'autre. Vous offusquer parce que
c'est difficile de changer n'est pas une bonne idée à ce moment-là.
Dites-vous bien qu'il est plus difficile de se faire mégenrer° au
quotidien pour les personnes concernées que pour vous de changer une
habitude de langage.

## Quelle autre chose puis-je faire pour être alliéex° ?

Dans le monde parfait de Loïc Chevalley (c'est moi, qui écris depuis
tout à l'heure), toutes les personnes se présenteraient en disant leur
prénom, leur pronom et leurs accords d'adjectifs. Pourquoi ? Parce que
ça permettrait qu'il n'y ait pas que les personnes trans° qui le fassent
et que le fait de dire ses pronoms ne soit pas une forme de "coming out
forcé". Une autre possibilité pour répandre cette pratique est
également de le noter sur son CV, de le noter sur les étiquettes de
prénom dans les réunions où il y en a, etc. En bref : normaliser la
pratique pour ne pas mettre de côté les personnes pour qui c'est un réel
besoin.

## Et dans la pratique, à l'oral ?

Donc d'abord, on demande les pronoms et les accords des gens, ensuite il
suffit de les appliquer ! Donc si une personne te dit qu'ol utilise le
pronom "ol", il suffira d'utiliser ce pronom pour parler d'ol. Que
cette personne soit là ou pas d'ailleurs ! À ce propos, point
important : certaines personnes trans ne sont pas out dans tous les
milieux. Il se peut qu'une personne vous demande de lae genrer de telle
manière quand vous êtes avec ellui et qu'iel vous demande de lae genrer
d'une autre manière quand vous parlez d'ellui aux gens. Il peut être
vital pour cette personne que vous fassiez attention à ça. Se faire
*outer* sans son consentement peut être violent, mais également
dangereux ! Si vous êtes dans une situation comme celle-ci, n'hésitez
pas à demander à la personne s'iel veut que vous lae genriez de la même
manière quand iel n'est pas là.

Si vous êtes face à une personne trans et *out*, alors même lorsqu'iel
n'est pas là, il est important de lae genrer correctement et de
reprendre les gens qui ne le font pas.

Tu pourras dire :

\begin{nscenter}
"\emph{Hier j'ai vu Alix, ol va au cinéma demain, tu veux venir ?}"
\end{nscenter}

Si une personne te dit qu'iel utilise des accords neutres, tu peux
essayer de rendre ton langage le plus neutre possible, soit par des
contractions de mot :

\begin{nscenter}
"\emph{Iel est très belleau aujourd'hui avec ces chaussures !}"

"\emph{Ael est coiffeureuse, son salon est à côté de la gare\index[tags]{gare} !}"
\end{nscenter}

Soit en prononçant le "x" qu'on écrit parfois :

\begin{nscenter}
"\emph{Iel est heureuxe.}"
\end{nscenter}

Si la personne te dit qu'elle alterne entre le masculin et le féminin,
tu pourras alterner entre le masculin et le féminin, et ainsi de suite !

## Et dans la pratique, à l'écrit ?

À l'écrit, tu as vu quelques exemples pour les pronoms, pour les
adjectifs, là aussi, tout dépend de la personne ! Certaines personnes
utilisent le point médian pour leurs adjectifs :

\begin{nscenter}
"\emph{J'arrive à 2\ h, je suis désolé·e du retard.}"
\end{nscenter}

D'autres y ajoutent le "x" :

\begin{nscenter}
"\emph{J'arrive à 3\ h, je suis désolé·e·x, je suis un peu en avance.}"
\end{nscenter}

L'idéal reste de demander ou d'imiter les manières individuelles de se
genrer à l'écrit.

## Et si je parle ou j'écris de manière générique ?

Ne présuppose pas les pronoms des gens (ni à cause de leur prénom ni à cause
de leur expression de genre), dans le doute\index[tags]{doute} utilise le neutre et accepte
qu'on te reprenne. Pour parler de groupes, utilise une écriture
réellement inclusive, par exemple de la manière suivante :

\begin{nscenter}
"\emph{Iels sont arrivé·e·x·s en France\index[tags]{france} jeudi.}"
\end{nscenter}

Et pas :

\begin{nscenter}
"\emph{Ils et elles sont arrivé·e·s en Allemagne vendredi.}"
\end{nscenter}

Qui est excluant pour toutes les personnes qui ne se reconnaissent ni
dans le masculin ni dans le féminin.

Si ça peut t'aider, quelques formules encore plus inclusives :

**Ellui** : Contraction de "elle" et de "lui"

\begin{nsright}
"\emph{C'est vraiment ellui qui a fait ça ?}"
\end{nsright}

**Lae** : Contraction de "la" et de "le"

\begin{nsright}
"\emph{Lae pote de ma mère\index[tags]{mère} aime les fleurs.}"
\end{nsright}

**Lo** : Néopronom pour remplacer "le" ou "la"

\begin{nsright}
"\emph{Lo pote de ma mère\index[tags]{mère} aime les jeux de société.}"
\end{nsright}

**Maon** : Contraction de "ma" et "mon".

\begin{nsright}
"\emph{Maon collègue est formidable !}"
\end{nsright}

**Touxtes** : Mot non genré pour "toutes et tous".

\begin{nsright}
"\emph{L'anniversaire\index[tags]{anniversaire} de Megan était plein de monde. Touxtes étaient très sympas !}"
\end{nsright}

**Celleux** : Contraction de "celles" et "ceux".

\begin{nsright}
"\emph{Celleux qui ont choisi l'option basket vont à droite, celleux qui ont choisi hockey vont à gauche.}"
\end{nsright}

[^99]: *À lire à haute voix*\ \[n^o^\ 32\] propose un exercice pratique pour s'exercer oralement à l'écriture inclusive.

