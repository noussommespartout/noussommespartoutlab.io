---
title: Conventions typographiques et inclusivité du français
meta: true
num: "0-2"
---

Le collectif logistique a décidé d'un ensemble de
conventions typographiques, dont la plupart concerne l'écriture
inclusive. Dans l'histoire et la pratique militante, cette écriture a
connu de nombreuses variations, occasionnant plusieurs propositions de
conventions, souvent complémentaires. En compagnie de personnes militant
sur la question, nous avons opté pour des conventions fluides, en mêlant
expériences et propositions historiques.

## Usage du X

L'usage du X engage, dans la structure genrée de la langue,
un trouble, la possibilité d'une troisième voie qui échappe (ou
plutôt qui représente la possibilité d'échapper) à la normativité
binaire°. Un militant est une personne qui se reconnaît comme un homme,
une militante est une personne qui se reconnaît comme une femme, unex
militanxte est une personne libre de se situer où iel le voudra dans le
spectre riche et complexe de l'identité de genre°.

Dans ce livre, le X est utilisé dans l'ensemble des mots et
des classes lexicales sujets à l'accord de genre dans la langue
française. Cette typographie inclusive a été appliquée à
l'ensemble des termes désignant des individualités génériques ou des
groupes rassemblant des personnes dont le nombre et/ou le genre ne sont
pas objectivement connus. Elle n'est par contre pas appliquée pour
désigner des groupes n'incluant objectivement pas de personnes non
binaires. Dans ce cas, c'est le féminin prioritaire qui est employé. Le
masculin, quant à lui, est utilisé pour désigner des groupes n'incluant
objectivement que des hommes.

Le plus souvent, ces questionnements et ces précisions
étaient explicitement souhaités par les auteurixes. Dans certains
textes, à leur demande, certains groupes ou fonctions sociales ont été
accordés au masculin traditionnel pour mettre l'accent sur les aspects
masculins de certaines positions de pouvoir ("les policiers", "les
patrons", "les politiciens", etc.).

Enfin, l'écriture inclusive binaire est utilisée pour
désigner les animaux non humains.

## Accords non binaires sans points médians

Dans la mesure où *Nous sommes partout* est un projet qui
se destine également à des formes orales[^2], la priorité a été mise
sur l'aisance de lecture.

Les parenthèses, les crochets, les barres obliques, les points
classiques ou médians tendent en effet à alourdir la lecture et à
générer des incertitudes dans l'énonciation orale ("les militant.x.e.s",
"les militant(e)(x)s", "les militant/e/x/s", etc.). Nous avons donc fait
le double choix de supprimer l'ensemble des séparateurs typographiques
et de placer librement les X dans les mots genrés pour créer des formes
plus facilement oralisables. Dans certains cas, quand le fait de genrer
s'entend, nous avons employé des formes faciles à oraliser, en
interprétant assez librement les possibilités ("les militanxtes").

Dans les cas où le résultat n'était pas facilement
oralisable, nous avons simplement fait le choix d'un X final, placé
avant les S au pluriel ("iels n'étaient pas sûrexs"). Quand le genrage
repose sur l'ajout de voyelles ou de consonnes muettes, construire des
formes oralisables nous a paru plus simple avec l'ajout d'un X final
("des militanxtes énervéexs").

Au fil de l'écriture, à mesure que les contributeurixes relisaient et
transformaient les conventions, plusieurs formes possibles d'un même
accord en sont venues à coexister dans le volume, ce qui nous convient
très bien.

Les pronoms genrés et certaines locutions d'usage sont
contractés : "iel" pour "il/elle", "elleux" pour "elles/eux", lae pour
"la/le".

Nous avons fait le choix de respecter les accords
classiques des mots non genrables désignant des groupes sans distinction
de genre : "les personnes sont énervées" (et pas "énervéexs").

## Autre

Sur la suggestion bienvenue d'une contributrice, les
marques et entreprises privées sont écrites sans leur majuscule initiale
pour ne pas leur donner plus d'importance qu'elles n'en ont déjà.

Les termes suivis d'un\ ° sont définis ou présentés dans un
glossaire à la fin de l'ouvrage.

[^2]: Voir dans l'introduction les précisions relatives aux lectures publiques.
