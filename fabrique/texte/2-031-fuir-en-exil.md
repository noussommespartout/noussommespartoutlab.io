---
author: "Mikele"
title: "Fuir en exil"
subtitle: "De collectif en collectif, d'occupation en occupation"
info: "Transcription d'un entretien"
date: "juillet 2020"
categories: ["luttes migratoires", "antiracismes", "autogestion, expérimentations collectives"]
tags: ["index"]
lang: ["fr"]
num: "31"
quote: "j'ai participé à l'occupation d'une église tout en y habitant"
---

*Le 8 mars 2015, le collectif R[^79] (un groupe citoyen constitué
d'environ 200 personnes) occupe l'église Saint-Laurent au centre de
Lausanne pour y ouvrir un refuge destiné à accueillir des personnes
victimes de la politique migratoire européenne, et particulièrement des
règlements de Dublin\index[tags]{dublin}°. Après avoir installé un rapport de force\index[tags]{force} avec les
autorités, l'occupation tiendra un peu plus d'une année et sera
finalement levée en vue de l'occupation d'une autre chapelle au
centre-ville, qui demeurera un refuge et un lieu d'organisation jusqu'en
décembre 2017.*

\vspace{1\baselineskip}

Je m'appelle Mikele, j'ai 29 ans, ça fait six ans que je suis en
Suisse[^80].

J'aimerais vous parler de l'Érythrée, de son système politique
dictatorial, et de celui de la Suisse qui est démocratique. La
différence, c'est qu'en Érythrée, tout est toujours "militaire\index[tags]{militaire},
militaire, militaire\index[tags]{militaire}". Je ne suis pas venu en Suisse pour rien, je ne
suis pas venu pour prendre des vacances\index[tags]{vacances}. J'étais au service militaire
là-bas et je suis objecteur de conscience. Je suis arrivé en Suisse
après un parcours qui a duré une année. Ici, on a le choix entre le
service militaire\index[tags]{militaire}, le service civil et la protection civile. C'est une
vraie différence. Chez nous, t'as pas le choix, tu pars un an et tu n'as
qu'un mois de vacances\index[tags]{vacances}, pas de week-ends. En Suisse, c'est plus
tranquille, tu peux rentrer le week-end et surtout, tu peux faire le
service civil. Je n'arrive pas à expliquer avec la parole\index[tags]{parole} ce que ça
suscite en moi, avec mon visage\index[tags]{visage}, je pourrais sûrement mieux l'exprimer.

En Érythrée, personne ne lutte contre le service militaire\index[tags]{militaire}, les gens ne
peuvent pas. Si tu critiques le gouvernement\index[tags]{gouvernement/gouverner}, t'es foutuex, tu n'existes
plus. En Suisse, les personnes que j'ai rencontrées participent à des
manifestations contre les dictatures, les Érythréennexs de Genève par
exemple, mais aussi les Suissessexs. Au début, je me suis demandé
pourquoi les Suissessexs participaient à ces manifestations. C'est à
nous de nous engager contre la dictature. Et j'ai réalisé la grande
différence qu'il y a entre ici et là-bas. Ces personnes sont suisses,
elles participent à des manifestations contre le gouvernement\index[tags]{gouvernement/gouverner} érythréen,
avec nous. C'est incroyable, vraiment.

Aujourd'hui, je ne peux pas porter des vêtements militaires qu'on trouve
dans les magasins, je ne me sens pas à l'aise. Je suis différent,
notamment grâce aux personnes que j'ai rencontrées dans le Collectif R
et dans le collectif d'ici[^81]. C'est grâce à elleux que j'ai pu
participer à des manifestations, que j'ai pu comprendre pourquoi on en
fait régulièrement en Suisse et que j'ai développé des connaissances sur
les droits qu'on a quand on y participe. Grâce à ces deux collectifs,
j'ai pu participer à plein d'autres choses. On organise des repas à prix
libre°, par exemple[^82]. Ici, c'est l'égalité. Même si tu n'as pas de
travail, même si tu n'as pas de papiers, même si tu n'as rien du tout,
c'est l'égalité.

Avec les personnes du collectif R, j'ai organisé des manifestations
contre le règlement de Dublin\index[tags]{dublin}°[^83]. Je suis allé au foyer\index[tags]{foyer} où les
Érythréennexs vivent pour distribuer des informations. Mais iels avaient
peur. Iels n'arrivaient pas à dire non, iels n'arrivaient pas à dire
oui. Iels n'arrivaient pas à venir aux manifestations que j'organisais,
parce qu'iels avaient toujours en tête les principes dictatoriaux
d'Érythrée. Iels pensaient que s'iels participaient à ces événements
ici, il allait se passer la même chose que là-bas.

Pendant un an, j'ai participé à l'occupation d'une église tout en y
habitant. D'autres personnes blanches du Collectif R venaient pour
assurer une permanence\index[tags]{permanence}. Iels restaient avec nous et nous protégeaient.
Iels ne pouvaient pas nous laisser seuls, notamment à cause de la
police. Si la police avait réussi à venir, on serait allé en prison. On
n'avait rien du tout, même pas des papiers blancs° et ça, pendant un an.
Quand on voyait la police arriver, on courrait vers l'église. Pendant un
an, ça s'est passé comme ça. Mais pendant ce temps, on faisait aussi des
cours de français, on discutait. Toutes les deux heures, il y avait
quelqu'unex qui venait. On faisait à manger, on organisait des matchs de
foot. Parfois on sortait, parfois on ne sortait pas. Parfois, on jouait
même à l'intérieur de l'église, en bas, il y avait une grande salle.
C'était vraiment cool, j'ai rencontré plein de gens incroyables. Des
gens qui font du bien dans leur vie, qui partagent leur vie avec les
autres.

Cette expérience-là m'a fait me demander : "pourquoi je suis là moi ?
comment et pourquoi j'ai changé ? comment j'ai compris que c'est pas
comme en Érythrée ?". C'est grâce aux personnes que j'ai rencontrées :
elles m'ont appris les lois suisses et le français. Grâce au collectif
R, j'ai appris la politique et j'ai essayé de participer à plein de
trucs. On a appris ensemble avec les cohabitants de l'église et on est
amis maintenant. Chaque année, on fête\index[tags]{fête} l'anniversaire\index[tags]{anniversaire} du Collectif R. Ça
fait 5 ans maintenant.

Aujourd'hui, c'est plus comme en 2015, il n'y a plus beaucoup de
personnes qui luttent contre le règlement de Dublin\index[tags]{dublin}. Personnellement,
j'essaie encore au maximum, je discute avec des gens, avec mes amiexs.
Nous, on est les premiers exemples, on a un permis de séjour
maintenant. Aujourd'hui, je fais un apprentissage. C'est notre rôle de
protéger celleux qui arrivent et qui sont sous le règlement de Dublin\index[tags]{dublin}.
Pour moi, l'année 2015, c'était l'université. J'ai appris, en un an, ce
que j'aurais appris en trois ans de sciences sociales.

J'ai commencé à fréquenter cet autre collectif, ici. D'abord tous les
mercredis, puis de plus en plus souvent. J'y ai appris plein de choses :
le prix\index[tags]{prix} libre par exemple. Tu achètes une bière\index[tags]{bière} et tu mets ce que tu
peux. Si je vais dans un bar, je paye six ou sept francs pour une bière\index[tags]{bière}.
Je me suis dit "pourquoi payer six ou sept francs, alors qu'ici, je
paye à la hauteur de ce que je peux payer, alors qu'ici, c'est génial ?
Pourquoi ne pas participer aux activités ?".

Un jour, une amie m'a dit que je pouvais devenir membre du collectif,
parce que j'étais souvent là. Les personnes que j'ai rencontrées ici
sont vraiment géniales. J'ai de la chance. Ici, tu viens quand tu veux
et tu peux être ce que tu veux. On est ouvertexs. J'ai peur\index[tags]{peur} que la
maison de ce collectif ne puisse plus exister dans un ou deux ans.
J'aimerais qu'il y ait un endroit pour ce collectif qui reste pour
toujours. J'ai peur\index[tags]{peur} que tout change. Je n'aimerais pas qu'on s'éloigne
de cette maison. J'aimerais continuer à pouvoir\index[tags]{pouvoir} y faire plein de choses,
j'aimerais que les artistes en profitent. Moi, je ne suis pas artiste\index[tags]{art/artiste},
mais j'aime bien voir des artistes travailler ici.

J'ai envie d'ajouter quelque chose par rapport à la sexualité. En fait,
quand je suis arrivé en Suisse, quand je voyais des gens qui
s'embrassaient dans la rue\index[tags]{rue}, même si c'était un homme et une femme,
j'étais choqué. Avant cela, je n'avais jamais vu de couples s'embrasser.
C'était encore plus choquant lorsque c'était deux hommes ou deux femmes.
C'était fort, parce que je n'avais jamais vu et jamais imaginé ça. Et
après une ou deux années, je me suis dit : "comment elles font, chez
nous, ces personnes-là ? Elles se cachent. Mais c'est leur choix, c'est
leur vie. Pourquoi on ne protège pas ces personnes-là ?". Et je me suis
dit qu'il fallait que je travaille sur moi-même, qu'il fallait que je
sois égalitaire. J'ai appris l'égalité. C'est très important dans la
sexualité. Je ne sais pas comment expliquer ça. Mais je pourrais
l'expliquer par la danse.

[^79]: le Collectif R est aussi évoqué dans *Du sable dans l'engrenage*\ \[n^o^\ 7\], *Jean Dutoit en lutte*\ \[n^o^\ 13\] et *L'histoire d'une lutte*\ \[n^o^\ 16\].

[^80]: *Lutter sans papiers*\ \[n^o^\ 25\] et *L'histoire d'une lutte*\ \[n^o^\ 16\] sont aussi des récits de lutte en exil.

[^81]: Un collectif qui lutte notamment pour les droits des sans-papièrexs et qui nesouhaite pas être nommé ici.

[^82]: Sur l'importance de la nourriture dans les luttes, lire *Le Grand Midi*\ \[n^o^\ 47\].

[^83]: *L'absurdité de devoir prouver sa vie*\ \[n^o^\ 50\] problématise aussi les règlements de Dublin.
