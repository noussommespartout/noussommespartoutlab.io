---
author: "Collectif Afroféministe Amani"
title: "Si ce n'est maintenant, alors quand est-ce qu'on se libérera ?"
subtitle: "Discours lu sur la plaine de Plainpalais le 8 mars 2020 à l'invitation du collectif 8 mars révolutionnaire"
info: "Texte retravaillé pour le recueil"
date: "avril 2020"
categories: ["antiracismes", "féminismes, questions de genre"]
tags: ["index"]
lang: ["fr"]
num: "14"
quote: "et ne surtout jamais, jamais, se mettre en colère"
---

Nous, le collectif afroféministe Amani de Nyon, nous
nous inscrivons dans la lutte contre le racisme et le sexisme que subissent
les personnes noires et afrodescendantes qui se reconnaissent dans le
genre féminin en Suisse. Suivant une approche intersectionnelle°, nous
luttons pour assurer l'élimination des discriminations de race, de
genre, de classe, de sexualité, de religion\index[tags]{religion} et contre l'oppression capitaliste.

On est une grande famille, on peut tout se dire, non?

On le savait déjà, les voix\index[tags]{voix} des femmes\*° noires, quand elles affrontent
le *statu quo* dans les arènes publiques, sont sources de confusions,
elles engendrent des débats moraux sur la véracité, la justesse ou la
respectabilité de nos propos. Dans un monde où les questions féministes
gagnent de plus en plus de terrain dans notre champ de crédibilité
politique, nous, femmes\* afrodescendanxtes, vivons à la marge et dans
l'ombre d'un féminisme blanc, dominant, qui se veut universel, tout en
se positionnant comme étant le seul gardien de la justice\index[tags]{justice} sociale.
Pourtant, ces grands récits héroïques ne sont jamais neutres, car leur
vérité particulière, élevée au statut mythique, invisibilise les visages
et assourdit les voix\index[tags]{voix} de touxtes les autres femmes\* non blanchexs.
Comme l'a dit Bell Hooks, "Toutes les femmes\* sont blanches, tous les
hommes sont noirs, mais nous sommes quelques-unes à être courageuses."

Nous, personnes afrodescendantes, migrantes, mères, travailleuses,
étudiantes, portons le poids de notre passé dans un présent qui nous nie
la possibilité d'exister entièrement. Être une femme\* noirex et
afrodescendanxte, c'est tous les jours, encore, tenter d'être dans la
discrétion absolue, de se dissoudre dans une norme impossible\index[tags]{impossible} à
atteindre, de ne jamais se voir représentéexs autrement que dans des
rôles stéréotypés, et ne surtout jamais, jamais, se mettre en colère\index[tags]{colère}.

Vous vous demandez souvent : "mais où sont les femmes\* noirexs dans
les luttes féministes ?". Nous répondons : "Elles s'occupent de vos
enfants, de vos aînéexs, de votre ménage". Comme ce fameux 14 juin
dernier, où des employéexs raciséexs° nous ont fait signe par la fenêtre
d'un hôtel, dans lequel visiblement, elles étaient en train de
travailler. Elles savent que prendre la parole\index[tags]{parole}, c'est dangereusement
s'exposer à des représailles. Le coût de la liberté\index[tags]{liberté} peut valoir la mort
sociale. C'est pourquoi, dorénavant, nous exigeons un futur dans lequel
nous pourrons exister et qui ne se fera pas sans une éducation
décoloniale, libératrice, sans l'instauration d'une justice\index[tags]{justice} réparatrice°
et émancipatrice. Nous parlons d'une éducation décoloniale. Oui. La
Suisse, se cachant derrière les pays ayant été ouvertement
esclavagistes, a un passé tout aussi colonial et devrait faire face à
cette histoire. À l'heure du réchauffement climatique et des
soulèvements populaires en faveur de l'environnement, la Suisse a tout
intérêt à remettre en question sa politique capitaliste d'enrichissement
aux dépens des pays du Sud. Les activistes écologistes de ces pays ont
été les premièrexs à alerter sur les modifications de leurs
environnements causées par les multinationales occidentales. Iels n'ont
pas été écoutéexs et sont encore effacéexs, aujourd'hui, des grandes
photos de famille des militanxtes écologistes (comme nous l'avons vu
récemment dans le cas de Vanessa Nakate, activiste noire ougandaise).

Être afroféministe, c'est assumer ce défi révolutionnaire, assurer un
futur libre pour touxtes, une justice\index[tags]{justice} réparatrice et émancipatrice pour
les personnes trans, intersexes, non binaires°, en situation de handicap
ou homosexuelles. Si nous ne sommes pas touxtes libéréexs, alors
personne ne le sera. Nous les voyons et nous voyons aussi les
changements radicaux que la société doit réaliser, afin que la vie de
ces personnes puisse compter à part entière. Nous croyons en notre
potentiel et en notre rage\index[tags]{rage} d'exister. Nous la portons, pour que dans un
futur proche, nous puissions assurer un avenir radicalement inclusif.
Nous sommes pleinexs d'espoir, ce soir, au côté de nos sœurs de lutte,
dans ce virage intersectionnel que prend le féminisme en Suisse. Nous
luttons ensemble, car nous savons que nous sommes touxtes capables de
solidarité.

