---
title: Introduction
meta: true
num: "0-1"
---

## Objet de paroles

*Nous sommes partout*, l'objet que vous tenez entre vos mains, est une
sauvegarde.

Imprimée sur demande et au prix le plus bas possible, chaque édition du
livre enregistre l'état d'une base de données en ligne qui accueille des
contributions dans lesquelles des militanxtes racontent leurs combats.

La base est accessible sur
[*www.noussommespartout.org*](https://www.noussommespartout.org).

\vspace{1\baselineskip}

\begin{nscenter}\includegraphics[width=0.38\textwidth]{../gabarit/code.png}\end{nscenter}

\vspace{1\baselineskip}

*Nous sommes partout* collecte et partage des voix antifascistes,
féministes, anticapitalistes, antiracistes, antispécistes, des paroles
de hackeureuxses, des voix en lutte pour les droits des migranxtes,
contre toutes les formes d'oppression de nos sociétés, pour les droits
LGBTQIA+, contre les écocides, pour les droits des travailleureuxses du
sexe, contre les violences policières, pour les droits des
sans-papièrexs, pour l'autodétermination et l'émancipation de touxtes
les travailleureuxses, contre la précarisation, contre le système
carcéral et pour les ZAD.

Le projet est pensé comme un regroupement de ressources, une
constellation de paroles. On y lit essentiellement des témoignages
écrits pour l'occasion, des textes retravaillés ou des transcriptions de
discussions orales.

Toutes les personnes ayant contribué à l'ouvrage sont rémunérées. Les
fonds du projet sont notamment collectés via des partenariats avec des
institutions culturelles qui accueillent en échange des soirées
d'écoute-lecture. Le public y est invité à lire les textes du recueil à
voix haute, pendant quelques heures.

Toutes les personnes contribuant à la base de données sont pleinement
informées de ce dispositif. Elles écrivent en sachant que leur texte
pourra être lu à voix haute par toute personne s'étant inscrite pour
participer à l'une de ces sessions participatives d'écoute-lecture.

Le *Nous* qui donne son titre au projet revendique le rêve d'une énergie
agglomérée, d'une somme de *Je* qui écrivent en leur nom et d'autres
*Nous* qui écrivent en collectif, ici et ailleurs, une constellation
que le pronom n'uniformiserait pas. Heureusement, le *Nous* n'a pas le
pouvoir de réduire les parties à un tout. Dans ce projet, le *Nous* fait
apparaître les convergences, les synergies de centaines de voix qui sont
autant de vecteurs d'un vaste mouvement de transformation.

Si la lutte est comme un cercle, sans début ni fin, alors partout et en
tout temps *Nous* sommes là.

## Collectif logistique

Le collectif derrière le projet, que nous appelons parfois "groupe éditorial" ou "collectif logistique", est constitué, pour cette édition
suisse romande, de sept personnes. Certaines viennent de milieux
artistiques ou littéraires et sont politiquement engagéexs dans leurs
pratiques respectives.

En pratique, le groupe éditorial a d'abord rédigé un appel à
contributions qui a circulé dans certains milieux militants, puis il
s'est occupé des transcriptions, des relectures, de l'harmonisation
typographique et des échanges avec les canaux de financement. Le groupe
se charge également de l'organisation des sessions d'écoute-lecture dans
les institutions (et de certaines sessions ayant lieu en dehors des
institutions).

Le collectif logistique essaie de limiter au maximum toute intervention
éditoriale autoritaire pour que *Nous sommes partout* reste centré sur
son geste primordial : accueillir des voix. Nous voulons éviter de
dessiner des lignes de force sculptant trop strictement le projet, nous
essayons de lui laisser prendre la forme que lui donnent les
contributions, sans intervenir. Notre intention est de socialiser des
réflexions sur les pratiques militantes --- dans les termes choisis par
les très nombreuxses auteurixes. Nous croyons en la nécessité de
l'échange et en l'agentivité des lecteurixes à qui nous adressons
cette collecte protéiforme comme une interrogation sociale, critique et
historique sur les combats qui structurent la société et sur la manière
dont on peut lutter pour changer le réel.

Le principe même de cette tentative d'édition participative et organique
nous amène toutefois à assumer certains paradoxes inévitables et à faire
des choix.

## Terrains

Nous avions l'idée d'un recueil qui ne soit ni un ouvrage de théorie
critique, ni une enquête problématisée autour d'axes précis, ni une
histoire structurée des combats d'un territoire spécifique. Nous
imaginions une somme de paroles qui relate l'immédiateté de luttes
contemporaines, relevant d'une mise en texte de vécus pratiques et
émotionnels. *Nous sommes partout* s'inscrit dans un terrain
expérientiel, un terrain géographique et un terrain idéologique.

Chaque texte, à sa manière et à son degré, est lié à *l'expérience* de
l'action militante. Cette expérientialité se révèle sous différentes
formes au fil des textes, de partages sensibles au sein de collectifs,
de manières de vivre l'action directe ou encore de narrations centrées
sur certains moments de vie considérés par les auteurixes comme
particulièrement politiques.

Cette édition est située dans un terrain géographique précis, la Suisse
romande, afin qu'on puisse y reconnaître un contexte sociopolitique
commun, des zones de lutte partagées, une actualité mutuelle, un tissu
temporel cohérent. Les villes les plus présentes dans les textes sont
Lausanne et Genève (les deux plus grandes villes de la région), mais le
recueil visibilise aussi certaines résistances situées dans de plus
petites villes ou dans des villages. Les textes se font naturellement
écho : les noms de certaines places publiques, de certains lieux ou de
certains collectifs reviennent ; certains événements, institutions ou
logiques structurelles sont décrits depuis plusieurs points de vue.

Centré sur l'expérience de l'action politique radicale, *Nous sommes
partout* ne se structure pas autour de perspectives idéologiques
prédéfinies. Cela dit, par souci d'accueillir des voix, des idées et des
modes d'action peu présents dans l'interdiscours dominant, un certain
environnement conceptuel s'impose de lui-même. On peut essayer de le
saisir par la constellation d'adjectifs utilisés par les
contributeurixes pour se décrire : non institutionnel,
radical, libertaire, intersectionnel, anticapitaliste, antifasciste,
antiétatique.

Nous essayons de rester très prudenxtes avec les étiquettes, parce que
les mots désignent des réalités différentes selon les personnes, les
histoires individuelles, les imaginaires, les villes, les pays, les
terrains, les idéologies ou les vocabulaires de référence. Le groupe
éditorial n'établit pas de bornes idéologiques stables dans lesquelles
chaque contributeurixe serait contrainxte de se reconnaître en
participant au projet (touxtes peuvent d'ailleurs retirer leur texte à
tout moment de la base de données, et donc des éditions futures du
livre).

D'autres paradoxes apparaissent encore si l'on veut décrire cette
tentative d'édition participative et organique, mais centralisée tout de
même autour d'un collectif, notamment le choix des textes. Nous ne
refusons jamais un texte qui soit un témoignage de lutte contemporain,
issu de Suisse romande et n'entrant pas en contradiction avec l'univers
idéologique qui se dessine. Au commencement du projet, l'appel à
contribution a été diffusé sur internet. Depuis, *Nous sommes partout*
se propage en réseau : les auteurixes parlent du projet à d'autres
militanxtes dont iels ont envie de faire entendre les voix, qui envoient
à leur tour de nouvelles contributions.

## Formats

Concrètement, les textes circulent sous trois formes : une base de
paroles données, différentes versions imprimées et des sessions
d'écoute-lecture publiques.

La **base de paroles données** est en augmentation constante, au fil de
l'arrivage des contributions. Tous les textes y sont disponibles
gratuitement, en ligne et dans leur intégralité, et tous peuvent
également être téléchargés individuellement dans des formats facilitant
leur impression et leur diffusion en brochures. La base de données
centralise l'ensemble des textes issus des différentes versions
linguistiques et géographiques de *Nous sommes partout*.

Les **versions imprimées** sont des sauvegardes de la base de données,
orientées autour de territoires précis. Les livres sont imprimés au prix
le plus bas possible et nous avons fait le choix de leur ouvrir le
circuit de diffusion traditionnel (librairies, vente numérique, etc.)
pour privilégier leur circulation et leur visibilité. Ni le collectif
éditorial ni la maison d'édition Abrüpt n'engrangent de bénéfices sur
les ventes.

Les **sessions d'écoute-lectures publiques** ont lieu dans des
environnements institutionnels et non institutionnels. La pratique de la
lecture collective a une histoire riche et plurielle, protéiforme selon
les contextes et les usages. Nous nous sommes inspiréexs, pour ces
sessions, de "l'arpentage", une technique développée dans les luttes
syndicales pour pallier le manque de temps dédié à l'éducation
législative et politique. Une session d'arpentage fait appel à
l'intelligence collective : on prend un livre, on arrache chaque
chapitre et on se les partage. Chacunex lit les pages qu'iel tient entre
ses mains puis, dans un deuxième temps, on résume collectivement ce
qu'on a lu.

Durant les lectures de *Nous sommes partout*, nous utilisons une forme
performative simple. Les spectateurixes sont conviéexs à une session
d'écoute-lecture pendant laquelle iels donnent voix aux textes
collectés, accessibles à touxtes sous la forme de chapitres déchirés.
Les lectures dans les espaces institutionnels sont aussi le cœur de
l'interface matériel du projet, puisqu'elles permettent de lever les
fonds nécessaires à la rémunération des auteurixes.

## Suites

À l'avenir, la base de données accueillera de nouveaux textes tant que
le groupe éditorial parviendra à lever des fonds. Nous travaillons à ce
que d'autres collectifs puissent mettre en place des collectes de textes
similaires dans d'autres terrains linguistiques et géographiques.

Écrivez-nous si vous voulez contribuer à la base de données et aux
futures versions du recueil[^1],

Bonne lecture, Plein d'amour,

\vspace{1\baselineskip}

\begin{nsright}

Le groupe éditorial de \textit{Nous sommes partout} (Suisse romande)

1\textsuperscript{er} mai 2021

\end{nsright}

[^1]: *noussommespartout\@riseup.net*.

