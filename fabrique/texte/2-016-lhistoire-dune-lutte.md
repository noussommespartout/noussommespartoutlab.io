---
author: "Samba"
title: "L'histoire d'une lutte"
subtitle: "Une nouvelle vie que tu dois apprendre"
info: "Transcription d'un entretien oral (traduit de l'anglais)"
date: "avril 2020"
categories: ["luttes migratoires", "prison, justice, répression", "antiracismes"]
tags: ["index"]
lang: ["fr"]
num: "16"
quote: "vivre sans papiers, c'est une vraie lutte"
---

Je m'appelle Samba[^45], je viens de Gambie. Ça a été une épreuve de
venir en Europe. Sur la route, on a fait face à beaucoup de difficultés
en traversant tous les pays africains. C'était pas toujours facile
d'être avec tous ces gens que je ne connaissais pas, de vivre une vie
que je ne vivais pas avant. L'endroit le plus dur par lequel je suis
passé, c'est la Libye. Là-bas, personne ne va te donner quelque chose à
manger ou t'aider. On a essayé de trouver de la nourriture, du travail
pour survivre, un endroit où dormir. Tu peux te prendre un coup de
couteau sans raison. Ta vie est toujours en danger. Au bout d'un moment,
tu penses même plus qu'un jour tu vas pouvoir\index[tags]{pouvoir} partir de là. Chaque jour,
quand tu sors, tu ne sais pas si tu vas revenir ou non. Tu te réveilles
chaque matin en te disant : "aujourd'hui, peut-être que je meurs,
peut-être que je vis". Je suis allé en prison, j'ai été maltraité, j'ai
vécu des trucs vraiment graves, des trucs que tu ne peux pas imaginer.

Arriver en Europe, c'est encore une autre histoire. Tu dois réapprendre
à vivre dans une société différente, dans une culture différente, avec
des gens différents, en surmontant la barrière de la langue. Tu es dans
une nouvelle vie que tu dois apprendre et tu passes par beaucoup de
choses. Ma vie en Europe, elle a commencé dans un camp en Italie. Tu
habites dans un camp avec plein d'autres personnes, qui ne viennent pas
du même pays que toi ou qui ne parlent pas la même langue. Là-bas, les
problèmes s'enchaînent. Les gens font face à toutes sortes de
difficultés, certains ont de graves problèmes psychologiques. Tu n'as
pas le choix, tu dois partager ton lieu de vie. Après avoir quitté le
camp, il y a d'autres difficultés à surmonter. Tu n'es plus dans un lieu
sous contrôle où il y a des gens qui s'occupent de la nourriture, tu te
retrouves tout seul. Si tu n'as pas de papiers, ou même si tu en as,
comment tu trouves un travail si tu ne parles pas la langue ? Comment tu
trouves un travail si tu ne connais personne? Certains jours, c'est
vraiment très dur. Si tu n'as pas de papiers, tu ne peux pas avoir une
maison, tu ne peux pas payer un loyer... Pour moi, quitter le camp a été
le début des épreuves. Je ne connaissais personne, je suis arrivé en
Suisse et j'ai fait une demande d'asile. Mais ma demande a été rejetée.
Je n'en connais pas la raison, mais je pense que c'est parce qu'il n'y a
pas de violence\index[tags]{violence} dans mon pays, il n'y a pas de guerre. La Suisse m'a dit
de retourner en Italie, où on avait pris mes empreintes[^46]. Alors je
me suis dit : qu'est-ce que je vais faire de ma vie maintenant ? Tu dois
te battre pour rester un être humain. Si tu n'oublies pas, même un peu,
ce que tu as vécu, tu vas perdre la raison. Ça arrive à des personnes
qui deviennent folles dans la rue\index[tags]{rue}.

J'ai rencontré le collectif[^47] à travers plusieurs événements. Je
suis venu dans le lieu qu'iels occupaient pour cuisiner\index[tags]{cuisiner/cuisine} avec elleux et
quelqu'un m'a demandé si je voulais rejoindre les conversations en
français. Cette personne m'a aussi demandé si j'étais motivé à venir
tous les mercredis, parce qu'il y avait de la nourriture et des gens.
C'est un endroit tellement cool : je ne vais pas au restaurant ou dans
les cafés parce que je n'ai pas d'argent. Ici, c'est prix\index[tags]{prix} libre et comme
je n'ai rien, je ne paie rien. Je peux me reposer aussi. Ici, j'ai
commencé à me sentir faire partie de la société, j'ai commencé à
organiser des événements, à rire\index[tags]{rire/se marrer/rigoler}. J'ai réalisé que c'était bien
d'apprendre le français pour mon futur. Je progresse : *je parle
maintenant un peu français*[^48]. Alors bref, je venais de plus en
plus ici et, un jour, on m'a demandé si je voulais carrément intégrer le
collectif. J'ai accepté avec plaisir, mais il m'a fallu du temps pour me
sentir connecté. À cause de la barrière de la langue, mais aussi pour
comprendre le système, comment ça fonctionne. Ça prend du temps... D'un
point de vue politique et administratif\index[tags]{administration/administratif}, c'est vraiment dur si tu n'as
pas le droit de vivre dans le pays. Parfois, tu ne te sens pas à l'aise
d'aller quelque part parce qu'il y a des contrôles et que tu risques de
finir en prison. Ne pas avoir de papiers, ça change tout. Ça empêche
beaucoup d'actions. Il y a aussi le racisme omniprésent, tu y fais face
dans la rue\index[tags]{rue}, tu es attaqué constamment et tu ne peux jamais te défendre.
On te retire le droit de te défendre. Si les flics arrivent, même si tu
as raison, tu finiras en prison. Quelle que soit la situation, les flics
ne verront qu'une seule chose : tu n'as pas le droit d'être ici, tu n'as
pas de droits tout court. Alors tu laisses tomber, tu n'as pas les armes
pour te défendre. Ça m'affecte de plein de manières différentes, mais
faire partie d'un collectif organisé, c'est une nouvelle expérience,
c'est cool, ça me donne plus d'opportunités, plus de connexions.
Organiséexs ensemble, on peut participer à une manifestation, et ça nous
donne de la visibilité, comme personnes noires et comme personnes à qui
on refuse le droit de vivre dans ce pays et les moyens de se défendre.
La seule chose qu'on peut faire, c'est manifester pour que les gens
entendent ça de nos propres bouches. C'est important de rendre les gens
conscients, même si ça prend du temps. Ça amène le changement, pas à
pas.

Vivre sans papiers c'est une vraie lutte, et ça fait cinq ans et demi
maintenant. Partout où tu vas et tout ce que tu fais, tu dois bien
réfléchir : comment je fais pour aller d'un point A à un point B ? Par
où je passe ? Même pour aller au parc, tu dois faire attention. Les
flics sont peut-être en train d'attendre pour t'attraper. Parfois tu
calcules si ça vaut la peine d'y aller : tu n'es pas libre. J'ai des
amiexs avec des papiers, quand on se voit, c'est moi qui leur dis où on
va. Parce qu'iels n'ont pas à réfléchir à ce genre de choses, iels ne
vivent pas la ville comme moi.

Chaque fois que je trouve un job, j'y vais une journée et ensuite iels
me disent que je ne peux pas rester, que je ne peux plus travailler dans
cet endroit. Iels disent que je suis un bon travailleur, mais que sans
papiers c'est impossible\index[tags]{impossible} et iels me renvoient chez moi. Ma vie dépend
toujours des autres, que ça me plaise ou non. Sans les autres, je
dormirais à la rue\index[tags]{rue} et je devrais faire des choses illégales, parce que
je n'aurais pas d'autre choix. Je ne suis pas à l'aise avec ça, je ne
veux pas demander de l'argent directement à quelqu'unex. Mon but c'est
de vivre, de trouver un travail, d'avoir un futur. Je suis heureux de
faire partie du collectif, ça a changé ma vie, c'est toujours difficile
mais au moins je ne dors plus dehors. Alors même si c'est difficile, je
ne vois pas d'autre pays où vivre maintenant, à part en Suisse. Ici,
c'est l'endroit que je connais, c'est là où je veux être et je serais
terrifié de devoir vivre à nouveau dans la rue\index[tags]{rue}.

Être noir c'est une autre lutte encore. Tu fais face au profilage racial
tout le temps, même s'il y a des lois contre le racisme. Tu peux dire à
quelqu'unex que ce qu'iel dit est raciste. Tu peux y mettre des mots,
juste pour que les gens se rendent compte que ce qu'iels disent est
raciste, même si tu ne peux pas les changer. Mais quand tu n'as pas de
papiers, c'est comme si tu n'avais pas d'armes pour te défendre. Il y a
quelques jours, j'étais à la gare\index[tags]{gare} et un mec m'a frappé le bras de
manière très inappropriée. Je sais très bien qu'il n'aurait pas fait ça
à une personne blanche. Je n'ai pas pu le garder pour moi, je lui ai dit
"Pourquoi vous m'avez fait ça ? Je ne vous connais pas." Je lui ai
expliqué comment je me sentais et il a fini par s'excuser. Mais avant de
penser à s'excuser, il faudrait d'abord penser à ce que tu fais et ne
pas le faire. Si j'avais eu des papiers, j'aurais pu avoir une vraie
conversation avec lui, une conversation plus longue. Je ne voulais pas
avoir de problème, même si ce n'était pas moi qui avais créé le
problème. Mais bien sûr, avant que les gens autour aient pu comprendre
ce qu'il se passait, iels auraient immédiatement appelé les flics. Et
même si j'étais la victime dans cette histoire, c'est moi qui aurais
payé parce que je n'ai aucun droit. Alors j'ai juste laissé le mec. Ce
qu'il a fait n'était pas juste, il a fini par s'excuser, mais ça n'était
pas satisfaisant pour moi. Cette histoire est une anecdote qui résume la
situation : je me bats pour mes droits, mais je n'ai pas les armes pour
le faire à cause de ma situation illégale.

[^45]: *Fuir en exil*\ \[n^o^\ 4\] et *Lutter sans papiers*\ \[n^o^\ 25\] sont d'autres récits personnels du rapport à l'exil.

[^46]: Référence aux Réglementations dites de Dublin°.

[^47]: Un collectif qui fonctionne en autogestion et lutte pour l'accueil des personnes sans-papiers.

[^48]: En français dans le texte.

