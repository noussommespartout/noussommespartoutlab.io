---
author: "Oshose, Janko et Z"
title: "Jean Dutoit en lutte"
subtitle: "Discussion entre le collectif noir et le collectif blanc"
info: "Transcription d'un entretien oral"
date: "juin 2020"
categories: ["luttes migratoires", "antiracismes", "autogestion, expérimentations collectives", "prison, justice, répression", "squat, occupations, logement"]
tags: ["index"]
lang: ["fr"]
num: "13"
quote: "la mort de notre ami Mike, tué par la police de Lausanne"
---

Le collectif Jean Dutoit a été fondé en 2015 à Lausanne. Il est né de la
rencontre entre une centaine de personnes originaires d'Afrique de
l'Ouest et un groupe de citoyennexs suisses. Si le Collectif s'est formé
dans le but de trouver un toit pour ses membres africains --- ils
vivaient et dormaient dans la rue\index[tags]{rue} faute d'hébergement disponible adapté
--- il a été immédiatement confronté aux dimensions politiques, sociales,
économiques et culturelles qui conditionnent l'existence des personnes
qui migrent en Suisse et en Europe. Ses membres issus de la migration
(résidents de la maison occupée par le Collectif) et ses membres suisses
(qui tiennent un rôle d'interface avec la société locale) ont uni leurs
efforts au cours des deux dernières années pour combattre les
discriminations et les abus dont les premiers sont la cible, en
construisant des alternatives viables. Le collectif soutient que
l'amorce d'un changement de cap devrait venir d'une politique de
coopération et de mobilité, qui pourrait passer par la mise en place
d'une stratégie d'accès aux droits et de plusieurs transformations
législatives et structurelles favorisant l'état de droit et la
démocratie.

## Discussion entre Oshose et Janko, deux résidents de la maison occupée à Écublens

--- J'ai rejoint le collectif alors que j'étais requérant d'asile. Je
venais d'arriver en Suisse, on avait pris mes empreintes à Chiasso et
j'ai été envoyé à Thoune. Ma requête a été refusée après deux ou trois
semaines et on m'a dit que j'allais être renvoyé chez moi. Je leur ai
dit que je n'avais nulle part où aller. Alors, je suis venu à Lausanne,
où on dormait dans des bunkers pour 5\ CHF par nuit\index[tags]{nuit/obscurité} qu'il fallait quitter
chaque matin. Quand j'ai rejoint Jean Dutoit, le collectif était déjà
fondé. C'était en 2016 au Sleep-In. C'était très dur là-bas. On
survivait grâce à une association qui nous amenait de la nourriture
chaque soir, aux environs de 19\ h. Les conditions étaient terribles, il y
avait trop de monde à l'intérieur, alors on dormait dans des tentes qu'on
avait construites nous-mêmes, dehors. Un jour, la police est arrivée et
a dit "fini les tentes". La nuit\index[tags]{nuit/obscurité}, quand il pleuvait, on devait aller
se mettre à l'abri et revenir quand la pluie s'arrêtait. Et puis la
police a commencé à arrêter des gens à la sortie du Sleep-in : un jour,
ils ont arrêté 30 ou 40 personnes qui ont été expulsées du pays.

--- Quand nos frères et nos sœurs ont vu la situation, iels ont commencé
à vouloir nous aider à trouver une solution\index[tags]{solution}. La solution\index[tags]{solution} c'était d'avoir
un lieu où vivre. C'est à ce moment-là que nous sommes partis à
Chailly-Village. Le propriétaire\index[tags]{propriétaire} nous a dit qu'on pouvait rester un
mois. Après un mois, il a fallu déménager à Romanel. C'était très dur,
pas confortable du tout. On a tout juste réussi à mettre un toit sur nos
têtes. Ça a duré un an avant qu'on doive déménager à nouveau. On est
partis à la Blécherette où on a aussi vécu un an, avant que le
propriétaire nous annonce qu'il avait besoin de récupérer les lieux. Là,
on a investi l'usine\index[tags]{usine} switcher. Là-bas, la répression policière était
très dure : à chaque fois qu'on sortait, on se faisait contrôler[^34].
C'était trop. Le propriétaire\index[tags]{propriétaire} nous a dit qu'il fallait partir et nous
avons déménagé, encore, à la Sallaz. Je crois qu'on a vécu là-bas quatre
ou cinq mois. C'était le meilleur endroit où on a vécu : les conditions
de vie étaient bonnes et la police nous contrôlait moins. Mais, à
nouveau, le propriétaire\index[tags]{propriétaire} a exigé notre départ. On a essayé d'expliquer
qu'on ne pouvait plus vivre dans des tentes. Malheureusement, la maison
devait être détruite, alors on n'avait pas le choix. Nos sœurs et nos
frères ne pouvaient pas nous laisser tomber, imaginer qu'on se retrouve
à nouveau dans la rue\index[tags]{rue}. Iels ont dit : "trouvons une solution\index[tags]{solution}". C'est
là que nous avons investi les lieux où nous sommes maintenant,
c'était...

--- Il y a trois ou quatre mois.

--- Oui, il y a trois ou quatre mois.

--- Début mai, au début du confinement.

--- Dans ce bâtiment, aujourd'hui, on est environ 70. Au début, on
accueillait tout le monde, soit 10 à 15 nouvelles personnes par mois.
Mais ça nous a amené tellement de problèmes, qu'on a dû limiter. Ici, on
s'auto-organise, on traite les problèmes nous-mêmes. On a créé une *task
force* avec une dizaine de personnes pour écrire et mettre en place des
règles pour vivre ensemble. Ce n'est pas si facile de cohabiter avec
autant de monde, d'organiser un si grand groupe. Ici, personne n'est
au-dessus des règles. C'est comme un pays : qui que tu sois, tu dois
suivre les règles. S'il y a un problème, un conflit, on en discute et on
essaie de trouver des solutions collectivement. Grâce à cette
organisation, on est devenu comme une famille, on se conseille, on
explique ce qui est bien pour nous, pour tous. On organise par exemple
un ménage général deux fois par semaine, pour garder les lieux propres.
On s'assure que tout soit calme. Tout le monde s'est
engagé à suivre les règles communes.

--- On a différentes nationalités, ça n'est pas toujours facile de
cohabiter. Aussi, on a tous des statuts légaux différents : certains ont
des permis de séjour, d'autres sont en attente que leur permis soit
approuvé, d'autres sont sans-papiers. L'important, c'est d'essayer de
créer des conditions pour vivre ensemble, qu'importe nos différences. On
n'encourage pas les luttes tribales par exemple, c'est inscrit dans nos
règles. On se voit tous comme des frères. On a le même but : trouver de
meilleures conditions de vie. Donc dans l'intervalle, on doit se
soutenir comme une famille. On est aussi en contact avec d'autres
collectifs à Lausanne, on fait des actions ensemble, on essaie de
trouver des idées ensemble. Il y a des choses qu'on ne peut pas faire
sans un soutien mutuel.

--- Quand on doit trouver un nouveau lieu de vie, il se peut qu'on
l'occupe illégalement. Si c'est le cas, la police vient et nous demande
ce qu'on fait là, ils entrent en contact avec le propriétaire\index[tags]{propriétaire} qui vient
généralement sur les lieux pour négocier. Il se peut qu'il accepte
qu'on reste, sous certaines conditions, pour peut-être quatre ou six
mois. Normalement, quand le contrat est terminé, on doit partir. Ça
dépend de comment on y vit, si on crée des problèmes ou pas. S'ils
voient qu'on ne détruit rien par exemple, il se peut qu'on puisse rester
plus longtemps, que le contrat soit renouvelé. Dans ce bâtiment par
exemple, on est en train de le renouveler, j'espère qu'on pourra rester.

--- Les choses se sont améliorées depuis 2016, on ressent moins de
pression de la police, les choses changent graduellement. C'est aussi
parce qu'on a manifesté, on a lutté, on a parlé publiquement de notre
situation. On ne profitera peut-être pas directement de notre lutte,
mais on espère que d'autres en profiteront.

--- Arriver dans un pays différent, c'est rencontrer des gens nouveaux et
apprendre des manières de vivre différentes. On trouve toujours des gens
qui nous encouragent et qui nous aident à comprendre. Ces gens font
aussi partie du collectif, on tire aussi notre courage\index[tags]{courage} de leur présence :
c'est une question de survie. On a besoin de courage\index[tags]{courage} pour avancer. Il
y a des personnes à Lausanne qui n'ont pas de lieu où vivre, pas
d'endroit où reposer leur tête la nuit\index[tags]{nuit/obscurité}. Si on abandonnait, on n'aurait
nulle part où aller.

--- Le plus grand challenge auquel on fait face, c'est la fin des
contrats de logement. On se demande toujours " On va où maintenant?".
On va de lieu en lieu, c'est sans fin. Pourquoi est-ce qu'on ne peut pas
avoir une maison ? Le prochain endroit sera-t-il meilleur ? Sera-t-il
pire ? Ici, maintenant, on connaît le bâtiment, on sait comment aller au
centre-ville, on a nos habitudes. Le prochain lieu pourrait être loin du
centre, sans transports publics. L'insécurité est notre plus gros défi,
c'est ce qui nous tire vers le bas. Nous sommes beaucoup ici, on
pourrait faire tellement de choses, aider la communauté, faire des
projets. Mais c'est impossible\index[tags]{impossible} : dans notre situation, les choses sont
trop incertaines et on est toujours occupé à se loger.

--- Le futur de ce projet, c'est qu'on trouve tous un meilleur endroit
pour vivre. Certaines personnes nous voient, elles voient qu'on lutte,
qu'on ne pourra pas trouver seuls des solutions. Peut-être qu'un jour,
le gouvernement\index[tags]{gouvernement/gouverner} nous aidera, nous donnera des papiers pour qu'on puisse
vivre ici. Même si on déménage tous, le collectif survivra, parce qu'on
se voit comme une famille. Ça nous aide à rester concentrés, et ça, on
ne l'oubliera jamais.

--- Avant, il y avait beaucoup de gens qui venaient nous voir, beaucoup
de médias qui parlaient de nous. À mon avis, beaucoup de ces gens le
faisaient pour leur propre gloire, iels tiraient profit de nous pour
leurs projets. Les journalistes qui écrivaient sur nous, ça ne faisait
qu'attirer l'attention sur nous et amener la police. Et notre situation
ne s'améliorait pas, rien ne changeait. Alors on a décidé de se
concentrer sur notre propre chemin et d'organiser le collectif de
l'intérieur.

## Arrivée de Z dans la discussion, membre du collectif blanc, Suissesse

J'étais là dans les premiers moments du collectif. Certaines de mes
amies blanches de Lausanne m'ont dit ce qu'il se passait. Je les ai
rejointes pour un meeting qui a eu lieu dans le jardin\index[tags]{jardin} du Sleep-In. De
là, on a rapidement décidé de déménager dans une maison à Fourmi\index[tags]{fourmi},
c'était le premier pas.

Je trouve inacceptable que des lieux restent vides alors que des gens
sont dans la rue\index[tags]{rue}. La Suisse, c'est dur pour les gens qui ne viennent pas
d'ici. Mon engagement a changé depuis le début, parce qu'avant j'étais
seule et maintenant j'ai des enfants. Je suis moins là qu'avant. Et il y
a autre chose qui a changé : au début, les blanches qui faisaient partie
du collectif dirigeaient beaucoup. C'était avant le mouvement *Black
Lives Matter*. Les gens, et surtout les blanches, ne remettaient pas autant en question
leur position dominante. C'était difficile[^35].

Puis, à l'usine\index[tags]{usine} heineken, il y avait énormément de monde. On a dû dire
non aux arrivants. Il y a eu des bagarres, les gens ne pouvaient pas se
reposer, l'énergie n'était pas toujours au beau fixe. On a mis en place
un fonctionnement qui permettait de prendre des décisions
collectivement. Au début, c'est beaucoup le collectif blanc qui prenait
les décisions, puis le pouvoir\index[tags]{pouvoir} a commencé à se répartir, et maintenant
c'est le collectif noir qui est en charge. Nous, les gens qui ne vivons
pas ici, on ne vient plus à toutes les réunions. On fonctionne davantage
comme un support administratif\index[tags]{administration/administratif}. On écrit des lettres en français ou on
cherche des informations. Quand il faut se déplacer, on vient aider pour
occuper des maisons, vu qu'on est moins vulnérables face à la justice\index[tags]{justice}.
Mais pour l'essentiel de l'organisation de la vie quotidienne, les
personnes qui habitent sur place sont complètement autonomes. Et je n'ai
rien à dire sur la manière dont ils veulent vivre.

Au départ, on était une vingtaine dans le collectif blanc, il y avait
une certaine mixité de femmes et d'hommes. Mais, très vite, il n'y a
plus eu que des femmes. Je me l'explique en partie par la manière dont
on a été élevées : quand t'es une femme, la société t'apprend à prendre
soin des autres. J'aimerais bien qu'il y ait plus d'hommes impliqués
dans ce genre de collectif, qui prennent soin des autres.

Mais globalement, on ne veut pas vraiment que plus de gens rejoignent le
collectif blanc. Il y a beaucoup de personnes qui ne s'investissent que
pour elleux-mêmes. C'est souvent le même schéma : une personne approche
le collectif blanc pour essayer d'atteindre les personnes qui occupent
le bâtiment. Elle veut que ses bonnes actions soient vues par des
personnes blanches : mais vas-y, rends-toi directement sur place,
rencontre les personnes qui vivent là et propose ton aide. C'est arrivé
qu'une personne veuille donner des cours de français et me demande de
les organiser, sans jamais réussir à comprendre que ce n'était pas moi
qui organisais la vie collective du bâtiment occupé. C'est arrivé aussi
qu'une personne insiste pour venir participer aux réunions, se
décommande plusieurs fois, puis finisse par se pointer à une réunion
dans laquelle on discutait d'affaires internes. On lui a dit qu'elle ne
pouvait pas y assister et elle a piqué une colère\index[tags]{colère} du genre "moi je
prends du temps, je me déplace ici et je ne peux pas rester, c'est
saoulant" et elle n'est jamais revenue. C'est important de comprendre
que si tu veux vraiment aider, parfois, tu dois te mettre en retrait et
accepter de faire ce que les gens te demandent de faire, accepter que ce
n'est pas à toi de décider à leur place. Tu dois aussi comprendre que le
collectif reçoit de l'aide d'ailleurs, par exemple des églises parce que
beaucoup sont croyantes, et que tu n'as pas à juger ça. Et d'ailleurs,
tu dois aussi comprendre que le collectif est une étape de vie pour des
personnes, un espace temporaire avant de construire quelque chose de
plus durable, en obtenant des papiers ou en se mariant. Personne n'a
pour objectif de vivre dans une usine\index[tags]{usine} vide avec 70 autres personnes.

La chose qui me décourage le plus, c'est les réactions des voisinexs et
de la police. La mort de notre ami Mike, tué par la police de Lausanne,
a été l'une des pires choses que nous ayons dû affronter. Vivre sans
papier, ne pas savoir où tu vas, ça crée beaucoup de violence\index[tags]{violence}. Si tu
n'es pas putain de fortex, ça peut te bouffer. On a vu des personnes
emmenées en prison et médicamentées de force\index[tags]{force}. Quand elles reviennent,
elles ne sont plus les mêmes. C'est arrivé ici. Ou alors les flics
emmènent quelqu'un et le tabassent tellement qu'il ne sera plus jamais
pareil. Ce genre de choses bousille vraiment les êtres.

Personnellement, ce qui me donne du courage\index[tags]{courage}, c'est que certains sont
devenus des amis. Il y a plus de cinq ans que je viens ici. J'ai envie
qu'ils se sentent bien, que leur vie soit meilleure. J'aimerais pouvoir
en faire plus, mais je n'ai malheureusement pas l'imprimante adéquate
pour faire des passeports suisses. Sur le fond, je pense que créer
ensemble, ici, nous rend heureuxses. Et puis, on partage beaucoup de
joie, on rit, on fait des fêtes, les gens sont géniaux.

## Discussion entre Oshose, Janko et Z

--- Occuper un bâtiment, c'est entrer dans une zone de guerre. On se
prépare à fond, on s'organise scrupuleusement. On s'assure d'être en
nombre, si on n'est que trois ou quatre, c'est sûr que la police va nous
arrêter. Et le risque d'être arrêté est trop grand pour les personnes
sans-papiers. Alors on prie pour que rien ne nous arrive.

--- Dieu fait partie du plan ?

--- (*Rires.*) Oui il fait partie du plan ! C'est mieux de l'avoir avec nous
que contre nous !

--- Quand tu entres dans un bâtiment, plusieurs charges peuvent être
retenues contre toi : occupation illégale, cambriolage, etc. Si la
serrure est cassée, ils vont t'accuser de dommages à la propriété. Alors
il faut entrer sans rien casser. Il faut contacter lae propriétaire
avant que la police arrive pour maximiser ses chances. Comme ça on peut
leur dire que lae propriétaire\index[tags]{propriétaire} est au courant, qu'iel connaît la
situation. C'est un truc qu'il faut préparer minutieusement : la lettre
doit être envoyée, puis le bâtiment occupé directement, sinon ton plan
est foutu. Il faut aussi parler à lae propriétaire\index[tags]{propriétaire} d'une certaine
manière pour qu'iel te laisse entrer : dire que tu vas payer pour l'eau
et l'électricité, que tu ne vas pas faire de dommages, que tu vas
t'occuper des ordures, que tu vas respecter le voisinage, que tu veux
lae rencontrer, que tu l'aimes bien. (*Rires.*)

--- C'est un mélange d'engagement et de manipulation. (*Rires.*)

--- Pour emménager ici, on a eu de l'aide de nos amiexs pour nos
affaires. D'abord on a occupé avec nos corps\index[tags]{corps}, puis avec nos affaires. On
ne garde pas grand-chose, on n'achète pas de bons meubles parce qu'on
sait qu'on va devoir bouger bientôt et que ça fera trop à déménager.
C'est assez minimaliste comme mode de vie. Avant, on avait des grands
lits, maintenant on est tous passés à des plus petits parce qu'ils sont
plus simples à déplacer. On sait qu'on ne va pas rester ici
indéfiniment. On choisit ce qu'on possède en fonction de nos mouvements.

--- Tout le monde ici souhaite avoir un lieu de vie pour au moins deux ou
trois ans, un espace pour penser à d'autres choses, pour pouvoir\index[tags]{pouvoir} faire
des projets.

--- Dans l'intervalle, on est plus fortexs ensemble, on se donne du
courage mutuel. Ça serait très effrayant sinon. Et ce qui nous aide
aussi, c'est de croire profondément que ce qu'on fait est bon. Ça n'est
pas mal, nous ne faisons de mal à personne. On ne doit pas se sentir
coupables d'occuper des lieux. C'est à elleux de se sentir coupables de
laisser des bâtiments vides alors que des personnes dorment dans la rue\index[tags]{rue} :
avoir cette conviction\index[tags]{conviction} nous aide à faire face à la police et aux
propriétaires, ça nous rappelle que notre action est légitime et
nécessaire.

[^34]: La question du profilage racial quotidien est abordée dans *They don't see us*\ \[n^o^\ 4\].

[^35]: Dans *Un jour j'ai poussé la porte d'un hangar tout pété*\ \[n^o^\ 26\], une militante raconte les remises en question qui la traversent.

