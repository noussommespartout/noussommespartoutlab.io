---
author: "Anonyme"
title: "Drones"
subtitle: "Grandeur et misère du feu d'artifice antipolice"
info: "Texte rédigé pour le recueil"
date: "mai 2020"
categories: ["manifestation, émeute", "police", "violence, non-violence", "relation à la militance", "sabotage, action directe"]
tags: ["index"]
lang: ["fr"]
num: "1"
quote: "l'émeute est l'expérience d'une reprise en main de ta réalité sociale"
---

Une fois, j'ai tiré un feu\index[tags]{feu} d'artifice sur une rangée de flics. C'était
probablement l'un des moments les plus intensément politiques de ma vie.

C'était en Suisse, pendant une manifestation. On avait marché près d'une
heure et, pour être honnête, on ne formait pas un cortège très
impressionnant, on ne devait pas alerter grand monde. Mais de la masse
bouillonnante de colère\index[tags]{colère} s'échappaient de petits groupes plus mobiles qui
s'engouffraient dans les rues, esquivant les fourgons, pour distraire le
dispositif de sécurité, s'emparer de l'inemparable en attaquant les
antiémeutes ou en refaisant la peinture du mobilier urbain (c'est comme
ça que les médias appellent les vitrines des banques).

La police avait sorti les costumes noirs, les canons à eau\index[tags]{eau}, les lanceurs
de balle de défense. Le message était clair, il est toujours très clair :
nous sommes le bras armé, la milice dont la violence\index[tags]{violence} est légitime par
nature, toujours couverte, toujours impunie, et les rues sont à nous,
nous sommes les gardiens de l'acceptable, si vous osez franchir les
frontières qu'on assigne à vos mouvements, si vous contestez la zone
autorisée pour l'exercice de votre pouvoir\index[tags]{pouvoir}, on vous éclate en morceaux.
Ils n'ont pas de visage\index[tags]{visage}, aucun corps\index[tags]{corps} reconnaissable. Les experts en
design qui gèrent leur apparence étudient leurs uniformes dans cet
objectif précis : cacher leur humanité. Même s'ils sont terrifiants, ce
n'est pas tellement pour faire peur\index[tags]{peur}, c'est pour qu'ils restent
acceptables. Quand on les verra à la télé[^3], casser des membres ou
tirer à bout portant sur des manifestanxtes, ils ne ressembleront pas à
des humains. S'ils ressemblaient à des humains, les images seraient bien
plus dures à accepter, elles laisseraient un goût plus amer dans la
bouche des téléspectateurixes. On verrait, dans les rues suisses, des
humains ouvrir le feu\index[tags]{feu} à bout portant sur des humains. Mais sans visage\index[tags]{visage},
sans corps\index[tags]{corps} et sans nom, on ne voit que des drones bipèdes, un genre
d'instrument, éventuellement un idéal type de fonctionnaire en prise
avec le terrain, jamais des êtres sentients. L'intérêt des robots, à
part qu'ils rendent bien à l'image, c'est qu'ils ne produisent pas le
spectacle du libre arbitre, juste l'image d'une société froidement
organisée qu'on se contentera d'accepter lascivement avant de zapper sur
une autre chaîne.

C'est assez difficile, presque un peu douloureux, de considérer qu'un
pauvre feu\index[tags]{feu} d'artifice sans conséquence soit l'un des moments
les plus intenses de ma vie politique. Je le dis par sincérité, parce
que je ne veux pas nier cette sensation, mais mon propre vécu me déçoit
un peu. Je trouve pas très réjouissant d'avoir trouvé ça aussi intense.

D'abord, et essentiellement, parce que j'ai eu recours à la violence\index[tags]{violence}. Ma
fusée les a à peine touchés, mais j'aurais pu blesser un visage\index[tags]{visage},
déchirer une bouche\index[tags]{bouche}, crever un œil, atteindre quelque chose de la chair
qu'un des drones dissimule sous son uniforme. Le pétard aurait pu se
coincer derrière le bouclier antiémeute ou, pire, derrière la visière.
C'est déjà arrivé, ici en Suisse, que la rage\index[tags]{rage} politique d'une personne
en défigure une autre, qu'elle transforme à jamais le reste de sa vie en
une vie vraiment sans visage\index[tags]{visage}, tout ça parce qu'un jour elle a décidé de
se mettre au service de l'État. Et c'est triste à pleurer. C'est triste
à pleurer de vivre un monde dans lequel la violence\index[tags]{violence} est une réponse
légitime et nécessaire à la violence\index[tags]{violence}[^4]. À peu près trois millénaires
de théorie\index[tags]{théorie} politique pour en arriver là.

Ensuite, mon propre vécu me déçoit, parce que j'en ai vécu d'autres, des
moments que j'aurais pu ressentir comme politiques avec beaucoup plus
d'intensité. J'ai collaboré à des projets collectifs incroyables, eu la
chance de participer à des cercles de parole\index[tags]{parole} dans lesquels les gens
cherchent à se réparer ensemble, collaboré à des initiatives qui m'ont
laissé entrevoir qu'une autre organisation sociale était possible,
fondée sur la bienveillance\index[tags]{bienveillance} et sur l'amour\index[tags]{amour/love/amoureuxse}. Tout ça pour avoir enfin
véritablement la sensation d'exister en allumant quelques secondes une
fusée fabriquée en Chine dans une rue\index[tags]{rue} sans importance. Certaines voix
s'élèveront, elles vous parleront de "casseurs sociopathes", d'une
jeunesse sans repères qui se défoule en saccageant des vitrines, d'une
génération qu'elles appelleront "Orange Mécanique" sans voir qu'elles
sont les seules vraies nihilistes de la farce. Elles ne comprennent
rien. L'essentiel de nos vies militantes est fait d'amitié, de
bienveillance, de remise en question, de réflexions, de transformation
de soi, de recherche d'une harmonie entre accomplissement individuel et
liberté collective[^5]. Nonante-neuf pour cent d'amour\index[tags]{amour/love/amoureuxse}, un pour cent
de feu\index[tags]{feu} d'artifice. Deuxième facteur de tristesse : nous rêvons d'un
monde sans feu\index[tags]{feu} d'artifice, et moi, c'est le feu\index[tags]{feu} d'artifice qui me
comble.

Enfin, mon expérience m'attriste, parce qu'on ne peut pas cacher le
shoot d'adrénaline\index[tags]{adrénaline}, la satisfaction morbide du combat de rue\index[tags]{rue}. On en
parle très rarement, entre praticiennexs de l'émeute, de ce que ça fait
de se jeter dans cette joie-là. C'est fondamental, la joie de la
mobilisation collective, mais il y a cette petite part sombre qu'on
n'aime pas évoquer. Parfois on pleure après, c'est douloureux, ça fait
peur de s'affronter dans les rues, même quand c'est du théâtre\index[tags]{théâtre}, un
simulacre de combat (comme le sont la plupart des manifestations en
Suisse). Parfois, même si on s'en passerait bien dans le monde d'après,
on vit avec, c'est comme ça, ça fait partie de la lutte. Parfois, on
s'en vante, il y en a qui sont un peu addicts, il y a aussi quelques
gros bras trop déterminés qui en font l'éloge, et là on touche au
virilisme du feu\index[tags]{feu} d'artifice. Sociologiquement, le black bloc est
constitué d'environ un tiers de personnes qui ne sont pas des mecs cis°
(et de plus de deux tiers d'universitaires, mais ça c'est un autre sujet). Le
problème de fond, c'est un problème d'imaginaire. On a envie d'en
imposer, de montrer qu'on est pas prostréexs dans leur domination et
enchaînéexs à leur représentation du monde. On a envie d'anéantir leur
sentiment d'impunité. On a envie que la peur\index[tags]{peur} change de camp. Alors on
les affronte, on leur montre que s'ils veulent jouer le jeu de la
violence, on jouera, que si le combat est le seul langage qu'ils
comprennent, c'est celui qu'on parlera. On veut démontrer que leur
pouvoir ne repose ni sur la bienveillance\index[tags]{bienveillance}, ni sur l'empathie\index[tags]{empathie}, ni sur la
délibération, mais bien sur une horde\index[tags]{horde} de drones qu'ils sortent quand ils
n'arrivent plus à cacher la mascarade ; et il n'y a que quand on
commence à tirer des feux d'artifice qu'ils n'arrivent plus à cacher la
mascarade. Mais en faisant ça, on tombe inévitablement dans le piège de
l'adrénaline\index[tags]{adrénaline} et du virilisme. Leur toile imaginaire nous capture et nous
pousse à vivre le combat en reproduisant un rapport masculin et
autoritaire à nos émotions. C'est extrêmement difficile d'exercer une
violence sans mobiliser un ensemble de repères affectifs et
intellectuels liés à la virilité et à la loi du plus fort. On ne connaît
pas d'autre modèle pour comprendre, expérimenter et raconter le combat
auquel ils nous obligent. Et là ça donne ce qu'on sait, les concours de
bites militants sur le nombre de gardes à vue ou de coups échangés avec
les keufs. Si tu dis que tu pleures après chaque manif un peu agitée,
y'aura toujours une voix\index[tags]{voix} pour invoquer des valeurs guerrières, comme le
courage et la force\index[tags]{force}, pour te faire sentir comme une merde\index[tags]{merde}, pour te
dévaloriser. Et cette voix\index[tags]{voix}, avec son adhésion sans doute\index[tags]{doute} inconsciente à
l'imaginaire des oppresseurs, avec tout le cortège de discriminations
qu'elle embarque, cette voix\index[tags]{voix} trop virile, elle n'a pas compris qu'elle
était devenue elle aussi la voix\index[tags]{voix} d'un drone\index[tags]{drone}. Mais malgré tout, dans ce
qui m'a rendu mon geste aussi intensément politique, il y a un peu de
virilisme que j'ai du mal à affronter.

Donc voilà où ça nous mène, voilà les causes de la tristesse, la racine
de toute l'organisation foireuse du sentiment qui fait qu'on en vient à
vivre aussi intensément un geste dérisoire. Mais, outre la tristesse,
quelles sont les causes de l'intensité ? C'est sans doute\index[tags]{doute} que l'émeute
est une forme d'expérience politique sensible qui n'est accessible dans
aucune autre situation de la vie ordinaire, aussi militante soit-elle.
L'émeute est l'expérience d'une reprise en main de ta réalité sociale,
un bloc de sensation qui mélange la possibilité de faire ce que tu veux
des rues, des murs, de tout l'espace qui s'impose à toi et la sensation
de s'autodéterminer collectivement, d'aller dans le sens que l'on veut
et de n'obéir à rien d'autre qu'au sentiment d'être un groupe. Ce qu'il
se passe, c'est que tu te reconnectes avec la sensation d'être vivanxte,
c'est pour ça que c'est le contraire du nihilisme. Et ça en dit long sur
l'ensemble des processus qui cherchent à tuer en nous le sentiment
d'exister, ça en dit long sur la construction sociale de l'angoisse, de
l'aliénation putride et de la haine de soi. Allumer la mèche d'un feu
d'artifice, c'est à la fois le vertige que tu ressens face à la liberté
véritable, à la fois une décision qui t'ancre profondément dans le sol,
qui te remet les pieds sur le plancher et qui te donne l'impression de
fouler la terre pour la première fois en ayant pleinement conscience de
ce que tu es. Plus qu'une loi, un ordre social ou un système politique,
tu contestes ce qu'on a fait à ta vie, ce que l'ensemble de l'humanité
s'est mise d'accord pour définir comme étant "la vie", une trajectoire
existentielle en forme de marchandise négociable, une somme de gestes
quotidiens qui te sont profondément étrangers, toute la sociabilité
convenue comme l'image d'un monde auquel tu collabores sans y croire,
sans t'y reconnaître, sans avoir la sensation d'y vivre.

Mes sensations me dérangent, mais c'est comme ça. Dans la configuration
actuelle de nos sociétés supposément démocratiques, vu comme on nous
coupe de toute emprise sur le réel, vu comme on noie le sentiment
critique et affectif d'avoir le pouvoir\index[tags]{pouvoir} sur sa propre vie, vu comme tout
est gris et sécurisé, c'est comme ça. Une fois, j'ai tiré un feu
d'artifice dérisoire sur une rangée de flics que ça n'a pas inquiétés.
C'était probablement l'un des moments les plus intensément politiques de
ma vie. Et je le referais sans hésiter.

[^3]: *Spectacle nulle part.* Care *partout*\ \[n^o^\ 23\] dresse une critique de la (non)couverture des actions policières par les médias.

[^4]: *Survivre dans un black bloc*\ \[n^o^\ 15\] aborde la violence comme moyen de contestation légitime.

[^5]: Certains autres textes du recueil décrivent ces aspects de la vie militante : *Un jour, j'ai poussé la porte d'un hangar tout pété*\ \[n^o^\ 26\], *La fête est finie*\ \[n^o^\ 20\] et *Quand l'espace s'étire *\[n^o^\ 27\].

