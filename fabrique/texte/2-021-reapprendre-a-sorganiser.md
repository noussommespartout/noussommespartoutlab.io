---
author: "Anonyme"
title: "Réapprendre à s'organiser"
subtitle: "Quelques outils pour une communication et une organisation interne horizontales"
info: "Texte rédigé pour le recueil"
date: "octobre 2020"
categories: ["DIY", "autogestion, expérimentations collectives"]
tags: ["index"]
lang: ["fr"]
num: "21"
quote: "le silence est bienvenu"
---

Les modes de communication et d'organisation que nous mettons en place
naturellement sont issus de la société dans laquelle nous nous sommes
construixtes et dans laquelle nous vivons. Instinctivement, nous
reproduisons les codes et les réflexes d'une société qui s'organise
hiérarchiquement, qui valorise la spécialisation et dans laquelle
certaines voix\index[tags]{voix} prennent naturellement plus d'importance que d'autres.

Lorsque nous voulons créer des alternatives et que nous voulons militer
ensemble et horizontalement, il nous faut nous réinventer, apprendre des
nouvelles formes d'écoute et de partage. Dans l'autogestion anarchiste
ou autonome, il ne s'agit pas de créer des espaces sans règles et sans
cadres, mais de construire et d'organiser des alternatives qui
permettent une répartition des pouvoirs, des tâches chiantes, des tâches
valorisantes. Il s'agit d'inventer des outils qui ne sont pas imposés
par des patronnexs, des cheffexs, des États et des polices, mais qui
sont créés par le bas, par touxtes, des outils amovibles et mouvants et
non des lois gravées dans le marbre.

Il existe certainement des livres d'outils théoriques sur ces questions,
mais je n'essaierai pas de les reproduire ici. Dans les collectifs
auxquels j'ai participé, nous nous sommes forméexs sans livres,
peut-être plus lentement que si nous avions appliqué un savoir déjà
constitué et reconnu, par tâtonnements, en créant les outils qui nous
convenaient selon nos situations, nos particularités, nos objectifs.

Voici une liste d'outils que j'ai utilisés et que j'ai participé à
mettre en place ici et là, pour créer des modes d'organisation et de
communication plus horizontaux. Comment communiquer lorsque nous ne
parlons pas la même langue et que nous n'avons pas les mêmes bagages
éducatifs ou expérientiels ?

## Le silence

### Pourquoi ?

Dans une société où parler fort et prendre de la place sont des
constructions sociales valorisées, l'instauration et l'accueil du
silence peuvent se révéler un outil\index[tags]{outil} puissant ; c'est l'outil\index[tags]{outil} de celleux
qui ne peuvent pas sauter sur la parole\index[tags]{parole}, c'est un outil\index[tags]{outil} qui permet de
prendre le temps de réfléchir et d'écouter. Le silence\index[tags]{silence} peut aider à
rétablir un semblant d'horizontalité\index[tags]{horizontal/horizontalité}.

Il nous a fallu un peu de temps pour que le silence\index[tags]{silence} soit accueilli dans
nos interactions. Quelqu'un a dit l'autre soir, durant une réunion\index[tags]{réunion}, "si
la qualité de nos liens se calcule à la qualité de nos silences, alors
on est bien". Et oui, les silences étaient de qualité pendant cette
réunion. Ce sont de réels instants de partage, des instants pendant
lesquels on digère ce qui est dit.

### Comment accueillir le silence ?

- Au début d'une discussion ou d'une réunion\index[tags]{réunion}, annoncer simplement que
  le silence\index[tags]{silence} est bienvenu et que l'on peut tenter collectivement de
  lui laisser une place réelle dans les interactions. Le dire permet
  d'évacuer une bonne partie de la gêne qui peut s'installer pendant
  les moments de silence\index[tags]{silence}. En fait, assez rapidement, il n'y a plus
  besoin de le dire, car le silence\index[tags]{silence} devient un vrai participant.
- Instaurer des tours de paroles avec un temps délimité : cinq minutes
  par personne, par exemple. Si une personne dépasse le temps de
  parole\index[tags]{parole} accordé, on l'interrompt. Si la personne s'arrête de parler
  après trois minutes, on laisse le temps restant s'écouler en
  silence\index[tags]{silence}.
  Lorsque des traductions sont nécessaires, il ne faut pas les faire
  pendant ces deux minutes de silence\index[tags]{silence}. Il vaut mieux attendre que ce
  temps se soit entièrement écoulé.
- Pour distribuer la parole\index[tags]{parole}, au lieu de faire des tours de table
  pendant lesquels chacunex parle à son tour, on peut utiliser les
  formules "je prends" et "je laisse". "Je prends", lorsque je
  souhaite parler et "je laisse", lorsque j'ai terminé. Cet outil
  permet de laisser la place au silence\index[tags]{silence} si on annonce dès le début
  qu'il est possible de garder la parole\index[tags]{parole} sans rien dire. En plus,
  puisque l'ordre des prises de parole\index[tags]{parole} n'est pas défini à l'avance,
  lorsque quelqu'unex laisse la parole\index[tags]{parole}, on ne ressent pas l'obligation
  de sauter sur la parole\index[tags]{parole}.

## L'écriture

### Pourquoi ?

Nous ne sommes pas touxtes égalexs face à l'écriture, mais encore moins
face à la parole\index[tags]{parole}. Il ne s'agit pas ici d'écrire des longs textes, mais
de laisser des espaces pour que chacunex puisse prendre un temps à soi
pour écrire une idée ou une envie sans être stresséex de devoir parler
vite, de devoir répondre tout de suite aux choses qui sont dites ou de
parler en public.

### Comment ?

- **La criée** est une boîte où il est possible de glisser des idées,
  de partager des mots d'amour\index[tags]{amour/love/amoureuxse} ou des déceptions, des envies ou encore
  des rêves. Une criée peut être un objet physique dans un lieu
  précis, il reste là, on y glisse des mots pendant les journées ou
  les semaines de discussion et d'organisation collectives. On peut
  ensuite choisir des moments communs de partage pendant lesquels on
  fait tourner cette boîte. Chaque main s'y glisse, prend un billet et
  le lit à voix\index[tags]{voix} haute. Toutes ces voix\index[tags]{voix} peuvent ainsi être entendues.
  Parfois, il s'agit de traiter des situations précises ensemble,
  parfois on peut choisir de réagir, parfois on peut simplement être à
  l'écoute.
  - La criée peut servir à gérer un partage émotionnel collectif,
    mais elle peut aussi permettre de faire un ordre du jour
    participatif ou d'organiser un partage d'idées et de thématiques
    à discuter en réunion\index[tags]{réunion}. Il est parfois plus évident d'avoir un
    temps de réflexion seulex, que de devoir s'exprimer devant tout
    le monde.
  - Attention, à mon avis, la criée ne doit jamais servir de tribune
    publique pour les dénonciations. S'il existe des conflits à
    l'interne, il vaut mieux les traiter d'une autre façon. Il faut
    éviter de dénoncer publiquement une personne en particulier dans
    une criée anonyme. L'impact émotionnel peut être trop fort. La
    criée peut servir à dénoncer des insuffisances collectives, mais
    pas individuelles.
- **Le butinage.** Pendant un butinage, on répartit des grandes
  feuilles sur des tables. Après un brainstorming (écrit ou oral), on
  écrit sur chaque grande feuille l'un des thèmes que l'on a décidé de
  traiter. Ensuite, on se promène avec un stylo à la main de table en
  table, de feuille en feuille, et on écrit des mots, des phrases, on
  discute sur papier, on tourne et on laisse le butinage se faire
  naturellement. Ce qui en sort, ce sont ces feuilles où chaque thème
  a pu être nourri par chacunex et où chaque mot a pu mûrir dans des
  esprits tranquilles.

## Les groupes restreints

### Pourquoi ?

Parler en public est plus simple pour certaines personnes que pour
d'autres. Parler devant 10, 15 ou 20 personnes, c'est pas facile. Les
plénières traditionnelles sont un haut lieu de prise de pouvoir\index[tags]{pouvoir} où
celleux qui ont le plus de facilité à s'exprimer ou le plus
d'informations prennent de la place et installent un rapport de pouvoir\index[tags]{pouvoir}.
Prendre des décisions en plénière est particulièrement compliqué car les
débats se polarisent souvent autour de quelques personnes et deviennent
inégaux. Se répartir en petits groupes de discussion permet de diminuer ces prises de pouvoir\index[tags]{pouvoir}.

### Comment ?

- C'est tout simple : on se répartit en petits groupes pour discuter
  d'un ou plusieurs sujets avant de revenir en plénière. Lorsque tout
  le monde est à nouveau réuni, une personne par groupe peut résumer
  ce qui s'est dit afin de mettre en commun.

Lorsqu'il s'agit de sujets sensibles, on n'est pas obligéexs de tout
raconter dans la mise en commun. Dans les petits groupes, on se confie
plus facilement, on se permet d'aller dans plein de directions pour
ensuite se reprendre, etc.

- Par groupe de 3 : une personne parle d'un sujet pendant une durée
  définie, une deuxième personne l'écoute de façon active. Iel pose
  des questions ou lae relance s'iel s'essouffle, mais n'émet ni
  commentaire ni jugement. La troisième personne prend des notes de ce
  qui est dit. À la fin du temps défini, les rôles s'inversent. Quand
  tout le monde a parlé, on prend un moment pour synthétiser,
  comprendre là où l'on se rejoint et là où les idées diffèrent.
- Cette méthode est intéressante pour aborder des questions de
  fond ou des questions très ouvertes. Elle permet d'aller en
  profondeur et de tisser des liens entre nos différents vécus.
  Elle permet aussi de s'exprimer en toute confiance\index[tags]{confiance}, avec peu de
  gens, et d'avoir un moment où toutes les personnes se sentent
  vraiment écoutées et prises en considération.

## Le corps et l'espace

### Pourquoi ?

Le théâtre\index[tags]{théâtre}-image est un outil\index[tags]{outil} d'éducation populaire que nous avons testé
à plusieurs reprises dans nos collectifs et qui a permis, à chaque fois,
de faire émerger des émotions, des sujets profonds et sincères. Il
permet, à l'aide de nos corps\index[tags]{corps} et de différents objets, d'exprimer des
choses que nous n'exprimerions pas forcément avec la parole\index[tags]{parole}. Il permet
aussi de prendre du temps pour penser autrement notre rapport au groupe.
Cet outil\index[tags]{outil} peut être utilisé pour traiter de différents sujets, mais
celui du rapport de chacunex au groupe fonctionne particulièrement bien.

### Comment ?

On met en place une scénographie avec ce qu'on a sous la main.
Souvent, on utilise des chaises ou des canapés, pour signifier
différentes positions dans l'espace. La scénographie se construit en
lien avec la question qui est posée et que l'on souhaite traiter. Si
on veut parler du rapport de l'individu au groupe, il est
intéressant de jouer avec la "centralité" en mettant certains
éléments très au centre et d'autres plus en périphéries, dans les
recoins. On peut également jouer avec la verticalité, avec la
hauteur, en plaçant, par exemple, une chaise sur une table *et* au
centre de l'espace ainsi que des chaises basses sur le sol.

Ensuite, on peut orienter les chaises dans différentes directions :
tournées vers le centre ou, au contraire, dos au centre. On peut aussi
déséquilibrer une chaise en la plaçant sur une surface inégale ou créer
toutes sortes de scénographies adaptées aux sujets qu'on a envie de
traiter.

Lorsque la scénographie est prête, on pose trois questions au groupe et
chacunex peut ensuite aller se placer à l'endroit où iel le souhaite
selon son ressenti par rapport au groupe.
- Première question : où te situes-tu dans le groupe ?
  Deuxième question : où penses-tu que les autres te situent dans le groupe ?
  Troisième question : où aimerais-tu être ?
- Après chaque question posée, chacunex circule dans l'espace et
  va se positionner sans bruit. Ensuite, on pose la deuxième
  question, puis la troisième. À la fin de l'exercice, chacunex
  sort de la scénographie. Pour la mise en commun, chacunex, à son
  tour, va refaire son trajet en expliquant aux autres son
  parcours et les raisons qui l'ont pousséex à se positionner dans
  les différents endroits choisis.
- Cet outil\index[tags]{outil} permet de faire voir des choses qui restent souvent
  invisibles, il conduit parfois à des moments émotionnels forts.
  C'est un outil\index[tags]{outil} qui peut être pertinent dans plusieurs contextes,
  mais il est particulièrement intéressant à mettre en place dans
  un groupe déjà formé depuis un certain temps.

## La charge mentale et les tâches ingrates

On le sait, une histoire de vaisselle pas faite, de frigo qui pourrit ou
de chiottes sales peut suffire à faire péter un collectif. Au-delà de
ces tâches ingrates et souvent invisibles, comment fait-on pour répartir
les charges mentales qui prennent souvent beaucoup d'ampleur dans les
lieux collectifs ? Les enjeux sont d'éviter les "spécialisations" et
de valoriser et visibiliser chaque tâche effectuée pour éviter les
frustrations et les rancœurs.

Mais concrètement, comment peut-on le faire ? D'autant plus qu'il y a
plusieurs visions qui s'affrontent : celle qui consiste à dire "je ne
veux pas de règles et de contraintes" et celle qui consiste à dire
"j'ai besoin de règles et de répartition des tâches pour ne pas
culpabiliser ou pour ne pas en faire trop".

Différents modes d'organisations ont vu le jour, aucun n'a jamais fait
l'unanimité, mais un mode d'organisation en particulier m'a semblé
pertinent et intéressant et a été, en ce qui me concerne, très
satisfaisant.

Comment visibiliser toutes les tâches et se rendre compte de leur
répartition au sein du collectif ?

- Distribuer des tas de post-it à chaque personne pour qu'elle y note
  toutes les tâches qu'elle prend en charge au sein du collectif :
  nettoyage du frigo, des chiottes, tenue du bar, achats, travaux,
  accueil, réponses aux mails, écriture des newsletters, finances,
  etc. Chaque tâche est écrite, de la plus petite à celle qui prend le
  plus de temps, de la moins valorisante à la plus valorisante. On
  place ensuite tous ces post-it sur une grande table. On peut tenter
  de les répartir par "pôles" : administration\index[tags]{administration/administratif}, entretien de la
  maison, communication extérieure, etc.
  - Tous ces post-it permettent d'avoir une vue d'ensemble sur tout
    ce qu'on fait au sein du collectif et de mettre en valeur toutes
    les tâches. On se rend aussi vite compte qu'il y a des tâches
    qui sont effectuées par plusieurs personnes et d'autres tâches
    qui reposent sur la même personne. L'idée n'est pas de comparer
    le nombre de tâches effectuées par chacunex. Pour éviter cette
    dynamique, on peut poser les post-it anonymement. Il s'agit
    simplement de se rendre compte de tout ce qu'on fait, parce que
    c'est valorisant, et de prendre conscience de la répartition du
    travail au sein du collectif.
- On peut ensuite développer l'exercice en tirant au sort les
  personnes qui seront chargées de s'occuper d'un des "pôles"
  pendant une période déterminée, un trimestre par exemple.
  Lorsqu'aucune des personnes tirées au sort n'a les connaissances ou
  compétences nécessaires à la "gestion" de ce pôle, on refait un
  tirage au sort. Le groupe formé par les personnes qui ont été tirées
  au sort a ensuite la responsabilité, la charge mentale et une partie
  de la charge de travail du pôle en question. On peut et doit bien
  sûr s'investir et agir en dehors de ces pôles attitrés et on n'a pas
  à effectuer toutes les tâches qui concernent ces pôles, mais on a
  pour mission de savoir le plus possible, ce qui est déjà fait et ce
  qui est encore à faire. Chaque groupe s'organise ensuite comme il le
  souhaite. Puis, à chaque trimestre, on change.
- Cette technique permet de répartir la charge mentale et de mieux
  s'organiser. Elle permet aussi d'apprendre de nouvelles choses,
  puisqu'on change de pôle après chaque trimestre, et de se rendre
  compte de tout ce qui est fait par chaque personne, dans le but de
  valoriser les rôles de chacunex et d'éviter les frustrations du
  travail invisible. Ces tournus par petits groupes tirés au sort
  permettent que chacunex s'organise avec des personnes avec
  lesquelles iel n'aurait pas eu le réflexe de former un groupe.

À nous d'inventer, d'adapter, de piocher ici et là et de chercher des
solutions qui conviennent au plus grand nombre et au plus de
sensibilités possibles. L'envie de continuer à militer vient aussi de
là : des configurations de socialisation, d'écoute et d'organisation
nouvelles.

