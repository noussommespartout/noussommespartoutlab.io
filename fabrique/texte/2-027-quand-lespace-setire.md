---
author: "Anonyme"
title: "Quand l'espace s' é t i r e"
subtitle: "La mixité choisie"
info: "Texte rédigé pour le recueil"
date: "août 2020"
categories: ["autogestion, expérimentations collectives", "relation à la militance"]
tags: ["index"]
lang: ["fr"]
num: "27"
quote: "c'est l'excitation d'exister ensemble"
---

\setlength{\parindent}{0em}

\[*La mixité choisie° est une pratique consistant à organiser des
rassemblements réservés aux personnes appartenant à un ou
plusieurs groupes sociaux opprimés ou discriminés, en excluant la
participation de personnes appartenant à d'autres groupes considérés
comme potentiellement discriminants (ou oppressifs), afin de ne pas
reproduire les schémas de domination sociale. La pratique militante est
utilisée par certains groupes se réclamant de divers courants, notamment
dans le féminisme, l'antiracisme, les mouvements LGBTQIA+, les mouvements
de minorités de genre ou de personnes en situation de handicap.*\]

\vspace{1\baselineskip}

Quand on se retrouve en mixité choisie, tout d'un coup, on crée de la
place.

L'espace qui se dégage, et qui est à prendre, s'étire et tout devient
large.

\vspace{1\baselineskip}

\begin{nscenter}
s~~~p~~~a~~~c~~~i~~~e~~~u~~~x
\end{nscenter}

\vspace{1\baselineskip}

Dans mon expérience, il y a parfois des silences.

\[\hspace{2em}\]

\hspace*{0.2\textwidth}\[\hspace{2em}\]

\hspace*{0.4\textwidth}\[\hspace{2em}\]

Et ces silences,

\hspace*{0.6\textwidth}\[\hspace{2em}\]

\hspace*{0.8\textwidth}\[\hspace{2em}\]

c'est du vide.

\hspace*{\fill}\[\hspace{2em}\]

Et le vide, ça peut se combler,

le vide c'est du possible,

c'est de la place à occuper,

c'est comme une friche,

c'est comme une brèche\index[tags]{brèche}.

\vspace{1\baselineskip}

Parfois, il n'y a pas de silence\index[tags]{silence},

il y a de l'euphorie,

celle qu'on ne s'autorise pas,

partout,

tout le temps.

\vspace{1\baselineskip}

C'est l'excitation d'exister ensemble.

C'est la joie du refus :

le refus d'accepter,

en tout temps,

la cohabitation avec les discriminations systémiques.

\vspace{1\baselineskip}

Parfois, il y a de la magie,

les choses deviennent limpides,

évidentes,

elles coulent de source,

alors qu'elles semblaient

si difficiles à exprimer

et à expliquer

ailleurs,

dans les autres espaces-temps

si étriqués.

\vspace{1\baselineskip}

Parfois, c'est intense,

et on pleure

les plus chaudes

et les plus réconfortantes

\clearpage
l

a

r

m

e

s

de nos glandes l
de nos glandes l

\phantom{de nos glandes~~~~}a

\phantom{de nos glandes}c

\phantom{de nos glandes~~~~}r

\phantom{de nos glandes}y

\phantom{de nos glandes~~~~}m

\phantom{de nos glandes}a

\phantom{de nos gland}l

\phantom{de nos gla}e

\phantom{de nos~~}s

\phantom{de nos~~~~}abîmées.

\vspace{1\baselineskip}

La mixité choisie,

c'est pas un projet de société,

c'est un outil\index[tags]{outil},

c'est une stratégie,

c'est du repos.

\vspace{1\baselineskip}

Se retrouver en mixité choisie,

c'est aussi être toujours conscienxtes de sa propre socialisation°.

On est là pour socialiser différemment[^72],

pour parler avec des nouveaux mots,

et avec des nouvelles intonations,

pour reconfigurer les relations humaines,

pour construire les structures sociales de lendemains meilleurs,

pour créer des stratégies de soin,

pour se regarder dans les yeux avec la joie de se découvrir

ou, plutôt, avec la joie de se *re-*découvrir,

pour construire des réparations,

pour apprendre à nous aimer, parce que souvent,

on nous a appris à nous méfier les unexs des autres,

à nous trouver moins bien,

moins cools,

moins fortexs que les autres.

\vspace{1\baselineskip}

Parfois, on est mal à l'aise.

Parfois, on se sent faibles.

C'est difficile de l'admettre.

\vspace{1\baselineskip}

C'est difficile d'admettre,

que dans un groupe de 15 personnes,

quand il n'y a pas de mecs cis°,

on manque de certaines compétences,

parce qu'on ne nous les a pas transmises,

on nous les a refusées.

\vspace{1\baselineskip}

C'est difficile d'admettre,

que dans un groupe de 15 personnes,

quand il n'y a pas de blanchexs,

on manque de certaines ressources,

parce qu'on ne nous y a pas donné accès,

parce qu'on nous les a volées.

\vspace{1\baselineskip}

Mais on invente des solutions,

on apprend,

l'écologie des moyens

et surtout,

on apprend,

on reprend,

on cherche,

on vole.

\vspace{1\baselineskip}

Parfois, on organise

des ripostes de rêve,

on passe à l'action.

C'est dommage,

mais ça va plus vite,

en mixité choisie.

Alors, c'est la joie de CRIER\index[tags]{crier},

\vspace{1\baselineskip}

\begin{nscenter}
dagirdirectementsansintermédiaires\\sansdemandersansexpliquerdagirvite
\end{nscenter}

\vspace{1\baselineskip}

\begin{nscenter}
C'EST

\fbox{L'AUTO}

ÉMANCIPATION
\end{nscenter}

\vspace{1\baselineskip}

\begin{nscenter}
C'EST PAR NOUS

ET C'EST POUR NOUS
\end{nscenter}

\vspace{1\baselineskip}

\begin{nscenter}
C'EST UNE ARME

GUERRIÈRE

ET AMOUREUSE
\end{nscenter}

\setlength{\parindent}{1em}

[^72]: Sur des manières d'organiser alternativement un collectif : *Réapprendre à s'organiser*\ \[n^o^\ 21\].
