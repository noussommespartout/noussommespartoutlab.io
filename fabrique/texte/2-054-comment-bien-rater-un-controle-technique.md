---
author: "Anonyme"
title: "Comment bien rater un contrôle technique ?"
subtitle: "Éléments de sabotage automobile"
info: "Texte rédigé pour le recueil"
date: "avril 2020"
categories: ["DIY", "sabotage, action directe"]
tags: ["index"]
lang: ["fr"]
num: "54"
quote: "une question essentielle de la postmodernité"
---

Avec la démocratisation des transports motorisés individuels, le
contrôle technique est devenu une formalité administrative bien connue
des automobilixstes. Dans une bonne partie des pays occidentaux, le
contrôle technique est imposé une ou deux fois par an. L'individu doit
alors amener son véhicule personnel chez unex garagixste reconnuex et
agrééex de manière à le faire évaluer. Si la mécanique fonctionne bien,
lae professionnellex délivre alors à l'automobilixste un certificat
l'autorisant à continuer l'utilisation de son véhicule.

Pour celleux que la pollution ou l'extraction énergétique préoccupent,
il est devenu de plus en plus difficile d'échouer à ce type de formalité,
ce qui soulève une question essentielle de la postmodernité : comment
bien rater un contrôle technique ?

Naturellement, tout le monde le sait, ce n'est pas chose aisée. Les
garages automobiles sont toujours plus laxistes, l'état ferme les yeux
sur les véhicules non homologués, les entreprises redoublent
d'inventivité pour importer toujours plus vite les inévitables pièces de
rechange, le lobby du contrôle-technique-réussi est de plus en plus
influent auprès des parlementairexs, bref, rater son contrôle technique
est devenu un vrai casse-tête. Mais ne vous découragez pas, avec un peu
d'huile\index[tags]{huile} de coude, tout est possible !

On rassemble ici quelques astuces simples et efficaces, proposées par
les meilleurexs expertexs en échec, qui aideront chacunex à foirer au
mieux leurs contrôles techniques. Surtout, parce que l'entraide et la
solidarité sont des valeurs essentielles, ces astuces pourront aussi
être appliquées sur les véhicules des autres (en toute discrétion bien
sûr, le vrai altruisme, c'est de ne pas se vanter). Ouvrez l'œil, il y
a sûrement autour de vous des véhicules appartenant à des propriétairexs
moins débrouillardexs qui ne demandent qu'à échouer eux aussi à leur
contrôle technique. Très souvent, en tant que citoyennex éclairéex, il
ne s'agit que d'un renvoi d'ascenseur bien mérité ! N'hésitez pas à
aider directement les voitures de police (qui protègent la population),
les engins de chantier (qui embellissent nos villes de magnifiques
centres commerciaux) ou encore les voitures haut de gamme garées dans
les quartiers riches (qui permettent aux plus aiséexs de nos
concitoyennexs de faire ruisseler leurs richesses sur les moins
aiséexs).

## Quelques précautions préalables

Bien rater un contrôle technique implique quelques précautions[^160] :

- Manipuler l'ensemble de son matériel, si possible dès son achat ou
son déballage, avec des gants épais de chantier. Les gants en silicone
fin ou en laine sont à proscrire.
- Enrouler l'ensemble de son matériel mécanique dans du ruban adhésif
noir, pour éviter au maximum les bruits métalliques qui risqueraient de
déranger le sommeil des voisinexs.
- Se déplacer à visage\index[tags]{visage} couvert et sans signes distinctifs (couleurs,
tatouages, etc.), pour rester modestexs. Pour la même raison, se
déplacer la nuit\index[tags]{nuit/obscurité}.
- Privilégier les lampes à lumière rouge et à faisceau resserré qui
émettent une lumière beaucoup moins visible de loin (toujours les
voisinexs qui dorment, on oublie pas la politesse). Vous pouvez aussi
peindre une lampe torche à LED avec une peinture acrylique rouge.
- Modestie toujours, agissez seulex ou avec un très petit nombre de
personnes en qui vous avez toute confiance\index[tags]{confiance}. N'en parlez pas (surtout en
ligne).
- Organisez-vous et faites des recherches en respectant les règles de
sécurité numérique[^161], pour passer sous le radar du grand lobby des
contrôles techniques réussis.
- Rangez vos outils dans un sac en toile, plus discret, et ne laissez
rien derrière vous (on ramasse ses déchets !).
- Privilégiez des baskets légères, plus tendance que des grosses
chaussures de marche et leurs empreintes reconnaissables. Astuce fashion :
si vous voulez utiliser des chaussures de marche, recouvrez-les d'une
grosse chaussette.
- Transportez vos liquides dans des bouteilles en plastique nettoyées
de toute empreinte. Une fois rentréex, brûlez les bouteilles. N'oubliez
pas votre entonnoir, au cas où il faudrait faire une pause pâtisserie en
chemin (ou verser une grande quantité de matière dans une petite
ouverture) !

## Quelques astuces

Il existe un grand nombre d'astuces qui favorisent un bon échec de
contrôle technique. Des rigolotes, des compliquées, des rapides, des
plus lentes. C'est au bon goût de chacunex !

- Pour abîmer convenablement les serrures de votre véhicule, enfoncez-y
plein de petits bouts de bois, par exemple des cure-dents. Cassez bien
tout ce qui dépasse. Pour plus de stabilité, remplir ensuite la serrure
de super glu. Astuce déco : utilisez un bois coloré ! Variante :
faites-y coulisser un fil de fer et coupez bien les deux extrémités à la
pince.
- Pour abîmer convenablement votre système de refroidissement, versez
une bonne quantité de riz sec dans le radiateur. Avec l'arrivée de
l'eau\index[tags]{eau}, les grains vont gonfler et rendre le système inutilisable.
- Pour abîmer convenablement votre pot d'échappement, glissez-y une
pomme de terre, enfoncée le plus loin possible à l'aide d'un bâton.
Quand le moteur sera démarré, il toussera puis s'éteindra. Variante,
remplissez-le de mousse polyuréthane isolante (qui gonfle à
l'application).
- Pour abîmer convenablement votre réservoir à essence\index[tags]{essence}, on peut y
verser plusieurs substances. Les plus discrètes sont du sel de table, du
sable fin, de l'acide butyrique.
- Pour abîmer convenablement vos pneus, vous pouvez les crever
délicatement avec un canif. Dans le cas des pneus à haute pression,
mieux vaut dévisser la valve et creuser le trou par lequel l'air
s'échappe.
- Pour abîmer convenablement le système de distribution d'huile\index[tags]{huile}, le
plus efficace et le plus sûr est d'y introduire un abrasif. Le plus
simple est le sable, mais attention à ne pas utiliser un sable proche de
chez soi. Le sable carbo-siliceux peut s'acheter et c'est un bon choix.
Il suffit alors de trouver l'accès, soit le bouchon du réservoir à
huile. Sur de gros engins, étudier préalablement pour trouver les points
d'accès. Attention, ils sont parfois cadenassés.
- Pour rigoler\index[tags]{rire/se marrer/rigoler} un peu, même si cela n'empêchera sans doute\index[tags]{doute} pas la
réussite du contrôle technique, enroulez le véhicule de papier toilette
(3-4 rouleaux suffisent pour une voiture\index[tags]{voiture/bagnole}, prévoir plus pour les gros
engins de chantier) et aspergez-le d'eau\index[tags]{eau} pour que ça colle bien. Voilà,
vous avez créé une toile, et vous pouvez laisser votre âme d'artiste
peintre s'exprimer !

## Pour conclure...

Il n'est pas si difficile de rater un contrôle technique. Si un petit
millier de personnes mettait en pratique ces quelques astuces, seulement
de temps à autre, il est très probable que la police ne puisse plus
jamais rouler, ce qui soulagera beaucoup les agents subissant des
rythmes de travail effrénés.

La totalité des formes d'action politique n'impliquant que la parole
sont globalement autorisées. D'ailleurs, la démocratie se vante
continuellement que touxte unex chacunex puisse affirmer qu'iel n'est
pas d'accord. Mais dès lors que la contestation se transforme en action,
on devient unex délinquanxte et la répression s'acharne. Si l'on peut
dire à peu près tout ce qu'on veut, on ne peut pas le dire n'importe
comment. En 2008, devant le centre de rétention de Mesnil-Amelot, une
banderole déployée où l'on pouvait lire "feu\index[tags]{feu} aux prisons" a valu des
poursuites à ses auteurixes lorsqu'un incendie a éclaté le même jour
dans le bâtiment. La justice\index[tags]{justice} ne s'y est pas trompée : la différence
entre l'acte et la parole\index[tags]{parole} n'est pas si grande qu'il n'y paraît. De la
pièce intentionnellement mal usinée au cure-dent dans la serrure, en
passant par la dégradation qui rend la marchandise invendable, l'alarme
incendie intempestive ou le sabot\index[tags]{sabot/saboter/sabotage} dans la chaîne de production, attaquer
les structures de la domination a été le point de bascule de nombreuses
luttes. C'est grâce aux saboteureuxses que vous avez des week-ends, que
vous ne bossez pas 80\ h, que vous avez le droit de vote\index[tags]{vote} si vous êtes une
femme et que vos enfants vont à l'école.

Nique les contrôles techniques !

[^160]: Pour d'autres précautions possibles encore, lire *L'usure ordinaire*\ \[n^o^\ 6\].

[^161]: Pour tenter désespérément de communiquer en ayant l'impression de ne pas pouvoir être traquéex, lire *Camouflage dans l'infosphère*\ \[n^o^\ 40\].


