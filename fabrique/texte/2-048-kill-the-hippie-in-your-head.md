---
author: "Anonyme"
title: "Kill the hippie in your head"
subtitle: "Comment j'ai abandonné le pacifisme"
info: "Texte rédigé pour le recueil"
date: "septembre 2020"
categories: ["manifestation, émeute", "relation à la militance"]
tags: ["index"]
lang: ["fr"]
num: "48"
quote: "un truc à cagoules et cocktails molotov"
---

\begin{addmargin}[0.15\textwidth]{0em}
\emph{Hippie : Jeune adepte, aux États-Unis puis en Europe occidentale,
d'une éthique fondée sur le refus de la société de consommation qui
s'exprime, dans la non-violence\index[tags]{violence}, par un mode de vie non conventionnel. Lae hippie n'est pas politiséex.}
\end{addmargin}

\vspace{1\baselineskip}

C'était lors d'une manif contre monsanto. Y'avait pas mal de bobos. Pas
mal de baboss'. Des enfants et des vieilleux. Un peu de tout, y'avait du
mélange social, mais c'était pas un truc à cagoules et cocktails
molotov[^142], ça c'est sûr. J'ai envie de raconter ça, même si,
depuis, mon militantisme a changé de forme et de fond, parce que je
crois que ça a été une de mes premières découvertes du potentiel
insurrectionnel. Ce jour-là, j'ai découvert qu'on peut protester contre
les choix délétères de l'économie et de celleux qui dominent, sans
garantie aucune de succès, bien sûr, peut-être même en sachant que ça ne
mènera pas à grand-chose. Mais j'ai aussi envie de raconter ça, parce
que je me dis que, parmi celleux qui tomberont sur ce bouquin, y'en
aura pour qui cette histoire sera peut-être utile...

Donc, j'étais en manif contre monsanto et le dispositif mis en place
pour garder les manifestanxtes loin du bâtiment était impressionnant,
surtout qu'on était dans une ville relativement petite, à la population
molle et bourgeoise, une ville socialo, le genre qui peut accueillir
monsanto sans trop de dissonance\index[tags]{dissonance} cognitive.

Moi, je n'avais que les films américains comme repère, alors le
dispositif ne me choquait pas plus que ça. Par contre, les gens qui
m'entouraient, plus expérimentés que je ne l'étais, étaient surpris
par l'ampleur du déploiement policier. Cet organe qui, en théorie\index[tags]{théorie},
aurait dû servir à défendre des intérêts publics, avait été mobilisé
dans tous ses effectifs --- les keufs arrivaient de toutes les communes
environnantes --- pour défendre une entreprise privée.

Avec leurs armes et leurs casques, les milices étaient postées dans
l'ombre de l'entrée du bâtiment. C'est quand on a passé les barrières
branlantes qu'elles sont sorties. J'en voyais d'autres qui, plus loin,
lançaient des œufs et d'autres trucs sur les vitres, dont les stores
étaient fermés. J'aurais aimé, je crois, être avec elleux. C'était assez
grisant pour moi. Je n'avais pas fait ça souvent. Je me souviens que je
transpirais beaucoup, tellement que ça ruisselait dans mon dos et sous
mes bras. Rétrospectivement, j'ai pris quelques risques un peu stupides
ce jour-là. Mon visage\index[tags]{visage} n'était pas dissimulé et je n'étais pas
accompagnée.

Je ressentais une colère\index[tags]{colère}, mais vague. Ces espèces de robocops en face de
moi m'intriguaient autant qu'elles m'agaçaient. Ça me semblait vraiment
très injuste de les voir se dresser entre nous et cette entreprise
merdique. Il y en avait un qui me semblait très jeune sous sa visière,
et j'ai eu envie de le regarder de plus près, surtout de comprendre
jusqu'où je pouvais m'approcher de sa face. C'était de la provoc', c'est
sûr, de la curiosité aussi, je voulais voir si la zone était vraiment
aussi inapprochable qu'elle en avait l'air. Je ne devais pas lui faire
très peur\index[tags]{peur}, parce qu'il a à peine bronché quand je me suis plantée devant
sa gueule\index[tags]{gueule}. Je crois que ça m'a un peu vexée de pas l'inquiéter plus que
ça. J'ai décidé que j'allais rester plantée quelques heures devant sa
gueule.

C'était long.

Je crois qu'au bout d'un moment, il s'est quand même senti un peu défié,
le flic, par cette gamine rivée au sol à cinq centimètres de son gilet
pare-balle et de son fusil d'assaut. J'avais l'impression de jouer à ne
pas cligner des yeux. Il a tenté un clin d'œil, j'ai grimacé. Au bout
d'une ou deux heures, son commandant est venu lui proposer de se
déplacer. Il a refusé, il a dit que ça lui convenait bien d'être devant
une "si jolie fille". Je commençais sérieusement à douter de l'intérêt
de ma démarche. Visiblement, il trouvait ça agréable et c'était pas le
but escompté. Bon, je crois quand même qu'il aurait préféré être
ailleurs, surtout que des photographes prenaient des photos de la scène.
J'aurais dû cracher, je pense, ça aurait sûrement été efficace. J'étais
assez proche pour atteindre son visage\index[tags]{visage}. D'un côté, j'étais dégoûtée, de
l'autre, je commençais sérieusement à me faire chier. Je transpirais
encore mais, à ce stade, c'était plus à cause du soleil\index[tags]{soleil} que de
l'adrénaline\index[tags]{adrénaline}. J'aurais au moins dû lui rire\index[tags]{rire/se marrer/rigoler} à la gueule\index[tags]{gueule}, lui tirer un
doigt, lui dire que c'était une merde\index[tags]{merde}. Je l'ai pas fait. Je suis restée
plantée là, comme une espèce d'idéaliste inutile.

D'un côté, je pense que j'étais encore imprégnée par cet imaginaire
pacifiste de la révolution. Cet idéal un peu hippie, qui te fait
t'imaginer glissant des fleurs dans des canons, détournant le tracé d'un
tank en restant debout, stoïque et fière. Mais la vérité, c'est que les
tanks t'écrasent. Ces images sont super belles et tout hein, mais je
crois avoir capté que ça marche pas comme ça. C'est pas si simple
d'inquiéter vraiment l'autorité, de la faire chanceler avec des gestes
simples et symboliques. Ces images sont fortes, elles forgent un
discours acceptable, émouvant, facilement médiatisable, mais la vérité,
c'est que les changements sociaux ne sont pas venus, ou si peu, de la
non-violence\index[tags]{violence}, d'une espèce de consensus mou provoqué par le
brandissement public d'actes héroïques face à un gouvernement
conciliant. Bien souvent, ce sont des luttes souterraines, voire armées,
qui, d'une voix\index[tags]{voix} sourde et étouffée, ont fait trembler quelques
fondations, et parfois éclater le pavé pour qu'une foule les ramasse et
le jette au visage\index[tags]{visage} du pouvoir\index[tags]{pouvoir} casqué. Ce que l'on perçoit des luttes,
les gens privilégiés comme moi, c'est trop souvent la partie visible de
l'iceberg. Alors, souvent par ignorance, on s'étonne, voire on
s'offusque de la réaction des oppresséexs. Ça paraît exagéré, trop
radical, trop violent.

L'autre aspect à capter quant au problème de cette spectacularisation
des révoltes, c'est que, trop souvent, elle conduit à une récupération
des luttes par des personnes peu ou pas concernéexs. Je m'explique.
L'histoire que je viens de raconter avait donné lieu à une photo à
l'époque, une photo qui est ressortie y'a pas longtemps pour défendre je
sais plus trop quoi, en oubliant un peu que c'est mon statut privilégié
qui me permettait d'adopter cette posture, de rester plantée plusieurs
heures devant le robocop. C'est un problème récurrent avec les images un
peu sensationnelles comme celle-là. D'un côté, elles rendent
individuelles des luttes collectives et de l'autre, elles visibilisent
toujours trop les mêmes combats et les mêmes militanxtes blanchexs, cis,
validexs, bobos, celleux qui ont des papiers d'identité en règle et une
grande gueule\index[tags]{gueule}, en ne montrant pas les personnes raciséexs, trans, non
binaires, handicapéexs, paupériséexs, sans papièrexs, qui sont celleux
qui défendent leurs droits et devraient, justement, être visibles.

Mais à l'époque, je ne savais pas encore que si cet abruti de keuf ne me
cassait pas la gueule\index[tags]{gueule}, c'est juste parce que je suis une jeune blanche
gonflée de privilèges qui défendait une cause politiquement récupérable
et acceptable. Je ne savais pas encore que ces gens-là tuent, que des
vraies personnes meurent sous leurs coups et qu'ils ne sont jamais
punis[^143]. Je savais pas encore. Je savais pas encore à quel point
ACAB°. Je ne réalisais pas non plus que me mettre en avant comme ça
pouvait être problématique.

Mais qu'à cela ne tienne. Ce sont des choses qu'on apprend en faisant
communauté, en écoutant les récits, en constatant les images de
violences policières qui se font de plus en plus présentes et inondent
de rage\index[tags]{rage} jusque dans les ventres. Ouais, en fait c'est vraiment par
l'écoute que ça passe. Juste deux minutes, on la ferme et on écoute. On
regarde comment on peut être utilexs. Parfois le mieux qu'on puisse
faire, c'est rester en retrait, faire à bouffer[^144], laisser la
place, filmer les flics[^145], attendre devant un poste de keufs que
les gens sortent, organiser des soirées de soutien. En gros, utiliser
nos privilèges pour les mettre à dispo de celleux qui en ont besoin.
Juste deux minutes, nous autres privilégiéexs, il faut arrêter de
vouloir toujours être au premier plan et arrêter de nous raconter entre
nous le mythe spectaculaire de notre propre lutte. Il n'y a rien de vrai
là-dedans. Il faut faire taire ce côté de nous qui a été biberonné par
une vision réformiste, universaliste, non violente et donc tronquée de
la réalité sociale. Quand on réalise la teneur de l'injustice\index[tags]{injustice}, quand on
rencontre celleux qui sont confrontéexs à la violence\index[tags]{violence} verticale tous les
jours, que ce soit par leurs actions ou leur identité, on comprend un
peu mieux que ce n'est pas que "*about us*". En tout cas j'espère. Et,
à petit feu\index[tags]{feu}, on laisse crever lae hippie dans notre tête.

Alors la fois d'après, ce sera peut-être un peu moins oppressif, moins
problématique, moins nul.

La fois d'après, on y retournera.

Avec des cagoules.

À l'arrière des cortèges.

Avec des pavés peut-être.

Avec des adelphes°, c'est sûr.

Et on ira en masse, on ira avec nos imaginaires dépareillés et nos
privilèges asymétriques, mais on va essayer.

On sera horde\index[tags]{horde} et multiples et sans visages.

Surtout, on va essayer.

[^142]: Pour lire des trucs à cagoules, voir *Drones*\ \[n^o^\ 1\] et *Survivre dans un black bloc *\[n^o^\ 15\].

[^143]: À propos de l'impunité des violences policières, lire *Brisons l'isolement*\ \[n^o^\ 39\] et *They don't see us*\ \[n^o^\ 4\].

[^144]: Le *Grand Midi*\ \[n^o^\ 47\] propose une expérience appliquée.

[^145]: *Surveiller la surveillance*\ \[n^o^\ 55\] donne quelques éléments sur le sujet.
