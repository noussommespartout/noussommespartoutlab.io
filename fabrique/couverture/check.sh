#!/bin/sh
pdftops $1.pdf -eps $1.eps
status=$(grep -o "RGB" $1.eps | wc -l)
echo "$status instance(s) of RGB colorspaces found in file $1.eps"
exit $status

