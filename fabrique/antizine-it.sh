#!/bin/bash

INPUT=$1
TEX=$2
LAST=`ls -tb ../content/italiano/articles/* | head -1`

if [[ ${INPUT} = "compare" ]]; then
  ARTICLES=`ls -1q ../content/italiano/articles/* | wc -l`
  PDFS=`ls -1q ../static/etc/*.pdf | wc -l`
  PDFS=$((PDFS - ARTICLES - ARTICLES))
  if [[ $ARTICLES != $PDFS ]]; then
    echo "---------------------------------"
    echo "---------------------------------"
    echo "                              "
    echo " Attention ! Il manque $((ARTICLES - PDFS)) PDF !"
    echo "                              "
    echo "---------------------------------"
    echo "---------------------------------"
  fi

elif [[ ${INPUT} = "clean" ]]; then
  cd antizines/italiano
  latexmk -c
  rm *.cb
  rm *.cb2

# elif [[ ${TEX} = "tex" ]]; then
elif [[ -n $INPUT ]]; then
  PDF=$(basename -- ${INPUT%.md}).pdf
  TEXFILE=$(basename -- ${INPUT%.md}).tex
  TEXA4FILE=$(basename -- ${INPUT%.md})-antilecture.tex
  BOOKLET=$(basename -- ${INPUT%.md})-antizine.tex
  TEXSOURCE=./antizines/italiano/$TEXFILE
  TEXA4SOURCE=./antizines/italiano/$TEXA4FILE
  pandoc -s --metadata lang:it --lua-filter ./outils/replace.lua --lua-filter ./outils/pandoc-quotes.lua -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=outils/template-it.tex --toc --toc-depth=3 -o $TEXSOURCE $INPUT
  latexmk -lualatex -output-directory=./antizines/italiano $TEXSOURCE
  NBPAGES=$(pdfinfo ./antizines/italiano/$PDF | grep Pages | awk '{print $2}')
  pandoc -s --metadata lang:it --metadata numberofpages:$NBPAGES --metadata pdfsource:./$PDF -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=outils/booklet.tex --toc --toc-depth=3 -o ./antizines/italiano/$BOOKLET $INPUT
  pandoc -s --metadata lang:it -V a4paper --lua-filter ./outils/replace.lua --lua-filter ./outils/pandoc-quotes.lua -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=outils/template-it.tex --toc --toc-depth=3 -o $TEXA4SOURCE $INPUT
  latexmk -lualatex -output-directory=./antizines/italiano $TEXA4SOURCE
  cd antizines/italiano
  sed -i -- 's/{src/{\.\.\/src/g' $TEXFILE
  latexmk -c
  rm *.cb
  rm *.cb2
  vim $TEXFILE

elif [[ ${INPUT} = "last" ]]; then
  PDF=$(basename -- ${LAST%.md}).pdf
  BOOKLET=$(basename -- ${LAST%.md})-antizine.pdf
  PDFSOURCE=./antizines/italiano/$PDF
  pandoc -s --metadata lang:it --lua-filter ./outils/replace.lua --lua-filter ./outils/pandoc-quotes.lua -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=outils/template-it.tex --toc --toc-depth=3 -o $PDFSOURCE $LAST
  NBPAGES=$(pdfinfo $PDFSOURCE | grep Pages | awk '{print $2}')
  pandoc -s --metadata lang:it --metadata numberofpages:$NBPAGES --metadata pdfsource:$PDFSOURCE -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=outils/booklet.tex --toc --toc-depth=3 -o ./antizines/italiano/$BOOKLET $LAST

else
  for i in ../content/italiano/articles/* ; do
    test "$i" = ../content/italiano/articles/_index.md && continue
    PDF=$(basename -- ${i%.md}).pdf
    TEXFILE=$(basename -- ${i%.md}).tex
    TEXA4FILE=$(basename -- ${i%.md})-antilecture.tex
    BOOKLET=$(basename -- ${i%.md})-antizine.tex
    TEXSOURCE=./antizines/italiano/$TEXFILE
    TEXA4SOURCE=./antizines/italiano/$TEXA4FILE
    pandoc -s --metadata lang:it --lua-filter ./outils/replace.lua --lua-filter ./outils/pandoc-quotes.lua -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=outils/template-it.tex --toc --toc-depth=3 -o $TEXSOURCE $i
    latexmk -lualatex -output-directory=./antizines/italiano $TEXSOURCE
    NBPAGES=$(pdfinfo ./antizines/italiano/$PDF | grep Pages | awk '{print $2}')
    pandoc -s --metadata lang:it --metadata numberofpages:$NBPAGES --metadata pdfsource:./$PDF -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=outils/booklet.tex --toc --toc-depth=3 -o ./antizines/italiano/$BOOKLET $i
    pandoc -s --metadata lang:it -V a4paper --lua-filter ./outils/replace.lua --lua-filter ./outils/pandoc-quotes.lua -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=outils/template-it.tex --toc --toc-depth=3 -o $TEXA4SOURCE $i
    latexmk -lualatex -output-directory=./antizines/italiano $TEXA4SOURCE
    sed -i -- 's/{src/{\.\.\/src/g' $TEXFILE
  done
fi

exit 0
