---
title: Présentation
meta: true
num: "0-1"
metatext: |

  <figure class="centre--image image--presentation"><a href="https://abrupt.cc/nsp/nous-sommes-partout/" target="_blank" rel="noopener"><img class="lazy image" alt="le livre Nous sommes partout" src=/img/livre-nsp-1.jpg sizes="(min-width: 50em) 50vw, 100vw" srcset="/img/livre-nsp-1-small.jpg 400w, /img/livre-nsp-1-medium.jpg 800w, /img/livre-nsp-1-large.jpg 1200w, /img/livre-nsp-1.jpg 2400w"></a></figure>

  **Collectifs de Suisse romande et alémanique**

  <a href="https://abrupt.cc/nsp/nous-sommes-partout/" target="_blank" rel="noopener">La première sauvegarde</a> a été éditée sous la forme d’un livre, paru aux éditions Abrüpt en octobre 2021. Une deuxième collecte est en cours, en vue d’un deuxième livre. Les collectifs peuvent être contactés à l'adresse : noussommespartout \[@\] riseup.net.

  Les collectifs continuent d’organiser des lectures pirates et institutionnelles : 
  - Suisse romande / sauvegarde n°1 : 8-13.12.2020, Le Grütli, Genève
  - Suisse romande / sauvegarde n°1 : 8-10.10.2021, Le Grütli, Genève
  - Suisse romande / sauvegarde n°1 : 23.10.2021, Ferme collective de la Touvière, Meinier
  - Suisse romande et Suisse alémanique : 08.01.2022, Aargauer Kunsthaus
  - Suisse romande / sauvegarde n°1 : 20.02.2021, Les Amazones, Genève
  - Suisse romande / sauvegarde n°1 : 10-12.03.2021, La Grange de Dorigny
  - Suisse romande / sauvegarde n°1 : 24-26.03.2021, MCBA Lausanne
  - France / sauvegarde n°1 :06-08.06.2022, Rencontres raisons sensibles, Toulouse
  - Suisse romande / sauvegarde n°1 :25.06.2022, Centre syndical Pôle Sud, Lausanne
  - Belgique / sauvegarde n°1 :19.10.2022, Festival des Libertés, Bruxelles

  **Collectif de Bologne (Nord de l’Italie)**

  Une première collecte est en cours, en vue d’un premier livre, à paraître courant 2022. Le collectif peut être contacté à l’adresse : siamo_ovunque \[@\] inventati.org.

  Le collectif organise des lectures pirates et institutionnelles : 
  - 09-10.07.2022, Festival des arts vivants de Sant’Archangelo
  - 02.12.2022, Zona K, Milan


  **Collectif de Basse-Normandie (Nord-Ouest de la France)**

  Une première collecte est en cours, en vue d’un premier livre, à paraître courant 2023. Le collectif peut être contacté à l’adresse : noussommespartoutbn \[@\] riseup.net.

  **Libre diffusion**

  *Nous sommes partout* est une base de données qui peut être diffusée librement. Tous les textes publiés sur ce site sont donc mis à disposition selon les termes de la [Licence Creative Commons BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr).

  **Technique**

  *Nous sommes partout* s'appuie sur la puissance de [l'outil libre Git](https://gitlab.com/noussommespartout/noussommespartout.gitlab.io/sources) pour diffuser et versionner ses textes.

  Le [code source](https://gitlab.com/noussommespartout/noussommespartout.gitlab.io) du site est quant à lui placé sous [licence MIT](https://gitlab.com/noussommespartout/noussommespartout.gitlab.io/-/blob/main/LICENSE-TOOLS).
---

## Objet de paroles

*Nous sommes partout* collecte et partage des voix antifascistes,
féministes, anticapitalistes, antiracistes, antispécistes, des paroles
de hackeureuxses, des voix en lutte pour les droits des migranxtes,
contre toutes les formes d'oppression de nos sociétés, pour les droits
LGBTQIA+, contre les écocides, pour les droits des travailleureuxses du
sexe, contre les violences policières, pour les droits des
sans-papièrexs, pour l'autodétermination et l'émancipation de touxtes
les travailleureuxses, contre la précarisation, contre le système
carcéral et pour les ZAD.

Le projet est pensé comme un regroupement de ressources, une
constellation de paroles. On y lit essentiellement des témoignages
écrits pour l'occasion, des textes retravaillés ou des transcriptions de
discussions orales auto-organisées.

L'ensemble des militanxtes et des collectifs ayant écrit pour *Nous
sommes partout* ont reçu de l'argent en soutien à elleux et à leurs
luttes. Le modèle économique du projet est celui des arts vivants
subventionnés : *Nous sommes partout* prend aussi la forme de sessions
d'écoute-lecture collectives, inspirées des arpentages
anarcho-syndicalistes. La création de cette forme scénique co-produite
par des institutions ainsi que sa tournée  permettent de lever les fonds
nécessaires.

En cela, le projet essaie un dispositif de répartition et de
collectivisation de son propre financement institutionnel. Les sessions
de lecture, situées dans le champ culturel comme des « spectacles »,
mais dont le dispositif cherche à rester anti-spectaculaire, fondent la
possibilité d'un soutien matériel et financier aux luttes sociales, y
compris aux luttes anti-institutionnelles.

Toutes les personnes contribuant à la base de données sont pleinement
informées de ce dispositif, du mode de financement comme de la forme
scénique. Elles écrivent en sachant que leur texte pourra être lu à voix
haute par toute personne s'étant inscrite pour participer à l'une de ces
sessions participatives d'écoute-lecture.

Le *Nous* qui donne son titre au projet revendique le rêve d'une énergie
agglomérée, d'une somme de *Je* qui écrivent en leur nom et d'autres
*Nous* qui écrivent en collectif, ici et ailleurs, une constellation que
le pronom n'uniformiserait pas. Heureusement, le *Nous* n'a pas le
pouvoir de réduire les parties à un tout. Dans ce projet, le *Nous* fait
apparaître les convergences, les synergies de centaines de voix qui sont
autant de vecteurs d'un vaste mouvement de transformation.

Si la lutte est comme un cercle, sans début ni fin, alors partout et en
tout temps *Nous* sommes là.

## Expansion

À l'origine, *Nous sommes partout* est à l'initiative d'un collectif
éditorial constitué de sept personnes baséexs en Suisse romande. La
première phase de la collecte de textes s'est donc concentrée sur ce
territoire politique. En octobre 2021, la base de données accueille 57
textes, tous publiés dans le livre  « Sauvegarde n°1 / Suisse romande »,
paru aux éditions Abrüpt.

D'autres sauvegardes viendront, car tant que le collectif éditorial
parviendra à trouver des fonds, de nouveaux textes seront ajoutés sur la
base de données. Ce processus rend l'édition augmentative; d'autres
livres sortiront au fil de l'expansion de la collecte : une seconde
sauvegarde, une troisième sauvegarde, etc. La taille et le volume
croissants du *pavé* signifient matériellement le poids des luttes, et
rendent le livre toujours plus susceptible de traverser des vitrines.

*Nous sommes partout* est destiné à s'étendre au-delà de son territoire
suisse romand originel. D'autres collectifs, complices du groupe romand
mais autonomes dans leur travail éditorial, ont entrepris de nouvelles
collectes de textes en lutte, notamment en Suisse alémanique et dans le
Nord de l'Italie.

Les textes de ces versions à venir seront ajoutés à la base de données
centrale et publiés sous la forme de livres indépendants : d'autres
sauvegardes centrées sur d'autres territoires politiques. La base de
données essaiera de proposer les textes traduits dans autant de langues
que possible, toujours de manière augmentative et au fil du temps.

Pour la première édition en Suisse romande, le groupe éditorial a
d'abord rédigé un appel à contributions qui a circulé dans certains
milieux militants, puis il s'est occupé des transcriptions, des
relectures, de l'harmonisation typographique et des échanges avec les
canaux de financement. Le groupe se charge également de l'organisation
des sessions d'écoute-lecture dans les institutions (et de certaines
sessions ayant lieu en dehors des institutions).

Le collectif logistique essaie de limiter au maximum toute intervention
éditoriale autoritaire pour que *Nous sommes partout* reste centré sur
son geste primordial : accueillir des voix. Nous voulons éviter de
dessiner des lignes de force sculptant trop strictement le projet, nous
essayons de lui laisser prendre la forme que lui donnent les
contributions, sans intervenir. Notre intention est de socialiser des
réflexions sur les pratiques militantes --- dans les termes choisis par
les très nombreuxses auteurixes. Nous croyons en la nécessité de
l'échange et en l'agentivité des lecteurixes à qui nous adressons cette
collecte protéiforme comme une interrogation sociale, critique et
historique sur les combats qui structurent la société et sur la manière
dont on peut lutter pour changer le réel.

## Terrains

*Nous sommes partout* n'est ni un ouvrage de théorie critique, ni une
enquête problématisée autour d'axes précis, ni une histoire structurée
des combats d'un territoire spécifique. C'est une tentative d'édition
participative, une somme de paroles qui relate l'immédiateté de luttes
contemporaines, relevant d'une mise en texte de vécus pratiques et
émotionnels.

Chaque texte, à sa manière et à son degré, est lié à *l'expérience* de
l'action militante. Cette expérientialité se révèle sous différentes
formes au fil des textes, de partages sensibles au sein de collectifs,
de manières de vivre l'action directe ou encore de narrations centrées
sur certains moments de vie considérés par les auteurixes comme
particulièrement politiques.

La première édition est située dans un terrain géographique précis, la
Suisse romande, afin qu'on puisse y reconnaître un contexte
sociopolitique commun, des zones de lutte partagées, une actualité
mutuelle, un tissu temporel cohérent. Les textes se font naturellement
écho : les noms de certaines places publiques, de certains lieux ou de
certains collectifs reviennent ; certains événements, institutions ou
logiques structurelles sont décrits depuis plusieurs points de vue. Les
autres collectifs éditoriaux, en Italie comme en Suisse alémanique
construiront leur propre terrain géographique.

Centré sur l'expérience de l'action politique radicale, *Nous sommes
partout* ne se structure pas autour de perspectives idéologiques
prédéfinies. Cela dit, par souci d'accueillir des voix, des idées et des
modes d'action peu présents dans l'interdiscours dominant, un certain
environnement conceptuel s'impose de lui-même. On peut essayer de le
saisir par la constellation d'adjectifs utilisés par les
contributeurixes pour se décrire : non institutionnel, radical,
libertaire, intersectionnel, anticapitaliste, antifasciste,
antiétatique. *Nous sommes partout* essaie d'éviter les étiquettes, car
les mots désignent des réalités différentes selon les histoires
individuelles, les imaginaires, les villes, les pays, les terrains, les
idéologies ou les vocabulaires de référence. Le groupe éditorial
n'établit pas de bornes idéologiques stables dans lesquelles chaque
contributeurixe serait contrainxte de se reconnaître en participant au
projet (touxtes peuvent d'ailleurs retirer leur texte à tout moment de
la base de données, et donc des futures éditions imprimées).

## Formats

Concrètement, les textes circulent sous trois formes : une base de
paroles données, des livres et des sessions d'écoute-lecture publiques
et participatives.

La **base de données** (noussommespartout.org) est en augmentation
constante, au fil de l'arrivage des contributions. Tous les textes y
sont disponibles gratuitement, en ligne et dans leur intégralité, et
tous peuvent également être téléchargés individuellement dans des
formats facilitant leur impression et leur diffusion en brochures. La
base de données centralise l'ensemble des textes issus des différentes
versions linguistiques et géographiques de *Nous sommes partout*.

Les **livres** sont des boîtes noires, des sauvegardes de la base de
données, orientées autour de territoires précis. Ils sont imprimés au
prix le plus bas possible et nous avons fait le choix de leur ouvrir le
circuit de diffusion traditionnel (librairies, vente numérique, etc.)
pour privilégier leur circulation et leur visibilité. Ni les collectifs
éditoriaux ni la maison d'édition Abrüpt n'engrangent de bénéfices sur
les ventes.

Les **sessions d'écoute-lectures publiques** ont lieu dans des
environnements institutionnels et non institutionnels. La pratique de la
lecture collective a une histoire riche et plurielle, protéiforme selon
les contextes et les usages. Ces sessions sont inspirées de
« l'arpentage », une technique développée dans les luttes, notamment
ouvrières, pour pallier le manque de temps disponible pour l'éducation
législative et politique. Une session d'arpentage fait appel à
l'intelligence collective : on prend un livre, on arrache chaque
chapitre et on se les partage. Chacunex lit les pages qu'iel tient entre
ses mains puis, dans un deuxième temps, on résume collectivement ce
qu'on a lu.

Les lectures de *Nous sommes partout* adoptent une forme performative
simple. Les spectateurixes sont conviéexs à une session d'écoute-lecture
pendant laquelle iels donnent voix aux textes collectés, accessibles à
touxtes sous la forme de chapitres déchirés. Les lectures dans les
espaces institutionnels sont le cœur de l'interface matériel du projet,
puisqu'elles permettent de lever les fonds qui seront partagés avec les
auteurixes.
