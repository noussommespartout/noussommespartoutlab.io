---
title: Index
meta: true
type: taxonomyindex
weight: 1
printtags: true
num: "0-5"
metatext: |
  L'index propose une entrée non conventionnelle dans la base de données. Il invite à décaler le regard sur le corpus, à ouvrir les portes dérobées des mots du quotidien --- d'un quotidien à transformer.

  Pour naviguer dans les textes selon les luttes abordées, mieux vaut se diriger vers les [mots-clés](/categories/).
---

