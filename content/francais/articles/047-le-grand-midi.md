---
author: "Anonyme"
title: "Le Grand Midi"
subtitle: "Une semaine de vie de la cantine autogérée de Lausanne"
info: "Texte rédigé pour le recueil"
datepub: "juillet 2020"
categories: ["autogestion, expérimentations collectives", "sabotage, action directe"]
tags: ["amende", "amour, love, amoureuxse", "arrestation", "autocritique", "banque", "barricade", "bouffe", "café", "coller", "colère", "compost, composter", "coulant", "cuisiner, cuisine", "doute", "eau", "erreur", "foot", "fourmi", "gueule", "huile", "huile", "jardin", "justice", "logique", "machine", "mail, email", "mercredi", "outil", "peur", "peur", "pizza", "prix", "réunion", "salade", "stockage", "sucre", "sueur", "visage", "voiture, bagnole"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "47"
quote: "cette socialisation du crado"
---

## Lundi & Mardi

À la cantine, on devrait se reposer les lundis et les mardis, mais
c'est pas toujours le cas. Si quelque chose se passe en début de
semaine, c'est que c'est une livraison imprévisible, tous ces petits
accidents chaotiques qui se terminent par de la bouffe en plus dans les
stocks. On reçoit des messages à n'importe quelle heure de la journée :
"Eh, j'ai deux kilos de courgettes, ça vous intéresse ?". Le plus
souvent, ça nous intéresse : avec la cinquantaine de personnes en
contact permanent, il y a toujours quelqu'unex pour se bouger, aller
chercher les deux kilos et stocker ça au frais en attendant le samedi.
Avec le temps, la bouffe arrive de plus en plus d'horizons différents :
on refuse de s'affilier aux centrales de récupération officielle de
l'État, alors on est bien obligéexs de privilégier la débrouille. Parce
que ça commence à se voir, que certaines solidarités sont pas du bon
côté de la barricade.

Une fois, les brigades solidaires de Genève, qui revendiquent
l'antifascisme et l'autodéfense populaire, nous ont livré des
quantités absurdes de barres chocolatées. Ça arrive dans des gros
cartons industriels, on sait pas d'où ça vient, on veut pas savoir. On
en a distribué dans les rues pendant des semaines et des semaines. Il y
a aussi des refuges antispécistes[^136], certains ont des accords avec
les commerces du coin pour récupérer leurs fruits ou leurs légumes,
souvent intacts. Alors iels nous amènent ce que les personnes animales
abritées chez elleux ne mangent pas. On a appris à quel point iels
n'aiment pas les brocolis et les agrumes, parce que les camarades nous
en amènent des caisses et des caisses. C'est devenu la marque de
fabrique de la cantine, faire des pâtes au pesto de brocoli.

Surtout, il y a plein de trucs imprévisibles, une école qui a pas pu
écouler ses stocks de céréales chimiques ou de confiture industrielle,
une ferme du coin qui s'apprête à jeter sept kilos de tomates, une
camarade qui déménage, un restaurant qui ferme et qui donne son matos de
cuisine, l'épicerie du coin qui offre du café et des épices en soutien,
une autre épicerie plus éloignée mais tenue par une camarade qui croit
en nous et se plie en quatre pour nous aider à récolter des fonds ou de
la farine bio, des copainexs qui nous permettent de mettre en place une
grosse récup de casseroles...

Une fois, on nous a proposé d'aller récupérer les invendus d'un
cinéma. On s'est retrouvéexs à traverser la ville avec un caddie rempli
de coca et de m&m's. On a eu l'impression de faire de la vente directe
de sucre industriel, ça nous a tellement saouléexs que, après
discussion, on a collectivement décidé d'abandonner ce plan.

Cet approvisionnement spontané était dur à gérer au début, il a fallu se
dépêcher de récupérer trois frigos et un congélateur, imprimer des
flyers et des étiquettes pour nos conserves, appeler des camarades à la
rescousse pour électrifier le local, bidouiller le four et les frigos,
retaper un coin de l'espace et arrêter de tout stocker sur des tables.

### Comment je suis arrivé là ? (1)

*J'étais de retour à Lausanne seulement depuis quelques mois. J'ai
toujours été actif, à mon échelle, sur les questions de transformation
alimentaire, d'agriculture ou en faisant de l'artisanat, mais je le
faisais plutôt seul et ça n'avait pas beaucoup de sens pour moi. De
manière générale je n'arrivais pas à trouver un collectif affinitaire
dans lequel m'investir.*

*Pendant le mois d'avril, on mangeait avec deux amis à Lausanne. Eux
aussi sont engagés dans différents projets ou activités. On parlait
d'expériences dans des cuisines collectives et des cantines de luttes,
de distribution de pain maison et de légumes, et de la frustration de ne
pas vivre ce genre de choses à Lausanne. De mon côté je commençais à
réfléchir à qui serait intéressé à monter un collectif mêlant cuisine
et luttes.*

*Deux jours plus tard, un de ces amis reçoit un sms de la part d'un de
ses potes qu'il n'avait pas vu depuis des années. Une cantine autogérée
s'était montée depuis plusieurs semaines et iels
cherchaient du monde pour rejoindre le projet. Que de coïncidences... On y
est alléexs le samedi suivant, pour s'informer, jeter un coup d'œil et
voir ce qu'il s'y passait. Et depuis, on y vient tous les samedis.*

## Mercredi

*Le 25 juillet 1943, à "la casa Cervi" située en Émilie-Romagne, lieu
de militantisme antifasciste, on apprend l'arrestation de Mussolini.
Alcide Cervi raconte la joie qu'il y avait dans tous les cœurs ce
jour-là dans* I miei sette figli. *Il raconte que son fils lui avait dit
"Papa, aujourd'hui on offre des pâtes à toute la région." Et ils
amenèrent des bidons de pâtes sur la place du village pour en faire avec
et pour tout le monde. Ça a donné 380 kg de pâtes. C'est une belle
histoire, un chouette mythe antifa : "La pastasciutta antifascista".
On colle des étiquettes sur les sachets de pâtes fraîches qu'on met au
marché gratuit, on leur a donné un nom, "Pasta antifascista".*

Chaque mercredi, une petite autoproduction de pâtes fraîches se met en
place. Un copain a déniché une machine de qualité qui
crache six kilos de pâtes maison à l'heure. Deux ou trois personnes y
vont chaque semaine et à chaque fois, c'est le pied. On accompagne,
avec nos mains, les pâtes encore chaudes qui sortent de la gueule de
cette machine, on les coupe et on les étale sur des claies de séchage de
sérigraphie. Elles sont belles et vraiment trop bonnes, tout le monde le
dit. Les pâtes, on les a faites dès la première cantine. Et un copain
italien en particulier y a trouvé beaucoup de sens. La première cantine,
c'était un 25 avril. À ses yeux, ça commémorait bien les insurrections
de Milan qui ont conduit à la fuite des troupes fascistes, le 25 avril
1945, devenu depuis en Italie le "jour de la libération".

### Comment je suis arrivé là ? (2)

*D'origine italienne, je suis arrivé en Suisse un peu par hasard. Le
confinement a été très particulier pour moi, le temps était comme
suspendu. Et moi aussi, entre le soulagement de ne pas être chez moi, en
Italie, et la peur de rester coincé chez d'autres, ici. D'autant plus
que j'avais toujours l'impression, malgré les années, d'être ici en
touriste, de ne pas me sentir à l'aise. Or, je savais que je
n'arriverais pas à accepter d'avoir quitté les copainexs en Italie, tant
que je ne trouverais pas une manière de me plonger dans la ville où
j'habite, où je vis.*

*Déjà chanteur militant par le passé, je me suis investi dans la Chorale
Anarchiste de Lausanne. Et c'est sur la liste mail de la Chorale que
j'ai vu passer pour la première fois des informations sur le projet de
cantine autogérée : distribution alimentaire d'urgence. Et iels avaient
besoin d'aide pour la récup ou tout autre plan et expérience qui
pouvait contribuer à soutenir la cantine dans ses actions. J'étais sans
plans et sans expériences, mais c'était là une trop belle occasion de
m'intégrer concrètement dans les luttes locales, de contrer
collectivement ce sentiment d'impuissance imposé par la pandémie ---
surtout quand je voyais ce qu'il se passait en Italie --- et de
participer à faire exister un peu plus de solidarité là où je vis.*

## Jeudi

Le jeudi, c'est un pilier de la cantine. Chaque samedi, on planifie les
tournées de récup du jeudi soir. En général, ça se fait à deux, dans une
voiture. Si t'as jamais fait de récup dans ton bled, voilà un petit
tuto pour faire une bonne tournée de récup. Il faut d'abord étudier le
terrain, repérer les cibles : les supermarchés. Un premier tour du coin
permet de délimiter les magasins accessibles, ceux qui laissent leurs
poubelles ouvertes, ceux qui ne broient pas les aliments ou ne les
recouvrent pas de Javel. Souvent, les supermarchés des zones
industrielles sont à privilégier si on est véhiculé : les poubelles des
supermarchés de centre-ville, plus accessibles à pied ou à vélo, sont
déjà récupérées et c'est cool de les laisser aux personnes qui habitent
en ville et qui n'ont pas de bagnole. En Suisse, la coop et la migros
méritent la médaille du sécuritarisme : leur gestion logistique des
déchets est fermée et rigoureuse. Les containers sont le plus souvent
stockés dans des entrepôts verrouillés à double tour, voire directement
dans les camions qui partiront à la déchetterie. Bon, parfois, on peut
entrer dans les camions, mais les amendes peuvent être salées. Ici, on
ne rigole pas avec les yaourts passés de date et les paquets de bananes
déchirés.

Une fois que la liste des magasins est établie, on passe repérer où sont
les poubelles, où se garer pour ne pas que la plaque d'immatriculation
ou les visages soient visibles par les caméras de surveillance. Si on ne
laisse pas de traces de son passage, il y a très peu de raisons que les
bandes soient visionnées par les magasins. Oui, en régime capitaliste,
tes ordures sont aussi ta propriété privée, et que d'autres les
utilisent pour se nourrir est considéré comme du vol. Certains magasins
s'en fichent complètement et sont très ouverts au "vol" de cette
propriété qui les encombre. Le mieux est d'établir une carte précise,
indiquant l'emplacement des caméras et les emplacements où se garer, ça
peut paraître *too much* mais quand il y a régulièrement des nouvelles
personnes qui arrivent et qui n'ont jamais fait de récup, c'est plutôt
cool ! Pour que tout le monde se sente bien, privilégier les magasins
accueillants, ça évite de devoir s'habiller en black bloc[^137] pour
aller récupérer des tomates.

Pour partir en tournée de récup, mieux vaut ne pas oublier de quoi se
couvrir le visage si on préfère (à chacunex de gérer ses risques), des
chaussures imperméables, des fringues auxquelles tu tiens pas trop, des
gants en latex ou carrément de chantier, une lampe frontale, des caisses
de marché et des sacs plastiques. Après, c'est le début de la grande
pataugeoire dans les containers. Pour alimenter une cantine, il faut
prendre un maximum, et ça implique parfois de plonger jusqu'au fond des
bennes. On en a vu des copainexs en spéléologie ordurière, retourner des
couches de plastique ou de pots de compotes écrasés pour atteindre des
trésors. Il y en a qui ont du mal : on est touxtes conditionnéexs à un
certain dégoût de ce qui n'est pas calibré, aseptisé, standardisé, de
ce qui est percé, asymétrique, coulant, visqueux. Une fois qu'on a un
peu déconstruit cette socialisation du crado, la récup devient
l'activité du jeudi, un genre de chasse au trésor qu'on conseillerait
presque aux familles. Difficile de dire la joie collective et
triomphante lorsqu'on découvre enfin des dizaines d'abricots
impeccables qu'on arrachera à leur funeste destin industriel pour en
faire des confitures.

À la fin de la tournée, on reprend la route, en essayant de ne pas faire
attention à l'odeur de poubelle qui émane des vingt caisses entassées
dans le coffre. On arrive au local de stockage où nous attendent les
camarades de la cantine inscrixtes au planning. Musique à fond, avec de
l'eau et du bicarbonate de soude, on nettoie tout ce qu'on a récupéré
et on le stocke. Parfois, en prenant le temps de regarder ce qu'on a
récupéré en pleine lumière, on constate qu'on a été un peu optimiste. À
la lampe frontale et à travers les gants, ce qui semblait un fenouil
frais et juteux est en réalité pourri sur la moitié du bulbe. Tant pis,
ça part au compost, après tout c'était son destin[^138], le
capitalisme industriel n'en a pas voulu, les hérissons et les fourmis
le mangeront. Iels iront faire caca sur les salades du jardin, on espère
qu'on pourra les servir un jour à la cantine. La boucle sera bouclée.
Avec ce que jettent trois magasins en une seule journée, près de cent
personnes mangent tous les samedis et une autre vingtaine font quelques
courses au marché gratuit.

### Comment je suis arrivée là ? (3)

*J'ai commencé à militer dans un syndicat étudiant il y a quelque
temps. Je me sentais en décalage dans un environnement scolaire
oppressant et j'avais beaucoup de colère en moi. Le syndicalisme s'est
imposé à moi comme un outil qui me permettait de transformer cette
colère en lutte collective, comme une évidence dans laquelle
m'investir. À tel point que l'année suivante j'ai fait un burn-out
militant et j'ai commencé à remettre en question mon engagement
politique, qui ne m'apportait pas ce à quoi j'aspirais. Avec des
camarades, on a alors commencé à rêver de coopératives qui cultivent la
terre, qui font du pain et des bouffes pop. On en a beaucoup parlé, mais
rien n'a vu le jour, on était trop prisexs dans des conflits
interpersonnels et à la recherche de nouvelles forces. Une grande
déception. Je regardais avec envie les milieux autonomes, auxquels mes
camarades syndicalistes ne s'identifient pas du tout. Je sentais que
j'avais besoin de changements, de rencontrer de nouvelles personnes, de
faire de nouvelles choses, ailleurs. Et c'est là que j'ai vu passer le
flyer de la cantine autogérée.*

*Je suis arrivée le samedi suivant avec 5 kg de pain dans les bras que
j'avais cuit à l'aube. Et la suite, c'est beaucoup d'amour. J'ai
l'impression d'avoir rencontré une grande famille dans laquelle je me
sens bien. On prend notre temps, doucement on tisse des liens solides et
les projets collectifs se multiplient. Il y a un plaisir immense à
utiliser ses mains comme arme politique. Et qui que tu sois et quel que
soit ton passé, à la cantine, il n'y a pas cette notion de jugement
permanent, on peut y venir et être soi-même. C'est un vrai travail de
reconstruction pour moi où la bouffe n'est plus une souffrance
solitaire, mais une source de joie collective, solidaire et politique et
ça fait du bien.*

## Vendredi

Le vendredi, on devrait aussi se reposer, mais c'est pas toujours le
cas. Parfois la récup de la veille n'a pas suffi et il faut y
retourner. Mais le plus souvent, si on fait quelque chose le vendredi,
c'est parce qu'on a envie d'organiser le long terme. En servant
chaque samedi, on tisse du lien. Manger rapproche, aide à s'organiser,
et personne n'imagine qu'une cantine autogérée puisse ignorer les
luttes autour d'elle. Un projet qu'on porte collectivement, c'est
celui d'une cantine mobile. Bien sûr, il faut de la thune pour
construire ça. Et pour la thune, on n'est pas méga douéexs. Une fois, on
s'est pliéexs en quatre pour organiser une journée pizza, on voulait
d'abord l'appeler "Pizzacab", mais déroutéexs par notre propre
manque cruel d'imagination, on a opté pour "Pizz\@more", et c'est
mieux, il n'y a rien de plus révolutionnaire que l'amour. On s'est
organiséexs pour avoir des fours, de la pâte, des sauces. Les keufs ont
regardé nos fours énormes, iels ont demandé : "C'est à qui ces trucs
brûlants et précaires là ?", les cuistots ont répondu : "Je sais pas,
vous pouvez les déplacer si vous avez un transpalette, en attendant, on
va cuire des pizzas". Iels se sont barréexs. Une bonne centaine de
personnes est venue manger, et on a distribué une trentaine de
*calzones* dans les rues. C'était la dixième cantine, un grand moment.
Par contre, de touxtes les membres de la cantine qui étaient là,
personne n'a pensé à mettre une caisse <a href="/glossaire/#prix-libre" class="glossary-link" target="_blank" rel="noopener" data-no-swup>prix libre°</a>. Donc on n'a pas
vraiment récolté d'argent.

Pendant les grandes manifestations antisexistes de juin, on a customisé
un caddie pour se promener dans le cortège et on a fait un peu de fric
en vendant nos pâtes antifascistes autoproduites et... violettes ! Une
fois, on a organisé une journée pâtes au chaudron, dans le parc,
c'était la Sainte-Sorcière. Une autre fois, on a refait une pizzeria
improvisée sur la promenade de la Solitude avec un groupe de personnes
qui s'organise pour contrer la répression subie par les militanxtes, c'était
l'événement "Contre la répression judiciaire, pizzas solidaires". Là,
on a pu se faire un peu d'argent quand même.

Une autre fois encore, c'était la journée falafel, et on n'a pas trouvé
de jeu de mots plus pété qu'"antifa-lafel". On cuisine pour les
caisses de soutien d'antirépression ou d'entraide judiciaire, pour
aider à payer les amendes de celleux qui occupent des maisons
abandonnées[^139], celleux qui rejoignent les blocs révolutionnaires,
qui risquent l'expulsion du pays ou de leur maison, celleux qui n'ont
pas d'assurances maladie. Bien sûr, ça emmerde l'État qu'on nourrisse
gratuitement, qu'on n'ait pas besoin d'elleux, pas besoin de leur
accord pour exister, s'approvisionner et investir des parcs publics, en
dehors de toute norme ou règle autre que les nôtres. L'administration
nous emmerde comme elle peut, mais on se plaint pas pour le moment. Une
fois, une voiture d'une asso s'est garée de travers, quelques minutes,
le temps de récupérer trois cagettes de légumes pour les distribuer le
lendemain à des personnes qui ont perdu leur emploi à cause du Covid.
Les keufs qui rôdaient autour ont saisi l'occasion, voyant la moitié
d'un pneu dépasser d'une ligne blanche, ils se sont précipités pour
nous coller 200 balles d'amende.

Notre cantine mobile, on voudrait qu'elle puisse réagir très vite, être
en quelques minutes sur les lieux d'occupation, de manifestation,
d'arrestation, devant les commissariats quand des camarades sont
détenuexs. Plus les gens ont à manger, plus ils restent. Avec un stand
de crêpes *vegan* et deux enceintes, on multiplie par deux le nombre de
personnes qui viennent soutenir devant le poste, et par trois le temps
pendant lequel ils resteront.

### Comment je suis arrivée là ? (4)

*Même si j'ai participé à des manifs, assisté à des AG, j'ai jamais
vraiment milité. J'ai voté un temps, puis j'ai arrêté, puis j'ai voté
blanc, mais j'ai finalement jamais été vraiment investie. Je n'ai jamais
trouvé, en Suisse, de réseau dans lequel je pouvais me sentir à l'aise,
alors je mettais mon énergie ailleurs tout en me demandant où étaient
les "autres", celleux avec qui je pourrais être bien.*

*Pendant ce confinement j'ai ressenti le besoin de retourner dans une
réalité concrète pour agir, plutôt que de regarder de loin et de penser
seule. Je me sentais en colère, sensible à toutes ces insidieuses
dominations, si redoutablement ordinaires. De plus, ces derniers temps
j'ai commencé à me questionner sur l'anarchisme, et ça m'a semblé d'une
logique évidente. Et pendant que ces pensées-là faisaient leur chemin,
j'ai vu passer l'info de la cantine autogérée.*

*J'y suis allée, comme ça, simplement, un samedi matin à neuf heures.
J'avais jamais pensé à la lutte par la bouffe, mais après coup ça me
semble être une idée géniale et fondamentale que j'ai découverte en
cuisinant et en mangeant avec toute l'équipe de la cantine. Reste que
même si ça fait sens, rien n'est près de changer sauf les gueules des
bouffons sur les billets de banque. On continue malgré tout.*

## Samedi

Samedi, faute de Grand Soir, c'est le Grand Midi. Combien il y a de
personnes qui viennent cuisiner à la cantine ? C'est une info qui
intéresserait sans doute les clowns bleu fluo qui ont le monopole de la
violence légitime, donc on va dire entre une et mille. Cet assemblage
hétérogène d'individus hautement mobiles se réunit dans la matinée,
boit un café et commence à remonter les caisses de nourriture ramenées
le jeudi. On fait un genre de tas, une pyramide de caisses vertes avec
nos pâtes autoproduites, des fruits, des légumes, des produits emballés.
Collectivement, un menu s'élabore, avec le souci de cuisiner ce qui est
le plus abîmé, pour que les produits les plus préservés puissent être
distribués au marché gratuit. Une brève discussion suffit chaque samedi
matin : il faut désigner une personne de contact en cas de visite
policière, une personne qui se charge de garder un œil sur le marché
gratuit, un groupe de personnes qui seraient prêtes à faire la
distribution dans les rues, une personne pour éventuellement aller faire
des courses d'appoint (il y a des ressources de bases difficiles à
récupérer, surtout le sel, le sucre et l'huile).

Ensuite, ça commence à couper. Bien sûr, on pourrait faire des
plannings, des équipes, des responsables, diviser les tâches entre les
fourneaux, les planches à découper, le cru et le cuit. Mais tout se met
en place de manière organique. T'as la pêche : va aux fourneaux. T'as
envie de papoter : va découper des légumes sur la grande table. T'as
envie d'être tranquille : va laver les frigos et les stocks. T'as
envie d'être en plein air : va installer les tables ou vider le
compost. T'as envie de rien foutre : pose-toi faire un baby-foot,
relance du café pour tout le monde, vernis les ongles de tes copainexs.
Après une ou deux heures, on commence à installer la cantine extérieure
s'il fait beau, intérieur s'il fait moche. D'un côté du bâtiment, il y
a la bouffe chaude qui sera servie sur place, assez de chaises et de
tables pour accueillir 30-40 personnes. De l'autre côté du bâtiment, on installe
un marché gratuit, un magasin gratuit ou prix libre ouvert à tout le
monde, qui propose des aliments à cuisiner soi-même. L'idée du
*freeshop* est venue plus tard. On a remarqué qu'une initiative de
solidarité communautaire devait avoir plusieurs visages : il y a des
personnes qui ont envie ou besoin qu'on leur serve des repas chauds,
parce qu'elles n'ont pas les moyens d'en cuisiner elles-mêmes ou envie
de discuter un coup en plein air. Il y a des personnes qui ont envie ou
besoin de cuisiner pour elles-mêmes ou leurs proches, c'est elles qui
viennent au marché gratuit. Et puis, on n'aime pas touxtes les mêmes
choses, on ne cuisine pas de la même manière, on ne parfume pas nos
repas avec les mêmes épices, on n'est pas allergiques ou intoléranxtes
aux mêmes aliments.

Parfois, les réguliers et régulières du marché gratuit repartent,
parfois iels se posent manger dans le parc. On invite toujours tout le
monde à venir dès le matin, le samedi suivant, pour participer à la
cuisine, au menu. Les gens reviennent souvent. Dans les réus de la
cantine, il y a la volonté commune de créer un espace antimarchand de
quartier, et donc de se battre pour créer une vraie mixité sociale tout
en conservant la démarche d'autocritique nécessaire pour essayer de ne
reproduire aucune logique oppressive. Ça ne veut pas dire que c'est
réussi, ça veut dire qu'on essaie. Bien sûr, c'est un lieu militant, la
socialisation et les relations affinitaires favorisent parfois
l'entre-soi, mais c'est quelque chose qu'on cherche toujours à
remettre en question[^140].

Ensuite, quand midi approche, la bouffe chaude est prête : pâtes,
tortillas, salades mêlées, couscous, légumes sautés, salades de fruits,
smoothies, etc. Même, certainexs camaradexs de la cantine
ramènent parfois de l'huile d'olive à la truffe, des morilles ou
d'autres trucs chers volés. Des trucs qui viennent de l'au-delà.

Quel que soit le menu, avant de servir sur place, on prépare la ronde.
Entre 30 et 40 sacs sont distribués dans les rues, avec un repas chaud
complet, des couverts, une salade de fruits. Ces sacs sont pour celleux
qui n'ont pas envie de bouger ou de socialiser. Les heures qui suivent
sont les meilleures : on mange touxtes ensemble. L'info a tourné dans
les réseaux militants et dans les assos plus officielles, même dans
certains services sociaux de l'État.

Une fois, une habituée du *freeshop* nous a apporté plein
de barquettes d'un plat qu'elle avait cuisiné chez elle.\
Une fois, une habituée du marché gratuit a "payé" en weed.\
Une fois, une couturière indépendante est venue manger et nous a apporté
un stock de masques hygiéniques qu'elle a cousus à la main pour la
cantine. Tant mieux, les masques et le gel hydroalcooliques coûtent
cher.\
Une fois, une anarchiste de renommée internationale s'est pointée. On a
hésité à lui faire une haie d'honneur. Elle a dit : "Tenez, je vous ai
apporté des tupperwares. Y'a des pâtes ?" Elle s'est assise dans
l'herbe.\
Une fois, une bénévole d'une autre asso est venue prendre tous nos
fruits pour faire des confitures et des sirops qu'on a mis au
*freeshop* le samedi suivant.\
Une fois, une militante nous a raconté à table comment elle avait connu
Evo Morales quand il n'était qu'un jeune activiste indigéniste avant
de devenir le président bolivien qui tiendrait tête aux USA.\
Une fois on a remarqué que quelqu'unex avait pris de l'argent dans la
caisse du *freeshop* et on a eu un long débat pour savoir si la caisse
était aussi en libre-service. Proposer de l'argent à prix libre, c'est
un bon concept.\
Une fois, on nous a dit que les refuges du coin manquaient de produits
d'hygiène, alors on a organisé un atelier de confection de lessive.\
Une fois, quelqu'un s'est posé dans le parc pour organiser un
vide-grenier spontané.\
Une fois, des potes d'un squat nous ont apporté 200 lapins en chocolat
de 500 grammes. 100 kilos de chocolat. On les a distribués, mangés,
fondus, déplacés d'un stock à l'autre pendant des semaines.\
Une fois (plusieurs en fait), des voisinexs qui déménageaient ont
apporté toute la bouffe qui traînait dans leurs placards.\
Une fois, un camarade italien nous a expliqué que toutes les salades
vertes qu'on récupère, il faut les cuire à la poêle, avec de l'ail.
Évidemment, la majorité s'est un peu foutue de sa gueule, mais après,
la même majorité a goûté la salade cuite. Depuis, on en fait presque
chaque semaine.\
Une fois, un camarade de la cantine a mis par erreur au *freeshop* le
stock de bières de récup qu'on se garde pour la réunion de
l'après-midi. Si on croyait à la justice punitive, il aurait passé une
sale journée. Mais fuck la justice punitive, on a fait la réu en buvant
des jus de fruits. Depuis, d'ailleurs, on ne boit plus d'alcool
pendant les réus.\
Une fois (plusieurs en fait), les keufs sont venus se garer. Ils font ça
pour effrayer les personnes qui viennent manger. La police veut que le
mot tourne, que la cantine ne soit pas un espace *safe* pour celleux qui
ne peuvent se permettre un contrôle d'identité[^141]. Une pure
stratégie de peur et de division. Plus la masse mange ensemble, plus
elle discute, plus elle s'organise, et ça, c'est pas bon pour tout le
monde. Quand il fait chaud et qu'on mange dehors, on ne peut pas faire
grand-chose contre la surveillance. Quand il fait froid et qu'on mange
dedans, on est plus tranquille.\
Chaque fois, après avoir rangé le matériel, on se rassemble : c'est la
réu de la cantine. On débriefe, on planifie la semaine suivante, on
compte les sous, on propose des projets pour la suite.\
Chaque fois, les personnes qui peuvent être là *deviennent* la cantine
et poursuivent son amélioration constante, simple et collective.\
Souvent, le samedi soir, en sueur et joyeuxses, on a du mal à se
quitter. On reste pour préparer des conserves ou des confitures, on lave
plus que nécessaire pour rester ensemble, on fait des siestes les unexs
sur les autres, des tournois de baby-foot, des ateliers cirque ou
cosmétique, on picole.

## Dimanche

Le samedi soir, la bouffe qui n'a pas été prise au marché gratuit est
récupérée par une asso qui organise une distribution dans la ville
voisine le dimanche. Ces fruits et ces légumes que la société destinait
à la poubelle, on n'en jette quasiment pas.

De notre côté, s'il se passe quelque chose un dimanche, c'est souvent
une manif, un blocage, ou une autre activité militante. Il
arrive qu'on reste plus tard le samedi soir, ou qu'on se retrouve tôt
le dimanche matin pour préparer de la bouffe. Quand on sait qu'un bloc
<a href="/glossaire/#révolutionnaire" class="glossary-link" target="_blank" rel="noopener" data-no-swup>révolutionnaire°</a> se prépare quelque part, on lui fait parfois son
goûter. Voir un bloc venir se décagouler, se reposer après l'action en
dévorant des kilos de bananes de récup au chocolat de récup, c'est
quelque chose. Et quand, après une manif sauvage qui a débordé tout le
monde, des camarades ont fini au poste, on était là pour servir le
dessert. On est alléexs à Delémont, servir des tartines pour soutenir la
cantine (la cantine de Delémont est un super centre autogéré qui, on
espère, existera toujours à la lecture ou à la parution de ce recueil).
Et les prochaines manifs, on voudrait y
être. Et on voudrait servir de la bouffe jusqu'à l'effondrement
définitif de tous les systèmes de domination. On espère qu'on sera
assez.

On espère qu'on sera toujours plus nombreuxses.

On espère qu'un jour, nous serons partout.

## Les recettes

### Pesto de brocolis

Ingrédients : brocolis, ail (beaucoup !), amandes, poivre, sel et
pourquoi pas, quelques figues sèches.

Bien laver les brocolis. Sentir les brocolis, vérifier qu'il n'y ait pas
de jus de poubelle qui se soit glissé dans la touffe verte.

Couper très, très, très approximativement et faire bouillir les brocolis
dans de l'eau salée.\
Une cuisson *al dente*, c'est ce qu'il y a de mieux. Quand c'est prêt,
mettre tous les ingrédients dans un grand bol et mixer le tout jusqu'à
ce que la consistance vous plaise. Salez et poivrez !

### Salade cuite

À faire avec de la salade vieille, abîmée et de la panure à partir de
pain sec.

Ingrédients : salade, huile d'olive, ail, oignon, poivre, sel, petit
plus → panure.

Bien laver. Sentir les salades, vérifier qu'il n'y ait pas de jus de
poubelle qui se soit glissé entre les feuilles. Couper très, très, très
approximativement la salade et la laver abondamment.

Hacher l'ail et couper l'oignon en lamelles, en rondelles, en julienne,
comme bon vous semble.

Faire revenir l'ail et l'oignon puis ajouter la salade. Il faut des
grands contenants, on a l'impression qu'il y a de la salade pour 300
personnes et puis, ça réduit (comme les épinards).

Attendre que l'eau que rejette la salade se résorbe un peu puis à la fin
de la cuisson ajoutez la panure, ça donnera de la consistance et ça
sèchera la salade.

### Bombe de compost à lancer sur les keufs

Si vous avez du purin d'ortie ou du purin de limaces, c'est que vous
ferez les meilleures bombes qui soient. Ça schlingue de ouf.

[^136]: Pour en savoir plus sur les refuges, lire *Solidarité antispéciste* \[n^o^ 35\].

[^137]: Pour comprendre les implications de la tenue vestimentaire, lire *Survivre dans un black bloc* \[n^o^ 15\].

[^138]: Un autre destin pour le compost est envisagé dans *Le compost généralisé* \[n^o^ 41\].

[^139]: à l'instar du collectif dont des membres prennent la parole dans *Jean Dutoit en lutte* \[n^o^ 13\].

[^140]: Un questionnement parfois difficile, voir *Faudrait pas que notre révolution ait l'air trop révolutionnaire* \[n^o^ 44\] et *Un jour j'ai poussé la porte d'un hangar pété* \[n^o^ 26\].

[^141]: *They don't see us* \[n^o^ 4\] raconte l'acharnement de la police à contrôler les personnes sans-papièrexs.
