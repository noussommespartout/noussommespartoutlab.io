---
author: "Loïc Valley"
title: "Un terreau pour les fleurs de la révolte"
subtitle: "Colère non binaire"
info: "Texte rédigé pour le recueil"
datepub: "juin 2020"
categories: ["féminismes, questions de genre", "transpédégouines, queer", "relation à la militance"]
tags: ["amour, love, amoureuxse", "colère", "délit", "détruire, destruction", "gueule", "merde", "mère", "nuit, obscurité", "rire, se marrer, rigoler", "rue", "usine", "voix"]
langtxt: ["de", "fr"]
zone: "Suisse romande"
num: "52"
quote: "se rappeler que notre colère nous veut du bien"
---

{{%epigraphe%}}
La révolution (la vôtre, la nôtre) exige toujours de se réveiller au
milieu de la nuit : il faut activer la conscience justement quand elle
devrait s'éteindre.

--- Paul B. Preciado
{{%/epigraphe%}}

Je suis suisse. Je suis néex en Suisse, j'ai grandi et fait mes écoles
en Suisse. J'ai été forcéex à servir le pays dans lequel je suis néex.
J'ai appris la grandeur supposée de ses institutions, son modèle de
soi-disant parfaite démocratie, sa légendaire neutralité dans tous les
conflits mondiaux depuis sa création, j'ai été biberonnéex à la
tranquillité aseptisée d'un pays dans lequel le pire défaut que l'on
puisse avoir est celui de parler trop fort. "Vous comprenez, ça ferait
mauvaise façon..."

Alors évidemment, entre ses nourriciers apprentissages de mimétisme
morne, la Suisse m'a appris que les pédéexs, les gouines et touxtes
celleux qui n'épousaient pas l'hétérosexualité n'épouseraient rien du
tout dans ses frontières. Elle m'a appris que l'homophobie n'était pas
un délit (la transphobie, on n'en parle même pas), que mon anatomie ne
me permettait pas de me faire violer et que, surtout, ça n'allait pas
changer de sitôt. "Vous comprenez, il faut laisser le temps au
temps..."

Entre deux années scolaires, pendant qu'unex prof faussement
subversifvex appliquait à la lettre un plan d'étude qui doit s'appeler
secrètement "11 ans pour faire fermer sa gueule aux futurexs sujets
biopolitiques de la mère Suisse", j'ai découvert que l'hétérosexualité
m'ennuyait autant que les remontées mécaniques et que la non-binarité
était un mot qui me faisait pleurer quand je l'entendais, parce que
j'avais l'impression qu'il avait été créé pour moi.

Et là ?

On fait quoi ?

Non, je repose la question parce qu'elle est essentielle. On fait quoi
quand on vit dans la reproduction urbaine et matérielle de la définition
du "Calme" et qu'on découvre que notre identité, en elle-même, menace
ce "Calme" ?

Je vous promets qu'on ne fait qu'une chose quand on est une jeune pédéex
de 15 ans. On fait tout pour être comme les autres. On fait des blagues
oppressives sur nous-mêmes parce que ça fait rire les gens et qu'ils
nous trouvent "pleinex d'autodérision", on joue à la pédale parce
qu'on nous appelle comme ça, on joue à la tafiole parce qu'on nous dit
qu'on en est une et on casse le poignet en marchant dans la rue pour que
les hétéros se rendent bien compte qu'on n'est pas dans leur camp, mais
qu'on s'applique pour que ça fonctionne. Qu'on est motivéex. "Vous
comprenez, c'est important la motivation, “Monsieur” Valley."

Je vous jure, passer des années à lécher les bottes pleines de merde de
l'*establishment* et de systèmes qui ne veulent pas qu'on existe, ça
politise.

Ça crée des déclics.

Ça n'a pas manqué.

J'ai découvert ma pire alliée, ma meilleure ennemie. Une histoire
d'amour de celles qui sont intenses et qui vous portent loin. J'ai mis
longtemps à comprendre que c'était une personne qui voulait mon bien et
qu'elle me le disait comme elle le pouvait, abruptement, mais sans rien
laisser passer. J'ai compris qu'elle était là tout le temps, partout,
dans toutes les circonstances et j'ai souri à chaque fois qu'elle m'a
relevéex.

J'ai découvert ma colère.

Ça m'a pris du temps, parce qu'on lui avait appris à se ranger dans un
coin et à ne pas exister, mais la Suisse a merdé. Ce n'est pas rare
qu'elle merde, mais là elle a vraiment merdé. Elle a créé l'usine de sa
propre destruction. Des êtres qu'elle n'a pas aimés, mais tolérés en
tentant de faire taire leur colère avec des processus, tous plus
ingénieux les uns que les autres, et qui ont formé des volcans à
retardement qui un jour ont explosé.

La Suisse est la Génitrice d'enfants qui planifient sa mort. La Suisse
est une héroïne de tragédie grecque : elle prie l'Olympe pour qu'on ne
la laisse pas tomber, puis elle se rend compte qu'elle est un être faux
et monstrueux et qu'elle ne peut pas vivre dans un monde qui cherche à
*devenir*, à se réveiller et à ne pas mourir.

{{%ligneblanche%}}

Suisse, mère de ma colère. mère de la colère[^153] de lae Non-Binaire
Queer Trans\* Pédéex Grossex Hyperactivex et HP[^154] que je suis.

{{%ligneblanche%}}

Suisse, mère de ma colère. mère de la colère de lae Non-Binaire Queer
Trans\* Pédéex Grossex Hyperactivex et HP que je suis.

{{%ligneblanche%}}

Le répéter encore, comme dans les tragédies, pour prévenir l'Olympe de
ce qui arrive et les spectateurixes que le dernier acte sera sanglant.

{{%ligneblanche%}}

Suisse, mère de ma colère. mère de la colère de lae Non-Binaire Queer
Trans\* Pédéex Grossex Hyperactivex et HP que je suis.

{{%ligneblanche%}}

Le répéter comme un rituel.

{{%ligneblanche%}}

Suisse, mère de ma colère. mère de la colère de la meuf Non Binaire
Queer Trans Pédéex Grossex Hyperactivex et HP que je suis.

{{%ligneblanche%}}

Une fois cela fait, se rappeler que notre colère nous veut du bien.
Qu'elle ne veut du mal qu'aux personnes et aux institutions qui nous
font du mal.

{{%ligneblanche%}}

Cultiver notre colère

Lui donner forme pour la rendre forte

Comprendre sa nécessité

Accepter sa nécessité

Ne jamais rien laisser passer. Jamais, jamais, jamais.

Accepter de laisser passer les choses quand il s'agit de nous protéger,
mais ne pas faire taire notre colère.

Lutter.

Se regrouper.

Se révolter contre tout ce qui est révoltant.

Être radicaleauxs.

Être sans concession.

Se regrouper.

Crier dans la nuit.

Crier tellement fort que le jour, lorsqu'on se réveillera on entendra
les échos de nos propres voix.

Découvrir l'anarchisme et se demander comment l'appliquer.

Être doucexs avec soi. Être gentillexs.

Noter que nous sommes plusieurs et que ce plusieurs prend de plus en
plus d'ampleur.

Noter que *Nous sommes partout*.

{{%ligneblanche%}}

Et faire de notre colère un terreau pour les fleurs de la Révolte.

{{%ligneblanche%}}

[^153]: Pour d'autres colères, lire *Cette colère immense, collective, transgénérationnelle, internationale* \[n^o^ 30\].

[^154]: Haut Potentiel.
