---
author: "Anonyme"
title: "C'est gratuit/It's free"
subtitle: "Comment occuper les péages de nos voisinexs ?"
info: "Texte rédigé pour le recueil"
datepub: "mars 2020"
categories: ["DIY", "sabotage, action directe", "syndicalisme, luttes des travailleureuxses", "squat, occupations, logement"]
tags: ["bouche", "délit", "essence", "force", "France", "frontière", "liberté", "lunettes", "pouvoir", "soleil", "souffle", "tuto", "visage", "voiture, bagnole"]
langtxt: ["fr"]
zone: "Suisse romande"
num: "22"
quote: "nous sommes légitimes"
---

Le texte est signé par une personne française habitant et militant à
Genève qui se déplace fréquemment en France voisine pour participer aux
mouvements des Gilets jaunes. Si l'action racontée se passe
effectivement en France, nous avons intégré le texte dans le recueil
pour rendre visible cette militance que la frontière n'arrête pas, qui
ne se situe pas et ne se pense pas par rapport à une délimitation
artificielle entre la Suisse romande et la France.

Alors, qu'est-ce qu'on fait ? Quand on voit ce qu'il se passe dehors et
dans le monde, on est obligéexs de se poser cette question : qu'est-ce
qu'on fait ?

Choisissons d'abord plusieurs objets et étalons-les devant nous. On ne
va pas prendre des objets au hasard. Prenons des lunettes de piscine, un
mégaphone, une voiture, des allumettes, un transpalette, un casque, des
masques, du gel hydroalcoolique, un parapluie, des clous, des palettes
et bien d'autres choses --- ce qui traîne à droite et à gauche dans nos
lieux d'habitation, dans nos rues. Étalons ces objets devant nous et
laissons-nous la liberté de nous demander ce que ces objets peuvent nous
permettre de faire. Regardons-les comme des outils pratiques. Pensons
concret !

Prenons la voiture. À quoi ça peut nous servir ? Je démarre, vous montez
à l'arrière, on s'engage sur la route. Par la fenêtre, vous voyez les
radars défiler, ceux qui flashent à gogo et vident nos épargnes. On
s'arrête à la pompe à essence, pompe à fric, pour que ça roule. Vous
savez que ça se paye le droit de conduire ces objets ? Permis,
assurances, leasings, contrôles techniques, taxes... Ça a l'air con tout
ça. Ça ne l'est pas. Ce n'est pas anodin, parce que c'est un peu le
point de départ du mouvement des Gilets jaunes. Et ça, ce n'est pas
rien.

Nous sommes maintenant sur l'autoroute, le vent souffle autour de
l'habitacle.

On s'engage sur une voie de sortie. Il y a un panneau : "Péage, 200
mètres". Il y en a beaucoup en France. On y paie une taxe pour un
trajet effectué sur une autoroute. À la base, cette taxe existe pour
amortir les coûts de construction et les salaires des travailleureuxses.
Mais bon, ça fait des années que ce coût a largement été amorti pour la
plupart des autoroutes françaises. Les autoroutes ont été vendues à des
grands groupes, au privé quoi. Maintenant, aux péages, on paye
directement les actionnaires.

La question que l'on va se poser maintenant, c'est "comment rendre un
péage gratuit ?"

Au premier abord, ça paraît assez simple. Il y a une barrière, assez
légère d'ailleurs, soulevez-la : c'est gratuit. Vous vous en doutez
bien, c'est pas si simple, mais on va passer en revue tous les détails
qui peuvent nous permettre d'occuper un péage nous-mêmes. Comme un
tutoriel quoi. Apprendre les savoirs pratiques des mouvements amis, ça
ne mange pas de pain.

Avant de passer à l'action, demandons-nous encore : sur quels réseaux
militants, citoyens, de voisinage, associatifs, va-t-on s'appuyer pour
faire ce type d'action ?

Ensuite, le nombre est une question essentielle. Si nous sommes
nombreuxses, nous allons pouvoir tenir l'occupation du péage plus
longtemps. Par contre, être beaucoup, ce sont des contraintes
d'organisation et de communication. Plus on veut être nombreuxses, plus
on va se sentir obligéexs d'utiliser des médias sociaux de masse non
sécurisés. Il y aura plus de risques de fuites d'information et les
troupes du pouvoir seront là avant nous. Au contraire, moins nous sommes
nombreuxses, plus c'est facile de s'organiser, mais plus c'est difficile
de tenir le péage longtemps. Faut pas se mentir, le bouche-à-oreille,
c'est ce qu'il y a de plus sûr. Il y a aussi des réseaux de
communication sécurisés qui peuvent nous rendre bien des
services[^58]. On va dire qu'il faut être minimum dix pour un petit
péage. À 50, on peut tenir un bon moment, quelle que soit la taille du
péage.

Une fois le groupe constitué, il faut définir un lieu et une heure de
rendez-vous et se demander comment arriver sur place[^59]. Plus le
lieu de rendez-vous est éloigné du péage, moins on aura de chance que
les plaques d'immatriculation de nos voitures soient relevées. Par
contre, être proche, ça permet d'intervenir rapidement. C'est à nous de
savoir ce qui est mieux selon la stratégie d'occupation adoptée. Bien
sûr, un repérage en amont est essentiel, notamment pour prévoir une
échelle s'il y a une barrière (ça permet à touxtes, quelle que soit la
condition physique, d'être de la partie) et, pour étudier les voies
d'accès. Il y a différents péages, de différentes tailles et différents
types d'accès. Les plus petits, on peut y accéder à pied. Les plus
grands sont protégés par des barrières.

Tout le monde est prêt ? Allez, on entre dans la zone. Notre objectif
est d'occuper le côté du péage où les automobilistes *sortent* et
payent. N'empêchons, en aucun cas, celleux qui *entrent* de prendre un
ticket, sinon iels auront une bien mauvaise surprise quand iels
sortiront de l'autoroute après leur trajet !

Ensuite, que faire des caméras ? Les péages sont équipés
de caméras et leur présence a été renforcée avec le mouvement des Gilets
jaunes. Il y a deux types de caméras. Celles qui sont prévues pour
relever les plaques d'immatriculation et celles sur pylône qui ont été
ajoutées, afin d'avoir des images générales. Les premières se situent le
long des voies de circulation, elles prennent les plaques à l'avant et à
l'arrière des véhicules. Vous pouvez les baisser avec la main ou prévoir
du scotch pour masquer les objectifs. Le scotch, ça permet d'être sûrex
qu'aucunex automobiliste ne puisse être inquiétéex. Et les caméras sur
pylône ? Bon, on va partir du principe que personne ne prend de
disqueuse, d'accord ?

Ce tutoriel est pensé pour qu'il y ait le moins possible de flagrants
délits. Ce choix permet de recruter large, de mener des actions faciles
à faire, funs, pas trop engageantes. D'ailleurs, pour lever les
barrières et masquer les caméras, on peut former un petit groupe masqué
qui entrera en premier : aucun individu ne sera associé à ce qui peut
être considéré comme un délit par un représentant de l'ordre
excessivement zélé. Ce groupe peut ensuite aller se changer hors du
champ des caméras ou tout simplement partir. Par la suite, le reste du
groupe peut avoir le visage découvert[^60]. La seule chose que nous
avons à nous reprocher, c'est de rendre gratuit ce qui *doit* l'être.
Nous sommes légitimes ! Dans ce tuto, on part de l'idée qu'il y aura
forcément des images de nous par la suite et que la tactique consiste à
ce qu'il n'y ait rien de "délictuel" (dégradations, vols, mise en
danger) sur ces images tant qu'on est à visage découvert.

N'oublions pas de touxtes nous munir d'un gilet fluorescent pour être
bien visible. Jaune, bleu, orange, rouge, multicolore, à chacunex ses
tips *fashion*.

Ensuite, il faut savoir que les péages ont été conçus de sorte qu'il y
ait des voies de circulation piétonnes : des travailleureuxses passent
par là et avant, il y avait même des guichets avec de vraies personnes.
Ce n'est plus le cas, mais les passages piétons existent encore et entre
chaque voie de circulation il y a des zones protégées par des
terre-pleins en béton. Restons dans ces zones et soyons vigilanxtes les
unexs aux autres. C'est une action qui peut être très grisante et on
peut facilement se mettre en danger là où des voitures roulent à grande
vitesse. Diminuons les risques, nos vies sont précieuses !

Une fois les barrières levées, la société d'autoroute appelle la troupe.
Une première patrouille arrive assez rapidement, ils sont moins de dix,
souvent trois. Généralement, leur première question est : "C'est qui
lae responsable, c'est qui lae cheffex ?" Votre réponse peut être :
"personne" ou "les Gilets jaunes" ou "Y'a pas de cheffex ici." Ne
donnons jamais de nom, ne nommons jamais une personne référente et
favorisons, par là même, les organisations non hiérarchiques pour que la
troupe ne cible personne en particulier. Diffusons la responsabilité !
S'ils veulent nous poursuivre, qu'ils osent nous poursuivre touxtes et
nous construirons une défense collective. La deuxième question qu'ils
peuvent poser, c'est : "Combien de temps allez-vous rester ?" Là
encore, préférons les réponses évasives : "On sait pas, on verra bien,
ça dépend du soleil."

Ensuite, ils nous demandent d'évacuer le péage, mais ce n'est pas prévu
pour tout de suite ! À partir du moment où la troupe est là, un rapport
de force s'installe : celui du nombre. Si nous sommes nombreuxses,
jamais la troupe n'engagera une opération pour vider le péage, parce
qu'iels se mettraient en danger. Il faut garder en tête que le péage est
un lieu dangereux. Pour mener une évacuation, la troupe va devoir vider
la sortie d'autoroute en amont et en aval, de sorte qu'il n'y ait aucune
voiture pour une intervention sans danger. Là est notre avantage, la
troupe a besoin de déployer un important dispositif et des effectifs
conséquents, s'iels décident de nous sortir *manu militari*. Iels vont
donc appeler des renforts, si renforts il y a, et ils mettront du temps
à arriver. C'est un enjeu stratégique majeur que de faire déplacer des
membres de la troupe : ça peut permettre à d'autres groupes amis de
mener des actions ailleurs. La tactique que je propose dans ce tutoriel
consiste à quitter le péage une fois que les renforts arrivent en nombre
suffisant et que le rapport de force bascule en leur faveur. On peut
négocier 10, 20, 30 minutes supplémentaires, ça ne coûte rien si la
situation s'y prête ! Préférons une posture pacifique pour ce type
d'action. N'oublions pas qu'ici, le cœur et le sens, c'est la
redistribution des richesses pour les automobilistes[^61].

La troupe ne doit pas accaparer notre attention, elle ne la mérite pas.
Bien sûr, l'autodéfense populaire et la solidarité en cas d'agression
doivent être un réflexe[^62]. Rappelons-nous que si nous occupons un
péage une ou plusieurs heures, ce sont déjà des centaines, voire des
milliers d'euros, qui restent dans les poches de celleux qui comptent à
nos yeux. Soyons satisfaixtes et donnons-nous rendez-vous une prochaine
fois !

Quand nous décidons de partir, à nous de voir si nous voulons laisser le
lieu dans l'état où nous l'avons trouvé en remettant les caméras en
place et en récupérant nos pancartes. Il peut être plus malin de
récupérer le scotch qui garde une bonne mémoire de nos empreintes et de
récupérer nos banderoles pour la prochaine fois. Aussi, n'oublions pas
une règle d'or : ne jamais laisser personne isoléex, pour ne pas créer
une vulnérabilité accrue face à des membres malintentionnés de la
troupe. Ce que l'on se dit souvent entre Gilets jaunes quand on part en
action c'est : "si on part à cinq, on revient à cinq". Cette vigilance
doit prévaloir dans les groupes d'affinité, mais aussi dans les groupes
élargis quand il y a plus de monde.

Sur une action péage gratuit, vous pouvez mettre en place une caisse de
grève ou une caisse solidaire. Lors du mouvement contre la réforme des
retraites (hiver 2020), des syndicalistes, des retraitéexs et des Gilets
jaunes m'ont raconté qu'iels ont récolté plus de 2 000 euros pour les
travailleureuxses en grève au péage de Cluses en Haute-Savoie. Il faut
se dire que les automobilistes ayant prévu de s'acquitter de la taxe ont
souvent déjà préparé de la monnaie avant d'arriver au péage. Heureuxses
de découvrir le péage gratuit, iels soutiendront volontiers votre cause.
C'est d'ailleurs une action très appréciée qui va nous permettre de
visibiliser et de populariser nos luttes. Il y a aussi des
automobilistes qui vont être un peu déboussoléexs et qui vont parfois
freiner ou changer de voie au dernier moment. Nous devons prévoir des
pancartes claires et visibles pour les informer à bonne distance.

Notre rôle est aussi de les accompagner dans cet acte de désobéissance
civile notamment en leur expliquant que leurs plaques d'immatriculation
ne seront pas relevées par les caméras (que nous avons pris soin
d'obstruer). S'iels souhaitent quand même donner leur argent aux
actionnaires, grand bien leur fasse, laissons-les faire, mais vous
verrez, c'est assez rare.

Et voilà, quand est-ce qu'on part sur le péage le plus proche ? Cette
action n'est bien évidemment pas la seule à réaliser et n'est pas
magique. D'ailleurs, tous les pays ne sont pas équipés de péages, et
heureusement. Ce tutoriel est aussi conçu pour nous permettre d'exercer
notre imagination à réaliser des actions possibles et pour s'éduquer
ensemble à l'<a href="/glossaire/#action-directe" class="glossary-link" target="_blank" rel="noopener" data-no-swup>action directe°</a>. Revenons aux objets que nous avions étalés
devant nous, je vous laisse imaginer tout ce qui peut être possible avec
un peu d'organisation

[^58]: Lire *Camouflage dans l'infosphère* \[n^o^ 40\] pour quelques éléments de sécurité numérique.

[^59]: *L'usure ordinaire* \[n^o^ 6\] offre quelques réflexions sur les points de rendez-vous et l'organisation.

[^60]: *Survivre dans un black bloc* \[n^o^ 15\] propose une réflexion tactique sur le fait de se changer et sur l'anonymat dans la rue.

[^61]: Lire *Comment bien rater un contrôle technique ?* \[n^o^ 54\] pour un questionnement automobile.

[^62]: *Spectacle nulle part.* Care *partout* \[n^o^ 23\] propose une réflexion sur l'autodéfense face à la police.

