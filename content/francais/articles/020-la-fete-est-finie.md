---
author: "Anonyme"
title: "La fête est finie"
subtitle: "Féminisme, teuf, safe spaces et inclusivité"
info: "Transcription d'un entretien oral"
datepub: "novembre 2020"
categories: ["féminismes, questions de genre", "autodéfense", "relation à la militance"]
tags: ["bienveillance", "brûler", "chat", "cigarette, clope", "colère", "crier", "cul", "fatigue", "force", "fête", "gueule", "huile", "impossible", "limite", "logique", "merde", "nuit, obscurité", "peur", "pouvoir", "prix", "violence", "voiture, bagnole"]
langtxt: ["fr"]
zone: "Suisse romande"
num: "20"
quote: "hyper déter, hyper vénère"
---

--- Est-ce que tu te considères comme militante ?

--- C'est difficile de répondre, ce serait arrogant de dire : "oui
évidemment je suis militante". Je fais des trucs, c'est clair, je
participe à des projets et j'en crée. Je vois des gens, on se retrouve
et on est <a href="/glossaire/#vénèrex" class="glossary-link" target="_blank" rel="noopener" data-no-swup>vénèrexs°</a>... Donc si la militance ça veut dire : essayer de
faire changer les choses en mieux dans une perspective radicale, oui.
Après, je ne suis pas dans un parti politique, je n'arrive pas à
concevoir la politique comme une forme de militance efficiente. J'ai ma
manière de militer. Mais je ne serais pas à l'aise de dire : je suis
militante. C'est un mot que je trouve trop vague, utilisé par des
personnes avec lesquelles je ne suis pas toujours d'accord.

--- Dans un de tes *posts* instagram, tu dis que tu t'es radicalisée ces
dernières années. Ça veut dire quoi pour toi ?

--- Ça veut dire que j'essaie de tendre de plus en plus vers une forme de
cohérence politique pour casser les dissonances cognitives. Mais bon, je
dis ça alors que je suis en train de rouler une cigarette avec un tabac
qui vient de dieu sait où, exploité par dieu sait qui. (*Rires.*) La
radicalité m'apporte un confort intellectuel. En face, on considère
souvent la radicalité comme quelque chose de dangereux, pour moi c'est
une manière de trouver des ressources, de gagner beaucoup de temps.
Arrêter de faire des compromis avec n'importe qui, arrêter de faire de
la pédagogie, de discuter avec des gens qui puent trop du cul... enfin,
qui ont des idées opposées à ce que je considère comme bienveillant. La
radicalité, je pense qu'elle est nécessaire, si on veut vraiment
changer, faire évoluer les choses, le compromis et la tiédeur, c'est
plus possible. C'est pas en bouffant tous les jours un burger que
t'as acheté au petit producteur du coin que tu vas faire évoluer la
question antispé' par exemple.

La radicalité est aussi liée à la question de la violence, c'est
important. La violence est légitime, pas uniquement celle liée à
l'autodéfense. La violence est une forme de proactivité. Casser des
choses, brûler des machins, crier, bloquer, occuper, se réapproprier des
espaces et des langages...

--- À quel moment dans ta vie ces questions ont commencé à t'occuper ?

--- Je suis tombée dans le chaudron de la radicalité et de la militance à
17 ans, quand j'ai déménagé à Saint-Imier. Dans ce bled, il y a un des
plus vieux centres anarchistes d'Europe. J'y ai passé sept ans à bouffer
pas mal de littérature et à faire des actions. C'est venu naturellement.
Enfin, je ne me suis pas réveillée un matin en me disant, hmm, je vais
devenir anar' et féministe. De par mon entourage et par la seule
structure intéressante du bled, j'ai mangé tout ça. C'était logique,
c'était radicalement plus à gauche que de m'inscrire dans un parti.
Quelque chose de beaucoup plus concret. À partir de là, la suite a été
logique : déménagement à Lausanne, fréquentation d'endroits alternatifs,
de squats, de l'Espace Autogéré.

--- Tu trouves où la force ?

--- À l'origine, il y a beaucoup de frustration et de colère et, au bout
d'un moment, c'est énergivore. Tu ne t'alimentes pas vraiment avec la
colère... par contre la colère des autres, ça marche bien. La solidarité
est très nourrissante aussi. Les collectifs, les potes, les espaces en
<a href="/glossaire/#mixité-choisie-ou-non-mixité" class="glossary-link" target="_blank" rel="noopener" data-no-swup>mixité choisie°</a>, l'<a href="/glossaire/#adelphes-et-adelphité" class="glossary-link" target="_blank" rel="noopener" data-no-swup>adelphité°</a>, la <a href="/glossaire/#sororité" class="glossary-link" target="_blank" rel="noopener" data-no-swup>sororité°</a> : c'est ça qui me tient
encore à flot. Parce que je t'avoue que j'ai beaucoup d'épuisement
militant et que ce n'est pas toujours facile de continuer à se
nourrir... Aussi, ma strat', c'est de faire moins de trucs avec des
collectifs larges, et plus de trucs affinitaires, très précis, où j'ai
moins l'impression de me répandre dans tous les sens et de m'épuiser. Le
gros de la force vient de la colère des autres, que je partage : on est
en colère ensemble, hyper contenxtes d'être simplement ensemble et c'est
hyper galvanisant.

--- Tu peux raconter le contexte des actions " La fête est finie! " et
comment ça a commencé ?

--- La genèse, c'était il y a deux ou trois ans. Une pote d'une pote
s'était fait droguer au GHB[^54] pendant une soirée au Bourg et elle en avait
parlé. C'était effroyable. Puis, il s'est avéré que durant cette même
soirée, il était arrivé la même chose à une autre femme qui n'a
malheureusement pas eu la "chance" de la pote de ma pote. Elle s'est
fait amener dans un camion et violer dans les bois... Horrible, vraiment
effroyable. Avec une copine, on s'est dit que ce n'était vraiment plus
possible, qu'on ne pouvait pas laisser passer ça. On a déjà entendu 1000
histoires horribles sur le GHB et le GBL, sur ce genre d'abus
sexuels, avec une absence totale de consentement. On a écrit un premier
texte, le texte de base de "la fête est finie !" et on l'a diffusé. On
a commencé par aller sur le lieu en question, on était une vingtaine à
distribuer des fascicules. Ensuite, j'ai pris contact avec des personnes
du Bourg, tant avec l'Association du Salopard[^55] qu'avec le Bourg.
Les réponses ont été assez tièdes. Les salopards disaient "c'est très
bien, c'est nécessaire, mais nous, on ne peut rien faire". J'étais là
"mais c'est complètement de la merde ta réponse, ça ne va pas". J'ai
donc repris contact avec un de ces types quelque temps plus tard pour
dire "hey, est-ce qu'on se voit pour en discuter, je pense que ce
serait bien qu'on vous file deux, trois outils pour faire en sorte que
ça n'arrive plus". Lettre morte, on n'a jamais répondu à mon courriel.

Quand il y a eu, une énième fois, des agressions au GHB à Lausanne l'été
passé, on a ressorti ce vieux texte, on a fait des banderoles, des
nouveaux fascicules à diffuser, et là, ça a fait un peu plus d'écho.
Grâce aux réseaux sociaux et à la Grève Féministe, j'ai l'impression que
ça a fait le tour, plus que lors de la première action il y a deux ans.
Ça a fait une chouette tache d'huile sur les terrasses lausannoises.
C'est une action qui faisait notamment écho à Bienne et à la
Chaux-de-Fonds, il y a eu des copines là-bas qui s'étaient fait droguer
plusieurs fois et la réponse des bars a toujours été la même : "si on
en parle, ça nous met en porte-à-faux avec les autorités, donc on
préfère cacher la merde au chat"...

--- Ou la réponse de la police fribourgeoise[^56]...

--- Ah ! "Ne marchez pas seule la nuit, fermez vos volets, ne laissez
jamais paraître que vous êtes une femme qui vit seule, ne montez jamais
dans la voiture d'un inconnu, etc.[^57]". Nom de dieu, merci le
patriarcat ! C'est vraiment effroyable, kikou la culture du viol,
horrible, vraiment indécent. Ça me gave, ça me met encore en colère.
D'ailleurs, lors des tractages, tu vois des personnes déjà
sensibiliséexs et extrêmement soutenanxtes, mais tu vois aussi les
réactions des autres, souvent complètement j'm'en-foutistes, voire
rigolardes. Il y a un type par exemple, qui nous a littéralement dit
"ah bah moi j'aimerais bien qu'on me drogue si c'est gratuit...".
C'est vraiment pas hilarant Jean-Charles, c'est vraiment de la merde ce
que tu dis. Certains bars étaient franchement mal à l'aise de nous voir
débarquer, parce que tu vois, des personnes un peu masquées, un peu
alterno, avec des tatouages, ben ça fait cheni dans le décor.

Au centre de tous ces problèmes, il y a aussi toute la question du
divertissement capitaliste. Comment, ces salles de concert et ces bars,
ont-ils capitalisé sur le fun ? Sans prendre soin des personnes qui leur
filent de la thune, des personnes grâce à qui, iels peuvent organiser
des soirées festives, etc. Je trouve que même dans les espaces politisés
on ne s'occupe pas assez de ce genre de questions. Qu'est-ce qu'on fait
pour anticiper ces problèmes, pour les prévenir ? Le slogan "ton GHB
dans mon verre, ma main dans ta gueule", c'est une réaction. La
violence qu'on propose est légitime et on continuera de l'utiliser. On
détruira des bars si on doit vraiment le faire, parce que ce n'est plus
du tout tolérable que les personnes qui organisent les soirées soient
ravies qu'il y ait plein de gens sans prendre soin d'elleux, voire en
prenant soin des potentiels trous du cul qui se permettent de verser des
produits dans les verres ou, à l'extrême, de leur faire du mal. À
Bienne, il y a même eu deux ou trois bars pire alternos, voire sauvages,
qui étaient là "oui mais après on ne peut plus faire la fête". Mais la
priorité elle est où ? Prioriser la fête sur la santé mentale et
physique d'autres personnes, c'est pas possible. Tu ne peux pas te
considérer comme un bar alterno ou un espace militant si tu mets les
priorités dans ce sens-là.

--- Ce texte, il proposait quoi concrètement ?

--- Le premier texte qu'on a fait est relativement tiède, c'est une série
de propositions de réactions pour les personnes qui pensent s'être fait
droguer et dans le pire des cas, pour les personnes qui ont été abusées
sexuellement. On dit qu'on peut alerter des proches, alerter si c'est
possible, s'il est informé, le personnel du bar, qu'on a aussi le droit
de ne rien faire, de ne pas vouloir porter plainte, de ne pas vouloir en
parler, etc. On a essayé de créer un truc extrêmement bienveillant
vis-à-vis des personnes qui ont subi ces attaques. Et puis on a essayé
de dire aux proches "vous pouvez réagir" et "il y a tout ça à faire".
On peut aller au CHUV (Centre Hospitalier Universitaire Vaudois),
on peut (surtout) entourer la personne avec bienveillance,
l'<a href="/glossaire/#empowerment--empouvoirement" class="glossary-link" target="_blank" rel="noopener" data-no-swup>empouvoirer°</a>, c'est-à-dire lui dire : "est-ce que t'as envie de faire
ça ?", "est-ce que t'as envie de boire un coca ?", "est-ce que t'as
envie qu'on se déplace ?", "est-ce que t'as envie de t'asseoir ?"
"est-ce que t'as envie de discuter ?", de sorte à renverser cette
dynamique victimisante, tout en restant extrêmement délicaxtes.

Toute une série de points abordent les questions de la plainte (comment
porter plainte ou comment ne pas le faire ?), de la dénonciation, de
l'identification de ce qu'on a subi --- est-ce qu'on se considère comme
une "victime" ou non ? etc.

Et il y a surtout le message "check tes potes". Et en fait, "check
tes potes", ça ne veut pas forcément dire check tes potes qui risquent
de se faire emmerder, ça veut surtout dire check tes potes qui risquent
de faire de la merde. Ceux qui font de la merde ne sont pas juste des
sociopathes sans potes, qui errent seuls dans les bars dans l'espoir de
droguer quelqu'unex. C'est vraiment important de le comprendre. On connaît
touxtes quelqu'un qui utilise ce genre de produit, on a touxtes pu
déceler des comportements inacceptables autour de nous. Après, chacunex
fait comme iel peut avec ses potes problématiques, tu peux ne plus
jamais lui parler, lui péter la gueule, faire de la pédagogie, le
sensibiliser, le faire réfléchir à ça, le *outer* (le dénoncer
publiquement dans son entourage)... Il y a plein de strats toutes
aussi bonnes les unes que les autres. Mais il faut arrêter de se *focus*
que sur les personnes qui subissent des agressions quand il s'agit de
faire de la prévention et de donner des conseils pour pas que ça arrive,
il faut réagir vis-à-vis des personnes qui font du mal.

On doit repenser le sens de la fête : est-ce que la fête est légitime
quand ça met en danger d'autres personnes ? J'ai l'impression que non.
D'où le choix du slogan "la fête est finie". On chie sur la fête si
elle n'est pas *safe* pour touxtes. Qu'on s'entende bien sur ce truc de
*safitude*, c'est impossible de créer un *<a href="/glossaire/#safe-space" class="glossary-link" target="_blank" rel="noopener" data-no-swup>safe space°</a>* parfait, et
l'ultrahygiénisme est aussi insensé... Mais c'est FINI d'organiser des
fêtes sans se dire qu'il faut que ce soit agréable pour touxtes. C'est
une réflexion qu'il faut étendre et avoir sur des questions féministes,
antiracistes, sur la lutte contre les oppressions de genre et
d'orientation sexuelle. Il faut que la fête soit agréable ou qu'il n'y
en ait plus.

--- Évidemment, il y a la solidarité et la bienveillance dans un cercle
de gens qu'on connaît, mais ça remet la responsabilité sur les gens, sur
nous. Quid des réactions merdiques, de la prise en charge médicale, de
la prise en charge de la police ? Est-ce que tu as des ressorts ou des
idées pour atteindre aussi ce niveau-là ?

--- Pour moi, militer c'est aussi pouvoir militer à des strates dans
lesquelles on se sent efficienxte. Moi, je suis pas une personne qui
aurait envie de discuter avec la municipalité, avec la police du
commerce ou pire, la flicaille. Je pense qu'il y a d'autres gens qui
sont capables de le faire bien mieux que moi. J'aime bien cette idée
d'envisager la militance comme un ensemble de strates. Il y a celle du
terrain hyper déter, hyper vénère, potentiellement hyper "radicalisée"
et il y a, peut-être, celleux qui arrivent à faire des ponts entre les
différentes strates, jusqu'à pouvoir interpeller le chef de la police
communale, ou cantonale, ou réfléchir carrément à des aspects
législatifs... Moi je ne suis pas dans ce délire-là, du tout. Mais
j'ai l'impression que les actions "La fête est finie !" sont
arrivées à plusieurs niveaux : il y a eu des contacts avec Fribourg par
exemple, qui a mis en place, depuis des années, des trucs assez concrets
sur la prévention en soirée. L'idée c'est d'arriver à des effets plus
globaux. Mais ça ne sera pas possible tant qu'on aura pas détruit et le
capitalisme, et le patriarcat, et le racisme, et toutes les oppressions
qui y sont liées.

--- T'as l'impression qu'il y a eu des effets directs de ce que vous
avez fait, des prises de conscience ?

--- Oui, clairement. Ne serait-ce que faire peur aux tenancièrexs et
autres limonadièrexs, c'est assez chouette. Tu les vois chouiner sur
les grands internets en disant "mais quoi j'ai reposté votre story,
pourquoi vous m'agressez ?". Je trouve assez cool que ça mette un
petit malaise, qu'il n'y ait plus une impunité totale. Le rayonnement
sur les réseaux sociaux fait que les gens se calment un peu le
cul. Lors des actions physiques, il y a deux ou trois Jean-Kevin qui
font des blagues extrêmement gênantes, qui refusent de prendre le
fascicule parce que ça risque de tacher leurs mains de féminisme
(beurk). Mais tu vois aussi que ça en remet certains dans leurs petits
souliers. Et ça, c'est vraiment extrêmement agréable, cette manière de
reprendre l'espace public tout en disant "poussez-vous, vous allez pas
*<a href="/glossaire/#mansplaining-mansplainer" class="glossary-link" target="_blank" rel="noopener" data-no-swup>mansplainer°</a>* dans tous les sens, dans ces terrasses, ces rues, ces
bars on a aussi le droit d'y être". Plus on est nombreuxses dans les
rues à être fâchéexs, plus on prend l'espace, plus on devient légit',
et plus on a les coudées franches, plus on peut se sentir à
l'aise. Rien que ça c'est déjà très confortable.

--- C'est intéressant parce que l'excès est globalement accepté, voire
encouragé dans le contexte de la fête, mais le soin apporté aux autres
ne l'est pas.

--- Clairement. Droguez-vous si vous avez envie, si ça vous fait plaisir,
je m'en fous, pas de jugement. Mais si tu te défonces assez pour que tu
ne remarques même pas que les autres sont dans une situation à risque,
c'est un peu ennuyeux. Du coup, peut-être développe des stratégies pour
te dire "ok, ce soir je me défonce à fond, mais je vais checker avec
d'autres potes" --- alors ça peut être les potes du bar, ça peut être la
clientèle, ou des travailleureuxses sur place : il faut qu'il y ait des gens avec
l'esprit assez clair pour prendre soin des autres, pour ne pas
encourager la surconso tous azimuts.

--- C'est cool de ne pas se limiter aux réseaux sociaux, on évite
l'entre-soi. Tu ne vas pas forcément toucher Jean-Kevin sur ton réseau
facebook, mais peut-être que tu vas le toucher avec un flyer parce
qu'il sera dans ce bar.

--- C'est fondamental. On avait aussi mis en place un petit infokiosque
portable sur un caddie, avec plein de brochures féministes. Ça parlait
de contraception, de vasectomie, de la légitimité des espaces en
<a href="/glossaire/#mixité-choisie-ou-non-mixité" class="glossary-link" target="_blank" rel="noopener" data-no-swup>mixité choisie°</a>... plein de petites brochures auxquelles les gens pouvaient
avoir accès gratuitement. La question de l'accessibilité à ce genre de
brochure est importante. C'est vraiment bien le support papier, c'est
très bien de le diffuser, mais il y a aussi très vite des langages qui
vont être des langages d'entre-soi militants. Il y a plein de
néologismes qui ne sont compris que dans certains réseaux. On se pose la
question de les réécrire, voire même d'en faire des formats audio, pour
les personnes malvoyantes ou pour celleux qui ont des troubles de
l'attention, des difficultés de lecture, etc. On s'est dit que ça
pourrait être aussi très rigolo de prendre d'assaut des soirées avec
une boombox et puis de balancer ces textes hyper fort. Tu sais tout à
coup : pfouah, on baisse le volume sur la table de mix et on envoie ! Le
fait de monter sur une scène et de diffuser ces messages, d'attendre
les gens à la sortie, d'avoir ces trucs qui tournent, c'est aussi des
bonnes strat' pour atteindre des gens qui n'ont pas trop envie d'écouter
ou qui n'ont pas la possibilité d'y accéder.

--- On en revient à ce que tu disais plus tôt, l'importance de prendre
possession ou de reprendre le pouvoir sur des espaces, en se les
réappropriant, très concrètement.

--- À l'époque, quand j'étais pas encore trop fatiguée par les nuits
lausannoises, on organisait beaucoup de soirées à l'Espace Autogéré,
c'était méga bien. Mais il y avait vraiment un énorme travail. Quand
t'es derrière le bar ou à la caisse, tu te casses le cul à expliquer la
vie --- enfin la vie... --- en tout cas les fonctionnements internes comme
le <a href="/glossaire/#prix-libre" class="glossary-link" target="_blank" rel="noopener" data-no-swup>prix libre°</a>, ce qu'on considère comme des comportements relous,
pourquoi on va potentiellement te sortir de l'endroit ou, si on a
l'énergie, t'expliquer pourquoi ce que tu fais ne correspond pas du
tout aux positions politiques du lieu. J'étais plus à l'aise de faire ça
moi : expliquer concrètement sur place nos modes de fonctionnement
plutôt qu'à l'entrée. Mais par contre on devait se préparer pendant
plusieurs réus en amont : se mettre touxtes d'accord, mettre en place un
petit cordon de bienveillance qui se déplace durant la soirée,
rencontrer les nouvelles personnes qui participent à l'orga de la soirée
pour discuter avec elleux. Ça me paraît indispensable si tu veux que ta
soirée ait un minimum de cohérence politique.

J'aimais bien que ce soit un peu la gueule du loup quand on organisait
nos soirées. On se réjouissait presque des trous du cul qui avaient un
comportement inadéquat pour pouvoir leur bourrer le mou avec des trucs,
des cinq ou six meufs et mecs non cis qui tiennent des conversations
avec l'index brandi en gueulant. Jean-Cishet, soit il revenait plus et
c'est bien, soit il revenait dans un état d'esprit différent. Et puis
Jean-Cishet peut potentiellement en parler à ses potes, en bien ou en
mal, c'est pas grave, mais en tout cas le message se diffuse quoi.

--- Tu disais qu'aujourd'hui t'es épuisée, mais tu continues quand même à
faire des trucs. Tu trouves quand même l'envie et l'énergie.

--- Il y a vraiment plein de militanxtes épuiséexs. Tout le monde dit
qu'iel est fatiguéex. Et souvent, la strat' c'est de se mettre en
retrait. Du coup tu perds les gens, tu perds les réseaux, tu perds les
affinités. Donc les nouvelles personnes doivent réinventer la roue à
chaque fois, c'est hyper chiant. J'aimerais avoir plus d'outils pour
savoir comment ne pas s'épuiser. L'épuisement complet, ça m'a menée à ne
faire que des trucs en mini groupe, mais vraiment mini mini mini, des
trucs extrêmement précis, qui me font pire du fun. Organiser une manif
par exemple, je ne le ferais plus, je n'en serais plus capable.
J'arrive juste à marcher. Si vraiment je veux me donner de la peine,
j'ai un panneau et des slogans et c'est cool, à la limite je prends
quelques bières pour celleux qui en veulent, ou des boissons sans alcool
parce que c'est important aussi.

--- En faisant des choses plus petites, tu vois les effets directs de ce
que tu fais. L'épuisement vient quand t'as quelque chose de trop gros
en face de toi, non ?

--- Bah quand t'as le patriarcat en face...

Il faut remettre au centre de la militance, la notion de plaisir, c'est
hyper important. On peut parfois l'oublier dans les luttes militantes
vénères. Il faut vraiment qu'on ait du fun. Si on n'a plus de fun, ça
apporte de l'aigreur, de la fatigue et de la frustration. Les réus
deviennent tendues, il y a moins d'écoute, moins de bienveillance...
Il faut un militantisme joyeux.

[^54]: Le GHB comme le GBL sont des substances qui induisent un état de désinhibition et d'euphorie. À haute dose, elles ont un effet sédatif qui augmente encore quand elles sont mélangées à de l'alcool. Surnommées "drogue du violeur", elles plongent dans une profonde inconscience et provoquent une forte amnésie.

[^55]: L'Association du Salopard est à l'origine du projet culturel du Bourg, programmant des concerts, performances, projections, spectacles et autres activités culturelles à Lausanne depuis 2005. Après 15 années au Bourg, l'association a quitté la salle lausannoise et poursuit une saison d'itinérance dans divers lieux lausannois.

[^56]: En août 2020, la police fribourgeoise a publié une liste de conseils aux femmes afin de "prévenir les délits et les agressions sexuelles". Après les multiples réactions, notamment sur les réseaux sociaux, dénonçant le paternalisme de ces conseils et la culpabilisation des victimes, ce guide a été retiré et des excuses ont été présentées.

[^57]: On peut *encore* trouver la liste des "conseils" donnés par la police fribourgeoise sur le site Skppsc.ch, le site de prévention criminelle suisse. URL : [*www.skppsc.ch/fr/wp-content/uploads/sites/5/2016/12/guidedesecuritedelapolice.pdf*](https://www.skppsc.ch/fr/wp-content/uploads/sites/5/2016/12/guidedesecuritedelapolice.pdf).

