---
author: "Emmathegreat"
title: "Stay"
subtitle: "Poème"
info: "Texte rédigé pour le recueil"
datepub: "janvier 2021"
categories: ["antiracismes", "relation à la militance", "luttes migratoires"]
tags: ["sexe", "amour, love, amoureuxse"]
langtxt: ["de", "fr"]
zone: "Suisse romande"
num: "10"
quote: "montre-moi le pire de toi, je le prends"
---

If I knew what I know now, I would have walked up to Her and say
“Stay”.

These could be the worst parts of me, I have some best parts. You want
them? then “Stay”.

There were some adorable ones. They left, they couldn't wait, maybe
because I never said “Stay”.

Show me Your worst side, I'll take it, because if I don't, then I
don't deserve your best side. I'll never leave as long as you
“Stay”.

“Stay!” Like it's the only thing I ask for.

“Stay!” Like every good sex we have, we want more.

“Stay!” Just like how we forget about the bad times while having some
good times.

“Stay!” Like you're the only road I took that leads me Home. Not Rome.

“Stay!” I'll remember how long you stay. How you do the things that you
do, that make me call you “Chouchous” In Switzerland we say “Coucou”. My
native name is Ebuka, but relatives call me “Bubu”.

“You're a nice guy”. That's what they all say.

“You have a big heart”. But they can have the same.

“l love You”. That's what we all say.

“Stay!” That's what I'll do.


## Reste

Si j'avais su ce que je sais aujourd'hui, je me serais approché d'Elle
et je lui aurais dit "Reste !".

C'est peut-être ce qu'il y a de pire en moi, mais j'ai aussi de bons
côtés. Tu veux les découvrir ? Alors "Reste !".

Il y en a eu des adorables. Ils sont partis, ils ne pouvaient pas
attendre, peut-être parce que je ne leur ai jamais dit "Reste !".

Montre-moi le pire de toi, je le prends, si je ne le fais pas, je ne
mérite pas le meilleur. Tant que tu "Reste !", je ne te quitte pas.

"Reste ! " Comme si c'était suffisant.

"Reste !" Comme quand le sexe est bon et qu'on en veut encore.

"Reste !" Comme on oublie les mauvais jours quand tout va bien.

"Reste !" Comme si t'étais le seul chemin qui m'avait mené chez moi. Pas
à Rome.

"Reste !" Je me souviendrais du temps passé ensemble, de toutes ces
choses que tu fais qui me font t'appeler "Chouchous". En Suisse, on dit
"Coucou" Mon vrai nom est Ebuka, mes proches m'appellent "Bubu".

"T'es un mec sympa". C'est ce qu'iels disent touxtes.

"T'as un grand cœur". Iels pourraient en avoir un aussi.

"Je t'aime". C'est ce que nous disons touxtes.

"Reste !" C'est ce que je vais faire.

