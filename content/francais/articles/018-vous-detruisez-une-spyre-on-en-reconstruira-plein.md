---
author: "Collectif Bambou"
title: "Vous détruisez une Spyre, on en reconstruira plein"
subtitle: "Lettre aux propriétaires de la maison occupée"
info: "Lettre retravaillée pour le recueil"
datepub: "décembre 2020"
categories: ["squat, occupations, logement", "autogestion, expérimentations collectives"]
tags: ["absurde, absurdité", "bambou", "canapé", "conviction", "doute", "détruire, destruction", "feu", "force", "injustice", "jardin", "justice", "liberté", "logique", "luxe", "nuit, obscurité", "pouvoir", "prix", "propriétaire", "racine", "rage", "souffle", "violence"]
langtxt: ["fr"]
zone: "Suisse romande"
num: "18"
quote: "bienvenue dans notre monde, celui qu'on construit dans les poubelles du vôtre"
---

Le collectif bambou a été évacué de la Spyre au début du mois de janvier
2020, après l'envoi de cette lettre.

{{%ligneblanche%}}

*On écrit ces lignes depuis la Spyre --- ses grandes maisons rose pâle et
son immense jardin retiennent leur souffle, se demandant combien de
temps il nous reste. La Spyre, c'est ce squat perché dans les hauts de
Pully, où, depuis déjà plus de trois mois, on crée ce drôle de processus
de <a href="/glossaire/#gentrification" class="glossary-link" target="_blank" rel="noopener" data-no-swup>gentrification°</a> inversée, comme une incursion de vie communautaire et
engagée dans un quartier bourge et individualiste. On écrit coincéexs
entre plein de personnes incroyables, sur un canapé qu'on devra sûrement
abandonner. Dehors, d'autres se réchauffent autour du feu qui brûle dans
le vieux puits décoré de fer forgé. Les maisons vibrent de vie, de
réjouissance, mais aussi d'incertitude --- c'est potentiellement un de
nos derniers jours ici. On oscille entre plein d'émotions beaucoup trop
intenses. Aujourd'hui, plusieurs personnes se sont fait poursuivre par
la police en essayant de revenir à la maison. On sent qu'on n'a plus le
luxe de remettre nos rêves à demain. La répression des lieux alternatifs
ne semble pas aller en s'adoucissant.*

*Après avoir reçu la décision du tribunal qui nous demande de quitter
les lieux dans les 48 h, on a décidé d'écrire une lettre aux
propriétaires. On ne sait pas trop si on l'a écrite pour eux, pour leur
faire changer d'avis, ou pour nous, pour pouvoir pousser un dernier cri
avant de disparaître. En tout cas, le lendemain de la réception de notre
lettre, ils ont quand même donné l'ordre à la police de nous évacuer dès
que possible. On ne sait pas encore quand cette décision sera exécutée,
mais la résistance s'organise.*

Voici cette lettre --- on espère qu'elle aidera à saisir tout ce qui
nous anime dans cette période périlleuse d'activisme sur fond d'urgence
climatique et de pandémie globale. Les mots ne rendront jamais justice à
l'amplitude émotionnelle de ce qu'on vit ici, mais au moins, ils
pourront laisser une trace de ce combat contre l'immobilier de luxe et
son monde.

## Lettre ouverte à Dune Capital SA, propriétaire des bâtiments de la Spyre, lieu de vie et d'activités alternatives et engagées

La Spyre, peut-être que vous n'en aviez jamais entendu parler avant. On
commençait à bien se faire connaître dans le monde militant. En dehors
de ces réseaux-là, on n'avait pas priorisé l'image qu'on renvoyait, on
était trop occupéexs à vivre, à construire, à créer. Mais désormais, on
n'a plus trop le choix, on utilise une de nos dernières armes : le
pouvoir des mots et des histoires, même si on se doute que la nôtre
risque de disparaître, car l'histoire est toujours écrite par les
vainqueurs. On vivait un peu cachéexs, mais aujourd'hui, on veut
partager quand même ce que vous vous apprêtez à démolir : nos envies,
nos créations, nos maisons. Bienvenue dans notre monde, celui qu'on
construit dans les poubelles du vôtre.

Hier, à la tombée de la nuit : on est dehors, sur la terrasse de notre
maison, celle qu'on habite depuis bientôt trois mois. On est
heureuxses, on discute du passé de cet endroit, de tout ce qu'on y a
vécu. On s'aventure même à parler de l'avenir --- chose rare car on est
encore en suspens sur ce qui va se passer. On checke nos mails --- et
c'est la douche froide : le tribunal estime que la situation juridique
dans laquelle nous nous trouvons est limpide et nous donne 48 h pour
partir. On savait très bien que la soi-disant "justice" était de votre
côté. On met "justice" entre des guillemets car on a de la peine à
voir ce qu'il y a de juste dans le fait que certaines personnes peuvent
avoir plusieurs maisons --- dont certaines ne seront jamais utilisées ---
pendant que d'autres n'en ont pas.

Il y a presque trois mois, on vous rencontrait pour la première fois.
C'était dans des circonstances un peu particulières, pas forcément
idéales. On avait occupé des bâtiments qui vous appartiennent. Ces
bâtiments, vous les avez achetés il y a deux ans dans le but de les
démolir, pour remplacer un immense terrain verdoyant par des immeubles
d'appartements de luxe.

La Spyre a moins de trois mois, mais c'est devenu un lieu de rencontres,
de création et de vie. Après deux années à l'abandon, ces maisons ont
été habitées par un nombre incroyable de personnes différentes et de
moments forts ; elles ont été réparées, repeintes, aimées. Ce n'est pas
juste un projet d'habitat personnel, c'est un projet politique qui met
en place et défend des modes de vie plus communautaires, plus solidaires
et plus écologiques. C'est énormément de travail de réflexion,
d'organisation, de logistique, de construction, de décisions collectives
et de prises de risque. Sachez que chaque fois qu'un squat se fait
expulser, c'est tout un monde qui s'effrite. Ce sont des personnes qui,
du jour au lendemain, se retrouvent sans chez-soi, ce sont des
centaines, des milliers d'heures de travail qui disparaissent
brutalement.

On se demande comment vous avez réagi à la réception de la décision du
tribunal. Vous avez sûrement dû célébrer ça. Nous, on pleure, on rage,
c'est toute notre vie qui bascule du jour au lendemain.

Vous nous avez déjà exprimé que ça vous semble bien culotté qu'on vous
expose ces ressentis et ces sentiments d'injustice, alors que nous
sommes *chez vous*. Or, c'est peut-être là, le problème. Aucune
communication constructive n'a pu être établie, car la toute-puissance
culturelle et juridique de la propriété privée vous aveugle. D'emblée,
nous étions à vos yeux des criminellexs, certes *gentillexs*,
mais des criminellexs quand même. Tout le système
pousse à ce constat et vous en perpétuez les méfaits à travers votre
refus de la discussion. Vous allez expulser des personnes qui n'auront
probablement jamais vos privilèges, votre richesse et votre protection
sociale. Vous détruisez un habitat regorgeant de vie en croyant en
fabriquer un autre. Or, dans le quartier, les appartements du même type
que ceux que vous prévoyez sont, pour la plupart, vides. Vous ne pensez
pas ces espaces pour les faire vivre, mais pour les faire fructifier, au
détriment des personnes qui ont simplement besoin d'espaces. Vous
détruisez de la vie pour créer du vide, qu'il soit physique, social ou
moral, tout en continuant d'alimenter les mécanismes qui pillent les
ressources terrestres et poussent notre société dans ses derniers
retranchements. Le simple fait que ce vide vous enrichisse vous suffit
pour, chaque jour, fermer les yeux sur les vérités qui sont devant vous,
devant nous touxtes. Cette lettre sera probablement notre dernière, mais
vous, vous continuerez d'être confrontés à cette réalité créée par le
camp que vous avez choisi. Vous continuerez d'observer l'accroissement
exponentiel des inégalités sociales, la montée du niveau des océans, les
migrations climatiques que cela va induire, ou encore la jeunesse
révoltée dans les rues contre les mains sales du capitalisme.

Vous avez encore le choix, puisque vous n'avez pas encore de permis de
construire. Venez nous rencontrer en personne ! Venez parler d'humain à
humain, sans l'intermédiaire du langage juridique qui protège les
intérêts financiers ! Venez voir ce qui a été créé avant de donner à la
police l'ordre de nous sortir avec force et violence. On ne sait pas si
vous nous entendez, si vous nous comprenez : on parle le langage du
partage et des possibles ; vous nous répondez à coups de lois absurdes
et d'amendes. Venez, si vous osez sortir des chemins confortables et du
statut social que vous procure votre pouvoir financier. Venez voir ce
qu'on fait ici, comment on vit avec des chambres pleines de lits, des
têtes pleines de rêves et des cœurs pleins de vie. Venez goûter le sirop
de romarin fait avec les plantes du jardin. Venez vous confronter
personnellement à ce que vous vous apprêtez à détruire, à cette brèche
qu'on a ouverte dans le destin de ce terrain que vous voulez vider à
tout prix.

Vous nous pensez utopistes ? Ça fait longtemps qu'on ne l'est plus.
Nos actions sont une concrétisation d'alternatives réalistes face à ce
qui se déroule dans ce monde. La vie avance, on en tire des leçons à
force de se la ramasser en pleine face. Alors on continuera d'essayer,
on continuera à créer des modes de vie plus communautaires, plus
résilients. On continuera à contester la logique selon laquelle il est
normal que la majorité de notre temps soit vendue pour enrichir les
riches et détruire le vivant. On vit en squat pour plein de raisons :
par besoin, par conviction, par envie. On squatte pour libérer du temps
et pour le consacrer à des actions qui font bouger, au moins un peu, le
monde que vous défendez si ardemment. Cette lettre n'est pas une
attaque personnelle, elle dénonce une situation qui fleurit et meurt
dans un contexte très spécifique, mais qui prend racine dans des
problèmes et des dynamiques bien plus larges. L'expulsion du lieu de
vie et de rencontres qu'était la Spyre peut sembler n'être qu'une
petite vague face à l'ampleur du désastre global, mais
elle est un symptôme bien concret d'un système qui court à sa perte.

Pour terminer, nous ne citerons qu'une seule phrase du tribunal, bien
emblématique de l'absurdité de la situation : "*...l'argument selon
lequel les bâtiments demeureraient vides durant plusieurs années avant
d'être démolis ne légitime aucunement les membres du Collectif bambou à
usurper les lieux, chaque propriétaire étant libre d'exercer sa
possession comme il le souhaite.*"

Mais bon, on espère que votre liberté d'exercer votre possession en la
vidant de toute vie vous fera plaisir.

Vous détruisez une Spyre, on en reconstruira plein.

