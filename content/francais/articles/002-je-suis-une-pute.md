---
author: "Anonyme"
title: "Je suis une pute"
subtitle: "Petit guide pratique pour putes révolutionnaires"
info: "Texte rédigé pour le recueil"
datepub: "novembre 2020"
categories: ["féminismes, questions de genre", "travail du sexe", "DIY", "autodéfense"]
tags: ["amour, love, amoureuxse", "corps", "force", "internet, web", "pouvoir", "pute", "rue", "sexe", "théâtre", "téléphone", "visage", "voiture, bagnole", "voix"]
langtxt: ["de", "fr"]
zone: "Suisse romande"
num: "2"
quote: "un mot pour exprimer une jouissance, un plaisir"
---

Je suis une pute.

Intriguéexs ? Dégoûtéexs ? Envie de m'avoir ? Cela peut vous choquer,
mais je m'en fous. Vous vous demandez peut-être pourquoi je fais ça ?
Vous vous dites qu'il y a forcément d'autres manières de gagner sa vie ?
Que c'est de la thune facile[^6] ? Que je suis accro au sexe ? Que je
n'ai pas d'autre choix ? Je n'ai pas de comptes à vous rendre, mais je
vais vous laisser entrer dans ma vie privée, le temps de ce texte.

Tout a commencé par une envie de faire du téléphone rose --- il paraît
que j'ai une voix adaptée pour ça --- mais je ne savais pas où trouver
les informations pour me lancer. Ensuite, j'ai voulu être
strip-teaseuse, j'aime danser, ça aurait pu être fun, mais là encore, je
ne savais pas comment m'y prendre. Je me suis dit que ça serait plus
simple de s'informer sur le métier de prostituée. C'est l'amie d'une
amie, elle-même pute, qui m'a expliqué comment elle s'y est prise pour
débuter. J'en ai parlé à plusieurs potes qui avaient aussi envie de
commencer. Je vous assure, il y a plus de travailleureuxses du sexe ou
de futurexs travailleureuxses du sexe qu'il n'y paraît.

Alors, comment devient-on travailleureuxse du sexe (ci-après TDS) ? On
ne s'en rend pas forcément compte, mais le TDS, c'est pluriel : il y a
mille manières de le pratiquer. Dans la rue, sur internet, via les
annonces de journaux, avec plusieurs clients réguliers, avec un seul
client régulier, uniquement avec des premiers coups, dans la rue, dans
une voiture, dans ta chambre, chez les clients. Je vais vous présenter
une méthode parmi d'autres, la mienne, celle qui me permet de rester
anonyme et de m'assurer que mon entourage ne l'apprenne pas.

1. S'inventer un faux nom. On a touxtes voulu s'appeler autrement à un
  moment donné dans notre vie, c'est l'occasion : Sacha, Monika,
  Cerise, Cannelle...
2. Se construire une fausse identité avec une fausse histoire, tout en
  restant plausible. Inventez-vous une histoire et soyez prêxtes pour
  les questions. Ne dites pas que vous êtes étudianxtes en physique si
  vous ne connaissez pas les lois de Newton. Souvent, les clients
  aiment vous écouter parler de vos études, de votre boulot, de votre
  vie. Il m'est déjà arrivé de dire des trucs contradictoires sur mon
  année d'étude et sur mon âge. J'ai dû improviser toute une histoire
  selon laquelle j'avais sauté des classes pour que les deux puissent
  correspondre.
3. Prendre des photos de votre corps. Les photoshootings avec unex
  pote, c'est plus marrant. Perso, je conseille d'éviter de faire
  circuler des photos de votre visage pour éviter de se retrouver dans
  une situation de *stalking* (harcèlement) ou d'extorsion.
4. Fixer une tranche salariale qui vous semble correcte. Je dirais 200 CHF
  minimum par heure et 400 CHF minimum par soirée. Perso, j'ai
  réussi à me faire jusqu'à 700 CHF par soirée. Pour garantir
  l'anonymat, n'acceptez que le cash.
5. Créer un compte, par exemple sur petitesannonces.ch. Dans la
  rubrique érotique, vous pouvez trouver des annonces ou en poster
  vous-mêmes. Présentez-vous un minimum, dites ce que vous aimez et
  explicitez bien vos limites. Les clients n'aiment pas forcément les
  "professionnellexs", se présenter comme unex TDS occasionnellex peut
  être avantageux. Un client m'a dit une fois que les
  "professionnellexs" le considèrent comme un simple moyen de gagner
  de l'argent, comme un client parmi d'autres, que le temps est trop
  bien calculé, etc. Bref, que ça ressemble trop à du boulot. Il y a
  aussi énormément de mépris, certains trouvent que les TDS qui
  pratiquent depuis longtemps sont "salexs" ou "étiréexs du
  vagin".

Jusque-là, ça paraît simple, facile et efficace, mais en ce qui me
concerne j'ai découvert la dose de travail qu'il peut y avoir entre le
moment où tu mets ton annonce et le moment où tu reçois ta thune.

Sur 40 messages envoyés, 25 répondent. Sur ces 25, 15 seulement
poursuivent l'échange. Sur ces 15, 10 demandent un rendez-vous, sept
annulent à la dernière minute et 2 ne vont simplement pas se pointer au
rendez-vous. Quand il y a trop de messages et que ça n'aboutit pas à un
rendez-vous ou que vous ne le sentez pas, passez à autre chose. Il ne
faut pas forcément s'accrocher aux premiers venus, c'est pas grave, je
vous assure, il y aura d'autres clients. Les clients réguliers, c'est
super pratique. Ça évite de mettre trop d'énergie dans les échanges de
mails et du point de vue de la confidentialité et de la sécurité, c'est
plus facile à gérer. Franchement, si je pèse le temps passé à échanger,
à planifier, à me déplacer et l'argent effectivement gagné, c'est pas
aussi bien payé qu'on peut se l'imaginer. Alors pourquoi continuer ? Je
trouve ça thérapeutique. Je le vis comme une prise de pouvoir sur les
mecs <a href="/glossaire/#hommes-cis--mecs-cis--cisgenres" class="glossary-link" target="_blank" rel="noopener" data-no-swup>cis°</a>. J'ai un passé d'abus sexuels. Avec mon premier client, je me
suis sentie étonnement en sécurité, encore plus qu'avec les partenaires
que j'avais pu avoir auparavant. Ça m'a fait réfléchir. Évacuer tout
aspect sentimental, ça a été un énorme pas pour réussir davantage à
gérer ma sexualité.

Quand j'ai rencontré mon premier client, je suis restée allongée sur le
dos, avec sa tête entre mes jambes, sa langue sur mon clito. C'était son
*kink*, le seul truc qu'il voulait faire : je me plains pas. Ce soir-là,
je me suis sentie puissante, j'ai repris une forme de pouvoir, quelque
chose qui m'a aidée à continuer à soigner les blessures de mon enfance.
Depuis, avec chaque nouveau client, je reprends encore davantage le
contrôle sur ma sexualité et je profite un peu plus du système
patriarcal qui m'est imposé.

Petites anecdotes :

Une fois, avec un client, on est passé en voiture à côté d'un lieu où se
trouvaient mes parents. Je me suis baissée d'un coup dans la voiture et
mon client n'a rien compris. Réfléchissant, après coup, j'aurais pu leur
dire que je faisais du stop.

Une fois, j'ai eu l'occasion de parler d'anarchisme à un ancien
communiste, devenu capitaliste. C'était une discussion assez
constructive et c'était fun de parler de ça dans ce contexte. Je me suis
demandé si, en me décrivant comme anarchiste, je n'en avais pas un peu
trop dit sur mon identité. Oups.

Une fois, pendant qu'un client me faisait un cunnilingus, j'ai joui et
j'ai crié fort, sans faire exprès, "PUTAIN !". C'était la première
fois de ma vie que je criais un mot pendant un orgasme et il s'avère que
c'était un mot pour exprimer une jouissance, un plaisir. Putain. Ce mot
est utilisé à tort et à travers tout le temps. Arrêtez d'utiliser ce
terme comme une insulte ou comme un juron, apprenons à le considérer
d'une manière nouvelle, comme un mot plein de force, de joie et de
jouissance.

Pourquoi ne pas profiter de ce système patriarcal qui nous est imposé ?
Ce système que nous subissons sans choix et qui nous objectifie ? C'est
justement parce qu'il fait de nous des objets que nous pouvons choisir
d'utiliser ses outils contre lui, comme un ressort pour renverser les
positions de pouvoir. Contrairement aux propos putophobes de certainexs
féministes qui veulent abolir la prostitution[^7], ce n'est pas
toujours quelque chose que l'on subit, au contraire, pour certainexs,
c'est un moyen de reprendre le contrôle, un moyen de lutter, et parfois,
tout simplement, un moyen de bouffer.

Le TDS est un boulot et, comme tout boulot, il y a des disparités dans
les conditions de travail. Pour réfléchir à la prostitution et à ses
implications en matière de genre, c'est plus facile de réduire ce métier
à une seule réalité, véhiculée par l'imaginaire obscur qui hante la
figure de la pute. Mais il est plus juste de maintenir la complexité que
l'on peut observer sur le terrain, sur *les* terrains. Et puisqu'il y a,
effectivement, des réalités plus dures pour certainexs TDS que pour
d'autres, créons des syndicats, améliorons leurs conditions de travail,
faisons des manifs et crions fort, sans jamais oublier que...

...le TDS peut être libertaire. On a touxtes le droit au plaisir sexuel
sans amour.

...le TDS peut être subversif, parce que le sexe est un terrain de lutte
sur lequel on peut déstabiliser le patriarcat.

...le TDS, donc la marchandisation du sexe, peut être un retournement du
rapport social de production.

...le TDS est aussi un travail social.

...le TDS, c'est un peu du théâtre dans lequel tu représentes une figure
féminine type sans obligation de l'être véritablement.

...le TDS vend le fantasme d'une domination masculine, mais il ne la
reproduit pas nécessairement, ce n'est qu'un échange, une transaction.

Pour des rencontres, vous pouvez m'écrire sur telegram... et c'est 600 CHF
la soirée 😉.

[^6]: *Nous sommes touxtes des putes* \[n^o^ 42\] aborde aussi ce préjugé de l'argent "facile" lié au travail du sexe.

[^7]: Pour d'autres paroles abordant la question du féminisme <a href="/glossaire/#abolitionnisme" class="glossary-link" target="_blank" rel="noopener" data-no-swup>abolitionniste°</a>, lire *Pas de féminisme sans les putes !* \[n^o^ 37\] et *Nous sommes touxtes des putes* \[n^o^ 42\].
