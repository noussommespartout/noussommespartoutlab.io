---
author: "Anonyme"
title: "Solidarité antispéciste"
subtitle: "Discussion au sein d'un sanctuaire"
info: "Transcription d'un entretien"
datepub: "mars 2020"
categories: ["antispécisme"]
tags: ["amour, love, amoureuxse", "eau", "fête", "peur", "poule", "silence", "solution", "violence"]
langtxt: ["fr"]
zone: "Suisse romande"
num: "35"
quote: "devenir antispéciste, c'est juste élargir son cercle de compassion"
---

--- On commence par les refuges, c'est ça ?

--- Ok, je commence. Donc un sanctuaire ou un refuge, c'est un lieu dans
lequel on peut accueillir des animaux non humaines qui ont besoin d'un
espace pour vivre. Que ce soit parce qu'iels vont être envoyées à
l'abattoir, qu'iels sont maltraitées[^96] ou qu'iels sont dans une
situation où les gens ne peuvent plus s'occuper d'elleux.

--- Les animaux non humaines qui sont dépendantes de nous n'ont pas le
choix : iels sont obligées de se reposer sur nous.

--- Pourquoi plus sanctuaire que refuge ?

--- Un refuge c'est un endroit où les animaux vont aller pendant un
certain temps; l'objectif c'est de les replacer dans d'autres familles
d'accueil par la suite. Alors que dans un sanctuaire, le but c'est
vraiment qu'iels aient le plus longtemps possible un espace, où iels
pourront terminer leur vie de façon digne. Voilà un petit peu les
grosses différences entre un refuge et un sanctuaire.

--- Refuge, c'est un peu SPA, et sanctuaire, c'est plus politisé comme
terme.

--- Mais iels viennent d'où les animaux ?

--- On peut à la fois avoir des animaux non humaines qui ont été sauvées
de manière illégale, simplement par des personnes qui sont allées les chercher
là où iels étaient en détresse, ou bien de manière légale, par des
saisies ordonnées par les autorités d'État, mais ça reste rare.

--- En Suisse, et partout ailleurs, les lois sont très spécistes et on a
plus tendance à vouloir faire disparaître le problème qu'aider les
individus qui ont besoin d'aide. Par exemple si quelqu'unex s'occupe
extrêmement mal d'animaux non humaines (il faut quand même s'occuper
extrêmement mal d'elleux pour avoir des problèmes légaux), donc qu'iel
ne leur donnerait ni à boire ni à manger et qu'iel aurait eu plusieurs
avertissements, le vétérinaire cantonal pourrait lui donner une
interdiction d'en posséder et là, iel aurait un mois pour se débarrasser
de ces individus.

--- Se débarrasser des animaux non humaines inclut l'abattoir, ou les
ventes, ou quoi que ce soit d'autre. Du moment que le problème n'existe
plus, c'est considéré réglé pour l'État, mais à aucun moment on se pose
la question de comment on peut faire pour aider ces individus qui sont
en détresse.

--- Parmi ces animaux, il y en a qui ne sont pas forcément tout de suite
destinées à être tuées mais à être exploitées. Je pense notamment aux
poules pondeuses dont le temps de vie est assez réduit et qui sont dans
des états vraiment très déplorables : plus de plumes, une fatigue
extrême, plus aucune énergie. Dès le moment où elles produisent moins
d'œufs qu'au départ, la solution pour les éleveureuxses, c'est de les
tuer. Et ça, c'est une des raisons pour lesquelles des personnes iraient
sauver des individus d'un sort effroyable comme ça.

--- Peut-on parler de la façon dont on s'occupe de ces individus ?

--- Les refuges ou les sanctuaires ont besoin de pas mal de ressources
pour s'occuper de toustes ces animaux, que ça soit pour couvrir les
frais vétérinaires ou simplement pour les nourrir. Comme ces animaux
étaient en situation de détresse, beaucoup vont avoir des problèmes de
santé qui vont durer longtemps et vont être récurrents, que ce soit
parce qu'iels ont été mutilées lors de l'élevage, ou qu'iels ont été
exploitées pendant des années, ou encore qu'iels ont été séparées de
leurs parents extrêmement tôt et donc iels n'ont pas développé un
système immunitaire assez fort.

--- Les animaux non humaines qui sortent d'élevages ne sont pas en bonne
santé : les 18 cabris qui avaient été sauvées en 2018 de l'abattoir de
Rolle avaient toustes des pneumonies et des vers.

--- Les cochons qui sortent d'élevage ont la queue coupée ; ça veut
dire que leur colonne vertébrale est coupée et donc iels développent des
problèmes de dos en vieillissant.

--- Je veux juste rajouter que ça se fait à vif, sans anesthésie.

--- Ce sont des problèmes dont l'industrie spéciste n'a rien à faire
puisqu'ils les tuent très très vite...

--- Tout cela donne des problèmes de santé assez conséquents pour les
rares individus qui arrivent à sortir vivants de ces institutions.

--- Tout ça, ça fait des charges en plus pour les sanctuaires et les
refuges qui arrivent à accueillir ces individus, à cause des conditions
vraiment horribles d'exploitation qu'iels ont subies durant leurs vies.

--- Tout ça nécessite donc beaucoup de ressources financières, mais aussi
beaucoup de temps et d'énergie, non ?

--- C'est un engagement sur le long terme, parce qu'on a des individus
qui vont vivre une petite dizaine d'années, comme les poules, et
d'autres qui vont vivre très longtemps, comme les équidés par exemple.
Alors que, de manière générale, dans les élevages, ces mêmes animaux ne
vont pas vivre aussi longtemps. Une poule va vivre deux ou trois ans, peut-être quatre ans...

--- Si elle est pondeuse.

--- Si elle est pondeuse parce que sinon...

--- 30 jours.

--- Voilà...

--- Il manque beaucoup de recherches et de connaissances là-dessus. Un
coq ou une poule ne vont jamais se faire soigner parce que tout le monde
s'en fiche, leur vie ne vaut rien du tout. Iels n'ont pas de valeur
marchande donc il n'y a pas de remède quoi.

--- J'ai l'impression que le but des sanctuaires et des refuges, c'est de
réinventer notre rapport aux autres animaux. Et c'est très
difficile à faire parce que dans un refuge ou dans un sanctuaire, eh
bien le spécisme est toujours là. C'est naïf de penser que parce que
l'animal est dans un sanctuaire, iel est éloignée du spécisme. Ça veut dire qu'il faut se remettre en
question tous les jours, essayer de se
rendre compte qu'il y a des rapports de domination, comme dans toutes
les autres luttes. Que les personnes dominantes, elles seront toujours
dominantes. Il faut faire d'énormes efforts pour que la
domination soit la plus faible possible.

--- Je pense qu'une des premières leçons que tu peux tirer dans un
sanctuaire ou un refuge, c'est exactement ça. Par exemple, quand tu
interagis avec des cochons, tu te rends compte qu'iels sont très
intelligentes. Iels ne sont pas du tout les êtres répugnants qu'on nous
a toujours dépeints. Ce sont des êtres dotés de personnalités et quand
on les voit juste comme des chiffres dans des barquettes, ben on a
l'impression que ce sont des objets, tellement c'est déshumanisé.

--- Peut-être qu'on devrait expliquer pourquoi on dit "personne" pour
parler des animaux ?

--- Il me semble qu'une personne, c'est une individu doté de
personnalité. Quand on passe du temps avec les animaux non humaines, on
se rend bien compte qu'il y en a qui ont un caractère assez sévère,
d'autres qui sont très curieuses, d'autres qui aiment bien jouer,
ou d'autres encore qui ont besoin de calme. Donc, ça en fait des
personnes à part entière. Et ça, c'est quelque chose qui souvent dérange
les personnes humaines. Parce qu'elles ont l'impression de perdre un
privilège. (*Rires.*) Et nous, ça nous semble très important de rappeler...
que nous ne sommes pas les seulexs à avoir des personnalités différentes.

(*Silence.*)

--- Peut-être vous pourriez expliquer quelles sont les tâches à faire
dans un sanctuaire?

--- Leur donner de l'eau, contrôler leur santé, les amener chez le véto
quand il faut...

--- Nettoyer leurs cacas[^97]. Ce que je veux dire là-dessus, c'est que
s'il faut nettoyer leurs cacas, c'est à cause du manque d'espace que
nous leur donnons. Parce que dans la nature, iels parcourent des
kilomètres, donc deux, trois cacas de chèvre c'est rien du tout.

--- C'est même bien.

--- Et être dans un petit espace où iels piétinent tout le temps, ça peut
aussi engendrer certaines maladies.

--- En fait, on fait des choses dont iels ont besoin mais dans d'autres
circonstances, iels pourraient s'en sortir sans nous.

--- Ce sont des besoins générés par l'humain quoi...

--- Je voudrais rajouter que les animaux non humaines ont aussi
besoin de temps et d'affection.

--- Iels en ont besoin, mais iels ont surtout besoin d'avoir du temps
entre elleux.

--- On en revient de nouveau à l'espace. Ce qui est sûr, c'est qu'un
animal qui a un petit espace, iel a besoin d'interaction avec les
personnes qui s'en occupent, alors que si t'as un immense terrain avec
plein de ressources différentes, l'animal non humaine pourra devenir
plus ou moins autonome.

--- Je voulais vous demander si pour vous l'antispécisme a du sens sans
l'anticapitalisme. Et quelles sont les connexions entre
l'anticapitalisme et l'antispécisme ? Peut-être entre féminisme et
antispécisme[^98] ?

--- Je ne pense pas que ce soit possible d’envisager un monde ou une société qui soit antispéciste, sans qu’elle soit libertaire, anticapitaliste et féministe. La lutte écologiste a peu de sens si elle est uniquement pour les humains.

--- Tout ça requiert un changement sociétal assez énorme.

--- D'autres luttes doivent s'inscrire dans l'antispécisme pour faire
sens.

--- J'ai aussi l'impression que c'est toujours une question de privilèges
et de domination. On ne peut abolir l'un sans l'autre. Soit on
change totalement nos rapports à tous les individus, soit ça
continuera tout le temps à merder quelque part.

--- Y'a une phrase qui depuis tout à l'heure me trotte dans la tête et j'ai
juste envie de la dire : ouvrir son cœur aux animaux
non humaines, devenir antispéciste, c'est juste élargir son cercle de
compassion. Tu peux pas juste ouvrir ton cœur aux animaux et dire
"c'est bon, j'ai fait ma part des choses". Non, quand on ouvre son cœur,
on l'ouvre bien au-delà, on essaie de l'ouvrir à toutes ces personnes
qui sont oppressées et qui ont toujours été écrasées par la façon
dont on vit.

--- Dans tous les repas traditionnels, y'a des animaux morts, ça
renforce vraiment le spécisme tout le temps, partout.

--- Au sein d'une famille, s'opposer au repas de Noël, parce qu'il comprend des cadavres, de la chair d'animaux non humaines pour festoyer une fête qui est censée prôner l'amour, c'est vu comme quelque chose de vraiment très agressif.
C'est vu par bon nombre de familles comme une violence,
je sais que pour beaucoup de personnes, rien que cette petite étape, ça
représente quelque chose d'énorme.

--- Qu'est ce qu'on peut faire pour être militanxte antispéciste?

--- J'aime pas trop le terme "militanxte antispéciste", ça me
dérange un peu, j'ai l'impression que c'est un mot un peu identitaire...
Les hommes <a href="/glossaire/#hommes-cis--mecs-cis--cisgenres" class="glossary-link" target="_blank" rel="noopener" data-no-swup>cis°</a> ne
peuvent pas être féministes, mais ils peuvent être
alliés. Nous qui ne subissons pas l'oppression, c'est un
peu délicat de s'identifier comme antispécistes. On essaie de faire ce
qu'on peut et, enfin, je préfère parler du spécisme et expliquer à quel
point ça ne devrait pas exister, à quel point il faut se battre contre,
mais j'ai un peu peur de ce mot antispéciste. J'ai l'impression qu'on a
tendance à moins se remettre en question dans nos rapports quotidiens
avec les autres espèces, à trop se reposer sur nos acquis, à
juste se dire qu'on est antispéciste. Avant, il y avait le mot
*vegan*, mais le véganisme était concentré sur NOS habitudes
alimentaires, alors pour pallier ça, on a commencé à utiliser le mot
antispéciste. Mais en fait ça reste la même chose, c'est quelque chose
d'identitaire, ce sont nos habitudes, ce sont nos trucs, et on parle
très peu des personnes qui sont concernées par le spécisme.
C'est-à-dire, pas nous en fait. Ça me paraît beaucoup plus simple de
parler de spécisme et de se concentrer sur ce que subissent vraiment les
victimes.

[^96]: À ce propos, lire aussi le récit d'un sauvetage dans un élevage intensif de lapins dans *Bidule, Truc et Machin à la ferme* \[n^o^ 29\].

[^97]: Sur une utilisation possible de cette ressource, voir *Le compost généralisé* \[n^o^ 41\].

[^98]: Sur ce sujet, lire un dialogue chanté écoféministe dans *Cette colère immense, collective, transgénérationnelle, internationale* \[n^o^ 30\].

