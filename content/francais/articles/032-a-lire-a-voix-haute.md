---
author: "Anonyme"
title: "À lire à voix haute"
subtitle: "Gymnastique pour apprendre à parler français non binaire"
info: "Texte rédigé pour le recueil"
datepub: "octobre 2020"
categories: ["DIY", "transpédégouines, queer"]
tags: ["blague", "colère", "corps", "cuisiner, cuisine", "fluide, fluidité", "foot", "fête", "gueule", "nuit, obscurité", "peur", "pizza", "pouvoir", "sexe", "soleil", "thé, tisane", "violence"]
langtxt: ["fr"]
zone: "Suisse romande"
num: "32"
quote: "iel comme ciel, comme miel"
---

Iel[^84].

Pas "il", pas "elle", pas "elle/il", pas "il-elle", juste iel.

Iel comme ciel, comme miel.

Iel comme jupe et barbe, pantalon et maquillage, jupe et maquillage,
pantalon et barbe. Iel comme je fais comme je veux, m'en tape de vos
règles étriquées.

Iel comme iel est belle, iel est beau, iel est beaule, iel est belleau.
Y'a pas de règle fixe d'accords avec iel, à moins qu'iel précise quels
accords iel préfère. Sinon, invente et amuse-toi, crée ton propre
cocktail, trouve les accords qui ont le plus de sens pour toi et qui
sont propres à la personne dont tu parles. Iel, c'est aussi
une façon d'envoyer chier la rigidité archaïque de l'institution du
langage, une façon de se réapproprier les mots et notre capacité à
modeler notre monde. Et si tu as peur de lae vexer, demande-lui ce
qu'iel en pense.

Iel pense, iel danse, iel mange, iel dort, iel pète, iel m'a dit
qu'iel avait trop bien dormi cette nuit. Iel rêve. Iel imagine. Iel
crée. Iel construit. Iel avait faim, alors iel est allé·e se chercher
une pizza. Iel a joué au foot hier soir. Iel a regardé la télé en buvant
des bières avec ses amixes (invente ta règle de prononciation), puis
iels se sont fait mutuellement les ongles des pieds. Iels sentent bon
des fois, et des fois iels puent. Iels m'ont appris à réparer mon vélo.
Iels n'avaient pas vu l'heure, c'est pour ça qu'iels sont en retard.
Iels sont partixes au skatepark. Iels sont alléxes cueillir des
champignons. Iels ne savaient pas comment coudre des serviettes
hygiéniques lavables, alors iel a organisé un atelier chez elleux pour
leur montrer comment faire. Iels étaient trop content·es.

Iels aiment. Iels n'aiment pas. Iel aime la confiture de framboise. Iel
n'aime pas les piqûres de moustiques. Iel aime les orages. Iel n'aime
pas l'odeur des voitures. Iel aime les gros chiens. Iel n'aime pas la
musique triste. Iel aime la tisane à la sauge. Iel n'aime pas parler au
téléphone. Iel aime les plages au soleil. Iel n'aime pas les files
d'attente. Iel aime ses tatouages. Iel n'aime pas avoir la gueule de
bois. Iel aime aller au cinéma. Iel n'aime pas quand iel est dans le
bus, qu'iel lève son bras pour appuyer sur le bouton, et qu'une personne
qu'iel ne connaît pas lui dit qu'iel devrait se raser les aisselles.
(Iel se rase les aisselles s'iel le veut.) Iel aime cuisiner avec ses
ami·es. Iel n'aime pas qu'on lui lance des insultes homophobes quand iel
sort avec du maquillage. Iel aime quand tu dis "iel". Iel n'aime pas
le vin chaud. Iel aime qu'on lui demande avant de lae toucher.

Iel a un corps. Son corps est en souffrance des fois et des fois son
corps est rempli de sensations agréables. Iel aimerait parfois
s'arracher une partie du corps, tellement on lui impose de se comporter
d'une certaine manière à cause de cette partie du corps. Iel se trouve
trop gros·se. Mais iel sait qu'iel ne penserait pas ça s'iel n'avait pas
grandi dans une société grossophobe. Iel travaille là-dessus. Quand iel
se caresse, iel aime son corps. Iel se masturbe tous les jours pour
oublier. Iel ne se masturbe jamais parce qu'iel n'a pas de désir et
c'est ok. Iel m'a demandé s'iel pouvait me caresser. J'avais très envie
d'iel. Iel m'a demandé comment j'aimais faire le sexe. Iel m'a
écouté. Iel et moi, nous nous sommes fait beaucoup de câlins. Ça fait du
bien. Iel est homo. Iel est hétéro. Iel n'est ni l'un ni l'autre. Iel
s'en fout. Iel ne sait pas et c'est ok.

Iel est en colère parce qu'iel sait que son genre existe[^85] et iel
aimerait qu'il ne soit ni une blague, ni un tabou, ni une soi-disant
maladie, ni un soi-disant caprice. Iel lutte et des fois c'est trop dur,
alors iel se laisse <a href="/glossaire/#mégenrer" class="glossary-link" target="_blank" rel="noopener" data-no-swup>mégenrer°</a>, c'est-à-dire qu'iel laisse les autres
dire autre chose qu'"iel" pour parler d'iel à la troisième personne.
Iel souffre d'être mégenré·e à longueur de journée. Mais iel sait que
son pronom est un néologisme et que le simple fait de demander de
l'utiliser l'obligerait la plupart du temps à se lancer dans un débat
concernant l'existence de son identité. Iel fait un effort pour rendre
notre langue moins genrée, moins sexiste et iel a du plaisir à faire cet
effort car il lae rend plus libre, mais iel aimerait bien partager cet
effort. Iel ne t'en voudra pas si tu te trompes. Iel se trompe aussi
souvent. Iel sait que c'est un apprentissage. Iel rêve du moment où
tousxtes les genses seront à l'aise de poser des questions. Iel veut en
parler. Iel aimerait parler d'autre chose aussi des fois et aimerait que
tu te renseignes par toi-même. J'espère qu'iel te le dira s'iel n'a
pas envie de répondre à tes questions pour l'instant. Mais iel ne t'en
voudra jamais d'avoir demandé. Iel t'en voudra par contre si tu lae
mégenres volontairement, parce que ça veut dire que tu nies son
identité. Iel n'a pas la tâche facile, parce que son identité est
invisible dans notre culture. Iel espère que ça va changer. Iels
espèrent touxtes ça. Et iels espèrent que tu seras leur allié·e.

Iel change aussi. Iel est un être vivant et se transforme comme tous les
êtres vivants. Iel devient il parfois. Iel devient elle aussi parfois.
Iel peut même devenir elle, puis il et elle, puis iel, puis il de
nouveau, puis elle encore, puis iel et elle... Iel peut être autre chose
chaque jour et c'est ok. Parce qu'on doit essayer plusieurs choses
avant d'être sûrexs, et que même quand on est sûrexs, des fois ça
change. Iel est parfois sûr·e que ça change régulièrement. Iel
revendique le droit au changement.

Iel est boulangère et danseur, guérisseur et musicienne, informaticienne
et cordonnier. Iel est multiple, iel est mosaïque. Iel refuse d'être
homogène.

Iel a peur, iel doit se cacher pour survivre la plupart du temps, même
si se cacher produit aussi de la souffrance. Iel est gêné·e par sa
propre identité des fois, parce qu'iel a malgré iel incorporé dans sa
propre perception la domination des personnes qui ne respectent pas la
loi imaginaire mais bien réelle du genre. Iel est colonisé·e à
l'intérieur. Toi aussi. Iel et toi êtes des chantiers multicolores.

Iel s'appelle Mélusine, Mimi, Naé, Charli.e, Fauve, Bread, David, James,
Cachou, Emi, Fjord, Poky, Keelan, Stéphane, Pierre, Caillou, Fleuve,
Montagne... ; iel utilise son nom de naissance ou pas et ça ne regarde
personne d'autre qu'iel.

Iel est triste que ce soit aussi compliqué de changer de prénom, même
officieusement, parce qu'iel trouve que l'exploration de l'identité par
le prénom est une expérience très enrichissante. Iel vit de la
transphobie quand ses parents, sa famille, ses amixes refusent de
l'appeler par son prénom choisi. Iel a choisi un autre prénom, parce que
son prénom de naissance l'enferme dans un rôle de genre qui ne lui
convient pas, parce qu'iel est en questionnement, parce qu'iel n'aime
pas son prénom de naissance, parce qu'iel a ses raisons, et c'est très
dur parfois pour iel de dire qu'iel a changé de prénom. Iel aimerait que
ça puisse être une fête.

Iel fait la fête quand iel est joyeuxeuse. Iel est amoureureuse des
possibilités infinies qu'offre la vie. Iel aimerait pouvoir exploiter
ces possibilités sans qu'on l'en empêche pour des raisons qu'iel trouve
injustes.

Iel n'est pas une femme, iel n'est pas un homme. Iel refuse de jouer à
ce jeu, à moins de pouvoir inventer de nouvelles règles. Iel sait que la
perception occidentale du genre n'est pas une vérité fondamentale mais
culturelle, qui a participé à la colonisation de peuples qui conçoivent
le genre autrement, de telle sorte que ces peuples sont maintenant si
minoritaires --- s'ils ne sont pas assimilés ou exterminés --- qu'il est
très difficile de retrouver des traces de ces autres narrations du
genre. Iel sait que la perception binaire du genre s'est imposée de
manière extrêmement violente. Iel veut décoloniser le genre. Iel est
<a href="/glossaire/#non-binarité--personnes-non-binaires" class="glossary-link" target="_blank" rel="noopener" data-no-swup>non binaire°</a>, agenre, postgenre, *genderqueer*, *genderfluid*, bigenre...
Iel est <a href="/glossaire/#trans--transgenre-transidentités" class="glossary-link" target="_blank" rel="noopener" data-no-swup>trans°</a> ou pas, ça dépend. Iel revendique le droit à la fluidité,
au nomadisme, au rejet des injonctions du genre, à l'appartenance à
plusieurs genres, à la volonté de créer un monde sans hétéronormes, sans
sexisme, sans transphobie, sans violence du genre. Iel n'est qu'un germe
parmi d'autres.

Iel, c'est un pronom personnel. Peut-être qu'après, iel pourra aussi
changer les pronoms impersonnels. On dira "iel fait belleau
aujourd'hui !" ou "iel est trois heures et demie". En attendant, iel,
c'est notre pronom.

[^84]: Pour un point sur la grammaire inclusive, voir *Pronoms et langage* \[n^o^ 36\].

[^85]: On pourra lire une autre colère non binaire dans *Un terreau pour les fleurs de la révolte* \[n^o^ 52\].

