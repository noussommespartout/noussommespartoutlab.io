---
author: "Ondine"
title: "ZAB 2028"
subtitle: "Dystopie basée sur l'occupation actuelle de l'ancienne usine Reuge, 1450 Sainte-Croix"
info: "Texte rédigé pour le recueil"
datepub: "avril 2020"
categories: ["autogestion, expérimentations collectives", "squat, occupations, logement", "écologie"]
tags: ["anniversaire", "arbre", "banque", "bière", "café", "eau", "effondrement", "erreur", "fête", "graines", "huile", "jardin", "pouvoir", "procès", "rue"]
langtxt: ["fr"]
zone: "Suisse romande"
num: "43"
quote: "un arbre perce la terre et le ciel simultanément"
---

*--- ...le prototype de microturbine hydro-électrique est OK. Y'a plus qu'à
en fixer plein dans les tuyaux de récupération d'eau pour l'alimentation
des batteries... vu qu'on passe de canicule à mousson, ça va améliorer
notre autonomie énergétique en temps de pluie.*

*--- T'as vu la jungle de tuyaux ? T'es frappadingue Gaston ! T'imagines
le merdier pour y plugger des centaines de tes bidules ? Sans parler de
les fabriquer.*

*--- Fais pas ta débile Krys. Ta conseillère ORP t'a trouvé un autre
programme d'occupation ?*

*--- T'es con !*

{{%ligneblanche%}}

Ces deux-là sont autant inséparables qu'ils s'insupportent cordialement.
Mécanicien de précision et ex-étudiante en ingénierie énergétique, l'un
aligne les idées de fou, l'autre est connue pour sa pugnacité. Le
bâtiment est quasi autonome énergétiquement, particulièrement grâce à
elleux. Pour nos sobres besoins bien entendu.

Comme chaque année en cette période, la BAZ grouille comme une ruche en
pleine miellée. On fête la BAZ, la ZAB (zone à bâtir). Quand on y
promène le regard, on a du mal à imaginer le vieux cargo fantomatique
auquel ressemblait cette vaste friche industrielle. Les toits ont été
végétalisés, le vert grimpe aux murs et dégouline des hauteurs. La vigne
vierge et le lierre abritent des colonies d'insectes et d'oiseaux. Dans
quelques semaines, ce sera une explosion de couleurs et de gazouillis.
Maka, Rob et Mila --- respectivement graffeur ayant fui Manchester avant
que l'Angleterre ne ferme définitivement ses frontières, fan de street
art exilée d'Israël et acrobate réfugiée de la guerre civile aux
États-Unis --- ont recouvert la façade nord de glyphes peints avec de la
mousse de forêt. La nature parle sur nos murs : "*Un arbre perce la
terre et le ciel simultanément*", "*Qu'importent leurs verrous, nous
sommes passe-murailles.*", "*Aujourd'hui sera passé avant que tu ne
résolves demain.*"

La "grande mascarade" a duré plusieurs années. Les États capitalistes
ont tenté de gérer la crise sanitaire tout en relançant au plus vite
l'économie. Ça n'a pas fonctionné. Les inégalités sociales ont atteint
leur paroxysme. La bourse a collapsé. Les conflits économiques entre les
grandes puissances ont dégénéré en une espèce de seconde guerre froide.
Les vagues de pandémies se sont ajoutées aux sécheresses, incendies,
inondations et tremblements de terre : conséquences mortifères de deux
siècles de démence collective. On a rapidement arrêté de compter les
mortexs. Les flux migratoires ont explosé et si certaines frontières se
sont fermées définitivement, d'autres ont simplement disparu, désertées
par des militaires refusant de tirer à vue sur ces marées humaines
désespérées. Les actes de bravoure et de solidarité se sont amplifiés
proportionnellement aux atrocités.

Des ZAD[^119], ZAB, ZAG (À Défendre, À Bâtir, Autogouvernées) ont
émergé un peu partout, des archipels solidaires connectés en plusieurs
réseaux intercontinentaux. Elles se sont mises à raconter au monde une
histoire qui dément les plus grandes *fake news* de tous les temps :
"*On ne peut pas vivre sans argent*", "*la nature humaine est
malfaisante et égoïste*".

Les meunières de la plaine sont arrivées à la BAZ ce matin, pour livrer
la farine. Elles échangent des semences avec des collectifs français de
paysannexs-boulangèrexs arrivés hier, tout en dégustant le dernier
brassin fait avec l'orge malté qu'elles fournissent à la brasserie des
Trois Dames. Beaucoup partagent une affection particulière pour la
bière, les brasseureuxses ne manquent jamais de rien et on ne manque
jamais de bière, sauf quand il y a pénurie d'eau.

Les compagnonnexs grecquexs ont réussi à venir avec une cargaison
d'huile d'olive et de sel. Iels repartiront dans quelques semaines avec
tout ce qui leur manque et que nous pouvons leur offrir. L'économie au
sein des archipels fonctionne sur un principe basique : "*De chacunex
selon ses moyens, pour chacunex selon ses besoins.*"

Et ça fonctionne.

Certainexs vivaient déjà ainsi longtemps avant l'effondrement.
Aujourd'hui l'argent a pratiquement disparu et plus personne ne sait
vraiment ce qu'il vaut.

Vlam et la Daronne multiplient depuis plusieurs jours le levain-chef
pour faire les plus grosses fournées de l'année. Demain, c'est 150 kg de
pain qui sortiront du four à bois et ainsi tous les deux jours jusqu'à
ce que ça se calme. Bien que le grand rassemblement soit une grosse
bastringue, tout se fait sans organigramme, cahier des tâches ou
responsables. C'est ça l'autogestion : pouvoir se fier à la
complémentarité des âges, des savoir-faire et des différentes
temporalités des personnes. Pour les tâches ingrates, un tournus se met
en place naturellement. On a toutes et tous conscience que l'équilibre
relationnel de la collectivité en dépend.

Ce huitième anniversaire marque la fin d'un cycle. L'assainissement des
6000 m^2^ du bâtiment et des 8000 m^2^ de terrain touche à sa fin. En 2020,
aucune technologie n'était capable de décontaminer ce genre de site. Sur
ce projet pilote, des étudiantes et étudiants en ingénierie
environnementale, biologie, chimie, sciences sociales, deux professeurs
de l'EPFL[^120] et plusieurs autres scientifiques ont partagé leurs
connaissances avec un maraîcher, un magnétiseur et une vigneronne en
biodynamie. La municipalité a participé en prêtant des machines pour le
gros œuvre et l'entreprise responsable de la contamination a
volontairement déboursé les fonds nécessaires. Si l'ex-futur
propriétaire (victime des faillites en cascade du krach de 2021) avait
gagné son procès contre l'entreprise, elle aurait dû débourser des
centaines de milliers de francs pour n'assainir que superficiellement le
site. En réalité, l'extraction des solvants les plus accessibles a coûté
quelques dizaines de milliers de francs et les autres polluants
infiltrés en profondeur ont été résorbés par une méthode biotech
développée ici même. Comme d'habitude, la complicité, l'inventivité, le
travail et le temps ont remplacé les millions.

En me rendant sur le toit pour vérifier les ruches, j'en profite pour
zigzaguer dans les étages. C'est comme faire un grand voyage en une
poignée de minutes. Quelques ados et adultes apprennent le bouturage
dans le jardin d'hiver. La salle d'arts martiaux est déserte. Au Barbaz,
une paire de mini punkettes --- désireuses de fabriquer un filet géant
en crochet --- envahissent l'espace d'une ancienne qui tricote en buvant
de l'absinthe. Je lui lance : "C'est pas un peu tôt Mamie ? Et tu crois
pas qu'elles sont un peu jeunes pour toi ?". Elle m'envoie cordialement
balader, avant d'introduire discrètement une de ses aiguilles dans
l'interstice du jeans d'une des postadolescentes qui en grimpe au
rideau, médusée.

À l'imprimerie, des immenses tissus passent sous les cadres de
sérigraphie pour faire flotter au vent les couleurs et messages des
archipels. La crèche, comme à son habitude, grouille sous le regard
bienveillant de la Louve. Le contraste est toujours frappant avec le
calme de la bibliothèque où quelques gamins écoutent sagement des
histoires. Ashem, cinq ans, demande à Edaline :

"C'est vrai qu'avant on pouvait pas manzer et faire dodo au chaud si on
avait pas de l'arzent ?"

Je passe par l'atelier de poterie --- une multitude de tasses et bols sont
en train de sécher --- et grimpe sur le toit et sa vue vertigineuse. Les
abeilles commencent à sortir de leur hibernation.

Les collectivités autonomes du Chiapas au Mexique fournissent le café à
plusieurs archipels. Soucieuse de ne pas louper LE café de la journée,
je ne passe pas par la serrurerie, la forge, les ateliers d'arts
plastiques, d'arts visuels, la filature, le labo d'ingénierie, la salle
de classe, la mercerie, le labo photo, les trois espaces dédiés aux
festivités et conférences, les deux espaces polyvalents, les deux salles
de répétition pour les musiques bruyantes, la banque de graines, la
ludothèque, les trois cuisines, les quatre dortoirs et multiples
chambres d'hôtes, la savonnerie-droguerie, l'atelier vélos et la
boutique qui cumule dans ses étagères tout ce qui est produit, amené et
récupéré. Sans parler des cabanes, dômes et autres autoconstructions sur
le terrain.

Pilou, Chauve-Souris, Baloo, Mouman et Vieux Schtroumpf papotent au
soleil sur la terrasse n^o^ 1, entouréexs de quelques "touristes". Chaque
année il leur est réclamé l'histoire de la prise de la BAZ pendant
l'hiver 2019-2020.

--- Pourquoi le 3 février, répond la Chauve-Souris ? Parce que, bien
qu'iels soient arrivés le 11 novembre 2019, c'est ce jour-là que la
Daronne et Pilou se faisaient foutre dehors pour la deuxième fois et que
moi, Arthur et plein d'autres militanxtes sont immédiatement venuexs
réoccuper. Cette date nous rappelle l'importance de la complicité des
luttes, quelle que soit leur forme.

--- Et après vous avez pu rester à la BAZ ?

--- Il y a eu la première pandémie et le confinement. Un bref répit...
c'était un des seuls espaces de création encore ouverts ! T'imagines ?
Mais les gens se méfiaient encore du projet, il n'y avait pas autant de
monde qu'aujourd'hui. Ensuite, ça a recommencé : on nous mettait dehors
et on revenait, plus nombreuxses et plus déterminéexs. Les flics ne
savaient jamais combien on était...

--- Et ça nous rendait dingues, coupe le vieux Schtroumpf. On a essayé
tous les biais possibles pour qu'ils crachent le morceau.

--- T'es méchant ! T'étais d'la police! Pourquoi tu voulais casser la BAZ ?

--- On pensait défendre de justes valeurs. Mais quand les vrais problèmes
sont arrivés, tout ça allait à l'encontre du bon sens. S'en prendre aux
rares personnes travaillant depuis longtemps à trouver des solutions
pour notre futur était une grave erreur.

--- Et t'as fait quoi, lui demande Zita ?

--- J'ai démissionné. Depuis j'offre mes talents de médiateur à la
collectivité. C'était une des bonnes choses qu'on nous apprenait chez
les flics. Beaucoup de mes collègues ont fait comme moi. On continue
d'aider et protéger les gens mais sans être en même temps le bras armé
de ceux qui détruisent le vivant pour leur propre profit.

La fillette lui répond en chantant "La rue des lilas" : "...car la
guerre est un massacre de gens qui ne se connaissent pas, au profit de
gens qui toujours se connaissent, mais qui ne se massacrent pas..."

[^119]: *Là-haut sur la Colline* \[n^o^ 51\] raconte une autre ZAD.

[^120]: École polytechnique fédérale de Lausanne.
