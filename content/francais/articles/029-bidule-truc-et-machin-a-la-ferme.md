---
author: "Truc"
title: "Bidule, Truc et Machin à la ferme"
subtitle: "Récit d'une action antispéciste dans les hautes herbes"
info: "Texte rédigé pour le recueil"
datepub: "novembre 2020"
categories: ["antispécisme", "sabotage, action directe"]
tags: ["coller", "corps", "courage", "eau", "force", "internet, web", "merde", "nuit, obscurité", "parole", "pouvoir", "silence", "téléphone", "voiture, bagnole"]
langtxt: ["de", "fr"]
zone: "Suisse romande"
num: "29"
quote: "dans l'ombre de tous les abattoirs"
---

Dans mes écouteurs qui grésillent, j'entends Machin qui râle. Les lapins
sont très lourds et surtout iels[^75] veulent pas tellement se laisser
emmener par les gentillexs *vegans* que nous sommes. Machin essaie de
leur parler, doucement, mais ça a pas l'air de les convaincre. Il faut
en choisir trois ou quatre sur celleux qui sont entassées là, et je suis
contente de pas devoir m'y coller. Machin râle de plus belle.

Moi, je suis planquée dans les hautes herbes, j'ai jamais fait ça, j'ai
les yeux qui louchent à force de scruter l'obscurité pour vérifier que
personne ne débarque. Et j'ai très besoin de pisser. On a oublié le
talkie-walkie, donc je fais le guet avec un téléphone sécurisé. C'est
moins bien.

J'entends Bidule qui respire et qui arrête pas de dire à quel point les
clapiers sont dégueulasses et petits. Je lae crois sur parole et à
l'odeur. L'air empeste la merde, la chair en décomposition et le foin
mouillé. Y'a des insectes partout.

J'entends jurer. Machin en a perdu un qu'il faut essayer de récupérer,
dans le noir, sans autre lampe qu'une infrarouge. On crève de chaud sous
nos cagoules, nos vestes et nos capuches en plein mois de juin. Bidule
souffle comme un bœuf dans le micro des écouteurs. Il y a un silence, un
silence qui me laisse le temps de réaliser qu'on est trois peléexs au
fin fond de la campagne en train de récupérer quatre pauvres lapins chez
un type pas spécialement aimable, ce qui me fout un peu les boules.
Puis, Bidule me demande si la voie est libre. Je louche encore plus pour
être sûre qu'il y a pas un éleveur en embuscade. J'en vois pas. Machin
arrive en trottinant tant bien que mal avec son sac plein de lapins.
Bidule suit de près avec les lampes et le matos. On rejoint la voiture,
parquée à ce qui me semble des kilomètres de distance et on quitte le
site, les phares éteints. Une fois que mon cœur daigne quitter ma bouche
et que je recommence à respirer normalement, je jette un œil à ces
fameux lapins. Leur corps est immense, si gros qu'on dirait des chiens.

C'est quelque chose qui me surprend à chaque fois qu'on récupère un
animal, ou qu'on fait des images dans les élevages de Suisse, à quel
point les animaux sont disproportionnées. Tellement gonflées que parfois
leur peau semble éclater comme un fruit trop mûr. Souvent, iels se
blessent sur le métal de leur cage. Souvent iels se piétinent, se
mangent mutuellement. Ce qu'on ferait aussi, sûrement, à leur place. Je
ne sais pas comment on peut penser que c'est normal, naturel, ou dans
l'ordre des choses de faire subir ça à un Truc qui vit et qui,
manifestement, ressent. Ces images sont, grâce à ce que mes camarades
font, disponibles sur internet. Tapez élevages suisses sur internet.
Tapez abattoirs suisses sur internet.

Une autre fois, on a été faire une sortie de repérage, pour filmer dans
un élevage de vaches et de porcs. Comme souvent, je suis restée planquée
dans les hautes herbes, comme un crapaud à capuche, à loucher sur la
pénombre tandis que les autres entraient dans l'enceinte de
l'exploitation. Je confirmais seulement qu'on ne voyait par leur lumière
de l'extérieur. Dans les écouteurs, Machin et Bidule constataient ce
qu'on constate toujours : des cadavres en décomposition sous des bâches,
la puanteur, les bébés cochons à l'agonie, les plaies et les abcès sur
les pattes et les ventres. C'était à gerber.

Plus loin, dans un autre bâtiment, iels ont entendu des mugissements qui
venaient de derrière d'immenses bottes de paille. Le silence a été long.
Je les ai entendu murmurer, je distinguais pas ce qu'iels disaient. Ce
qu'iels ont découvert, je l'ai appris plus tard : l'éleveur avait
enfermé trois veaux derrière ces bottes gigantesques, sans eau ni
nourriture. Il essayait de les cacher. Il les avait laissés là pour
mourir. Pas rentables, trop chers à faire abattre. On a décidé d'aller
chercher des bidons d'eau, des bouteilles. Vu l'urgence, on a aussi
décidé de faire une dénonciation au vétérinaire cantonal. Je crois que
ces veaux ne sont pas morts de soif finalement. Ils ont été abattus,
je suppose. Quelle victoire.

Ce n'est pas en sauvant trois lapins et en filmant les élevages qu'on
change la donne ou qu'on règle le problème. Et c'est pas non plus comme
ça qu'on adresse le problème de la précarité des éleveureuxses, je le
sais bien. Parfois, c'est une cause qui semble ne jamais avancer. Mais
j'entends et j'apprécie celleux qui disent que chaque animal sauvée est
une petite victoire. Parce que c'est vrai quelque part. Et parce que ça,
ça donne du courage.

Sûrement, ça doit paraître dérisoire à plein de gens de se préoccuper du
sort des animaux. Il y a même des militanxtes qui se battent pour
d'autres luttes, pour d'autres droits, qui trouvent que l'antispécisme
est un truc réservé à la bourgeoisie.

Je sais que ça peut être vrai. Je vois des *vegans* qui ne s'intéressent
pas aux luttes pour les droits humains, qui ne perçoivent pas la chance,
si on peut dire ça comme ça, qu'iels ont de pouvoir lutter pour d'autres
espèces que la leur. Je connais des *vegans* qui ne voient pas que
s'iels peuvent lutter pour les non-humaines, c'est peut-être aussi un
peu quand même parce qu'iels ont des droits et des privilèges. Pourtant,
il paraît évident que, dans toutes les luttes, il faudra bien converger
pour arriver à quelque chose. Pour faire masse.

D'ici là, ça me va d'être postée dans les herbes hautes à faire le guet,
avec mon strabisme, et Machin, et Bidule, et touxtes les autres qui font
pareil et même mieux, dans l'ombre de tous les abattoirs, de tous les
élevages, de tous les camions transporteurs.

[^75]: En accord avec les auteurixes, l'écriture inclusive binaire est utilisée pour les animaux non humaines, conformément aux conventions du volume, le féminin prime.

