---
author: "Deux personnes actives dans Porteous"
title: "Transformer une prison en centre socioculturel"
subtitle: "De l'occupation de Porteous à la négociation"
info: "Transcription d'un entretien"
datepub: "septembre 2020"
categories: ["squat, occupations, logement", "combats institutionnels"]
tags: ["administration, administratif", "banque", "confiance", "créatifvex, créativité", "impossible", "nuit, obscurité", "peur", "pouvoir", "prix", "radeau", "réunion", "théorie", "vacances", "voiture, bagnole"]
langtxt: ["fr"]
zone: "Suisse romande"
num: "8"
quote: "prenons la ville"
---

## L'occupation

À Genève, le rendez-vous de la rentrée, c'est la course de radeaux : on
construit un radeau avec ses potes et on dérive sur le Rhône dans la
joie et la bataille, pendant une bonne heure. C'est une tradition qui
existe depuis plus de vingt ans : la première course Intersquat a eu
lieu en 1999. C'est une façon de se retrouver entre gens qui luttent
pour s'amuser avant de commencer l'année, de reprendre le cours de la
vie normale.

Avec le collectif "Prenons La Ville", on ne voulait pas en rester à ce
qui nous semblait être l'échec vécu au printemps 2017 : l'occupation
avortée après une nuit d'un bâtiment construit il y a vingt ans en plein
cœur de Genève pour être une banque, mais jamais investi d'aucune forme
de vie. Alors, durant l'été, au milieu des vacances, on a préparé
secrètement une occupation pour la fin de la course. La cible était
toute trouvée : un superbe bâtiment désaffecté à Vernier, destiné à
devenir une prison, inutilisé depuis 22 ans, à quelques pas de
l'arrivée de la course.

Malgré un temps plutôt maussade, on était presque une vingtaine de
radeaux à prendre le départ, sous les applaudissements d'environ 150
personnes venues nous encourager. L'ambiance était populaire et
familiale. À l'arrivée : tâtonnement et cramponnement. On n'était pas
vraiment sûrexs de nous. Quelques fourgons de police antiémeute avaient
été aperçus non loin de là et on ne comprenait pas si c'était parce que
leur terrain d'entraînement était proche ou si ça indiquait qu'un énorme
dispositif se préparait à intervenir. Il y avait de la peur et de
l'hésitation. La fuite d'information semblait plus probable que la
coïncidence. Et puis, au moment de se lancer, d'avancer à pas feutrés
dans la forêt qui relie le ponton d'arrivée de la course au plongeoir
servant de point de départ à l'occupation, tout a marché comme sur des
roulettes. Quelques personnes ont tendu une échelle, grimpé dans le
bâtiment et hissé une banderole sur laquelle apparaissait cette phrase
d'espoir : "Nous construisons un monde sans prison". La soirée s'est
poursuivie sous le préau qui s'avance jusqu'au-dessus du Rhône avec un
repas, de la musique et la foule venue soutenir l'action. La police
locale a pointé le bout de son nez, mais semblait surtout préoccupée par
la nuisance sonore : elle a relégué le volet politique à ses supérieurs.
Pendant la deuxième nuit, quelques vigiles zélés ont tenté une incursion
ratée grâce aux barricades. Le bâtiment était devenu une forteresse. Le
lendemain, on s'est surprisexs à imaginer les activités qui pourraient
s'y dérouler, on s'est organisé un programme pour le reste de la
semaine. En bref, on s'est installéexs.

## Lever l'occupation

Pendant les semaines qui ont suivi, il a fait hyper chaud, c'était
magnifique. Il y avait tout le temps de la vie, des moments géniaux. Il
y avait toujours des gens qui étaient là pour bosser, pour faire des
trucs. Puis, peu à peu il a commencé à faire plus froid, il y avait de
moins en moins de personnes qui venaient sur place. Il y avait aussi des
personnes qui étaient peut-être moins engagées politiquement et
davantage intéressées à avoir un lieu où dormir. Au final, c'étaient
celleux qui étaient là le plus souvent, parce qu'iels n'avaient pas
d'autre choix.

Pendant l'occupation, il y a eu trois pôles. Le pôle des négociations
avec l'État, où on faisait toujours attention à se présenter en grosses
délégations, pour que tout le monde puisse prendre part aux
négociations. Le pôle qui s'occupait de la vie sur place, avec les
personnes qui voulaient éviter de trop croiser les politiques. Et enfin,
le pôle construction, les gens qui venaient pour faire avancer les
travaux. Il y avait évidemment des interactions entre ces pôles. Au mois
de mars, après sept mois d'occupation, les négociations avaient plus ou
moins abouti. Enfin, on n'a pas du tout eu l'impression de négocier ;
chacunex tenait ses positions. Et finalement, nous avons reçu une
proposition.

On nous a proposé de sortir du bâtiment en ayant la possibilité d'avoir
deux containers dehors pour qu'on puisse quand même garder un pied sur
place. En échange, iels levaient toutes les charges et les poursuites
juridiques qu'il pouvait y avoir contre les personnes ayant participé à
l'occupation. Aussi, trois places nous ont été attribuées dans une
commission qui allait décider de l'avenir de Porteous et iels
acceptaient notre revendication principale : la réaffectation du
bâtiment à l'Office cantonal de la cohésion sociale qui s'occupe de la
culture sous la direction du conseiller d'État Thierry Apothéloz, le
retirant ainsi des mains du département de la sécurité dirigé par Pierre
Maudet alors embourbé dans plusieurs scandales politiques. Cela voulait
dire que le bâtiment aurait dorénavant la vocation de devenir un centre
socioculturel plutôt qu'un centre carcéral. C'était déjà une victoire.

On a donc accepté la proposition et levé l'occupation. À cette période,
on était une bonne vingtaine, voire un peu plus. Il faisait froid, tout
le monde était emmitouflé, il y avait tout le temps des bougies partout
sur la table. C'était une période où j'étais fatiguée. On avait subi
beaucoup de pressions. Les négociations étaient épuisantes, toutes ces
réunions avec le Conseil d'État dans leur belle salle... En fait, je
pense que tout le monde était fatigué. Il y avait aussi moins d'énergie
à fournir sur place et, en même temps, la cabane sur le toit venait
d'être terminée. J'ai trouvé que c'était un très beau moment, parce
que je ne voyais plus forcément où ça pouvait aller. C'était quand même
une sacrée réussite et il y avait vraiment l'envie d'en profiter
aussi, de continuer à y avoir accès... Enfin voilà, il y avait toutes
ces choses qui nous disaient qu'on pouvait quand même avancer dans une
direction intéressante. Au final, je pense que les gens ne
s'attendaient pas à ce que l'occupation tienne. À la base c'était un
peu un coup de tête : "on fait des radeaux, on accoste et puis
voilà...". Le mouvement "Prenons La Ville" avait déjà fait plusieurs
occupations à Genève, mais elles avaient toutes été symboliques, elles
n'avaient jamais duré. Du coup, les personnes impliquées n'avaient pas
forcément envie de continuer. À cette période-là, il y a aussi eu un
très gros changement dans le milieu militant genevois, un changement de
génération. Les personnes plus âgées, plus expérimentées, se dirigeaient
vers d'autres choses. Certainexs d'entre iels s'éloignaient
temporairement de la Suisse, d'autres avaient des enfants, etc. Et la
nouvelle génération n'avait pas forcément envie de s'investir de la
même manière que les anciennexs, en ouvrant des squats par exemple.
C'était un moment... très émotionnel. Pendant la discussion où on a
décidé de lever l'occupation, tout le monde a pu s'exprimer. On a fait
des tours de table et on s'est dit : "on a déjà gagné le fait que ça ne
soit plus une prison et maintenant, si on a envie de gagner autre chose,
on le fera en essayant une voie plus institutionnelle."

## Une commission

C'était la première fois à Genève qu'il y avait une commission comme ça.
On s'est dit que ça pouvait être intéressant. On avait déjà rencontré
Nicole Valiquer[^21], la présidente de la commission. On connaissait
aussi pas mal d'architectes, comme Thierry Buache, qui venait de
terminer son travail de master sur Porteous et qui nous avait suiviexs
et conseilléexs depuis le début. Il y avait aussi des gens issus de la
culture, des personnes issues des Hautes Écoles, des personnes du
patrimoine... Il y avait aussi des représentants de la détention avec
l'idée de faire de la réinsertion dans ce nouveau lieu. Et des personnes
de l'OCBA, l'Office Cantonal des Bâtiments et de l'Architecture, sous
la direction de Hodgers[^25], et c'est elleux qui ont été les plus
difficiles à convaincre au début.

C'était un peu kafkaïen comme commission. On est sortiexs du
bâtiment fin mars 2019 et la première réunion de la commission n'a eu
lieu qu'en septembre 2019. Ce sont les temporalités étranges de l'État.
Ce qui était ridicule, c'est que jusqu'en janvier, la plupart des
personnes qui siégeaient dans la commission n'avaient jamais vu le
bâtiment, n'y étaient jamais entrées ; elles en discutaient sans savoir
de quoi elles parlaient. Des rapports sur l'état du bâtiment avaient été
effectués et montraient une présence restreinte de matières dangereuses
telles que le PCB, l'amiante, etc. C'était surtout pour bloquer le
processus. Mais à la suite de la visite, en janvier 2020, ça a commencé
à se débloquer et l'OCBA s'est montré beaucoup plus ouvert.

L'enjeu de cette commission était de proposer un projet à la fin du
processus : soit un projet sur lequel touxtes les représentanxtes de la
commission s'accordaient, soit deux projets parmi lesquels le Conseil
d'État devait trancher. Finalement, on a réussi à se mettre d'accord.
Pour ce projet, on a décidé de partir des nombreuses contraintes de
Porteous. Les espaces sont énormes, donc ça ne va pas être possible de
chauffer dans un premier temps. L'accès aussi est quand même une sacrée
contrainte, parce que le seul accès pour les véhicules traverse les
SIG[^22], la station d'épuration des eaux qui est juste derrière. En
gros, c'est presque impossible de venir à Porteous en voiture. Et,
niveau accessibilité pour les personnes à mobilité réduite, c'est très compliqué. En même temps, c'est une contrainte qui nous
oblige à réfléchir sur la mobilité douce, qui est une priorité. Pourquoi
ne pas réfléchir aussi à une mobilité par le Rhône ? À la base, les
Mouettes genevoises[^23] étaient censées aller jusqu'aux tours du
Lignon. Il y a déjà tous les embarcadères...

## Un centre socioculturel

De nouveau, c'est la construction d'un projet à partir des contraintes
et de l'expérimentation du lieu. Porteous est un bâtiment difficilement
habitable, même si ça aurait plu à beaucoup de personnes. Un temps, il y
avait le projet de faire un village de cabanes éparpillées partout sur
le site. Mais rendre habitable le bâtiment en soi, c'est un peu
utopiste. Du coup, une fois qu'on a mis ça de côté on s'est dit : "bon,
qu'est-ce qu'on a envie d'en faire ?". On a proposé d'en faire un
lieu "socioculturel" ; la culture c'est quand même quelque chose qui
rassure beaucoup l'État... C'est là où, en tous cas pour moi, ça fait
sens de rester dans ce projet, de continuer à essayer de défendre
certaines valeurs. En se demandant par exemple de quelles cultures on
parle ? Ça, ça va être les prochains combats...

Les bases du projet sont déjà posées par le rapport de la commission.
Les travaux ne se feront pas durant une seule longue période où le
bâtiment serait fermé, mais étape par étape, des activités prenant place
dans les parties mises aux normes pendant que les travaux se poursuivent
dans les espaces suivants. Cette façon de faire permet également
d'intégrer des nouvelles personnes, collectifs, associations de façon
organique à mesure que les espaces sont disponibles. Ainsi, on propose
plutôt une façon de faire qu'un projet terminé.

## Aujourd'hui

La commission a rendu sa proposition de projet en juin 2020 à la demande
d'André Klopmann[^24], le supérieur de Nicole Valiquer. Cette dernière
a depuis changé de département, elle fait toujours partie de la
commission même si elle n'en est plus la présidente. La présence de
l'architecte français Patrick Bouchain, qui a développé l'idée du
"permis de faire", a permis l'organisation d'une visite de Porteous en
septembre 2020 avec les conseillers d'État concernés par le projet, le
Chef du Département cantonal de la Cohésion Sociale Thierry Apothéloz,
et aussi Antonio Hodgers. À ce moment-là, on s'est rendu compte
qu'Apothéloz n'avait même pas encore eu le dossier entre les mains. Du
coup, l'hiver s'est passé au fil des mails pour relancer la commission,
on ne savait même pas si elle était terminée, ce qui allait se passer
par la suite.

Klopmann, en copie, n'a jamais répondu. Il y avait ces questionnements :
sur quoi faut-il appuyer ? quelle pression faut-il jouer ? On a décidé
qu'on allait continuer à avancer. Comme le projet est en train d'être
validé et qu'il a été plus ou moins accepté par la commission, c'est le
Conseil d'État qui doit valider maintenant, et s'il y a des fonds à
voter c'est le Grand Conseil. On s'est dit qu'on allait avancer de
notre côté et qu'on allait se tourner vers la FPLCE, la Fondation pour
la Promotion de Lieux pour la Culture Émergente, pour demander des fonds
pour l'aménagement de Porteous et la mise en fonction de la première
salle du bâtiment, le Chic and Shlag[^26]. De plus, une
sous-commission a été formée pour rédiger les statuts de la future
coopérative qui gérera Porteous. C'est ça la stratégie pour le moment.

## Le risque que ça nous échappe

C'est le risque qu'on a décidé de prendre, mais c'est un risque qui
était clair depuis qu'on a décidé de sortir du bâtiment. Ce sont des
choses qui ont été longuement discutées. Après, on a déjà gagné le fait
que ce soit un lieu socioculturel; tout ce qu'on gagne ensuite, c'est
du plus... On l'a vu comme ça. Et ça nous a permis de lâcher un peu la
pression, parce que... On était épuiséexs. Il faut que le temps qu'on
passe à monter ce projet, ce soit aussi du plaisir, parce qu'on ne sait
pas ce que ça va donner. Il faut que les souvenirs qui mènent à ça
soient de bons souvenirs.

J'ai confiance dans le fait que ça va gentiment se mettre en place.
C'est un projet qui a quand même reçu un soutien populaire très
large et très fort. Maintenant il va falloir être stratégique
et trouver des fonds, c'est aussi un bon moyen de
mettre la pression sur l'État pour que ça se débloque. On leur propose
un projet à trois millions. Avant, pour le projet de prison, ça montait
à 22 millions... C'est clair qu'ils ne vont pas mettre de l'argent
pour la culture là-bas, mais tant mieux en fait. Ils nous filent le
terrain, on fait la coopérative, et puis... on n'a plus besoin de
l'État.

## La motivation de départ

En fait, je pense qu'il faut se rendre sur place une fois pour le voir,
on se dit qu'on ne peut pas laisser un lieu comme ça vide. Déjà, quand
j'étais ado, je me souviens, il y avait les anciennes caves de Satigny
qui étaient vides et j'adorais aller me balader là-bas et faire des
projets, me dire "ah mais ce serait tellement bien de reprendre un
espace". Il y a vraiment ce truc de s'approprier un lieu qui n'est
utilisé par personne pour en faire quelque chose de chouette, parce
qu'on a la créativité, on a l'envie, on a l'énergie... Moi ce sont
des valeurs avec lesquelles j'ai grandi et avec lesquelles j'ai envie
de continuer à grandir. Et je pense que ce sont des possibilités
géniales à partir du moment où t'as une responsabilité et que tu
t'appropries quelque chose. Tu vois les effets de ce que tu fais. Je
pense que ce sont des pratiques qui se perdent et qui manquent beaucoup.
Alors ça donne beaucoup de pouvoir aux gens de se rendre compte qu'ils
sont capables de le faire par eux-mêmes. Et de réaliser qu'on n'a pas
besoin de l'État pour tout.

Aussi, pour moi, les espaces sont essentiels. Les endroits où j'ai pu
me politiser, c'étaient des lieux, c'était des espaces. C'est là où tu
rencontres des gens, où tu peux construire des choses. Les espaces sont
essentiels à la politisation. À partir de valeurs qui peuvent être
conceptuelles, réussir à se dire "bon maintenant, comment est-ce que
concrètement on les met en place ?". Que ce soit l'inclusivité, le
féminisme, les questions de prix... Se dire : "comment on fait pour
que Porteous soit équitable et accessible à touxtes en termes de prix ?"
Il y aura des personnes qui seront payées pour faire
l'administration, mais du coup est-ce qu'il faut payer les artistes ou
pas ? Ben oui, il faut payer les artistes. Mais, en même temps, si
c'est le collectif, qui est parfaitement bénévole, qui s'occupe des
tâches ingrates de gestion et de nettoyage, c'est compliqué de payer
les artistes. Je trouve que ce sont des choses qui ouvrent sur beaucoup
de questionnements pratiques en fait. De la théorie à la pratique, et la
pratique changera la théorie. Expliquer ce que
c'est qu'un bar prix libre, le fait qu'il y ait une attention portée au
respect du consentement d'autrui... C'est déjà construire un lieu
accueillant. Enfin voilà, ce sont des ambiances qui permettent de poser
des premières questions et de construire quelque chose de chouette.

[^21]: Nicole Valiquer Grecuccio, (1960) "Adjointe scientifique sur les “lieux culturels” chez Office cantonal de la culture et du sport" (in Profil LinkedIn).

[^22]: SIG est l'acronyme de Services Industriels de Genève. Établissement public du canton de Genève, SIG est une entreprise fournissant notamment en eau, électricité, gaz et énergie thermique et responsable des réseaux correspondants. Elle est également en charge du traitement des eaux usées, du traitement des déchets et de la gestion d'un réseau de fibre optique.

[^23]: Les Mouettes Genevoises sont un réseau de transport lacustre officiant dans la rade de Genève.

[^24]: André Klopmann, (1961) directeur général a.i. de l'office cantonal de la culture et du sport.

[^25]: Antonio Hodgers, (1976) homme politique suisse membre du parti des Verts. Il est conseiller d'État du canton de Genève depuis le 10 décembre 2013 et président du Conseil d'État du 13 septembre 2018 au 17 octobre 2020.

[^26]: Le bar installé au rez-de-chaussée de Porteous.

