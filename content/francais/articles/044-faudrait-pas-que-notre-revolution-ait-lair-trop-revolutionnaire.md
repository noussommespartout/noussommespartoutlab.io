---
author: "Quelques militantes d'Extinction Rebellion en Suisse"
title: "Faudrait pas que notre révolution ait l'air trop révolutionnaire"
subtitle: "Discussion auto-organisée entre plusieurs militantes d'Extinction Rebellion"
info: "Transcription d'une discussion orale"
datepub: "juin 2020"
categories: ["violence, non-violence", "écologie", "autogestion, expérimentations collectives"]
tags: ["autocritique", "bienveillance", "blague", "bouffe", "crier", "empathie", "erreur", "force", "France", "gouvernement, gouverner", "gueule", "injustice", "internet, web", "justice", "logique", "logo", "merde", "peur", "pizza", "pouvoir", "rage", "rue", "sabot, saboter, sabotage", "silence", "solution", "théorie", "violence", "vote"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "44"
quote: "ah non, bouh, on n'aime pas la violence, caca la violence"
---

## Chapitre 1 --- Faudrait pas que notre révolution ait l'air trop révolutionnaire

--- Bon, le gros sujet, c'est le niveau de radicalité acceptable dans
Extinction Rebellion (XR), et pourquoi les autres mouvements militants
s'éloignent de nous.

--- C'est pas compliqué. Pour l'instant, XR c'est un truc pour petixtes
bourgeoisexs blanchexs qui se foutent pas mal de ce qui ne concerne pas
le climat. À la base, XR n'est pas du tout fait pour ce qu'il fait
maintenant. Le projet, c'est que XR doit faire tout ce qui est possible
pour qu'émergent des rébellions ; par exemple former des personnes à la
désobéissance civile. XR devrait être un mouvement ouvert aux autres
luttes, les autres devraient pouvoir nous dire quand on fait de la
merde, parce que plus on est nombreuxses, plus nos revendications ont du
poids et du sens, plus on se comprend comme une lutte contre toutes les
formes d'oppression.

--- Eh bah tu commences fort ! (*Rires.*)

--- XR s'est bâti sur trois revendications... peut-être qu'il faut les
rappeler.

--- Arf. (*Rires.*)

--- Y'a la déclaration de l'urgence climatique, l'objectif d'avoir zéro
émission carbone pour 2025 et la création d'assemblées citoyennes
souveraines pour régler la question environnementale. Clairement, il
faudrait ajouter une quatrième revendication qui soit sociale, parce que
là, XR n'apparaît pas du tout comme un mouvement qui veut transformer
la société.

--- Aujourd'hui, en Suisse, c'est un mouvement qui se referme sur son
petit créneau et abandonne toute autocritique. XR reste dans sa zone de
confort de désobéissance écolo non violente sans accepter les critiques
des autres groupes militants.

--- Le cercle vicieux, c'est que le mouvement n'attire que des personnes
qui se reconnaissent dans la doctrine, alors la masse consensuelle des
militanxtes XR étouffe toutes les personnes qui essaient de porter une
autocritique.

--- On appelle ça le "*XR Social Club*" entre nous.

--- C'est quoi ça ?

--- C'est genre *Lions Club* quoi. Ça donne des gens qui se retrouvent
entre eux pour partager des moments sociaux, ce qui est cool en soi,
mais ils le présentent pas comme ça. On dit que c'est un truc radical
pour le climat et souvent, en réalité, on attend que le temps passe en
bouffant des pizzas...

--- Ça décourage les gens qui pourraient se radicaliser, ça te calme
alors que c'est pas le moment de se calmer.

--- C'est un vrai problème dans le milieu militant en général. C'est que
tu lances des trucs, tu vois, t'as une énergie de départ qui prend,
après, ça devient hyper affinitaire. Les camarades deviennent des potes
et du coup tous les moments de lutte sont des moments hyper chouettes et
du coup tu te réjouis d'y aller et, au bout d'un moment, tu l'as pas vu
arriver, mais t'es devenu un groupe de potes et ce groupe de potes n'est
plus dans une logique très militante et ça se referme... déjà c'est
difficile d'entrer dans un groupe militant et c'est pire quand il y a
quelque chose de très social, quand tout le monde se connaît hyper bien.

--- T'es aussi dans l'entre-soi de la pensée, c'est-à-dire qu'il n'y a
rien qui remet en question le *social club* de XR. C'est devenu
tellement hermétique. Le mouvement n'est plus capable de générer de
lui-même la remise en question nécessaire pour continuer à avancer.

--- Moi ce qui me choque, avec ce truc de *social club*, c'est que XR ça
devient ton identité, quelque chose auquel tu tiens de manière très
affective, donc tu le défends d'autant plus fort, ton raisonnement
devient parfois irrationnel.

--- Des fois, face aux critiques, c'est plus XR et des idées que tu
défends, c'est toi et tes potes, ça change tout.

--- Le problème, c'est aussi les "Principes et Valeurs" que tu dois
accepter pour rejoindre XR...

--- Faudrait les résumer, non ?

(*Un moment de silence.*)

--- Normalement, tout le monde peut se revendiquer de XR à condition
d'accepter dix principes et valeurs. Ces principes et valeurs, tu peux
les interpréter un peu comme tu veux, mais il y a une interprétation
dominante qui en est faite et elle devient dogmatique, intouchable et
socialement partagée. Avec les méthodes d'action, c'est pareil. Il y a
eu des blocages de ponts et maintenant, on ne fait que des blocages de
ponts, parce que c'est ça qu'il faut faire. Le reste on ne connaît pas,
donc on ne fait pas. On ne prend pas de risque. Au début, on collabore
avec la police, et un an après, celleux qui se radicalisent et qui ne
veulent plus collaborer avec la police ou qui remettent cette méthode en
question sont désavouéexs.

--- C'est intéressant ce que tu dis, parce que c'est aussi l'entre soi
des mouvements militants qui empêche d'explorer des stratégies d'actions
différentes. On sait que les actions directes plus radicales ont
fonctionné, comme les <a href="/glossaire/#suffragettes" class="glossary-link" target="_blank" rel="noopener" data-no-swup>suffragettes°</a> qui ont cramé des banques pour le
droit de vote des femmes. On sait aussi que les grands mouvements qu'on
dit pacifistes[^121], comme la décolonisation indienne ou le mouvement
pour les droits civiques aux États-Unis ont été largement accompagnés
d'actions violentes qui ont créé un rapport de force. Chez XR, la
majorité refuse ce niveau de radicalité. Il n'y a pas d'actions avec les
autres mouvements, pas d'actions de solidarité communautaire.

--- Mais ça vient quand même du fait que y'a beaucoup de monde qui *like*
la page facebook ou qui porte des pin's XR, mais en réalité pas grand
monde qui organise vraiment des choses.

--- Est-ce que ça vient pas aussi du projet de base ? XR, c'est cette
espèce de truc qui est à la fois nouveau, intéressant et problématique :
une rébellion et un mouvement offerts clés en main desquels tu récupères
des principes et valeurs qui ont été faits par d'autres personnes dans
un autre contexte. Tu rejoins XR et on te dit : principes et valeurs,
méthodes tactiques, c'est comme ci et comme ça. Sur internet tu peux
tout trouver : des idées pour organiser la formation des nouvelles
personnes, des listes d'actions, des conseils, des discours tout faits,
des communiqués de presse. Tout est déjà prêt. C'est pas du tout une
structure qui est faite pour avoir des discussions de fond : c'est un
projet tactique. On bloque les villes, on essaie de mobiliser très vite
un petit pourcentage de la population pour créer un changement rapide.
Si ça marche pas, faut passer à autre chose.

--- Mais c'est aussi ce que je trouve trop beau dans XR, grâce à ce côté
"clés en main ", il y a des antennes qui se sont organisées
spontanément aux quatre coins de la planète. Il y a des groupes XR en
Californie, en Inde, en Suède, au Ghana, au Japon, au Maroc, etc. Mais
le souci, c'est que les discours et les tactiques ont été
développés par XR en Angleterre et ne peuvent pas être copiés-collés
dans tous ces contextes différents : les situations politiques et
écologiques ne sont pas les mêmes, et la répression policière non plus
ni les appareils juridiques. D'ailleurs, tu l'observes, spontanément
dans certains pays, le logo XR a été adopté par des collectifs qui
mènent des luttes décoloniales ou anti-industrielles en Asie du Sud-Est,
des combats féministes en Afrique Centrale, et d'autres réalités encore.
C'est devenu un symbole de résistance globale, et c'est super.

(*Un moment de silence.*)

--- C'est sûr, quand j'ai rejoint XR, j'ai suivi les formations que
propose le mouvement aux nouvelleaux et tout me paraissait hyper
réglementé. Je ressens la même chose avec les débats de fond, on te
répond souvent : "Oui mais il y a une règle supérieure, donc on ne peut
pas en rediscuter, donc c'est comme ça". Il y a une marche à suivre
d'action qui est présentée comme la seule à être légitime. Surtout, il y
a cette idée de "<a href="/glossaire/#consensus-daction" class="glossary-link" target="_blank" rel="noopener" data-no-swup>consensus d’action°</a>" : si tu viens sur un blocage XR,
tu acceptes explicitement certaines règles, comme ne pas boire d'alcool
ou ne pas insulter la police. Si tu refuses le consensus d'action, ça
fait des problèmes.

--- Un des problèmes, par exemple, c'est le positionnement du mouvement
vis-à-vis de la police. Il ne faut pas crier des slogans antipolice, il
faut respecter le travail de la police[^122]. Et on ne parle même pas
de la création d'un rapport de force physique. Quand t'essaies de créer
un vrai débat de fond, ben les gens qui créent ce "consensus"
dogmatique ne sont pas là. En Suisse, il y avait eu une journée pour
parler de ça, de la police, mais comme d'hab quand tu mets ces sujets
sur la table, il n'y a que les personnes convaincues qui viennent pour
en discuter[^123]. Personne ne veut en parler, et on n'avance pas.

--- Et il y a beaucoup de personnes qui font super attention à l'image du
mouvement, que ce soit par rapport à elleux parce qu'iels n'ont pas
envie d'être dans un truc trop radicalisé ou parce qu'iels tiennent
beaucoup trop à cette image médiatique[^124] des gentillexs bobos qui
font pas trop chier.

--- Bon c'est pas comme ça qu'iels le disent, évidemment. (*Rires.*)

--- Les actions de XR se basent tellement sur la portée médiatique des
choses que, du coup, t'es obligéex de rester dans des discours, des
méthodologies et des actions acceptables et audibles par les médias.

--- En fait, on s'est renduexs médiatiques.

--- Ouais.

--- Être bien vu des médias, c'est automatiquement être dans une
perspective réformiste et dans une perspective de lobbying où tu fais
pression sur l'État dans l'espoir que l'État amène des solutions parce
qu'au fond tu crois à ça, tu crois que l'État a la solution. En fait,
tactiquement ces personnes ont raison, dans leur logique. Pour créer une
discussion avec l'État, faire un gros bloc révolutionnaire qui pète tout
et qui n'écoute pas la police, ça peut pas marcher : l'État ne va pas
t'écouter. L'État est convaincu d'être la volonté du peuple, en tout cas
c'est toujours derrière ça qu'il se cachera. En gros, soit tu joues le
jeu truqué du débat démocratique, soit la démocratie te met en prison.

--- C'est à la fois la force et la faiblesse de XR : c'est que ça veut
plaire au plus de gens possible et pour convaincre plein de gens, tu ne
peux pas être trop radicalex, tu dois rester dans une semi-radicalité
cheloue. Je suis rentrée à XR et j'étais pas du tout radicale, j'ai pu
rencontrer des gens, j'ai appris des choses et ça, c'est une force
incroyable. Mais je pense qu'il faut pas attendre autre chose de ce
mouvement qu'une sorte de porte d'entrée vers d'autres militantismes.

--- Mais dans cette logique, du coup il faudrait que les personnes
radicales restent dans XR pour continuer à faire le pont vers des formes
d'actions et des mouvements plus révolutionnaires.

--- C'est ça. Mais c'est pas facile.

--- Je suis pas si sûr. Les gens peuvent peut-être se radicaliser au fil
de leur parcours, mais il n'y a rien qui l'assure et je pense qu'il ne
faut pas surestimer XR. Au final, on tourne sur très très peu de
personnes et on a un énorme ventre mou de personnes sympathisantes qui
hésitent entre aller au marché et aller à une action.

--- Et on nourrit ça en fait, XR est allé vendre des t-shirts au marché.

--- C'est là que tu te dis ben super on a vraiment pas compris le
problème. On va avoir des centaines de personnes qui se promèneront avec
des t-shirts XR dans la ville, mais qui seront jamais sur les actions,
qui seront jamais investies dans le mouvement, qui ne se radicaliseront
pas, mais qui afficheront cette espèce de mot "rébellion" sans en
comprendre le début du sens.

--- C'est méprisant ce que tu dis.

--- Ouais, mais on est tellement dans la merde qu'on devrait déjà être en
train de saboter l'État, faut être clairexs[^125].

--- Oui.

--- Oui.

--- Oui.

--- Ouaip.

(*Un moment de silence.*)

## Chapitre 2 --- Les chenilles, les pandas et l'intrication des systèmes de domination

--- Donc le souci, c'est peut-être le discours de XR.

--- Par exemple, les "formations" à l'urgence climatique, le "*talk*"
qu'on te propose pour rejoindre le mouvement.

--- Ouais, alors le *talk* c'est une présentation gérée par les
militanxtes, ouverte à tout le monde, durant laquelle on présente les
causes du réchauffement climatique, les prévisions scientifiques comme
le rapport du GIEC[^126], l'inaction des gouvernements, les limites de
la consommation individuelle responsable, mais aussi la stratégie de XR
et pourquoi la désobéissance civile non violente peut marcher.

--- Dans ces conférences-là, on devrait juste expliquer ce qui se passe,
bien le faire et le faire tellement bien que les gens aient envie de
cramer les infrastructures de l'oppression. Genre en vrai, c'est ça
qu'il se passe, c'est que c'est tellement horrible que personne ne
devrait rester immobile. Même les gens qui n'ont pas envie de militer
pour le climat devraient crever de peur, c'est ça la réalité.

--- Et ça, c'est raté. C'est pas ce que fait ce *talk* de formation que
propose XR.

--- Perso je trouve que c'est bien, cette perspective de *talk* et de
formation, mais il faut donner une portée politique plus large aux
discours et aux réalités scientifiques et environnementales qui sont
toujours au-devant de la scène. Rien n'est dit à propos de l'intrication
des oppressions et de leurs côtés systémiques par exemple. Certes les
arguments scientifiques sont indéniables, mais ils sont pas suffisants
pour fonder une analyse sociale et politique de la situation.

--- Et puis c'est très biaisé. Certes il y a Gandhi, mais ça s'est fait
dans une violence incroyable, la lutte décoloniale en Inde. Les
<a href="/glossaire/#suffragettes" class="glossary-link" target="_blank" rel="noopener" data-no-swup>suffragettes°</a> ont incendié des banques pour obtenir le droit de vote.
Luther King a soutenu l'armement d'une majorité des militanxtes du
*Black Panther Party*. Mandela a littéralement co-fondé et dirigé l'aile
militaire de l'ANC[^127], *Umkhonto we Sizwe*, mené des centaines
d'actions de sabotage et défendu la nécessité de la lutte armée.

--- Niveau violence, c'est autre chose que d'écrire <a href="/glossaire/#acab-1312" class="glossary-link" target="_blank" rel="noopener" data-no-swup>ACAB°</a> sur des murs.
Si tu le fais lors d'une action XR, tu te fais reprendre et limite
dégager de l'action. Même quand tu hues la police tu te fais reprendre.

--- Ces *talks* devraient présenter XR comme un mouvement <a href="/glossaire/#systémique" class="glossary-link" target="_blank" rel="noopener" data-no-swup>systémique°</a>.
Parce que la crise climatique résulte de l'ensemble de l'exploitation du
vivant et des autres formes de domination : capitaliste, patriarcale,
raciste, spéciste. En plus, il y a une convergence naturelle des combats
sociaux et écologiques, par exemple, c'est souvent en luttant pour leurs
conditions de travail que les syndicats sont les premiers à alerter sur
les dangers écologiques dans les industries.

--- La formation devrait parler de l'État et du climat. Si tu prends
toutes les lois que l'État a savamment créées, c'est simple, aucune
n'est respectée, aucune. C'est le meilleur moyen de pousser à la
désobéissance ; c'est de montrer que l'État ne respecte pas les lois
qu'il a lui-même créées.

--- En gros, l'État ne respecte pas les lois que le peuple a l'illusion
d'avoir lui-même créées[^128].

--- Aussi, ça me frappe que les luttes écologistes s'appuient constamment
sur le discours des sciences dures (la biologie, le climat et tout),
mais qu'on y entende presque jamais les sciences sociales, qui décrivent
et expliquent justement comment toutes les oppressions s'imbriquent. La
théorie politique a vachement disparu, la sociologie est très peu
présente dans nos mouvements écologistes. Même les groupes plus radicaux
auront tendance à faire une chiée de com' sur la fonte des glaces et sur
tout un tas de faits qui paraissent convaincants et du coup le reste
disparaît, comme si on était convaincuexs que ce discours sociopolitique
n'était plus nécessaire.

--- Moi, je suis tellement convaincue qu'on gagnera jamais que je m'en
fous, on pourrait me dire n'importe quoi, on pourrait me dire "on veut
du caca rose", je descends quand même dans la rue, c'est pas ça qui...
mais peut-être que ça fait tellement longtemps que je suis entrée dans
les luttes que je me pose même plus la question de ce qui est
revendiqué, c'est débile. Mais, pour moi on gagnera jamais, c'est déjà
tellement trop tard que je milite juste pour sauver ma dignité
d'humain.

(*Un temps de silence.*)

## Chapitre 3 --- Tout le monde déteste la... violence

--- Il y a aussi des choses qu'il faut expérimenter pour te rendre compte
que ça ne fonctionne pas. Quand tu te retrouves face à une injustice,
quand tu te prends une flash-ball, c'est pas pareil que quand tu lis un
article de journal. Il y a beaucoup de personnes qui deviennent plus
radicales quand elles se font arrêter. Enfin, il y a tout un tas de
violences, tant que tu les as pas vécues, elles ont l'air fictionnelles,
irréelles, médiatiques.

--- D'où le paradoxe du rapport entre XR et l'action offensive.

--- Tu veux dire "violente" ?

--- Ah non, bouh, on n'aime pas la violence, caca la violence.

--- XR est un mouvement qui prône la désobéissance non violente et il n'y
a aucun problème à choisir cette tactique, c'est inclusif, c'est une
tactique qui peut fonctionner, comme les autres, et ça peut visibiliser
la violence étatique, etc. En fait, le problème c'est qu'on s'est
renduexs compte qu'on n'est pas touxtes d'accord sur ce qui est violent
et ce qui ne l'est pas, même à l'intérieur du mouvement.

--- Le débat n'a presque pas lieu d'ailleurs. La non-violence est devenue
une sorte d'obsession. Du coup, tout est pesé et rendu le plus léger
possible, tout est lissé.

--- Chacunex a une tâche précise lors d'une action. Durant chaque action,
il y a des militanxtes à qui on attribue le rôle de *peacekeeper*.
Encore un truc importé sans réfléchir de XR UK. Ces *peacekeepers*
sont là pour s'assurer que les actions ne débordent pas. Genre iels vont
désescalader dès que ça chauffe avec les keufs ou avec les gens qui
passent par là et qui nous insultent.

--- Tant que XR cherchera à policer ses actions, de moins en moins de
gens feront l'expérience, comme tu disais, de la violence d'État,
expérience qui est au fondement de la tactique de la non-violence.

--- C'est tellement un paradoxe.

--- Franchement, faut pas déconner, dans les blocages XR, je ressens à
peine la violence de l'État. Je l'ai expérimentée ailleurs. Je connais
la théorie de la désobéissance non violente. Je sais que c'est un
ressort tactique, que la non-violence permet de faire de la violence
d'État un spectacle pour qu'elle soit vue et officialisée, sauf qu'en
fait en étant dans une méga obéissance et discipline, tout ce que t'as,
c'est des vagues images de personnes assises portées par des keufs.

--- En fait c'est une erreur d'avoir pensé que l'État, en tout cas en
Suisse, allait exprimer sa violence face à des blanchexs qui se disent
non violenxtes. La répression subie chez XR est différente. Les keufs
vont faire des filatures, te mettre la pression, t'humilier au poste, et
la justice nous accable totalement. Mais rien de cette violence ne
s'exprimera au grand jour. C'était une erreur galactique de penser que
l'État et la police allaient nous traiter de manière spectaculaire.

--- Regarde en France, une fois les CRS ont gazé un <a href="/glossaire/#sit-in" class="glossary-link" target="_blank" rel="noopener" data-no-swup>sit-in°</a> pacifique
d'XR, et les médias leur ont mis tellement cher que le gouvernement a
développé des stratégies différentes, plus sournoises. Il laisse le
mouvement mourir dans sa désobéissance inactive.

--- En plus, pendant les actions XR, quand les flics interviennent, il y
a des personnes qui disent "non mais la police c'est des gentils, ils
font simplement leur travail". Pour certaines personnes cette
intervention pourrait déjà sembler violente, et bah non, même pas, tout
le monde autour de toi te rappelle qu'en fait ça n'est pas violent,
c'est "normal" que les keufs nous dégagent. (*Rires, mais dépités.*)

--- Même la planification des actions de blocage essaie de tout prévoir,
y compris les keufs. À tel point qu'il y a même des personnes nommées au
préalable qui sont responsables du dialogue avec les keufs, comme on
disait juste avant. La police arrive et elle n'est pas perturbée une
seconde, elle sait à qui parler, tout est prêt et ça devient une routine
puisque c'est toujours la même chose.

--- La police n'a même plus besoin d'avoir des micros.

--- C'est ça, c'est même plus à elle de faire le taff quoi.

--- Je suis pas totalement d'accord avec toi quand tu dis que jamais la
police ne sera violente avec des blanchexs bobos sur un pont, dans ce
mode d'action là, oui, mais dans l'histoire des luttes écologistes,
c'est pas vrai. La police est violente quand on arrive à la dépasser.
Alors que là, pendant un blocage XR, la police sait que tout va bien se
passer, du coup elle peut être tranquille et réprimer de manière
télévisuelle.

--- Le jour où on se met à nasser les flics et puis à s'enchaîner à leurs
camions, qu'on leur impose des trucs qu'iels ne comprennent pas, sans
violence physique, ben là, ça va vite changer de registre. En réalité,
les keufs suisses sont assez peu préparéexs et organiséexs pour gérer
des actions de masse.

--- À l'interne du mouvement, le problème, c'est qu'on en revient à
l'obsession de l'image médiatique. On est à fond pour recruter de
nouvellaux militanxtes en leur montrant que c'est cool et c'est devenu
uniquement ça. On bouffe de la pizza et personne ne va dire non à de la
pizza. À partir du moment où tu commences à lancer des pavés, plus
personne ne te soutient.

--- Donc le mouvement se pacifie naturellement, devient montrable et
médiatique. La police nous rend télévisuellexs.

--- Je pense que vous avez tort sur un truc, c'est que la majorité des
personnes qui militent dans XR ne pensent pas que la désobéissance non
violente sert à exposer la violence de l'État.

--- Pourtant, c'est la stratégie d'XR. En tout cas, c'est le premier truc
qu'on m'a dit durant le *talk* de formation.

--- Bah moi, si je vous avais pas rencontréexs, j'aurais jamais compris
cette stratégie je crois.

--- Il peut y avoir des trajectoires hyper différentes à l'intérieur d'un
mouvement, surtout dans un grand mouvement comme ça, ça dépend aussi des
groupes affinitaires que tu vas rencontrer.

--- C'est même visuel. Là, nous, on s'assied tout de suite, mais rester
debout tout simplement, c'est déjà autre chose. Faut être
désobéissanxtes, non violenxte pourquoi pas, mais puissanxtes et
fortexs.

--- Le plus souvent, les actions XR sont pensées comme des vitrines pour
intéresser et recruter de nouvelleaux militanxtes. En Suisse, c'est
sûrement une erreur stratégique de croire qu'on grandira jusqu'à
atteindre une masse suffisamment grande pour faire bouger les choses. On
n'a pas l'historique des luttes sociales de la France ou de l'Allemagne ;
faut penser différemment.

--- Je sais pas si vous savez, mais à Lausanne tout le mouvement s'est
divisé parce qu'une seule personne avait tagué <a href="/glossaire/#acab-1312" class="glossary-link" target="_blank" rel="noopener" data-no-swup>ACAB°</a> pendant une action.
Un <a href="/glossaire/#acab-1312" class="glossary-link" target="_blank" rel="noopener" data-no-swup>ACAB°</a>, et tout le mouvement tremble. Les bases du mouvement sont
solides...

(*Moment de silence.*)

## Chapitre 4 --- Quand le néo-chamanisme bobo blanc achète la paix sociale

--- Bon c'est bientôt la fin de cette discussion, et on a une autre
réunion après.

--- Quand j'ai commencé à militer, j'aurais aimé qu'on me prévienne que
j'allais passer le peu de temps libre que me laisse le capitalisme à
faire des réunions[^129]. (*Rires.*)

--- De quoi on veut parler pour conclure ?

--- De ce qui rend XR super ?

--- Bah on pourrait, mais c'est clair : sa force de frappe, le rapport à
l'<a href="/glossaire/#action-directe" class="glossary-link" target="_blank" rel="noopener" data-no-swup>action directe°</a> désobéissante, le fait qu'il agrège plein de personnes
et surtout qu'il propose un cadre à des personnes qui militent pour la
première fois et qui ne savent pas comment s'engager en dehors de
l'impasse des partis et des ONG.

--- On pourrait parler de pourquoi il faut arrêter de se comporter comme
une start-up qui fait du *branding*[^130] avec des drapeaux et des
logos partout ?

--- On pourrait parler de l'après, de la résilience, d'une autre manière
de vivre ?

--- Ce qui s'appelle la "culture régénératrice" chez XR.

--- Arf.

--- L'idée de base est super : XR s'organise et crée un rapport de force
pour arriver à la constitution d'<a href="/glossaire/#assemblee-citoyenne" class="glossary-link" target="_blank" rel="noopener" data-no-swup>assemblées citoyennes°</a> souveraines
émancipées des États. Pendant ce temps-là, le mouvement travaille à
construire une autre culture, plus décroissante, désaliénée, emphatique,
sociale et bienveillante. Après la révolution, XR se dissout et il ne
reste que des assemblées citoyennes et une culture régénératrice.

--- Sur le papier, c'est chouette.

--- Le problème, c'est la pratique.

--- Ouaip.

--- Aujourd'hui ce que le mouvement porte comme une culture
régénératrice, c'est une grosse blague.

--- Chez XR, on confond bienveillance et mièvrerie. La bienveillance,
c'est aussi de dire les choses quand il le faut, de reconnaître ses
privilèges et les capacités de chacunex, de construire une offensive
sans que la pratique du militantisme devienne violente pour les
personnes. La bienveillance, c'est reconnaître et essayer de lutter
contre le sexisme ou le racisme qu'on reproduit inévitablement dans
toute organisation collective et qui exclut les personnes concernéexs de
nos luttes[^131].

--- Aujourd'hui, chez XR, y'a même des gens qui parlent de bienveillance
à l'égard des patrons et des flics.

--- Bon, c'est une minorité quand même.

--- Ouais, mais c'est symptomatique du problème. Moi, quand j'ai la rage
contre le système qui détruit le vivant, que j'ai envie de l'attaquer,
on m'a déjà répondu : "Tu devrais te faire aider, pour surmonter la
colère qui est en toi." (*Rires.*)

--- C'est toi qui avais dit ça une fois, que la culture régénératrice
chez XR, pour le moment, c'est un truc pour bobos néo-bouddhistes,
franchement le yoga énergétique avant les réus... ça va deux secondes.

--- Aujourd'hui, la culture régé, c'est se mettre des cuites, méditer et
faire des balades en forêt. Y'a pas d'empathie là-dedans. C'est de
l'empathie envers soi-même.

--- Justement, le problème de tout cet univers chamanico-bien-être, c'est
que c'est centré sur soi, et pas sur les autres. Moi je me bats et je
lutte pour la collectivité, pas pour ma gueule, surtout que je suis
blanche et que j'ai un taff valorisé par la société. J'ai pas besoin
qu'on prenne soin de moi. J'ai besoin qu'on détruise les structures qui
nous empêchent de prendre collectivement soin les unexs des autres.

--- C'est naze, parce que la culture régé, c'est censé être un truc qui
te donne de l'énergie pour faire des actions et du coup ça devrait aussi
être un truc dans lequel on peut rager ensemble. Je suis plus rassurée,
régénérée et remotivée en discutant avec vous, qu'après n'importe quel
événement régénérateur d'XR.

--- Il faut une culture rage-énératrice.

--- On va vraiment finir sur un jeu de mots pourri ?[^132]

--- Qui a mieux ?

(*Silence.*)

[^121]: *Kill the Hippie in your Head* \[n^o^ 48\] remet aussi en question l'efficacité du pacifisme.

[^122]: *They don't see us* \[n^o^ 4\] aborde les violences policières vécues.

[^123]: Deux convaincuexs en parlent dans *Spectacle nulle part.* Care *partout* \[n^o^ 23\].

[^124]: *Survivre dans un black bloc* \[n^o^ 15\] aborde la question du pacifisme dans les médias.

[^125]: Dans *Piraterie ordinaire* \[n^o^ 37\] on aborde la question du sabotage.

[^126]: Groupe d'Experts Intergouvernemental sur l'Évolution du Climat créé en 1988 par l'Organisation des Nations Unies. Ses rapports --- qui fournissent des évaluations détaillées de l'état des connaissances scientifiques, techniques et socioéconomiques sur les changements climatiques, leurs causes, leurs répercussions potentielles et les stratégies de parade --- sont une référence scientifique internationale.

[^127]: Le Congrès national africain (en anglais *African National Congress*) est un parti politique sud-africain fondé en 1912 pour défendre les intérêts de la majorité noire contre la minorité blanche. Déclaré hors-la-loi par le Parti national pendant l'*apartheid* en 1960, il a été à nouveau légalisé en février 1990. Les États-Unis l'ont classé comme organisation terroriste entre 1986 et 2008. En 1994, les premières élections législatives multiraciales au suffrage universel sans restriction permettent à l'ANC de conquérir le pouvoir et à Nelson Mandela, président de l'ANC, d'être élu président de la république d'Afrique du Sud. Depuis lors, L'ANC domine la vie politique sud-africaine.

[^128]: *Là-haut sur la Colline* \[n^o^ 51\] évoque l'application populaire des lois.

[^129]: *Réapprendre à s'organiser* \[n^o^ 21\] propose des outils pour repenser les réunions.

[^130]: Technique marketing de gestion de l'image des marques et des entreprises.

[^131]: *Un jour, j'ai poussé la porte d'un hangar tout pété* \[n^o^ 26\] raconte ça de l'intérieur.

[^132]: On trouvera dans *Le Grand Midi* \[n^o^ 47\] d'autres jeux de mots bien pourris.
