---
author: "Anonyme"
title: "Quand l'espace s’ é t i r e"
subtitle: "La mixité choisie"
info: "Texte rédigé pour le recueil"
datepub: "août 2020"
categories: ["autogestion, expérimentations collectives", "relation à la militance"]
tags: ["brèche", "crier", "outil", "silence"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "27"
quote: "c'est l'excitation d'exister ensemble"
---

\[*La <a href="/glossaire/#mixité-choisie-ou-non-mixité" class="glossary-link" target="_blank" rel="noopener" data-no-swup>mixité choisie°</a> est une pratique consistant à organiser des
rassemblements réservés aux personnes appartenant à un ou
plusieurs groupes sociaux opprimés ou discriminés, en excluant la
participation de personnes appartenant à d'autres groupes considérés
comme potentiellement discriminants (ou oppressifs), afin de ne pas
reproduire les schémas de domination sociale. La pratique militante est
utilisée par certains groupes se réclamant de divers courants, notamment
dans le féminisme, l'antiracisme, les mouvements LGBTQIA+, les mouvements
de minorités de genre ou de personnes en situation de handicap.*\]

 

Quand on se retrouve en mixité choisie, tout d'un coup, on crée de la
place.

L'espace qui se dégage, et qui est à prendre, s'étire et tout devient
large.

 

{{%centre%}}
s   p   a   c   i   e   u   x
{{%/centre%}}

 

Dans mon expérience, il y a parfois des silences.

\[   \]

<span style="margin-left: 2em;"></span>\[   \]

<span style="margin-left: 4em;"></span>\[   \]

Et ces silences,

<span style="margin-left: 6em;"></span>\[   \]

<span style="margin-left: 8em;"></span>\[   \]

c'est du vide.

<span style="margin-left: 10em;"></span>\[   \]

Et le vide, ça peut se combler,

le vide c'est du possible,

c'est de la place à occuper,

c'est comme une friche,

c'est comme une brèche.

 

Parfois, il n'y a pas de silence,

il y a de l'euphorie,

celle qu'on ne s'autorise pas,

partout,

tout le temps.

 

C'est l'excitation d'exister ensemble.

C'est la joie du refus :

le refus d'accepter,

en tout temps,

la cohabitation avec les discriminations systémiques.

 

Parfois, il y a de la magie,

les choses deviennent limpides,

évidentes,

elles coulent de source,

alors qu'elles semblaient

si difficiles à exprimer

et à expliquer

ailleurs,

dans les autres espaces-temps

si étriqués.

 

Parfois, c'est intense,

et on pleure

les plus chaudes

et les plus réconfortantes

 

l

a

r

m

e

s

de nos glandes

<div><span aria-hidden="true" style="visibility: hidden;">de nos glandes</span>l</div>

<div><span aria-hidden="true" style="visibility: hidden;">de nos glandes</span>a</div>

<div><span aria-hidden="true" style="visibility: hidden;">de nos glandes</span>c</div>

<div><span aria-hidden="true" style="visibility: hidden;">de nos glandes</span>r</div>

<div><span aria-hidden="true" style="visibility: hidden;">de nos glandes</span>y</div>

<div><span aria-hidden="true" style="visibility: hidden;">de nos glandes</span>m</div>

<div><span aria-hidden="true" style="visibility: hidden;">de nos glandes</span>a</div>

<div><span aria-hidden="true" style="visibility: hidden;">de nos glandes</span>l</div>

<div><span aria-hidden="true" style="visibility: hidden;">de nos glandes</span>e</div>

<div><span aria-hidden="true" style="visibility: hidden;">de nos glandes</span>s</div>

<div><span aria-hidden="true" style="visibility: hidden;">de nos glandes</span>abîmées.</div>

 

La mixité choisie,

c'est pas un projet de société,

c'est un outil,

c'est une stratégie,

c'est du repos.

 

Se retrouver en mixité choisie,

c'est aussi être toujours conscienxtes de sa propre <a href="/glossaire/#socialisation-socialiséex" class="glossary-link" target="_blank" rel="noopener" data-no-swup>socialisation°</a>.

On est là pour socialiser différemment[^72],

pour parler avec des nouveaux mots,

et avec des nouvelles intonations,

pour reconfigurer les relations humaines,

pour construire les structures sociales de lendemains meilleurs,

pour créer des stratégies de soin,

pour se regarder dans les yeux avec la joie de se découvrir

ou, plutôt, avec la joie de se <em>re-</em>découvrir,

pour construire des réparations,

pour apprendre à nous aimer, parce que souvent,

on nous a appris à nous méfier les unexs des autres,

à nous trouver moins bien,

moins cools,

moins fortexs que les autres.

 

Parfois, on est mal à l'aise.

Parfois, on se sent faibles.

C'est difficile de l'admettre.

 

C'est difficile d'admettre,

que dans un groupe de 15 personnes,

quand il n'y a pas de mecs <a href="/glossaire/#hommes-cis--mecs-cis--cisgenres" class="glossary-link" target="_blank" rel="noopener" data-no-swup>cis°</a>,

on manque de certaines compétences,

parce qu'on ne nous les a pas transmises,

on nous les a refusées.

 

C'est difficile d'admettre,

que dans un groupe de 15 personnes,

quand il n'y a pas de blanchexs,

on manque de certaines ressources,

parce qu'on ne nous y a pas donné accès,

parce qu'on nous les a volées.

 

Mais on invente des solutions,

on apprend,

l'écologie des moyens

et surtout,

on apprend,

on reprend,

on cherche,

on vole.

 

Parfois, on organise

des ripostes de rêve,

on passe à l'action.

C'est dommage,

mais ça va plus vite,

en mixité choisie.

Alors, c'est la joie de CRIER,

 

{{%centre%}}
<span style="word-break: break-all;">dagirdirectementsansintermédiairessansdemandersansexpliquerdagirvite</span>
{{%/centre%}}

 

{{%centre%}}
C'EST

{{%cadre%}}
L'AUTO
{{%/cadre%}}

ÉMANCIPATION
{{%/centre%}}

 

{{%centre%}}
C'EST PAR NOUS

ET C'EST POUR NOUS
{{%/centre%}}

 

{{%centre%}}
C'EST UNE ARME

GUERRIÈRE

ET AMOUREUSE
{{%/centre%}}

[^72]: Sur des manières d'organiser alternativement un collectif : *Réapprendre à s'organiser* \[n^o^ 21\].
