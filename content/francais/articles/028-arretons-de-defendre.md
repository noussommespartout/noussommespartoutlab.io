---
author: "Anonyme"
title: "Arrêtons de \"défendre\""
subtitle: "Quelques réflexions depuis des zones occupées"
info: "Texte rédigé pour le recueil"
datepub: "décembre 2020"
categories: ["écologie", "autogestion, expérimentations collectives"]
tags: ["absrude, absurdité", "courage", "doute", "détruire, destruction", "fatigue", "force", "impossible", "logique", "machine", "permanence", "pouvoir", "sabot, saboter, sabotage"]
langtxt: ["fr", "en", "it"]
zone: "Suisse romande"
num: "28"
quote: "nous décidons de la temporalité du conflit"
---

Dans nos cultures militantes, nous avons intériorisé que notre rôle
était de *défendre*. Notre vocabulaire ne laisse aucun doute : "Zone à
Défendre", "Protection de la nature", "Défense de l'environnement".
C'est la logique même de notre langage et donc forcément celle de notre
réflexion, de notre action. Qu'y a-t-il de mal à ça, après tout, il
s'agit bien d'empêcher que le système capitaliste emporte le vivant dans
son autodestruction, non ? En réalité, ce n'est pas si évident. À mes
yeux, notre objectif est plus ambitieux encore. Nous devons récupérer ce
que ce système a détruit pour reconstruire autre chose. Nous devons
mener l'offensive contre cette énorme machine et saboter son potentiel
de destruction. Nous devons quitter la posture défensive et passer à
l'attaque. En ne faisant que défendre ce à quoi nous tenons, nous nous
vouons à l'inefficacité.

## Vivre la défense

J'ai beaucoup d'admiration pour les personnes qui mettent en place des
ZAD et des projets similaires, pour leur courage et leur dévouement. Je
suis aussi très reconnaissant de tout ce que j'ai vécu dans ces lieux,
de toutes les expérimentations sociales qui y sont réalisées. Les ZAD
sont de bons instruments de communication et de sensibilisation, des
laboratoires d'idées et de pratiques[^73]. Elles montrent que d'autres
idéaux existent, mais elles relèvent d'une stratégie défensive limitée
qui ne doit pas nous suffire.

C'est une illusion de croire qu'il suffit de vivre pour défendre. Vivre,
c'est reposant, la défense, c'est épuisant. J'ai vécu dans une ZAD, je
me souviens que l'entreprise à laquelle nous nous opposions venait nous
voir, qu'elle visitait les lieux. Dans ces moments, ce que tu ressens
est vraiment paradoxal, parce que tu accueilles ton ennemi sur ton lieu
de vie, en forçant le sourire, pour la forme, pour l'image. Quand on y
pense, c'est absurde que nos adversaires puissent se promener librement
sur nos lieux de vie. Cela veut dire que notre principal ennemi sait où
nous trouver et ça, ça change tout. Il peut observer notre organisation,
il peut nous attaquer quand il le souhaite.

J'ai vécu dans des squats, qui sont aussi des lieux à défendre. Je sais
ce que ça fait de recevoir un avis d'évacuation, un bout de papier
administratif qui t'explique que ta maison ne va plus être ta
maison[^74] : que vivre dans le lieu, lui *donner vie*, tout ça n'a
absolument aucune valeur aux yeux de la société, ni des keufs ni des
tribunaux. Et on sait bien comment ça se passe. Le plus souvent, on
essaie de prévenir comme on peut d'autres militanxtes qui vivent non
loin, dans l'espoir qu'iels se mobilisent et que leur réaction sera
rapide. Malheureusement, le temps et l'incertitude jouent contre nous.
La police intervient souvent à cinq heures du matin, après plusieurs
journées glaciales ou pendant les fêtes, pour être bien sûre de
s'attaquer à des personnes peu nombreuses, fatiguées ou surprises. Dans
la plupart des cas, personne ne vient aider à défendre. C'est tout à
fait normal. D'abord, je comprends bien que la plupart des personnes ne
peuvent pas prendre de gros risques juridiques pour défendre des maisons
qu'iels n'habitent pas. Mais surtout, la police mène complètement le
rapport de pouvoir : quand tu reçois l'avis d'évacuation, tu ne sais pas
si ça va tomber dans deux heures, dans deux jours ou dans deux mois. À
partir de là, tu vis dans l'urgence et dans l'angoisse. Tu n'arrêtes
plus de faire des hypothèses sur le déroulement de l'expulsion, sur les
scénarios de défense, souvent irréalistes. Les moments d'incertitude
s'enchaînent, l'incertitude devient stress, puis angoisse, puis
insomnie. Souvent, ces situations créent des conflits et des tensions
dans le collectif. Tout le monde fatigue. Comment rester mobiliséexs
aussi longtemps et dans un tel état d'incertitude, sans s'épuiser ?

À partir de maintenant, je vais remettre en question notre inefficacité et
laisser peu de place à mes émotions. Ne voyez pas en moi un être
insensible. Je vis le ventre noué à l'idée de ce que l'humanité
détruit : cette rivière entourée de verdure vouée à devenir un parking
ou cette forêt vivante et mystérieuse abattue parce que située sur un
gisement de lithium. C'est parce que ces défaites sont aussi épuisantes
que démoralisantes que je mets en doute l'efficacité de ma lutte.

## Faiblesses d'une stratégie défensive

### perte de la mobilité

Défendre un squat ou protéger une ZAD nous rend facilement localisables,
nous condense sur un espace restreint. Si la police ou un groupe ennemi
souhaite nous attaquer, il sait où nous trouver. Pour pêcher d'un coup
l'ensemble des militanxtes de la région, rien de mieux qu'une descente
dans la ZAD la plus proche. L'infiltration est aussi facilitée : avec
une coupe longue et quelques tatouages, la vigilance militante
s'effondre vite, sans compter que la vie de défenseureuxse est trop
remplie pour être toujours sur ses gardes.

### perte de l'initiative et de la surprise

C'est peut-être un des points les plus stressants durant une occupation :
une fois que nous sommes bien installéexs dans un lieu, l'ennemi est
libre de choisir le moment idéal pour attaquer. C'est tout le bénéfice
qu'il y a à avoir l'initiative : choisir les conditions les plus
favorables pour soi et les plus défavorables pour celleux qui défendent.
Avoir l'initiative permet aussi de planifier à l'avance son attaque,
avec des moyens choisis et un plan déterminé. À l'inverse, ne pas avoir
l'initiative signifie rester dans l'attente et réagir au plan adverse,
en tentant de l'anticiper au mieux.

### rester mobiliséexs dans la durée

Défendre une position exige qu'on s'y maintienne physiquement,
longtemps. C'est même une mesure du succès : combien de temps avons-nous
tenu ? Un mois ? Deux ans ? Mais l'inégalité est profonde. Celleux qui
défendent doivent rester mobiliséexs tous les jours jusqu'à l'expulsion
(perdant un temps considérable, peinant à se concentrer sur d'autres
tâches). La police, pour sa part, ne sera mobilisée le plus souvent
qu'une journée, journée qui lui suffira à déloger les militanxtes. Dans
ce déséquilibre des forces, la police sera toujours disponible et
organisée pour s'attaquer à de nombreux lieux, puisque c'est elle qui
mène le rapport de force et choisit la temporalité du conflit. En
revanche, nous serons confrontéexs au choix difficile des lieux et des
causes qui méritent d'être défendus, en abandonnant obligatoirement
d'autres par manque de temps.

### faire des choix

Cela est aussi vrai sur un plan plus global, à l'échelle d'un pays par
exemple. Nous ne sommes pas assez nombreuxses pour défendre tout ce qui
devrait être défendu. Les militanxtes étant limitéexs par leur nombre,
nous devons choisir nos luttes : nous ne pouvons pas nous opposer à
chaque nouvelle artificialisation des sols, à chaque nouveau grand
projet inutile. Nous choisissons donc toujours les plus importants, ceux
qui ont le plus de valeur symbolique, notamment parce que ce sont ceux
qui sont susceptibles de mobiliser le plus de personnes. Comment choisir
entre cette prairie sur le point d'être ravagée ou bien cette forêt sur
le point d'être abattue ? J'ai déjà vécu ce genre de choix impossible,
ils sont particulièrement durs à vivre émotionnellement. Avec cette
stratégie de pure défense, pour chaque occupation, des dizaines de
projets sont menés à bien sans être inquiétés, sans être attaqués. En
Suisse, nous cédons 2 700 mètres carrés *par heure* aux constructions de
logements et au bétonnage. Autant de luttes qui ne sont pas menées,
faute de ressources.

### l'absence de défaite n'est pas une victoire

Le mieux que l'on puisse espérer en défendant un lieu, c'est qu'il ne
soit pas détruit ou en tout cas moins détruit que ce que prévoyait
l'État. Ce n'est pas une victoire, c'est une absence de défaite.
Geoffroy de Lagasnerie écrit à ce sujet : "Nous avons résisté à une
offensive, mais nous n'avons pas lancé notre propre offensive. Et alors,
en appelant cette situation une “victoire”, nous participons à une sorte
de transmutation des valeurs : nous convertissons psychologiquement
l'ordre présent comme un ordre voulu, souhaité (nous sommes heureuxses
de l'avoir conservé) et donc nous avons régressé". Souvenons-nous de
notre dernière victoire : Notre-Dame-des-Landes. Avons-nous détruit des
aéroports ? Avons-nous réduit le nombre d'avions qui atterrissent en
France ? Pas du tout. Alors qu'avons-nous gagné ? Seulement que
l'aéroport ne soit pas construit. En plus, vinci, l'entreprise chargée
de la construction, a été dédommagée et des projets alternatifs sont
déjà en cours, notamment l'agrandissement des aéroports existants. Même
si cela m'attriste profondément, Notre-Dame-des-Landes n'a rien d'une
victoire, c'est à peine une absence de défaite, qui nous a coûté
beaucoup de temps et de militanxtes.

### Pour une "riposte" de l'environnement

En acceptant une position défensive, nous inversons ce que devrait être
le rapport de force. Ce n'est pas nous, mais le système qui devrait
rester statique, ses infrastructures les plus critiques (mines,
raffineries, réseaux de transport et de communication) ne peuvent pas
être déplacés. Si nous attaquions, c'est elleux qui seraient en
position défensive. Nous pourrions choisir nos cibles en conscience de
nos forces et de nos faiblesses. Nous pourrions être celleux qui
décident du moment le plus propice pour attaquer, de l'endroit, de
l'heure et de la méthode qui surprendront le plus. C'est le système qui
devrait avoir à choisir quelles positions défendre et quelles positions
sacrifier. Il ne me reste finalement que cette idée : nous pourrions
être plus efficaces, nous pourrions gagner, nous pourrions recevoir plus
souvent des bonnes nouvelles.

Nous avons besoin de vraies victoires, et ces victoires nous ne les
obtiendrons qu'en passant à l'offensive. Passer à l'offensive, par
exemple en s'organisant en petits groupes pour aller saboter des cibles
précises, revient à contrer les failles de la stratégie défensive.
D'abord, nous décidons de la temporalité du conflit, et c'est désormais
aux entreprises <a href="/glossaire/#écocide--écocidaire" class="glossary-link" target="_blank" rel="noopener" data-no-swup>écocidaires°</a> de s'inquiéter, de dépenser du temps et de
l'argent pour se défendre en permanence, sans savoir jamais quand nous
attaquerons. Ensuite, nous restons mobiles : en prenant toutes les
précautions nécessaires, nous pouvons nous déplacer et déplacer le
conflit pour qu'il reste le plus imprévisible possible. Aussi, nous
pouvons faire l'ensemble des choix offensifs, et c'est désormais au
système écocidaire de décider ce qu'il peut défendre en permanence, et
ce qu'il est obligé de laisser par moment sans surveillance --- en tout
cas si l'on commence à devenir vraiment nombreuxses. Et la durée nous
épuise beaucoup moins, puisque nous avons tout le temps nécessaire pour
nous régénérer entre deux actions, dont la temporalité nous appartient.
Chacunex peut reprendre le conflit quand iel est à nouveau prêxte et en
forme. Enfin, nous avons toujours l'initiative et nous devenons la
surprise même. Nous pouvons toujours choisir les conditions les plus
favorables pour nous et les plus défavorables pour celleux qui
défendent. Nous les forçons à attendre, à ne faire que réagir, à
*défendre*.

[^73]: *Là-haut sur la colline* \[n^o^ 51\] relate quelques-unes de ces pratiques et *ZAB 2028* \[n^o^ 43\] spécule sur le futur qu'elles portent en elles.

[^74]: *Vous détruisez une Spyre, on en reconstruira plein* \[n^o^ 18\] constitue une réponse à ces avis d'évacuation.
