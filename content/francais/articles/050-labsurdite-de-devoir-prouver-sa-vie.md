---
author: "Anonyme"
title: "L'absurdité de devoir prouver sa vie"
subtitle: "Des difficultés dans la lutte pour l'asile"
info: "Entretien retranscrit"
datepub: "19 septembre 2020"
categories: ["luttes migratoires", "combats institutionnels", "antiracismes"]
tags: ["confiance", "dublin", "feu", "force", "foyer", "gueule", "impossible", "limite", "parole", "pouvoir", "prix", "rage", "théâtre"]
langtxt: ["fr", "it", "en"]
zone: "Suisse romande"
num: "50"
quote: "on est tout le temps en train de bricoler, à la marge"
---

Nous, on est un collectif qui lutte dans le domaine de l'asile[^148].
On essaie de soutenir des personnes qui sont en cours de procédure pour
rester en Suisse ou hors procédure. Dans un cadre institutionnel, quand
on prend la parole pour défendre les personnes qui sont dans des
situations difficiles, tout est à la marge, à la limite. Il faut être
très pertinenxte, très compétenxte.

Une des choses qui nous aide, c'est d'avoir un réseau de contacts
privilégiés. Des médecins de confiance par exemple, qui nous donnent
certaines informations utiles dans le rapport de force avec les
autorités. On a aussi des taupes dans les institutions, dans les foyers,
dans les services de l'État, dans les hôpitaux, mais pas beaucoup,
souvent elles restent pas longtemps ; c'est compliqué, voire impossible,
de travailler dans ces institutions-là quand t'as des convictions.

On est tout le temps en train de bricoler, à la marge. On est tout le
temps en train d'utiliser les petits espaces que les institutions nous
laissent. Pour donner un exemple concret : l'année passée, une jeune
femme de 16 ans est arrivée en Suisse après être passée par l'Italie.
Son parcours est douloureux, elle a été victime de traite. C'est un cas
typique des règlements de <a href="/glossaire/#dublin-réglementation-de" class="glossary-link" target="_blank" rel="noopener" data-no-swup>Dublin°</a> : la Suisse refuse d'entrer en matière
sur la procédure d'asile, parce qu'elle considère que c'est l'Italie qui
doit prendre en charge son dossier. En plus, les autorités avaient
décidé qu'elle n'était pas mineure. Pour l'aider à rester en Suisse, on
a travaillé avec un centre zurichois spécialisé dans les cas de traite.
Elle a pu se faire accompagner et ça nous a permis de "prouver"
qu'elle a effectivement eu un parcours traumatique. C'est tellement
absurde que les institutions mettent les personnes dans l'obligation de
devoir "prouver" leur vie. Ça en dit long sur l'humanité qui
s'installe quand on laisse la gestion à une idéologie purement
*gestionnaire*.

L'ORS, l'entreprise qui gère les foyers de réquéranxtes, ne se donne
aucune peine pour ce genre de cas. Iels travaillent avec des
psychologues avec qui iels ont un accord pour avoir les prix les plus
bas. Le rapport médical qu'on a pu obtenir, a pu "prouver" qu'il
s'agissait d'une personne "vulnérable". Si tu réussis à "prouver" ta
vulnérabilité, avec des certificats, des rapports, des expertises, alors
là, parfois, les autorités font preuve d'un peu moins de brutalité. Et
dans cette affaire, les autorités ont quand même insisté pour faire un
<a href="/glossaire/#test-osseux" class="glossary-link" target="_blank" rel="noopener" data-no-swup>test osseux°</a>. Iels essayaient de "prouver" qu'elle n'était pas
mineure, alors que ce genre de tests est complètement contesté par la
Société Suisse des Pédiatres, mais aussi plus largement au niveau
européen. La marge de précision de ces tests osseux est d'environ deux
ans : ça ne veut donc strictement rien dire. Alors iels ont fait le test
et le résultat disait qu'elle avait entre 16 et 18 ans. Après tout ça,
elle a enfin été reconnue comme personne "vulnérable" et elle a pu
commencer une procédure d'asile normale. Maintenant, son quotidien a
changé. Elle fait du théâtre, du sport, elle a le droit de socialiser.
Elle a pu sortir du foyer pour aller vivre chez une famille.

Quand on milite dans un collectif actif sur les questions d'asile, on
est toujours entre deux feux, entre l'accompagnement individuel des
personnes et le travail politique. Pour nous, militer sur les deux
niveaux à la fois a pleinement du sens. Même si c'est un défi, parce que
le travail d'accompagnement avec des personnes qui ne vont vraiment pas
bien est toujours prioritaire, et que ça nous laisse peu de temps, vu
nos faibles ressources, pour réfléchir activement aux actions plus
systémiques.

Et c'est vrai que les choses qu'on a vraiment réussi à améliorer se
situent plutôt à une échelle individuelle qu'à une échelle politique. On
a besoin de voir que ce qu'on fait a un sens, besoin de voir qu'on
arrive quand même à obtenir des petites victoires, même si c'est pas
face au système dans son ensemble. Malgré tout, plusieurs personnes ont
réussi à obtenir, pas forcément un permis stable, mais juste la
possibilité d'entrer en procédure d'asile ou une admission
provisoire. Personnellement, je suis active sur les questions d'asile
depuis à peu près 30 ans. J'ai pu voir à quel point la situation s'est
dégradée. Aujourd'hui, on se réjouit si quelqu'unex arrive à obtenir un
permis N (permis de requérant d'asile). Alors qu'avant, on se
réjouissait quand quelqu'unex recevait un <a href="/glossaire/#permis-de-réfugiéex" class="glossary-link" target="_blank" rel="noopener" data-no-swup>permis de réfugiéex°</a>. C'est
choquant de se dire que le plus souvent, on se bat pour que les
personnes puissent intégrer une procédure d'asile normale et même pas
pour qu'elles puissent vraiment rester à la fin de la procédure, alors
que c'est là que tout commence normalement. Aujourd'hui, du côté de
l'État, il y a une généralisation du refus d'entrer en matière qui est
flagrante. Mais bon, c'est souvent les militanxtes de la base qui font
bouger les communes, qui font bouger les cantons, qui font bouger les
parlementaires.

Je voudrais finir en disant que le problème du militantisme, c'est
l'usure. C'est l'arme des autorités. J'aime bien la phrase de Coluche
qui dit "la dictature c'est ferme ta gueule, la démocratie c'est cause
toujours". Pour moi c'est exactement ce qu'il se passe. Quand on va
voir les politiques, les autorités, on arrive, on est prêxtes, on a des
documents, des rapports, des chiffres ; iels nous accueillent, on
discute, les médias en parlent. Mais à la fin, il ne se passe absolument
rien. Alors on se dit, on va continuer. Iels nous laissent faire nos
manifs, iels accueillent nos pétitions. Puis rien. Rester accrochéex à un
collectif quand tu n'as pas de réponse, c'est très difficile. Au niveau
individuel, on a des victoires, on permet à des personnes de rester,
humainement, c'est important, mais c'est pas assez. Malgré tout, j'ai
l'impression qu'on force les autorités à ne pas se contenter de leur
discours répressif et défensif, tu vois, ce discours hypocrite de la
Suisse comme "pays humanitaire", comme "terre d'accueil". J'ai
l'impression que sans notre pression ce serait encore pire. La situation
des personnes dans le domaine de l'asile et la façon dont elles sont
maltraitées par les institutions est invisibilisée au niveau politique,
médiatique et même au niveau sociétal. Sans la dimension humaine et les
victoires en accompagnement individuel, ce serait très difficile pour
moi de militer. J'ai vu des renvois en direct, j'ai accompagné des
personnes à la police qui ont été embarquées devant mes yeux. Et c'est
fini, on ne peut plus rien faire. Parfois on arrive à les faire revenir.
C'est pas toujours possible malheureusement, mais on avance. On doit
rester humble pour pouvoir maintenir le feu du militantisme. Pour ne pas
trop déprimer, il faut savourer chaque petite avancée, surtout au niveau
politique. Il faut garder la rage, la pugnacité, il ne faut jamais
lâcher, parce que c'est la stratégie de l'État : nous avoir à l'usure.

[^148]: *L'absurdité des amendes qui permettent de socialiser un peu* \[n^o^ 34\] a été écrit par une membre du même collectif et évoque un autre aspect des rencontres avec les autorités.

