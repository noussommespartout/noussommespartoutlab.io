---
author: "Elena & Rina"
title: "Lendemains"
subtitle: "Vivre la ville postmanif"
info: "Texte rédigé pour le recueil"
datepub: "septembre 2020"
categories: ["manifestation, émeute", "relation à la militance"]
tags: ["amour, love, amoureuxse", "corps", "crier", "eau", "empathie", "force", "gare", "horde", "mcdo", "rue", "thé, tisane", "usine", "voix"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "3"
quote: "hanter tous les murs"
---

Aujourd'hui, la routine redémarre, les voitures circulent, on marche sur
les zones désignées à cet effet.

Stop au rouge, top départ au vert.

Rester sur les bandes jaunes, parallèles.

Désamorcer tout *eye contact*.

Tracer sa propre route, dans une ville-usine, fonctionnelle,
ergonomique.

C'est bizarre. J'ai l'impression d'être à l'étranger ou dans un rêve, je
ne reconnais pas tout à fait "ma" ville. La sensation est presque
physique. Je me sens seule. Je comprends vite pourquoi.

Aujourd'hui, à la Riponne, mon corps entier est surpris de tracer une
diagonale toute droite, la diagonale de ce trajet quotidien qui mène au
taf. Une diagonale efficace : le moins de pas possible, le plus vite
possible. Quelque chose m'incite à prendre un autre chemin, plus
chaotique. Mais seule, le chaos n'a plus de sens.

En quittant la place, je vois un pochoir violet, sous un banc, seul lui
aussi, ça me rend un peu triste, mais ça m'apaise. Et puis je me
retrouve à la gare, là où hier, j'ai fait partie d'une bande, d'une
horde, d'un essaim d'abeilles sans reine : une des plus belles manifs de
toute ma vie, la première de cette envergure. La configuration de
l'espace, l'organisation de la circulation, tout est transfiguré. Il y a
quelques heures, la rue était à nous, la gare était à nous, toute la
ville était à nous. Repasser par là, pour aller bosser, c'est une petite
expropriation, c'est des montagnes russes dans un parc d'attractions
bien niqué.

Depuis ce jour-là, ça m'est arrivé plein de fois. La cartographie
mentale de la ville où j'habite mute. Chaque acte de militance
transforme mon rapport à certaines rues, à certaines devantures de
magasins, à certains carrefours. À chaque fois qu'au milieu d'une horde,
j'ai repris un morceau de ville, je l'ai perdu, alors c'est paradoxal,
mais plus la ville m'appartient, plus la dépossession du lendemain est
difficile.

Tout espace qui a été, pendant quelques heures, propriété collective,
autrement dit, espace réellement public, fait marche arrière et
redevient morcelé : quelques mètres carrés au starbucks, quelques mètres
carrés au mcdo, quelques mètres carrés à la coop, quelques mètres carrés
à h&m, quelques mètres carrés à une entreprise dont on ne connaît pas le
nom qui spécule sur les matières premières et tout le reste à l'État,
avec sa surveillance, ses barrières, ses sens interdits, ses espaces
policés, réservés à une seule partie de la population, celle qui est
"en règle" et qui attend, au sein d'une démocratie cadrée par les
lobbies qui défendent les mètres carrés privés cités ci-dessus,
d'obtenir plus de droits. J'ai envie de dire : donner c'est donner,
reprendre c'est voler.

Et puis, le jour d'avant, on était une masse avec un but commun, touxtes
ensemble, on chantait les mêmes slogans, des slogans qui réclament des
droits qui ne te concernent pas forcément, qui exigent une société
nouvelle pour touxtes. Le collectif, ça dépasse ta personne, tes
intérêts singuliers. Dans une telle configuration sociale et politique,
tu gagnes des libertés. Tu peux taguer sur les vitrines des banques qui
réclament, elles, les droits des entreprises privées, le droit à
l'<a href="/glossaire/#écocide--écocidaire" class="glossary-link" target="_blank" rel="noopener" data-no-swup>écocide°</a>, à la <a href="/glossaire/#précariat" class="glossary-link" target="_blank" rel="noopener" data-no-swup>précarisation°</a> impunie ; tu peux bloquer les autoroutes
pour obliger à réfléchir aux violences conjugales et aux violences de
genre.

Les lendemains, tu te retrouves à marcher aux heures de pointe, au
milieu d'une masse d'individus isolés qui se chargent de leurs petites
affaires. Une masse de personnes qui, au final, se contrôlent mutuellement :
tu ne peux plus crier, taguer, obliger les multimilliardaires à
partager, à cesser leurs génocides commerciaux.

Quand on fait le compte, on n'est pas beaucoup à ressentir ces
sensations, parce qu'on n'est pas beaucoup à crier ou à ouvrir des
squats avec les personnes sans domicile fixe. Alors un écart se creuse
entre ton équipe et le reste du monde qui te paraît si incompréhensible
et insensé. Tu sais que ta représentation de l'espace est toute
particulière. Tu aimerais la partager avec touxtes ces passanxtes, mais
ce n'est pas possible. On nous a sacrément bien dresséexs.

Alors tu continues à marcher. Devant la gare, des potes se sont fait
arrêter, devant ce commissariat, on les a attenduexs pour les accueillir
chaleureusement à leur sortie, sur cette place j'ai dansé les seins à
l'air, derrière ce buisson, j'ai lancé une bonbonne et un pochoir quand
j'ai vu des flics, dans ce bar, j'ai bu un thé chaud pendant un blocage
en plein hiver, dans cette rue j'ai compté les keufs mobilisés pour
prévenir des camarades qui préparaient une action, dans cette autre rue
j'avais plus de voix tellement j'ai crié, ce grand bâtiment vide depuis
des années, on a essayé de l'occuper --- dans les toilettes du dernier
étage, il y a de l'eau courante.

Dans cette ruelle, j'ai pu lire :

*NI*

*UNA*

*MENOS*

en majuscules A4 rouges sur du papier blanc, collé à la hâte sur un mur
qui se trouve à 20 mètres du commissariat. Et ce collage me remplit de
chagrin autant que de force, il me fait monter des larmes enragées pour
toutes les personnes que j'aime et toutes celles que je ne connais pas.

Ça me donne envie de refaire la façade de tous les immeubles de la
ville. De hanter tous les murs jusqu'à ce qu'on ne puisse plus regarder
ailleurs, puisqu'il n'y aura plus d'ailleurs, jusqu'à ce qu'ils ne
s'entendent plus penser, puisque nous serons partout. Dans toutes les
rues, je veux qu'il y ait des actes de déprédation qui soient des œuvres
d'amour. Je rêve d'un acte d'empathie général et d'une insurrection
commune.

