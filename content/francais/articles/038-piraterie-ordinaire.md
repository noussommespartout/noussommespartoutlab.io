---
author: "El."
title: "Piraterie ordinaire"
subtitle: "Quelques idées de sabotage créatif"
info: "Texte rédigé pour le recueil"
datepub: "avril 2020"
categories: ["DIY", "sabotage, action directe"]
tags: ["alcool", "bouffe", "coller", "graines", "internet, web", "logo", "nuit, obscurité", "rire, se marrer, rigoler"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "38"
quote: "vive le vent, vive le vent, vive le vandalisme"
---

Je n'ai jamais vraiment aimé les idéologies. Elles ont l'avantage de
fournir un cadre, de devenir des sortes d'îlots où se repérer un peu,
des espaces qu'on peut explorer avec un peu plus de certitude au milieu
du grand lac des possibles. Les idéologies délimitent ce qu'on peut
dire, à l'intérieur de quel langage les mots peuvent avoir un sens.
Elles aident à parler, à penser, mais quand on les pense jusqu'au bout,
on finit toujours par situer ce qu'on pense, par se dire que les limites
de ce qui est juste, ce sont les limites du langage qu'on se choisit
pour décrire le monde.

Donc je préfère agir localement, user de ma personne pour distraire
l'ordinaire, pour perturber les évidences oppressives, trouver le
contrordre de l'ordre des choses. Agir c'est pas si différent de penser,
parce qu'on est obligéexs de situer l'action. Le sens de l'action est
relatif à ta position, et si ta position c'est d'être un numéro, un
pourcentage qui cherche vaguement un job, un pion dans le grand jeu
funèbre de la marchandise totalitaire, t'as tout intérêt à agir en
pirate. De là, la mise en pratique d'une piraterie de tous les jours,
qui pourrait se partager simplement. Faudrait faire la liste des
microgestes capables d'interrompre le signal[^104], de créer une
perturbation locale du réseau, de percer l'idéologie dominante pour
qu'elle se vide par son fondement. Ce qu'il se passe après, on verra
bien. On l'inventera collectivement. On n'a pas besoin d'aide pour
inventer nos vies. Si l'espace que tu habites te définit comme unex
utilisateurixe, c'est que tu es dominéex par lui, c'est que le grand
filet de la discipline ne te fournit pas les ciseaux qui te
permettraient d'y découper une zone vivable.

Vive le vent, vive le vent, vive le vandalisme. La liste ci-dessous,
c'est en partie des idées piquées ailleurs, en partie des trouvailles.
Mais on ne trouve jamais rien, au mieux, on reproduit des stratégies
dont on ignorait l'existence.

Pour détourner le réseau, tu peux...

...faire un calendrier mensuel qui contient une liste des événements
qui distribuent de la bouffe gratos (vernissages, colloques,
conférences, premières de spectacles, etc.) et la distribuer aux
personnes autour de toi.

...voyager en stop.

...installer des potagers dans les parcs publics.

...dessiner tes propres passages piétons ou tes propres pistes
cyclables avec du scotch et de la peinture.

...imprimer plusieurs visages sur tes t-shirts pour déjouer la
reconnaissance faciale.

...changer les slogans des affiches publicitaires.

...ajouter à l'aide d'un pochoir un logo "handicap" à toutes les
places de parking d'un supermarché du coin.

...utiliser des carabines à air comprimé pour désactiver les caméras de
surveillance de ta ville.

...renvoyer à l'expéditeur les enveloppes dispensées de timbrage qui
se trouvent dans les publicités avec encore plus de pubs.

...avec un simple tournevis et une bâche, transformer un banc public en
abri gratuit pour la nuit.

...faire des jardinières spontanées le long des trottoirs, dans des
vieux pneus, des poubelles, ou des palettes récupérées.

...mettre du colorant alimentaire dans les fontaines publiques, pour
faire rire les enfants et les passanxtes.

...utiliser les caisses automatiques des magasins, et ne scanner que
les deux tiers des produits que tu achètes.

...planter des graines "indestructibles" (comme le kudzu) dans les
parcs publics ou autour des immeubles inoccupés pour accélérer la
revitalisation des villes.

...rendre des voitures de police ou des engins de chantier
inutilisables en enfonçant une pomme de terre dans les pots
d'échappement.

...bloquer une serrure à l'aide d'une seringue remplie de colle époxy
mélangée à un peu d'alcool.

...faire les poubelles des grands magasins et ouvrir un étalage de
produits gratuits sur un trottoir[^105].

...abîmer les codes-barres des trottinettes électriques avec un
tournevis ou du scotch à moquette.

...coller des boutons "skip ad" sur les affiches publicitaires.

...te glisser dans les chantiers la nuit pour réaliser des châteaux ou
des sculptures avec le sable ou le gravier. Elles donneront un sourire
aux ouvrièrexs le lendemain matin.

...donner le code de ton wifi à tes voisinexs.

...poser des vieux tapis sur les barbelés pour les franchir sans
risque.

...enrouler du tissu sur les pics destinés à éloigner les pigeons, pour
qu'iels puissent s'y poser à nouveau.

...installer des balançoires sous les ponts, sous les arbres, sous les
cadres des parkings.

...te protéger des chiens policiers en utilisant du piment de cayenne,
qui bloquera leur odorat pendant un moment (ça ne leur fait aucun mal).

...mélanger des graines, de la terre et de l'argile pour faire des
seed-bombs et reverdir ta ville.

...récupérer des vélos dans les déchetteries ou les ateliers de
réparation, les peindre tous de la même couleur en écrivant "vélos
gratuits" sur leur cadre et les disséminer dans ta ville.

...voler des produits neufs et revenir le lendemain pour te les faire
rembourser. Distribuer l'argent reçu.

...répondre à toutes les offres d'emploi avec des lettres dans
lesquelles tu racontes à quel point tu n'as pas envie de travailler
pour elleux.

...faire des greffons d'arbres fruitiers sur les arbres publics pour
qu'ils commencent à donner des fruits (regarde sur internet les espèces
qui vont bien ensemble).

...laisser des mauvaises notes et des commentaires désobligeants à tous
les hôtels de police de ta ville sur google maps.

...piquer les guirlandes de Noël des magasins de bourges et les
installer dans des endroits surprenants.

[^104]: Dans *L'usure ordinaire* \[n^o^ 6\] on trouvera d'autres microgestes, un peu plus incendiaires parfois, ainsi que quelques conseils automobiles dans *Comment bien rater un contrôle technique?* \[n^o^ 54\].

[^105]: *Le Grand Midi* \[n^o^ 47\] raconte une semaine d'activités d'une cantine autogérée à Lausanne.
