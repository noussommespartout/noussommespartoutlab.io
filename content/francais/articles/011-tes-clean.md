---
author: "Gleisson Juvino"
title: "T'es clean ?"
subtitle: "Journal audio d'une personne vivant avec le VIH et de sa lutte contre toute forme de discrimination"
info: "Journal audio enregistré pour le recueil"
datepub: "avril 2021"
categories: ["sexualités", "transpédégouines, queer", "relation à la militance"]
tags: ["adrénaline", "bienveillance", "déconstruire, déconstruction", "force", "impossible", "internet, web", "outil", "peur", "pouvoir", "procès", "sexe"]
langtxt: ["de", "fr"]
zone: "Suisse romande"
num: "11"
quote: "c'est une partie de moi, ça n'est pas qui je suis"
---

## 1. --- Auto Enregistrement ---

Je m'appelle Gleisson, nous sommes le 11 avril 2021. J'aimerais
commencer en parlant de moi, parce que c'est important pour la suite de
mon témoignage. Je me considère comme une personne <a href="/glossaire/#non-binarité--personnes-non-binaires" class="glossary-link" target="_blank" rel="noopener" data-no-swup>non binaire°</a> et, même
si je pense que je l'ai toujours été, je n'ai trouvé que récemment les
ressources pour embrasser cette part de mon identité[^27].

Je vis avec le VIH depuis bientôt six ans et c'est grâce au travail de
déconstruction des stigmates et de l'autostigmatisation, grâce à une
recherche d'autres types de représentations aussi, que j'ai eu
l'opportunité de prendre conscience de qui je suis. Sans en avoir honte.

Dans ma pratique, la lutte contre la sérophobie est une lutte contre
toutes les formes de discrimination. On ne peut pas vaincre une épidémie
sans essayer de comprendre ce qui éloigne les gens des systèmes de
santé. Mon vécu avec le VIH est, dans ce sens, une ressource qui m'a
sensibiliséx, qui m'a permis d'embrasser des identités. Mon vécu avec le VIH m'a amenéx à
des questionnements, à des prises de conscience sur les privilèges que
je peux avoir --- notamment en tant que personne blanche, qui vit là où
je vis, qui a accès à des ressources comme la thérapie antirétrovirale.
La transmission pédagogique, la proximité avec la communauté, être là,
lutter pour la visibilité : tout ça a commencé pour moi avec le VIH,
tout ça m'a amenéx là où je suis aujourd'hui.

## 2. --- Auto Enregistrement ---

Nous sommes le jeudi 15 avril 2021. J'aimerais commencer par un point
sur ma météo intérieure, en disant que je suis heureuxe et reconnaissanx
de pouvoir mener mes activités et déconstruire les perceptions négatives
de la vie avec le VIH aujourd'hui.

Je m'investis beaucoup dans la lutte contre les idées reçues,
principalement auprès des populations les plus jeunes. Ces groupes, je
les rencontre soit dans mes activités volontaires, soit sur internet.
J'y entends des choses comme "le VIH et le SIDA c'est la même chose,
non ?". Cette confusion[^28] est stigmatisante et discriminatoire :
elle propage des images négatives qui ont été construites et médiatisées
au début de l'épidémie, dans les années 80. La différence entre les deux
est primordiale pour comprendre le principe de traitement comme
prévention : une personne vivant avec le VIH, sous traitement, peut être
indétectable. Cela veut dire qu'une personne peut vivre avec le VIH,
sans le transmettre et sans qu'il évolue vers le SIDA, le stade où le
système immunitaire est trop affaibli pour combattre des infections
mortelles. Comprendre cette distinction est essentiel pour lutter contre
les discriminations et les préjugés, mais aussi pour conduire les
discussions vers la réalité contemporaine de l'épidémie.

J'entends aussi des choses comme : "le SIDA se soigne aujourd'hui". Et
là, j'utilise mon vécu personnel comme ressource pour affirmer que c'est
faux. Oui, je vis bien avec le VIH en dépit de toutes les difficultés
rencontrées sur le plan médical et social, et oui, on peut maîtriser le
VIH grâce aux thérapies rétrovirales, mais ce sont des traitements à
vie, lourds et coûteux. Par contre, ce qu'on peut "soigner", c'est le
manque d'informations autour du VIH et du SIDA. On sait très bien que la
prévention par la peur ne fonctionne plus aujourd'hui et qu'il faut
surtout éviter de stigmatiser encore plus les personnes concernées.
C'est pour ça que je préfère toujours parler en JE et aborder mon propre
vécu.

## 3. --- Auto Enregistrement ---

Nous sommes le samedi 17 avril 2021 et c'est mon troisième
enregistrement. Comme d'habitude, j'aimerais commencer par un point
météo. Je me sens chanceuxe et fierx. Chanceuxe d'avoir des <a href="/glossaire/#alliéex" class="glossary-link" target="_blank" rel="noopener" data-no-swup>alliéexs°</a>,
des personnes sur qui je peux compter dans mon entourage, pour des
moments de *<a href="/glossaire/#care" class="glossary-link" target="_blank" rel="noopener" data-no-swup>care°</a>* et de partage. Chanceuxe de me sentir soutenux dans
ma trajectoire en tant que personne vivant avec le VIH. Je ressens
beaucoup de gratitude envers toutes les personnes qui sont venues avant
moi, depuis le début de l'épidémie, il y a 40 ans. Cette force
communautaire et militante me donne l'énergie de continuer, l'envie de
faire ma part.

Aujourd'hui, je retrouve les raisons pour lesquelles je lutte contre
l'*outing*[^29], pour développer des ressources de bienveillance et
de protection. J'aimerais illustrer ça par quelque chose qui m'est
arrivé récemment. En dépit de ce que je fais publiquement, du fait que
je parle ouvertement de mon vécu avec le VIH sur internet ou lors
d'événements, il y avait encore des personnes dans mon entourage
familial qui n'étaient pas au courant, dont mes grands-parents. Et il se
trouve qu'une personne leur en a parlé. Je suis heureuxe de dire que,
contrairement à une expérience similaire que j'ai vécue il y a quelques
années, ça ne m'a pas autant affectéx cette fois. J'y ai même vu
l'opportunité d'un échange. Je suis fierx de dire que je ne me sens pas
*outéx*, parce que j'ai les ressources nécessaires pour clarifier ces
situations. La nécessité de parler ouvertement et l'urgence de changer
les mots posés sur les réalités m'apparaissent clairement --- utiliser,
par exemple, les termes "personnes vivant avec le VIH" plutôt que
"les contaminéexs, maladexs, souffranxtes, infectéexs".

## 4. --- Auto Enregistrement ---

C'est le 23 avril. Je ne me sens pas très bien émotionnellement depuis
quelques jours. Je m'enregistre quand même, parce que cela fait partie
d'une vie avec le VIH. Les années qui ont suivi le diagnostic, j'étais
en très grande souffrance. La solitude me poussait par moments à une
recherche absolue d'adrénaline, comme un besoin de me sentir vivanx, et
parfois il me devenait impossible d'accorder de l'importance à ma vie ou
à celleux qui m'entourent. Il arrive, comme aujourd'hui, que je ne me
sente pas bien, et j'ai l'impression d'emprunter ce chemin à nouveau.
Sauf que je ne suis plus la personne que j'étais durant les premières
années du diagnostic. Je ne veux plus être là, je sais où ça m'a mené et
c'est pour ça que ça me blesse autant.

## 5. --- Discussion enregistrée pour le recueil ---

Nous sommes le samedi 24 avril 2021, il est 14 h 06. Le point météo du
jour : je me sens plus forxt que ce que j'étais au cours de la semaine
passée et heureuxe de l'être. L'enregistrement d'hier était le plus
personnel, le plus émotionnel. Ça m'a permis de voir que tout ce que
j'ai vécu me touche encore. Ça a été une semaine difficile. J'étais liéx
à quelqu'un pendant sept ans et on est en train de se séparer
légalement. Il y a des hauts et des bas liés à mon vécu avec le VIH. Une
des choses qui m'a beaucoup fait souffrir, c'est que j'ai été *outé* par
mon ex-compagnon. Ça m'a blessé, j'ai vécu des moments terribles, car
cela a pris d'autres dimensions : on se servait de mon statut
sérologique pour m'attaquer personnellement et professionnellement. J'ai
porté plainte, mais l'affaire a été classée sans suite à cause du
contexte dans lequel l'*outing* a eu lieu : une discussion entre amiexs.
C'est très difficile d'arriver à un procès pour ce genre de cas. J'ai
porté plainte, mon ex et ses amiexs ont été convoquéexs au poste de
police, iels ont été entenduexs et ça a été transmis pour évaluation,
puis classé sans suite. À la suite de ça, j'ai continué cette relation
pendant encore trois ans. C'était très difficile parce qu'à ce
moment-là, je vivais avec le VIH depuis deux ans, j'essayais comme tous
les jeunes de 23 ans de trouver des repères dans la vie, personne de ma
famille ne savait, j'en avais pas parlé. L'*outing* et les attaques qui
ont suivi m'ont plongéx dans un profond désespoir, je n'avais pas de
soutien, je n'avais pas de réseau, j'avais seulement ma sœur qui
habitait à Genève, mais elle ne savait pas non plus, je n'avais pas
vraiment d'amiexs, ça ne faisait que quatre ans que j'habitais ici. J'ai
fini par commettre une tentative de suicide. J'ai eu besoin d'être
hospitaliséx, j'ai eu besoin de suivi.

Aujourd'hui, je regarde tout ça et je me dis que, paradoxalement, c'est
une ressource essentielle : j'ai vécu le pire, je sais comment ne pas en
arriver là à nouveau. Même dans les moments où, comme hier, je suis pris
dans un tourbillon de sentiments, j'essaie de prendre du recul et je
vais rechercher dans ce qui m'est arrivé une forme de soutien. Le
militantisme me sert à trouver de la force dans ce genre de moments.

Une part de mon activité consiste à travailler avec des jeunes
adolescenxtes, des étudianxtes du postobligatoire à Genève. J'essaie de
déconstruire, de faire de la prévention sans avoir recours à la peur et
à la culpabilité, en évitant de stigmatiser davantage la sexualité. La
manière dont nous construisons des relations a beaucoup changé en 40 ans
de lutte contre le VIH. Mais le discours des années 80 a toujours un
impact sur les imaginaires contemporains : il y a encore des gens qui
pensent qu'on peut attraper le VIH en partageant un verre ou en
embrassant quelqu'un.

J'ai commencé à faire du volontariat avec le groupe SIDA Genève il y a
deux ans et demi, principalement dans un groupe qui s'appelle "Regards
Croisés" où des personnes vivant avec le VIH reçoivent, dans leurs
locaux, des étudianxtes du postobligatoire pour parler de prévention en
dehors du contexte scolaire. On mène des entretiens, dans un cadre moins
formel, en se servant d'outils comme le photolangage par exemple[^30].
Durant ces rencontres, d'autres thématiques émergent fréquemment. Les
étudianxtes arrivent souvent à faire des parallèles entre la sérophobie
et d'autres formes de discrimination, de genre ou d'origine par exemple.
À part ce travail-là, j'organise des événements, des campagnes de
prévention, des discussions autour de la vie avec le VIH. En ce moment
je suis dans un nouveau projet, un pôle trans qui promeut des espaces en
<a href="/glossaire/#mixité-choisie-ou-non-mixité" class="glossary-link" target="_blank" rel="noopener" data-no-swup>mixité choisie°</a>[^31], des ateliers, des rencontres entre personnes
trans et <a href="/glossaire/#alliéex" class="glossary-link" target="_blank" rel="noopener" data-no-swup>alliéexs°</a>.

Quand le Covid-19 est arrivé, j'étais au Brésil et j'ai dû rentrer en
urgence en Suisse. J'étais enferméx chez moi, je me suis retrouvéx
coupéx de toutes ces activités et c'était dur. Du coup, j'ai commencé à
faire des choses sur le net. J'avais déjà utilisé grindr[^32], de
manière anonyme, comme outil de prévention. Sur l'app, on peut indiquer
son statut sérologique dans son profil. Dans le langage courant, la
question "t'es clean ?" revient souvent. J'ai eu pas mal d'échanges,
mais aussi beaucoup d'attaques directes sur le fait que je parlais
ouvertement de mon statut ou que j'en parlais de manière anonyme. Il y a
une sorte d'aura sexuelle autour des personnes vivant avec le VIH, sur
leurs comportements sexuels. Je recevais des messages avec des
propositions sexuelles très ouvertes, sans protection, comme si le fait
d'avoir le VIH induisait que j'allais tout me permettre. J'étais
assimilé à une personne qui prend des risques.

Depuis cette première expérience, j'essaie d'écrire davantage, j'ai créé
le compte instagram \@goodhivvibesonly pour partager mes expériences. Il
m'a fallu du temps pour me sentir prêxt à en parler publiquement, à
répondre à des questions, même de la part de ma famille ou de mes
amiexs, notamment parce que, dans certains cas, je n'avais pas de
réponse moi-même. Ça m'a pris du temps. Sur grindr, puisque c'est un
site de rencontre, j'ai choisi l'anonymat pour éviter de me faire
*outer*. Sur ces plateformes, comme partout ailleurs, les personnes
vivant avec le VIH ne sont pas toujours très bien traitées. J'ai
commencé anonymement sur instagram pour apprendre, pour voir comment je
réagissais, pour me protéger et explorer en me familiarisant avec
cet outil. Aujourd'hui je ne fais plus de prévention sur grindr, mais
ailleurs. Sur Instagram, je ne suis plus anonyme et ça me rend fierx de
pouvoir parler de façon ouverte. C'est important qu'on le normalise : je
ne vois pas pourquoi ce serait si grave. C'est une partie de
moi, ça n'est pas qui je suis. Je ne vis plus ça comme une punition. Je
me rends compte qu'on n'est pas à l'abri, qu'on peut touxtes prendre des
risques à un moment donné. La charge morale de la sexualité autour du
VIH est très importante, elle induit une culpabilité qu'il faut savoir
remettre en question.

Quand j'ai appris ma séropositivité, je venais d'arriver en Suisse,
j'étais une personne migrante. Je me rends compte de plus en plus que
l'accumulation des discriminations est dangereuse : on sait que le taux
de suicide est plus élevé chez les personnes trans, non binaires, chez
les personnes migrantes et chez les personnes vivant avec le VIH. Le
milieu médical n'est que peu ou pas préparé à la prise en charge de ces
personnes, alors que ces groupes sont précisément les populations clés
dans la prévention. J'ai essayé de ne pas y prêter attention, j'ai
laissé passer des choses parce que j'étais déjà assez fatiguéx, que
j'avais beaucoup de charges et de traitements. Depuis 40 ans de lutte
contre le VIH, on est encore et toujours en train d'essayer d'améliorer
cette prise en charge, alors qu'on sait qui sont les personnes qui
souffrent le plus. Qu'on en soit encore là, quand on voit le peu de
ressources financières qui sont investies mondialement pour la
prévention, c'est choquant.

En fait, on aurait les moyens d'en finir avec l'épidémie du VIH/SIDA :
la PrEP[^33], le préservatif, le traitement comme prévention et le
dépistage. Mais le manque d'informations et de ressources est évidemment
politique. Les populations clés, quand on parle de prévention, sont
aussi celles qui sont le plus marginalisées et discriminées : les
travailleureuxses du sexe, les personnes trans, LGBTQIA+, les
migranxtes, etc. Au-delà des enjeux de santé, la véritable impasse à la
fin de cette épidémie est une question sociopolitique.

[^27]: Pour se familiariser ou s'exercer oralement à l'accord non binaire, voir *À lire à voix haute* \[n^o^ 32\].

[^28]: Le VIH (virus de l'immunodéficience humaine) est le virus responsable du syndrome d'immunodéficience acquise (SIDA).

[^29]: L'*outing* est le fait de révéler à des tiers l'homosexualité, la bisexualité, la transidentité, la non-binarité ou, dans ce cas précis, le statut sérologique d'une personne sans son consentement, voire contre sa volonté.

[^30]: Méthode pédagogique inventée en 1968 qui vise à permettre à un groupe d'exprimer ses représentations sur un thème par le biais d'un outil qui favorise l'expression orale. L'objectif de cette méthode est de faciliter la prise de parole de chaque membre du groupe à partir de sa connaissance, ses attitudes, ses valeurs, sa pratique et son expérience.

[^31]: Pour une perception poétique de la puissance des rencontres en mixité choisie, lire *Quand l'espace s'étire* \[n^o^ 27\].

[^32]: Créée en 2009, grindr est une application de rencontre géolocalisée conçue pour les hommes homosexuels, bisexuels ou bicurieux.

[^33]: La PrEP (prophylaxie préexposition) concerne les personnes séronégatives qui s'exposent à un risque important d'infection. Elle permet d'éviter une transmission du VIH et est reconnue comme un outil efficace de prévention depuis 2014 par l'Organisation Mondiale de la Santé. Les nouveaux diagnostics au VIH ont diminué de 28 % à San Francisco ou de 25 % à Londres, depuis 2014, où la PrEP est disponible. En Suisse, le médicament Truvada est prescrit depuis 2006 dans le cadre de trithérapies visant à contrôler le virus. Comme ce médicament n'est pas enregistré comme traitement préventif, il n'est pas remboursé par l'assurance maladie de base et coûte 900 CHF par mois --- 65 % de plus qu'en France. Aucun générique n'étant autorisé en Suisse, de nombreuses personnes s'en procurent à l'étranger. Le 1^er^ avril 2019, Swissmedic a fait passer de trois mois à un mois la quantité maximale de médicaments pouvant être importés pour usage personnel tels que ceux utilisés dans le cadre de la PrEP. En janvier 2020, une pétition a été remise à Alain Berset, ministre de la santé publique, demandant de supprimer cette limitation.

