---
author: "C."
title: "Un jour, j'ai poussé la porte d'un hangar tout pété"
subtitle: "Militer en perpétuelle remise en question"
info: "Texte rédigé pour le recueil"
datepub: "novembre 2020"
categories: ["autogestion, expérimentations collectives", "relation à la militance"]
tags: ["amour, love, amoureuxse", "bière", "canapé", "cellule", "cicatrice", "cigarette, clope", "corps", "doute", "déconstruire, déconstruction", "garage", "gueule", "intime", "justice", "liberté", "mercredi", "peur", "pouvoir", "rue", "réunion", "sang", "souffle", "violence", "visage"]
langtxt: ["fr"]
zone: "Suisse romande"
num: "26"
quote: "reprendre possession de notre vie collective"
---

La première fois que j'ai poussé la porte de cet espace militant, une
porte de garage grinçante, lourde, usée et pleine de tags, j'ai eu peur.
Je me disais que ce n'était pas mon monde, que c'était un monde
d'artistes qui ont plein de choses à dire alors que moi, je voulais
juste que le monde aille bien. Ma tête était organisée en
compartiments : tout y était bien rangé. Il y avait les artistes,
celleux qui pensent le monde et créent des nouveaux imaginaires, et
nous, les non-artistes, qui gérons le monde plus rationnellement.
J'avais d'autres cases aussi, plein d'autres cases. Une case avec mes
loisirs : quelques soirées dédiées au sport, une autre aux copines et,
ce qu'il me restait, je l'investissais dans mon couple monogame et
hétérosexuel. Les journées de la semaine étaient bien compartimentées
aussi, à chaque jour sa couleur et son émotion. Il y avait la haine du
dimanche, puisqu'il est suivi du lundi, le soulagement du mercredi, du
mi-chemin... le mi-chemin d'un éternel cycle voué à recommencer chaque
semaine. Même mes amiexs étaient compartimentéexs. Celleux de la teuf,
celleux des émotions, celleux de l'intellect. Tout était bien rangé dans
un esprit pourtant bordélique et chaotique, mais tout de même bien
dressé. J'étais vouée à devenir douce, à l'écoute, un peu passive pour
permettre aux autres, à l'autre moitié, de briller à ma place. J'étais
vouée à devenir une personne de l'ombre, du privé, du *<a href="/glossaire/#care" class="glossary-link" target="_blank" rel="noopener" data-no-swup>care°</a>*. Les
choses étaient faciles, car prédéfinies, rangées, lissées, polies.

Et vu que tu n'es pas la seule dont la vie est fragmentée et toute
rangée, ben, ta petite bulle croise les bulles des personnes qui te
ressemblent. Derrière chaque porte que tu pousses, tu rencontres des
personnes qui te sont semblables : ces portes-là ne font pas peur, il
n'y a pas grand-chose qui t'invite à remettre en question ta vie. Alors
tu avances, tu passes d'une case à une autre, d'une cellule à une autre.
Et le vendredi soir, tu racontes à tes amiexs ce qu'il s'est passé dans
les différentes cases de ta vie. T'as l'impression que tu pourrais
trouver beaucoup d'amour pendant ces moments, dans cette case-là, mais
tu dois très vite la refermer, parce que c'est dimanche et le lendemain
c'est lundi.

Ça, c'est jusqu'au jour où tu pousses, avec crainte, une porte nouvelle,
la porte d'un vieux garage tout pété qui donne sur une espèce de hangar
désaffecté. Il fait sombre, il fait froid, mais il y souffle comme un
vent de possibles. Ces possibles ne semblent pas encore écrits,
dessinés, tu ne sais pas quelles formes ils peuvent prendre et c'est
bien pour ça que ce sont des possibles. Dans ce hangar, on t'a invitée
pour casser l'entre-soi artistique, pour casser l'entre-soi blanc et
privilégié. Non pas que tu représentes une autre catégorie que celle de
la blanchité et du privilège, mais une personne semble voir en toi, une
possible aptitude à créer du lien avec les gens. T'es pas vraiment sûre
de ça toi, tu ne sais pas bien ce que tu sais faire ou ce que tu es.
T'as l'impression d'être une sorte de pièce interchangeable dans une
société qui valorise la spécialisation alors que toi, t'es dans le
général, ce que tu sais faire, c'est soigner, accueillir. T'as beau
penser qu'il peut s'agir de qualités, tu doutes, car on t'a bien appris
à douter.

On vient te chercher pour te proposer d'organiser une sorte de "salon
des sans-refuge". Tu ne connais pas les sans-refuge, tu es entourée
de personnes qui ont un refuge, mais tu te lances, tu sais que tu es une
personne d'action, c'est le faire qui te permet de penser. Quand tu
repenses à ce projet, il te semble beau dans sa sincérité, mais si naïf.

Alors bon, tu commences par gratouiller le bord de ta bulle qui semblait
si hermétique. Tu pousses plein de nouvelles portes, toutes grinçantes,
souvent taguées : des portes de squats, d'associations et parfois tu
sors simplement dans la rue. T'as toujours peur, mais tu pousses. Ta
bulle commence à s'effriter et à accueillir, en son sein, la réalité et
le vécu d'autres personnes. T'ignorais tout d'elleux si ce n'est ce que
disent les journaux, la culture populaire et les gens. Les
mondes des sans-refuge et les mondes des avec-refuge ne se croisent
pas. Les espaces et les temporalités ne sont pas les mêmes. Iels sont
les invisibles, tu ignores leur existence, à part de temps en temps ce
croisement de regard dans la rue, un échange plein de préjugés.

Cette fois, tu vas les chercher et tu vas leur dire qu'il y a un vieux
hangar désaffecté, froid, qui pue la cigarette et la vieille bière où
iels peuvent venir partager un moment avec toi, avec le collectif. Tu ne
sais pas vraiment qui est "le collectif", ce n'est pas encore un
concept très courant pour toi. Tu connais la cellule familiale, des
individus, les groupes de potes, mais le collectif, c'est encore très
flou et il n'est pas encore construit dans ce nouveau lieu. Le
"elleux", tu le définis mieux, la catégorie est déjà créée et figée
pour toi, par d'autres. Tu ne sais pas trop ce que tu leur offres. Un
peu de chaleur humaine peut-être ? Tu es certainement encore bien
formatée et tu as une image "humaniste" de toi-même. Humaniste dans le
sens où, tu n'es pas encore féministe par exemple, tu veux l'égalité et
non "la domination des femmes sur les hommes". Alors, pleine de bons
sentiments, tu vas tendre ta main blanche. Tu le fais avec beaucoup de
sincérité, mais ta sincérité ne suffit pas à comprendre tout ce qui se
cache derrière tes préjugés. T'y vas quand même. T'as besoin d'action
pour comprendre. Alors tu pousses de plus en plus souvent la porte de ce
garage. Le hangar se remplit peu à peu. Des mondes différents se
côtoient, partagent un même espace. Se rencontrent-ils vraiment ? Tu le
penses mais tu n'en es pas sûre. Tu sens bien qu'il y a encore un
"elleux" et un "nous". Le "nous" commence d'ailleurs à se définir
petit à petit, réunion après réunion, même si tu ne parles que rarement
en réunion. Tu es principalement entourée d'hommes blancs éduqués dont
la rhétorique t'impressionne et dont la spontanéité ne te laisse pas le
temps d'en placer une. Mais l'ombre te plaît ou du moins te convient.
Elle est confortable. Tu en prends soin. Tu prends soin des relations
qui se tissent. Tu laves aussi, tu ranges, tu t'impliques. Tu te donnes
une raison d'être dans le groupe, dans le collectif. T'essaies de faire
en sorte que chacunex trouve sa place et se sente bien dans cet espace.
En réalité, tu as l'impression de ne rien faire de concret. Tu penses
que c'est un rôle secondaire, car tous les rôles primaires sont pris.

Tu ne peux pas t'empêcher de te mettre à la place des autres. Quand
quelqu'unex pousse la porte, tu sens à son langage corporel, à ses
expressions du visage, si cette personne se sent à l'aise, si elle se
sent à sa place. Si tu y lis le moindre doute, alors tu accoures, tu lui
introduis le lieu, les gens, le fonctionnement, les codes. Tu ne
souhaites à personne de ressentir cette désagréable sensation de ne pas
être à sa place. Tu la ressens chaque jour, entourée d'hommes qui savent
tellement mieux et plus que toi, entourée d'artistes qui ont tellement
de choses à raconter.

Ce n'est que plus tard que tu comprendras que tu fais le travail de
l'ombre, celui qu'on ne valorise pas : ce qu'on attend de toi. C'est un
dû, même si tu le fais avec plaisir et que ça te remplit. Tu te donnes
corps et âme et c'est lorsque tu donnes ton âme que tu te mets en danger
et que la critique devient dure à avaler. Ce n'est pas ton esprit, ta
réflexion qu'on remet en question, mais ta sincérité et la façon dont tu
prends soin des gens.

Un jour, alors que tu cours partout pour organiser, ranger, prendre
soin, on te sort l'épée de la justice, la fameuse épée de la radicalité,
prête à te couper la tête si tu n'es pas dans la justesse, dans la
pureté politique. Tu n'y as pas encore goûté, c'est encore la lune de
miel militante. Tu penses que faire, c'est toujours mieux que de ne pas
faire. Mais tu sens qu'elle se rapproche. Quelques tensions et quelques
concepts sur l'antiracisme te parviennent. Tu sens que les choses ne
sont pas si évidentes, mais tu ne t'attends pas à une sentence aussi
dure. Et voilà que l'épée s'abat sur toi, sur ton collectif. Elle ne
vient pas de l'extérieur, elle vient du centre, de l'intime, d'une
personne avec laquelle tu as pensé, avec laquelle tu t'es construite et
déconstruite. Elle s'abat avec une telle violence que tu ne peux plus
bouger. Une lettre ouverte. On te dit que tu représentes ce qu'il y a de
pire dans le racisme, que sous des airs de personne engagée, tu es la
honte du militantisme, que tu perpétues le colonialisme, que ton être
domine et écrase ce qui ne lui ressemble pas, que tout ce que tu as
fait, donné, aimé, pensé est faux et que tu aurais mieux fait de rester
dans ta bulle. Tu prends ça comme une accusation de ton âme et de ton
cœur. Ton monde s'écroule. Tu avais encore tant à faire aujourd'hui,
tellement de monde à accueillir, mais tu ne peux plus bouger. Plus rien
ne fait sens, les larmes coulent et tu te réfugies sur un canapé avec un
capuchon fixé sur ta tête. Tu as rarement ressenti une blessure aussi
profonde. Tu es incapable de comprendre que ce n'est pas ton être
profond que l'on remet en question, mais ce qu'on a fait de toi, tous
les schémas de domination qui ont construit cet être qu'on exècre dans
les espaces politiques militants. Tu portes en toi toutes les saletés
d'une société oppressive. Tu n'es pas du côté des "bons" comme tu le
pensais, tu es un peu comme tout le monde. Tu ne comprends pas
véritablement le racisme et son aspect systémique : "tu as des amiexs
noirexs", "tu travailles depuis 5 ans en Afrique de l'Ouest", tu ne
peux pas être raciste, ça n'a pas de sens. La plaie est géante, tu veux
tout abandonner, ta bulle était plus douce, plus confortable, on se
rassurait entre privilégiéexs, on menait des combats charitables : la
charité est confortable tant que tu n'as pas saisi qu'elle fait partie
d'un système qui te conforte et qui ne remet pas en question tes
positions sociales. Alors, l'espace d'un instant, tu veux baisser les
bras. Il te semble que les autres aussi. Tu sens que ce collectif
constitué depuis peu est fragile, que la blessure infligée de
l'intérieur est peut-être trop grande pour pouvoir continuer.

Mais pourtant, assez vite, le corps collectif reprend forme, se relève,
une tête après l'autre. Le cœur et le bide sont toujours blessés, mais
la tête commence à comprendre de quoi il s'agit. La tête comprend que le
collectif est constitué d'un groupe blanc privilégié qui fait des grands
discours sur le monde et qu'il y a "elleux". Ce "elleux" est composé
de personnes qu'on continue à appeler "les migranxtes". Iels
fréquentent le lieu, mais iels ne sont pas *dans* le collectif[^70].
Le collectif est constitué de corps identiques, les autres corps n'ont
pas la même place. Les liens sont différents. Les mots de cette lettre
ouverte te reviennent. Ils continuent à résonner fort, mais tu commences
à comprendre ce qu'on veut dire par "déconstruction". Tu comprends que
ce n'est pas un chemin intellectuel. Tu comprends que tu peux lire un
million de livres sur le racisme et le sexisme systémiques et sur tous
les autres types d'oppression que tu n'arrives même pas encore à
imaginer, mais que tant que ça ne te touche pas dans les tripes, c'est
que tu n'as certainement pas commencé ton chemin. Tu captes que se
déconstruire, ce n'est pas faire des grands discours et avoir les bons
mots pour exprimer une pensée : la pensée militante radicale. Tu captes
qu'il y a du vrai dans ce qui t'a été reproché. Tu captes que tu
associes des comportements et des manières de penser à des personnes,
alors que tu ne les connais pas si bien. Tu captes que tu ne te
comportes pas de la même façon avec un corps blanc et avec un corps
racisé. Tu captes que tu n'accordes pas la même valeur aux paroles qui
sortent des bouches blanches et des bouches racisées. Tu te trouves
sale, pleine de préjugés, tout autant que les personnes que tu méprises
politiquement et qui te confortent dans l'idée que tu serais dans la
bonne case. Après un moment d'apathie, c'est le dégoût de toi-même. Tu
ne sais pas quoi en faire. Tu hésites à retourner dans ta bulle. Tu
faisais partie des "bonnes personnes". Mais ta bulle s'est effritée,
tu as été éventrée et les fissures ne sont pas faciles à colmater. Alors
une fois de plus, tu pousses la porte du garage. Tu remets en question toutes
les interactions que tu as, tu apprends un nouveau vocabulaire. Parfois
tu fatigues. Tu pensais avoir trouvé un monde où s'émanciper du carcan
capitaliste, rationaliste, productiviste et tu as parfois l'impression
d'entrer dans un nouveau carcan, lui aussi, plein de règles. Te voilà
concentrée à assimiler des nouveaux codes pas si évidents. Ton style
vestimentaire, tes façons de penser, de parler ne semblent pas toujours
adéquats. Tu as le sentiment de ne pas avoir les bons gestes, de ne pas
agir comme il faudrait. Tu cherches à te corriger comme une bonne élève.
Tu as parfois la sensation d'un monde policé. Tu ne comprends pas tout.
Tu ne te sens pas toujours à ta place. Mais tu ne peux pas pour autant
retourner là où tu étais avant. Tu n'as plus le choix. Tu fous un gros
coup de pied dans ta bulle. Elle se brise. Ça fait mal. En identifiant
tes propres comportements racistes, t'identifies aussi ce que tu as subi
avec ton corps assigné femme. Tu captes pourquoi tu ne te sentais pas
libre. Tu captes les abus, les contraintes et les agressions sexuelles,
les chantages affectifs, les micro-agressions qui te font douter chaque
jour de toi-même. Tes boyaux sont à nouveau écartelés dans tous les
sens. La bile te monte à la gorge, les images de nombreux abus te
reviennent à l'esprit et c'est un nouveau dégoût qui s'empare de toi.
Comment est-il possible que toi, qui te pensais si indépendante et
forte, t'aies pu accepter toutes ces choses, t'aies été incapable de
dire non ? Et si toi tu les as acceptées, qu'en est-il des autres
femmes\* et des autres personnes minorisées qui n'ont peut-être pas ton
caractère, ton éducation, ton filet de sécurité familial ? Là, un chemin
très long s'ouvre devant toi et il te fait peur. Tu vois toutes tes
relations sous un nouveau prisme et tu comprends qu'il sera aussi
difficile de te déconstruire que de mettre fin au <a href="/glossaire/#patriarcat" class="glossary-link" target="_blank" rel="noopener" data-no-swup>patriarcat°</a>...

Et pourtant, tu continues. Tu restes la personne de l'action que tu
étais. Tu agis, tu essaies, tu expérimentes avec toutes les incroyables
personnes qui t'entourent et chaque jour te fait douter un peu plus,
tout en te remplissant d'un nouveau sentiment de plénitude, car avec le
collectif, tu prends les choses en main, tout le monde tente,
collectivement, de se réapproprier la vie. Tu n'es pas sûre d'être dans
le juste, cette lettre ouverte est encore suintante, tu as peur qu'elle
se rouvre, tu ne peux pas totalement te défaire de la sensation qu'on
t'attend au virage pour identifier tes erreurs. Tu as parfois le
sentiment que l'ennemi n'est pas l'ordre dominant, mais toi-même. Alors
tu agis en tremblant. Et parfois, l'épée s'abat sur toi. L'épée de la
pureté politique. Une fois, tu n'as pas su inclure suffisamment de
diversité, une autre fois tu as oublié le *trigger warning*
(avertissement au public), une autre fois tu as materné une personne
aux agissements douteux, une autre fois tu as exclu des gens à l'aide
d'un langage trop militant, une autre fois tu les as exclus avec un
langage pas suffisamment inclusif, une autre fois tu as demandé ce qu'il
fallait faire avec les agresseurs avant d'avoir posé la question des
agresséexs, etc. Et à chaque fois, l'épée revient s'enfoncer un peu
plus, elle rouvre la plaie, mais jamais totalement. Ça saigne
légèrement, mais la cicatrice se refait et se solidifie toujours plus.
Chaque goutte de sang qui sort, c'est un petit pas de plus dans ce long
chemin qui va vers la compréhension de ces schémas oppressifs qui
t'habitent, alors tu regardes la goutte couler, elle te dégoûte encore
un peu, tu en veux à la société de t'avoir inculqué si profondément
cette pensée binaire, hiérarchique, oppressive. Mais tu lui pètes la
gueule. Tu n'as plus le choix, ta bulle n'existe plus, tu lui as
vraiment pété la gueule. Tu te rapproches chaque jour un peu plus des
autres, des autres dans toutes leurs diversités et c'est cette diversité
qui te nourrit. Alors tu es de moins en moins paralysée. Tu acceptes et
tu revendiques de plus en plus les balbutiements collectifs tout en
acceptant ces baffes qui font partie du chemin. Tu regardes avec plus de
tendresse et de patience ce vaste terrain vague où l'on prend le droit
d'hésiter et d'apprendre.

Nous passons notre temps à déléguer aux institutions tous les pans de
nos vies, nous avons été dépossédéexs de notre pouvoir politique, de nos
possibilités d'agir, dépossédéexs du soin que nous devons porter à nos
ainéexs, à nos marginaliséexs, à nos blesséexs. Nous avons été
dépossédéexs du savoir sur nos propres corps, sur le vivant,
dépossédéexs de tout et nous sommes censéexs savoir comment faire pour
reprendre possession de notre vie collective, et comment faire pour bien
l'organiser[^71] ? Non, nous n'avons aucune recette miracle. Mais il
nous faut un lieu miraculeux. Un espace de liberté où il nous est permis
de rêver à nouveau ensemble pour donner corps à ce rêve. Un espace où
nous apprenons à nous sentir légitimes et un espace où nous savons
accompagner les autres dans leurs doutes, leurs craintes, leurs peurs.
Un espace où nous n'avons plus peur de nommer les émotions, les plus
belles comme les plus dures. Un espace où nous arrêtons de nous
comparer. Un espace où personne n'est illégal.

Il n'y a pas de recette miracle, il n'y a surtout rien de figé, il n'y a
qu'un mouvement perpétuel, ouvert et perméable, où le collectif prend la
forme des individus qui le composent. Nous ne voulons pas d'un autre
monde, nous voulons une multitude d'autres mondes. Nous ne voulons pas
que deux collectifs se ressemblent, nous voulons apprendre les unexs des
autres, mais sans appliquer de recette préfabriquée, clés en main. Nous
voulons plein de nouveaux mondes, mouvants, étonnants, créatifs,
bienveillants et tendres. Et nous voulons pousser plein de portes
nouvelles. Même si nous avons des craintes, nous voulons accueillir les
questionnements, les balbutiements, les erreurs, pour que chacunexs
trouve sa place.

C.

[^70]: Voir *Jean Dutoit en lutte* \[n^o^ 13\] pour le récit d'expérience d'une situation proche.

[^71]: Sur des manières d'organiser alternativement un collectif : *Réapprendre à s'organiser* \[n^o^ 21\].

