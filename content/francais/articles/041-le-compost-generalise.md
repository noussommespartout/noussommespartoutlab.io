---
author: "Les composteuses"
title: "Le compost généralisé"
subtitle: "Compost's revolution"
info: "Texte rédigé pour le recueil"
datepub: "décembre 2020"
categories: ["sabotage, action directe", "écoféminismes", "écologie", "manifestation, émeute"]
tags: ["café", "colère", "compost, composter", "créatifvex, créativité", "cul", "eau", "engrais", "force", "graines", "gueule", "outil", "rage", "raisins", "soleil", "stockage", "urbanisme"]
langtxt: ["de", "fr"]
zone: "Suisse romande"
num: "41"
quote: "le monde est un vaste compost"
---

Le monde est un vaste compost.

L'essentiel de notre passé est sous nos pieds, bien composté, à part les
dinosaures qui ont eu la mauvaise idée de se transformer en pétrole,
mais bon, c'est pas de leur faute. Notre présent est prêt à se faire
composter la gueule. Et nous aussi, nous sommes du compost, puisque tout
organisme devient vivant en émergeant du compost. Notre futur, déjà on
verra bien s'il se pointe ou pas. Pour l'instant, c'est un futur
quantique, il est à la fois là, à la fois pas là, ça dépend un peu de ce
qu'on foutra de nos vies. Mais ce qui est sûr, c'est que nous
deviendrons touxtes l'engrais de nouveaux mondes et de futurs organismes
vivants.

Bref, on marche sur du compost, on est du compost, on mange du compost,
on chie du compost. C'est à peu près ce que nous dit le bout de compost
qu'est Donna Haraway, même si elle le dit avec un peu plus d'élégance et
de complexité.

Il y a un temps pour la réflexion et un temps pour le <a href="/glossaire/#zbeul" class="glossary-link" target="_blank" rel="noopener" data-no-swup>zbeul°</a>.

Aujourd'hui, c'est le *mood* zbeul partout.

Alors zbeul partout, compost partout[^111].

Les Gilets jaunes ont déjà exploré le *mood* zbeul-compost : du purin
devant la préfecture de Manosque, du purin et de la poubelle devant les
impôts de Challans et de Limoges, du fumier devant le Centre des
Finances Publiques de Lesparre, du fumier sur un rond-point de
Mirecourt, du fumier à Douai, du fumier en Haute-Vienne, des fruits
pourris à Thuir. Iels ont commencé une vaste entreprise de compostage du
capitalisme.

Quand nous aurons tout composté, nos territoires seront des champs
fertiles, nous entrerons dans l'ère du compost, de l'humus, de la
fertilité et nous accueillerons les futurs enfants du compost : une
nouvelle humanité plus *bio*. Ça devrait parler aux bobos citadinexs. On
peut leur créer des slogans : *be more bio, be more compost*. On peut
leur créer des médailles, des labels. Tant qu'iels se décident à sortir
un peu dans les rues, ça nous va.

## Une solution, révolution

Alors voici un mode d'emploi pour entamer ou continuer l'entreprise de
compostage massif qui marquera un pas supplémentaire sur le chemin d'une
révolution écolo et féministe[^112].

---

***TRIGGER WARNING***

*Aux flics, aux responsables politiques, aux multinationales et aux
banques de notre chère Suisse : ne vous méprenez pas, c'est pas des
conneries, on est en train d'accumuler quantité de caca fertilisant et
de compost, et tout ça nous servira à vous recouvrir. Littéralement.
C'est une action massive que nous préparons avec soin et qui aura lieu
l'année qui vient, ou la suivante, selon le temps de fermentation.
N'oubliez pas que vous êtes des organismes vivants, et que ça vous rend
hautement compostables.*

---

Alors fermièrexs, paysannexs, bobos des villes, bobos des campagnes,
familles nombreuses, colocs étudiantes, squats, maisons de quartier,
soyez touxtes prêxtes à produire du compost en masse lorsqu'on vous
donnera le signal. Aux quartiers, hameaux, villages, créez des composts
collectifs. Ils vous permettront de créer du lien social, d'utiliser du
fertilisant pour vos plantations communautaires, mais aussi d'amasser,
d'amasser et d'amasser, pour la journée internationale du compost qui
s'annonce belle et féconde.

Jour après jour, en cuisinant, remplissons ces grands bacs où finissent
les morceaux de légumes et les épluchures de fruits. Regardons ces gros
tas de végétaux pourrir et continuer à vivre, fermenter et former des
terreaux fertiles. Accumulons patiemment quantité de compost, humons-le
et scrutons-le avec joie. Des graines soigneusement choisies y
pousseront : les graines de la rage et de la colère.

## Pour un compost <a href="/glossaire/#vénèrex" class="glossary-link" target="_blank" rel="noopener" data-no-swup>vénère°</a>, écœurant et répugnant à lancer sur les gardiens de l'ordre capitaliste les plus tenaces, plusieurs recettes

### Le compost dégueu qui fermente trop

- Choisissez un emplacement lumineux, en contact direct avec les rayons
du soleil.
- Pour faire du compost en masse, privilégiez les grands espaces (en
plein air ou dans un lieu clos suffisamment vaste, comme un Parlement).
- Étanchéisez le lieu de stockage en plaçant dessous des bâches en
plastique. Cela vous permettra de récolter un maximum de sucs.
- Si vous souhaitez que votre compost dégage une odeur de soufre,
arrosez-le tous les jours.
- Si vous préférez les odeurs d'ammoniac, privilégiez les matières
vertes.
- Afin d'éviter que votre compost ne perde en puissance, évitez d'y
jeter des coquilles d'œufs (évitez tout simplement de manger des œufs,
vous épargnerez quelques poules qui n'ont rien demandé[^113]), ainsi
que les matières brunes comme les feuilles mortes ou le café. Ces
matières atténuent les odeurs fétides.

### Le purin d'orties

Le purin d'orties, sacré activateur de compost, est particulièrement
infect. Et, bonne nouvelle, les orties, c'est facile à trouver. Partout,
cette plante désobéissante et insupportable pour les fanatiques proprets
d'urbanisme néolibéral, pousse sauvagement, sans rien demander, se
défendant grâce à ses propriétés urticantes.

- Mettez les orties dans un bac qui ne soit pas en métal.
- Ajoutez-y de l'eau : 10 litres d'eau pour 1 kg d'orties.
- Laissez macérer 1 à 2 semaines en remuant le mélange tous les 2,
3 jours.

### Le jus de poubelle de la colère

Y'a des endroits où les poubelles pullulent encore plus que les orties.
Et ça tombe bien, elles sont aussi faciles à trouver que leurs cousines
urticantes. Les ingrédients de base pour un bon jus de poubelle, c'est
ce poivron âcre d'Espagne, celui que les grandes surfaces emballent par
trois et finissent la plupart du temps par jeter puisqu'un des légumes a
pourri dans le plastique, c'est le pauvre citron qui n'a pas supporté
d'être baladé pendant des semaines et a été tej' avec les huit
autres membres du filet, les quatre raisins tout mous de la grappe qu'on
a balancée aux ordures parce qu'elle n'était plus conforme aux canons de
beauté supermercantiles, les peaux de bananes trop brunes et tous les
autres fruits et légumes qu'on offre aux poubelles.

- Ouvrez les poubelles des supermarchés et réjouissez-vous de leur
puanteur nauséabonde. Le compost va d'autant plus parfumer sa cible.
- Récupérez les fruits et les légumes encore bons, s'il y a une chose
que les poubelles de supermarché peuvent faire, c'est nourrir celleux
qui en ont besoin[^114].
- Gardez tous les éléments à composter, entiers ou en morceaux. *Zéro
déchet* !
- Suivez *la recette du compost dégueu qui fermente trop* --- arrosez
suffisamment, car un compost peu arrosé garantit un humus boisé, et
c'est clairement pas ce qu'on cherche.
- Récoltez le suc de la colère ainsi obtenu et déversez-le dans le lieu
stratégique qui aura été défini collectivement. Ou mieux, selon vos
envies, vaporisez-le sur les individus qui auront été choisis, ou
devrait-on dire "élus", à cet effet.

{{%ligneblanche%}}

Le compost n'a pas de but en soi ; nous décidons collectivement de ce
que nous en faisons. C'est un outil formidable qui a la capacité de
faire grandir nos projets. Et puis, d'un autre côté, sa raison d'être
c'est aussi celle des casseureuxses[^115] : réduire en morceaux ce
dont on veut se débarrasser. Le compost est une force qui destitue
l'ordre en place pour instaurer une nouvelle forme de vie.

Aux agriculteurixes qui en ont ras le cul d'une politique qui favorise
l'agriculture intensive, la monoculture, et autres naturocides, venez
avec vos tractopelles, vos camions, vos charrues pour les moyenâgeuxses
du futur, rassemblez-vous pour une parade bien crado.

Aux squats, aux constructeurixes qui ont la flamme de la créativité,
forgeons des LCD, des
lanceurs de compost Défensifs, des catapultes pour propulser nos balles
à compost bien dégueux.

C'est l'occasion de renouveler nos imaginaires insurrectionnels.

Brûlez, certes, mais aussi compostez.

[^111]: Dans *Piraterie ordinaire* \[n^o^ 38\], on découvrira quelques autres usages créatifs du compost.

[^112]: *Cette colère immense, collective, transgénérationnelle, internationale* \[n^o^ 30\] annonce cette révolution.

[^113]: Pour un renversement de perspective, voir *Solidarité antispéciste* \[n^o^ 35\].

[^114]: *Le Grand Midi* \[n^o^ 47\] partage des conseils pratiques sur l'organisation d'une cantine autogérée.

[^115]: Lire leur perspective dans *Survivre dans un black bloc* \[n^o^ 15\].

