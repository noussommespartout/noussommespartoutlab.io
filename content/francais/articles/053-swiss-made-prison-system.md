---
author: "Anonyme"
title: "Swiss made prison system"
subtitle: "Récit de parloir sauvage avec des prisonnièrexs"
info: "Texte rédigé pour le recueil"
datepub: "avril 2021"
categories: ["police", "antiracismes", "autodéfense", "prison, justice, répression", "sabotage, action directe"]
tags: ["amende", "amour, love, amoureuxse", "caméra", "cigarette, clope", "colère", "corps", "courage", "crime, criminellex", "doute", "empathie", "erreur", "fleur", "force", "France", "impossible", "lunettes", "pouvoir", "rage", "rue", "silence", "soleil", "solution", "urbanisme", "violence", "voix"]
langtxt: ["fr"]
zone: "Suisse romande"
num: "53"
quote: "la prison est un non-lieu, parce que rien ni personne n'y habite"
---

Là, on remonte une ruelle, une artère sans âme quelque part dans la
périphérie urbaine. C'est souvent dans les ruelles que ça se passe,
c'est souvent dans la périphérie urbaine sans âme.

Le groupe n'est pas très grand. Touxtes ont dissimulé leurs visages,
touxtes essaient de ne pas trop se faire remarquer. Le soleil commence à
se coucher, mais la ville est encore animée et lumineuse. Les gens
rentrent du boulot. En plein jour, tout est plus risqué, mais tant pis :
notre action ne peut fonctionner qu'avant le coucher du soleil.

Avec nos sacs à dos et nos banderoles, on avance d'un pas qu'on aimerait
un peu plus assuré. À l'arythmie de nos jambes, aux regards qu'on jette
régulièrement derrière nous, notre anxiété est perceptible. On puera la
sueur avant d'avoir fait la moitié du chemin[^155].

La prison n'est pas un endroit, ce n'est pas un lieu à proprement
parler, c'est un morceau de territoire découpé de murs, de miradors, de
barbelés et de tôle. Personne n'y vit, personne n'y habite au sens où
habiter c'est donner une forme à l'espace. Aucune des personnes
arpentant l'espace de la prison n'a de pouvoir sur sa forme. Sa forme a
été pensée par des générations de théoriciennexs du contrôle qui n'y
habitent pas, dessinée par des architectexs qui n'y habitent pas, payée
par des contribuables qui n'y habitent pas, construite par des
ouvrièrexs qui n'y habitent pas, réformée par des réformixstes qui n'y
habitent pas. La prison est un non-lieu, parce que rien ni personne n'y
habite.

Arrivéexs en haut de la rue, on commence à apercevoir les crêtes des
murs d'enceinte et un grand mirador qui surplombe les toits alentour. La
prison est encastrée entre plusieurs pâtés de maisons, posée comme une
fleur dans un quartier résidentiel. On dirait bien que cette grosse
aberration de violence, posée au milieu des jardins, ne dérange
personne. De là où nous sommes, la prison ressemble à un ancien hôpital,
avec sa couleur de mouroir glauque et ses briques un peu déglinguées qui
rappellent les asiles du XIX^e^ siècle. Si Hollywood filmait ça, ce serait
pour poser le décor d'une histoire horrible.

C'est toujours surprenant, en Suisse comme ailleurs, de constater
combien les prisons sont proches des centres-villes. Des dizaines de
milliers de personnes vivent, dorment, mangent, pratiquent tout ce qui
fait leur quotidien, *habitent* vraiment l'espace où elles vivent, mais à
quelques mètres de là dorment des personnes dont on a temporairement
arrêté l'existence, des personnes qui attendent, des mois, des années,
des dizaines d'années. On vit autour de non-lieux dans lesquels d'autres
ne vivent pas et on nous a appris à trouver ça normal, endoctrinéexs à
accepter l'inégale répartition du droit à *habiter* l'espace. On nous a
dresséexs à accepter la prison comme une évidence.

{{%ligneblanche%}}

*On the other hand, the prison is considered an inevitable and permanent
feature of our social lives. Most people are quite surprised to hear
that the prison abolition movement also has a long history, one that
dates back to the historical appearance of the prison as the main form
of punishment. In fact, the most natural reaction is to assume that
prison activists --- even those who consciously refer to themselves as
"antiprison activists" --- are simply trying to ameliorate prison
conditions or perhaps to reform the prison in more fundamental ways. In
most circles, prison abolition is simply unthinkable and implausible.
Prison abolitionists are dismissed as utopians and idealists whose ideas
are at best unrealistic and impracticable, and, at worst, mystifying and
foolish. This is a measure of how difficult it is to envision a social
order that does not rely on the threat of sequestering people in
dreadful places designed to separate them from their communities and
families. The prison is considered so "natural" that it is extremely
hard to imagine life without it.*[^156]

{{%alignement-d%}}
(Angela Davis, *Are Prisons Obsolete*.)
{{%/alignement-d%}}

{{%ligneblanche%}}

Avant de venir, on s'est équipéexs pour faire du bruit, du bruit et de
la fumée. On a des banderoles à étendre, peut-être à attacher si on y
arrive, si on nous en laisse le temps. Tout ça pèse relativement lourd,
tout ça encombre nos mouvements. Impatienxtes d'arriver enfin au pied
des barbelés, on presse encore le pas. Le groupe est trop petit. Pas
besoin de discuter, rien qu'à sentir mutuellement nos odeurs de
transpiration, on se souvient des risques que l'on prend.

Il y a quelques mois de ça, en France, un homme a écopé d'un mois de
prison avec sursis et de deux mille euros d'amende. Son crime ? Avoir
bravé la loi qui interdit formellement de tenter d'entrer en
communication non autorisée avec une personne incarcérée dans un
établissement pénitentiaire. Le criminel en question avait 65 ans. Il
s'était approché de la fenêtre d'une maison d'arrêt pour hurler "Bon
anniversaire mon fils, je t'aime". D'après un voisin, quand la police
l'a interpellé, il pleurait, assis dos au mur extérieur du pénitencier.

Le groupe s'enfile dans une petite allée bordée d'arbres et de bâtiments
fonctionnels destinés aux tâches logistiques et au personnel
administratif. Tout est calme. À part le bruit des voitures, la zone est
déserte. Ou presque déserte. À l'angle d'une baraque de chantier, trois
types en train de fumer une clope nous regardent d'un œil mauvais. Ils
ont des gueules qu'on finit par reconnaître, des manières de s'asseoir,
de marcher, des manières de tirer des lattes qu'on internalise, avec le
temps. Par définition, le flic en civil porte les cheveux courts,
souvent en brosse. Il a des lunettes de soleil, des fringues de sport,
ou plus exactement des fringues *sport n' chic*, genre veste bien coupée
et pantalon un peu moulant, mais pas assez moulant bien sûr pour que
quiconque puisse contester sa virilité solaire. En général, son torse
est bien bombé, ses jambes sont arquées comme celles du cowboy
fraîchement descendu de cheval et il tire tellement ses épaules en
arrière qu'il est presque incapable de garder les bras le long du corps.
Il a pas l'air souple. Il a pas l'air sympa. Il a pas l'air malin. Et
quand tu croises des types du genre qui sont pas des keufs en civil,
souvent, dans leur tête, c'est quand même un peu des keufs en civil.

Sans avoir eu besoin de dire un seul mot, le petit groupe partage un
léger doute. Sur un diapason tacite des corps, touxtes comprennent la
nécessité d'une réaction improvisée mais rapide. Impossible de
contourner les trois hommes. On les ignore froidement et on s'engouffre
dans un genre de chemin pédestre qui décale sur la gauche et nous
éloigne du portail principal. Apparemment, la conscience collective de
l'essaim silencieux lui dicte de contourner le bâtiment. Difficile de
penser qu'on a alerté personne. Nos visages sont presque tous masqués.
Nos sacs à dos pleins à craquer sont éloquents. L'une d'entre nous porte
une caméra en bandoulière. En nous voyant, même un enfant aurait compris
qu'on s'apprêtait à donner un petit spectacle.

Le chemin qui contourne la prison ressemble à toutes les ruelles qu'on a
empruntées jusque-là, avec encore une couche de silence en plus.
Maintenant, on voit le dos du bâtiment, une muraille percée d'ouvertures
minuscules surplombant une cour intérieure entourée de grillages que
plusieurs déroulés de fil barbelé renforcent. À la hâte, on s'essaime
sur la zone pour constater rapidement qu'elle n'est pas surveillée.
Derrière nous, surtout des arbres, un petit bout de forêt que
l'urbanisme socialiste destine aux randonnées urbaines et au *crossfit
outdoor*. Au moins, ici, nous sommes face au dos de la prison, et dans
notre dos à nous, personne ne peut arriver par surprise. Il n'y a pas
encore de keufs affectés aux forêts urbaines et la présence
bienveillante des arbres nous rassure. On reforme un petit bloc et on
s'approche du grillage. La rage nous monte aux yeux et on se décide à
crever le silence.

Il faut que nos voix traversent les barbelés, volent au-dessus de la
cour, percent les épais murs de ciment et de briques, se glissent entre
les barreaux des fenêtres. Touxtes gueulent le plus fort possible. Et
c'est la rage qui parle en premier, pour être bien certainexs que les
personnes enfermées comprennent immédiatement que les voix entendues
sont des voix amies. On commence à chanter : "Tout le monde déteste les
prisons". On se demande s'iels perçoivent nos chants, mais très vite
d'autres voix s'élèvent de l'intérieur des murs, hurlant elles aussi,
incapables d'estimer la distance à laquelle nous nous trouvons de leurs
fenêtres. Plusieurs incarcéréexs commencent à reprendre en chœur :
"Tout le monde déteste les prisons".

On déploie les banderoles, en espérant qu'iels aperçoivent ce point de
couleur parmi les graviers, juste sous les arbres. Comme on s'agite, il
y a un bref instant de silence, à nouveau brisé par des phrases sans
visage. De là où on est, c'est une immense muraille de brique qui
s'époumone :

"Venez nous sauver !"

"Ils nous tuent !"

"Libérez-nous !"

"On nous bute !"

"J'ai rien fait !"

Difficile de savoir quelle émotion collective parcourt le petit groupe.
Un mélange de colère, d'impuissance et d'empathie. Nos chants deviennent
des chants de paix, les rythmes de celleux qui ne peuvent rien faire
sinon essayer de transmettre un peu de force :

"On vous aime. On vous aime. Tenez bon."

C'est étrange d'envoyer de l'amour à des dizaines de personnes en même
temps, mais c'est possible. C'est de l'amour structurel, de l'amour
politiquement légitime, de l'amour adressé comme une pierre ou une
ortie, inséré dans les fêlures d'une organisation sociale répressive.

La Suisse compte 106 prisons d'État, enfermant près de 7 000 personnes,
dont 71 % sont des étrangèrexs (en comptant les permis C qui représentent
25 % des incarcéréexs) dont le quart sont des personnes migrantes en
attente d'une expulsion forcée du territoire et dont la quasi-totalité
sont recensées comme des hommes par les statistiques officielles
(96 %)[^157]. Les cantons romands sont les plus répressifs, mais aussi
ceux qui appliquent la politique de gestion de la criminalité la plus
xénophobe (avec le record européen de 79 % d'étrangèrexs parmi les
incarcéréexs).

{{%ligneblanche%}}

*Si le code est le même pour tous, la palme d'or en matière de châtiment
toute nationalité confondue revient aux cantons de Vaud et Genève.
"Alors que ces deux cantons représentent 14,6 % de la population suisse,
ils prononcent 43,4 % de toutes les peines." Cherchez l'erreur. Une
explication? "C'est Pierre Maudet, le conseiller d'État, et Olivier
Jornot, le procureur général, des hardliners en matière de condamnation.
Il y a tout un climat, en tout cas à Genève comparativement à Zürich ou
à Bâle", indique Daniel Fink. Et cet ancien chef de la section
Criminalité et droit pénal de l'Office fédéral de la statistique (OFS)
d'évoquer encore la part très élevée des étrangers non résidents à
Champ-Dollon, atteignant par moments jusqu'à 80 %, tout au moins en
détention avant jugement. "En Suisse, un quart de toutes les détentions
provisoires sont réalisées à Genève. Et qui va en détention provisoire ?
Ce sont les étrangers non domiciliés en Suisse. Il y en a pourtant
certainement autant à Bâle et à Zürich." De fait, à la prison de
Champ-Dollon, le pourcentage d'étrangers s'élève à 89 % et dans tout le
canton de Genève il est de 86 % (chiffres 2017).*

{{%alignement-d%}}
(Revue *Allez Savoir*, n^o^ 71, janvier 2019.)
{{%/alignement-d%}}

{{%ligneblanche%}}

On commence à déballer du matériel, des trucs pour faire du bruit, du
bruit et de la fumée, des trucs à balancer. Là on se sent un peu plus
fortexs et en sécurité, comme on se sent toujours lorsque l'adrénaline
de l'<a href="/glossaire/#action-directe" class="glossary-link" target="_blank" rel="noopener" data-no-swup>action directe°</a> parcourt tout le corps. Les heures, voire les
jours, qui précèdent toute action, même la plus anodine, sont souvent
difficiles et stressantes. Aussi nul que ça puisse être, on repense
beaucoup aux risques. Un an plus tôt, une femme, française également,
est elle aussi tombée sous le coup de la loi interdisant les parloirs
sauvages pour avoir lancé une fleur à son compagnon, par-dessus un
barbelé, le jour de la Saint-Valentin. La cour l'a condamnée à trois
mois de prison ferme, avec application immédiate. Les heures, voire les
jours, qui suivent toute action, surtout si elle s'est bien passée,
procurent un soulagement profond, une paix intérieure, le sentiment
heureux d'avoir eu le courage de briser le silence. Mais dans le temps
immédiat de l'action, on ne pense plus à rien.

"Attention, ils arrivent !"

Ce sont les prisonnièrexs qui nous préviennent, depuis l'intérieur du
bâtiment.

L'unex d'entre nous hurle quelque chose et touxtes aperçoivent les
fourgons de police qui remontent la ruelle et foncent vers notre
position. On se disperse[^158]. On replie les banderoles en quelques
secondes, on range le matériel et on déguerpit. Intuitivement, la
plupart courent en direction des arbres, s'engouffrent dans la forêt où
les véhicules ne pourront pas les suivre. D'autres s'élancent dans le
quartier résidentiel, slaloment entre les voitures, sautent par-dessus
les barrières. Les voix s'échappent toujours de la prison, elles nous
donnent de l'énergie, elles nous couvrent : "Tout le monde déteste la
police !", "Partez sur la droite !", "Attention ils entrent dans la
forêt !". Les prisonnièrexs continueront à gueuler pendant de longues
minutes, attirant l'attention de la police qui ne parvient pas à trouver
celleux d'entre nous qui se sont cachéexs parmi les arbres. Une fois que
nous sommes complètement disperséexs, tout à fait sûrexs que les keufs
ont abandonné la course-poursuite, nous nous retrouvons, dans la ville ;
certainexs sont soulagéexs, d'autres déçuexs de n'avoir pas pu rester
plus longtemps. Et la prison, elle, tient toujours debout.

Même les rapports des commissions internationales de l'ONU, que l'on
peut difficilement soupçonner d'être un groupuscule <a href="/glossaire/#abolitionnisme" class="glossary-link" target="_blank" rel="noopener" data-no-swup>abolitionniste°</a>
antiprison d'ultragauche, signalent que près de 70 % des détenuexs dans
le monde "vivent dans des conditions qui ne respectent pas la dignité
humaine" et, surtout, que près de 80 % des détenuexs peuvent être
considéréexs comme des "prisonniers socioéconomiques" au sens où
"des programmes d'amélioration de l'environnement social et économique
permettraient d'éviter 80 % des emprisonnements". Et voilà comment
l'ONU, l'organe suprême du capitalisme d'État mondialisé, confesse
d'elle-même que le fondement de son système répressif repose sur la
violence socioéconomique qu'elle exerce.

Le principe d'une transformation socioéconomique comme solution à
l'emprisonnement est depuis toujours l'argument fondamental des
abolitionnistes. Souvent, les discussions s'enlisent car elles se
coincent dans un piège argumentatif : quelle alternative trouver à la
prison ? Plusieurs positions réformistes proposeront alors des variantes
autour de l'idée d'enfermement (détention à domicile, bracelet
électronique, etc.). Alors qu'il s'agit en réalité de proposer un
ensemble de transformations sociales qui empêchent la précarité, cause
principale d'incarcération : transformation de l'éducation, gratuité et
accessibilité du système de santé et particulièrement de l'encadrement
psychologique et psychiatrique, décriminalisation des parcours
migratoires, construction d'un système judiciaire populaire et
autogestionnaire, basé sur la réparation et non sur des logiques
punitives, dépénalisation des drogues.

La prison, comme bâtiment, n'est pas le noyau du problème répressif.
C'est l'ensemble du système carcéral, comme réseau imbriqué, que repense
la perspective <a href="/glossaire/#abolitionnisme" class="glossary-link" target="_blank" rel="noopener" data-no-swup>abolitionniste°</a>. La question de l'alternative <a href="/glossaire/#justice-restaurative-ou-réparatrice-ou-transformatrice" class="glossary-link" target="_blank" rel="noopener" data-no-swup>punitive°</a> à
la prison est une fausse question. Dans une société qui garantit un
revenu minimal d'existence à touxtes, dépénalise les drogues, fournit un
encadrement psychosociologique gratuit et égalitaire, garantit une
dignité matérielle pour touxtes (par exemple au moyen d'un revenu
universel), renonce à des lois d'asile racistes et violentes[^159], il
n'y aurait plus besoin de prisons, ou, pour être plus exact (toujours
d'après l'ONU), près de 80 % des personnes incarcérées ne l'auraient
jamais été.

Est-ce qu'on accepte de vivre dans une société pareille ? Est-ce qu'on
accepte un système conçu pour reproduire sans cesse l'oppression des
plus précaires ? C'est une autre conclusion particulièrement stable des
recherches en criminologie : l'incarcération favorise la
réincarcération, la prison légitime fonde et engendre la prison.

Alors, quand on dit "que crève la taule", ça veut dire que crève
l'incarcération comme système organisé, que crève la prison en tant
qu'elle engendre la prison.

Que crève le cercle sans fin.

[^155]: Pour une autre action mêlant anxiété et sueur, lire *Bidule, Truc et Machin à la ferme* \[n^o^ 29\].

[^156]: En français : La prison, en revanche, est perçue comme un élément constitutif et immuable de nos sociétés. On ignore trop souvent que le mouvement pour l'abolition carcérale est lui aussi riche d'une longue histoire qui remonte à l'époque où la prison est apparue en tant que principale forme de châtiment. En fait, on a spontanément tendance à penser que les militants anticarcéraux (y compris ceux qui se désignent eux-mêmes comme "militants antiprison") cherchent uniquement à améliorer les conditions de détention ou à réformer fondamentalement le système carcéral. Pour une écrasante majorité de citoyennexs, la suppression des prisons est tout simplement impossible et inconcevable. Les personnes qui militent pour l'abolition carcérale sont considérées comme des utopistes et des idéalistes dont les idées seraient, au mieux, irréalistes et inapplicables et, au pire, mensongères et insensées. C'est dire à quel point il est difficile d'envisager un ordre social qui ne repose pas sur la menace de l'incarcération des individus dans des lieux épouvantables conçus pour les séparer de leurs proches et de leur communauté. La prison est considérée comme un élément si "naturel" qu'il est extrêmement difficile d'imaginer la vie sans elle.

[^157]: Ces chiffres et statistiques sont issus de la revue *Allez Savoir*, n^o^ 71, janvier 2019.

[^158]: Pour en savoir plus, lire *Survivre dans un black bloc* \[n^o^ 15\].

[^159]: Sur les liens entre prison et racisme institutionnel, lire aussi *Brisons l'isolement* \[n^o^ 39\].
