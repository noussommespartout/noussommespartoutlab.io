---
author: "Anonyme"
title: "Nique le cynisme"
subtitle: "Et tout particulièrement celleux qui sont assez cyniques pour vivre sur Mars"
info: "Texte rédigé pour le recueil"
datepub: "février 2021"
categories: ["relation à la militance"]
tags: ["amour, love, amoureuxse", "banque", "cul", "gueule", "logique", "mère", "poil"]
langtxt: ["de", "fr"]
zone: "Suisse romande"
num: "12"
quote: "on s'en tape de Mars"
---

On reproche souvent au discours militant d'extrême gôche radical sa
rigidité, son impression d'avoir toujours raison, d'avoir toutes les
solutions. En vrai, j'avoue, je trouve que j'ai raison sur pas mal de
trucs. Il faut penser que t'as raison pour bouger ton cul et militer, au
moins un peu raison. C'est pas un défaut de penser que t'as raison.
Avoir des "convictions", si on change un peu les mots, c'est plutôt
une qualité, un truc qu'on noterait presque sur son CV.

Pour créer un bon consensus mou helvétique à deux balles, je prends deux
exemples bien faciles :

Au plus profond de moi, je crois que j'ai raison de m'insurger contre le
fait qu'il y a des multimilliardaires qui se la coulent douce et des
pauvres qui galèrent. Au plus profond de moi, je crois que j'ai raison
d'être contre la peine de mort, pour la création d'une société plus
juste, plus sociale, plus heureuse.

On me dira : "Mais bien sûr tout le monde trouve ça injuste, c'est un
élément de langage de gauche ! Vous n'avez pas le monopole du cœur".
Alors, bon, déjà, la Gôche ça veut rien dire. Ensuite, une chose : si
c'est un élément de langage que tout le monde partage, ça veut dire
qu'on est touxtes d'accord. Et si on est touxtes d'accord, pourquoi il y
a encore des multimilliardaires qui se la coulent douce pendant que des
pauvres galèrent ? Ma logique est implacable. D'une manière ou d'une
autre, si tu me réponds ça, c'est que tu ne partages pas *vraiment* cet
avis, que tu ne trouves pas ça *vraiment* injuste.

"Oui, mais c'est plus compliqué que ça". Ok, ce qui est peut-être plus
compliqué, c'est de renverser ce qui maintient ces inégalités, mais si
t'es d'accord sur le fond, pourquoi t'es pas en train de t'insurger, toi
aussi ? Et surtout, pourquoi t'es pas d'accord avec moi ? Pourquoi
t'adoptes cette posture de, "je maîtrise la complexité", au lieu de
venir gueuler avec moi ?

Au pauvre banquier cynique et social-traître de ma famille de prolos qui
trouve que c'est bien normal que sa mère gagne le cinquième de son
salaire parce qu'au lieu de faire des études elle a passé son temps à le
mettre au monde et à le biberonner de lait et d'amour, j'ai envie de
répondre avec un pavé dans sa gueule. J'ai envie de répondre qu'il a
choisi son camp seul, ok pas tout à fait, la pub, sa propre banque, la
grosse inertie idéologique de la démocratie, tout ça l'a bien aidé, mais
il a fini par choisir. Sa logique est efficace, son intelligence est
capable de recracher des principes méritocratiques cyniques et pourris :
il a tous les outils en main pour être d'accord. Je ne lui demande pas
de s'engager, même pas d'envoyer septante balles par année à Greenpeace,
juste d'être d'accord, juste de pas s'obstiner à me contredire, juste de
pas prendre un malin plaisir à répéter que "c'est plus compliqué que
ça" d'un air satisfait avant de repartir travailler, le cœur
tranquille, dans sa banque.

Je suis triste, et énervée, triste de pas comprendre, de pas réussir à
m'expliquer pourquoi tout le monde n'est pas d'accord avec moi, pourquoi
tout le monde n'est pas en train de s'insurger. C'est tout con comme
réflexion, je dis pas que je suis intelligente, je dis que j'ai raison.

Et des pauvres banquiers qui gagnent cinq fois plus que ma tante, il y
en a plein, des multimilliardaires, il y en a nettement moins. On serait
de taille pour répartir, même un poil plus, toute cette thune, celle qui
finit dans ces projets débiles qui visent à créer des capsules blanches
et lisses et dégueulasses pour habiter sur Mars. On s'en tape de Mars.
Tant qu'il y aura encore une seule personne sur cette planète-ci qui
crève la dalle, qui n'a pas de toit sur sa tête, qui est moins
"légale" qu'une autre sur un territoire, on chiera sur Mars. Il n'y a
rien là-bas qui puisse nous aider véritablement, juste de la poussière
rouge qui vit sa meilleure *life* en dehors de l'anthropocène. Il n'y a
pas (encore) de multimilliardaires à spolier, de sociétés à transformer,
de forêts à sauver. Sur Mars, y'a personne à aimer.

Voilà, c'est mon coup de gueule quotidien, perpétuel et cyclique, celui
qui me vient quand je regarde les infos, quand je marche dans les
quartiers riches, quand je vis le dérèglement climatique, quand je lis,
quand je bois, quand je mange, quand je vais faire les courses, quand je
vois toutes ces tantes précaires et tous ces cousins banquiers.

Et parfois j'en peux plus, je gonfle comme une montgolfière et j'ai
envie de lâcher du lest pour me casser dans les airs, pour faire la
course avec leurs fusées. Mais j'arrive pas à détacher les poids de la
cabine, c'est trop lourd, les nœuds sont trop serrés. C'est dur de faire
capter cette sensation sans passer pour la bonne âme charitable.

C'est pas des éléments de langage, c'est réel et surtout, c'est
*fucking* raisonnable.

Il n'y a que le cynisme qui soit déraisonnable.

Voilà, ce sera tout pour moi.

Militer, c'est niquer le cynisme.

