---
author: "Anonyme"
title: "La piraterie n'est jamais finie"
subtitle: "Une conversation sur un tchat en ligne lié à Anonymous"
info: "Transcription adaptée d'une conversation virtuelle"
datepub: "décembre 2020"
categories: ["hack, offensive numérique", "sabotage, action directe", "prison, justice, répression"]
tags: ["amour, love, amoureuxse", "barricade", "déception", "détruire, destruction", "feu", "force", "gouvernement, gouverner", "internet, web", "isolement", "limite", "mail, email", "merde", "pirate", "pouvoir", "prix", "rue", "théâtre"]
langtxt: ["fr"]
zone: "Suisse romande"
num: "5"
quote: "feu aux prisons de béton et de pixels"
---

\[online.channel1407\#j8.private.\]

\[XX.XX.2020\]

\~ \-\-armor

\~ \-\-bash/reset

\[Y\] : salut X ça va ?

\[X\] : moyen et toi ?

\[Y\] : ça va, pourquoi moyen ? 😊

\[X\] : je sais pas, avec le Covid, c'est chaud, les politiques du
canton, on se sent en prison

\[Y\] : bon c'est normal, ça me choque pas de protéger les personnes
les plus vulnérables

\[X\] : non bien sûr, mais on n'a pas besoin de keufs, on a besoin de
fric dans les hôpitaux publics et de couverture médicale gratuite

\[Y\] : c'est clair

t'as du temps ?

on parle un peu de toi et de quelques actions, pour ce livre, là ?

\[X\] : je suis chaud 😂

par contre je sais pas du tout écrire

je suis vraiment mauvais

genre je saurais même pas expliquer mes propres actions

ce que je fais

c'est tragique mdr

\[Y\] : t'inquiète, je corrigerai un peu l'orthographe et la syntaxe 😊

pour le texte, on peut faire plein de formats

limite on fait une version adaptée de ce qu'on se dit

au pire je te pose des questions et on dit que cette conversation c'est
la participation ?

\[X\] : je comprends pas

\[Y\] : bah on met ce qu'on se dit dans le livre

genre dialogue de théâtre mdr

\[X\] : ouais ok, mais tu connais les règles du tchat

\[Y\] : ouais, pas de souci, c'est un peu les mêmes dans le livre

le but c'est pas de faire un annuaire pour la police

\[X\] : lol

ouais je veux bien confesser mes péchés alors

mais vous avez fait ça en mode *safe* ?

si toutes les personnes qui brûlent des trucs sur cette planète vous
écrivent par mail vous êtes surveilléexs dans dix minutes

\[Y\] : déjà c'est relativement localisé autour du pays et on fait le
maximum pour que ce soit *safe* numériquement[^13]

d'ailleurs tu serais ok de me dire ce que tu penses de notre protocole

\[X\] : ouais

\[*discussion technique peu intéressante*\]

le protocole est bien

bravo baby

\[Y\] : tu veux commencer par te présenter ?

\[X\] : bof

honnêtement, qui je suis, c'est pas dingue

\[Y\] : le forum sur lequel on tchate, là, c'est un lieu d'échange sur
l'activisme numérique et le hack, relativement sécurisé, faudrait que
tu présentes cet endroit et ce que tu appelles "faire des trucs"

\[X\] : bah ici c'est un repère de clowns qui bricolent des trucs pour
essayer de détruire le système capitaliste

mais plus sérieusement c'est une zone d'échange qui a été créée par
plein de gens et qui est modérée par d'autres gens

c'est souvent des personnes qui ont été actives pendant les grandes
années des Anonymous, je sais qu'il y a des anarchistes et des
communistes de partout, des <a href="/glossaire/#troll-verbe--troller" class="glossary-link" target="_blank" rel="noopener" data-no-swup>trolls°</a> qui servent à rien, des flics, des
révolutionnaires mexicains, plein de monde

\[Y\] : et toutes ces personnes sont des hackeureuxses ?

\[X\] : bah tout le monde est unex hackeureuxse

faut pas croire que tu peux arriver et faire de la magie et badaboum les
gouvernements tremblent

disons qu'ici ça s'organise

\[Y\] : lol ok

quand est-ce que t'as commencé à militer en ligne ?

\[X\] : y'a trois ou quatre ans je pense

bizarrement c'est venu de rencontres <a href="/glossaire/#irl" class="glossary-link" target="_blank" rel="noopener" data-no-swup>IRL°</a>

même pas de rencontres en ligne

en Suisse alémanique, pour pas être trop précis

mais depuis, je fais que des trucs depuis le net

je déteste les meetings en vrai

les <a href="/glossaire/#hackerspaces" class="glossary-link" target="_blank" rel="noopener" data-no-swup>hackerspaces°</a> ou les <a href="/glossaire/#hackathons" class="glossary-link" target="_blank" rel="noopener" data-no-swup>hackathons°</a>

c'est de la merde

c'est toujours au final des pros de la sécurité qui vont finir par faire
des trucs qui servent des intérêts contre-révolutionnaires

iels se rendent pas compte qu'iels se font récupérer en fait

\[Y\] : tu te définis comme révolutionnaire ?

\[X\] : bien sûr, pas toi ?

je sais pas trop, libertaire, crypto-anarchiste, quelque chose comme ça

\[Y\] : on peut être révolutionnaire sur le net ?

\[X\] : j'ai envie de dire oui, mais j'en suis pas si sûr

c'est quand même de plus en plus difficile de faire des trucs

à cause de plusieurs trucs

déjà, plus les systèmes se complexifient, plus il faut devenir
spécialiste des trucs

par exemple, plus une boîte ou une agence gouvernementale est grande,
plus elle peut se payer des pros à tous les niveaux et en général les
*<a href="/glossaire/#black-hat" class="glossary-link" target="_blank" rel="noopener" data-no-swup>black hats°</a>* ont pas la même force de frappe

ensuite, c'est aussi que le pouvoir, c'est de plus en plus virtuel

la Silicon Valley sera bientôt plus puissante que les États et les
multinationales classiques

donc notre marge de manœuvre se réduit avec les années tu vois

\[Y\] : pourquoi ?

\[X\] : parce qu'iels décident toujours plus de ce à quoi ressemblent
les communications, tout le réseau, toute la connexion des machines et
l'interaction avec les utilisateurixes

quand tu commences à t'y connaître, tu sens particulièrement bien la
prison qui se referme, mois après mois, année après année, les sites
sont toujours mieux protégés

c'est de plus en plus dur d'être anonyme ou de bidouiller des trucs

de se promener là où t'es pas censéex être

ce qu'il se passe dans le cyberespace, c'est un peu comme si, dans le vrai
monde, les gens acceptaient d'être masqués, puis acceptaient que les
rues soient toutes surveillées, puis acceptaient de plus pouvoir sortir
de chez elleux sans décliner leur identité, puis acceptaient de ne plus
avoir le droit de marcher où iels veulent dans leur ville... 😞

\[Y\] : lol

\[X\] : puis t'apprends que y'aura un keuf dans chaque rue, puis devant
chaque maison et finalement t'acceptes que y'ait un keuf qui vive avec
toi tout le temps

c'est ça qu'on ressent avec l'évolution du net ces dernières années

demande à des militanxtes *offline* comment iels arriveraient à militer
avec un keuf affecté directement dans leur maison

et bientôt, avec tous les objets connectés, y'aura plusieurs keufs par
maison, un keuf dans le frigo qui regarde ce que tu manges, un keuf dans
les chiottes qui regarde ce que tu chies, un keuf dans ton bureau qui
regarde ce que tu écris

et tous ces robots-keufs communiquent instantanément

les données vont très vite

et l'analyse de ces données va de plus en vite aussi

ça on le dit moins

c'est pas tout de récolter des millions de données

encore faut-il leur donner du sens pour des humains

il faut les compiler

repérer ce qui est utile

et les intelligences artificielles font ça de mieux en mieux

il y a de moins en moins de failles ou d'erreurs humaines dans la
surveillance

alors que c'est le principal truc que les hackeureuxses peuvent
exploiter

\[Y\] : en gros, les robots sont de plus en plus doués pour rendre les
données humaines compréhensibles aux humains

\[X\] : oui

\[Y\] : donc le hack militant est en train de disparaître ?

\[X\] : oui et non

on se bat quand même

\[Y\] : on parle un peu des actions que t'as pu faire ? t'as commencé
par quoi ?

\[X\] : franchement j'ai pas fait une suite de trucs logiques

j'ai papillonné ici et là

un peu comme tout le monde dans la communauté

je me suis intéressé au *hardware*, puis je me suis intéressé aux
applis, puis forcément aux langages de programmation, au langage
machine, aux logiciels libres, à la cryptographie, aux ondes

je suis spécialiste de rien

mais plus je découvrais ces trucs, plus je commençais à envisager leur
intérêt politique

et puis quand j'étais plus jeune, Anonymous me faisait pas mal rêver

\[Y\] : t'as jamais été déçu ?

\[X\] : bien sûr

c'est une déception permanente Anonymous 😂

lol

mais c'est vraiment personne

ou tout le monde

et y'a une majorité de connards de trolls

bon j'avoue qu'au début, j'étais un peu un troll

j'aimais bien foutre la merde

mettre des trucs à l'envers pour voir ce que ça faisait

cracker le facebook du mec *bully* et violent de mon gymnase pour
exposer sa vie de merde d'agresseur

mais j'ai jamais trop été le cliché du geek sans amiexs

\[Y\] : et tu t'es politisé comment ?

\[X\] : en lisant des trucs je crois

en voyant les actions les plus politiques d'Anonymous

en allant à des manifs un peu

mais les manifs ça me fait pas mal flipper lol

je suis pas très courageux dans la vraie vie

et puis j'ai un taf donc voilà

c'est plus commode de hacker la nuit

bref voilà, j'ai commencé à traîner sur les forums et à participer à des
trucs

\[Y\] : ça te dit de me raconter des actions possibles, des gestes
politisés en ligne qui ont marché ?

\[X\] : ouais, mais pas dans le détail

il y a des trucs qui se savent pas encore

vaut mieux les garder scred

\[Y\] : bien sûr, commence par le cadre : comment on fait pour agir ?

\[X\] : comment on se met à hacker, il y a pas de règles, faut
s'intéresser aux machines et bidouiller et voilà

jamais viser trop gros

toujours se coordonner à plusieurs

jamais rien dire sur toi

ne pas bouger avant d'être sûrex de ce que tu fais et des traces que tu
laisses

après, pour moi, le mieux c'est de comprendre les systèmes, d'identifier
la cible et d'être patienxte, tout en guettant ce qui bouge à
l'international

le mieux, mais on va en parler je pense, c'est d'infiltrer un réseau,
par exemple l'intranet d'une entreprise ennemie, et les surveiller
patiemment, pour comprendre ce qu'iels font, voir sans être vuex et
agir d'un coup, en piratant tous leurs documents ou en vidant leurs
comptes

ça tu peux le faire seul, mais en coordination internationale c'est
mieux

c'est à ça que servent les <a href="/glossaire/#irc" class="glossary-link" target="_blank" rel="noopener" data-no-swup>IRC°</a>

il y a des gens de partout, d'Inde, de Hong Kong, des USA, de Russie, de
plus en plus d'Afrique, des gens qui débarquent et qui disent "voilà
dans mon pays il se passe ça et ça, l'État est merdique, les gens sont
exploités, il faut agir, attaquer, dénoncer" et il arrive que ça
parte en conversation, que ça s'exporte dans d'autres cercles de forums
et que ça débouche sur des actions collectives

mais le plus souvent, on va pas se mentir, il se passe rien

faut économiser ton énergie

guetter la synergie collective

bon, après, je vais pas rédiger un manuel de hack, mais tu peux faire
plein de choses

\[Y\] : par exemple ?

\[X\] : le truc classique d'Anonymous c'est de <a href="/glossaire/#ddos-attack-verbe--ddosser" class="glossary-link" target="_blank" rel="noopener" data-no-swup>ddosser°</a> des cibles

ça veut dire que tu te synchronises à plusieurs, idéalement un petit
millier

tout le monde installe un logiciel comme HOIC qui surcharge un serveur
de connexions

si t'arrives à planter le serveur qui héberge le site cible, comme un
site scientologue ou le site de la NSA, comme ça a été fait par
Anonymous, en gros le site devient inaccessible tant que l'action dure,
après tu peux faire un communiqué en ligne pour expliquer pourquoi

après les plus fortexs hackent autrement le site et vont en plus venir
ajouter un message qui peut être sur la page d'accueil ou bien dans
l'adresse URL

je me souviens qu'on avait écrit une fois <a href="/glossaire/#acab-1312" class="glossary-link" target="_blank" rel="noopener" data-no-swup>ACAB°</a> dans l'URL d'une agence
de keufs aux USA

je me souviens plus laquelle

un petit commissariat de campagne qui avait buté des personnes pendant
une garde à vue[^14]

bon, le plus important, c'est de faire sortir des informations

c'est là que des gens se sont fait un nom, comme Hammond, Manning ou
Hamza

maintenant Chelsea Manning est libre, après 7 ans de prison, elle a fait
fuiter les principaux documents *classified* de l'armée américaine qui
se sont retrouvés sur WikiLeaks

Hammond vient d'être libéré, il y a deux semaines, pareil, presque dix
ans de taule

Hamza est encore en taule. Hamza a fait perdre près d'un milliard aux
banques américaines et a versé des centaines de milliers de dollars aux
luttes en Palestine

le protocole de Hamza, c'était un *spyware*, un petit virus qui
s'infiltrait partout et récoltait les données, comme les mots de passe
des comptes bancaires

faut leur donner de l'amour dans ce texte, à elleux et à touxtes les
autres

Free BX1 ! Free Hamza !

bref le protocole de ces personnes donne des résultats de ouf

mais, même à petite échelle, le vol de données reste le plus intéressant
je pense

pirater des informations

tu peux soit les diffuser à la presse

faire éclater des scandales comme les Anonymous ont beaucoup fait

surtout à l'époque où on soutenait à fond WikiLeaks

maintenant c'est plus compliqué, même s'il y a d'autres canaux, rien n'a
retrouvé la visibilité de WikiLeaks

c'est probablement le taf le plus important des hackeureuxses
aujourd'hui

faire éclater les barricades de la sécurité et l'impunité des
puissanxtes

après ça suffit pas toujours

on a révélé que la quasi-totalité des élites mondiales détournaient de
l'argent au Panama ou aux Caïmans

tout le monde s'en doutait hein

mais là c'était différent, les hackeureuxses et les journalistes ont
sorti des milliers de preuves, des documents comptables, des
enregistrements qui condamnent les politiciens, les patrons, les
sportifvexs, les artistes

et puis, il s'est juste rien passé

plouf dans l'eau

zéro sursaut de conscience de classe chez 99 % des gens

aucune émeute

c'est déprimant

donc il y a l'autre possibilité, plus discrète, ce qu'a fait Hamza

en gros, tu fais comme les pirates individualistes qui arnaquent des
privés ou des banques pour gagner de la thune sauf que tu reverses la
thune dans les luttes

moi je l'ai déjà fait

je volais de la thune à des privés riches

j'ai jamais fait de virement à des ONG

moi je refilais l'argent volé en cash partout où je passais

dès qu'il y avait un truc <a href="/glossaire/#prix-libre" class="glossary-link" target="_blank" rel="noopener" data-no-swup>prix libre°</a>

genre une soirée de soutien antifa

je lâchais des centaines de francs dans le prix libre lol

les gens hallucinaient mdrrrr

bref

mais la plus belle action que je connaisse à ce sujet c'est ce que fait
Phineas Fisher, une hackeuse espagnole

elle détourne des milliers de dollars pour arroser de thune les luttes,
comme les luttes *<a href="/glossaire/#native" class="glossary-link" target="_blank" rel="noopener" data-no-swup>natives°</a>* aux USA, les trucs anti-G20, les trucs
antiprison et aussi le Rojava je crois (je peux pas être sûr à 100 % d'où
vient l'argent)

mais le plus cool, c'est qu'elle publie des fanzines dans lesquelles
elle explique tout son mode d'emploi

en gros, elle fait ce que je disais avant, elle repère une petite
banque, style ces microsociétés qui servent qu'à faire des transactions
pour les ultrariches là

avec des petits scripts de code

elle infiltre leurs réseaux, leurs boites mails, leurs logiciels de
compta ou de transactions bancaires

personne sait qu'elle est dans leurs réseaux, qu'elle observe tout ce
qui se fait, tout ce qui se dit

elle attend patiemment de comprendre comment fonctionnent les
employéexs, les transactions

et une fois qu'elle est sûre, elle attaque d'un coup, elle lance
plusieurs virements internationaux et elle efface toutes ses traces

c'est un hold-up de haut vol

c'est souvent de bonnes cibles, ces petites boîtes

c'est inutile d'essayer d'attaquer des monstres genre UBS ou les
services de renseignements

mais dans ce merveilleux techno-capitalisme mondialisé, il y a beaucoup
d'intermédiaires, et les intermédiaires sont fragiles

toute l'affaire des *Panama papers* a éclaté à cause d'un petit cabinet
d'avocaxtes tout pourri, avec quelques employéexs, basé sur un paradis
fiscal ; iels avaient une tâche très secondaire dans le réseau de
blanchiment, mais leur site c'était un vieux truc bricolé, limite un
*wordpress* gratuit mdr

iels se sont faits infiltrer hyper facilement

et dans leur base de données, il y avait des fichiers avec des mots de
passe d'entreprises sœurs

et de fil en aiguille

à partir d'une petite faiblesse d'une microsociété

on a tiré toute la pelote des *Panama papers*

et bim, voilà comment on passe d'un *wordpress* mal sécurisé à l'un des
plus grands scandales de l'histoire financière mondiale

c'est vraiment ça aujourd'hui, lutter en ligne, c'est trouver la toute
petite faille humaine dans le réseau

il y a des gens qui ont aussi une technique simple, le *social hacking*,
en gros tu chopes le numéro d'unex collaborateurixe de seconde zone, tu
l'appelles, tu fais croire que t'es le service informatique de la boîte
en dissimulant ton numéro, tu chopes son mot de passe en lui faisant
croire je ne sais quoi, et boom, t'as infiltré l'intranet

ça peut aussi se faire avec du *<a href="/glossaire/#phishing" class="glossary-link" target="_blank" rel="noopener" data-no-swup>phishing°</a>* par mail, mais ça tout le
monde connaît

souvent, la différence entre le hack de pirate débile et le hack
révolutionnaire, c'est pas les méthodes, c'est juste les cibles, le
message politique et là où tu envoies l'argent et/ou les informations
que t'as récoltés

plus récemment, avec une technique similaire, on a piraté les données de
surveillance de la police de Hong Kong en chopant les accès d'une
start-up qui gérait une sous-tâche de la reconnaissance faciale par les
caméras posées dans les rues, un clic et tu supprimes les fichiers
d'identification des manifestanxtes, ça a sûrement épargné pas mal de
procès

c'était pas mal cette opération, pas les grandes heures de ce qu'on a
connu avec Anonymous, un peu chaotique, mais bien menée quand même

le dernier truc qui me semble pertinent et que j'ai un peu fait, c'est
attaquer tout le monde du hack qui se fait coopter

les hackeureuxses militanxtes c'est vraiment 1 %

il y a des pirates qui la jouent solo

même pas anars, juste des libertariens insupportables, genre qui
braquent des banques pour leur pomme

mais surtout, il y a les hackeureuxses du grand Capital

il y a celleux qui bricolent des logiciels de surveillance, des
virus-espions qu'iels vendent à la police

il y a celleux qui testent les failles des sites ou des bases de données

il y a celleux qui bossent comme détectives privéexs pour des
multinationales

ça c'est vraiment la lie de la communauté, ce que le cyberespace a
produit de plus merdiques comme individus

et c'est la majorité

en vrai, tu peux te faire des fortunes comme ça

et surtout, tu deviens très riche en toute sécurité et en toute légalité

tu dois pas te planquer en permanence

tu peux travailler avec le matos surpuissant des boîtes ou des
entreprises

il y en a de plus en plus des saletés du genre

un autre taf révolutionnaire, mais là c'est vraiment réservé aux
expertexs, c'est justement d'attaquer ces programmes-là, de les
comprendre, de révéler l'ampleur de la surveillance mondiale

ces hackeureuxses cooptéexs, ces *<a href="/glossaire/#white-hats" class="glossary-link" target="_blank" rel="noopener" data-no-swup>white hats°</a>*, c'est les mêmes qui
bricolent des programmes de reconnaissance faciale

dans le cadre d'Anonymous, un truc assez efficace, c'est de soutenir les
activistes physiques en les aidant à contourner la censure, à surveiller
leurs machines et leurs serveurs pour voir s'iels sont écoutéexs, à les
aider à accéder à des informations que le gouvernement dissimule

dans le cadre des Printemps arabes, il y a eu une énorme mobilisation
internationale pour soutenir les émeutes, pour contourner les pare-feux,
déjouer la censure, arrêter les écoutes

c'est le truc le plus cool je crois, se coordonner à l'international
pour jouer un rôle de support, un soutien, on enlève une charge mentale
à pas mal de militanxtes

pour parler d'autres trucs positifs, c'est arrivé que des hackeureuxses
pas très politiséexs se voient misexs au défi de craquer ces trucs, par
Anonymous par exemple, alors là leur égo devient utile sur le plan
politique, iels passent des heures à essayer de démontrer qu'iels sont
les plus fortexs, mais au moins à la fin, on affaiblit les hackeureuxses
contre-révolutionnaires

et il y a de plus en plus de taf, le hack se gentrifie, il y a toujours
plus d'entreprises qui viennent coopter les petixtes géniexs qui sortent
des écoles pour les endoctriner et les mettre au service de la sécurité
des riches

\[Y\] : et c'est quoi les trucs négatifs dont tu parles pas ?

\[X\] : il y en a plein

y'a la dépression qui frappe, l'isolement social des hackeureuxses, la
neutralisation émotionnelle de la génération des enfants du web,
l'absence de joie dans la lutte, le fait que tout est mathématisé,
mécanisé, inhumain, franchement, il y a des jours où c'est dur 😞

moi j'en vis pas, je refuse de me faire de l'argent, mais c'est encore
pire pour celleux qui en vivent, iels ont vraiment plus aucune raison de
sortir dehors

il y a la compétition technique, la course à qui est lae meilleurex et
aussi les trolls, ce qui va ensemble, je connais un hackeur militant qui
s'est fait balancer aux flics par un collectif de hackeureuxses trolls
apolitiques qui voulait juste démontrer qu'iels pouvaient découvrir sa
vraie identité, à la base iels s'étaient embrouilléexs sur une connerie,
voilà le genre de monde où on traîne

avec ça, il y a les violences sexistes aussi, une atmosphère souvent
misogyne ou transphobe, c'est vraiment un monde de mecs imbus, un repère
d'*<a href="/glossaire/#incels" class="glossary-link" target="_blank" rel="noopener" data-no-swup>incels°</a>*, un monde où c'est trop lol d'uploader des images
pédopornographiques sur des sites pour enfants

heureusement, y'a de plus en plus de cyberactivistes féministes et <a href="/glossaire/#queer" class="glossary-link" target="_blank" rel="noopener" data-no-swup>queer°</a>
qui viennent voler dans les plumes de cet aspect merdique et transformer
la culture numérique

je veux pas faire une image horrible du truc, c'est une contre-culture
communautaire, donc il y a plein de dérives et de merdier

mais le vrai message que je veux faire passer, c'est que tout le monde
doit apprendre à comprendre au moins les bases, pour arrêter d'être trop
contrôlables, trop gouvernables

en vrai, la communauté hack, on s'en fout, c'est une méga minorité, la
vraie question sociale du net, c'est la majorité, c'est le fait de
donner du pouvoir au peuple par la maîtrise du réseau et de
l'information, par la maîtrise du code et des langages de programmation,
par la compréhension de la surveillance, du guidage, ne serait-ce que de
l'autovalidation de ce que tu penses

le problème c'est le fait que les algorithmes t'enferment dans ce que tu
penses, ne te présentent que des contenus avec lesquels tu es a priori
d'accord, et surtout le fait que c'est complètement la direction que
prend le web 3.0, l'internet de demain

pour les gouvernants, c'est super d'avoir des sujets à gouverner
emprisonnés dans des petites bulles confortables, y compris les
militanxtes de notre côté de la barricade hein

ce truc d'autovalidation algorithmique clive les sociétés, fait que les
gens se divisent sur des détails, se rencontrent plus, ça détruit les
espaces de sociabilité dans lesquels tu peux constater que les
différences sont pas si graves que ça dans la vraie vie

pour moi, cette autovalidation par les algorithmes, c'est le plus gros
danger technique qui pèse sur la démocratie, je déconne pas, c'est hyper
sérieux

d'où l'importance d'une conscience populaire du piège, conscience qui
s'acquiert seulement vraiment quand tu commences à comprendre comment
fonctionne un algorithme, une base de données, un réseau social

après, quand on arrive à de véritables actions de cyberguérilla, comme
celles dont on a parlé, c'est super, c'est un très bon moyen de résister
à l'expropriation capitaliste qui existe depuis toujours et qui se
reproduit en mode dérégulation puissance 1000 sur le web

mais le plus important, c'est que la population se laisse pas avoir par
cette transition douce, c'est qu'on fasse circuler suffisamment la
connaissance pour que touxtes réalisent qu'iels ne veulent pas avoir un
keuf dans chaque maison

\[Y\] : le mot de la fin ?

\[X\] :

{{%centre%}}

feu aux prisons

de béton et de pixels

 

*Transparency for the powerful*

*Privacy for the weak*

{{%/centre%}}

[^13]: Pour tenter désespérément de communiquer de façon plus sécurisée, lire *Camouflage dans l'infosphère* \[n^o^ 40\].

[^14]: Pour savoir comment d'autres luttent contre les violences après arrestation, lire *Brisons l'isolement* \[n^o^ 39\].

