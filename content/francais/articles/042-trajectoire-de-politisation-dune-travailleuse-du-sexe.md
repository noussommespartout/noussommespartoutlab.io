---
author: "AL."
title: "Nous sommes touxtes des putes"
subtitle: "Trajectoire de politisation d'une travailleuse du sexe"
info: "Transcription d'un entretien"
datepub: "novembre 2020"
categories: ["féminismes, questions de genre", "travail du sexe", "relation à la militance", "syndicalisme, luttes des travailleureuxses"]
tags: ["corps", "dissonance", "détruire, destruction", "force", "gueule", "internet, web", "mcdo", "merde", "pouvoir", "sexe", "usine", "violence", "voix"]
langtxt: ["fr"]
zone: "Suisse romande"
num: "42"
quote: "la vulve n'a pas plus de valeur que le cerveau"
---

*Je ne souhaite pas que ce texte soit lu à haute voix et en public par
un homme <a href="/glossaire/#hommes-cis--mecs-cis--cisgenres" class="glossary-link" target="_blank" rel="noopener" data-no-swup>cis°</a>*[^116]

{{%ligneblanche%}}

J'ai commencé le <a href="/glossaire/#travail-du-sexe--travailleureuxses-du-sexe" class="glossary-link" target="_blank" rel="noopener" data-no-swup>travail du sexe°</a> à 16 ans.

Un bar à strip-tease, par curiosité. Ça participait d'une espèce de
désir d'expériences un peu *borderline*, d'un rapport au corps un peu
particulier. J'ai grandi avec un corps qui n'était pas dans la
"norme", un corps dont on se moquait à l'école. Et puis, un jour, ce
corps a changé. Il est revenu entre mes mains et c'est devenu un objet
avec lequel je pouvais reprendre du pouvoir et gagner de l'argent.
C'était une activité qui me semblait évidente. Je suis sortie du lycée
sans trop savoir quoi faire et ça me semblait facile. Ça me convenait
assez bien de travailler sans trop faire d'efforts. Aussi, ça me
permettait de prendre de l'argent à des mecs qui ont de l'argent à
dépenser pour ça. En leur prenant leur argent, tu leur prends une part
de leur pouvoir, quelque part --- mais bon, ça c'est des trucs auxquels
j'ai réfléchi a posteriori, je ne les avais pas verbalisés sur le
moment. C'était gratifiant d'être payée pour faire un travail avec mon
corps et de faire avec mon corps des choses qui m'appartenaient
vraiment. Tout allait très bien et à ce moment-là, je ne pensais pas
forcément aux conséquences sociales qui allaient suivre. En tous cas,
c'était super. J'étais au clair avec mes désirs, avec ce que j'avais
envie de faire et avec mes limites aussi. Je dis pas que c'est facile
pour tout le monde, mais pour moi ça l'était.

À l'époque, je travaillais seule, j'en parlais pas, j'avais pas de
copine qui faisait du travail du sexe (ci-après TDS). J'habitais dans
une petite ville, donc vraiment fallait pas trop que ça se sache.
J'avais peut-être conscience, au fond, que socialement c'était mal vu.
Et puis, de fil en aiguille, j'ai eu plusieurs activités : massages
érotiques, escorting sur internet, j'ai fait de la vidéo et j'ai
travaillé sur des tournages. J'ai fait plein de métiers différents dans
l'industrie du sexe. C'est durant cette période que j'ai déménagé à
Paris et j'y ai rencontré plein de monde. Il n'y a pas que des gens bien
intentionnés dans l'industrie du sexe, on ne va pas prétendre que tout
est rose, loin de là, mais finalement, j'ai fait plein d'activités
différentes et aucune ne m'a traumatisée. Sans vouloir rendre toute rose
la clientèle non plus, j'ai eu la chance de travailler auprès de
personnes qui étaient plutôt *clean*. Je n'ai jamais eu de problème
d'agressions, ce qui n'est pas le cas de touxtes les TDS. Je ne suis
jamais tombée sur des hommes qui considéraient qu'ils pouvaient abuser
de leur pouvoir. Au contraire, ils étaient plutôt timides, ils voulaient
souvent parler. Je n'ai pas eu affaire à ces "clients horribles" qui
habitent nos imaginaires nourris par la presse, la politique et le
discours public. En fait, les violences sexuelles envers les <a href="/glossaire/#femmes" class="glossary-link" target="_blank" rel="noopener">femmes*°</a>,
c'est dramatique et malheureusement très courant, il n'y a pas besoin
d'être TDS pour en faire l'expérience. Mais quand on parle de
prostitution, de celleux qui "font ça", on envisage directement cet
imaginaire de l'horreur, de l'abus, de la victime. Ça ne collait pas
avec mon expérience. Il y avait une dissonance entre ma réalité et le
discours qui était posé dessus.

Au fil du temps et à force de lire et d'entendre les propos des
mouvements <a href="/glossaire/#abolitionnisme" class="glossary-link" target="_blank" rel="noopener" data-no-swup>abolitionnistes°</a>[^117] du TDS, cette dissonance a augmenté
et j'ai commencé à me sentir mal. Je me disais que, peut-être, c'était
pas bien ce que je faisais, que peut-être j'étais une mauvaise personne,
une mauvaise femme, une mauvaise féministe. Tous ces discours ont
commencé à tourner dans ma tête et à insidieusement rentrer dans mon
corps. J'ai commencé à ne plus être à l'aise, je trouvais mon corps de
plus en plus sale. Et, au final, c'est quand j'ai arrêté le TDS que ça a
été le pire. C'était difficile de découvrir que d'autres femmes\*
étaient rebutées, voire offensées par mes activités, alors que je
considérais le TDS comme un taf. Ça ne conditionnait pas toute mon
identité.

Après cette période de lutte avec moi-même, où j'essayais de dealer avec
cette dissonance entre un prétendu "bien" et un prétendu "mal" ---
comme s'il n'y avait que ces deux options --- j'ai commencé à lire beaucoup
d'ouvrages féministes <a href="/glossaire/#pro-sexe" class="glossary-link" target="_blank" rel="noopener" data-no-swup>pro-sexe°</a> et pro-putes. Ça me faisait vachement de
bien, mais ça restait hyper abstrait, je ne connaissais pas de
féministes qui tenaient ce type de discours, je me demandais où étaient
toutes ces personnes si incroyables. Dans ma tête ça n'existait que dans
les livres. C'est en arrivant à Lausanne que j'ai pu enfin parler
ouvertement de ce sujet. J'ai pu en parler avec d'autres femmes\*, des
femmes\* que j'admire en tant que féministes et en tant que
militanxtes. Il y a eu, pour la première fois, une validation de mes
ressentis, de mon vécu et de mes opinions. C'était hyper agréable à
entendre. Ça m'a permis de sortir de cette période d'humiliations et
d'autoflagellations et j'ai pu transformer tous ces sentiments en une
lutte contre le système qui crée cette culpabilisation, contre le
système qui légitime qu'on dise du mal de meufs\* parce qu'iels ont des
activités X ou Y.

Tout à l'heure, je disais que j'avais gagné beaucoup d'argent, mais il
faut relativiser. J'ai jamais eu de statut, de cotisations sociales,
tout ça c'est aussi du <a href="/glossaire/#précariat" class="glossary-link" target="_blank" rel="noopener" data-no-swup>précariat°</a>. J'ai des trous gigantesques dans mon
CV, comment tu fais pour justifier ça ? Qu'est-ce que tu vas dire lors
d'un entretien ? "Oui, là j'ai eu des activités de prostitution, c'est
cool" ? Il n'y a pas grand monde qui peut entendre ça, même en Suisse,
même si c'est légal. Il y a toute une symbolique victimisante autour des
travailleureuxses du sexe : "Oh la pauvre meuf qui savait pas quoi
faire et qui du coup a fait du TDS". Pour moi, il faudrait une vraie
révolution de la vision du corps des femmes\* et de leur sexualité. Si
on ne change pas radicalement cette symbolique victimisante, t'as beau
légaliser le TDS, ça ne changera pas la manière dont on nous perçoit,
dont on nous traite. Et dans les mouvements féministes, c'est pareil,
c'est ça qu'on doit faire, changer la façon dont est perçu le TDS,
accepter le fait qu'on a le droit d'avoir un corps et de faire des
choses avec si on est consentanxtes, tout simplement. Faut juste nous
laisser tranquillexs.

Petite parenthèse pour illustrer le poids très concret que peut avoir
cette symbolique sur une vie : après le TDS, je me suis formée, et puis
j'ai été, un temps, éducatrice en milieu scolaire. Dans les millions de
contenus qui existent en ligne, il a fallu qu'un jour, un étudiant tombe
sur une de mes vidéos. Ça a fait le tour du collège en deux heures.
Plein d'étudiants se sont moqués assez violemment. Par contre, des
étudiantes --- que des filles, hyper jeunes entre 13 et 14 ans --- sont
venues me voir pour me faire des câlins et pour me dire que c'était pas
grave, que je faisais ce que je voulais, qu'il y avait pas de soucis.
C'était méga chou. De l'autre côté, je me suis faite humilier par deux
supérieures et la directrice du collège m'a demandé de démissionner. À
l'époque, je savais pas que c'était illégal de sa part de faire ça. Tu
ne peux pas pousser une personne à démissionner. Mais comme il y a toute
cette symbolique autour du TDS, au fond de moi, je me disais que c'était
normal qu'on me traite comme une merde, que c'était bien fait pour moi.
C'est seulement plus tard que j'ai réalisé que ce n'était absolument pas
normal. C'est pour ça que j'insiste sur cette symbolique, sur ce qu'elle
nourrit. Et c'est pour ça que je suis hyper choquée de devoir encore
expliquer pourquoi elle nous fait du mal dans la vie de tous les jours.
Elle nous empêche d'accéder à des droits aussi basiques que le droit du
travail, dans le TDS mais aussi en dehors !

Ensuite, de manière générale, j'ai l'impression qu'il y a souvent un
problème avec le sexe dans les mouvements féministes. C'est aussi, je
crois, lié au rapport qu'on a avec la violence. La violence sur nos
corps, c'est un truc qu'on a intégré depuis touxtes petixtes, parce
qu'on nous a socialiséexs de cette manière. Du coup, tout ce qui est de
l'ordre de la violence dans le sexe, tout ce qui est un peu trash, on se
dit que l'on ne devrait pas aimer. Mais en fait, le sexe c'est à la fois
très ancré dans le quotidien, à la fois un petit peu à part, dans nos
intimités. Tu peux aimer des choses, avoir des fantasmes et des
pratiques qui n'ont rien à voir avec la manière dont tu te comportes
dans le reste de ta vie. Et dans tes fantasmes, dans tes pratiques
sexuelles consenties pendant lesquelles tu as l'espace de poser tes
limites, il ne devrait y avoir aucun problème. Parfois, j'ai
l'impression que pour certaines camarades féministes, il ne faudrait pas
qu'on aime le sexe. Soit il faut qu'on l'aime vanille et tout mignon,
soit il faut qu'on ne l'aime pas du tout. Pourquoi ? Pareil avec le
porno. C'est pas en critiquant les femmes\* qui en font (ou qui en
consomment) qu'on va régler le problème des conditions de production et
d'exercice. J'ai des copines sur Paris qui ont été payées 300 € la scène
pour avoir, tout au long de leur vie, leur gueule sur internet. Ça ne
sert à rien de dire "le porno c'est mal". Oui, 99 % des productions
pornographiques sont merdiques, elles ne proposent rien d'intéressant,
pas de nouveaux imaginaires, pas d'alternatives[^118], mais c'est pas
en voulant abolir et en diabolisant ce métier, qu'on va résoudre le
problème de nos copainexs super mal payéexs.

Posons-nous les questions autrement. Le pouvoir réside dans l'argent et
je n'ai jamais rencontré de féministes abolitionnistes du TDS qui ont
radicalement envie de détruire le système capitaliste. Peut-être qu'iels
existent, mais je ne les ai pas trouvéexs. J'ai l'impression que les
abolitionnistes s'attaquent à la pointe d'un iceberg, à un problème de
surface et pas à ses racines. Ce sont ces racines qu'il faudrait
attaquer. Peut-être qu'on pourra, dans un monde meilleur, se permettre
de se battre uniquement pour des imaginaires alternatifs et des
productions visuelles de qualité, plus intéressantes et dénuées
d'objectifs capitalistes. Il ne faut pas oublier que l'industrie du
sexe, c'est un énorme business, c'est beaucoup d'argent et il y a des
travailleureuxses précairexs là-derrière. Dans les luttes syndicalistes,
on ne milite pas pour l'interdiction pure et simple de pizzerias qui
livrent à domicile, on lutte pour obtenir des conditions de travail
dignes, on lutte contre les patrons qui précarisent leurs employéexs. Ça
devrait être pareil pour le porno ou le TDS.

Je reviens sur le jeune âge auquel j'ai commencé à faire du TDS et sur
les réticences qu'on peut avoir à cela. On vit dans un monde qui
valorise la jeunesse, la "fraîcheur". Cette valorisation se retrouve
dans tous les domaines et boulots ayant un rapport avec le corps et
l'image. Quand j'ai commencé, j'avais 16 ans et personne ne m'a demandé
mon âge. C'est ça qu'on devrait remettre en question, et pas mes motivations à
moi. Pourquoi, des hommes adultes peuvent se permettre de ne pas
demander l'âge de femmes\* très jeunes qu'ils payent pour les voir
bouger, pour profiter de leur présence, ou pour des faveurs sexuelles ?
Pourquoi la jeunesse, et particulièrement la jeunesse des femmes\*,
est-elle autant prisée ? Pourquoi dès que t'as 30 ans, t'es un peu
périmée ? Je considère que si à 16, 17 ans tu peux faire des petits
boulots au mcdo, tu peux aussi faire du TDS --- toujours bien sûr, dans
un cadre posé, avec les limites que tu choisis, etc. De mon point de
vue, il n'y a pas de différences entre le TDS et le travail avec son
corps dans une usine, ou tu mets ta santé en jeu par exemple. À mon avis
(c'est que mon avis), la vulve n'a pas plus de valeur que le cerveau,
qui n'a pas plus de valeur que les bras. Si on veut transformer
l'imaginaire social ambiant qui valorise les corps très jeunes et
légitime les comportements d'hommes très vieux ayant le pouvoir et
l'argent, je ne crois pas qu'il faille cloîtrer les mineurexs pour
qu'iels ne croisent pas ces mecs-là. Je ne vois pas pourquoi c'est
inacceptable qu'une fille de 16 ans se dise : "Ah tiens, là, il y a un
vieux type qui a de l'argent et qui est un petit peu naïf parce que je
suis gentille avec lui --- je schématise bien sûr --- ça me permettrait de
payer mon permis et des études". Ben c'est cool, vas-y ! Si ça te
permet de te barrer de chez toi, de passer à autre chose dans ta vie,
mais vas-y, aucun souci. Ça ne veut pas dire qu'il n'y a rien à
déconstruire dans la démarche de ce vieux gars.

Le TDS reste encore et toujours un gros tabou, une question qui pose
problème. Et s'il y a encore des copainexs militanxtes qui ne sont pas
convaincuexs que le TDS est légitime, ou qui ne sont pas convaicuexs
qu'il faut travailler et militer *avec* ces personnes pour leur donner
accès à un maximum de droits en tant que travailleureuxses, j'ai envie
qu'iels lisent ça et qu'iels réalisent que tout ce discours victimaire
finit par transformer des personnes qui se sentaient relativement bien
en victimes, alors qu'elles pourraient ne pas l'être. Ça les empêche
d'agir, d'avoir du pouvoir sur leurs corps et leurs activités. C'est
pour ça que j'en parle.

[^116]: Cette indication est liée aux sessions d'écoute-lecture, pour en savoir plus, lire l'introduction.

[^117]: *Pas de féminisme sans les putes* \[n^o^ 37\] aborde aussi la question de la place du TDS dans les milieux féministes. Lire aussi *Je suis une pute* \[n^o^ 2\], un petit guide pratique pour putes révolutionnaires.

[^118]: Pour une discussion sur le porno alternatif et ses nouveaux imaginaires, lire *Tout est porno* \[n^o^ 46\].
