---
author: "Aladin Dampha"
title: "They don't see us"
subtitle: "Ils ne nous voient pas"
info: "Transcription d'un entretien oral (traduit de l'anglais)"
datepub: "janvier 2021"
categories: ["prison, justice, répression", "squat, occupations, logement", "luttes migratoires", "antiracismes", "police"]
tags: ["arrestation", "art, artiste", "confiance", "film", "foot", "force", "fourmi", "France", "injustice", "justice", "machine", "nuit, obscurité", "passeport", "peur", "pouvoir", "rue", "sabot, saboter, sabotage", "téléphone", "violence", "voix"]
langtxt: ["de", "fr"]
zone: "Suisse romande"
num: "4"
quote: "c'est la violence de l'État et l'État n'existe que grâce au peuple"
---

J'ai commencé à organiser des actions contre la violence policière en
Suisse avec Jean Dutoit[^7bis], un collectif luttant pour les droits des
personnes sans-abri. Tout a commencé dans un lieu appelé le Sleep-in.
Là-bas, il y avait beaucoup de gens, principalement des réfugiéexs, des
migranxtes, la plupart d'Afrique de l'Ouest, surtout du Nigeria et de
Gambie. À un moment, le Sleep-in est devenu bien trop petit pour
accueillir tout le monde. Alors, un mouvement a émergé, composé de nous,
les gens qui dormaient dehors, et du personnel du Sleep-in. On s'est misexs
d'accord pour ouvrir un squat touxtes ensemble. La première maison qu'on a
occupée se trouvait dans le quartier de Fourmi, à Lausanne. On y a passé
trois semaines et on est partiexs, parce que la maison devait être
détruite pour construire une école. Il y a quelques jours, quelqu'un m'a
dit que les lieux sont toujours vides. On a déménagé dans l'ancienne
usine heineken à Renens. On y est restés toute une année.

C'est là qu'on a commencé à militer contre les injustices et les
violences policières. En luttant pour les maisons du collectif Jean
Dutoit, j'ai eu la possibilité de rencontrer des officiellexs. C'était
un peu risqué, mais il fallait qu'on les confronte à nos problèmes, pour
leur expliquer nos revendications. On a décidé de prendre ce risque
parce que ce n'est pas juste que ce soient toujours des personnes blanches
qui soient toujours en première ligne, qui disent aux officiellexs ce
dont on a besoin, alors qu'elles ne vivent pas dans ces maisons. Alors,
je faisais toujours partie de ces discussions, j'ai été impliqué dans la
plupart des négociations politiques. J'ai même eu ma tête dans le *20
Minutes*. (*Rires.*)

De là, j'ai rejoint d'autres luttes, des collectifs qui se battent
contre le racisme et contre le système carcéral. Je fais partie d'un
soundsystem qui fait des collectes de fonds pour les
personnes en prison qui n'ont pas de moyens ou de famille pour les
soutenir. On fait des fêtes, on récolte de l'argent pour elleux, on leur
achète du crédit pour leur téléphone, du tabac, des trucs, des piles. On trouve
même des avocaxtes, quand c'est nécessaire.

Ensuite, on a fondé le collectif Kiboko pour récolter des témoignages à
propos de la brutalité policière, en se demandant comment on pouvait les
rendre publics. On voulait lancer quelque chose pour la défense de nos
droits face à la police. Beaucoup ne connaissent pas leurs droits. Les témoignages
racontaient deux ou trois crises, comment la police les a gérées et ce
qui était juste ou non. Ça a sensibilisé sur les expériences des Noirexs
et comment iels peuvent se comporter quand la police est dans les
parages. Plusieurs personnes du collectif ont suggéré d'en faire un
film sur la situation et sur les modes d'action disponible pour
combattre le système. Le film s'appelle "No Apologies", il donne la
parole à de nombreuses personnes à propos de leurs expériences avec la
police, comment iels ont été confrontéexs à la brutalité, à la
discrimination, au profilage racial, à l'injustice et à la violence.

Quand je suis arrivé en Suisse, j'ai été surpris de remarquer que la
plupart des gens n'ont aucune idée de comment faire face à la brutalité
et qu'iels ne connaissent rien de leurs droits. Quand on n'a pas de
papiers, on n'a absolument aucun privilège et aucune arme face à la police.
On se sent entièrement responsable de tout ce qui nous arrive parce
qu'on n'a pas le droit d'être là. Mais en tant qu'être humain, on a le
droit d'être là où on veut.

Maintenant, malgré le fait que j'ai le droit de vivre en Suisse, la
situation n'a pas vraiment changé. La police est toujours violente avec
moi, comme avec beaucoup de personnes noires ; ils nous voient toujours
comme des personnes sans droits. Je vis toujours la même violence, sauf
que maintenant je peux résister d'une certaine manière.

Une fois, j'étais en Italie et j'ai pris le dernier train de Milan pour
la Suisse. C'était aux environs de 19 heures. J'avais tout avec moi :
mon ticket, ma pièce d'identité, mon passeport. J'avais un petit bagage
parce que j'avais passé deux jours seulement en Italie. J'étais le seul
Noir dans le wagon. Un contrôleur m'a demandé mon billet, je lui ai
tendu avec confiance. Je me souviens que je regardais un match de foot
sur mon téléphone. Il a directement fait un signalement aux douaniers,
en disant littéralement :

--- Il y a un homme noir dans le train.

J'ai vu deux officiers de police des douanes s'approcher et me parler en
allemand. Je leur ai dit :

--- Je ne parle pas allemand.

Ils m'ont parlé en Italien. Je leur ai répondu :

--- Oui, je vais à Lausanne.

Ils m'ont demandé mes papiers. J'ai dit :

--- Non, pourquoi ? Je suis assis au milieu du train, je ne suis pas la première personne que vous voyez.
Il y a d'autres personnes dans le wagon par lesquelles commencer si
vous voulez faire des contrôles d'identité.

Ils m'ont répondu :

--- Oui, mais nous devons vous fouiller.

--- Non, contrôlez d'abord les gens là-bas avant d'arriver à moi. Ça
serait juste pour tout le monde.

Mais ils ont insisté, ils devaient me fouiller. Ils m'ont fait arrêter
mon match de foot et éteindre mon téléphone pour que je leur montre mes
papiers. Ils voulaient juste montrer aux gens qu'ils faisaient leur
boulot. Je leur ai montré mon passeport gambien. Je savais que si je
leur montrais ce passeport-là, ils n'auraient pas ce qu'ils voulaient.
Alors j'ai décidé de saboter leur journée et de leur faire perdre leur
temps.

J'ai insisté avec mon passeport gambien, il a dit :

--- Vous devez me suivre.

--- Non, je ne dois pas vous suivre. Rendez-moi mon passeport et je vous
donne ce que vous voulez.

Il a demandé :

--- Qu'est-ce que vous voulez me donner?

--- Rendez-moi mon passeport et je vous donne ce que vous voulez.

Finalement, je lui ai montré mon permis B, mais ça ne s'est pas arrêté
là. Ils ont pensé que c'était un faux. Ils ont essayé de le contrôler,
d'appeler des numéros et ils se sont rendu compte que tout était en
règle. Le contrôleur a dû me rendre mon bagage et mes affaires.

Je lui ai dit que s'il était allé contrôler d'abord d'autres gens avant
de se précipiter vers moi, il aurait peut-être trouvé des Blanchexs sans
papiers valables. Il a refusé de parler de ça. Quelques minutes plus
tard, quand il est parti, un homme italien assis à côté de moi m'a dit :

--- On n'a pas de papiers suisses, on est Italiennexs, pourquoi il ne
nous a pas contrôléexs ?

--- Vous n'êtes pas noirexs et vous êtes européennexs, il s'en fout.

--- Oui, mais on est en Suisse, toi tu as des papiers plus valables que
les nôtres, nous on n'est pas Suisses.

Une autre histoire. C'était à Lausanne. Ma machine à laver était en
panne, alors je suis allé dans un lavomatic près de chez moi où je
devais attendre une heure. J'ai décidé d'aller à la bibliothèque à
Chauderon pour passer le temps. J'ai regardé un film et, en revenant,
devant les escaliers qui mènent à l'arrêt de bus, j'ai hésité sur le
chemin à prendre. Un flic m'a vu. Parce que j'étais hésitant, il a cru
que j'essayais de lui échapper. Il s'est précipité vers moi :

--- Contrôle ! Vous essayez de m'éviter ; vous vendez de la drogue ici.

--- Quoi?

--- Oui, vous dealez ici.

--- Non, je ne vends pas de drogue. C'est juste que j'avais une heure à
attendre pour que ma machine à laver se termine.

--- Ouvrez votre sac!

J'ai dit non. Il m'a plaqué contre le mur, rapidement et violemment. Une
amie a vu la scène, elle est venue vers moi et a demandé :

--- Vous lui faites quoi ?

--- Il vend de la drogue.

Il m'a dit :

--- Allez, tu mens.

Je lui ai montré mes papiers, j'ai pris mon permis dans mon
porte-monnaie et je l'ai jeté par terre. Il l'a ramassé et m'a dit :

--- Pourquoi est-ce que tu l'as jeté par terre, tu voulais t'échapper en
courant ?

--- Non, je ne veux juste pas être poli avec vous. Vous avez mes
papiers, mais vous n'en avez rien à foutre.

Il a refusé de voir mes autres papiers, refusé de parler avec nous et
nous a dit de bouger de là. On ne voulait pas partir parce que cet
endroit ne lui appartient pas. C'est devenu marrant parce que tous les
gens de l'arrêt de bus ont commencé à s'arrêter pour nous regarder.
C'était trop cool parce que ça nous a donné plus de confiance : les gens
voyaient comment la police se comporte, iels voyaient que ça n'est pas
juste et qu'il n'avait aucune preuve pour m'accuser. Il a parlé de
drogue alors qu'il n'y avait pas de drogue. Les gens pouvaient voir mes
papiers. J'étais très heureux. Pour la première fois, j'étais confronté
à la police avec des témoins[^8]. Le flic a eu l'air stupide et tout
le monde a vu à quel point son comportement était injuste.

Avant la sortie de *No Apologies*[^8bis] en 2015, quelques policiers me
connaissaient déjà. C'est pour ça que je n'avais pas peur de participer
et d'apparaître dans le film. Certains flics, chaque fois qu'ils me
voyaient en ville me criaient : "Dampha, je t'ai dit, je ne veux plus te
voir ici." Dans l'espace public. Parfois ils m'embarquaient dans les
toilettes publiques, parfois ils me frappaient, me laissaient seul dans
des gares, tout ça sans aucune charge. Ils m'ont beaucoup fait souffrir.
Maintenant que j'ai des papiers, chaque fois que je croise ces keufs, je
leur demande de me contrôler.

J'espère que le film *No Apologies* aidera à modifier les relations de
pouvoir. L'idée était de changer l'état d'esprit de celleux qui nous
considèrent comme des moins que rien. La plupart des gens en Suisse ne
veulent pas bouger le petit doigt pour aider, pour lutter. Les gens
parlent constamment de la brutalité policière en France ou aux
États-Unis, mais continuent de penser qu'en Suisse "c'est cool". Mais la
Suisse n'est pas cool, la Suisse est violente et personne ne veut le
voir.

Quelque part, j'espère que ce film va effrayer la police. Mais
évidemment, le film lui-même n'a pas créé de vraie protection. Un film
ne peut pas modifier tout un système basé sur la violence qui
maintient les gens dans la précarité. Les personnes sans-papiers ne
peuvent pas avoir de job, le seul endroit où elles auraient le droit de
travailler, c'est en prison. Elles se retrouvent souvent en prison parce
que c'est illégal d'être dans la rue et qu'elles ne peuvent pas payer les
amendes[^9]. En prison, elles gagnent genre trois francs par heure. En
sortant de prison, elles ne peuvent pas travailler légalement. C'est un
cercle vicieux. Elles peuvent aussi trouver des jobs illégaux, mais quand
tu vis comme ça, chaque jour est dangereux[^10]. Le système est fait
pour garder les personnes sans-papiers dans des endroits spécifiques de
la ville, c'est une décision politique. Tout est fait pour que les
Noirexs se sentent en insécurité.

Les gens parlent constamment de ce qui est arrivé à George Floyd aux
États-Unis. Mais il est arrivé exactement la même chose à Mike Ben Peter
à Lausanne. Les médias suisses ont beaucoup plus parlé de Floyd en
quelques mois que de Mike en des années, alors qu'on se bat pour que la
justice soit faite pour Mike depuis longtemps. Si un homme noir tirait
sur un flic suisse blanc, ça serait dans tous les médias pendant au
moins un an. Mais si un flic blanc tire sur un homme noir, on n'en
parle pas vraiment parce que, bon, c'est juste un homme noir. C'est
comme ça que le système fonctionne et il inclut la justice et les
avocaxtes.

Imagine, la police t'arrête avec un gramme de weed, la police ne vient
même pas devant la cour pour témoigner. Je connais un mec noir qui s'est
fait choper avec un gramme de weed, un officier de police a écrit une
lettre pour témoigner qu'il l'avait attrapé alors qu'il n'était même pas
présent au moment de l'arrestation. Le mec noir est devant la cour,
devant unex juge, mais tout le système continue à penser que la police
dit la vérité. La police ne se retrouve jamais devant unex juge, elle écrit
juste des lettres. Les juges pensent encore que "les flics ne mentent
pas". Quoi qu'il arrive, quoi que tu dises, leur décision est
déjà prise.

J'ai une autre histoire de flics qui mentent. Quand on squattait l'usine
à Renens, la police nous attendait toujours vers l'arrêt de bus. Une
fois, j'ai sauté dans le bus et deux policiers, en civil,
m'ont sorti de force. Ils m'ont fouillé et n'ont rien trouvé. Mais ils
m'ont embarqué dans une petite station pas loin et m'ont enfermé. Un
flic a pris le téléphone et je l'ai entendu dire :

--- Il a deux grammes de weed.

Alors j'ai dit :

--- Non, vous mentez, je n'ai pas de weed, vous venez de me fouiller.

J'ai passé la nuit là-bas et, au matin, ils m'ont emmené dans un plus
grand poste de police, pour pouvoir faire un scanner intégral. J'étais
là :

--- Merde, vous avez menti en disant que j'avais deux grammes de weed
et maintenant vous m'emmenez ici comme si j'étais un grand criminel ?
J'aurai ça sur mon casier jusqu'à la fin de mes jours ?

Ensuite, ils m'ont relâché et le lendemain, au même arrêt de bus, les
deux mêmes policiers m'ont contrôlé à nouveau. Je leur ai dit :

--- Sérieusement, vous m'avez contrôlé hier et vous avez menti. Vous
allez faire quoi maintenant?

Ils ont juste répondu :

--- C'est notre boulot, notre patron nous demande de faire ça.

--- Non, ce n'est pas votre job et votre patron n'est pas
là. Ici, il n'y a que vous et moi.

C'est juste un exemple, je sais que beaucoup d'autres sont dans
cette situation, se font arrêter pour des choses qu'iels n'ont pas
faites. Tu peux te
retrouver en prison pour un gramme de weed. Quand ils ne peuvent pas te
mettre en prison parce que tu n'as pas de papiers, ils mentent et disent
que tu avais de la drogue sur toi. C'est illégal, mais ils le font tout
le temps. Quand on parle de statistiques sur les personnes noires
arrêtées et mises en prison, on parle principalement de situations comme
celle-là : des innocenxtes en prison à cause de flics menteurs. C'est ça la
Suisse.

La violence en Suisse, c'est la violence de l'État et l'État n'existe
que grâce au peuple. Bien sûr qu'il y a des
gens en Suisse qui aident, qui tendent la main. Mais l'État lui-même nie
l'existence de certainexs réfugiéexs. Pour moi, les Suisses, individuellement, sont
cools pour la plupart. Mais du point de vue de l'État, c'est toujours
plus dur pour celleux qui n'ont pas de droit, qui n'ont pas la priorité.
Les décisions de l'État ne sont pas en faveur des Noirexs ou des gens de
couleur. Il ne les considère pas.

Dans la vie quotidienne, tu peux saboter un contrôle de police en
faisant perdre du temps aux flics. Mais pour moi, on a besoin de
balayer le système dans son entier, parce que le système est pensé pour
oppresser celleux qui ont le moins de privilèges, celleux qui ne sont
pas suisses, les minorités, les Noirexs.

Peut-on vraiment lutter contre un système qui emprisonne les gens sans
raison ?[^11] Nous devons utiliser nos esprits, nos voix, nos stylos,
notre art pour dire ce que les gens traversent. J'espère que beaucoup
vont lire ce livre et que d'autres feront des chansons ou des films. Il
faut tout changer en armes. Pensons à notre place dans la société :
qu'est-ce qu'on peut faire ? Si on est unex avocaxte par exemple, on peut
faire beaucoup.

Pour conserver sa légitimité, le système s'est construit en cachant des
choses. On doit montrer la vérité et la reconstruire. Changer
tout et changer la police. Pourquoi est-ce qu'elle n'est jamais punie ?
Arrêtons les flics et jugeons les flics. Ça devrait être simple et
facile. Vous m'arrêtez pour un gramme de weed ? Et vous, vous tuez un
homme noir sans que rien ne se passe? Vous ne faites jamais face à unex
juge ? Est-ce qu'on parle des mêmes lois ici? Les flics qui ont tué Mike
travaillent toujours dans les rues de Lausanne. Tant qu'on ne les jugera
pas, la police se sentira en parfaite impunité.

Les gens disent que les policiers sont mauvais (et c'est la version
polie), parce que si tu es une bonne personne, tu ne choisis pas de
devenir flic[^12]. Si tu es une bonne personne, tu ne choisis pas de
devenir quelqu'un qui peut dire "je t'ai tué, rien ne s'est passé et je
peux vivre avec ça".

[^7bis]: Pour un historique de ce collectif, lire *Jean Dutoit en lutte* \[n^o^ 23\]

[^8]: *Surveiller la surveillance* \[n^o^ 55\] essaie de renverser le rapport de pouvoir avec la police.

[^8bis]: Les informations relatives à ce film se trouvent sur : [https://noapologiesfilm.com](https://noapologiesfilm.com).

[^9]: Pour une description plus complète de cette pratique administrative, lire *L'absurdité des amendes qui permettent de socialiser un peu* \[n^o^ 34\].

[^10]: Pour un autre récit d'expérience du danger pour une personne migrante et racisée, voir *L'histoire d'une lutte* \[n^o^ 16\].

[^11]: D'autres textes dépeignent les luttes anticarcérales : *Brisons l'isolement* \[n^o^ 20\] et *Swiss made prison system* \[n^o^ 53\].

[^12]: Un collectif de <a href="/glossaire/#copwatching" class="glossary-link" target="_blank" rel="noopener" data-no-swup>copwatch°</a> partage des réflexions sur l'ordre policier dans *Spectacle nulle part.* Care *partout* \[n^o^ 23\].

