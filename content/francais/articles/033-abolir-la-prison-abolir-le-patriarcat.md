---
author: "Une militante parmi d'autres"
title: "Abolir la prison, abolir le patriarcat"
subtitle: "Droit, féminisme radical et système répressif"
info: "Transcription d'un entretien"
datepub: "octobre 2020"
categories: ["autodéfense", "relation à la militance", "prison, justice, répression", "police", "féminismes, questions de genre"]
tags: ["administration, administratif", "amour, love, amoureuxse", "autocritique", "coller", "confiance", "courage", "crier", "cul", "doute", "déconstruire, déconstruction", "force", "impossible", "injustice", "intime", "justice", "limite", "logique", "machine", "merde", "mère", "outil", "parole", "peur", "pouvoir", "procès", "silence", "théorie", "torture", "violence", "voix"]
langtxt: ["fr"]
zone: "Suisse romande"
num: "33"
quote: "la violence et son usage, c'est le terrain des mecs cis"
---

*Tu veux te présenter et raconter la position depuis laquelle tu parles ?*

{{%ligneblanche%}}

J'ai grandi ici. On peut dire que je viens d'une famille de femmes. Je
pense que ça m'a rendue sensible au fait que les vécus sont sexués. J'ai
grandi à la campagne et je suis venue en ville pour mes études. Mes
parents n'ont pas fait l'université et iels tenaient à ce que je fasse
des études "sérieuses", alors j'ai fait Droit. Au début ça ne me
plaisait pas, je voulais étudier la philo ou l'Histoire, puis je me suis
dit que le droit pouvait être un outil utile. J'étais déjà choquée par
la quantité d'histoires rudes que j'avais entendues jusque-là de la part
de différentes femmes : divorces compliqués, violences dans les couples
ou entre ados, etc. Je voulais lutter contre tout ça. En arrivant à
l'uni, j'ai découvert le milieu militant, au début via des trucs assez
institutionnels dans les assos étudiantes. J'y ai rencontré pas mal de
monde, notamment des féministes. Ça a eu un gros impact sur la suite de
mon parcours militant. Avant les études, je faisais partie de celleux
qui disent "je suis pas féministe", j'avais même annoncé haut et fort
que "je ne mettrai jamais de jupe". J'avais <a href="/glossaire/#misogynie--sexisme-intégrée--intériorisée" class="glossary-link" target="_blank" rel="noopener" data-no-swup>intériorisé°</a> beaucoup de
misogynie et de sexisme[^86], je n'avais que des copains garçons quand
j'étais ado. Enfin, jusqu'au jour où ils ont découvert le porno et
décidé que j'étais pas vraiment comme eux, que je n'avais pas ma place
avec eux. Je trouvais que les filles étaient superficielles, un peu
chiantes et elles me faisaient du mal : toutes ces choses que nous
enseignent nos sociétés patriarcales en gros. Du coup, j'étais pas mal
seule.

Avec ces contacts militants et les études, il y a deux thèmes qui ont
attiré mon attention. D'abord, le harcèlement sexuel, c'est ce sur quoi
je me suis penchée en premier et ça m'a menée à m'engager davantage.
J'ai bossé là-dessus pendant mes études et, plus je creusais, plus je
comprenais les mécanismes sous-jacents à ces violences, plus je me
rendais compte de l'ampleur du problème et du système qui produit ces
actes. C'est là que j'ai commencé à fréquenter des personnes de plus en
plus radicales, notamment dans le milieu squat et "alternatif".

L'autre grand thème sur lequel j'ai ouvert les yeux pendant ces années
d'études, c'est la prison et tout son système carcéral. Plus j'en
apprenais sur son fonctionnement, plus cette institution me semblait
absurde. Le système carcéral, c'est une machine à créer toujours plus de
souffrance. Or, comprendre la souffrance et essayer de la réduire me
semblait une priorité. Ça peut paraître bateau, mais c'était un point
central au début de mon engagement, même si les luttes <a href="/glossaire/#anticarcéral" class="glossary-link" target="_blank" rel="noopener" data-no-swup>anticarcérales°</a>
sont extrêmement marginales en Suisse romande[^87].

Ensuite, après mon Bachelor, j'ai eu besoin d'une pause, car le droit me
semblait trop imparfait et peu politique. Alors je suis partie faire des
études genre à l'étranger pour créer des ponts entre les outils
juridiques et les outils sociaux et politiques. Là-bas, j'ai intégré un
collectif <a href="/glossaire/#queer" class="glossary-link" target="_blank" rel="noopener" data-no-swup>queer°</a>, féministe, radical, en <a href="/glossaire/#mixité-choisie-ou-non-mixité" class="glossary-link" target="_blank" rel="noopener" data-no-swup>mixité choisie°</a> et ça m'a fait
méga du bien, j'y ai appris plein de trucs. C'est en revenant en Suisse
que j'ai décidé de continuer le droit, parce qu'une fois de plus, avec
tout ce que j'avais expérimenté dans ce collectif, il me semblait que le
droit était un outil nécessaire également dans le milieu militant. Sans
le droit, on se fait écraser.

Je me suis enfin sentie bien avec cette double casquette : juriste utile
et féministe radicale. Aujourd'hui, ça me permet de lutter sur plusieurs
fronts en même temps et ça m'aide à me sentir légitime, autrement j'ai
l'impression de rien avoir à apporter, mais ça crée aussi plein de
tensions.

Ces tensions --- le côté limite de ces outils --- je les ai aussi
expérimentées très vite dans le cadre de permanences juridiques
auxquelles j'ai participé. Je me suis rendu compte du nombre de
personnes qui vivent des situations épuisantes, faites d'humiliations
quotidiennes. Ce sont les femmes qui ont la charge mentale de la
famille, qui veillent à ce que rien ne pèse sur les enfants, etc.[^88]
À une échelle structurelle, le droit aide peu, c'est pour ça que le
militantisme est essentiel. Le militantisme rappelle que le combat du
droit devrait être un combat pour la justice et pas pour le maintien
d'un ordre conservateur, le militantisme fait évoluer le droit. Et
parfois c'est le droit qui vient aider les militanxtes, iels le
convoquent pour appuyer leurs causes avec, par exemple, des textes comme
la Convention d'Istanbul[^89] ou la Convention sur l'élimination des
discriminations faites aux femmes[^90].

{{%ligneblanche%}}

*La justice a ses raisons que le droit ignore.*

{{%ligneblanche%}}

D'un côté, le droit peut être un outil puissant. C'est parce qu'on
interdit la torture, qu'on peut lutter contre les opérations de
réassignation sexuelle à la naissance pour les personnes intersexes.
C'est parce qu'il y a des personnes qui se sont battues sur le plan du
droit, que tu peux avoir des congés payés ou que tu peux être protégéex
d'un licenciement lorsque tu portes un enfant.

Par contre, c'est toujours douloureux de dire à quelqu'unex : "Écoute,
oui là tu dois aller devant unex juge pour lui demander de trancher,
parce que t'es en train de sombrer. C'est imparfait, mais c'est ce qu'on
peut faire ensemble, là je peux t'aider". Concrètement, ces personnes
elles en chient. Il y a des agressions, un père qui dit qu'il passe
chercher sa fille et qui finalement ne vient pas, celui qui refuse de
payer les allocs, celui qui biffe la signature de la mère dans le carnet
de l'enfant pour la remplacer par la sienne, toutes ces choses qui
épuisent. Et si tu ne mobilises pas le droit, ces femmes doivent
continuer à subir seules toutes ces choses.

C'est clair, c'est chaud d'aller devant lae juge, mais c'est souvent la
seule manière de renverser un tant soit peu le rapport de force. Dans
certaines situations, le droit aide à valider les ressentis de certaines
personnes. Quand tu dis à une femme qui n'a pas les outils pour poser
certains mots sur ce qu'elle vit, "Madame, quand votre collègue de
bureau vous force à regarder des vidéos pornos, c'est du harcèlement
sexuel, c'est illégal, c'est dans la loi", elle peut se dire avec plus
de tranquillité "Ok, ce qui m'arrive n'est pas normal, j'ai la
possibilité de réagir et de me défendre."

Mais bon, le droit, c'est tout un langage et c'est aussi un de ses
aspects oppressifs : il faut beaucoup de compétences pour le comprendre.
Le droit peut protéger, mais uniquement dans certaines circonstances. Il
peut être un outil de libération, mais il peut vite devenir un outil
d'oppression. C'est un outil d'oppression, notamment parce qu'il
t'oblige à passer par des mécanismes méga institutionnels, hyper lourds,
dans lesquels tu dois maîtriser la langue, gérer les dossiers, être
capable de répondre à des courriers, savoir et pouvoir bien expliquer ta
situation ou encore tout simplement performer.

Il y a un autre problème : idéalement on devrait avoir des institutions
qui tentent de pacifier, de rééquilibrer les rapports entre les
personnes, qui tiennent compte des inégalités structurelles, de la
culture sexiste. Mais, aujourd'hui, l'institution judiciaire n'est pas
neutre. Elle est sous l'influence de celleux qui la maîtrisent, donc
beaucoup de mecs. Elle est <a href="/glossaire/#systémique" class="glossary-link" target="_blank" rel="noopener" data-no-swup>systémiquement°</a> défavorable aux vécus des
femmes. Du coup, tu te retrouves à dire à une femme en procédure de
séparation : "Si vous prenez quelqu'un pour vous défendre devant tel
tribunal, prenez un homme, on sait que si vous y allez avec une femme,
on va vous considérer comme une féministe et avoir un a priori négatif"
Ou encore : "Si vous allez à l'audience, mettez des habits neutres
et sérieux". La justice est, en ce sens, le reflet de notre société.
Les juges ont aussi des stéréotypes de merde et, comme partout, il y en
a qui réfléchissent et il y en a qui réfléchissent moins. Quand tu
soutiens quelqu'unex, t'as pas envie de prendre le risque qu'iel perde à
cause de trucs absurdes comme être "mal présentéex" ou "mal maîtriser
ses émotions". Donc tu te retrouves à donner des conseils
problématiques. Ça me remet beaucoup en question de le faire, ça me révolte,
c'est violent, alors j'essaie d'être aussi transparente que possible
avec les personnes que je vois. Stratégiquement, il faut parfois jouer là-dessus pour renverser le rapport de force.

Le droit est extrêmement limité en ce qui concerne le harcèlement ou les
violences sexuelles au sens large : on sait que, la plupart du temps, ça
se passe dans la sphère domestique. C'est difficile de déposer une
plainte pénale contre une personne dont t'es amoureuxse, c'est même
monstre chaud de mettre les mots sur ce qui t'arrive, donc très souvent
tu ne mobilises pas l'appareil légal. Ce système ne permet pas de
réparer, seulement de punir. Pour l'instant, la <a href="/glossaire/#justice-restaurative-ou-réparatrice-ou-transformatrice" class="glossary-link" target="_blank" rel="noopener" data-no-swup>justice restauratrice°</a>,
c'est un peu une chimère[^91]. C'est un énorme problème et ça sert une
idéologie contre laquelle je veux me battre en tant que féministe
radicale. Donc t'es face à un dilemme : soit tu subis, t'endures et les
mecs s'en sortent encore, soit tu utilises ce système pour te défendre
et te protéger un peu tout en sachant qu'il pose problème.

Les liens entre abolitionnisme carcéral et féminisme radical
apparaissent : quand survient la condamnation ? qui criminalise-t-on ?
qui va en prison ? Eh bien, ce sont les personnes qui n'ont pas les
moyens et les ressources nécessaires pour se défendre, qui n'ont souvent
pas de boulot, qui sont précarisées, racisées et qui occupent des
positions sociales marginales. C'est la spirale de la précarité. Le haut fonctionnaire dans l'administration publique ou le patron dans une
entreprise privée, il va mobiliser les meilleurexs avocaxtes de la place
pour se défendre et le procès, ce sera un vrai bras de fer, une bataille
très dure sur le plan psychologique, financier, etc. Ces procès durent
longtemps et tu ne sais jamais si la personne qui a trouvé le courage de
dénoncer va tenir le coup.

Cette justice à deux vitesses qui épargne les puissants, on l'a un
peu remise en question ces derniers temps avec #MeToo et Adèle
Haenel[^92], même si ces cas "exemples" sont très discutables : il
ne faut pas oublier qu'Adèle Haenel, c'est une femme blanche, non
précaire, reconnue. Elle peut davantage se permettre de gueuler que
d'autres personnes (et heureusement qu'elle le fait). Quand t'as pas
tout ça, on te suspecte de mentir, de vouloir te venger ou de vouloir te
rendre intéressante. Merci le patriarcat.

Ensuite, dans la grande majorité des cas, la procédure judiciaire est
difficile pour celleux qui portent plainte, parce qu'on va te demander
de parler de ton vécu, d'amener les preuves d'événements très intimes.
C'est souvent parole contre parole. Et quand tu portes plainte
contre une personne avec laquelle tu as partagé beaucoup de choses, il y
a parfois de la retenue, t'as pas envie d'en dire trop de mal, tu ne
veux pas tout étaler à des inconnuexs. La logique d'un procès,
c'est qu'il y a une personne qui gagne et une personne qui perd. C'est
une logique punitive qui invite les gens à s'attaquer violemment, il
n'y a pas d'espace pour l'expression d'une souffrance ou pour
l'accompagnement. Et, évidemment, la violence et son usage, c'est le
terrain des mecs <a href="/glossaire/#hommes-cis--mecs-cis--cisgenres" class="glossary-link" target="_blank" rel="noopener" data-no-swup>cis°</a>.

L'autre problème, c'est que si un agresseur est condamné, il va en
prison et il n'y apprend strictement rien, il n'y a rien de bien qui en
sort. Jamais. L'enjeu, c'est de l'écarter de la société pour qu'il
arrête de nuire, mais on ne réfléchit pas à comment le réintégrer dans
la suite de son parcours. L'enjeu est simple. La prison se contente de
dire : "Cette personne est un problème, on la neutralise". En plus, le
système carcéral réduit les personnes à quelques-uns de leurs actes et
c'est violent. Il fige. Il y a quelques vagues témoignages de personnes
qui disent que la prison leur a donné un cadre, mais c'est
essentiellement un outil normalisant qui vise à faire dire à la sortie :
"Ok, je vais trouver un boulot et bien payer mes factures", on ne
donne aucune ressource d'émancipation. On veut faire rentrer dans le
rang, on ne veut surtout pas que le système capitaliste et patriarcal
soit remis en cause. On dit "t'as merdé", mais on ne dit pas pourquoi,
on n'écoute pas. Il en résulte très souvent des personnes renforcées
dans leur sentiment d'injustice et d'incompréhension. La prison ne
permet pas de construire un projet personnel. On ne vise pas le
bien-être des personnes incarcérées, on ne cherche pas la
réconciliation, la reconstruction, la réflexion. C'est un système
autoritaire qui sanctionne et "fait payer".

L'abolitionnisme du système carcéral n'est pas facile à articuler avec
la lutte contre le patriarcat et les violences qui lui sont propres. Je
veux un monde avec une justice, mais pas une justice répressive, je ne
vois pas comment on peut défendre la prison tant qu'on sait qui y va et
qui n'y va pas, tant qu'on sait qu'elle est incapable de prendre en
charge toute une série d'agressions et que rien n'en sort de bon.

C'est pour ça qu'avec le militantisme, j'en suis arrivée à explorer
d'autres pistes, autant dans mes réflexions sur le droit (la médiation,
la justice restaurative[^93]) qu'avec mes copainexs militanxtes. Avec
elleux, j'ai tenté des choses qu'initialement je ne m'autorisais pas à
imaginer : balancer des noms entre nous, hors système étatique, sans
faire de procès et sans envisager des sanctions pénales. Le faire pour
nous et pour les autres personnes qui risquent d'avoir à faire à ces
agresseurs, pour nous protéger. Dans les milieux militants, il y a aussi
du sexisme, du machisme, des agressions et des oppressions quotidiennes.
Dans ce contexte, c'est presque plus dur à supporter tellement c'est
hypocrite et tellement ça t'isole. Tu te sens trahie par tes camarades.
Du coup, on s'est dit qu'on allait sortir les noms des personnes qui
ont ce type de comportements et parler de nos vécus. Je ne suis pas sûre
que ce soit la bonne chose à faire, mais quand t'as déjà essayé de
causer plein de fois à ces types, quand tu sais que les autres ferment
les yeux, il faut bien tenter d'autres choses.

Il s'agit de tentatives, d'explorations. Dans mon militantisme, j'essaie
de rester dynamique, de faire des allers-retours entre tous ces
paradoxes, ces outils et ces dilemmes. J'essaie de me rappeler que la
critique et l'autocritique sont essentielles, que les solutions sont
liées à nos parcours personnels. Par contre, je pense que cette
autocritique ne doit pas devenir paralysante, il faut qu'elle laisse la
place aux essais, aux erreurs. Quand tu penses au nombre de vies brisées
par le patriarcat et par le système carcéral, tu te dis que cette
liberté de mouvement et de pensée est essentielle.

{{%ligneblanche%}}

*Pourquoi balancer un nom publiquement, ce n'est ni de la justice
répressive ni de la diffamation ?*

{{%ligneblanche%}}

Premièrement, c'est pas une justice répressive parce qu'on ne mobilise
pas un appareil d'État. On se prévient entre nous quand des mecs font
des trucs violents, quand ils nous blessent et qu'ils nous font
souffrir. Il n'y a pas de sanction au sens strict non plus. On ne fait
pas un procès, on n'étale pas des conversations sous les yeux d'unex
juge pour lui demander de trancher, d'assigner qui que ce soit à payer
des trucs ou de coller des entrées dans un casier judiciaire qui
pénalisent pendant toute une vie.

On visibilise le fait qu'on est dos au mur. En fait, j'ai l'impression
que même quand on balance des noms, quelque part, on est encore en train
d'essayer de créer un dialogue et un monde différent qui ne se construit
pas sur de tels rapports de force. On ne va pas dans le sens d'une
judiciarisation, mais dans celui d'une déconstruction, certes un peu
forcée. Est-ce que c'est la bonne manière ? Parfois je doute. Mais assez
vite, je me rappelle que tout est fait pour qu'on ne parle jamais, pour
qu'on se taise par peur de dire quelque chose qui puisse blesser, par
peur d'en faire trop. En gardant le silence, on invisibilise des trucs
graves.

Ensuite, la diffamation n'est pas une bonne analogie. Il ne faut pas
oublier que, pour qu'il y ait diffamation, il doit y avoir mensonge. En
termes juridiques, il faut "des faits contraires à la vérité qui ont
été propagés". Il faut aussi que la personne qui les propage sache
qu'il s'agit de conneries ou qu'elle ait des gros doutes. Le terme
"diffamation" ne se base pas uniquement sur le sentiment de la
personne qui est blessée.

C'est intéressant, ce truc de diffamation, c'est un exemple
d'utilisation du droit contre le féminisme. C'est utilisé pour dire
qu'il ne faut pas parler. Là, on mobilise le droit pour faire peur, sans
tenir compte de la réalité du terrain. C'est ultra rare qu'une personne
socialisée femme dénonce à tort uniquement pour nuire. Il n'y a que 2 %
de fausses allégations dans les cas de violences sexistes et sexuelles,
donc dans 98 % des cas, il s'agit bel et bien de choses véridiques.

{{%ligneblanche%}}

*Avec le mouvement #MeToo, balancer Weinstein, ça paraît limpide,
puisque d'un point de vue juridique, c'est impossible de lui faire face.
Mais à l'intérieur des milieux militants, ce sont d'autres enjeux, non ?*

{{%ligneblanche%}}

Ben en gros, si t'es pas un mec cis dans les milieux militants et que t'as
une perspective abolitionniste de la prison ainsi qu'une approche
féministe du droit, t'es face à un paradoxe. Si t'as recours au droit on
te dit : "Tu utilises les outils oppressifs contre lesquels on se bat,
c'est horrible" alors il y a des militanxtes qui décident de faire
sans. T'expliques qu'il y a des trucs violents qui se passent et on ne
te croit pas. On te répond : "Si c'était si grave, t'aurais fait
quelque chose, non ?" Et bien sûr : "Attention, faut pas diffamer !"
Bref, t'as toujours le cul entre deux chaises. Quoi que tu fasses, t'as
tort. Alors balancer des noms à un moment donné, c'est simplement crier :
"Mais en fait, vous nous faites chier avec vos contradictions de
merde, si vous avez le droit d'être pleins de paradoxes vous les mecs
cis, et bah nous aussi." Pourquoi c'est à nous d'être hyper cohérenxtes ?
d'avoir la charge mentale ? d'avoir pensé à toutes les options ? de
faire au mieux pour que personne ne souffre ? J'espère que ce n'est
qu'une étape par laquelle on doit passer, celle où il faut balancer des
noms, pour ensuite aller vers autre chose. L'étape sera franchie quand,
pour reprendre un slogan, la peur aura changé de camp. Évidement que
c'est pas un projet politique la peur, c'est hyper nul, mais si, à un
moment donné, il faut passer par là pour qu'on puisse aller vers une
autre utopie, s'il faut que des types flippent un peu parce qu'il y a
quelque chose qui risque de leur tomber sur la tête quand ils ont des
comportements oppressifs à répétition, je peux vivre avec.

Aujourd'hui, dans les milieux militants, les mecs cis se présentent tous
comme alliés et féministes "par défaut". Beaucoup d'entre eux ont lu
de la théorie féministe. Tout le monde est d'accord avec ce que tu dis,
du moins tant que ça reste des théories générales. Par contre, dans la
pratique et surtout dans l'intimité, la réalité est différente. Quand
t'as écrit des magazines, fait des réus, parlé, écouté, documenté et que
rien ne change, tu balances parce que t'en peux plus. C'est un outil qui
nous force à regarder la merde en face. Ça nous force à nous confronter,
pour de vrai, à ce qu'il y a de politique dans l'intime. On a de la
peine à concevoir l'intime (ce qu'on appelle souvent les histoires
"privées", que ce soit dans les relations de couple ou dans l'amitié)
comme étant politique. Le féminisme réussit à sortir de cette vision
"interpersonnelle" de la relation intime, il réussit à la
politiser[^94].

Il y a aussi une sorte de principe d'équivalence qui m'énerve. On te dit
souvent que balancer un gars c'est hyper salaud, qu'il va hyper mal, que
c'est violent pour lui et t'as envie de dire : "On peut se rappeler de
quoi on parle à la base ?" Comme si c'était un truc que tu faisais par
plaisir de balancer et d'exposer ton intimité. Après avoir parlé, tu
feras face aux : "Je vais pas m'en mêler, c'est un problème privé."
Parfois : "Je sais pas, j'ai pas entendu la version de l'autre." Ou
même : "Je trouve qu'elle se victimise." Et en gros tu dois, en plus
des agressions, gérer la souffrance de ton agresseur et convaincre les
autres ? En fait, c'est tout simplement une manière de nier ce vécu.
J'ai le sentiment que même quand des personnes balancent, il y a
toujours un espoir derrière, celui d'essayer de continuer à discuter, de
se dire qu'on pourra être entenduexs. Parler fort et quoi qu'il en
coûte, c'est refuser le silence pour changer les choses malgré tout.

Je peux te raconter cette histoire personnelle, un truc récent qui a
fait apparaître ces contradictions. Ça n'invalide pas la théorie, ça
montre que parfois c'est difficile de mêler pensée et action.

J'ai toujours des doutes : est-ce bien d'en parler ? Est-ce que mon vécu
vaut quelque chose dans tout ça ? L'histoire en question, c'est que j'ai
été en relation polyamoureuse avec un type que j'ai eu dans la peau,
comme j'en avais rarement eu avant. Il avait plus de quarante ans, il
était divorcé et il était très en lien avec des jeunes dans son taf.

Ce rapport était assez chaud à gérer puisqu'il avait une famille, des
responsabilités envers ses enfants et moi je devais me trouver une place
au milieu de tout ça. Je ne pouvais pas trop parler de ce que je vivais dans
mon milieu socioprofessionnel, j'avais peur aussi des stéréotypes
sexistes associés à la "nouvelle copine" d'un mec divorcé avec des
enfants et polyamoureuse par-dessus le marché. Un jour, il m'a expliqué
qu'il a eu une relation avec une femme de 16 ans et cette situation m'a
mise face à toute une série de contradictions, en tant que féministe et
en tant qu'abolo de la justice répressive. Je me suis retrouvée face à
un mec que je respectais beaucoup, qui me soutenait énormément et qui
avait presque 20 ans de plus que moi, qui m'expliquait que je ne pouvais
pas critiquer cette relation avec une personne de 16 ans, parce que
c'est légal (c'est la majorité sexuelle en Suisse) et parce que
j'entretenais d'autres relations, comme si ça justifiait quoi que ce
soit.

Le droit était utilisé pour couper court à une discussion sur mon
malaise, comme si lui et moi étions totalement égales (ainsi que lui
et elle). À la suite de ça, une autre personne a constaté cette
situation, ainsi qu'une autre situation un peu plus floue avec une autre
femme mineure. Elle a dénoncé ce mec aux autorités de protection de la
jeunesse. La question s'est alors posée de ce que je dirais à ces
autorités... et je ne savais pas quoi faire. Je n'étais pas d'accord
avec son comportement, mais j'avais confiance en lui. En même temps, une
petite voix dans ma tête me disait que, puisque j'étais amoureuse, je ne
voulais peut-être pas voir certains trucs. Je n'arrivais plus à être
nuancée. J'avais peur qu'il soit réduit par un système répressif à
quelque chose qu'il n'était pas. En plus, j'avais peur que, si une
enquête devait s'ouvrir, ses enfants et ces deux femmes mineures soient
interrogées par les flics. Même si c'est parfois nécessaire, c'est une
expérience que tu ne veux pas infliger à des ados. Ça me filait le
vertige de penser que ce que je dirais ou tairais pouvait mener soit à taire des soupçons d'actes pédocriminels soit à activer, peut-être à tort, une machine ultrarépressive qui aurait des conséquences sur un tas de personnes. En
plus, à mes yeux, il avait déjà assez souffert, il n'avait peut-être pas
besoin de ça. Pour autant, je constatais qu'il était incapable de
comprendre qu'il me faisait du mal et qu'il pouvait en faire à d'autres,
même sans le vouloir. Il comprenait pas que, dans sa manière de
relationner avec ces jeunes femmes, il était avant tout un mec borné et
incapable de se remettre en question, comme si lui ne faisait pas partie
d'un système, comme si lui n'avait aucun privilège, aucun rapport de
force en sa faveur. Comme si c'était OK à plus de quarante piges de coucher avec une meuf en rupture de l'âge de son fils. J'ai eu peur parce que d'un coup je me suis dit que
je n'avais jamais eu la version de cette jeune femme, je n'ai eu accès
qu'à la parole de ce mec selon laquelle elle prend du plaisir à leur
relation et est parfaitement consentante. Aussi, j'ai dû me battre
contre moi-même pour ne pas me dire que je devais protéger cette fille
mineure, pour ne pas l'infantiliser. Les catégories théoriques ne
collaient pas et j'étais complètement paumée. J'ai mis du temps à me
dire que c'était pas sur elle que je devais me concentrer, que c'était
pas mon rôle sur ce coup.

Et malgré tout ça, j'avais un truc qui tournait en boucle dans ma tête :
c'est la meilleure personne que je connais et il m'a tant soutenue et
donné, que c'est pas possible. J'ai finalement dit au service les choses
comme ça avec le plus de pincettes et de transparence possible et que vu
la situation, une intervention n'était sans doute pas nécessaire. C'est
pas allé plus loin : pas assez d'éléments, rien d'illégal, personne en
situation claire de danger. Maintenant, je me dis que c'est bien comme
ça, mais c'était dur. Cette situation m'a fait expérimenter les dilemmes
que vivent certaines des personnes qui me consultent.

Tout ça fait écho à notre discussion de tout à l'heure sur la
diffamation, sur le droit qui aide et en même temps cautionne. Dans ce
cas précis, c'est pas illégal, mais il y a une situation de pouvoir. On
ne peut pas condamner le mec par défaut, mais il ne fait aucun pas. Il
ne faut pas infantiliser cette femme, mais comment faire passer ce
message sans recourir à aucune autorité ? Le problème, c'est que
l'autorité est punitive, mais il dit qu'il m'aime, mais il me fait du
mal. Tu vois le souci, la boucle infernale. Je ne sais pas comment
répondre à ces situations en tant que féministe radicale abolo des
prisons, je ne sais pas y répondre dans le monde tel qu'il est. Malgré
tous mes outils théoriques, je suis souvent démunie. Dans ces moments,
je remercie mes copainexs militanxtes qui m'écoutent, me soutiennent et
m'aident à ne pas porter ça seule. J'applique à mon cas ce que j'essaie
de faire pour les autres. J'y trouve la force de continuer à me battre
pour que les choses changent.

[^86]: Misogynie intégrée ou <a href="/glossaire/#misogynie--sexisme-intégrée--intériorisée" class="glossary-link" target="_blank" rel="noopener" data-no-swup>intériorisée°</a>.

[^87]: Pour un récit de lutte anticarcérale, lire *Brisons l'isolement* \[n^o^ 39\].

[^88]: Pour découvrir la trajectoire de politisation d'une mère sur sa propre condition, lire *En el feminismo, lo personal es político* \[n^o^ 49\].

[^89]: Une convention qui vise à "protéger les femmes contre toutes les formes de violence", conclue à Istanbul en 2018.

[^90]: Une autre convention du même type est conclue en 1979 par l'Assemblée générale des Nations unies.

[^91]: Pour une autre perspective sur la gestion des violences sexistes en dehors du cadre judiciaire, lire *La fête est finie* \[n^o^ 20\].

[^92]: Lors de la cérémonie des Césars 2020, Adèle Haenel quitte la salle pour protester contre la décision du jury qui sacre Roman Polanski meilleur réalisateur.

[^93]: Sont utilisés aussi les termes justice restauratrice ou <a href="/glossaire/#justice-restaurative-ou-réparatrice-ou-transformatrice" class="glossary-link" target="_blank" rel="noopener" data-no-swup>transformatrice°</a>.

[^94]: *En el feminismo, lo personal es político* \[n^o^ 49\] articule également l'intime au politique.
