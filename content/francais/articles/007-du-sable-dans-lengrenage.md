---
author: "Une militante du Collectif R avec des papiers suisses"
title: "Du sable dans l'engrenage"
subtitle: "Occuper, s'organiser, informer, tenir, militer : appel d'R"
info: "Texte rédigé pour le recueil"
datepub: "août 2020"
categories: ["luttes migratoires", "prison, justice, répression", "squat, occupations, logement"]
tags: ["Dublin", "adrénaline", "art, artiste", "coller", "fatigue", "foot", "force", "injustice", "internet, web", "justice", "machine", "permanence", "pouvoir", "solution", "théâtre", "violence"]
langtxt: ["fr", "en", "it"]
zone: "Suisse romande"
num: "7"
quote: "il ne reste plus qu'à tout faire exploser"
---

Si je raconte mon expérience en utilisant le "on" et le "nous",
c'est que ce serait bien prétentieux de dire "je" pour raconter une
histoire tissée d'actions, d'ambiances et de décisions collectives dont
je n'étais qu'une infime partie. Ce texte n'est qu'un point de vue parmi
tant d'autres sur une expérience de lutte de plus de trois ans, qui
perdure encore sous d'autres formes.

## Occuper

On est début 2015, une année qui restera dans les annales pour le nombre
record de demandes d'asile en Europe. L'Italie et l'Espagne sont
incapables de s'occuper des nombreuses personnes qui arrivent par la
mer, l'accueil dysfonctionne. Les requéranxtes d'asile vivent dans des
conditions indignes. La Suisse, elle, s'obstine à renvoyer
systématiquement chaque requéranxte dans le premier pays européen qu'iel
a traversé. Comme l'autorise le règlement <a href="/glossaire/#dublin-réglementation-de" class="glossary-link" target="_blank" rel="noopener" data-no-swup>Dublin°</a>, les décisions
négatives sont rendues sans être motivées : de nombreuses personnes
reçoivent des décisions traumatisantes de renvoi imminent vers l'Italie.

C'est sur la base de ces témoignages que débute une mobilisation autour
de l'idée d'un "refuge", d'un lieu où se cacher pour éviter le renvoi,
de l'occupation d'une église[^20]. Une église, c'est un lieu où la
police ne viendra pas, pas sans faire polémique. Occuper une église, ça
fait parler : les soutiens, les oppositions ferventes, les "oui, mais"
qui condamnent l'art et la manière. Au moins, ça parle dans la presse,
dans la société civile, à l'université où par la suite un auditoire a
aussi été occupé. Cette forte visibilité permet de mobiliser des
militanxtes pour se joindre au collectif, organiser des manifestations,
etc. C'était aussi un levier pour être entenduexs et entamer des
négociations avec les autorités, même si celles-ci ont toujours
fermement condamné l'occupation et que les négociations n'ont, dans
l'ensemble, jamais abouti.

Cela amène aussi les églises, en tant qu'institutions, à se positionner
sur les durcissements de la politique d'asile, sur les renvois dans des
conditions inhumaines. Entre dénonciation et charité, elles ne peuvent
plus laisser la réalité hors de leurs murs si la réalité y entre sans
invitation. Les opinions, les soutiens et les actions de l'église sont
éminemment politiques et dans ce cas précis, les liens étaient d'autant
plus explicites que le conseiller d'État cantonal chargé de la migration
est membre du Parti démocrate-chrétien (PDC).

## S'organiser

Matchs de foot, jeux vidéo, ateliers théâtre, bouffes pop' et autres
activités se développent au "refuge". Dans ce cadre, c'est toute une
vie collective qui s'organise avec et autour des personnes qui vivent
"cachéexs" pour quelques jours, semaines ou mois dans la salle de la
paroisse. Cela comprend aussi la récupération des invendus alimentaires,
les nettoyages, la définition de règles de vie commune et l'accès aux
soins. Des milianxtes et des personnes avec un statut légal se relaient
sur place, surtout pour faire écran en cas d'intervention policière,
mais aussi pour créer du lien et amener une présence. Les sorties hors
de l'église se font uniquement accompagnées pour éviter une
arrestation. Les conditions de vie restent malgré tout très
difficiles pour les habitanxtes : elles prolongent un parcours
migratoire déjà traumatique.

## Informer

La mobilisation amène de nombreuses personnes recevant des décisions
"Dublin" négatives à venir demander du soutien. Une permanence
hebdomadaire se met en place, et, petit à petit, on développe une forme
d'expertise orale et collective des pratiques des autorités. La réalité
change vite et il faut sans cesse réadapter les informations et les
actions.

Durant les deux premières années, les autorités suisses ont un délai de
six mois à partir du dépôt de la demande d'asile pour renvoyer les
personnes. On en déduit qu'il faut les cacher pendant les six mois qui
suivent la réponse négative et la décision de renvoi pour que ce délai
soit dépassé et que la procédure puisse être rouverte. L'expérience nous
apprend qu'il faut absolument transmettre une adresse de domicile pour
chaque personne cachée, faute de quoi elle est considérée
administrativement comme "disparue", le décompte de six mois s'arrête
et la demande d'asile est en quelque sorte annulée. On contourne cette
règle en communiquant systématiquement l'adresse du "refuge", sans
risquer d'expulsion puisque la police ne vient pas dans l'église. Au
terme du délai de six mois, la procédure d'asile peut alors être
rouverte par un simple courrier, puisque la personne a résidé à une
adresse connue des autorités, mais n'a pas été renvoyée. Cette
stratégie s'est affinée toujours plus et a fonctionné pour environ 300
personnes dans le canton de Vaud. Leurs demandes d'asile sont traitées
en Suisse et elles échappent aux renvois vers un autre pays européen.

Par la suite, la situation se durcit et la marge de manœuvre se
restreint considérablement. La justice de paix commence à émettre des
assignations à résidence qui obligent les personnes à séjourner dans les
centres de déportation. Une prolongation étend ensuite le délai de
renvoi de six à dix-huit mois, ce qui contraint à se cacher sur une
durée quasiment intenable (et toujours sans ressources financières).
Finalement, la loi est complètement réformée, de nouveaux centres
d'asile fédéraux sont construits, des centres dont les personnes ne
ressortent plus du tout.

## Tenir

Les durcissements toujours plus nombreux opérés par les autorités
rendent les actions toujours moins utiles. Il devient difficile de
trouver du sens à notre présence lors des permanences, alors que l'on ne
peut plus rien proposer de concret. Les personnes migrantes viennent
toujours nous trouver dans l'attente d'une solution et on répète sans
cesse qu'on ne peut rien changer à leur situation. Cela nous force à
remettre en question notre posture. On choisit de valoriser l'écoute.
Trop souvent, les personnes en demande d'asile disent ne pas avoir la
possibilité de s'exprimer. On ne les écoute pas. Ni les employéexs qui
les "surveillent" et assurent la sécurité ou le logement, ni celleux
qui prennent les décisions qui les concernent. Ni parfois les
soignanxtes pour leurs problèmes de santé. Souvent, à leurs proches
restéexs au pays, iels cachent la vérité pour éviter les inquiétudes ou
sauver leur image.

Alors, on change le rôle de la permanence. On répond aux questions
concernant les stratégies qu'iels envisagent et on écrit des textes sur
les situations qui nous révoltent ou sur notre impuissance. Surtout, on
se définit comme un lieu de rencontre. Ce qui nous importe, c'est de
tisser des liens avec ces personnes en situation de vulnérabilité.
Certainexs reviennent régulièrement et l'ambiance est encore plus
chaleureuse en petit groupe et avec moins de démarches administratives à
entreprendre.

## Militer

Au début, ou à chaque action forte, c'est l'effervescence : une grande
visibilité à l'extérieur, beaucoup de choses à faire, à gérer, tout le
temps, partout. C'est beaucoup d'espoir, des rencontres, des univers de
possibles qui s'ouvrent à l'imagination. Des stimuli qui font vibrer. La
lutte donne du sens à l'existence, de l'épaisseur au quotidien et un
sentiment d'appartenance au(x) groupe(s).

C'est épuisant, mais dans ces moments-là, la motivation et l'entrain
prennent le dessus. L'immédiateté de l'action ne laisse que peu de place
à la fatigue : c'est le sentiment que tout se joue dans l'instant, une
notion d'urgence ou d'importance majeure. Combinée à l'absence
d'horaires, au mélange entre travail et sociabilité, les limites sont
dures à poser, c'est magique, mais cela abîme aussi parfois.

Les actions "fortes" et les moments d'hyperactivité, ça ne dure pas,
quelques mois tout au plus. Ils laissent place à des phases de
transition, des moments où les membres du collectif travaillent
toujours, mais sans grande visibilité. La reconnaissance sociale que
l'on tire de ces travaux et ces réunions se situe alors dans des liens
interindividuels : un sourire, une discussion intéressante, une amitié
qui se tisse. Pour le reste, c'est un travail parfois peu gratifiant, en
l'absence de salaire et à la valorisation sociale restreinte.
L'adrénaline ou l'euphorie sont redescendues et il faut continuer.
S'installent les réunions interminables, les éternels conflits et
alliances internes, les mêmes prises de pouvoir, les mêmes
problématiques toujours rediscutées. Dans ces conditions, certainexs se
désengagent. Dont celleux qui sont habituéexs à une valorisation active
de leur travail, via une reconnaissance publique sur la scène politique
par exemple. Ou encore celleux qui sont moins habituéexs au travail
domestique gratuit non complimenté.

## Épilogue

Décider de s'arrêter, de dissoudre le collectif ou non, de maintenir
certains groupes ou certaines activités ne sont pas des décisions
faciles. Le contexte actuel est bien différent de celui durant lequel le
Collectif R a commencé à militer. Les CFA, centres fédéraux de l'asile,
se situent dans des lieux géographiques isolés et peu desservis par les
transports publics. Aucune visite n'est possible dans ces centres, y
compris pour les journalistes ou les associations (hormis l'association
mandatée par l'État pour la représentation juridique et encore selon
des conditions très strictes). Les liens sont donc presque inexistants
entre la société civile et les personnes requérantes d'asile logées
dans les centres fédéraux. Ces dernières peuvent être directement
renvoyées depuis les centres, sans avoir jamais mis les pieds à
l'extérieur de ces ignobles boîtes noires. La lutte est encore et
toujours nécessaire, même si le système veut la rendre impraticable.

Les réalités de l'asile deviennent si dures que plus personne ne veut
s'y intéresser.

Il ne reste plus qu'à tout faire exploser.

Allez, viens !

## D'autres actions du Collectif R en bref

- Des ***mailing lists***, dont une très large (env. 2000
personnes) utilisée plusieurs fois par année pour relayer des appels à
manifester, des témoignages ou des constats sur l'évolution des
pratiques des autorités en lien avec les renvois et l'asile.
- Des **autocollants** rédigés à la fin des permanences d'informations
aux personnes requérantes d'asile. Chaque semaine, une phrase courte,
simple et marquante issue d'un témoignage du jour est imprimée avec la
même typographie et identité visuelle sur du papier autocollant. Chaque
personne repart avec des autocollants à coller sur son trajet et l'image
est parallèlement diffusée sur le site internet.
[*www.desobeissons.ch*](http://www.desobeissons.ch)
- Une **présence** aux audiences de la justice de paix. L'un des
durcissements de pratique opérés par les autorités consiste à assigner à
résidence des personnes en instance de renvoi. Cela les empêche de se
"cacher". L'issue de l'audience est déjà connue à l'avance, car lae
juge prononce l'assignation à résidence sans prendre en considération la
situation individuelle de la personne renvoyée. Nous y avons assisté en
tant que public, pour poser des questions à lae juge ou brandir des
panneaux dans la salle puis fait une conférence de presse à l'extérieur
du bâtiment. Si ces actions n'ont pas eu de portée concrète sur les
décisions juridiques, elles rendent visible l'injustice de la procédure.
Elles permettent de conscientiser que chaque maillon, chaque rouage de
cette machine à broyer des vies qu'est le système d'asile est constitué
d'êtres humains qui ont une marge de manœuvre et qui, s'iels
s'associaient pour en faire usage, pourrait faire évoluer la situation.
- Des **actions visuelles choquantes** : lors de manifestations, nous
nous sommes enchaînéexs pour dénoncer l'usage des chaînes aux pieds qui
sont utilisées sur les réfugiéexs, alors qu'il s'agit d'une pratique
illégale. Nous nous sommes attachéexs à des chaises et baillonées pour
montrer la violence des vols spéciaux, avons étendu un grillage et des
barbelés sur la place de la Riponne pour symboliser la violence aux
frontières, et d'autres images symboliquement fortes.
- Des **cartes postales envoyées aux autorités** : envois massifs, avec
une sélection de messages communs au recto et la possibilité de signer
ou d'écrire au verso.
- Des **tags et des collages**.

[^20]: *Fuir en exil* \[n^o^ 31\] raconte cette occupation depuis un autre point de vue.

