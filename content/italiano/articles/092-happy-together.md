---
author: "Mariana E. Califano"
title: "Happy Together"
subtitle: "La Federazione delle Resistenze, una realtà d'ispirazione startrekkiana"
info: "Testo scritto per la raccolta bolognese"
datepub: "Gennaio 2023"
categories: ["storia, memoria", "autogestione, autorganizzazione", "spazi, occupazioni, diritto all'abitare"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "92"
quote: "Storia, ultima frontiera!"
---

«Ciao! Ecco le scansioni di sei piccoli lavori che ho fatto per
l'occasione. Le illustrazioni sono esattamente come le ha rese lo
scanner, quindi molto più brutte degli originali! Il grafico saprà
rendere il tratto nero esattamente come dovrebbe essere. I disegni sono
allegati nell'ordine in cui avevo pensato di disporli e cioè: l'immagine
1, un ritratto di Kebedech Seyoum, in copertina del prossimo *Quaderno
di Cirene*; le immagini 2 e 3 raffigurano il corpo della donna etiope
sotto il giogo dell'invasore; l'immagine 4 rappresenta un piccolo gretto
fascista che mostra il suo trofeo; l'immagine 5 illustra la guerra di
Etiopia come vorrei che fosse finita ancor prima dell'intervento degli
inglesi e l'immagine 6 mostra la foresta di Harenna, nell'altopiano dei
monti di Bale nella regione di Oromia. Le immagini 2, 3 e 4 sono state
tratte da foto originali dell'archivio dell'Istituto Luce. Siete
ovviamente liberi di tagliare, elidere, espungere le parti che causano
problemi di impaginazione o di gusto! Sperando comunque e più di tutto
di aver centrato il senso della vostra pubblicazione, vi auguro buon
lavoro.

Cecilia.»

La “socia” l'abbiamo conosciuta nel 2019 attraverso l'e-mail in cui ci
inviava alcuni lavori per la quarta pubblicazione della nostra collana,
nata da una *call for papers *on line. Resistenze in Cirenaica aveva già
pubblicato tre quaderni con testi e illustrazioni dei membri del
cantiere permanente, così era arrivata l'ora di arricchirci di nuovi
contributi. Nei volumi precedenti avevamo scritto della liberazione di
Bologna dal nazifascismo; raccontato la storia delle resistenze
all'occupazione sulle due sponde del Mediterraneo - quella partigiana in
Italia e quella al colonialismo in Africa Orientale -; narrato le
vicissitudini di alcuni dei protagonisti e delineato l'architettura e la
toponomastica fascista della città felsinea. Il numero in lavorazione,
invece, era dedicato alle donne di queste resistenze e alle loro
battaglie. Prima di guardare Cecilia negli occhi e scoprire cosa facesse
a Milano però, abbiamo dovuto aspettare più di un anno. L'emergenza
pandemica stava per isolarci nelle nostre case.

Avevamo conosciuto Stefano in una delle tante serate al Vag61. Lui era
sempre partecipe e curioso. Pieno di domande. Un giorno ci chiama in
preda a quell'incontenibile entusiasmo che lo caratterizza. «A Reggio
abbiamo creato un collettivo simile al vostro, ci chiamiamo Arbegnouc
Urban. Sabato prossimo facciamo un evento al polo Makallè, davanti alle
scuole. Venite? Dai, re-intitoleremo via Makallè. Abbiamo coinvolto i
ragazzi. Loro hanno proposto di dedicare la via a Sylvester Agyemang, un
ragazzo investito davanti alla scuola. Abbiamo preparato delle locandine
informative, ci saranno alcuni graffiti e della musica!»

«Ciao Stefano. Ci piacerebbe tantissimo, ma venerdì sera presentiamo il
IV *Quaderno di Cirene*, quello nato dalla* call*. È un anno che aspetta
di uscire dalle scatole. La presentazione è slittata per il *lockdown* e
vengono tutti gli autori. Dobbiamo accoglierli e alloggiarli perché a
fine serata non ci sono più treni...»

«Allora non me la posso perdere! Ci vediamo venerdì sera e poi sabato
venite a Reggio appena riuscite. Un vostro intervento non può mancare.
Vi voglio bene.»

Venerdì 2 ottobre 2020, come promesso, Stefano era a Bologna. Aveva
caricato la bicicletta in treno per poter tornare a casa con un
regionale notturno a fine serata. Abbiamo finalmente incontrato Cecilia,
arrivata apposta da Milano ed è stato amore a prima vista. Sabato
mattina era la prima a essere pronta, poi siamo saltati in macchina alla
volta di Reggio Emilia. Un'umanità multicolore era raccolta davanti al
polo scolastico. Simon fotografava il susseguirsi d'interventi di
studentǝ, storicǝ e attivistǝ mentre si ultimavano le opere di street
art. Roberto, con diligenza, collocava dei depliant informativi che
rimarcavano il legame tra l'odonimo[^1] della via e la storia coloniale
sulla cancellata e alla fermata del bus. Stefano, con una scala
sottobraccio, scompigliava i capelli di un pirata di sette anni. Era una
giornata di sole e il cielo era terso.

Nel frattempo avevamo ricevuto un contatto nel nostro profilo social.
«Buonasera Mariana, come dicevo nel messaggio che vi ho scritto, mi
chiamo Fabio e faccio parte del collettivo antifascista di Carpi. Pochi
giorni fa il consiglio comunale ha approvato all'unanimità la mozione di
Fratelli d'Italia e Lega Nord che chiede l'intitolazione di una via o
una piazza a Norma Cossetto. La notizia è passata un po' in sordina,
probabilmente a causa dell'emergenza Covid, ma noi vogliamo darle
risalto. Stiamo pensando di organizzare un incontro per raccontare la
storia e le leggende che girano attorno a questa figura e per spiegare i
motivi che spingono la destra nostrana ad avvalersi di questo tipo di
retorica (intitolare strade, piazze ecc.). Un compagno sta cercando di
contattare il gruppo Nicoletta Bourbaki per un approfondimento storico
su Norma Cossetto. Nel messaggio che vi ho scritto ho chiesto
espressamente di te (scusami se ti do del tu) perché ho letto il tuo
scritto *Guerriglia odonomastica* e mi sembra perfettamente in linea con
quanto vogliamo spiegare. Saresti interessata a partecipare al fianco
del gruppo di lavoro sulle falsificazioni e le manipolazioni storiche?
L'evento è ancora in divenire perché stiamo pensando a come strutturarlo
e dove, ma di sicuro sarà a Carpi e vorremmo avesse luogo entro la fine
di luglio. Siamo ovviamente aperti a consigli e proposte. Attendiamo
vostre. Grazie mille per la risposta velocissima. Saluti.

Fabio»

Purtroppo Nicoletta Bourbaki non è riuscita a partecipare all'incontro,
perciò con Fabio ci siamo divisi il lavoro di raccontare chi era la
Cossetto e di sviscerare perché la destra italiana l'ha elevata a icona
della battaglia politica che cerca di equiparare il Giorno della Memoria
al Giorno del Ricordo. Il “caso Cossetto”, infatti, è paradigmatico per
spiegare come si crea una narrazione mitologica, come quella degli
*Italiani brava gente* sviluppata in seguito al periodo coloniale, e
rende evidente come l'odonomastica sia la scacchiera in cui opposte
*Weltanschauung* cercano di modellare la cosiddetta identità nazionale.
Tutte riflessioni che Resistenze in Cirenaica aveva già snocciolato
quando era scesa in campo per contrapporre una pratica dal basso alle
strategie “identitarie” della politica locale. L'invito del collettivo
di Carpi ci consentiva di condividere le esperienze maturate e
guadagnare nuovi proseliti per le azioni di “guerriglia odonomastica”.

Non abbiamo avuto il contatto di Luca per caso. L'invito a tenere una
lezione al corso virtuale sul colonialismo italiano del portale
d'informazione e culture indipendenti *Dinamopress* poneva un grattacapo
che lui poteva risolvere. Il seminario in questione era a pagamento, ma
noi pretendevamo che il nostro contributo fosse liberamente accessibile.
Per accogliere la richiesta, gli organizzatori chiedevano solo il
consenso dell'altra realtà che avrebbe condiviso l'incontro con noi,
altrimenti il link al collegamento sarebbe stato comunicato solo agli
iscritti al corso. Sono risalita a un recapito, ho indossato la maschera
della sfacciataggine e digitato il numero telefonico. «Pronto, parlo con
Luca? Mi chiamo Mariana e terrò insieme a te l'incontro del 12 maggio.
Ti disturbo?» Era libero, così ci siamo presentati. Lui avrebbe parlato
delle esperienze della Rete Anticoloniale Siciliana, il collettivo sorto
in seguito all'edizione del 2018 di Manifesta, la biennale nomade
europea di arte e cultura contemporanea che aveva ospitato
l'installazione «Viva Menilicchi!». Quella performance aveva preso
spunto da quanto sperimentato a Bologna ed era stata ideata e condotta
da Wu Ming 2, componente di Resistenze in Cirenaica fin dalla prima
iniziativa. La scoperta di una matrice di pratiche in comune dissolse la
sensazione di disagio che provavo per quella sfrontatezza che di solito
non mi appartiene. Il suo posto fu colmato dalla fluidità che intercorre
tra vecchi amici e Luca abbracciò senza reticenze la proposta oggetto
della mia chiamata.

Da questi incontri è nato il sodalizio che ha dato vita alla Federazione
delle Resistenze, un mega cantiere permanente che come tanti rizomi
ramifica in modo tentacolare esperienze e pratiche condivise.

L'Italia era immersa in un silenzio tanto assordante da aver riportato
cormorani, cigni e delfini nella laguna di Venezia e cervi e cinghiali a
pascolare ai lati del grande raccordo. La contemplazione dei vespri
mozzata solo da sirene spettrali dilatava le ore mentre l'irrequietezza
guadagnava terreno. Le azioni in strada del collettivo erano diventate
isolati segnali di fumo per indicare che eravamo ancora vivi.
All'orizzonte si profilava il 19 febbraio (Yekatit 12 nel calendario
etiope), diventato per noi una ricorrenza delle atrocità del
colonialismo italiano, una giornata in cui celebrare la resistenza dei
popoli dell'Africa Orientale alla nostra dominazione.

«Ciao Ceci, come stai? Posso rubarti qualche minuto? Ho avuto un'idea e
vorrei sapere che ne pensi.»

«Ciao Socia! Spara».

«Che ne pensi di creare una “federazione dei pianeti” in stile Star Trek
che accolga tutte le realtà impegnate in pratiche di memoria
antifascista e anticolonialista? Una rete strutturata in modo
orizzontale, rispettosa delle singolarità di ogni collettivo, ma che
scambi informazioni e condivida esperienze? Resistenze in Cirenaica
potrebbe mettere a disposizione la pagina web, dando spazio a tutti i
progetti e un recapito per chi volesse contattare le varie realtà;
fornire le credenziali perché tutti possano scrivere dei post,
promuovere le proprie iniziative e documentare con foto e video come
sono andate. Così facendo, nel tempo si creerebbe un archivio e il
materiale non si disperderà più nel mare magnum dei social. Tu potresti
aderire con RAM (Restauro, Arte, Memoria), caricare i filmati e le foto
dei restauri delle lapidi partigiane... Poi potremmo pensare a un
calendario con attività condivise da replicare in tutte le città.»

«Mi piace. Molto. Ma RAM si è impegnata solo sul fronte antifascista,
per ora non ha mai fatto niente legato al colonialismo italiano...»

«Appunto! La federazione sarà uno spazio in cui imparare dalle ricerche
e le esperienze altrui. Ogni realtà ha le sue particolarità. Gli
Arbegnouc, per esempio, lavorano molto più di noi sul tema dei migranti
e delle seconde generazioni. Loro re-intitolano le strade, noi le
ri-significhiamo.»

«Mi hai convinto. Ci sto! Quando facciamo la prima riunione?»

«Proponi un giorno e un orario che ti vada bene, io intanto contatto gli
altri e poi ti do conferma.»

La smania stava per essere canalizzata. Bastarono poche telefonate per
ritrovarci tutti on line. Il rito inaugurale prevedeva la sola adesione
ai princìpi dell'antifascismo e dell'anticolonialismo, i due soli della
nostra galassia resistente, oltre alla volontà di porre l'accento sulle
pratiche, ché per la ricerca e la teoria c'erano già gli accademici.
All'ordine del giorno c'era anche la definizione del calendario e uno
scambio sulle attività in programma di ogni collettivo. Il 19 febbraio
era alle porte, il tempo per elaborare un'azione condivisa
insufficiente, ma decidemmo di eleggerlo lo stesso a prova generale. In
pochi giorni la pagina web si arricchì con le descrizioni e i contatti
degli Arbegnouc Urbani, di Carpi Antifascista, della Rete Siciliana e di
RAM. Lo scambio di e-mail con materiale, foto e progetti diventò
frenetico. Tenemmo un corso accelerato per spiegare ai meno pratici come
scrivere e caricare i post nel sito. La mancanza di confidenza con il
mezzo e il timore di sbagliare o cancellare persino la pagina web
rallentò un po' l'autonomia dei singoli senza però intaccare l'operosità
dell'insieme. All'alba della commemorazione di Yekatit 12 eravamo pronti
e il programma congiunto di tutte le attività, le conferenze e le
performance dalle città della Federazione on line. Durante quel fine
settimana ci fu un susseguirsi di incoraggiamenti reciproci, di minimali
e entusiastici resoconti, di aggiornamenti minuto per minuto. Le chat
ardevano e la bruma dell'isolamento si era dissolta, liquefatta a colpi
di gioviale partecipazione.

Dopo la prova generale di febbraio, l'8 marzo 2021 segnò la data
inaugurale delle azioni condivise. Nel frattempo avevamo accolto le
padovane, con il loro collettivo Decolonize your Eyes. Per l'occasione
avevamo deciso di celebrare il coraggio di Violet Gibson e il suo gesto
dissidente nei confronti del fascismo. Le strade e le piazze di Bologna,
Milano, Carpi, Reggio Emilia, Palermo e Padova si sono risvegliate con
nuovi odonimi, contraltari alla sbilanciata celebrazione di personaggi
maschili. Per celebrare l'impegno femminile nelle città deserte, la
Federazione era tornata in strada, aveva rivestito di segnaletica fucsia
le mura grigie e raccontato la storia dell'irlandese che aveva attentato
a Mussolini, affrontando da sola una delle più squallide incarnazioni
del patriarcato che la storia ricordi.

Poco dopo ci ha raggiunto il collettivo romano Tezeta, con il progetto
«Harnet Streets», il Covid ha smesso di imprigionarci e i dispacci e le
azioni di guerriglia odonomastica, di trekking urbani e performance si
sono moltiplicati di pari passo alla ritrovata partecipazione in
presenza.

«Storia, ultima frontiera! Questi sono i viaggi della Federazione delle
Resistenze diretti all'esplorazione del passato per scoprire nuove forme
di presente, fino ad arrivare là, dove nessuno è mai giunto prima.»

## Nota per curiosǝ

Questo testo può essere letto senza approfondire chi sono i
protagonisti, cosa li spinge a fare quello che fanno e perché, in fin
dei conti in ogni storia i personaggi vengono costruite ai fini della
narrazione, no? Il fatto che rimandi però a una serie di realtà
veramente esistenti potrà intrigare moltǝ di voi e stuzzicare la vostra
voglia di saperne di più. Se abbiamo colpito nel segno, vi suggeriamo di
visitare la pagina https://resistenzeincirenaica.com, un punto di
partenza per esplorare una galassia in continua espansione.

[^1]:  Stesso significato di «omonimo», ma riferito ai nomi di strada :
    l'odonomastica è l'insieme dei nomi delle strade, piazze, e più in
    genere, di tutte le aree di circolazione di un centro abitato e il
    suo studio storico-linguistico.
