---
author: "Anonim*"
title: "Smettiamo di \"difendere\""
subtitle: "Riflessioni dalle zone occupate"
info: "Testo scritto per la raccolta svizzera romanda"
datepub: "Dicembre 2020"
categories: ["autogestione, autorganizzazione", "giustizia ambientale, ecologia", "sabotaggio, azione diretta"]
tags: ["absrude, absurdité", "courage", "doute", "détruire, destruction", "fatigue", "force", "impossible", "logique", "machine", "permanence", "pouvoir", "sabot, saboter, sabotage"]
langtxt: ["fr", "en", "it"]
zone: "Suisse romande"
num: "28"
quote: "dato che è lei a controllare il rapporto di forze e la temporalità del conflitto"
---

Nelle culture militanti abbiamo interiorizzato l'idea che il nostro
ruolo sia quello di *difendere*. Il vocabolario che usiamo non lascia
dubbi: "ZAD" (Zone À Défendre, letteralmente "zona da difendere"),
"protezione della natura", "salvaguardia dell'ambiente". Il concetto di
difesa appartiene alla logica stessa del nostro linguaggio e quindi
necessariamente anche alle riflessioni e alle azioni che portiamo
avanti. Dopo tutto cosa c'è di male? Si tratta pur sempre di impedire al
sistema capitalista di trascinare con sé il mondo vivente verso
l'autodistruzione. In realtà, non è così facile. L'obiettivo, secondo
me, dovrebbe essere ben più ambizioso. Dobbiamo recuperare ciò che è
stato distrutto da questo sistema per ricostruire qualcosa di diverso.
Lanciare l'offensiva contro questa enorme macchina e sabotarne il potere
distruttivo. Abbandonare la posizione difensiva e passare all'attacco.
Limitandoci a difendere ciò a cui teniamo, ci autocondanniamo
all'inefficienza.

## Vivere la "difesa"

Ho una profonda ammirazione per il coraggio e l'impegno di chi crea ZAD
e progetti affini, e sono grato per tutto ciò che ho vissuto in quei
luoghi e per le esperienze sociali a cui hanno dato adito. Le ZAD sono
ottimi strumenti di comunicazione e sensibilizzazione. Questi laboratori
di idee e pratiche mostrano che esistono ideali diversi. Tuttavia, si
basano su una strategia difensiva limitante che non può essere
sufficiente.

Credere che basti vivere un luogo per difenderlo è un'illusione. Vivere
è riposante, difendere è estenuante. Quando vivevo in una ZAD, l'azienda
alla quale ci stavamo opponendo veniva da noi a visitare l'area. È una
sensazione davvero paradossale, perché stai accogliendo il nemico nel
tuo spazio di vita, sforzandoti di sorridere per salvare le apparenze,
l'immagine. A pensarci bene è assurdo che l\* nostr\* avversari\* siano
liber\* di circolare negli spazi in cui viviamo. Significa che sanno
dove trovarci, e questo cambia tutto. Possono osservare come siamo
organizzat\* e attaccarci quando meglio credono.

Anche le diverse case occupate dove ho vissuto erano degli spazi da
difendere. So che effetto fa ricevere un'ordinanza di sgombero, un pezzo
di carta amministrativa che ti spiega che casa tua non sarà più casa
tua: che vivere in un luogo, *dargli vita*, non ha il minimo valore agli
occhi della società, della polizia e dei tribunali. E si sa come vanno
le cose. Il più delle volte cerchi di avvertire l\* militanti che
abitano nei paraggi, nella speranza che si mobilitino e reagiscano in
fretta. Purtroppo il tempo e l'incertezza giocano contro l\* occupanti.
La polizia tende a intervenire verso le cinque del mattino, dopo una
serie di giornate gelide o durante le feste, assicurandosi così di
attaccare persone poco numerose, stanche o impreparate. Spesso non viene
nessuno ad aiutarle a difendersi. È normale. Capisco bene che non siano
in molt\* a potersi permettere di correre dei grossi rischi legali per
difendere una casa nella quale non abitano. Ma soprattutto, l'equilibrio
dei rapporti di forza è completamente spostato a favore della polizia:
quando ricevi un\'ordinanza di sgombero, non sai se la cosa succederà
tra due ore, due giorni o due mesi. A partire da quel momento, vivi in
uno stato di emergenza e angoscia. Passi il tempo a elaborare ipotesi
sulle modalità dello sgombero e sulle strategie di difesa (spesso
irrealistiche) da adottare. I momenti di incertezza si susseguono
trasformandosi in stress, poi in angoscia, poi in insonnia. Il più delle
volte questo tipo di situazione genera conflitti e tensioni all'interno
del collettivo. Tutt\* si stancano. Come si può restare in una
condizione di costante mobilitazione e incertezza senza arrivare
all'esaurimento?

D'ora in avanti ho intenzione di rimettere in discussione la nostra
inefficacia e lasciare poco spazio alle mie emozioni. Non prendetemi per
un insensibile. Vivo con un nodo allo stomaco al pensiero di tutto
quello che l'umanità sta distruggendo: il fiume dalle rive verdeggianti
destinato a diventare un parcheggio, la foresta piena di vita e di
mistero rasa al suolo perché sorge sopra un giacimento di litio. Simili
sconfitte sono tanto estenuanti quanto demoralizzanti ed è proprio
questo che mi porta a dubitare dell'efficacia della mia lotta.

## Punti deboli della strategia difensiva

### perdita di mobilità

Difendere una casa occupata o una ZAD ci rende facilmente localizzabili,
concentrandoci in uno spazio ristretto. Se la polizia o un gruppo nemico
vuole attaccarci, sa dove trovarci. Per pescare in un colpo solo tutt\*
l\* militanti della zona, la soluzione più semplice è fare un raid nella
ZAD più vicina. Anche infiltrarsi diventa più facile: basta avere i
capelli lunghi e qualche tatuaggio per eludere la vigilanza dell\*
compagn\*, senza contare che chi difende è troppo impegnat\* per
rimanere sempre in guardia.

### perdita dell'iniziativa e dell'effetto sorpresa

Questo è forse uno degli aspetti più stressanti di un'occupazione: una
volta che ti sei ben sistemat\* in un luogo, il nemico è libero di
scegliere il momento ideale per attaccare. È il vantaggio di chi ha
l'iniziativa: puoi scegliere le condizioni più favorevoli per te e più
sfavorevoli per chi è in posizione di difesa.

Prendere l'iniziativa permette anche di pianificare in anticipo
l'attacco, valutando i mezzi e le strategie migliori da impiegare.
Viceversa, non prenderla significa rimanere in attesa e reagire al piano
avversario, facendo il possibile per anticiparlo.

### mobilitazione di lunga durata

Per difendere una posizione è indispensabile rimanerci fisicamente e a
lungo. Anzi, è proprio un metro di misura del successo: quanto siamo
riuscit\* a resistere? Un mese? Due anni? Ma si tratta di una profonda
diseguaglianza. Chi difende deve portare avanti una mobilitazione giorno
dopo giorno fino allo sgombero (con un considerevole dispendio di tempo
che impedisce di concentrarsi su altre attività). Alla polizia, dal
canto suo, spesso basta una sola giornata per far sloggiare l\*
militanti. In un tale squilibrio di potere, la polizia sarà sempre
pronta e organizzata per attaccare più luoghi, dato che è lei a
controllare il rapporto di forze e la temporalità del conflitto. Noi
invece dobbiamo fare i conti con la difficile scelta delle zone e delle
cause da difendere, abbandonandone inevitabilmente altre per mancanza di
tempo.

### fare scelte

Questo vale anche su un piano più generale, ad esempio su scala
nazionale. Non siamo abbastanza numeros\* per difendere tutto ciò che
andrebbe difeso. Il numero limitato di militanti ci costringe a
scegliere per cosa combattere: non possiamo opporci a ogni nuova
proposta di sfruttamento del territorio, a ogni nuovo grande progetto
inutile. Così finiamo per scegliere sempre le cause più importanti,
quelle che hanno un valore simbolico più alto, perché suscettibili di
mobilitare un maggior numero di persone. Come scegliere tra la
distruzione di una prateria e l'abbattimento di una foresta? Ho già
vissuto questo tipo di scelta impossibile, particolarmente difficile sul
piano emotivo. Finché continueremo ad adottare una strategia puramente
difensiva, per ogni occupazione ci saranno decine di progetti portati a
termine senza ostacoli. Ogni ora, in Svizzera, sacrifichiamo 2700 metri
quadri *all'ora* all'edilizia o all'asfalto. Tutte lotte mancate per
insufficienza di risorse.

### la non-sconfitta non è una vittoria

Quando difendiamo un luogo, nel migliore dei casi possiamo sperare che
non venga distrutto, o perlomeno che venga distrutto meno di quanto
previsto dallo Stato. Non è una vittoria: è una non-sconfitta. Geoffroy
de Lagasnerie scrive a questo proposito: "Abbiamo resistito a
un'offensiva ma non abbiamo lanciato la nostra. Definendo questa
situazione come una 'vittoria', partecipiamo a una sorta di
trasmutazione dei valori: convertiamo psicologicamente l'ordine attuale
in un ordine voluto, auspicato (siamo felici di averlo conservato) e
quindi abbiamo fatto un passo indietro". Pensiamo alla nostra ultima
vittoria: Notre-Dame-des-Landes.[^1] Abbiamo distrutto degli aeroporti?
Abbiamo ridotto il numero di aerei che atterrano in Francia?
Assolutamente no. E allora in cosa consiste la nostra vittoria?
Semplicemente nel fatto che l'aeroporto non è stato costruito. Senza
contare che vinci, l'impresa appaltata, è stata rimborsata e che sono
già in corso progetti alternativi, tra cui l'ampliamento di aeroporti
già esistenti. Lo dico con profondo rammarico, ma Notre-Dame-des-Landes
non è affatto una vittoria: è stata tutt'al più una non-sconfitta
ottenuta sacrificando un sacco di tempo e di compagn\*.

### per un "contrattacco" dell'ambiente

Accettando una posizione difensiva invertiamo quello che dovrebbe essere
il rapporto di forze. Non dovremmo essere noi a rimanere ferm\* ma il
sistema, dato che le sue infrastrutture più critiche (miniere,
raffinerie, reti di trasporto e di comunicazione) non possono essere
spostate. Se fossimo noi ad attaccare, sarebbero queste infrastrutture a
trovarsi in posizione difensiva. Potremmo scegliere i bersagli da
colpire valutando lucidamente le nostre forze e le nostre debolezze; il
momento più propizio per attaccare; il luogo, l'ora e il metodo che
garantiscono il maggior effetto sorpresa. E il sistema sarebbe costretto
a scegliere quali posizioni difendere o sacrificare. In fin dei conti,
rimango convinto di questa idea: potremmo essere più efficaci, potremmo
ottenere delle vittorie e ricevere più spesso buone notizie.

Abbiamo bisogno di vere vittorie, e le otterremo solo passando
all'offensiva. Ad esempio, il fatto di organizzarci in piccoli gruppi
per andare a sabotare bersagli precisi rimedierebbe ai punti deboli
della strategia difensiva. Prima di tutto, saremmo noi a stabilire la
temporalità del conflitto, costringendo le aziende ecocide° a investire
tempo e denaro per difendersi in permanenza, senza mai sapere quando
arriverà l'attacco. In secondo luogo, rimarremmo in movimento: prendendo
tutte le precauzioni necessarie, potremmo spostarci e spostare il
conflitto in modo da renderlo il più imprevedibile possibile. Inoltre
saremmo noi a decidere l'insieme delle scelte offensive, mentre starebbe
al sistema ecocida decidere cosa può difendere sul lungo periodo e cosa
è costretto a lasciare occasionalmente incustodito --- perlomeno se
fossimo davvero numeros\*. E le tempistiche sarebbero molto meno
stancanti perché avremmo tutto il tempo necessario per rigenerarci tra
un'azione e l'altra, nel pieno controllo della temporalità. Ognun\*
potrebbe riprendere la lotta quando è di nuovo pront\* e in forma.
Infine, avremmo sempre l'iniziativa: saremmo noi a essere l'effetto
sorpresa. Potremmo sempre scegliere le condizioni più favorevoli per noi
e più sfavorevoli per chi è in posizione di difesa. L\* costringeremmo
ad aspettare, a limitarsi a reagire, a *difendere*.

[^1]: La "ZAD de Notre-Dame-des-Landes" è un esperimento sociale nato
    dall'opposizione locale al progetto per la costruzione
    dell'aeroporto Grand-Ouest, progetto lanciato nel 1963 e abbandonato
    definitivamente nel 2018 \[NdT\].
