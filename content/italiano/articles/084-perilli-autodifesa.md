---
author: "Vincenza Perilli"
title: "Autodifesa femminista ed altre alternative"
subtitle: ""
info: "Testo riadattato per la raccolta bolognese"
datepub: "Ottobre 2022"
categories: ["femminismi", "autodifesa", "antirazzismi", "transfemminismoqueer, queer"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "84"
quote: "Urlare. Usare “armi improprie”."
---

*Urlare.*

*Usare «armi improprie».*

*Spegnere sigarette accese negli occhi, o vicino.*

*Usare penne o matite come pugnali su faccia, occhi, e collo.*

*Usare spray, di qualunque tipo, meglio quelli sui quali c'è scritto
«non spruzzare negli occhi».*

*Usare chiavi tenute insieme da un anello, in cui si infila la mano
chiudendo il pugno e lasciando le chiavi all'esterno per colpire facendo
molto male.*

*Ombrelli, ma è inutile darli in testa di piatto, meglio usarli di punta
mirando a faccia, pancia.*

*Cavatappi, raro averne uno a portata di mano, peccato perché è
un'ottima arma usata contro faccia, collo e addome soprattutto se dopo
il colpo si gira...*

Sono solo alcune delle tecniche di autodifesa che le donne possono usare
per opporsi alla violenza esercitata dagli uomini, ed elencate in
*Solutions: Female Rage and Other Alternatives*, l'ultimo capitolo di
*The Politics of Rape*, un libro dei primi anni '70, parzialmente
tradotto e poi pubblicato negli stessi anni da un collettivo femminista
romano.[^1]

Molte di queste tecniche saranno riprese in opuscoli e altre
pubblicazioni prodotte e fatte circolare in quegli anni.[^2] Infatti, se
già alcune tecniche di autodifesa erano state sperimentate storicamente
nell'ambito dei movimenti delle donne e femministi, per esempio dalle
suffraggiste inglesi (che le usavano per contrastare la violenza della
polizia durante le manifestazioni), e dalle operaie tedesche (che, negli
anni '30, dovevano difendersi dagli attacchi nazifascisti), è solo con
il movimento femminista degli anni '60-'70 che vengono messi a punto dei
veri e propri manuali.[^3]

Attingendo a svariate tecniche (anche ad arti marziali come il Jiu Jitsu
e il Wendo) questi manuali divengono, insieme a centri antistupro, Rape
Speak-out e ai gruppi di azione antistupro, dei formidabili strumenti
per denunciare la violenza e opporvi resistenza. Lungi dall'essere solo
un elenco di mosse e tecniche di combattimento, questi manuali, e i
corsi di autodifesa che si organizzano negli stessi anni, erano parte
integrante di un più complessivo percorso di lotta che partiva dalla
lucida e spietata analisi femminista della violenza di genere come
ancorata e generata da uno specifico rapporto di potere: quello che
intercorre tra chi questo potere lo detiene (gli «uomini») e chi lo
subisce (le «donne») . Certo, «uomini» e «donne» non sono qui da
intendere come categorie «naturali» ma sociali, che la discriminante
«sesso» contribuisce a costituire intersecandosi con altre variabili,
quali la «razza», la classe, l'età, l'orientamento, la scelta o
l'identità sessuale. Quindi, vittime della violenza di genere non sono
solo «le donne», ma tutt\* coloro che sono oggetto di un processo di
«inferiorizzazione»: lesbiche, sex-workers, transessuali di ambo i
«sessi», persone non-bianche, bambine e bambini.

E Infatti, questo enorme patrimonio storico e politico (rivisto
criticamente, adattato e ridiscusso) è alla base di molte esperienze di
questi ultimi anni, dai manuali di autodifesa in circolazione in Italia,
come quelli fatti circolare dal Gruppo autodifesa Filo-mena di Milano e
dalle Maistat@zitt@ di Roma, alle tecniche usate nei corsi messi a punto
*da* e *per* donne, lesbiche e trans, come quelli organizzati ad esempio
a Bologna a partire dagli anni Novanta dalle Amazora.

Ma allo stesso tempo assistiamo al proliferare di corsi di autodifesa
«femminili» organizzati non solo da palestre, comuni, associazioni, ma
anche da circoli anziani e ricreativi, poliziotti in pensione,
ex-bodyguard e palestrati precari, nonché da qualche immancabile *Wonder
Woman*, qualcuna di quelle super donne che, magari senza mai partecipare
a contesti collettivi, sono sempre pronte ad aiutare le «sorelle» più
deboli e sprovvedute... Corsi spesso rivolti esplicitamente alla
«sensibilizzazione sul problema delle aggressioni in generale, e di
stupro rapina e violenza domestica in particolare».

Negli scaffali delle librerie intanto si accumula un numero sempre
maggiore di pubblicazioni sul tema, alcune pubblicizzate con frasi
eloquenti come: «Gli impegni quotidiani di una donna la portano sempre
più spesso a essere da sola in situazioni di potenziale pericolo».
«Sola» in questo caso significa ovviamente senza la presenza
rassicurante e protettiva di un uomo...

Tutto questo mi allarma, e mi irrita. Cosa c'è che non va in questo modo
di trovare «soluzioni» alla violenza?

Per cominciare, in questo tipo di discorso, quasi sempre fatto da altri
su di noi, siamo ridotte nuovamente a vittime.

In secondo luogo il complesso rapporto di potere che è alla base della
violenza esercitata dal gruppo dominante sui soggetti «dominati» viene
ridotto ad una questione di «incapacità» fisica e/o psicologica. Siamo
«noi» (donne, lesbiche, trans ...), in quanto potenziali vittime, a
doverci sensibilizzare al problema: lo stupro è un problema delle
«donne», anzi esiste un «problema donne» tout court, così come c'è stato
un «problema nero» che non è mai diventato un «problema bianco»
(nonostante Malcom X...), così come c'è oggi un «problema immigrati»...

Del resto, se fossimo state sufficientemente «sensibilizzate» non ci
saremmo messe ad andare in giro da sole, e a rivendicare autonomia
dentro e fuori casa, cosa che --- dovremmo saperlo --- rende notoriamente
gli uomini piuttosto nervosi... vi ricordate di quella donna uccisa
perché non voleva cucinare?

Per lo meno, ci dicono, avremmo dovuto porci il problema di saperci
difendere...

Ma non temete care signore!... sono in arrivo torme di prodi cavalieri
senza macchia e senza paura pronti ad insegnarcelo, a istruirci, a
sensibilizzarci...

E voilà, in un attimo tutto il sapere che abbiamo accumulato anche su
questa questione viene cancellato con un bel colpo di spugna...

Del resto difendersi non è una cosa troppo difficile, possiamo impararlo
anche noi: come ebbe a dire non molto tempo fa un avvocato a difesa del
suo cliente, per non farsi stuprare basta un morso (un «morsetto» per
l'esattezza). È sempre la solita storia: se una donna subisce uno stupro
è perché, in un modo o nell'altro «l'ha voluto» o «se l'è cercata». Se
poi lo denuncia è una menzogna (era consenziente ma poi, per qualche
strano motivo, ha cambiato idea, insomma la volubilità delle donne di
goldoniana memoria...), perché in fondo sarebbe bastato --- se solo lo
avesse voluto --- «un morsetto» per evitarlo.

Si evita generalmente, in questi casi, di ricordare che anche gli uomini
spesso soccombono, quando sono aggrediti di sorpresa, o da persone di
cui si fidavano, o dalle quali non se l'aspettavano, e che sono
precisamente queste le condizioni in cui si verificano più
frequentemente aggressioni di tipo sessuale, in ambienti che percepiamo
come non pericolosi: l'interno della nostra casa o di altre case che
frequentiamo, e da parte di persone che conosciamo bene, che in alcuni
casi amiamo.

Queste condizioni non possono essere paragonate ad altre situazioni
(guerra, guerriglia urbana, zuffe al bar o simili) dove comunque si è in
uno stato (anche psicologico) diverso, dove è chiaro chi è il nemico dal
quale dobbiamo difenderci... che sono poi le situazioni dove gli uomini
devono di solito confrontarsi con la violenza.

Non voglio con questo sottovalutare l'importanza per i soggetti
«inferiorizzati» e «dominati» di corsi di autodifesa in generale, ma
sottolineare che questi hanno efficacia solo e quando vengono fatti in
specifici contesti e percorsi politici più ampi, fra cui il contesto
femminista. Contesto che sicuramente si è enormemente modificato dagli
anni '70, così come siamo cambiate (anche) noi.

Allo stesso tempo però mi sembra importante denunciare questo tentativo
più o meno subdolo di «sfruttare» la violenza sulle donne, non solo ai
fini del «business» ma anche per veicolare messaggi di tipo sessista e
razzista.

Già Angela Davis, nel suo *Sex, Race and Class*, denunciava alla fine
degli anni '70 il mito dello «stupratore nero», che nell'America
razzista era funzionale a giustificare e fomentare l'aggressione
razzista verso la comunità nera. E, seppure con un certo ritardo, anche
in Italia a partire dalla fine degli anni 2000 i movimenti femministi
hanno cominciato a denunciare con sempre più forza la sciagurata
equazione «violentatore = immigrato».

Insomma, abbiamo dimostrato di saperci difendere (all'occorrenza) con
ogni mezzo necessario compreso con morsi, «armi improprie», unghie, e
persino con le forbici... e qui è proprio a Lorena Bobbit che mi
riferisco, nonostante sia stata fatta passare per pazza e ridicolizzata
in barzellette sessiste mentre il marito veniva assolto con formula
piena...

Siamo state capaci di autorganizzarci. Ci siamo date forza e sostegno
reciproco. Ci resta molto ancora da fare ovviamente. Tanto per
cominciare opporci il più possibile alla nostra riduzione a «vittime»
(donne da «difendere», e in particolare difendere dallo «straniero»), e
all'utilizzo strumentale di questa retorica per rafforzare di fatto,
dispositivi di controllo sui nostri corpi e la violenza sessista e
razzista strutturale e sistemica nella nostra società.

E opporci anche, contemporaneamente e con altrettanta determinazione,
alla figura della Wonder Woman, una sorta di «eccezione», estranea e
avulsa da ogni percorso politico collettivo, pronta a intervenire per
aiutare e salvare «le altre», tutte povere e sprovvedute vittime,
incapaci di auto-difesa e auto-determinazione.

(testo originariamente apparso sul blog *Marginalia*)

[^1]: E precisamente il collettivo Limenetimena.

[^2]: Si veda ad esempio il volume *Le violentate* di Maria Adele
    Teodori (1977), forse uno dei primi testi italiani a usare il
    termine «femminicidio».

[^3]: Tra questi andrebbe almeno ricordato l'opuscolo *6, 7, 8...
    Eleven Black Women. Why Did They Die?* pubblicato dal gruppo
    Combahee River Collective nel 1979, che ha il merito di introdurre
    nel dibattito l'urgenza di forme di auto-difesa contro il sistema
    razzista e sessista che esponeva le donne nere alla violenza e alla
    morte.
