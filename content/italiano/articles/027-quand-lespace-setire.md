---
author: "Anonim*"
title: "Quando lo spazio si e s t e n d e"
subtitle: "La « mixité choisie° »"
info: "Testo scritto per la raccolta svizzera romanda"
datepub: "Agosto 2020"
categories: ["autogestione, autorganizzazione", "autoriflessione sulla militanza"]
tags: ["brèche", "crier", "outil", "silence"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "27"
quote: "è l'emozione di esistere insieme"
---

\[*La* mixité choisie *è un termine francese che non esiste in italiano.
Questo termine si riferisce a una pratica che consiste nell'organizzare
incontri riservati a persone appartenenti a uno o più gruppi sociali
oppressi o discriminati, escludendo la partecipazione dei gruppi
considerati come potenzialmente discriminanti (o oppressivi), al fine di
non riprodurre modelli di dominazione sociale. Questa pratica è uno
strumento militante utilizzato nei movimenti femministi, antirazzisti,
LGBTQIA+ e movimenti in difesa delle minoranze di genere e dei
disabili.*\]


 

Quando ci si trova in situazione di *mixité choisie*, di
colpo si crea uno spazio. Lo spazio che si apre, e che diventa
disponibile, si estende. Tutto diventa più ampio.

 

{{%centre%}}
s   p   a   z   i   o   s   o
{{%/centre%}}

 

Nella mia esperienza, a volte ci sono dei silenzi.

\[   \]

<span style="margin-left: 2em;"></span>\[   \]

<span style="margin-left: 4em;"></span>\[   \]

E questi silenzi,

<span style="margin-left: 6em;"></span>\[   \]

<span style="margin-left: 8em;"></span>\[   \]

sono un vuoto.

<span style="margin-left: 10em;"></span>\[   \]

E il vuoto è una cosa che si può colmare,

il vuoto è possibilità,

spazio da occupare,

come un terreno incolto,

come una breccia.

 

A volte non ci sono silenzi,

c'è euforia,

quella che non ci consentiamo,

ovunque

e sempre.

 

È l'emozione di esistere insieme.

È la gioia del rifiuto:

il rifiuto di accettare,

ad ogni istante,

la convivenza con le discriminazioni sistemiche°.

 

A volte c'è magia,

le cose diventano limpide,

evidenti,

naturali,

quando invece sembravano

così difficili da esprimere

e da spiegare

altrove,

in altri spazi-tempo

troppo ristretti.

 

A volte c'è intensità,

e versiamo

le più calde

e le più confortanti

 

l

a

c

r

i

m

e


delle nostre ghiandole

<div><span aria-hidden="true" style="visibility: hidden;">delle nostre ghiandole</span>l</div>

<div><span aria-hidden="true" style="visibility: hidden;">delle nostre ghiandole</span>a</div>

<div><span aria-hidden="true" style="visibility: hidden;">delle nostre ghiandole</span>c</div>

<div><span aria-hidden="true" style="visibility: hidden;">delle nostre ghiandole</span>r</div>

<div><span aria-hidden="true" style="visibility: hidden;">delle nostre ghiandole</span>i</div>

<div><span aria-hidden="true" style="visibility: hidden;">delle nostre ghiandole</span>m</div>

<div><span aria-hidden="true" style="visibility: hidden;">delle nostre ghiandole</span>a</div>

<div><span aria-hidden="true" style="visibility: hidden;">delle nostre ghiandole</span>l</div>

<div><span aria-hidden="true" style="visibility: hidden;">delle nostre ghiandole</span>i</div>

<div><span aria-hidden="true" style="visibility: hidden;">delle nostre ghiandole</span>danneggiate.</div>

 

La *mixité choisie*

non è un progetto sociale

è uno strumento,

è una strategia,

è riposo.

 

Riunirsi in *mixité choisie*

è anche essere consapevoli della propria socializzazione°.

Siamo qui per socializzare in modo diverso,

per parlare con parole nuove

e con nuove intonazioni,

per riconfigurare le relazioni umane,

per costruire le strutture sociali di un domani migliore,

per creare strategie di cura,

per guardarsi negli occhi con la gioia di scoprirsi,

o meglio con la gioia di ri-scoprirsi,

per costruire riparazioni,

per imparare ad amarci, perché spesso

ci hanno insegnato a non fidarci le un\* dell\* altr\*

a pensare che siamo meno brav\*,

meno figh\*,

meno forti dell\* altr\*.

 

A volte ci sentiamo a disagio.

A volte ci sentiamo deboli.

È difficile da ammettere.

 

È difficile da ammettere,

che in un gruppo di 15 persone,

quando non ci sono uomini cisgender°,

mancano alcune competenze,

perché non ci sono state trasmesse,

ci sono state negate.

 

È difficile da ammettere

che in un gruppo di 15 persone,

quando non ci sono bianch\*,

mancano alcune risorse,

perché a queste risorse non ci è stato dato accesso,

perché ci sono state rubate.

 

Ma noi inventiamo delle soluzioni,

impariamo

l'ecologia dei mezzi

e soprattutto,

impariamo,

riprendiamo,

cerchiamo,

rubiamo.


 

A volte, organizziamo

contrattacchi da sogno,

passiamo all'azione.

È un peccato,

ma in *mixité choisie*

le cose vanno più in fretta

E allora arriva la gioia di GRIDARE.

 

{{%centre%}}
<span style="word-break: break-all;">diagiredirettamentesenzaintermediarisenzachiederesenzaspiegarediagireinfretta</span>
{{%/centre%}}

 

{{%centre%}}
QUESTA È

{{%cadre%}}
AUTO
{{%/cadre%}}

EMANCIPAZIONE
{{%/centre%}}

 

{{%centre%}}
FATTA DA NOI

FATTA PER NOI
{{%/centre%}}

 

{{%centre%}}
È UN'ARMA

GUERRIERA

E INNAMORATA
{{%/centre%}}
