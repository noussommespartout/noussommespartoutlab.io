---
author: "Anonim*"
title: "Non torno più indietro"
subtitle: ""
info: "Testo scritto per la raccolta bolognese"
datepub: "Novembre 2022"
categories: ["femminismi", "transfemminismoqueer, queer", "spazi, occupazione, diritto all'abitare"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "83"
quote: "parlare liberamente, però, fa paura"
---

Infreddolite nelle domeniche di fine inverno, ci aggiriamo tra i
capannoni.

Marta ha risposto, arriverà prima o poi.

Seguono molti passi nel grigio e lembi di sciarpa rimboccati, fino a
quando c'è la pausa.

Ecco Marta, ecco le altre.

«L'otto marzo sarà sciopero globale»

«I sindacati confederali non ci appoggiano»

«Come facciamo a scioperare?»

Non conosci nessuna di loro, sai solo che le reti delle compagne sono
magiche e arrivano ovunque.

Parlare liberamente, però, fa paura. I padroni ci osservano: il ricatto
è grande, se sei una lavoratrice, se sei donna, e se sei straniera
ancora di più.

Eppure fuori dalla fabbrica, approfittando del quarto d'ora d'aria per
confrontarsi e organizzarsi, insieme, sembra che si ritorni a sperare.

Ci si saluta intrecciando i fili dei contatti.

--- Ti lascio il mio numero.

--- Grazie, ti scrivo della prossima assemblea.

--- A presto!

Un ultimo sorriso e ognuna dentro il suo capannone a portare avanti una
vita in un mondo che ti vuole produttiva e riproduttiva, dandoti in
cambio pochi spiccioli nelle tasche e sparute briciole sul tavolo.

Ma noi non ce la facciamo più, noi veramente adesso siamo stanche. E lo
siamo per noi stesse, ma anche per tutti gli altri.

Ci siamo prese in carico la complessità di questo mondo, rifiutando la
retorica eroica degli esseri sacrificali, ma con l'intenzione di essere
il motore della vera rivoluzione, quella che se non è di tutte e tutti,
e tuttu, se non è transfemminista°, allora non sarà.

E questo ce lo ribadiamo nelle assemblee del martedì, dove rivedi con
felicità, tra le altre, alcune di quelle donne con cui ci si era
scambiate i contatti in fabbrica. Non per tutte è facile organizzarsi
per venire, non tutte lo vogliono, un po' per disillusione, un po' per
incredulità. Un giorno anche le assenti di oggi saranno insieme a noi,
ne ho la certezza.

E si procede per turni di parola scritti su innumerevoli documenti
aperti sul pc della malcapitata incaricata della scrittura del report.
Non tutte parlano, pur trovandoci in uno spazio sicuro. Ognuna ha
bisogno dei suoi tempi per comprendere non solo le dinamiche ma alle
volte anche semplicemente le parole, le abbreviazioni e il gergo; non
tutte abbiamo una formazione politica o lo stesso background, non tutte
abbiamo l'italiano come prima lingua. Alcune parlano moltissimo, altre
poco, altre per niente. Non tutte abbiamo le stesse prospettive di
ragionamento: le assemblee non sempre sono facili, non sempre sono
lineari, non sempre ne esci con il cuore leggero.

Tutte, però, siamo accomunate dal vivere dentro la complessità dei temi
di questa lotta: parlare insieme di lavoro, relazioni, migrazioni,
lingua, ambiente, sessualità, riproduzione può sembrare confusionario
inizialmente se non hai dimestichezza nel ragionare sull'affastellamento
compulsivo di nodi da risolvere che ci dà questo mondo, ma il fatto di
viverli concretamente, nella realtà quotidiana, piano piano dissipa la
matassa e si apre davanti un panorama di intersezioni di cui il
femminismo riesce a trattare e per cui riesce a lottare.

Mi sono avvicinata a Non una di meno nel 2016, anno in cui è nata in
Italia: non era una prima esperienza per me, ero appena trentenne e
avevo già militato in altri collettivi, ma nessuno di questi era «solo»
femminista, tutti mi erano stati in qualche modo stretti; ho scelto di
entrarci perché mi entusiasmava prima di tutto la sfida che si poneva,
che andava ben oltre l'autocoscienza, il territorio e un singolo tema,
come può essere l'aborto, al contrario si prendeva in carico il
tentativo di sovvertire completamente l'ordine delle cose attraverso
teorie e pratiche politiche che applicano lo sguardo femminista alla
totalità del presente e del futuro e si spingono verso la
transnazionalità; inoltre pensare che stavamo replicando in varie parti
del mondo quello che le donne argentine, attraverso *Ni una menos*,
erano state e sono capaci di fare, mi faceva sentire in un processo di
cambiamento concreto, reale, globale. Ho creduto e credo che le
rivoluzioni comincino proprio così e non potevo non farne parte.

Il primo otto marzo, ho pianto, ho pianto tanto.

Non mi sembrava vero che quel processo che avevamo iniziato qualche mese
prima, che dall'altra parte dell'oceano era iniziato un paio d'anni
prima e, con una grande ondata, ci aveva raggiunto, avesse riunito così
tante persone, con così tanta forza e determinazione.

Ma quindi, tutti gli esseri umani hanno bisogno di femminismo, per
davvero!

Sento ancora i brividi dentro il cappotto emozionandomi nel camminare in
corteo insieme alle altre, nel battere mestoli su pentole malandate e
ridendo, ridendo molto, ridendo felici, cantando cori.

«Insieme siam partite, insieme torneremo. Non una, non una, non una di
meno!».

Quanto abbiamo urlato per accogliere gli interventi in piazza! La voce,
il giorno dopo, è sempre la grande assente.

Finito il corteo serale, le compagne ci avvisavano «Abbiamo occupato!
Oggi è nato un nuovo spazio transfemminista a Bologna!». E, come uno
sciame di api, tutte a rincorrere il nostro entusiasmo dirigendoci verso
la nuova e favolosa occupazione transfemministaqueer°: uno stanzone
quasi unico costituito da grandi magazzini vuoti, ai quali si arrivava
scendendo una rampa, già belli graffitati, illuminati, agghindati e
rinvigoriti, come un malato, prima pallido, che riprende colore. Tempo
un'oretta e la prima assemblea era già in piedi e poi via con la musica
e la gioia. Non che sia durata molto: la repressione da qualche anno in
qua si fa sentire più dura[^1]. Ma non ci arrendiamo e, anche se con una
frequenza più bassa, lo rifacciamo: ci riprendiamo fisicamente lo spazio
che ci serve per incontrarci e mescolarci, in sorellanza.

La sorellanza non è un mito e non vuol dire che si è tutte amiche. Non è
così e non potrebbe esserlo.

--- Se cadi, ci sono qui io! --- ci si dice. Nessuna deve più sentirsi
inadeguata, sconfitta, in colpa e se succede --- perché sì, in questo
mondo è facile che succeda --- c'è la rete che, se ti butti, ti accoglie
tra le maglie.

Certo, però, la sorellanza non basta. Ci vuole organizzazione,
discussione, studio, applicazione, confronto, azione, strategia. È un
lavoro che si sceglie di fare per il destino dell'umanità. Ed è quasi
l'unico lavoro che, una volta che lo cominci, è impossibile mettere da
parte, perché diventa il significato della vita stessa.

E così, ognuna di noi affianca alle proprie attività di lavoro, in casa
e fuori, anche questo compito, che per molte ha un carico materiale e
immateriale importante, che influenza le nostre esistenze
irrimediabilmente. Alle volte stanca ed esaurisce e così le birrette
contornate da noccioline e autocoscienza, o gli spazi di solitudine,
sono ricarica pura, altre volte arricchisce e riporta vita alla vita.

Chi ce lo ha chiesto? La rabbia e l'amore. La rabbia degli interventi
nei cortei e dell'urlo muto, quando ci si accuccia in silenzio per
qualche minuto e poi si scoppia in un grido di rivolta tutte assieme per
le strade. E l'amore che davanti al carro nei cortei ci fa ballare, ci
esalta, ci rende libere e potenti.

Cosa ci fa sentire forti? Sapere che siamo ovunque. E così una mattina
prendi il tuo giacchetto, inforchi la bici e arrivi dalle compagne
iraniane che, in piazza, cantano in una lingua sconosciuta e ballano in
modi differenti dai tuoi. Urlano «donna, vita, libertà», tu le guardi e
sai che niente vi divide, che siamo dalla parte giusta della storia e
che, unite, vinceremo.

Devo ammettere che essere femminista, essere una compagna, avere una
prospettiva comunista libertaria sulle esistenze e condividerla con
altre e altri, non sempre mi rende la vita più facile: avere una
coscienza di questo tipo ti fa sentire continuamente inadeguata nel
mondo «fuori dalla bolla», nel lavoro, negli ingranaggi della
formazione, nelle relazioni sociali esterne a quelle intrecciate nei
luoghi della politica dal basso, a volte persino nella famiglia
d'origine. Senti che quando esprimi le tue opinioni, gli altri, che
quella coscienza non l'hanno (ancora?) sviluppata, ti guardano come
un'aliena, non ti capiscono, per loro sei una che parla di qualcosa che
non li tocca, perché vivono nella convinzione di una supposta
«naturalità» del mondo del capitale in cui tutti e tutte viviamo.
Ascoltando molte persone non politicizzate a volte penso che è proprio
vero che «è più facile immaginare la fine del mondo che la fine del
capitalismo». Mi sento di avere un'energia e una spinta al cambiamento
che il mondo dove sono nata non accoglie, anzi schiaccia e reprime
considerandole da una parte fantasiose utopie, dall'altra pericolose
teorie. Vivi un'esistenza di contraddizioni costanti a cui sei costretta
a sottostare, ma che disprezzi profondamente e disprezzandole disprezzi
anche una parte di te stessa che deve rimanere attaccata a quel sistema
per pura sopravvivenza, esperisci sensazioni di ribrezzo continue,
quotidiane verso quello che accade intorno a te, vicino o lontano che
sia. Tutto ciò costituisce un po' uno stillicidio insano che ti lascia,
a volte, senza voglia di uscire di casa, talvolta senza voglia di
vivere.

Ma, c'è un gigantesco «ma» che non deve far scoraggiare nessuna per
quello che ho appena scritto, pur rimanendo crudamente reale!

Nonostante questa cruda realtà, io a tutto quello che sono e sento non
rinuncerò mai: posso dire con estrema certezza che essere una
femminista, una compagna con la sua prospettiva comunista libertaria e
condividere con altri e altre percorsi, lotte e vite mi ha estirpato dal
cuore l'aridità di un mondo in cui, per cultura e sovrastruttura
socio-economica, non esiste amore. Questo vale tutto e mi rende la
persona che desidero essere, insieme a tutte le altre. Indietro non
torno più: la vita ha senso solo se trascorsa insieme per impegnarsi a
costruire un mondo altro.

Io non mi accontento di cambiare il mio destino: questa è la soluzione
che il capitalismo neoliberista ci dà come banale consolazione alle
nostre piccole e insignificanti esistenze, disgregandoci. Io voglio, noi
vogliamo cambiare il mondo perché solo se lo facciamo unite e per un
destino altro, che va oltre quello personale, tutte e tutti potranno
godere della bellezza.

Il desiderio, l'aspettativa sul futuro, quindi, è una sola: amore e
rivoluzione.

In tutto il mondo, vogliamo vederla con i nostri occhi, non possiamo più
attendere. E intrecciare i fili delle nostre vite al di là di qualsiasi
confine materiale o immateriale è la pratica che ci salverà come umanità
e che ci porterà ad accendere questo fuoco di liberazione, ovunque.

[^1]: Il 9 marzo 2017 polizia e carabinieri sgomberano la consultoria
    transfemministaqueer° di via Menarini; ad accoglierli le note di un
    vecchio canto di mondine rivisitato in chiave queer: «O lioliolà,
    contro il macho noi siam qua, siamo qui tutte compagne, froce trans
    lesbiche e cagne» ([consultoriaqueerbologna.noblogs.org](https://consultoriaqueerbologna.noblogs.org/)).
