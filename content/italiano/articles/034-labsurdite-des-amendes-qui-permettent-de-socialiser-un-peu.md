---
author: "Anonim*"
title: "L'assurdità delle multe che permettono di socializzare un po'"
subtitle: "Lavorare senza averne il diritto"
info: "Testo scritto per la raccolta svizzera romanda"
datepub: "Settembre 2020"
categories: ["migrazioni", "antirazzismi", "polizia, repressione, amministrazione giudiziaria"]
tags: ["absurde, absurdité", "amende", "cuisiner, cuisine", "foyer", "pouvoir"]
langtxt: ["fr", "it"]
zone: "Suisse romande"
num: "34"
quote: "non ottengono il diritto di asilo"
---

In quanto collettivo che lotta nel campo dell'asilo, svolgiamo un
lavoro di sostegno, informazione e accompagnamento per le persone che si
trovano in una procedura di asilo o al di fuori di essa, ma facciamo
anche un lavoro di denuncia e di rivendicazione a livello politico e
pubblico. E a volte questi due livelli sono contraddittori.

Una delle tante assurdità del sistema di asilo, è che l\* richiedenti
asilo respint\*, cioè le persone che non ottengono il diritto di asilo,
devono lasciare il territorio, ossia non hanno il diritto di rimanere in
Svizzera. Se le autorità se lo possono permettere, arrestano queste
persone e le mettono sul primo aereo per espellerle. In pratica, però,
le procedure di espulsione richiedono spesso anni. È assurdo: queste
persone a volte rimangono in Svizzera per otto, nove, dieci anni, senza
il minimo diritto di soggiorno. Per tutti questi anni rimangono in case
di accoglienza per richiedenti respint\*, in condizioni schifose, in
baracche marce affittate da padroni di casa che si fanno un sacco di
soldi. E una volta alla settimana devono recarsi alla polizia dell\*
stranier\* --- ora è diventato il servizio demografico, suona meglio... ---
per ottenere un timbro che permette loro di ricevere aiuti d'emergenza.
In pratica, queste persone non hanno il diritto di stare in Svizzera,
sono completamente illegali, ma la loro presenza è ufficiale e
controllata, quindi legale. E naturalmente, quando queste persone girano
per la città, vengono regolarmente controllate dalla polizia e multate
per soggiorno illegale. Queste multe possono accumularsi. Per esempio,
c'è il caso di una persona che ha ricevuto nove multe. Il totale
ammonta a oltre 4.000 franchi svizzeri. E va sottolineato che queste
persone vivono solo con aiuti di emergenza, cioè con 10 franchi al
giorno.

Quando queste persone ricevono le multe, spesso si rivolgono prima alla
ORS (Organisation for Refugee Services), la società che gestisce le case
di accoglienza. La ORS dice loro di accettare la multa e di pagarla a
rate. Alcune persone si sono rivolte a noi con le loro multe e abbiamo
iniziato a fare ricorso con loro. Quando si fa ricorso e non si paga una
multa, il servizio di libertà vigilata trova altri modi per fartela
"pagare".

In breve, si riceve un modulo con due scelte: o vai in prigione o paghi
(aliquote giornaliere). Insieme alla persona che stavamo seguendo,
abbiamo spuntato la casellina "lavoro". Ci siamo dett\* che non avrebbe
funzionato perché bisognava menzionare il permesso di soggiorno, che
quella persona non aveva, per dimostrare che poteva lavorare legalmente
in Svizzera. E invece... ha funzionato! Lei era così felice. Passare
centinaia, migliaia di giorni in quella casa di accoglienza era una
morte intellettuale e affettiva. Ora invece poteva finalmente andare a
lavorare. Era felice di essere utile, di poter esistere agli occhi degli
altri. Non potevamo credere che avesse funzionato.

Credevamo che chi si occupava della pratica avesse commesso un errore,
che forse non ci avesse fatto caso. Alla multa successiva, abbiamo
riprovato a fare ricorso e bum, ha funzionato di nuovo. Ha funzionato
così per quattro o cinque volte. In questo modo, la persona che abbiamo
accompagnato ha potuto lavorare in case di riposo o nella cucina della
mensa universitaria, ad esempio.

Poi ci siamo dett\* che dovevamo denunciare questa assurdità, che
dovevamo fare qualcosa affinché queste persone non ricevessero più
multe. Così abbiamo messo in piedi una piccola azione, ne abbiamo
parlato con il Consiglio di Stato, che era ben consapevole
dell'assurdità della cosa, e anche con un ex giudice, che anche lui ha
ritenuto assurdo fare multe di 600 franchi a persone che hanno 10
franchi al giorno per vivere.

Ecco come stanno le cose: volevamo denunciare pubblicamente l'assurdità
della situazione di queste persone che la Svizzera rifiuta di ammettere
sul suo territorio ma che autorizza a lavorare. Volevamo parlarne con i
media. Ma quando ci siamo res\* conto di quanto potesse fare bene
lavorare piuttosto che stare in una casa di accoglienza, ci siamo
dett\*: "Meglio di no". E non abbiamo detto nulla.

