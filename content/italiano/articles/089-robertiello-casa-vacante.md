---
author: "Mattia Robertiello"
title: "Casa vacante"
subtitle: "Praticare la fine per praticare il possibile"
info: "Testo scritto per la raccolta bolognese"
datepub: "Ottobre 2022"
categories: ["transfemminismoqueer, queer", "arti, musica, fotografia, performance"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "89"
quote: "No. Stavolta deve essere diverso."
---

Ale dice che servirebbero più studi sulle droghe e la riduzione del
danno in chiave femminista.

Scopriamo un sottobosco enorme non bianco che parla lo spagnolo chiamato
narcofemminismo. Vogliamo fare traduzione militante. Ieri al Tank, via
Emilio Zago, abbiamo chiesto a Diana J. Torres come immagina un uso
pornoterrorista delle sostanze.

Ale e io siamo rapite dalla foodification, da guerra e droga e
patriarcato, dalla caccia alle streghe, dai nuovi processi di
sorveglianza: i libri che attraversano Modo Infoshop, via Mascarella,
sono schermi in cui proiettiamo i nostri desideri politici e letterari.

Modo Infoshop, due settimane fa, la presentazione di una ricerca
foucaltiana attraversata da corpi queer° e poly è il mio battesimo in
questa città.

Ale e io veniamo da città diverse nella stessa regione, mi capisce,
capisce da cosa sono scappato, psicogeograficamente. Anche lei è una
figlia di Madame Bovary.

A casa di Ale, via dei coltelli 17, riesco a fare pace con i sensi di
colpa e la paura che sia tutto inutile. Le parlo di Nico. Stavolta è
diverso. Ho tanti appoggi. C'è Nico. Ce la faccio.

Ale mi capisce quando le parlo della trappola in cui finisci quando vuoi
scappare da un posto ma non hai gli strumenti. Riesci a scappare ma non
hai modi per restare.

E torni. Peggio di prima. Caduta. Ma come cazzo ho fatto a tornare?

Domani parto, mi trasferisco a Bologna. Non dovevi dirmelo così. Non
dovevo dirlo così.

Potevo fare meglio. Curarmi meglio dei miei affetti. Potevo fare tutto
meglio. Senza fare danno. Casino. Come sempre.

Se vuoi andare a Bologna devi lavorare. Ti devi organizzare. Avere un
piano.

Non ce la puoi fare.

No. Stavolta deve essere diverso.

Prima autocolpevolizzarsi, poi autoindulgenza. Grazie Ale.

Porta San Vitale, aspetto Nico. Cercare casa.

Via Massarenti, una scuola abbandonata. Chiusa.

Chiuse anche le villette di via Azzurra e dal palazzo in San Donato un
signore ci caccia via perchè è franato.

Un supermercato è il primo posto fattibile che troviamo, grossi open
space e uffici più piccoli abitabili ma è già squattato da alcuni tizi
in tenda che fanno il fuoco e le pere.

Nicola mi racconta dello squat in cui viveva a Torino. Uno zoo
abbandonato. Vuole fare ricerca, dice. Ma la ricerca accademica fa
schifo.

Ha recuperato un sacco di strumentazioni nel tempo. È un ingegnere del
materiale. Vuole fare ricerca dal basso contro ogni gatekeeping infame.
Ricerca e bioarte.

Non ci capisco un cazzo ma ascolto, sono ispirato da cose con i funghi
che non capisco.

Una casa la troviamo. Merda di piccione marcia ovunque, c'è da pulire,
c'è da mettere le finestre, c'è da mettere il gas. Ma ci siamo.

In laboratorio per fare ricerca sui tumori, mi spiega, prendono i topi e
li irradiano di raggi Uva ultraviolenti. Restano corpi mutati, carne
morta inerme studiabile.

Viale Zamboni, storia della scienza.

Noia mortale. Paracelso sarebbe anche interessante ma questo stronzo è
una flemma, fossile, frontale e nozionista. Me ne voglio andare me ne
voglio andare me ne voglio andare. Odio questo rito del cazzo.

Non c'entro niente qui. Non mi vogliono, non mi voglio nemmeno io. Tutte
facce attente e ferme.

La mia faccia guarda l'uscita, aspetta qualcuno che esca fuori. La mia
faccia vuole scappare.

Esco a scroccare una sigaretta.

No, non studio filosofia. Vorrei però. Cioè non lo so. Praticamente sto
facendo orientamento sul campo.

È che sono un essere multidisciplinare e antidisciplinare.

Seguire il mio flusso di coscienza costante fuori da qualsiasi
imposizione accademica.

Ma hai casa? Eh no, non ho casa. Sto facendo divano. Non so se avete
presente. Una settimana qua. Una settimana là. In balia degli eventi.
Sono in viaggio.

Bohemien. Fluidità totale.

Wau, figo.

Figo sì. Quello che dico.

Mi ospiti qualche notte anche tu? Quello che non dico.

Sono un morto di fame che vuole vivere qua ma che non ha assimilato la
retorica del riscatto attraverso l'etica lavorativa.

Se non vuoi lavorare, risate, allora devi fare la puttana. Questo è il
retaggio patriarco-provinciale che ascoltano le figlie e i figli di
Madame Bovary quando si dissociano un attimo dai loro amici e, davanti
alla solita birra del solito bar, un pò tristi, un pò annoiati, un po'
ubriachi dicono a un coglione che il loro desiderio frustrato e
frustrante è andarsene via, via e basta.

Vorrei dire che ho fatto anche quello. Vorrei dire coglione è un lavoro
anche quello. Sex work is work? Mai sentito parlare? Vorrei dire ho
indossato anch'io la tua pelle docile.

Ho portato i piatti ai coglioni come te, ho montato i letti sanitari ai
corpi allettati dell'anzianità lancianese di cui presto farai parte, ho
segato il cazzo gonfio e sporco di un vecchio e quello è stato il meno
peggio finora.

Perchè sei partito e sei tornato?

Poi già che sei qui a ubriacarti con gli amici al posto di lavorare
significa che non te ne vuoi andare davvero, non te ne vuoi andare,
forse ti piace, in fondo in fondo ti piace, vorrei urlare, vorrei
spaccare ogni bar di questa città merdosa, ma poi là che fai, spaccarti
la faccia, spaccare tutto, vorrei dire mi sono messo questa pelle sopra
l'essere squamoso che abitava da mesi il mio letto, un silenzio, un
letargo, mamma controllava se respirava, se era vivo o morto. Devo
strapparmi questa pelle di dosso.

Nemmeno chi ha l'indeterminato trova casa. Tu chi cazzo sei?

Faccio parte di quei corpi più facili da espellere dal sistema
socio-immunitario di una città sempre più classista e aperta ai processi
di gentrificazione e capitalizzazione immobiliare. Non ho un lavoro, non
ho una fissa dimora e per adesso non ho nemmeno una posizione di studio
istituzionalizzata.

Animale marsupiale, uno zaino lungo un metro e mezzo attaccato alla mia
schiena dove deve stare tutto. Essere in fuga ma lento, strisciante.

Ma almeno una borsa di studio. No, nemmeno quella ma spero per l'anno
prossimo.

All'uscita un volantino dice: cerchi casa e trovi un divano? Occupazione
abitativa in via Capo di lucca 22.

Ma è uno squat? Squat è riduttivo, dicono.

{{%asterisme%}}

Un sacco di gente. Il posto è gigante. Un condominio Asp[^1] intero,
vuoto, in vendita da anni.

Una trentina ammassati in una stanza.

Sulle porte sono ancora attaccati i cognomi degli ex inquilini. Spettri
di famiglie sfrattate sono seduti insieme a noi a parlare di
gentrificazione e mercificazione urbana.

L'occupazione è un progetto politico, spiegano.

Obiettivo: sia rispondere alle necessità materiali sia aprire conflitto
e proposta politica.

Dani parla di Htc: laboratorio di riappropriazione dei saperi tecnici
dal basso.

La priorità è la conversione ecologica. Contro la crisi ecologica non
basteranno saperi e tecniche calati dall'alto.

Collettivizzare i saperi e contrastare il gatekeeping. Dani parla la
stessa lingua di Nico.

Lea apre una riflessione politica, semantica sul senso di abitare. Il
rapporto tra corpi e spazi. Come vengono immaginati, riprodotti e
disciplinati.

Diocane ci sto. Sì cazzo.

Guarda che ha i suoi sbatti anche occupare. Le responsabilità. Le
responsabilità. Le responsabilità. Le responsbailità che non ti sei mai
preso in vita tua, coglione, pezzo di merda, sfigato, ridicolo.

La paura di essere espulso da questa città che desidero abitare mi
riempie le viscere.

Paura fottuta.

Ma devo, voglio resistere.

Non diventerò uno spettro. Un accollo. Un parassita.

Questa casa è la mia possibilità, nucleo creativo, affettivo,
sperimentale indipendente da ogni ricatto legalizzato. Una via per
l'emancipazione dall'affitto e dal lavoro salariato.

Posso restare qui? Non ho un posto dove dormire. Sto facendo divano. Mi
interessa anche il progetto politico.

Eddai eddai eddai eddai eddai eddai eddai eddai eddai.

C'è una lista di numeri.

La mia camera è un puzzle minimal di materassi buttati a terra.

Altri materassi continuano ad entrare e salire le scale insieme ai primi
mobili.

La notte la passo infilato dentro il sacco a pelo, non fa freddo, il
contrario, il mio corpo accede allo stesso stato embrionale che
attraversa la camera.

Regressione cellulare. Sonno amniotico. Metamorfosi.

Sto perdendo le mie squame.

Reset.

Lo student hotel in Bolognina cambia nome in social hub per fare
rebranding ma è sempre lo stesso morto urbano legalizzato. Nuova
inaugurazione.

Noi vogliamo rompere il cazzo. C'è un aperitivo nell'atrio in cui ricchi
e disperati in cerca di una stanza bevono lo stesso spumante e mangiano
le stesse stronzate.

Riempiamo tutta la reception di fumo rosso, fumo blu, una voce unica,
corpi incazzati.

Via delle Moline, assemblea vicino un posto attacchinato.
Attacchinaggio, etichettamento dal basso. Wonderful Italy è un mostro
immobiliare dentro Airbnb che affitta questo posto e altri centinaia a
centinaia di euro al giorno. Dieci notti l'anno. In questa lotta è
fondamentale la riappopriazione della datistica abitativa e immobiliare.

Mamma. Ti sei iscritto? Stai cercando un lavoro? Sì sì con calma.

Mi sto concentrando su altro. Ti serve qualcosa, io posso darti poco lo
sai?

Finchè sono qua mi bastano pochissimi soldi, ho ancora un po' di soldi
da parte. Posso evitarmi un full time, posso evitarmi un fake part time.
Mi iscrivo al bando Ergo[^2] delle 150 ore.

Poi vedo dai. Comunque sì, mi sono iscritto.

Mamma. Ma vi lavate? La doccia? Come fate? Però ti sento bene.

Abbiamo tutto.

Sul pannello di legno in ingresso cominciano a moltiplicarsi i pezzi di
giornale attaccati che raccontano il pezzo di storia che stiamo
scrivendo.

Qua a Casa vacante c'è pneuma.

Un soffio vitale filobiblico parte dai pezzi di giornale attaccati in
atrio, attraversa i materassi sui carrelli, le buste di cibo, le birre,
i cornetti della colazione, i frigoriferi, i tavoli dove mangiamo, i
divani, le cassettiere pesanti come un cristo morto che stiamo salendo
adesso sulle scale; attraversa tutti i corpi che attraversano questa
casa e gli oggetti che la formano.

Cosa significa abitare?

L'abitante dovrebbe essere sempre in posizione creativa verso l'oggetto
casa.

Collettività abitante militante demiurgica.

Abbiamo eliminato l'arredamento in forma consumistica, l'affitto, le
bollette, tutti i ricatti economici e burocratici che soffocano il
soffio vitale che siamo capaci di infondere in ciò che ci circonda e
ricevere da ciò che ci circonda.

Il nostro soffio vitale però non obbedisce alla mistica eterosessuale,
non è una sostanza simil-spermatica che ha origine in un corpo egemone e
si diffonde su tutto il resto.

È compost di corpi e oggetti, collettivo e contaminante.

Animale marsupiale, fino a ieri il mio armadio era uno zaino da
campeggio lungo un metro e mezzo.

La modifica spaziale della mia stanza è una modifica corporea. Fuori dal
ricatto dell'affitto, dai circuiti di mercato, la simbiosi tra corpi e
spazi e oggetti è ancora più forte.

Ma quindi che vuol dire abitare? Abitare uno spazio, abitare un corpo?

Via Lodovico Berti, Nuovo cinema Nosadella. Filosofia morale. Un faro
nell'università che mi riempie il cuore. Frequenterò solo questo.

Pia Campeggiani fa un discorso che distrugge completamente la patina
oggettivante che pretendono alcune scienze e ridà legittimità a molti
vissuti.

Adrenalina, nostalgia, sadismo, masochismo, commozione, depressione
ansiosa, sono tutti esempi di stati emotivi misti di cui possiamo
constatare la realtà. Ma i modelli psicologici, le pratiche
terapeutiche, le ricerche neuroscientifiche sono ancora ferme al dogma
dell'emozione unica di ispirazione socratica.

Aristotele l'aveva già messo in crisi.

Non puoi provare più cose insieme, se dici di provarne è perchè sei
confuso.

L'intellettualismo etico diventa terapeutico.

Via Carlo Pepoli 41, passo a casa di Rocco a prendere alcune cose.

Rocco non c'è, mi apre Matteo. Come va? Hai trovato casa? Sì.

La mia valigia-armadio nel suo armadio, i miei vestiti buttati sulla
sedia insieme ai suoi. La mia fotocamera in mezzo ai suoi libri. Ma
adesso è sua. Più sua che mia.

E io sto mangiando sempre meno carne da quando Rocco ha smesso.

Non è una questione di morale. La morale fa schifo. L'antispecismo
moralista non serve a un cazzo, dice. Siamo esseri amorali, noi due. Il
punto invece è la filosofia della mente. La filosofia del linguaggio.
L'invenzione antropocentrica della coscienza. Il potere verbale.

Giusto e sbagliato non sono coordinate di crescita possibili.

Osmosi costante. Pezzi di me in giro.

Ho traccia sul mio corpo dei posti che attraverso e dei corpi con cui mi
contamino.

Quanto durerà questa stagione della mia vita che chiamerò utopia?

Il micelio ha tante applicazioni. Cosmetiche, tessili, energhetiche.

Guarda, c'è questo che ha fatto una sedia di micelio, foto. Mi interessa
anche il lato più artistico e performativo.

Farmacologiche? Aggiungo. No, davvero. Non intendo solo sballarsi con la
psilocibina.

La tachipirina si può fare, tipo.

Facciamo una farmacia punk do it yourself in cantina. Subito.

Ascoltare Nico che parla di micelio mi accende mille sinapsi, vengo
trascinato dalle sue parole giù nelle cantine di Casa vacante. Riesco a
immaginare centinaia di funghi che attraversano, liberi, lo spazio
grigio e battuto delle cantine. Mangiano la nostra immondizia.

Sputano tessuti. Sputano farmaci.

Alleanze interspecie contro la crisi ecoabitativa.

Ma non hai ansia che ti sgomberano? Cosa farai dopo?

Avrei ansia se dovessi lavorare 8 ore al giorno e pagare un affitto che
costa più di metà del mio stipendio di merda.

La mia vita adesso è inserita in una temporalità non lineare in cui
l'ansia non è un'emozione temporale possibile, un movimento utopico che
procede piano piano verso lo sgombero. Dopo lo sgombero tutto cambierà
ma non posso sapere come.

Impossibile immaginare il post-sgombero.

Immaginare lo sgombero invece è un esercizio collettivo.

Come sarà? Chi ci sarà? Che cazzo succede.

Occupare come esercizio stoico.

Autosorveglianza notturna come meditazione. Praticare il non
attaccamento. Prepararsi mentalmente alla fine.

Caffè e Netflix, serie tv droganti sviluppate col preciso obiettivo di
creare disturbi del sonno, dipendenze e consumi. Ce ne riappropriamo per
restare svegli tutta la notte. Disciplina militante. Stato di allerta
continuo.

La porta piena di lucchetti. Chi ci sarà in turno?

Chi avrà questa responsabilità?

Svegliare tutti. Resistere. La porta sbatte. E risbatte. Vogliono
spaccare tutto. Entrare. Cacciarci via.

Bloccare la porta. Il tavolo, il divano, panico, muoviti, svegliati,
blocca 'sta cazzo di porta. Bloccare.

Fumogeni.

E quando entreranno.

Saremo su, in caffetteria. Fermi. Immobili. Assorti. La bombola del gas,
facciamola scoppiare. Strage.

Sul tetto.

Nella resistenza passiva convergono posture militanti e posture
meditative. Come il Sitting dharna[^3].

Panico, se qualcuno va nel panico. Come prenderci cura? Come. Ma non c'è
solo panico, c'è anche pace.

Pace rilassata di chi aveva già previsto, di chi era già pronto. Rabbia
e panico insieme.

Rabbia e pace insieme.

La militanza è attraversata da stati affettivi misti.

Rilassamento meditativo di chi non è attaccato a un luogo materiale che
si è preparato da subito a lasciare misto al piacere adrenalinico della
lotta, alla rabbia dell'ingiustizia subita.

Non è attaccarsi a un luogo, a un oggetto-casa, nemmeno a un modello. È
attaccarsi al fatto che vivere così è possibile, è stato possibile,
anche solo per un po' e può esserlo ancora. Ci vogliono arte,
organizzazione, disciplina anche per fallire, finire, praticare la fine
per praticare il possibile.

[^1]: Azienda pubblica di servizi alla persona, quasi interamente di
    proprietà del Comune di Bologna, ndr.

[^2]: Agenzia per il diritto allo studio in Emilia-Romagna, ndr.

[^3]: V. Sit-in°.
