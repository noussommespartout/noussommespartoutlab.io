---
author: "Samba"
title: "Storia di una lotta"
subtitle: "Una nuova vita da imparare"
info: "Trascrizione di una conversazione orale (tradotta dall'inglese) — raccolta svizzera romanda"
datepub: "Aprile 2020"
categories: ["migrazioni", "antirazzismi", "polizia, repressione, amministrazione giudiziaria", "solidarietà, mutualismo"]
tags: ["administration, administratif", "cuisiner, cuisine", "gare", "impossible", "pouvoir", "prix", "rire, se marrer, rigoler", "rue", "violence"]
langtxt: ["fr", "en", "it"]
zone: "Suisse romande"
num: "16"
quote: "vivere senza documenti è una vera lotta"
---

Mi chiamo Samba e vengo dal Gambia. Arrivare in Europa è stata
un'impresa. Per strada ho incontrato molte difficoltà attraversando i
paesi africani. Non è sempre stato facile stare con tutta quella gente
che non conoscevo, vivere una vita che non avevo vissuto prima. Il paese
più duro dal quale sono passato è stata la Libia. Laggiù nessuno vuole
darti da mangiare o aiutarti. Abbiamo cercato di trovare del cibo, un
lavoro per sopravvivere, un posto dove dormire. Puoi beccarti una
coltellata senza motivo. La tua vita è sempre in pericolo. Alla lunga
non credi nemmeno più che un giorno potrai andartene di là. Ogni giorno,
quando esci, non sai se tornerai. Ti svegli ogni mattina pensando: "Oggi
forse morirò, forse vivrò". Sono stato in prigione, mi hanno
maltrattato, ho vissuto cose davvero terribili, cose che non ti puoi
immaginare.

Anche arrivare in Europa è stata tutta un'impresa. Devi reimparare a
vivere in una società diversa, in una cultura diversa, con gente
diversa, superando la barriera della lingua. Devi imparare a muoverti in
questa nuova vita e affrontare molte cose. La mia vita in Europa è
iniziata in un campo in Italia. Ti ritrovi a vivere in un campo con un
sacco di altra gente che non viene dal tuo stesso paese e non parla la
tua lingua. C'è un problema dietro l'altro. Le persone del campo hanno
difficoltà di ogni genere, alcune hanno gravi problemi psicologici. Non
hai scelta, devi condividere il luogo in cui vivi. Una volta uscito dal
campo ci sono altre difficoltà da superare. Non sei più in un posto
controllato con gente che si occupa del cibo, sei completamente solo. Se
non hai i documenti, ma anche se li hai, come fai a trovare un lavoro se
non parli la lingua? Come fai se non conosci nessuno? Certi giorni è
davvero dura. Senza documenti non puoi avere una casa, pagare l'affitto.
Per me l'uscita dal campo è stato l'inizio di molte difficoltà. Non
conoscevo nessuno, sono arrivato in Svizzera e ho fatto richiesta
d'asilo. Ma la richiesta è stata respinta. Il motivo non lo so ma credo
fosse perché nel mio paese non c'è violenza, non c'è guerra. La Svizzera
mi ha detto di tornare in Italia, dove mi avevano preso le impronte
digitali.[^1] Allora ho pensato: e ora cosa faccio della mia vita? Devi
lottare per rimanere umano. Se non dimentichi almeno un po' di quello
che hai vissuto, impazzirai. Succede a tante persone di impazzire così,
per strada.

Ho incontrato il collettivo[^2] attraverso diversi eventi. Sono andato
nel luogo che occupavano per cucinare insieme a loro e qualcuno mi ha
proposto di unirmi alle conversazioni in francese. Quella persona mi ha
anche chiesto se ero motivato a venire tutti i mercoledì, ci sarebbe
stato da mangiare e altra gente. È un posto fantastico: non vado mai al
ristorante o al bar perché non ho soldi. Siccome si funziona con le
offerte di sottoscrizione e non ho niente, non pago niente. E poi mi
posso riposare. È qui che ho sentito per la prima volta che facevo parte
della società, ho iniziato a organizzare eventi, a ridere. Ho capito che
imparare il francese era una buona cosa per il mio futuro. Sto facendo
progressi. *Je parle maintenant un peu français*. Insomma venivo qui
sempre più spesso, finché un giorno mi hanno chiesto se volevo entrare
proprio a far parte del collettivo. Ho accettato con piacere ma mi ci è
voluto del tempo per creare la connessione. Per via della barriera
linguistica ma anche per capire il sistema, come funziona. Ci vuole
tempo... Da un punto di vista politico e amministrativo è davvero dura
se non hai il permesso di vivere nel paese. A volte non te la senti di
andare da qualche parte perché ci sono i controlli e rischi di finire in
prigione. Non avere i documenti cambia ogni cosa. Ostacola molte azioni.
E poi c'è il razzismo onnipresente, lo affronti per strada, vieni
costantemente aggredito e non puoi mai difenderti. Ti tolgono il diritto
di difenderti. Se arriva la polizia, anche se hai ragione, finirai in
prigione. Qualunque sia la situazione, i poliziotti vedranno una cosa
sola: tu non hai il diritto di essere qui, anzi non hai proprio nessun
diritto. Allora lasci perdere, non hai armi per difenderti. La cosa ha
un impatto sulla mia vita in molti modi diversi, ma far parte di un
collettivo organizzato è un'esperienza nuova, bella, mi dà più
opportunità, più contatti. Quando ci si organizza insieme si può
partecipare a una manifestazione e questo ci dà visibilità, in quanto
persone nere e in quanto persone a cui sono negati il diritto di vivere
in questo paese e i mezzi per difendersi. L'unica cosa che possiamo fare
è manifestare per fare in modo che la gente senta queste cose dalla
nostra bocca. Sensibilizzare le persone è importante, anche se ci vuole
tempo. Poco a poco, il cambiamento arriva.

Vivere senza documenti è una vera lotta e ormai sono passati cinque anni
e mezzo. Ovunque tu vada e qualunque cosa tu faccia, devi riflettere
bene: come faccio per arrivare dal punto A al punto B? Da dove passo?
Anche per andare al parco devi fare attenzione. Forse le guardie sono lì
ad aspettarti per arrestarti. A volte fai il calcolo se vale la pena di
andarci: non sei libero. Ho amici e amiche che hanno i documenti e
quando ci diamo appuntamento sono io che dico dove ci troviamo. Perché
loro non hanno bisogno di riflettere a queste cose, la loro vita in
città non è come la mia.

Ogni volta che trovo un lavoro, dopo una giornata mi dicono che non
posso restare, che non posso più lavorare lì. Dicono che lavoro bene ma
che senza documenti non si può fare e mi rimandano a casa. La mia vita
dipende sempre dalle altre persone, che mi piaccia o no. Senza le altre
persone dormirei per strade e sarei costretto a fare cose illegali,
perché non avrei scelta. La cosa mi mette a disagio, non voglio chiedere
soldi direttamente alle persone. Il mio obiettivo è vivere, trovare un
lavoro, avere un futuro. Sono felice di far parte del collettivo, mi ha
cambiato la vita, è ancora difficile ma almeno non dormo all'aria
aperta. Quindi anche se è dura ormai non potrei immaginarmi vivere in un
altro paese che la Svizzera. È un posto che conosco, il posto dove
voglio stare e sarei terrorizzato all'idea di dover tornare a vivere per
strada.

Anche essere neri è una lotta. Devi continuamente affrontare la
profilazione razziale, anche se ci sono delle leggi contro il razzismo.
Puoi dire a una persona che quello che sta dicendo è razzista. Puoi
usare le parole, non fosse che per fare in modo che la gente si renda
conto che sta dicendo qualcosa di razzista, anche se non puoi cambiarla.
Ma quando non hai i documenti, è come se non avessi le armi per
difenderti. Qualche giorno fa ero in stazione e un tizio mi ha colpito
al braccio in una maniera che non andava affatto bene. So benissimo che
non lo avrebbe fatto se avessi avuto la pelle bianca. Avrei potuto stare
zitto, ma invece gli ho detto: "Perché mi ha fatto questo? Non la
conosco". Gli ho spiegato come mi sentivo e per finire il tipo si è
scusato. Ma prima di pensare a scusarti dovresti pensare a quello che
stai facendo e non farlo. Se avessi avuto i documenti avrei potuto avere
una conversazione vera con lui, una conversazione più lunga. Non volevo
problemi, anche se non ero stato io a creare il problema. Ma
naturalmente la gente intorno, prima ancora di capire cosa stesse
succedendo, avrebbe immediatamente chiamato la polizia. E anche se ero
la vittima in questa storia, sarei stato io a pagare perché non ho
diritti. E quindi l'ho semplicemente lasciato stare. Quello che quel
tizio ha fatto non era giusto e per finire si è scusato, ma non mi ha
dato soddisfazione. Questa storia è un aneddoto che riassume la
situazione: io lotto per i miei diritti ma non ho le armi per farlo per
via della mia situazione illegale.

[^1]: Riferimento al cosiddetto Regolamento di Dublino°.

[^2]: Un collettivo autogestito che lotta per l'accoglienza delle persone senza documenti
