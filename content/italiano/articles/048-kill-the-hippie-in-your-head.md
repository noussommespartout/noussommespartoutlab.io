---
author: "Anonim*"
title: "Kill the Hippie in Your Head"
subtitle: "Come ho abbandonato il pacifismo"
info: "Testo scritto per la raccolta svizzera romanda"
datepub: "Settembre 2020"
categories: ["manifestazione, rivolta, protesta", "autoriflessione sulla militanza"]
tags: ["adrénaline", "colère", "dissonance", "feu", "gueule", "horde", "injustice", "merde", "peur", "pouvoir", "rage", "rire, se marrer, rigoler", "soleil", "théorie", "violence", "visage", "voix"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "48"
quote: "con passamontagna e molotov"
---

{{%epigraphe%}}
Hippie: Giovane che aderisce, negli Stati Uniti e poi in Europa
Occidentale, a un'etica fondata sul rifiuto della società dei consumi,
espressa attraverso la non-violenza e uno stile di vita non
convenzionale. L\* hippie non sono politicizzat\*.
{{%/epigraphe%}}

{{%ligneblanche%}}

Eravamo a una manifestazione contro la monsanto. C'erano un bel po' di
radical chic e di fricchetton\*. Bambin\*, vecch\*. Un po' di tutto. Un
mix sociale, ma di sicuro niente a che fare con passamontagna e molotov.
Ho voglia di raccontare questa storia, anche se nel frattempo la mia
militanza è cambiata sia nella forma che nei contenuti, perché credo che
quella sia stata una delle prima volte in cui ho scoperto il potenziale
dell'insurrezione. Quel giorno ho scoperto che era possibile protestare
contro le decisioni catastrofiche dell'economia e di chi sta al potere.
Senza la minima garanzia di successo, certo, forse addirittura sapendo
che la cosa non porterà a nessun risultato. Ma ho voglia di raccontare
questa storia anche perché, tra chi si troverà questo libro tra le mani,
penso ci sarà qualcun\* a cui potrà tornare utile...

Insomma, ero a questa manifestazione contro la monsanto e i mezzi messi
in campo per tenere l\* manifestanti lontan\* dall'edificio erano
impressionanti, specialmente considerando che ci trovavamo in una città
relativamente piccola, con una popolazione molle e borghese, blandamente
di sinistra, il tipo di città che può accogliere la monsanto senza
grossi problemi di dissonanza cognitiva.

I miei unici punti di riferimento erano i film americani, per cui tutto
quell'apparato difensivo non mi scioccava più di tanto. Invece la gente
che avevo attorno, con più esperienza di me, era sorpresa dall'imponenza
di quel dispiegamento di polizia. Un organo che, in teoria, avrebbe
dovuto difendere gli interessi pubblici era stato mobilitato al completo
--- le guardie arrivavano da tutti i comuni circostanti --- per difendere
un'impresa privata.

Con le loro armi e i loro caschi, le milizie erano appostate all'ombra
dell'ingresso dell'edificio. Sono uscite fuori solo quando abbiamo
superato le transenne traballanti. Vedevo altre persone, più in là, che
lanciavano uova e altra roba contro le finestre dalle persiane chiuse.
Mi sarebbe piaciuto essere con loro, credo. La sensazione aveva qualcosa
di inebriante. Non lo avevo fatto molte altre volte. Ricordo che stavo
sudando tantissimo, rivoli di sudore mi colavano lungo la schiena e
sotto le braccia. Col senno di poi, quel giorno ho corso dei rischi un
po' stupidi. Non avevo il volto coperto ed ero sola.

Provavo un senso di rabbia, ma una rabbia vaga. Quelle specie di robocop
che avevo di fronte mi affascinavano e mi infastidivano al tempo stesso.
Mi pareva davvero ingiusto che fossero lì a fare da muro tra noi e
quell'azienda di merda. Uno di loro mi sembrava giovanissimo sotto la
visiera e mi è venuta voglia di guardarlo più da vicino, più che altro
per vedere fino a che punto sarei riuscita ad avvicinarmi alla sua
faccia. Era una provocazione, ovvio, ma era anche curiosità: volevo
capire se quella zona fosse davvero irraggiungibile come sembrava.
Chiaramente non lo spaventavo un granché perché non ha quasi battuto
ciglio quando mi sono piazzata davanti a lui. Ci sono rimasta un po'
male, credo. E ho deciso di rimanergli piantata davanti al muso per
qualche ora.

È stata lunga.

Dopo un po' immagino che lo sbirro si sia comunque sentito un po'
provocato da quella ragazzetta piazzata a cinque centimetri dal suo
giubbotto antiproiettile e dal suo fucile d'assalto. Era come giocare a
chi sbatte le palpebre per prim\*. Lui ha tentato di farmi l'occhiolino,
io ho fatto una smorfia. Dopo un paio d'ore il suo comandante è venuto a
proporgli di cambiare posto. Lui ha declinato, commentando che non aveva
problemi a restare davanti a una "ragazza così carina". Ho iniziato a
dubitare seriamente del senso della mia iniziativa. Chiaramente quello
sbirro stava gradendo la cosa, e l'obiettivo non era quello. OK,
immagino che avrebbe comunque preferito trovarsi altrove, tanto più che
c'erano dell\* fotograf\* a riprendere la scena. Avrei dovuto sputargli
in faccia, di sicuro avrebbe sortito qualche effetto. Ero abbastanza
vicina per centrarlo. Da un lato ero disgustata, dall'altro stavo
iniziando a rompermi seriamente le palle. Continuavo a sudare ma, a quel
punto, più per il sole che per l'adrenalina. Avrei dovuto almeno
ridergli in faccia, alzare il dito medio, dirgli che era una merda. Non
l'ho fatto. Sono rimasta piantata lì, come una specie di inutile
idealista.

In un certo senso ero ancora impregnata di un immaginario pacifista
della rivoluzione. Quell'ideale un po' hippie in cui ti vedi infilare
fiori nelle bocche dei cannoni o costringere un carro armato a deviare
sbarrandogli la strada, stoica e orgogliosa. Ma la verità è che i carri
armati ti schiacciano. Sono tutte immagini bellissime, per carità, ma mi
pare di aver capito che le cose non funzionano così. Non è mica tanto
semplice creare davvero problemi all'autorità, farla vacillare con gesti
semplici e simbolici. Sono immagini forti che compongono un discorso
accettabile, commovente, mediatizzabile, ma la verità è che quasi nessun
cambiamento sociale è nato dalla non-violenza, da quella specie di
blando consenso provocato dal pubblico sbandieramento di atti eroici di
fronte a un governo conciliante. Sono spesso le voci sorde e soffocate
delle lotte sotterranee, anche armate, a far tremare le fondamenta, a
volte facendo a pezzi il lastricato in modo che la folla li prenda in
mano e li lanci in faccia al potere in divisa. Quello che la gente
privilegiata come me percepisce delle lotte è spesso nient'altro che la
punta dell'iceberg. E così, il più delle volte per ignoranza, ci
stupiamo o addirittura ci offendiamo per la reazione dell\* oppress\*.
Una reazione che ci sembra esagerata, troppo radicale, troppo violenta.

L'altro aspetto da capire nella questione della spettacolarizzazione
delle rivolte è che fin troppo spesso le lotte acquistano visibilità
attraverso persone che con esse hanno ben poco a che fare.

Mi spiego. All'epoca, l'episodio che ho appena raccontato aveva ispirato
una foto, che è recentemente rispuntata fuori per difendere non so più
quale causa, dimenticando che era stato il mio status di privilegiata a
permettermi di assumere quella posizione, di rimanere piantata per ore
davanti al robocop. È un problema ricorrente di queste immagini un po'
sensazionalistiche. Da un lato, individualizzano le lotte collettive.
Dall'altro, hanno la tendenza a rendere visibili sempre le stesse lotte
e l\* stess\* militanti bianch\*, cis°, non disabili, radical chic, con
i documenti in regola e sempre pront\* a far sentire la propria voce.
Non mostrano le persone razzializzate, trans, non binarie, disabili,
povere, senza documenti --- proprio quelle che stanno difendendo i loro
diritti e che dovrebbero, appunto, essere visibili.

Ma all'epoca non sapevo che, se quel deficiente di uno sbirro non mi
aveva spaccato la faccia, era solo perché ero una giovane bianca piena
di privilegi che difendeva una causa politicamente accettabile e facile
da neutralizzare. Non sapevo ancora che quelli come lui ammazzano, che
le persone muoiono sotto i loro colpi e che comunque loro non vengono
mai punit\*. Non sapevo fino a che punto fosse vera la sigla ACAB°. Non
mi rendevo conto che questo mio mettermi sotto i riflettori potesse
essere tanto problematico.

Ma è andata così. Sono cose che si imparano facendo comunità, ascoltando
i racconti, osservando le immagini delle violenze della polizia, sempre
più frequenti, che ti riempiono lo stomaco di rabbia. Già, tutto passa
davvero dall'ascolto. Basta rimanere zitt\* due minuti e ascoltare.
Pensare a come rendersi utile. A volte la cosa migliore che tu possa
fare è restare indietro, preparare da mangiare, lasciare il posto,
filmare la polizia, aspettare l'uscita delle persone davanti a un
commissariato, organizzare eventi di supporto. Insomma, dobbiamo usare i
nostri privilegi per metterli a disposizione di chi ne ha bisogno.
Bastano due minuti. Noi privilegiat\* dobbiamo piantarla di voler stare
sempre in primo piano e raccontarci a vicenda il mito spettacolare della
nostra lotta. Sono tutte stronzate. Dobbiamo mettere a tacere quel lato
di noi che è stato nutrito con il biberon di una visione riformista,
universalista e non-violenta completamente scollegata dalla realtà
sociale. Quando ti rendi conto della scala dell'ingiustizia, quando
incontri chi si confronta con la violenza verticale giorno dopo giorno
per ciò che è o ciò che fa, capisci meglio che non è solo "*about us*".
Almeno lo spero. Dobbiamo lasciar morire, a fuoco lento, l'hippie nella
nostra testa.

E la prossima volta, forse, la situazione sarà meno oppressiva, meno
problematica, meno del cazzo.

La prossima volta, torneremo.

Con i passamontagna.

Nelle retrovie.

Forse con dei pezzi di lastricato.

Sicuramente con i nostri sorelli e le nostre fratelle.

E ci andremo in massa, con i nostri immaginari tutti diversi e i nostri
privilegi asimmetrici, ma ci proveremo.

Saremo una legione, innumerevoli e senza volto.

Soprattutto, ci proveremo.
