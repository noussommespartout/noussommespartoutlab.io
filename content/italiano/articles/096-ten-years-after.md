---
author: "Lorenzo Mari"
title: "Ten Years After (and beyond)"
subtitle: "Bologna 2013 --- 2023"
info: "La prima poesia è stata scritta nel 2013, la seconda per la raccolta bolognese"
datepub: "Aprile 2023"
categories: ["autoriflessione sulla militanza", "manifestazione, rivolta, protesta"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "96"
quote: "Pesa molto la luce"
---

## San Luca (ancora)

Pesa molto la luce, sul quarto

di scalinata. Turisti

fossimo stati, e non in viaggio,

avremmo fatto i gradini

quattro a quattro, fante e cole in mano ---

invece è sul ventesimo scalino

che la luce sembra possa gravare

anche totale e che perda senso

l'un due tre e anche il militare ---

il rischio è che una buona metà

del portico finisca scoperchiato,

che al muro, con il bersaglio rosso

in fronte, non ci si possa nemmeno

riposare.

                    *2013*

## San Luca (oltre)

Bersaglio rosso all'anca: il colpo

è duro, Andrea, più degli altri. Oggi poi c'è salita lunga,

senza gambe, e di nuovo non importa se di passaggio

o meno perché si vedono i gradini dall'altra parte, e poi sassi

rocce gessi e alberi: si va avanti, per capire cosa si squarcia ---

ancora avanti, riscendendo risalendo: Gessaroli

Casaglia Cavaioni Sabbiuno Lama di Setta

Ganzole Santa Barbara Marzabotto

Sperticano fino alla Bocca del Diavolo

e ancora oltre ---

dimenticate le seicentosessantasei spire di serpente

ancora nessun riposo Raffa chiusi i centri

sociali impossibili gli affitti svuotate con l'uso della forza

le case ritorna l'un due tre e anche il militare e nel movimento

c'è chi prende la via dei monti però meglio

di quanti l'avrebbero fatto per altre ragioni, dicevano

ma non hanno mai osato, in fin dei conti --- il punto rosso

Andrea lo si porta addosso lo si porta senza gambe e bianco di luna

c'è chi guarda il gesso Raffa per tornare a tracciare linee a scrivere

di notte --- cancellando il bersaglio, riscrivendo gli spazi.

*Qui è oltre ogni posto, qui è contro ogni posto*,

dice Andrea, dice Raffa, *oltre e contro e ancora*

*adesso*.

                    2023

{{%asterisme%}}

Il santuario di San Luca, sul Colle della Guardia, è una delle immagini
cosiddette "iconiche" della città di Bologna; tuttavia, al di là della
tradizione religiosa e politica che continua a rappresentare, San Luca
designa anche uno dei limiti topografici della città. Può servire,
quindi, come spunto per riflettere su quello che è successo dentro e
fuori dai confini cittadini nei dieci anni che sono passati tra i due
testi. Alcuni tra i fenomeni citati si sono non soltanto riprodotti ma
anche espansi, come ad esempio la difficoltà di accesso agli affitti per
chi studia, lavora, vive o transita in città, oppure il progressivo
sgombero degli spazi e dei centri sociali occupati. In questa
prospettiva, resta però la contrapposizione tra le forme di *militanza*
e la *militarizzazione* (in senso ristretto, ma anche in senso lato)
degli spazi cittadini --- una contrapposizione che viene ad essere agita
positivamente quando non si traduce in uno sterile esercizio di retorica
post-resistenziale (che si è rivelato appropriabile, e appropriata, da
più parti, in conflitto con chi continua a rivendicarne più
coerentemente le eredità) ma si rivela capace di generare nuove lotte e
nuove geografie.
