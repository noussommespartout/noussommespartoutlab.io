---
author: "Anonim*"
title: "Reimparare a organizzarsi"
subtitle: "Strumenti per una comunicazione e un'organizzazione interna
orizzontali"
info: "Testo scritto per la raccolta svizzera romanda"
datepub: "Ottobre 2021"
categories: ["autogestione, autorganizzazione", "DIY, tutorial"]
tags: ["administration, administratif", "amour, love, amoureuxse", "confiance", "corps", "horizontal, horizontalité", "outil", "parole", "pouvoir", "réunion", "silence", "théâtre", "voix"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "21"
quote: "il silenzio è benvenuto"
---

Le modalità di comunicazione e organizzazione che adottiamo
spontaneamente sono determinate dalla società che ci ha costruit\* e
nella quale viviamo. Riproduciamo d'istinto i codici e i riflessi di una
società organizzata in maniera gerarchica che valorizza la
specializzazione e all'interno della quale determinate voci assumono per
naturalità più importanza di altre.

Quando vogliamo creare alternative e militare insieme secondo modalità
orizzontali, dobbiamo reinventarci e imparare nuove forme di ascolto e
di condivisione. Nell'autogestione° anarchica o autonoma, l'obiettivo
non è di creare degli spazi senza regole o inquadramento ma di costruire
e organizzare delle alternative che permettano una ripartizione dei
poteri e dei compiti ingrati e valorizzanti. Si tratta di inventare
degli strumenti che non siano imposti da padron\*, cap\*, apparati
statali o polizieschi ma creati dal basso, da tutt\*: strumenti
amovibili e dinamici anziché leggi scolpite nella pietra.

Di libri che forniscono strumenti teorici per affrontare tali questioni
ne esistono senz'altro, ma non cercherò di riassumerli qui. Nei
collettivi di cui ho fatto parte ci siamo format\* senza libri --- forse
più lentamente di quanto avremmo fatto se avessimo applicato saperi
precostituiti e riconosciuti. Procedevamo a tentoni, creando gli
strumenti appropriati a seconda delle situazioni, delle particolarità e
degli obiettivi.

Ecco una lista di strumenti che ho utilizzato e partecipato a
implementare in vari contesti per creare modalità di organizzazione e
comunicazione più orizzontali. Come comunicare quando non parliamo la
stessa lingua e non abbiamo lo stesso bagaglio educativo ed
esperienziale?

## Il silenzio

### Perché?

In una società in cui parlare forte e tenere banco sono costrutti
sociali valorizzati, instaurare e accogliere il silenzio può avere un
forte impatto: aiuta chi non è in grado di accaparrarsi la parola e
permette di prendersi il tempo di riflettere e ascoltare. Il silenzio
può aiutare a ristabilire un senso di orizzontalità.

C'è voluto del tempo per accogliere il silenzio nelle nostre
interazioni. L'altra sera qualcuno ha commentato, durante una riunione,
che "se valutiamo la qualità dei nostri legami in base alla qualità dei
nostri silenzi, va tutto bene". E in effetti durante quella riunione i
silenzi erano proprio di qualità. Si tratta di veri propri momenti di
condivisione, durante i quali si assimila quello che è stato detto.

### Come accogliere il silenzio?

- All'inizio di una discussione o di una riunione, basta annunciare
semplicemente che il silenzio è benvenuto e che possiamo cercare
collettivamente di lasciargli un vero spazio nelle interazioni. Dirlo
permette di esorcizzare buona parte dell'imbarazzo che può nascere dai
momenti di silenzio. Anzi, ben presto non c'è nemmeno più bisogno di
dirlo perché il silenzio diventa un partecipante a pieno diritto.
- Stabilire dei turni di parola con un tempo delimitato: cinque minuti a
persona, ad esempio. Se la persona sfora il tempo di parola, viene
interrotta. Se smette di parlare dopo tre minuti, si lascia che il tempo
restante trascorra nel silenzio. La traduzione, quando necessaria, non
deve essere fatta durante quei due minuti di silenzio. Meglio aspettare
che scada il tempo.
- Per assegnare la parola, invece di fare un giro di tavolo in cui tutt\*
parlano a turno, si possono usare le formule "prendo" e "lascio".
"Prendo" quando voglio parlare e "lascio" quando ho finito. Questo
strumento permette di lasciare spazio al silenzio se si annuncia, fin
dall'inizio, che è possibile mantenere il tempo di parola senza dire
niente. Inoltre, siccome l'ordine non è prestabilito, quando una persona
lascia la parola non ci si sente obbligat\* a parlare subito.

## Scrittura

### Perché?

Se non siamo tutt\* uguali di fronte alla scrittura, lo siamo ancora
meno di fronte alla parola. Non si tratta di scrivere testi lunghi ma di
lasciare spazi che permettano a ciascun\* di prendersi il proprio tempo
per mettere su carta un'idea o un desiderio senza lo stress di dover
parlare in fretta, di rispondere immediatamente a ciò che è stato detto
o di parlare in pubblico.

### Come?

- **La "*gridata*"** è una cassetta dove si possono imbucare idee,
condividere parole d'amore o delusioni, desideri o sogni. Può trattarsi
di un oggetto fisico che rimane in un luogo specifico durante le
giornate o le settimane di discussione e di organizzazione collettive.
Si stabiliscono poi dei momenti di condivisione in cui si fa girare la
cassetta, estraendo un biglietto alla volta e leggendolo ad alta voce.
In questo modo, tutte le voci potranno essere ascoltate. In alcuni casi
si tratterà di abbordare insieme una situazione specifica, in altri di
reagire, e in altri ancora semplicemente di ascoltare.
- La "*gridata*" può servire a gestire una condivisione emotiva
collettiva, ma anche a stabilire un ordine del giorno partecipato o a
organizzare le idee e le tematiche da discutere durante la riunione. A
volte è più semplice avere un tempo di riflessione individuale piuttosto
che dover esprimersi davanti a tutt\*.
- Attenzione: secondo me la *gridata* non deve mai servire da tribuna per
denunce pubbliche. In caso di conflitti interni, è preferibile
affrontarli con altri mezzi. Bisogna evitare di denunciare pubblicamente
una persona in particolare durante una *gridata* anonima. L'impatto
emotivo rischia di essere troppo forte. La *gridata* può essere usata
per denunciare delle insufficienze collettive, ma non individuali.
- **Il "*bottinaggio*"** consiste nel disporre dei grandi fogli sui
tavoli. Dopo un brainstorming (scritto o orale), su ciascun foglio viene
scritto uno dei temi da trattare. Dopodiché ci si sposta con una penna
da un tavolo all'altro, da un foglio all'altro e si annotano parole,
frasi, discutendo sul foglio, continuando a girare e lasciando che il
processo si svolga naturalmente. Il risultato dell'operazione saranno
dei fogli su cui ognun\* avrà avuto modo di contribuire al tema,
maturando ogni parola in tutta tranquillità.

## Gruppi ristretti

### Perché?

Parlare in pubblico è più semplice per alcun\* che per altr\*. Non è
facile esprimersi davanti a 10, 15 o 20 persone. Le plenarie
tradizionali sono una mecca della presa del potere dove chi si esprime
con più facilità o dispone di più informazioni tiene banco e impone un
rapporto di dominanza. Prendere decisioni in riunione plenaria è
particolarmente complicato perché il dibattito si polarizza spesso
intorno a poche persone, creando ineguaglianze. Dividersi in piccoli
gruppi di discussione permette di ridurre queste prese di potere.

### Come?

- È semplice: ci si divide in piccoli gruppi per discutere uno o più
argomenti prima di tornare in plenaria. Quando ci si ritrova tutt\*
insieme, una persona per gruppo può riassumere ciò che è stato detto per
condividerlo.\
Quando si tratta di argomenti delicati, non si è costrett\* a dire tutto
durante la condivisione in plenaria. Nei piccoli gruppi ci si confida
con più facilità, ci si permette di imboccare direzioni diverse per poi
tornare indietro, ecc.
- A gruppi di 3: una persona parla di un argomento per una durata
predefinita; la seconda ascolta attivamente, ponendo domande o
rilanciando il dibattito se necessario, ma senza formulare commenti o
giudizi; la terza persona prende nota di ciò che viene detto. Una volta
scaduto il tempo, ci si scambia i ruoli. Quando hanno parlato tutt\*, ci
si prende un momento per fare la sintesi e capire i punti di accordo e
di divergenza.
- Questo metodo è interessante per affrontare questioni di fondo o molto
aperte. Permette di approfondire e di creare legami tra le diverse
esperienze. Dà inoltre la possibilità di esprimersi in un clima di
fiducia, con poche persone, e avere un momento in cui ognun\* si sente
davvero ascoltat\* e considerat\*.

## Il corpo e lo spazio

### Perché?

Il teatro-immagine è uno strumento di educazione popolare che abbiamo
testato in più occasioni nei nostri collettivi e che ha permesso, ogni
volta, di lasciar emergere emozioni e tematiche profonde e sincere. Con
l'aiuto dei nostri corpi e di diversi oggetti, siamo in grado di
esprimere cose che non diremmo necessariamente a parole. Possiamo anche
darci il tempo di ripensare la nostra relazione al gruppo. Questo
strumento può essere utilizzato per abbordare diversi argomenti, ma
quello della relazione al gruppo funziona particolarmente bene.

### Come?

La scenografia viene allestita con i materiali a disposizione. Spesso si
usano sedie o divani per indicare le diverse posizioni nello spazio.
L'allestimento è in funzione dell'argomento sollevato e che si desidera
affrontare. Se si vuole parlare del rapporto tra l'individuo e il
gruppo, è interessante giocare sul concetto di "centralità" posizionando
alcuni elementi al centro e altri più in periferia, in disparte. Si può
anche giocare con la verticalità, con le altezze, posizionando ad
esempio una sedia su un tavolo *e* al centro dello spazio e altre sedie
basse a terra.

Le sedie possono poi essere orientate in direzioni diverse: girate verso
il centro o, al contrario, verso la periferia. Si può anche sbilanciare
una sedia posizionandola su una superficie irregolare o creare diversi
tipi di scenografia in funzione dell'argomento che si desidera
abbordare.

Quando la scenografia è pronta, si pongono tre domande al gruppo e
ognun\* può posizionarsi come desidera a seconda del suo atteggiamento.

- Prima domanda: dove ti situi all'interno del gruppo?
- Seconda domanda: dove pensi che gli altri ti situino all'interno del
gruppo?
- Terza domanda: dove vorresti essere?

Dopo ogni domanda, le persone circolano nello spazio e vanno a
posizionarsi in silenzio. Alla fine dell'esercizio, tutt\* escono dalla
scenografia. Durante il momento di condivisione, ognun\* ripercorre i
propri spostamenti spiegando all\* altr\* le proprie motivazioni.

- Questo strumento permette di mostrare degli aspetti che rimangono
spesso invisibili e può condurre a momenti emotivamente molto forti. Può
essere pertinente in diversi contesti ma è particolarmente interessante
applicarlo all'interno di un gruppo già formato da tempo.

## Carico mentale e compiti ingrati

È risaputo: può bastare un incidente di piatti non lavati, un
frigorifero putrido o un bagno sporco per far saltare un collettivo.
Oltre i compiti ingrati e spesso invisibili, come fare per spartirsi il
carico mentale, spesso considerevole all'interno dei collettivi?
L'obiettivo è di evitare le "specializzazioni" e di dare valore e
visibilità a ogni compito per evitare frustrazioni e rancori.

Ma come si fa concretamente? Senza contare che esistono visioni
contrastanti: quella del "non voglio nessuna regola e costrizione" e
quella del "ho bisogno di regole e di spartire i compiti per evitare i
sensi di colpa o il sentimento di fare troppo".

Nessuna delle modalità di organizzazione sviluppate ha mai raggiunto un
consenso unanime, ma una di queste mi è sembrata particolarmente
pertinente, interessante e, almeno nel mio caso, soddisfacente.

Come dare visibilità a tutti i compiti e prendere coscienza della loro
spartizione all'interno del collettivo?

- Distribuire tanti post-it su cui ciascun\* annota tutti i compiti
svolti all'interno del collettivo: pulizia del frigorifero, del
gabinetto, gestione del bar, acquisti, manutenzione, accoglienza,
risposta alle mail, newsletter, contabilità, ecc. Ogni compito viene
annotato, dal più piccolo al più impegnativo, dal meno valorizzante al
più prestigioso. In seguito tutti questi post-it vengono disposti su un
grande tavolo. Possono essere suddivisi in "poli": amministrazione,
manutenzione, comunicazione con l'esterno, ecc.

- I post-it riuniti danno una visione d'insieme delle attività interne
del collettivo, valorizzando ciascuna di esse. Inoltre, ci si rende
presto conto che alcuni compiti sono svolti da più persone mentre altri
riposano su una persona sola. L'idea non è di fare un confronto tra la
mole di lavoro di ognun\*. Per evitare questa dinamica, i post-it
possono essere anonimi. Si tratta semplicemente di prendere coscienza di
tutto ciò che viene fatto, perché è una cosa valorizzante, e della
ripartizione del lavoro all'interno del collettivo.

- L'esercizio può essere sviluppato tirando a sorte le persone incaricate
di gestire uno dei "poli" per un dato periodo, ad esempio un trimestre.
Se nessuna delle persone sorteggiate ha le conoscenze o le competenze
necessarie per la gestione del polo, si ripete il sorteggio. Il gruppo
così formato avrà allora la responsabilità, il carico mentale e una
parte dei compiti del polo in questione. Naturalmente si può e si deve
anche impegnarsi e agire anche al di fuori dal polo assegnato e non si è
obbligati a svolgere tutti i compiti associati al proprio polo, ma la
missione è di conoscere al meglio ciò che è già stato fatto e cosa
rimane da fare. Ogni gruppo si organizza come crede. Poi, a ogni
trimestre, si cambia.

- Questa tecnica permette di spartirsi il carico mentale e di
organizzarsi meglio. Inoltre grazie ai turni trimestrali, si imparano
cose nuove e si prende coscienza di ciò che viene fatto da ognun\*,
valorizzando tutti i ruoli ed evitando le frustrazioni del lavoro
invisibile. Grazie al sorteggio, ci si organizza insieme a persone con
le quali non ci sarebbe necessariamente venuto in mente di collaborare.

Sta a noi inventare, adattare, pescare nuove idee e cercare soluzioni al
maggior numero possibile di persone e sensibilità. La voglia di
continuare a militare viene anche da questo: dalla configurazione di
nuove forme di socializzazione, ascolto e organizzazione.

