---
author: "Asia Usb (Associazione inquilini e abitanti — Unione sindacale di base)"
title: "Un muro popolare in Bolognina"
subtitle: ""
info: "Testo scritto per la raccolta bolognese"
datepub: "Maggio 2022"
categories: ["spazi, occupazione, diritto all'abitare"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "69"
quote: "io, misero muro composto da mattoni"
---

Presi forma una mattina del 1957, quando operai e manovali mi diedero
vita mattone su mattone. Ero in gran compagnia, con tanti altri vicini,
e pronto a svolgere la funzione per cui venni costruito: dare casa a chi
non ce l'ha.  Non ero un altezzoso villino che da qui vedo sorgere sui
colli, ma ci chiamavano IACP[^1].  Eravamo le nuove case popolari, la
conquista delle lavoratrici e dei lavoratori per garantire alle proprie
famiglie una casa degna in cui vivere e crescere. 


Finiti i lavori, attesi qualche mese finché finalmente venne assegnato
l'appartamento a una famiglia. Ero felice con tanti bambini che
correvano in giro per casa e nella corte, mentre i genitori erano
indaffarati nelle loro faccende. La sera intorno al tavolo il padre
parlava di scioperi, diritti, battaglie che la classe operaia doveva
portare avanti, che la casa in cui vivevano era il risultato soltanto
alle dure lotte dei lavoratori. 

Io, misero muro composto da mattoni, mi sentivo sempre più fiero di
quello che rappresentavo.  Segnato dalle tacche dei bambini che
crescevano, i piccoli erano ormai diventati grandi, i genitori si
avvicinavano sempre più verso la pensione.  Il quartiere stava
cambiando, sempre più fabbriche chiudevano e tra un muro e l'altro ci si
raccontava di quello che succedeva nelle vite dei nostri inquilini.

Arrivò anche per me, come per molti altri, il fatidico giorno in cui
rimasi senza compagnia. Gli anni erano passati, i ragazzi ormai grandi
si erano trasferiti e la vecchiaia aveva fatto il resto.

Rimasi solo per poco, però, quando accolsi una nuova famiglia.
All'inizio non capivo, parlavano una lingua a me sconosciuta, non
cucinavano i tortellini ma piatti speziati con odori mai sentiti.  Piano
piano iniziai a capire, a capire che venivano da lontano, imparai tanto
delle loro tradizioni e cultura, ma durò poco.

 La sera intorno al tavolo non parlavano di scioperi, diritti e
battaglie ma, a bassa voce per non farsi sentire dai bambini, di
licenziamenti, bollette e debiti. Settimana dopo settimana e mese dopo
mese respiravo un'aria sempre più preoccupata, dicevano che non ci
sarebbe stata soluzione e nessun posto dove andare. Non capivo quello
che stava accadendo, non sapevo spiegarmi perché questa famiglia, alla
quale ormai mi ero affezionato, non potesse restare a tenermi compagnia.

Un giorno dei signori arrivarono presto la mattina perché dovevano
«sfrattarli».

Questi signori, insieme a diversi agenti della polizia, erano entrati
urlando e con fare minaccioso nell'appartamento, dopo che avevano
forzato la porta di ingresso. I ricordi di quei momenti sono confusi,
ricordo soltanto molte urla, sedie e tavoli ribaltati, il pianto dei
bambini e valigie fatte in fretta e furia. 

La porta venne blindata e capii che per molti anni non avrei avuto
compagnia, non avrei fatto quello per cui ero stato creato decenni fa,
cioè sostenere un tetto per delle persone che non ce l'hanno.  Gli anni
passarono, iniziai ad avere sempre più muffa su di me, nessuno si curava
più di mantenermi, e anche gli altri muri mi raccontavano storie
tremende, chi come me abbandonato, o altri svenduti a poco prezzo.  

{{%ligneblanche%}}

Ormai mi ero messo nell'ottica di rimanere solo nella polvere e
sporcizia... Fino a quando una mattina presto, venni svegliato
all'improvviso da dei forti colpi sulla porta blindata che mi sigillava.

 In poco tempo la porta si aprì, ma la serratura questa volta venne
rotta, i nuovi arrivati le chiavi non le avevano.  Erano tanti, giovani
e meno giovani, insieme a una famiglia con dei bambini. Mi attaccarono
sopra dei volantini e delle bandiere. 

La chiamano «occupazione», dicono che se una casa è vuota e c'è chi è
rimasto senza casa, è giusto riaprirla.  Mi ricordai i discorsi della
prima famiglia qui dentro, delle battaglie, delle lotte e degli scioperi
che mi crearono, dello schifo che provai quando vidi i miei inquilini
sfrattati, degli anni rimasto abbandonato al tempo.

 Ma adesso tornavo a fare quello che mi riesce meglio.

 Ed in poco tempo tornai ad essere un muro di una casa popolare che
finalmente non era più vuota.

[^1]: Istituto autonomo per le case popolari, ndr.
