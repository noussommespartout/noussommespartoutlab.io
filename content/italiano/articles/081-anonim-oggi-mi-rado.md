---
author: "Anonim*"
title: "Oggi mi rado i capelli a zero"
subtitle: "Un'azione di piazza contro l'inerzia dei decisori"
info: "Testo riportato in vita per la raccolta bolognese"
datepub: "Maggio 2022"
categories: ["manifestazione, rivolta, protesta", "corpo, ambientale"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "81"
quote: "che si renda pubblico il tradimento delle istituzioni"
---

## Premessa

Bologna è attraversata da un'autostrada urbana inaugurata nel 1966: il
raccordo autostradale fra A1 e A14 è affiancato in complanare dalla
tangenziale. Attualmente l'infrastruttura ha 12 corsie complessive, dopo
l'ultimo allargamento completato nel 2008. Il decreto di «Valutazione di
impatto ambientale» dell'opera, che risaliva al 4 ottobre 2000,
prevedeva l'installazione di due centraline di rilevamento della qualità
dell'aria lungo il tracciato, mediante una convenzione fra Comune di
Bologna e Società Autostrade: quelle centraline non sono state mai
installate.

Ora è in fase di approvazione un progetto di ulteriore allargamento fino
a 18 corsie. Diversi comitati di residenti hanno ripetutamente chiesto
che le centraline siano finalmente attivate e che prima di pensare a
ulteriori allargamenti sia realizzata una «Valutazione di impatto
sanitario» sull'attuale infrastruttura, per conoscere l'impatto
dell'inquinamento sulla salute della popolazione.

L'azione a cui si riferisce questo testo fa parte di questa
rivendicazione, portata avanti fin dal 2016 con manifestazioni,
articoli, azioni legali, petizioni alle autorità italiane ed europee, ma
mai ascoltata dalle amministrazioni locali. Il discorso è stato
pronunciato da un'attivista che si è fatta radere a zero i capelli in
Piazza Maggiore, di fronte alla sede storica del Comune, per richiamare
l'attenzione sulle numerose vittime dell'inquinamento che sono costrette
a cicli terapeutici che provocano la perdita dei capelli.

## Bologna, Piazza maggiore, 26 luglio 2021

Oggi mi taglio i capelli a zero come atto di protesta e di solidarietà
nei confronti di coloro che non sono più tra noi o che si sono ammalati
per cause che un'intera platea di istituzioni da anni non ha avuto e non
ha la volontà di indagare. Sono qui a chiedere di tutelare la salute di
tutte e tutti, non a consegnare lo scalpo in segno di resa.

La solidarietà che provo a mostrare oggi è attraverso un gesto di lotta
verso ciò che è ingiusto. Chiedo che vengano installate le centraline di
monitoraggio dell'inquinamento lungo la tangenziale-autostrada, dovute
per legge fin dal precedente allargamento e che, tra l'altro, anche per
l'attuale progetto dovrebbero essere in funzione un anno *prima*
dell'inizio dei lavori.

I dati delle centraline consentirebbero un'indagine
epidemiologica. Attraverso la trasparenza, si può avere cognizione dei
problemi, innescando circuiti virtuosi (conoscenza-azione-cambiamento),
che altrimenti così, nell'ignoranza, restano impossibili. La
disponibilità di dati completi ed attendibili permette l'analisi della
realtà e di assumere decisioni che siano adeguate ad agire davvero sulle
cause dei problemi ambientali (pressioni ed impatti), migliorando la
qualità dell'ambiente e la sopravvivenza di molti.

È troppo pretendere dalla politica di uscire dalla logica degli annunci,
delle dichiarazioni di principio, per misurarsi con i dati concreti e
reali, misurabili, che permettano poi di verificare gli effetti delle
decisioni assunte? Forse...

Sottolineo però il dovere di tutti noi di operare affinché questo
cambiamento avvenga. Qui, ora, sto tentando --- grazie all'aiuto dei miei
compagni di lotta --- di rappresentare tutte e tutti coloro che hanno il
diritto di conoscere il nesso tra la malattia e l'inquinamento portato
dall'infrastruttura, persone per le quali gli effetti di una
chemioterapia sono probabilmente il male minore all'interno del loro
dramma personale e familiare.

Sono stanca come Sansone. Sono stanca di veder soffrire i miei amici, i
miei vicini. Non posso più stare a guardare senza fare nulla.

Infliggere il taglio dei capelli a un essere umano è un atto ignobile,
una violenza messa in atto per umiliare, dileggiare ferocemente,
annientare psicologicamente. Chi non vuole vedere quello che si deve
fare sta facendo questo.

La rasatura dei capelli per umiliare una donna ha origine antiche: in
genere significa voler additare fisicamente al pubblico ludibrio
qualcuno che si è macchiato di una grave colpa, vera o presunta;
significa voler espellere qualcuno definitivamente dalla comunità, da
quella comunità, e voler spettacolizzare la punizione.

Il mio gesto vuole essere speculare.

Sto accusando voi decisori per la vostra inerzia nel mettere al primo
posto la tutela dell'ambiente e della salute umana, la vostra inerzia ad
occuparvi degli ignari e degli indifesi e soprattutto dei bambini,
affinché possano crescere in salute. I bambini... il futuro...

L'umiliazione non è una tasta rasata, l'umiliazione è non essere
ascoltati.

Che si renda pubblico il tradimento delle istituzioni.
