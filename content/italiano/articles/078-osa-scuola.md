---
author: "Osa* (Opposizione studentesca d’alternativa)"
title: "Questa scuola ha bisogno di una rivoluzione"
subtitle: "Una testimonianza dal Galvani occupato"
info: "Testo scritto per la raccolta bolognese"
datepub: "Aprile 2022"
categories: ["manifestazione, rivolta, protesta", "autogestione, autorganizzazione", "scuola, università, student*"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "78"
quote: "come fermare il tempo per un attimo"
---

Il 21 marzo 2021 il liceo Galvani di Bologna ha deciso di occupare:
completamente coscienti delle conseguenze e carichi di un manifesto che
spiegava le ragioni per cui era necessaria una tale presa di posizione,
noi studenti ci siamo riuniti nel cortile della scuola e abbiamo
manifestato il nostro dissenso nei confronti di un'istituzione che, pur
promettendo di tutelarci ed educarci, ci opprime e non ci rappresenta
più, se mai l'ha fatto.

{{%ligneblanche%}}

A Bologna tutti conoscono il liceo Galvani, famoso per il suo carattere
«elitario», che nessun giornale si è dimenticato di menzionare, e
dell'eccellente preparazione che dà ai suoi studenti. Ma a che prezzo?
Certamente ognuno di noi, prima di scegliere la scuola superiore, era
stato avvisato: «Pensaci bene, il Galvani è un liceo difficile...». Ma,
sicuri delle nostre capacità ed entusiasti di imparare, ci siamo
ritrovati qui. E la mole di lavoro non è il più grande dei nostri
problemi. La maggioranza dei nostri docenti è come un muro di fronte
alle nostre richieste, considera di aver assolto i propri doveri
esponendo (e poi richiedendo) mere nozioni e affibbiandoci infine
valutazioni, vuote quanto ciò che abbiamo «imparato». La pressione
psicologica di cui ci lamentiamo non è quindi data soltanto dall'enorme
quantitativo di lavoro che siamo tenuti a svolgere, ma soprattutto
dall'indifferenza di quei docenti che dovrebbero invece fornirci gli
strumenti necessari al nostro futuro.

Nonostante la sua reputazione, e nonostante i giudizi e le minacce di
quegli insegnanti che probabilmente più si sono sentiti attaccati dalle
nostre parole, il liceo Galvani è stato occupato.

Siamo stati fortunati e intelligenti: non un giorno ha minacciato di piovere e avevamo utilizzato le settimane precedenti per organizzare al
meglio la nostra permanenza nell'edificio. Le giornate sono passate fra
le risate e il piacevole brusio delle chiacchiere, entrambe molto rare
al Galvani, ma nel frattempo c'era chi lavorava per mantenere in piedi
la protesta. Molti erano occupati nell'assicurarsi che nessuna persona
esterna al liceo entrasse: «ClasseViva[^1] e documento», certo una gran
briga, ma fondamentale perché l'occupazione riuscisse senza intoppi,
altri ad organizzare i pasti e fare in modo che tutto procedesse secondo
i piani. Ogni giorno era prevista un'assemblea in cui i nostri portavoce
esponevano ciò che il preside aveva da dire e facevano un breve bilancio
di ciò che era successo nei giorni precedenti.

{{%ligneblanche%}}

Il clima era così piacevole, l'aria che si respirava così genuina, che
non sembrava nemmeno di stare a scuola. Chi si occupava delle attività,
era impegnato a chiamare esperti e a programmare le giornate, che
diventavano così piene di stimoli, da non sapere cosa scegliere. C'era
inoltre chi studiava e chi giocava a pallacanestro, pallavolo o
pallamano. Chi leggeva e chi faceva l'uncinetto. Dovunque era pieno di
gente fiera di essersi presa la propria scuola, e di averla resa, almeno
per qualche giorno, il posto dove voleva essere in quel momento, dove
imparare cose nuove solo per il gusto di farlo, dove conoscere persone
vere, con la voglia di conversare o discutere. La sera poi, c'era la
serenità di poter ballare a ritmo di musica, cantare a squarciagola, o
passare semplicemente il tempo con gli amici, senza la solita ansia che
in genere ogni sera immobilizza i galvanini.

{{%ligneblanche%}}

Per tutti coloro che vi hanno partecipato, questa occupazione è stata
come fermare il tempo per un attimo: nessun dovere, soltanto un momento
per sospendere tutto e riflettere, con la speranza...

[^1]: ClasseViva è un software di gestione informatica del registro
    elettronico in molte scuole pubbliche italiane, ndr.
