---
author: "Flores Magón"
title: "In direzione ostinata e contraria"
subtitle: "Racconto dall'interno di un'esperienza di mutualismo"
info: "Testo scritto per la raccolta bolognese"
datepub: "Marzo 2022"
categories: ["corpo, salute, cura", "solidarietà, mutualismo", "autogestione, autorganizzazione", "anticapitalismo"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "75"
quote: "Se il sistema genera povertà..."
---

Se il sistema genera povertà, combattiamo il sistema o aiutiamo chi ci
rimane incastrato?

La Colonna solidale autogestita è nata dall'esigenza di fare entrambe le
cose: combattere il sistema e nello stesso tempo aiutare chi paga le
conseguenze delle sue storture, in un momento in cui la gestione di un
virus intergalattico, che complica assai la situazione, è appannaggio di
un manipolo di burocrati che oscillano tra incompetenza e disonestà.

## Lockdown

Iperstimolato da una connessione continua ma lontano dai miei simili ho
preso coscienza del valore del tempo e ho pensato a come dedicarlo a chi
poteva avere più difficoltà di me nel vivere una situazione fino a quel
giorno sconosciuta.

Ma che razza di società è quella che si adegua ad uscire dall'isolamento
solo per lavorare per il sistema e alimentare un vortice che ci ha
portato al problema? Le
mie reminiscenze scolastiche mi hanno fatto ripensare a quello che era
la definizione di società: «ogni insieme di individui (esseri umani o
animali) uniti da rapporti di varia natura e in cui si instaurano forme
di cooperazione, collaborazione, divisione dei compiti, che assicurano
la sopravvivenza e la riproduzione dell'insieme stesso e dei suoi
membri».

Facendo tesoro delle mie esperienze ed essendo da sempre più pratico che
teorico ho immaginato di poter dare un contributo individuale
appoggiandomi ad una rete che da anni promuove un'economia solidale e
virtuosa ma che, vittima delle regole del sistema in cui agisce, finisce
per essere forzatamente escludente.

All'interno delle mura del cassero di Porta Santo Stefano, infatti, il
Circolo anarchico Camillo Berneri ospita uno Spaccio popolare
autogestito che promuove l'avvicinamento tra le campagne e la città,
attraverso la distribuzione di cibo e prodotti di prima necessità,
cercando di contrastare la cultura del cibo spazzatura e della grande
distribuzione organizzata (Gdo).

In fondo il cibo ci unisce tutti, ma è inserito all'interno di dinamiche
economiche che rendono ciò che è virtuoso nella sua filiera produttiva,
troppo costoso per le fasce popolari.

## Autogestione°

Nonostante la malagestione del virus, che ci costringe a pensare solo
alla nostra sopravvivenza individuale, lo spirito solidaristico,
mutualistico e di intraprendenza mi ha spinto ad aggirare le restrizioni
e le regole imposte.

Da queste basi nasce l'idea di creare un punto di distribuzione di
prodotti di prima necessità per persone in difficoltà economiche.

Un luogo è un punto di incontro. L'incontro stimola la socialità, lo
scambio, la condivisione. Tutto questo è proprio quello di cui abbiamo
bisogno in questo momento.

Così nella mia testa è nata la Colonna solidale autogestita,
un'associazione che fa quello che lo Stato non fa e nonostante il virus
divida e la società capitalista individualizzi, questo progetto
entusiasma molte compagne.

Una situazione inedita e drammatica che è stata interpretata come
un'imperdibile occasione di fare qualcosa per essere d'aiuto alle altre
persone. E così anche molti ragazzi e ragazze, che mai avevano
partecipato ad attività politico-movimentiste, si sono avvicinati/e\*
con entusiasmo alla raccolta di cibo e beni di varia necessità, darsi da
fare per recuperarli, per redistribuirli, per stoccarli, per dare una
struttura pratica ad un concetto teorico.

Uno spirito di cooperazione che non immaginavo poter essere così forte.

Non pensavo nemmeno che in una città appellata come «la grassa» ci
fossero così tante persone che avrebbero fatto la fila per avere una
cassetta con pane, pasta, riso, patate, qualche biscotto e pannolini.
Sì, tanti pannolini stiamo distribuendo.

Non mancano certamente i problemi legati ad un'attività che mai nessuna
di noi aveva svolto prima. I furbi? I soldi per gli acquisti? E come
facciamo a far sapere quello che stiamo facendo a persone che non
frequentano i nostri spazi? E i prodotti dove li acquistiamo? Sfogliamo
le nostre agendine, facciamo telefonate, scriviamo, costruiamo un sito,
ci dotiamo di un cellulare, ci organizziamo in un collettivo che si dota
di tutti gli strumenti necessari a dare supporto a famiglie di cui le
istituzioni non conoscono nemmeno l'esistenza o non se ne curano.
Arrivano donazioni, materiali, soldi, ringraziamenti, si vedono facce
sorridenti nelle difficoltà, ci si conosce, si parla lingue diverse ma
ci si capisce. Mascherine, distanziamenti fisici e paure ci tengono
lontani, ma il bisogno e la solidarietà ci avvicinano. Siamo nel
quartiere più ricco di Bologna e nessuno dei residenti viene al Circolo
anarchico Berneri per conoscerci, per vedere cosa facciamo. Perché ci
sono tante persone in fila al Cassero di Porta Santo Stefano? Stanno
costruendo bombe? Siamo guardati con sospetto; non è piacevole ma siamo
abituate ormai a non curarcene. La gratitudine sincera delle persone che
ricevono le cassette ci dà le energie per continuare nonostante le
difficoltà. I soldi di coloro che in forma anonima decidono di versare
quote per l'acquisto di prodotti ci permette di continuare ancora oggi
di supportare chi è solo più sfortunata di noi. Però non siamo in una
favola, le difficoltà sono tantissime e il lieto fine probabilmente non
lo leggerete mai. I valori, se non son radicati, sono belle parole che
rimangono scritte sui libri e di libri ne abbiamo letti tanti.

Sono consapevole che la Colonna solidale autogestita non può essere solo
distribuzione di cassette; le mie aspirazioni erano tante, ma
soprattutto avrei voluto che queste famiglie sentissero la differenza
tra noi e la Chiesa, tra una rete di agricoltori/trasformatori e la
merce di scarto dei supermercati, che comprendessero ed entrassero a far
parte attivamente del nostro progetto politico. L'obiettivo è molteplice
ed è dato dalla sommatoria dei valori su cui mi piacerebbe si poggiasse
la comunità in cui vivo: mutuo appoggio, cooperazione, cura delle
relazioni. La declinazione che ha assunto la distribuzione delle
cassette mi ha lasciato una sensazione di fallimento e scoramento, forse
troppo ambizioso il mio sentire iniziale e forse troppe alte le
aspettative riposte nel progetto.

La gestione della situazione sanitaria è cambiata, le restrizioni non
son più cosi vincolanti ma nel frattempo le pratiche mutualistiche si
sono scontrate con le difficoltà delle persone che davano un contributo
attivo alla raccolta e alla distribuzione delle casette. La precarietà
ha portato alla perdita di posti di lavoro e chi prima dava una mano ora
ha bisogno di una mano. Siamo sull'orlo del precipizio e il
riaffacciarsi delle dinamiche egoistiche è dietro l'angolo.

## Svuotare il mare col secchiello

Come immaginare di chiedere un altro sforzo a coloro che già tanto hanno
dato per la Colonna solidale, ma l'evidenza è che la cooperazione
richiede tempo, sinergia, costanza e il nuovo obiettivo è sensibilizzare
altre persone alla partecipazione attiva, cercando nei luoghi in cui
viviamo la nostra socialità senza dimenticarci mai che il capitalismo
sussume le esperienze e le monetizza. Come non pensare agli empori
solidali, luoghi in cui la Gdo trasforma miracolosamente in profitti i
propri fallimenti, fatti di una logistica schiavista che nella frenesia
delle consegne recapita merce danneggiata, acquisti di quantità
gigantesca di prodotti per avere le scansie sempre piene e schiacciare
la concorrenza e il piccolo produttore, ma che poi non trovano sbocchi
nel supermercato e perciò deperiscono. Occorre dare atto al capitalismo
neoliberista di essere un mostro estremamente duttile e resiliente:
anche in questo caso ha trovato il modo di far pagare ai poveri la
propria condizione e di trarne un beneficio. L'emporio solidale è una
sorta di negozio in cui la merce invendibile in un supermercato si
trasforma in una donazione per chi è in una situazione di indigenza,
sempre che abbia tutti i documenti in regola. Scaffali pieni di merce
con involucri danneggiati o a brevissima scadenza che possono essere
presi da queste persone che finiscono per nutrirsi con quello che una
persona «normale» butterebbe nel pattume. Il guadagno, si chiederà, dove
sta? Quella merce che sarebbe per la Gdo un costo (sia come invenduto
che come smaltimento) diventa uno sgravio fiscale attraverso la
donazione alla fondazione che gestisce gli empori solidali.

Sembra di rivivere le esperienze infantili quando in spiaggia si provava
a svuotare il mare col secchiello, ma siamo consapevoli che la sfida va
affrontata a prescindere dai risultati che si potranno ottenere e la
nostra esperienza avrà valore se sarà compresa e replicata in ogni luogo
in cui se ne sentirà la necessità. Le nostre pratiche sono alla portata
di tutte, i nostri errori potranno essere compresi senza essere rifatti,
il nostro entusiasmo iniziale, così come questo scritto, potrà essere di
stimolo a tutte coloro che sentono l'importanza di essere solidali con
le ultime.
