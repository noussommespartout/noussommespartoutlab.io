---
author: "Valerie Taccarelli"
title: "Noi trans siamo un soggetto che combatte sempre"
subtitle: "complessità dell'attivismo trans"
info: "Intervista trascritta per la raccolta bolognese"
datepub: "Aprile 2023"
categories: ["femminismi", "transfemminismoqueer, queer", "corpo, salute, cura", "autoriflessione sulla militanza", "lavoro, precariato, lotte sindacali", "sessualità", "manifestazione, rivolta, protesta"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "95"
quote: "Alle nostre vite!"
---

Ho lasciato Napoli nel 1976, avevo 14 anni. Nel 1975, al doposcuola che
frequentavo, tenuto da cattolici di sinistra, ci avevano parlato
dell'omicidio di Pasolini e questa cosa mi aveva colpito moltissimo. Mio
padre era comunista ma quando ho cominciato ad andare alle
manifestazioni ogni volta che tornavo a casa erano guai, guai grossi.

A Napoli frequentavo i gruppi di Autonomia Operaia, in Piazza delle
Medaglie d'Oro; all'inizio non capivo bene chi fossero e cosa facessero,
pensavo che fossero i figli della buona borghesia napoletana del Vomero
o di Posillipo, li vedevo arrivare sempre con grandi macchinoni, e non
capivo bene da dove arrivassero tutti quei soldi, non sapevo all'inizio,
per esempio che alcuni facevano parte dei Nuclei Armati Proletari. Io
ero uno scugnizzo dei quartieri, un femminiello di 14 anni, giovane e
carino, ed era il periodo dell'amore libero, tutti loro volevano fare
un'esperienza sessuale "particolare". Poi su consiglio di uno di loro ho
cominciato a frequentare anche il FUORI di Napoli, e quindi ad
avvicinarmi anche al Partito Radicale, con cui il FUORI era federato. Il
Partito Radicale allora era una cosa ben diversa da oggi. Con tutti i
limiti delle loro posizioni su certe cose, frequentando il partito
radicale, la mia testa cominciava ad aprirsi, è lì che sono diventata
antimilitarista, anticlericale, contro la psichiatrizzazione, insomma...
anti tutto.

A Roma ci andai per la prima volta per una manifestazione, proprio
quella per il primo anniversario della morte di Pasolini. Dopo il corteo
non me la sono sentita di tornare a casa, ero stata via giorni senza
dare nessuna notizia, e chissà cosa sarebbe successo quando sarei
tornata a casa. Decisi di rimanere a Roma e i compagni del partito
radicale mi indirizzarono a Via del governo vecchio dove c'era la sede
storica delle femministe romane. All'inizio ho dormito lì. Le compagne
pensavano fossi una donna lesbica --- allora femministe e lesbiche erano
negli stessi collettivi --- e guardandomi lo avevano dato per scontato.
Poi venne fuori, che no, non ero nata donna e questo le mandò in
panico... insomma mi dissero che avrei potuto frequentare il posto e
partecipare alle attività ma che non avrei potuto dormire lì. Dovetti
andare via e fui accolta da una compagna lesbica.

A Roma, un po' più lontana da casa e dalla famiglia, ho cominciato ad
accentuare alcuni aspetti del mio modo di essere: mi vestivo con grandi
gonnellone e camicione a fiori, andavo in giro con una valanga di
spillette con simboli politici, insomma mi vestivo proprio come la
tipica femminista anni '70. Per me vestirmi in questo modo significava
tante cose, era anche l'assunzione di una identità politica... ero
femminista e andavo a tutte le manifestazioni femministe, anche perchè
il mio aspetto mi permetteva di frequentarle tranquillamente... credo di
essere stata la prima trans femminista in Italia, in qualche modo.

A Roma ho subìto anche una aggressione fascista, io ero parecchio
ingenua ancora, su alcune cose; fui avvicinata da un gruppetto che
all'inizio mi chiedeva cosa significassero tutti quei simboli sulle
spillette che avevo addosso... insomma erano un gruppo di fascisti che
mi massacrarono di botte. Io sono riuscita a scappare e mi sono
rifugiata all'Hotel Minerva, dove i radicali stavano tenendo un
convegno. Per la paura che avevo avuto mi era venuta una febbre
altissima.

Dopo poco arrivai a Bologna. Ero andata con una compagna lesbica del MLD
(Movimento di Liberazione della Donna) a Milano, e un giorno lei mi
aveva detto che ci sarebbe stato un convegno omosessuale a Bologna, a
Palazzo Re Enzo. Andammo insieme a Bologna e camminammo dalla stazione a
piazza Maggiore. Fu un colpo di fulmine per me: la città aveva
un'atmosfera unica. La piazza, dopo tutte le manifestazioni, si riempiva
di persone, decine di migliaia di persone di tutti i tipi, e ogni gruppo
aveva un po' il suo angolino dove stava. A un certo punto, in questo
mare di compagni tutti un po' vestiti uguali, secondo quella moda un po'
freakkettona dell'epoca, vedo arrivare quattro ragazze vestite con
fantastici abiti da sera, lunghi, eleganti, erano truccate in modo
esagerato... erano bellissime. Era impossibile non notarle... io fui
folgorata. Questo era uno dei tanti aspetti della creatività che
caratterizzava quel periodo. Per me fu importante capire che si potevano
attraversare quegli spazi anche esprimendosi in un altro modo, anche con
gli abiti e il trucco, con quello che si vuole: loro erano diverse fra i
diversi, e potevo esserlo anche io. Da Bologna non me ne sono più
andata.

Bologna mi ha accolto letteralmente, nel senso che per anni ho vissuto a
casa di compagne, compagne donne, che mi prendevano in casa loro, mi
davano da dormire, mi davano vestiti. Una delle prime era Serena Urbani,
compagna anarchica che faceva parte del Living Theatre: uno dei tanti
incontri che mi hanno aiutato a crescere, ad aprirmi ancora di più la
testa. Suo figlio, Massimiliano era cieco; era un ragazzo molto autonomo
e intelligente. Anche lì imparai moltissimo, Massimiliano studiava
ascoltando cassette, per esempio sulla storia di Sacco e Vanzetti o dei
Rosenberg. Mi ricordo un'estate in cui finii con lui a fare da baby
sitter alla figlia di Judith Malina.

Il modo in cui in tante mi accolsero a Bologna mi sembra una cosa che
oggi sarebbe impossibile. Il movimento era ampio, forte, diffuso, e
questo permetteva questa apertura e questa disponibilità che le persone
hanno dimostrato verso di me. A Bologna ho fatto il '77, non capivo
tutto bene, ma capivo che era importante esserci. Mi ricordo i carri
armati non solo in piazza verdi ma in tutte le porte, la città piena di
polizia. Una cosa tremenda. Tutte le facoltà erano occupate, e io avevo
scelto di stare nell'occupazione del DAMS; allora il DAMS esisteva solo
a Bologna e la loro occupazione, quella degli indiani metropolitani, era
la più creativa e aperta a tanti tipi di sperimentazioni. Mi ricordo
bene anche dov'ero il 2 agosto del 1980: ero in via Marsala per fare
delle commissioni e quando sentimmo il boato provenire dalla stazione
via Indipendenza si riempì di persone, come un gregge che andava verso
la stazione per vedere cosa era successo. Il giorno dopo ero lì a
raccogliere le macerie... Ero già nota alla polizia, per il mio
attivismo e anche perchè avevo cominciato a farmi di eroina, (una cosa
che mi sono portata dietro per dieci anni purtroppo), e mi ricordo di un
poliziotto che stupito mi disse, "anche tu sei qui a dare una mano, tu
che sei tossicomane?". Per dirti la considerazione che avevano di noi...

Sì, ero tossica, ed ero anche puttana, perché a un certo punto volevo
essere più autonoma e smettere di farmi mantenere, e allora come oggi
per le donne trans trovare un lavoro è una cosa difficilissima.
Cominciai a fare le marchette e guadagnavo benissimo, almeno per quello
a cui ero abituata; vivevo in un palazzo tutto occupato (da tossicomani)
in via Clavature 20, ci viveva anche Andrea Pazienza, che se la tirava
parecchio.

Dopo il '77 iniziai a frequentare il gruppo frocialista fondato da Lola
Puñales un rifugiato cileno. Eravamo tre gatte. Nel 1980, il 28 giugno,
c'è il primo pride italiano, a Bologna, dopo una manifestazione che
c'era stata a Pisa a novembre. Ero favolosa, avevo una tuta ska bianca e
nera e mi truccavo guardando le foto della Loren. Dopo il pride grazie
ad Annamaria Carloni, referente del P.C., noi "compagni busoni", come ci
chiamava la base del partito, fummo ricevuti dal sindaco, a cui volevamo
chiedere una sede. Dall'80 all'82 io, Beppe Ramina, Robertina, Luciana e
altre andavamo ogni settimana in comune a rompere le palle al sindaco.
Nell'82 finalmente ci diedero il cassero di Porta Saragozza.

In quel periodo anche mi scontrai per la prima volta coi racket della
prostituzione. Noi battevamo in tutto il quartiere della Fiera: a un
certo punto vidi che era arrivato un racket che chiedeva il pizzo alle
prostitute io portai la cosa ad una riunione del Cassero ma una parte,
inclusa la Puñales, non voleva schierarsi con le trans prostitute e ci
fu una spaccatura. Con Ramina andammo dalla Carloni ma lei diceva che la
base non era pronta... la minacciai che allora mi sarei messa a battere
sotto il Cassero, l'unico posto dove sarei stata al sicuro.... lei saltò
sulla sedia... ci trovarono subito un paio di avvocati. A capo del
racket c'erano una trans e un travestito: alla fine è finita perché c'è
stata una lotta intestina e cominciarono, loro e i loro scagnozzi, a
denunciarsi tutti a vicenda.

Quando Marcella di Folco porta il Mit a Bologna (prima era a Roma), nel
1987, io ci andai, ma lei fu molto ostile (forse perché appunto si
sapeva che mi facevo) e quindi per i primi anni non vi partecipai.
Dobbiamo tutt\* tantissimo a Marcella, ma era anche una persona molto
complicata e ben presto si ritrovò da sola in questa associazione che
aveva fondato. Io intanto avevo cominciato a partecipare sempre di più
alle attività della Lila. Nel 1986 avevo scoperto di essere
sieropositiva. In quegli anni una diagnosi così significava che pensavi
che saresti morta sicuramente. La Lila faceva tante cose, io raccoglievo
moltissimi soldi per loro fra le trans e le prostitute e fui molto
coinvolta nell'organizzazione della casa alloggio: stava in via Laura
Bassi e c'era bisogno di tutto, il comune voleva addirittura che
pagassimo l'affitto all'inizio. Io chiesi ad un mio amico travestito,
che grazie alla prostituzione stava bene economicamente, di aiutarci con
l'arredamento e tutto quello che poteva servire alla casa.

Si può dire che io sono arrivata all'attivismo trans dall'attivismo
sull'HIV. A un certo punto nel 1993 mi chiama il presidente della LILA
Bologna Rino Varrasso: il MIT aveva ricevuto un grosso finanziamento per
fare attività di prevenzione e supporto per le persone con HIV. Marcella
aveva in mente di destinare quei soldi per creare un consultorio
autogestito da persone trans che le aiutasse nel percorso di
transizione. Fu allora che capii che dovevamo impegnarci di più, anche
proprio come trans: supportare le persone trans nei loro percorsi di
autodeterminazione significa sicuramente assisterle nei percorsi di
affermazione del genere scelto, ma significa anche confrontarsi con
fenomeni come la prostituzione, la migrazione, la sieropositività, la
difficoltà di inserimento lavorativo. Bisogna avere questa visione di
tutte le complessità che influiscono sulle vite trans.

Fu allora che convinsi Porpora Marcasciano a partecipare alle attività
del MIT con Marcella. Possiamo dire quindi che il Mit ha due fasi:
quella in cui viene fondato da Marcella, nel 1979, con cui si arriva nel
1982 alla legge sul cambio di sesso del 164, e poi dopo il 1993, quando
entriamo io e Porpora per supportare Marcella. A questo punto io stavo
molto meglio, non mi facevo più e dopo la diagnosi di HIV avevo deciso
che volevo, per quel poco tempo che mi restava, il meglio dalla vita: mi
prendevo molta più cura di me stessa. Andai da Marcella con un tailleur
Armani prima linea, borsa e orecchini di Chanel, e i capelli in uno
chignon favoloso... lei rimase letteralmente senza parole...e chi
conosce la "Di Folkland", come la chiamavo io, sa che questa è una cosa
rara!!!

Io in quel periodo sono diventata buddhista e la pratica è sempre stata
importantissima per me. Il buddhismo ti dice chiaramente che tu devi
impegnarti e agire nella società e questo ha dato anche una nuova forza
al mio agire politico. Il buddhismo mi ha aiutato tantissimo anche ad
affrontare il periodo successivo alla diagnosi di HIV, e tuttora è una
parte importante della mia vita.

Con il Mit abbiamo fatto tantissime cose. Il consultorio è nato nel
1994. Mi ricordo che quando arrivarono le psicologhe Marcella mi
convinse a essere io la prima a fare la terapia: io all'inizio non
volevo, non vivevo la transessualità come un problema personale... poi
però pensai che potevo fare da "cavia"... chi meglio di me poteva
aiutare la psicologa a "farsi le ossa". In quegli anni ci si siamo rese
più autonome rispetto al MIT nazionale, e abbiamo cambiato nome da
Movimento italiano transessuale a Movimento Identità trans. Facevamo
interventi in carcere, fra le trans che si prostituivano, avevamo lo
sportello HIV, tutto veniva fatto con un modello "alla pari" e
mutualistico. Purtroppo per portare avanti le attività del consultorio
abbiamo sempre dovuto avere relazioni con le istituzioni. Relazioni
molto complicate.

Il comune di Bologna ci ha creato mille difficoltà e mille problemi e i
pochi soldi che ci dava venivano dalla comunità europea. Dovevamo
contrattare per tutto, e devo dire che non c'era quasi mai da fidarsi.
Una delle cose più penose per me, una delle ragioni per cui poi sono
uscita dal MIT nel 1999, è stato il modo in cui è andato a finire un
progetto per la formazione di donne trans che doveva servire a inserirle
nel mondo del lavoro, nel settore dei beni culturali e ambientali. La
contrattazione fu tremenda: alla fine volevano darci la metà dei soldi
per un monte ore doppio di quello che avevamo preventivato. Io avevo
convinto tante donne trans a partecipare e per loro chiaramente la
prospettiva di un impiego era un grosso sollievo. Alla fine dovemmo
litigare con un dirigente, che fra l'altro era pure una frocia, Vincenzo
Castelli, che a un certo punto mi aveva anche detto "ricordati da dove
vieni"... come se comunque avessi dovuto essergli grata. Io e Marcella
facemmo una sceneggiata, ma ottenemmo quello che avevamo chiesto. Poi
però ci fu l'elezione di Guazzaloca e il comune non mantenne i suoi
impegni. Questo è il problema dell'avere a che fare con le istituzioni,
anche con istituzioni che si sentono "illuminate" come quella di
Bologna: i loro equilibri politici vengono sempre prima di tutto, e le
vite delle persone finiscono a dipendere dai loro giochini o dai loro
capricci. Per me fu un bruttissimo colpo il fatto che queste donne che
avevo convinto a seguire questi corsi poi non avessero il lavoro per cui
si erano impegnate.

Un'altra pratica che abbiamo portato avanti per anni, io l'ho fatto dal
1996 al 1999, ma è continuata anche dopo, è stata quella dell'unità di
strada per le prostitute, o meglio "umidità di strada" --- perchè ne
prendevamo di freddo a stare in giro la sera. Andavamo in Fiera con una
mediatrice culturale (africana, o rumena, o albanese... le brasiliane
erano poche a Bologna), portavamo materiali informativi in lingua, e
tante cose che potevano servirgli. Soprattutto, cercavamo di convincerle
ad usare il preservativo. Per le donne non bianche e non italiane,
spesso sfruttate o indebitate, e per le donne cis anche più che per
alcune donne trans, è più difficile negoziare pratiche di sesso
protetto. All'inizio dovemmo vedercela con i papponi, che ci
minacciavano e non ci facevano avvicinare alle ragazze, ma poi siamo
riusciti a convincerli, in fondo conveniva anche a loro che le ragazze
potessero avere una assistenza sanitaria. Loro non sapevano che per
quelle che volevano uscire dalla tratta attivavamo un percorso con la
casa delle donne.

Se penso alle pratiche portate avanti dal movimento trans, quelle del
passato e quelle del futuro, penso che c'è sempre tanto da fare per le
persone trans, ma che le lotte trans vanno viste in una cornice ampia e
in modo intersezionale... come dire... le lotte trans devono essere per
tutt\*: Angela Davis ce lo dice da cinquant'anni. Questo vale anche per
il movimento LGBTQ in generale... per me concentrarsi solo sulla
famiglia o sui diritti delle coppie è una prospettiva asfissiante. Le
persone trans, forse anche perchè per loro l'integrazione è comunque più
difficile, si trovano in una posizione in cui possono sempre ricordare a
tutte le altre "cose strane", ai soggetti non-etero, che le nostre lotte
o sono intersezionali o servono a poco. Bisogna andare alle
manifestazioni dei migranti, delle prostitute, partecipare ai loro
percorsi, opporsi allo smantellamento del welfare sanitario e della
scuola. Io penso che le persone trans possono essere in prima fila in
queste lotte, come erano in prima fila i femminielli che hanno liberato
Napoli dal nazifascismo! Noi trans siamo un soggetto che combatte
sempre.

Ok...per ora basta così, un'altra volta magari vi racconto delle lettere
di Erri De Luca e delle telefonate di Milva, di Sylvia Rivera, Santa
Insolvenza o dei femminielli;

Adesso devo sentire questo ragazzo napoletano e capire se è il caso di
farmi venire a trovare...

facciamo un brindisi intanto....

"Alle nostre vite"!
