---
author: "Anonim*"
title: "Sopravvivere in un black bloc"
subtitle: "Teoria, argomenti, lacrimogeni, repressione"
info: "Testo scritto per la raccolta svizzera romanda"
datepub: "Febbraio 2021"
categories: ["polizia, repressione, amministrazione giudiziaria", "DIY, tutorial", "manifestazione, rivolta, protesta", "violenza, non violenza", "autodifesa", "sabotaggio, azione diretta"]
tags: ["bouche", "café", "colère", "corps", "câlin", "eau", "feu", "France", "liberté", "limite", "lunettes", "parole", "peur", "pouvoir", "rue", "thé, tisane", "théâtre", "violence", "visage"]
langtxt: ["fr", "it", "en"]
zone: "Suisse romande"
num: "15"
quote: "Sono ormai vent'anni che viene regolarmente annunciata la morte del black bloc e che questo si ostina a ricomparire"
---

{{%epigraphe%}}
*\[\...\] I Black Bloc sono i migliori filosofi politici del momento.*

*--- Nicolas Tavaglione, Le Courrier, Ginevra, 2003*[^1]
{{%/epigraphe%}}

{{%ligneblanche%}}

Sono ormai vent'anni che viene regolarmente annunciata la morte del
black bloc° e che questo si ostina a ricomparire, in Svizzera come nel
resto del mondo. Questa costante rinascita è il riflesso della nostra
epoca. Con l'avvento della sorveglianza di massa, la crescente
militarizzazione dei corpi di polizia e il divario sempre più visibile
tra il popolo e le élite che si autolegittimano attraverso le urne,
qualunque movimento significativo di trasformazione sociale sembra
richiedere l'anonimato, lo scontro violento con le autorità e una messa
in scena di sé in posizione di pieno potere sulla città.

Con la crisi mondiale del capitalismo globalizzato e le premesse di una
crisi ecologica e sociale in divenire, le strategie del blocco nero
vengono adottate con frequenza sempre maggiore, specialmente nei paesi e
nei contesti culturali nei quali costituiscono una novità. Si tratta di
strategie che hanno preso forma in un terreno decisamente ostile.
Parliamo di strategie relegate ai media di movimento°, quando non alla
vera e propria clandestinità, prive di un'autentica formalizzazione così
come di una reale condivisione dei saperi su di esse, che si scagliano
contro solidissime strutture di potere e godono di scarso credito presso
l'ala pacifista dei movimenti e, più in generale, il resto della
popolazione. Eppure è proprio dal "resto della popolazione" che proviene
chi sceglie, sempre più spesso, la radicalità del black bloc per stare
in situazioni di piazza.

Questo contributo combina l'esperienza svizzera romanda con alcuni
spunti dai testi *Défense du black bloc* (In difesa del blocco nero, 2010) della militante antirazzista Harsha Walia e di *Les black blocs,
la liberté et l'égalité se manifestent* (I black bloc, la libertà e
l'uguaglianza si mostrano, 2016) del teorico e militante anarchico
Francis Dupuis-Déri.

## Sopravvivere in teoria

A cosa serve un black bloc radicale? Beh, a un sacco di cose:

- costituisce una tattica, più o meno efficace, la cui legittimità non
viene discussa *di per sé*.
- in teoria, promuove l'idea che si sta tutt\* insieme in un unico
gruppo anonimo; nella pratica, dà a chiunque la possibilità di
manifestare eludendo la sorveglianza ed evitando di finire negli archivi
della politica, cosa che potrebbe perseguitarti per tutta la vita.
- spesso, il blocco permette d'impedire la traduzione in caserma di
chi è in mano alla polizia attirando l'attenzione, creando un effetto
sorpresa e adottando strategie di attacco. Il blocco è anche una delle
migliori espressioni della formula "si parte e si torna insieme".
- spezza le forme ritualizzate della disobbedienza e della
contestazione, che tendono a obbedire a una gestione autorizzata dello
spazio e a rimanere nei limiti delle convenzioni cosiddette democratiche
(manifestazioni, sit-in°, petizioni, ecc.).

## Sopravvivere alle critiche

Al black bloc si rimprovera spesso di permettere l'infiltrazione di
poliziotti-agitatori camuffati da manifestanti che commettono vandalismi
inutili (attaccando bersagli incoerenti come i piccoli negozi anziché le
grandi banche) o incastrano altr\* manifestanti.

È un rimprovero che non sta in piedi: la storia delle lotte sociali
dimostra che questo tipo di agitatori è sempre esistito e che i
poliziotti tendono a travestirsi, nella maggior parte dei casi, da
giornalisti o da manifestanti a viso scoperto.

Gli si rimprovera frequentemente di essere inefficiente, cosa che spesso
è vera. Ma ci si interessa troppo poco all'inefficienza di altre
tattiche, come ad esempio quella di indirizzare in continuazione
messaggi alle istituzioni. Eppure il blocco rimane il bersaglio
principale del discorso "sull'inefficienza", il che rivela che il
dibattito si basa essenzialmente sull'interiorizzazione dei sistemi di
valore dominanti relativi alla rappresentazione della proprietà privata
e della violenza.

Anche la violenza è un capo d'accusa frequente. Cosa alla quale si potrà
sempre rispondere che esistono tre tipi di violenza. La prima è quella
del capitale, dell'estorsione, dello sfruttamento, dell'uccisione, del
razzismo e del patriarcato. Poi viene la violenza difensiva, che esiste
esclusivamente come reazione legittima al primo tipo di violenza. Infine
c'è la violenza repressiva, attraverso la quale lo Stato si assicura che
il primo tipo di violenza possa continuare a essere perpetrato. Quando
si spaccano le vetrine di una banca che fa soldi grazie alle dittature,
ai network neocoloniali se non addirittura ai genocidi, e le cui
emissioni di CO2 sono fino a venti volte superiori a quelle di tutta la
popolazione svizzera, non si tratta forse della violenza difensiva degli
esseri viventi? I bersagli di un blocco non sono mai scelti a caso:
Organizzazione mondiale del commercio, Fondo monetario internazionale,
G8, WEF, banche, assicurazioni, multinazionali. Non dimentichiamo quali
sono le realtà che il blocco cerca di spaventare, né la forza di fuoco
delle forze dell'ordine. Stiamo parlando di poche persone che lottano
con pietre e sciarpe contro dei soldati super-allenati che dispongono di
un equipaggiamento da decine di milioni di franchi. Chi e cosa sta
difendendo la polizia?

E non dimentichiamo che la contestazione violenta diventa legittima e
necessaria agli occhi di buona parte della popolazione non appena è
storicizzata, ossia quando diventa una violenza *del passato* sulla
quale si sono costruiti alcuni progressi essenziali del mondo moderno.
In Francia la decapitazione del re è un buon esempio, e oggi Jeff Bezos
è ben più potente e oppressivo di quanto non fosse Luigi XVI. Le azioni
dirette e particolarmente violente degli scioperanti, delle suffragette°
o dei Black Panther hanno dato adito a un mondo più femminista, meno
razzista, un mondo in cui non si lavora più 12 ore al giorno e 360
giorni all'anno. E queste violenze, le violenze di ieri, sono ampiamente
accettate se non giustificate dalla popolazione. Se sarà più giusto, più
antirazzista, più femminista e più ecologico, il nostro futuro
giustificherà la violenza dei black bloc di oggi.

Si rimprovera al blocco di creare divisioni tra le lotte, ma
l'esperienza mostra anche che orientando il discorso pubblico verso una
distinzione tra manifestanti buon\* e cattiv\*, il blocco distoglie
l'attenzione della polizia dalle iniziative comunitarie aperte e
alternative, costringendo i media a considerarle positivamente (per
contrasto con il blocco). Spesso, anzi quasi sempre, chi partecipa al
black bloc rivendica una diversificazione delle tattiche, la necessità
dell'aiuto reciproco all'interno della comunità e comprende l'utilità
delle strategie pacifiste e non violente.

Invece, l\* portavoce dei movimenti riformisti che stanno al gioco e ne
approfittano per far bella figura con la stampa dissociandosi dal black
bloc non fanno che consolidare la legittimità della violenza di Stato.
Del resto non è raro che l\* rappresentanti della politica lancino
appelli a condannare la violenza. È un elemento molto importante della
retorica politica, volto a pacificare e dividere le lotte.

Siparietto tratto dalla realtà che illustra una buona reazione a un
appello mediatico al pacifismo:

> *David Pujadas: Il vostro sgomento è comprensibile, certo, ma le cose
> non si stanno spingendo troppo oltre? Lei deplora queste violenze?*
>
> *Xavier Matthieu (delegato CGT --- confederazione generale del lavoro
> --- di Continental): Sta scherzando, spero?*
> 
> (L'intervista prosegue.)
> 
> *David Pujadas: Va bene, abbiamo ascoltato la vostra rabbia, ma
> stasera lancerete un appello alla calma?*
>
> *Xavier Matthieu: Io non lancio un bel niente. Non ho nessun appello
> alla calma da lanciare. La gente è arrabbiata e questa rabbia ha
> bisogno di esprimersi. Chi semina miseria raccoglie rabbia.*
> 
> *(Telegiornale di France 2, 21 aprile 2009.)*

Notiamo tra l'altro che quello che Pujadas chiama "sgomento" è il
sentimento di centinaia di dipendenti licenziat\* da una multinazionale
che, quell'anno, aveva registrato incassi oltre la decenza. Denigrando
le strategie più violente, il discorso mediatico consolida l'immagine
pubblica di un movimento caotico e frammentato, mentre bisognerebbe
invece costruire l'immagine di un movimento coerente che accetta una
pluralità di tattiche. Più i media, la politica e gli altri movimenti
marginalizzano determinate forme di lotta, più la polizia potrà
permettersi di reprimerle con la violenza.

Si rimprovera spesso al blocco di essere una minoranza isolata. Ma la
storia delle lotte riposa essenzialmente sul ricorso di piccoli gruppi
all'azione diretta°. È quello che è successo in Svizzera durante gli
scioperi del 1917 e 1918, ad esempio. L'azione diretta emerge quando le
persone si difendono. Se bisognasse sempre aspettare che milioni di
persone si mobilitino allo stesso tempo o che la politica ascolti le
rivendicazioni di gruppuscoli e sindacati, oggi vivremmo in un mondo ben
peggiore. Essendo una tattica, si potrebbe anche rifiutare
l'argomentazione della "minoranza radicale", dato che in realtà esiste
un continuum tra le rivendicazioni emancipatrici e l'azione del blocco:
spesso un *green bloc°* porta avanti le stesse rivendicazioni dei
movimenti pacifisti come la Grève du Climat in Svizzera.

Si rimprovera a chi prende parte al black bloc il fatto di voler
spaccare tutto e basta: l\* loro avversari\* l\* chiamano teppist\*,
depoliticizzando le loro istanze. Si parla di "accessi di rabbia
apparentemente priva di senso \[...\] a prescindere dal retroterra
politico o ideologico". Per ribattere a questa critica basta
considerare gli striscioni portati dal blocco o la selezione dei
bersagli da vandalizzare.

Spesso si rimprovera loro di legittimare o rafforzare la brutalità dello
Stato di polizia. Ma se un argomento simile può essere utilizzato,
allora tanto vale abbandonare proprio l'idea di scendere in piazza: lo
Stato di polizia si giustifica da sé. Basta che una rivendicazione
prenda una certa ampiezza perché si attivi l'intero meccanismo della
repressione, senza alcun riguardo per la situazione reale. Da quando in
qua diamo la colpa all\* nostr\* alleat\* per il costante aumento delle
violenze poliziesche?

Spesso si rimprovera al blocco di delegittimare i movimenti di protesta,
facendo perdere loro credibilità presso i media mainstream. Stessa
logica: quando mai questi media si sono schierati dalla parte delle
lotte sociali?

In sintesi, non c'è alcun motivo di idealizzare il black bloc, che in
fin dei conti è una tattica come le altre (non sempre pertinente ed
efficace). Il punto è che la maggior parte delle argomentazioni
scagliate contro di esso rimandano in fin dei conti
all'interiorizzazione di una serie di valori dominanti e oppressivi
(rispetto della proprietà privata, presentabilità, responsabilità
amministrativa, legittimità agli occhi dei mass media, ecc).

## Sopravvivere ai lacrimogeni

La polizia antisommossa svizzera tende a far ricorso ai gas lacrimogeni
per disperdere le manifestazioni di una certa grandezza, anche quelle
organizzate in uno spirito di festa. Per esperienza, sembra invece più
incline a utilizzare i proiettili di gomma e i cannoni ad acqua quando
si tratta di gruppi piccoli e organizzati. Può capitare di beccarsi i
lacrimogeni, ma è possibile arrivare preparat\*:
- non farti prendere dal panico, fa schifo ma andrà meglio tra 10-15
    minuti, passerà tutto quando sarai al caldo, con un tè e un
    abbraccio.
- se sul momento hai la nausea, fidati che passerà.
- se sul momento ti vengono crampi e spasimi oppure se i disturbi
    respiratori persistono dopo un'ora, consulta un\* dottoress\*.
- se aspetti un bambino e hai respirato il gas lacrimogeno, anche in
    questo caso è meglio vedere un\* dottoress\*.
- produrre un eccesso di secrezioni (saliva, muco) è un riflesso
    biologico di difesa: non esitare a soffiarti il naso e a sputare il
    più possibile, altrimenti rischi di sentirti affogare nei miasmi.
- cerca di avvolgere il naso e la bocca in una sciarpa bagnata (anche
    sotto il passamontagna). Funziona anche se è bagnata di sudore.
    Assicurati di avere sempre dell'acqua nello zaino.
- proteggi il cuoio capelluto: il gas lacrimogeno si infiltra nei
    pori.
- in generale, meno esponi la pelle, meno aggressivo sarà il gas.
- evita di rasarti le parti del corpo esposte (faccia compresa)
    subito prima di una manifestazione: il gas ha un effetto fortemente
    irritante sui pori esposti.
- evita di toccarti gli occhi.
- non dimenticare gli occhialetti da piscina.
- portati dietro un sacco di siero fisiologico (in vendita senza
    ricetta in farmacia, e costa poco).
- se puoi, usa gli occhiali invece delle lenti a contatto, anche se a
    volte col passamontagna è un casino. Il contatto con il gas rischia
    di fonderle. Se hai messo le lenti a contatto, toglitele al minimo
    sospetto di gas lacrimogeno, prima di avere le dita tutte piene di
    gas, e mettiti gli occhiali. Se ti becchi i lacrimogeni sulle lenti
    a contatto, riempiti gli occhi e le mani di siero fisiologico,
    togliti le lenti e continua a pulire.
- sul momento puoi anche cercare di fermare l'azione del lacrimogeno,
    ma attenzione, per farlo devi avvicinarti al gas. Esistono due
    approcci:
    1.  usare una racchetta da tennis, o qualsiasi oggetto lungo ed
        elastico, per rimandare indietro il candelotto (mirando chi l'ha
        lanciato, ovviamente).
    2.  L'emissione di gas lacrimogeno è indotta da un'esplosione
        pirotecnica. In soldoni c'è qualcosa che brucia dentro e finché
        continua a bruciare, il gas continua a uscire. Puoi quindi cercare
        di spegnerlo come spegneresti (o accenderesti) un fuoco, ad esempio
        appoggiandoci sopra un cono segnaletico e versandoci acqua
        dall'apertura superiore (poco efficace perché richiede una grande
        quantità d'acqua, che non si ha necessariamente a disposizione), o
        più semplicemente appoggiandoci sopra un cartone o lanciando il
        candelotto in un container dei rifiuti (che va richiuso,
        ovviamente). Oggi molti modelli sono waterproof e questo trucco
        potrebbe non funzionare.
- una volta tornat\* a casa, limita al massimo il contatto tra i vestiti e l'ambiente interno (le particelle di lacrimogeni possono rimanere attive fino a 5 giorni) e fatti una doccia fredda di circa 20 minuti per ridurre l'irritazione.

## Sopravvivere alla repressione poliziesca

- prima di tutto, ed è la cosa più importante, tieni a mente il
    rischio di esporre altre persone alla repressione, quindi RIFLETTI
    al movimento che stai andando ad agitare: se è per mettere in
    pericolo gente che è già particolarmente vulnerabile alla
    repressione (ad esempio le persone senza documenti), tanto vale
    restare a casa. Le lotte operaie e/o ambientaliste spesso si
    prestano meglio ad azioni radicali e autonome.
- in generale, non arrivare mai a una manifestazione con tutto
    l'armamentario addosso, come chi fa l\* fig\* in fotografia. In
    primo luogo rischi di dare spettacolo nella metro. In secondo luogo,
    se sei identificabile, la polizia non ti attaccherà con proiettili o
    lacrimogeni ma si limiterà probabilmente ad accerchiarti e
    arrestarti. Per evitarlo, meglio rimanere in movimento e fondersi
    nella massa.
- non lanciarti mai da sol\* ma rimani sempre in un gruppo con cui
    hai affinità, cercando di non perderlo mai di vista. Per spostarvi
    in gruppo attraverso la folla, tenetevi sempre per mano. Se non
    conosci il gruppo, vieni almeno in coppia con un'altra persona per
    proteggervi a vicenda.
- agisci sempre all'inizio o nel pieno di un movimento di massa, mai
    alla fine. Innanzitutto per dare il tono alla giornata (che tenderà
    a radicalizzarsi se inizia già in modo concitato) e soprattutto per
    evitare di ritrovarti isolat\* a fine giornata. La polizia è molto
    più violenta quando non rimangono più molt\* "brav\*" cittadin\* per
    osservarla.
- il momento del mascheramento è cruciale ed è una questione
    difficile: se ti mascheri troppo tardi rischi di essere
    identificat\*, se lo fai troppo presto rischi di farti arrestare. I
    piccoli gruppi mascherati che vanno in giro isolati sono dei
    bersagli ideali. Nelle grandi manifestazioni, la soluzione migliore
    è fonderti nella folla, circondato da persone di fiducia, per
    raggiungere il punto dell'appuntamento e mascherarti.
- portati dietro diversi strati di abbigliamento per cambiarti mentre
    vai alla manifestazione e cambiarti di nuovo prima di andartene.
- cerca di rimanere sempre in contatto con chi va in ricognizione
    (una persona che ha voglia di assumere meno rischi) che gira intorno
    al blocco per prevenire i movimenti di massa della polizia.
- cerca di mantenere un blocco compatto per evitare che le pattuglie
    di polizia facciano incursioni per arrestare una persona in
    particolare. Posizionare dei grandi striscioni rinforzati di fronte
    e ai fianchi del gruppo può aiutare a evitare questo tipo di
    intervento. Attenzioni, gli striscioni vanno retti da più persone.
- all'interno del gruppo, usate dei soprannomi temporanei per la
    giornata per evitare di usare le vostre vere identità.
- comunica rispettando le buone pratiche di sicurezza digitale,
    specialmente se comunichi con uno smartphone. È comunque più
    prudente uscire senza smartphone.
- uno striscione o un cartello possono facilmente dissimulare uno
    scudo più resistente.
- le protezioni sportive (parastinchi, ginocchiere, ecc.) possono
    sempre tornare utili.
- le torce o anche i laser possono abbagliare le persone.
- la polizia tende a disperdere l\* manifestant\* (con lacrimogeni,
    proiettili di gomma, ecc.) quando non è pronta a effettuare arresti.
    Una volta pronta, farà probabilmente una carica in schiera e in
    successive ondate (avanzando e retrocedendo). La prima strategia è
    di solito quella di effettuare alcuni arresti individuali e violenti
    per impressionare e scoraggiare l\* altr\* manifestanti. Se la folla
    è compatta, è probabile che la polizia manterrà le distanze. Se
    invece la folla sembra evasiva, caotica o passiva, la polizia
    cercherà di formare delle linee per tagliare la massa e isolare
    gruppi più piccoli da accerchiare.
- se vieni pres\* dalla polizia, ricorda che anche il minimo riflesso
    di autodifesa da parte tua può aggravare sensibilmente il capo
    d'accusa (aggressione a pubblico ufficiale). Un buon metodo è quello
    di trasformarti immediatamente in peso morto, cessando ogni
    movimento. Questo limiterà i capi d'accusa e in più renderà più
    complicato il lavoro degli sbirri.
- alla fine dell'azione, l'ultima sfida è andartene senza problemi.
    Gli strati di vestiti sono molto utili: trovati un posto tranquillo
    per cambiarti, anche in mezzo alla folla o in una strada parallela,
    verificando sempre se ci sono telecamere. Se non conosci la città,
    non esitare a seguire i gruppi di persone che scappano o ad
    aspettare in un caffè. Meglio dissolvere il gruppo e rimanere in
    binomio.
- studiati bene gli aspetti legali della repressione. Impara a
    memoria o annotati sul braccio il numero di un\* legale prima di
    andare in piazza e consulta una buona guida di autodifesa giuridica
    scritta da militanti del tuo paese, se possibile una guida recente
    (le leggi cambiano in fretta). Queste guide possono aiutarti a
    capire, ad esempio, cosa dire o non dire durante un fermo di
    polizia, ecc.
- infine, la regola assoluta dell'azione diretta: fermati quando va
    ancora tutto bene, non esagerare e risparmia le energie per la
    prossima volta.

## Sopravvivere in conclusione

La sommossa non è e non deve diventare un progetto politico: è
semplicemente una forma di contestazione (tra l'altro, storicamente ci
sono state anche delle sommosse poliziesche). Tenendo a mente che ci
comportiamo così *perché non ci hanno lasciato altra scelta che la
violenza*, rimarremo concentrat\* sul vero obiettivo: costruire un mondo
accogliente e fatto di cura reciproca. Il resto è accessorio e tattico.
A volte si tratta di una valvola di sfogo, a volte di una riparazione,
ma l'esperienza del blocco ci ricorda sempre che ciò che *conta davvero*
è essere insieme, resistere, amarsi e non rinunciare al sogno di un
mondo meno peggiore.

[^1]: Incipit del libro *Les black blocs, la liberté et l'égalité se
    manifestent *di Francis Dupuis-Déry
