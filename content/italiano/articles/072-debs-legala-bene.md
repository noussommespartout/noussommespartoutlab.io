---
author: "Debs"
title: "Legala bene"
subtitle: "Storia di catene, sellini e sudore"
info: "Testo riportato in vita per la raccolta bolognese"
datepub: "Aprile 2022"
categories: ["sessualità", "ecofemminismi", "femminismi", "giustizia ambientale, ecologia", "manifestazione, rivolta, protesta"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "72"
quote: "liscia, libera, veloce"
---

Liscia, libera, veloce. Ogni pedalata è un pezzo di strada mangiato.
Sento il controllo e il potere di quello che succede fra le mie gambe.
La mia figa ha un trono stupendo. Mi infilo tra le macchine con un
manubrio strettissimo. Mi carico, mi espando. Devio le buche. Non temo
gli autobus. Sfreccio veloce. Divoro questa maledetta città. Faccio lo
slalom tra i passanti che si buttano tra un portico e l'altro. Sfuggo i
vigili urbani deviando drasticamente in contromano. Sono veloce. Mi
sento potente. Mi alzo sui pedali e stendo le gambe come se nuotassi. Ho
un culo grande e morbide cosce: mi piace farle vedere quando faccio
arrabbiare gli automobilisti. Mi sento invincibile in ogni rettilineo.
Non ho paure o ansie, sono libera. Posso andare ovunque e nessuno

mi sta dietro. Libera da consumi, lentezze, inquinamento, divieti,
orari.

*È l'inizio dell'estate e porto via la mia bici da un nido che l'aveva
accolta per molte notti. Il garage di René ha una porta facilissima da
aprire, niente scale, impicci, ci entravamo lisce. Mi mancherà quella
sicurezza, disinnesco delle mie paranoie notturne sulle fughe della mia
cara bicicletta. Domani avrei detto a René che non avremmo più dormito
nello stesso letto: ci eravamo succhiati, disidratati, sporcati
abbastanza. Prendo il sellino per accompagnare fuori la bici, così come
gli prendevo le palle appena gli sfilavo le mutande.*

Il centro lo detesto. Abito in periferia per poter pedalare di più. Ogni
bicicletta deve avere le giuste alleate

«È bruttissimo, ma ti giuro che è uno dei telai più leggeri che abbia
mai visto»

«Mi aiuti a metterla sù?»

«Guarda io ti dico cosa fare e poi rompi poco il cazzo che qua nessuno
si sa più riparare una camera d'aria da sola e devo far tutto io».

Gina è la biciclettaia più anziana e rispettata che conosca. Odia gli
uomini quanto io le macchine. È brava e generosa quanto antipatica.
Parla molto con le bici e poco con le persone. I suoi ricordi di
biciclette sono vivi,
rabbiosi, felici e si scontrano con una città che muore piano piano. Ha
spesso gli occhi lucidi e le mani ruvide, rugose, sporchissime.

*René era uno qualsiasi. Condividiamo quel che rimaneva dell'attivismo,
alcuni spazi, alcune assemblee. Neanche mezza volta mi era passato per
la testa di poterci scopare. Era timido, parlava il giusto, si vestiva
maluccio. Se non c'era non per forza te ne accorgevi. Era disponibile,
spettinato, non polemizzava mai, faceva il fonico in tutte le occasioni.
Chiusa la porta della sua stanza però era disinvolto, padrone,
eccitante, sicuro di se. I miei millenari pregiudizi entravano in
cortocircuito, il che mi permetteva di amplificare enormemente ogni
sensazione che mi suscitava. Mi chiedevo ogni tanto se scopavo la stessa
persona che incontravo fuori.*

Le bici a Bologna ormai si usano sempre meno. I turisti affittano dei
catorci pesantissimi per girare in poche vie del centro, come animali in
uno zoo, per vivere la Bologna di una volta. Qualche vecchio in
quartiere ci va su e giù lentamente. Qualche presunto intenditore compra
robaccia a prezzi vertiginosi pensando di collezionare chissà quale
chicca. Se usi la bici nella vita di tutti i giorni ormai sei un po'
strano. Tassisti, vigili urbani, commercianti, assessori vari ci hanno
piano piano sgonfiato le ruote. Associazioni per la mobilità sostenibile
svendute per qualche posto di lavoro. Multe, divieti, abbandono delle
ciclabili, dehors ovunque, bus turistici che sfrecciano come pazzi,
strade invase da macchine giganti che possono ignorare le buche. Andare
in bici è diventato un atto stravagante eccentrico rivoluzionario.

*Entrai a casa di René che il vento mi aveva asciugato tutto il suo vino
di dosso. Era la prima volta. Nella strada per casa sua mi teneva vicina
stretta e mi parlava. Mi chiedeva come stavo e poi parlava di lui, mi
sembrava sereno, leggero, pettinato. Gli abbracci diventavano carezze.
Avevo tutta la strada per dire buona notte ma non volevo lasciare andare
la mano che avevo sul culo. Entravo nel suo bagno lurido da ex studente.
Affrontavo i peli sparpagliati qua e là. Volevo essere pulita: la mia
figa ha un odore che non mi piace, la bici la schiaccia, la sento
strana, le cosce la soffocano, voglio controllarla. Ogni ingranaggio
sotto controllo. Mi misi a cercare uno dei prodotti per pulire il bidè,
una passata veloce per potermi guardare annusare con calma. Non c'era il
detergente intimo, respirai e abbondai con l'acqua poi rimisi tutto in
ordine. Lavai le mani e tornai da lui.*

*Avevo la sensazione come se stessimo per buttarci insieme da un
precipizio. Ridevo e mi facevo mordere. Mi strizzava le cosce. E poi con
forza mi mise a sedere sulla sua scrivania. Si fermò a guardarmi e mi
disse «ok?» fissandomi come se mi volesse divorare.*

*Ero talmente eccitata che tremavo.*

*«ok»*

*Una scopata liscia dopo l'altra. Tra le mani la lingua e il sudore di
René abbandonavo dubbi e ansie.*

*Avevo i capelli sporchi eppure me li facevo tirare toccare mordere.
Prima di farmi scopare da dietro mi metteva una mano sulla schiena e
tira e tira come se dovesse penetrarmi da un momento all'altro ma
attendeva. Ancora un po'. Mi portava la testa sul cuscino, mi prendeva i
polsi li avvicinava e li stringeva. E poi mi scopava. Non avevo niente
da perdere dovevo solo lasciarmi andare.*

Tutto tace e chi va in bici si rassegna a essere sempre più isolata. Ma
un giorno terribile segna me e quei pochi scappati di casa che erano
rimasti fuori dalle mura. L'ennesimo tassista dalla guida scellerata ha
fatto danni. Questa volta ha ucciso una donna in bicicletta, un'amica di
Gina, un po' più giovane di lei. I giornali parlano del tassista come di
un povero servitore della città che ha subito un grosso trauma. Lei
viene descritta come un tantino folle vestita male e un po' svampita.
Perché non era a casa? Che aveva da fare una donna in giro a quell'ora?
E perché in bici? Allora è vero sono pericolose e quindi è meglio usarle
il meno possibile. Lo sconcerto accende la rabbia. Nel giro di una
settimana la ciclofficina di Gina sforna biciclette e cargo di ogni
taglia. Ci mettiamo sopra qualsiasi cosa. Siamo tante. Pezzo dopo pezzo
con attenzione e metodo costruiamo il nostro esercito. Ogni tanto Gina
piange e quando pensa di essere sola urla un po'. Nessuno sa come
aiutarla se non stare attente il più possibile con l'ordine degli
attrezzi, dedicare tutto il tempo libero lì dentro e portarle del cibo
già pronto: odiava cucinare. Mangiare insieme nei momenti di pausa è
essenziale come un freno ben fatto, una camera d'aria riparata bene, un
movimento centrale funzionante, pulire tutto prima di andar via.

*Io e René mangiavamo tantissimo insieme. Scopavamo e mangiavamo, lui
cucinava io assaggiavo. Lui chiedeva io eseguivo. Non era fame né
appetito, era più un inghiottirsi. Mi sfamava. Capitava che non
rispettavamo i tempi dei pasti.*

*Mi ficcò due dita in gola piene di miele. Mi soffocava mentre mi misi a
sedere sul suo cazzo, un cazzo normalissimo ma che io trovavo stupendo.
La mia figa aveva un trono stupendo.*

*Quando il miele mi scivolò dal mento e arrivò sul letto, mi alzai, e mi
inginocchiai per succhiarlo.*

*Con una cinghia al collo che tirava mi potevo considerare sazia.*

*Mi sentivo mangiare quando mi mordeva il culo le cosce la figa.*

*«Oggi sto io dietro» mi disse una volta e si fece leccare le palle e
tutto attorno e anche un po' il culo. Mi tornava in mente il suo sapone
delle mani. Mi sentivo potente.*

*Gli schiaffi, gli orgasmi, le patatine fritte, le leccate, le lasagne
riscaldate, i lividi, i panini strapieni, le parole sussurrate:
costruivamo la nostra intimità.*

Iniziamo a girare in gruppo almeno una volta al giorno a orari diversi.
Iniziamo finalmente a essere tante, veloci, attente. Cambiamo orario di
incursione ogni giorno. È impossibile prenderci. I vigili urbani si
ostinano a seguirci in macchina: scemi. La zona universitaria sembra
bombardata dopo il nostro passaggio. In Via delle Moline, via del Borgo
e via Mascarella non c'è un tavolino in piedi. Un cimitero di aperitivi.
Arriviamo coloratissime, le luci, le decorazioni, le musiche da
carnevale ci guardano tutti felici per qualche minuto. Ci scambiano per
l'ennesima attrazione del comune. Degli artisti di strada con biciclette
stravaganti e colorate: in pochi minuti l'espressione nelle loro facce
cambiano sensibilmente. In via D'Azeglio e in via Farini con le
biciclette entriamo direttamente nei negozi. Sotto la questura
danneggiamo tutte le macchine in zona. Rubiamo tutto, mangiamo tutto.
Assaltiamo i bus turistici: li circondiamo. Qualcuna entra su e spiega
loro che è una performance artistica compresa nel loro pacchetto
vacanze. Si creano code immense. I clacson ci sfondano le orecchie. Non
vogliamo spaventare ma far fermare tutti per un po'. Ogni tanto sparisce
qualche reflex, qualche giacca, qualche portafoglio a qualche turista,
ma nessuna moralizza. Una sera abbiamo deciso di puntare solo sulle
macchine che ostacolano i passaggi delle biciclette e delle sedie a
rotelle. Un suv ha preso fuoco. Ero talmente eccitata che tremavo. Sul
pincio della montagnola è apparso un gigantesco striscione: hai voluto
il suv adesso pedala. Avevamo perso, vincevamo il nostro giro d'Italia.

*René mi sorprendeva e io mi sorprendevo di poter gestire le nostre
notti e le nostre giornate fuori dal letto quando capitava di
incontrarci. Il fatto che avesse un atteggiamento diverso da quel che
faceva apparire mi eccitava sempre più. Ci vedevamo fuori solo per caso
e quando uscivamo insieme andavamo in posti poco frequentati, magari a
qualche festa affollata per metterci poi appena fuori dal locale, in un
posto fintamente imboscato, per farci sgamare*[^1] *da qualcuno mentre
scopavamo. Ci penso e ripenso e forse domani non dovrei dirgli di
smettere di vederci. Quante inutili paranoie. Dovrei affrontare le cose
così come vengono. A volte gli imprevisti capitano e sono piacevoli. A
volte capita di lasciare il kit d'emergenza per le riparazioni a casa.
Come la sera in cui io e René c'eravamo parlati più vicino del solito.
Il laboratorio dei bimbi del pomeriggio aveva seminato puntine ovunque,
puntine impercettibili, fastidiose, infernali. Si era fatto tardi, avevo
bevuto, e avevo bisogno delle mie pedalate, del mio rettilineo, di
sentire la figa che strusciava sul sellino per poi buttarmi sul mio
letto. Ma inaspettatamente tutte e due le mie ruote erano sgonfie bucate
sconfitte. Un imprevisto.*

{{%ligneblanche%}}

{{%epigraphe%}}

«Beh dai capita! Abito qui vicino se vuoi puoi riposarti un po' da me.»

(testo apparso anche su «LUNGIDAME. Zine aperiodica libera e
accidentalmente punk», n. 4, 2020, pp. 82-85)

{{%/epigraphe%}}

[^1]: Scoprire, ndr.
