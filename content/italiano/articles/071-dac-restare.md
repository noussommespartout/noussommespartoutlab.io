---
author: "D(i)ritti alla Città (Rete per gli spazi pubblici dismessi)"
title: "Restare"
subtitle: "E resistere"
info: "Poesia scritta per la raccolta bolognese"
datepub: "Aprile 2022"
categories: ["spazi, occupazione, diritto all'abitare"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "71"
quote: "Discorsi. Scritte sui muri. Chiacchiere."
---


Questo era prima, 2020, il deserto dopo l'ultimo sgombero.

Mi guardo intorno. Leggo. Ascolto.

Discorsi. Scritte sui muri. Chiacchiere.

«La città si dà in pasto al capitale! Quartieri sventrati da
speculazioni e caro affitto! Il centro svenduto all'economia foodie[^1]!
Il tempo rubato! L'assenza di sogni!»

(--- Un sansimone!)

Chi ha comprato il diritto alla città?

(--- Un frangelico!)

Siamo monadi, distinte solitudini.

Ridotti ad automi con potere di acquisto.

Privi di spazi di libertà annaspiamo in questo labirinto di divieti e
sanzioni.

(--- Un frangelico!)

Ridotti a darci appuntamento nei bar, quelli dove almeno possiamo
scegliere la colonna sonora, basta un click:

«No, non ora non qui, in questa pingue immane frana».

Ricordiamo con nostalgia. Nostalgia impotente, nostalgia canaglia.

Le piazze di una volta, gli spazi di una volta, le lotte di una volta...
quelle di 5 anni fa, di 20 anni fa, di 40 anni fa.

C'era un prima, in cui vivere di notte non era scansare siringhe o
essere travolti dai lavastrada.

La notte passata accasciati sui marciapiedi, in cerchio senza quasi
parlare, ad aspettare il mattino. Oltrepassare il limite della notte,
anche questa libertà ci è rimasta. Indossiamo le stesse scarpe (nere),
gli stessi giubbotti (neri), lo stesso colore (nero). Una moda che finge
di essere una comunità.

Di comune non è rimasto nulla. Al massimo è il nome che diamo al nemico.

«Esistono ancora i beni comuni?»

Alla luce del giorno si svelano le occhiaie e i supermercati con le loro
vetrine lucide di trasparente obbedienza e l'apertura h24. E la domenica
mattina, con un po' di vergogna salutiamo gli ex compagni davanti alle
paste alla crema. Un euro fasullo per un sorriso al glucosio.

«Lipsia è la nuova Berlino».

«I compagni in galera».

«Hai visto hanno costruito un nuovo studentato».

«C'è un posto in Bolognina in cui si vende ancora il tabacco a 5,70».

Certo che nelle piazze le chitarre vanno ancora di moda. Non sanno che
saranno silenziate tra poco.

E le divise si portano a passeggio con il solito ritmo arrogante.

«Ci hanno forse chiesto se volevamo essere protetti?»

Non incontro sguardi complici. Ognuno pare sfuggire a se stesso. Mi son
riconosciuto in qualche sguardo atopico, straniero alla colonizzazione
dello spirito, malinconico di un'alterità scomparsa. Chissà se mi hai
riconosciuto come tuo complice in questa battaglia contro il #realismocapitalista. Siamo cosi saturi di questo reale postmoderno
senza poesia. Orfani di una visione.

«Ognuno sceglie la propria forma di compromesso, sacrificio o
espiazione».

Questo era dopo, 2022, districarsi tra le rovine.

Alcuni sono fuggiti dalla città per creare narrazioni altre e avere un
po' di respiro.

Alcuni sono fuggiti a loro stessi per deresponsabilizzarsi
dall'infelicità.

Alcuni sono rimasti.

D(i)ritti alla Città mi ha dato un motivo per restare. E resistere.

Per tirarci fuori da questo deserto non possiamo essere realisti né
nostalgici. Serve ritornare alla valvola del desiderio e cercarci nel
sogno collettivo di un presente prossimo.

Ma il «presente prossimo», proprio perché vicino, si costruisce oggi:
supportati da una visione liberatoria (chiamala, se vuoi, utopia), ci
tocca affrontare il reale con denti aguzzi e spalle coperte, con le mani
impegnate e la testa pensante. Insieme.

Ed è un ultimo sussulto di assalto al cielo che ci spinge a una massa
critica e festosa --- oltre i cancelli, nelle strade, negli spazi
liberati, nell'infosfera.

Diceva il poeta: «cercare e saper riconoscere chi e cosa, in mezzo
all'inferno, non è inferno, e farlo durare, e dargli spazio».

[^1]: Aggettivo inglese, da «food» (cibo). La gastronomia è uno degli
    aspetti su cui la promozione territoriale della città di Bologna ha
    puntato negli ultimi anni, a fini turistici, quasi sempre con
    l'utilizzo di anglicismi, ndr.
