---
author: "Craaazi (Centro di ricerca e archivio autonomo transfemministaqueer° “Alessandro Zijno”)"
title: "Dichiarazione di indipendenza della popola delle terre storte"
subtitle: "Una scheggia a controtempo"
info: "Testo riportato in vita per la raccolta bolognese"
datepub: "Maggio 2022"
categories: ["transfemminismoqueer, queer", "corpo, salute, cura", "anticapitalismo", "ecofemminismi"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "70"
quote: "corrono tempi bui"
---

*Rimettere in circolazione questo testo è una mossa che si iscrive
perfettamente nell'idea di ricerca e valorizzazione di saperi autonomi
di Craaazi. L'archivio è qui inteso come un processo di risignificazione
di percorsi che si cristallizzano in un oggetto --- in questo caso il
comunicato del Sommovimento NazioAnale --- che irrompe nel presente e si
posiziona a controtempo, sfidando il nostro modo di posizionarci
rispetto al passato, all'autorialità, all'idea stessa di originalità. I
pur labili confini della nostra «ricerca» ne escono provati, subiscono
una tensione e si dilatano arricchendosi di un elemento inaspettato:
inedito proprio in virtù del suo essere noto. Affermare e afferrare il
groviglio limpidissimo di imprevisti che queste righe pongono al nostro
sguardo è il motivo per cui abbiamo voluto offrire loro un ennesimo
supporto di cristallizzazione, come parola che non arresta la sua corsa
e riecheggia ovunque.* \[Ndr\]

## Dichiarazione d'indipendenza della popola delle terre storte

*Corrono tempi bui. Grigi signori in piedi con libri in mano, schierati
a scacchiera nelle piazze, predicano di correre subito ai ripari perché
imminente è la disfatta dell'ordine (v)eterosessuale e la vittoria
dell'Internazionale Frocialista. E hanno ragione. Senonché di ripari,
non ve n'è alcuno!*

*Corrono tempi bui. Mesi di travagliate discussioni affinché i
parlamentari decretassero che le froce° si possono unire in coppie
docili e mansuete, senza pargoli da allevare. Ma ben prima della loro
autorizzazione, abbiamo costruito e viviamo reti d'affetto multiple,
fatte di amiche, compagn@, fratelle°, sorelli°, bambin\*, amanti.*

*Corrono tempi bui. Società sessiste e eteropatriarcali° si scoprono
paladine della libertà femminile solo quando serve per mostrificare i
musulmani e militarizzare le città. Ma la lotta delle donne contro la
violenza maschile è da sempre autorganizzata°. Femministe, migranti e
froce di tutti i colori già sfilano insieme per distruggere i confini e
per il transito illimitato tra i generi e i territori.*

*Corrono tempi bui. Vi sono luoghi di lavoro in cui ci dobbiamo fingere
eterosessuali, altri in cui siamo obbligate a regalare la nostra
eccentricità all'azienda, confezionandola secondo i desideri
dell'ufficio marketing. E anche se il glamour gay, lo chic lesbo, il
look underground fanno aumentare i loro profitti, misera è la nostra
paga e precaria la nostra vita. Ora basta! Mentre si prepara la fucsia
primavera, se proprio dobbiamo venderci, saremo noi a stabilire il
prezzo e il modo.*

*Froce incivili, creative esaurite, camioniste fuori moda, vecchie
checche senza contributi, trans\* euforiche/i/u, massaie critiche,
butch° insolventi, puttane inflazionate, nonne ribelli, precarie messe
al bando, ci siamo unit\* e proclamiamo al mondo la*

{{%centre%}}

Dichiarazione di indipendenza della popola delle terre storte

{{%/centre%}}

Siamo finocchie selvatiche, femministe in erba, trans in fiore, genuine
e clandestine: creiamo genealogie e parentele oltre le specie. Siamo
trans-ecologiste e resistiamo alla radioattività della famiglia nucleare
sperimentando forme sovversive di affetto, piacere, solidarietà,
relazione. Siamo le guerrigliere della lotta anale contro il capitale.

Sottraiamo la nostra creatività ai brand della moda. La contessa di
AccaEmme, la regina di Kos, da oggi si vestiranno da sole. Designer e
parrucchiere, stiliste e commessi, allestiamo apparati effimeri per il
funerale dell'eterosessualità obbligatoria.

Lesbiche virtuose del fai-da-te, non maneggiamo più trapani, seghe,
martelli per vendere le merci del Re Merlin, ma li usiamo per costruire
spazi liberati dallo sfruttamento e dalla competizione neoliberista.

Ci siamo già infiltrate nelle redazioni dei giornali femminili, delle
radio commerciali, della televisione nazionalpopolare: interrompiamo la
trasmissione dei ruoli sessuali e la programmazione delle nuove identità
preconfezionate, produciamo format di sovversione.

Con i poteri che ci siamo date, aboliamo il culto
dell'autoimprenditorialità e l'obbligo di trasformare tutto ciò che
siamo e facciamo in qualcosa di spendibile sul mercato del lavoro.
Startuppami 'sta fregna!

Con le briciole di riconoscimento concesse dall'azienda e dalle
politiche antidiscriminatorie ci facciamo i biscottini. Abbiamo comunque
deciso di prenderci tutta la pasticceria.

Parliamo noi per noi stes(s)e e ci autoriconosciamo, le une con gli
altri/e/u.

Sottraiamo per sempre i nostri saperi e quelli prodotti su di noi
all'Accademia del Capitale, per restituirli alla libera circolazione.
Non saremo più un caso di studio, perché le nostre vite eccedono
qualunque teoria: autogeneriamo conoscenza su di noi, animali umani e
non umani, e sul mondo.

Ci riappropriamo in forma collettiva e autogestita° dei nostri corpi,
della loro capacità di godere, di creare, di trasformarsi.

Nelle consultorie transfemministefroce, decostruiamo e ri-costruiamo i
nostri corpi con tutte le protesi fisiche e chimiche che desideriamo,
reinventiamo i canoni estetici, i piaceri, il concetto di salute e
sovvertiamo le pratiche della cura.

Lavorare stanca: nella fucsia primavera proclamiamo l'abolizione del
ricatto del lavoro.

Istituiamo un piano queerquennale che prevede casa, luce, acqua, rose,
gardenie e fiori di lotta perpetua per tutti, tutte e tette.

Siamo stufe di stare in appartamenti cari e brutti: ci riprendiamo
basiliche, ville, condomini sfitti e castelli per tutti, tuttu e tutte!
A ciascuna, ciascuno e ciascunu secondo i suoi bisogni, i suoi desideri,
le sue fantasie.

Proclamiamo l'inizio della de-civilizzazione. Rifiutiamo la logica che
divide le culture in «avanzate» e «arretrate» con la scusa dei «diritti»
delle donne o delle cosiddette «minoranze» sessuali. Sostituiamo
l'avanzata rettilinea del Progresso con percorsi obliqui, grovigli,
passi di danza, vagabondaggi.

Ci prendiamo tutto lo spazio che ci serve. I pompieri sugli alberi miao,
gli sgomberi ciao.

Noi, Popola delle Terre Storte, irrompiamo nello spazio pubblico oltre
le forme autorizzate del vivere.

Siamo uscite/i/u dalle dark room, dalle palestre, dai ritiri in
campagna, debordiamo dagli spazi autogestiti° sgomberati, dalle strade e
dai marciapiedi, dai luoghi perimetrati dove volevate ghettizzarci.
Convergiamo in spazi comuni in continua espansione. Contaminiamo ogni
luogo con la nostra favolosità: ogni via, ogni strada, ogni angolo ci
serve per ridisegnare le geografie dei desideri e dei piaceri. Chi ci
voleva a casa a spolverare i mobili, ci ha trovato in strada a
polverizzare i ruoli di genere.

Siamo l'imprevisto nell'ingranaggio del capitale. Venite e godete con
noi!

{{%epigraphe%}}

Sommovimento NazioAnale

{{%/epigraphe%}}
