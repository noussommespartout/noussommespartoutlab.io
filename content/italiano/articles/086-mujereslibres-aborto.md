---
author: "Mujeres libres Bologna"
title: "«L'aborto non è un tema»"
subtitle: "Chiacchierata libera con due militanti di un collettivo transfemminista"
info: "Intervista trascritta e rielaborata per la raccolta bolognese"
datepub: "Luglio 2022"
categories: ["autogestione, autorganizzazione", "manifestazione, rivolta, protesta", "solidarietà, mutualismo", "diritti riproduttivi, aborto", "femminismi", "transfemminismoqueer, queer"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "86"
quote: "davanti a noi si apre un mondo"
---

Ogni volta che ci viene chiesto di raccontare una pratica femminista,
non possiamo che constatare che davanti a noi si apre un mondo. Se
parliamo di femminismo, infatti, tutto è un po' pratica, anche se ci
sono sicuramente azioni più conflittuali di altre. L'esempio che abbiamo
in mente è il contrasto alle/ai militanti No gender, o più in generale
anti-scelta, che in un certo senso contraddistingue il nostro
collettivo, perché su questo abbiamo fatto delle azioni simbolicamente
molto forti, con ottimi risultati. Ma prima di raccontare la nostra
esperienza a riguardo, per noi è importante sottolineare un fatto: a
caratterizzarci è soprattutto una modalità particolare, che cerca di
conciliare la dimensione interna, cioè un metodo decisionale basato
sull'orizzontalità, e quella esterna, fatta appunto di azioni,
comunicati, discorso pubblico e apertura verso chi non fa parte dei giri
militanti.

Ad ogni modo, dal punto di vista delle pratiche, va detto che con il
lockdown dovuto alla prima ondata di Covid-19 sono subentrati degli
altri problemi, che ci hanno un po' distolto dalla questione degli
antiabortisti. Pur cercando di rimanere sul pezzo, abbiamo deciso di
fare anche altro e non sempre riusciamo a fare tutto.

Ma andiamo con ordine, partiamo da un esempio recente.

## #TiSupportoLAborto

In piena pandemia ci siamo concentrate sul problema dell'accesso
all'aborto nelle strutture sanitarie, che è stato fortemente messo a
rischio con il lockdown. Abbiamo sentito l'esigenza di fare uno
sportello di supporto per le donne che avevano bisogno di abortire.
All'inizio della pandemia facevamo queste lunghe assemblee online che ci
hanno veramente tagliato le gambe, eravamo tutte un po' prese male dal
fatto di non riuscire a fare niente, noi che siamo un collettivo molto
«pratico»... e a un tratto ci chiediamo: ma come siam messe sull'accesso
all'aborto con le strutture ospedaliere riconvertite in centri Covid?
Così decidiamo di sondare il terreno, anche perché è un'attività che
facciamo regolarmente per l'aggiornamento annuale delle nostra guida
all'IVG[^1], e ci rendiamo conto che l'accesso all'aborto è fortemente
ridotto.

Non solo, ci rendiamo conto anche che la pandemia fa cambiare
continuamente i servizi e che è di fatto impossibile pubblicare una
guida che non perda di validità da un giorno all'altro. Viene così
l'idea di metterci a disposizione, perché è vero che dappertutto ci sono
i nostri contatti, ma è tutta un'altra storia quando si dice
chiaramente: «se avete bisogno chiamateci, scriveteci una mail, un
messaggio sui social e noi vi rispondiamo». Non a caso, in pochissimo
tempo arrivano le prime richieste e noi non ci limitiamo a dare le
informazioni di base --- «vai in farmacia, poi in ospedale, poi chiama il
consultorio...» --- ma diciamo: «se hai bisogno di una voce amica che ti
supporta noi ci siamo!».

Inizia così tutta un'altra pratica, molto più coinvolgente e impegnativa
rispetto alla guida, nel senso che ci mettiamo a disposizione come
compagne, sorelle femministe, per assistere le donne che hanno bisogno
di abortire. Si delineano quindi due piani, che noi consideriamo
complementari: quello autenticamente politico, sul significato
dell'accesso all'IVG, e quello personale, perché s'instaura un rapporto
privato e intimo. Un rapporto di complicità politica, anche, che
dimostra fino a che punto questi due piani sono intrecciati. Una delle
prime persone intercettate col supporto, infatti, è poi diventata una
compagna attiva del collettivo. E questa è una delle cose più belle che
ci sono successe da quando abbiamo avviato lo sportello.

Per entrare un po' più nel dettaglio, si tratta di un supporto che si
protrae per tutto l'iter dell'aborto, quindi due, a volte anche tre
settimane e più, perché a distanza di qualche tempo ci si risente per
sapere se va tutto bene. È qui che si instaurano dei rapporti profondi,
si entra nell'intimità, anche perché parliamo di un rapporto a due:
presi i primi contatti col collettivo, infatti, c'è sempre una compagna
che si fa carico di seguire personalmente la questione. Ovviamente
tenendo ben presente due aspetti. Il primo è che noi non siamo personale
medico specializzato, anzi siamo in autoformazione continua e ci
muoviamo con grande umiltà. Il nostro ruolo è innanzitutto dare una mano
a reperire le informazioni di accesso al Sistema sanitario e poi si fa
quel che si può: due chiacchiere in caso di dubbi, accompagnamento in
ospedale se richiesto o magari un lavoro di collegamento quando chi ci
contatta abita in un posto dove accedere ai servizi è più complicato. Il
secondo aspetto è che alle spalle della compagna che si «accolla» il
supporto c'è tutto il collettivo e per ogni dubbio o esitazione ci si
confronta velocemente per trovare una soluzione.

Si capisce quindi che la cosa può diventare anche molto impegnativa,
perché il supporto può essere una semplice richiesta di informazioni che
si risolve con una mail, ma può essere anche più lungo e, a volte, più
complicato per le ragioni più diverse. Come ad esempio la lingua, anche
se abbiamo compagne che parlano spagnolo, francese e inglese e quindi
siamo sempre state molto fortunate. Oppure capita di avere a che fare
con domande molto tecniche sull'operazione chirurgica, di fronte alle
quali la nostra tattica è quella di fare un passo indietro, perché su
questi aspetti noi non possiamo dire molto e perciò cerchiamo sempre di
spostare il supporto sull'aspetto emotivo. Che poi è quello di cui c'è
davvero bisogno: le donne che scelgono di abortire necessitano di
sentirsi appoggiate e rassicurate in questa scelta. Ma attenzione, la
questione della scelta per noi è cruciale e su questo cerchiamo di
essere sempre molto chiare: il nostro scopo non è portare in ospedale
chiunque si rivolga a noi! Tant'è vero che uno dei primissimi supporti
che abbiamo avuto in piena pandemia è stata una ragazza che alla fine ha
deciso di tenere il bambino.

In generale abbiamo avuto un ottimo riscontro, finito l'iter tutte le
ragazze ci mandano dei ringraziamenti commossi, e non è raro che alcune
diventino attiviste, non necessariamente delle Mujeres libres, ma di uno
dei gruppi della galassia femminista. Insomma, ci sembra di essere utili
in qualche maniera... è una pratica che ci dà grandi soddisfazioni come
collettivo ed è per questo che cerchiamo anche di valorizzare queste
esperienze. Lo facciamo ad esempio con la campagna «Abortisco e non mi
pento», che poi tempo fa si è materializzata anche in una fanzine
scaricabile dal nostro blog.

Il punto è dare voce alle persone che hanno abortito e stanno bene, che
si svincolano dalla narrazione mainstream che vuole le donne colpevoli,
assassine e tristi. Insomma, interveniamo sullo stigma alimentato dal
senso di colpa, grazie a chi ha vissuto l'aborto come momento di
liberazione e non come qualcosa di negativo. Non da ultimo, questo tipo
di azioni ci permette di uscire dalle «bolle militanti» --- perché
appunto l'aborto riguarda tutte, compagne e non --- e di tenere vivo un
dibattito all'interno del collettivo, di ragionare politicamente su cosa
significa fare un tipo di sorellanza anche con persone non
immediatamente vicine a noi. Nel caso specifico, appunto, il supporto
all'aborto si è inserito nel quadro di risposta dei movimenti ai
cambiamenti imposti dalla situazione pandemica: una risposta che noi
abbiamo voluto strutturare attorno al mutualismo, al mutuo appoggio, che
per alcune di noi provenienti dall'esperienza anarchica è molto
importante, mentre per molte compagne più giovani ha rappresentato la
possibilità di fare un affondo di concretezza, di dare corpo agli slogan
su cui si tengono le grandi piazze.

## Dieci, quindici compagne in sintonia fra loro...

Se parliamo di piazze e di fughe dalle bolle militanti, non possiamo non
citare un'esperienza che abbiamo messo in piedi nel 2015 ed è tuttora
tra le attività più interessanti che abbiamo fatto. Diciamo pure che è
fra le azioni che ci ha portato i risultati più importanti, anche se
allora eravamo ancora un collettivo molto piccolo: dieci, quindici
compagne in sintonia tra loro che non avevano neanche lontanamente le
risorse materiali per permettersi di fare azioni importanti.

Aborto e obiezione di coscienza sono sempre state tra i nostri
principali temi d'intervento. È per questo che ci siamo subito attivate
quando abbiamo scoperto che, all'ingresso del Reparto di ginecologia
dell'ospedale S. Orsola, ogni martedì fra le 7 e le 8 del mattino si
svolgevano delle preghiere «per la vita», ovvero contro l'aborto e
contro le donne che abortiscono. Fra stendardi e dépliant, la Giovanni
XXIII --- un'associazione cattolica attiva anche in campo sociale ---
diceva di voler «aiutare» le donne che vogliono abortire a intraprendere
la strada della gravidanza... un messaggio estremamente ambiguo, in un
momento delicatissimo della biografia di una donna: si può solo
immaginare cosa significhi affrontare un gruppo di fondamentalisti
ultra-cattolici se si sta andando ad abortire. Gente per altro un po'
crudele, va detto, che fa perno sullo stigma e sul senso di colpa, come
al solito molto diffusi nella cultura nostrana. Non ci stancheremo mai
di ripeterlo: anche se l'ultimo obiettore di coscienza scomparisse, ci
sarebbe ancora bisogno di continuare la lotta contro lo stigma e il
senso di colpa.

Mettere così in soggezione queste donne a noi è sembrato subito un fatto
molto grave, sia come attacco alla singola donna, sia per la sua
rilevanza pubblica e politica. Perché già allora si diceva che Bologna
era una città all'avanguardia, l'erede del modello emiliano --- oggi si
dice addirittura che sia la più progressista d'Italia... --- ma in realtà
sappiamo bene che anche a Bologna c'è molta obiezione. Con tutte le
nostre difficoltà, ma anche con un grande entusiasmo, ci siamo dette che
una schifezza del genere non era tollerabile.

Essendo un collettivo femminista autorganizzato, abbiamo riflettuto
molto su come dare una risposta che fosse al contempo efficace senza far
sentire nessuna esclusa. Così abbiamo deciso di presentarci davanti a
Ginecologia, dove c'è il parcheggio della Coop di via Massarenti, alle
6:45 di un giorno feriale, quindi prima di iniziare i nostri lavori... e
va detto che anche allora eravamo tutte più o meno precarie, in parte
studentesse, più o meno con le nostre vite e i nostri casini che dalle 9
in poi bussavano alla porta.

Avevamo bisogno di alleate, è ovvio. E così abbiamo diffuso una
chiamata, a cui hanno risposto alcun\* compagn\* del Circolo anarchico
Berneri, ma soprattutto la Sambalotta, che è una delle cose più
fastidiose che si può avere in un corteo: un gruppo di percussionist\*
in stile clown army. Faccio un inciso personale: al tempo io questa
esperienza non la reggevo, perché veramente ai cortei era una rottura
infinita. Ma in realtà col senno di poi devo ammettere che centravano in
pieno il loro obiettivo: abbassare i momenti di tensione in piazza,
senza contare il fatto che quando qualcun\* veniva bevut\*,[^2] loro
c'erano... cosa che io allora mi ero un po' dimenticata. Meglio così!

La Sambalotta cascava a pennello, perché noi dovevamo dare fastidio agli
antiabortisti in un modo che fosse meno attaccabile possibile. Cioè non
potevamo andare a prenderli a schiaffi e a calci, come avremmo voluto,
perché comunque la repressione in Italia non è uno scherzo. Avevamo
fatto le nostre valutazioni o forse, già allora, pensavamo che la
mobilitazione aveva bisogno di allargarsi. Così abbiamo scelto questa
contestazione rumorosa e fastidiosa, piena di cori, uno striscione e una
serie di comunicati infuocati. Avevamo addirittura scritto delle canzoni
in rima per fare un'ora di frastuono allietato da un leggero sottofondo
di tamburi.

Nonostante la buona dose di impreparazione che ci caratterizzava, ci
siamo riuscite. Ricordo sempre che una delle prima volte eravamo
talmente inesperte che siamo arrivate con poco materiale, lo striscione
scritto male e, addirittura, arrivando vedo due compagne appoggiate alla
macchina della Digos[^3] senza saperlo. Ma ci è servito a farci le ossa
e può servire magari anche a spronare altre. Anche se si è in un paese
piccolo, anche se si è in poche, io mi sento dire: provateci perché noi
quella volta ci abbiamo provato e la cosa è riuscita, è subito diventata
grande. Anche più grande di quello che eravamo in grado di gestire, se
vogliamo essere oneste, ma in realtà ce la siamo cavata benissimo.
Abbiamo addirittura fatto irruzione nel Consiglio comunale della città e
lì abbiamo capito che, di fatto, questa azione aveva aperto un dibattito
in città. Al di là dei tanti messaggi di solidarietà ricevuti, i
fondamentalisti ci fecero una figura terribile e, in poco meno di due
mesi, abbiamo portato a casa il risultato. Nel senso che loro erano
spariti dal S. Orsola, almeno in parte, e nel frattempo la mobilitazione
aveva guadagnato un'ampiezza straordinaria: donne, persone queer, gente
lontana dai movimenti, anche un po' dei compagni del giro misto (che
devo dire hanno sempre visto le questioni sull'aborto come una delle
cose di cui si occupano le ragazze...).

Dicevo almeno in parte perché, poco dopo, abbiamo scoperto che la
Giovanni XXIII aveva ripreso a incontrarsi con la scusa di pregare per i
«bambini mai nati», ma con numeri decisamente più contenuti. Il loro
obiettivo era sempre lo stesso: fare pressione alle donne che passano di
lì, perché altrimenti le preghiere le potresti fare tranquillamente
nella tua stanzetta, oppure in chiesa! E invece no, loro le fanno lì per
continuare ad avere una certa visibilità. Ma almeno adesso lo fanno in
una laterale, che è via Bertoni, e sono comunque molto meno visibili di
prima. È per questo che, durante la settimana transfemminista° del
luglio 2021, con Non Una di Meno siamo tornate in via Bertoni e abbiamo
rianimato una bella contestazione, anche questa volta molto partecipata,
contro le preghiere antiabortiste. Così come, qualche tempo dopo,
insieme ad altre realtà antifasciste siamo andate a gridare contro il
Comitato no 194 --- il riferimento, chiaro e liscio come l'olio, è alla
legge 194 che istituì l'aborto in Italia --- che animava un presidio in
Piazza San Giovanni in Monte.

Quando abbiamo deciso di smettere di presentarci al S. Orsola tutti i
martedì, lo abbiamo fatto perché consideravamo che il nostro contributo
l'avevamo dato e soprattutto non volevamo entrare in una dinamica di
scontro. Questo non vuol dire, però, che abbassiamo la guardia. Anzi, il
lavoro di monitoraggio e analisi sulla galassia No gender è qualcosa che
facevamo prima e che non abbiamo mai interrotto. Perché all'interno di
quel mondo di neofascisti e destre estreme si muove tutto un groviglio
di interessi, denaro, anche con forti legami internazionali, che ha
ormai preso di petto le questioni di genere e i diritti riproduttivi. In
fondo, Giorgia Meloni è esattamente questo. Se vuoi, la Meloni è un po'
il «sogno No gender», perché la destra oggi non dice più che la donna
deve stare a casa a lavare i piatti, ma dice che la famiglia deve
aderire a una norma precisa, così come i diritti civili, ecc.

Il problema è che ormai sono al potere, ecco, però noi non ci perdiamo
d'animo. Perché, in fondo, l'obiettivo ci è molto chiaro. Per noi queste
nostre pratiche, dalle contestazioni al supporto all'aborto, non si
esauriscono in se stesse, ma si inscrivono in un quadro conflittuale. Il
sistema in cui siamo non ci piace e vogliamo cambiarlo, con i nostri
mille tentativi e con mille difetti. Perché dietro c'è sempre l'idea di
trasformare completamente la realtà e le mentalità, cosa che passa anche
attraverso l'abbattimento dell'obiezione di coscienza. Insomma, per noi
l'obiettivo è sempre lo stesso: ribaltare il tavolo invece di
abbellirlo!

[^1]: Interruzione volontaria di gravidanza, ndr. Al momento di
    trascrivere questa intervista, l'ultima versione della guida è
    disponibile su:
    [https://mujeres-libres-bologna.noblogs.org/files/2022/03/guida-ivg-20221.pdf](https://mujeres-libres-bologna.noblogs.org/files/2022/03/guida-ivg-20221.pdf).

[^2]: Tratt\* in arresto o fermo dalla polizia, ndr.

[^3]: La Divisione investigazioni generali e operazioni speciali è una
    divisione della polizia italiana creata nel 1978 con compiti di
    controllo di manifestazioni pubbliche e movimenti politici, a cui si
    sono poi aggiunte tifoserie e terrorismo.
