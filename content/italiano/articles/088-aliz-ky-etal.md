---
author: "Alyz_Ki, Kinda, Andy Gio, Supersoft Sound dj@s, Elettra Irregolare, Trippy"
title: "Collettiva Elettronika"
subtitle: ""
info: "Testo scritto per la raccolta bolognese"
datepub: "Novembre 2022"
categories: ["autogestione, autorganizzazione", "scuola,  università, student*", "spazi, occupazione, diritto all'abitare"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "88"
quote: "un posto ai margini, dove tutto accade e dove tutto si trasforma"
---

{{%epigraphe%}}

«Il clubbing può configurarsi come spazio di resistenza quando permette
di realizzare un'immersione totalizzante in un nuovo orizzonte
sensoriale e di senso, opposto alla docilità dei corpi, alla
standardizzazione delle condotte e alla staticità delle identità del
regime disciplinare»

(Enrico Petrilli, *Notte tossiche*, Meltemi, 2020)

{{%/epigraphe%}}

La Collettiva Elettronika si forma nel 2017 a Bologna da un incontro tra
amiche dj e nasce da un'idea musicale transfemminista° e queer°. Siamo
unite dall'attivismo e dal desiderio di creare serate safe, macho free
zone, gender fluid. Il nome declinato al femminile nasce dal desiderio
di rivendicare e dare visibilità alla presenza di donne e persone trans
dj sempre più presenti alle consolle dei club.

La Collettiva Elettronika è composta da: Alyz_Ki, Kinda, Andy Gio,
Supersoft Sound dj@s, Elettra Irregolare, Trippy. Crediamo nella
collaborazione, nel creare rete e connessioni e invitiamo spesso altre
dj nelle nostre serate. Dal 2018 ha preso vita *Where is the cue*,
laboratorio per aspiranti djs su come mixare e sperimentarsi
nell'attività di djing.

La Collettiva Elettronika attraversa spazi e luoghi, libera corpi e si
libera essa stessa dai luoghi di commercializzazione del piacere
riservandosi un posto ai margini, dove tutto accade e dove tutto si
trasforma. La Collettiva porta la politica nel clubbing e il clubbing
nella politica, le casse che suonano sono luoghi di risonanza delle
nostre battaglie, ululato primordiale che rivendica il nostro diritto ad
esistere.

Oggi, dopo il decreto legge Meloni del 2022 che vieta i rave e i free
party, noi continuiamo ritmate a urlare sempre più forte che non ci
avranno mai. Anche nei regimi più autoritari --- ai quali sembra stia
puntando l'attuale governo --- si continua a festeggiare di nascosto,
negli edifici privati o nei boschi, lontano da tutto e da tutti. Il
desiderio di ascoltare musica e ballare è connaturato all'essere umano.
E noi vogliamo strabordare dai margini, smarginare!

## 1. Le signore della Techno

...

17:55

«ehi Sam a che ora passo da te ?»

...

19:08

«ehi ciao Ambrina! passa verso le 22.30»

«ricordati di portare le cuffie, io ho la consolle e mac»

...

Ricordarsi tutto è sempre la parte più difficile per due come noi che si
dimenticano pure le mutande. Quante volte ci è capitato di essere
tornate indietro? Ricordi quella volta che dovevamo suonare all'unione
civile di care amich3 sui colli a Bologna e a metà tragitto --- ed era la
metà di un tragitto piuttosto lungo --- ti ho chiamata perché mi ero
scordata il caricabatterie del mac a casa e tu sei dovuta tornare
indietro?

Sempre un po' scapestrate noialtre, ma sempre felici ed emozionate di
fare ballare le persone.

Per questa sera la playlist è pronta, fuori fa freddo e dentro farà un
cazzo di caldo, ma sarà bellissimo, anche se ogni volta ci chiediamo
«chi ce lo fa fare?», uscire tutte imbacuccate, caricare la macchina,
magari ci si mette pure la pioggia... «La macchina la prendi tu? E il
parcheggio?»

Poi appena si entra nel club, nel locale o posto occupato o associazione
o baracca, iniziamo subito a sentire un po' di fremito e di agitazione
misto a emozione dentro la pancia: «chi verrà stasera? ma, sopratutto,
verrà qualcunə? Speriamo di non fare errori alla consolle...» «Beviamo
qualcosa ?»

La serata inizia solitamente con una calma apparente, tutte noi intente
ad attaccare fili, cartelloni, fare scritte, truccarsi, nulla è più come
prima, tutto tende al luccichio, allo splendore e favolosità.

Un attimo che ho sempre notato e che mi torna spesso in mente è quando
una di noi, durante il sound check, fa partire un pezzo a caso, quel
beat, quel botto, quel sound spacca la tensione e tutte reagiscono in
qualche modo all'unisono, gioia, applausi, indistintamente reagiamo a
quel primo suono che oltrepassa e riempie lo spazio e che ci mette sulla
strada giusta.

Mi piace mettere musica in un duo, mi piace preparare le playlist
assieme, misto di chiacchiere e selecta.

Mi piace condividere le emozioni, le stanchezze, le soddisfazioni, i
trucchi, i pensieri, le vite. Amo la comunità, piccola come la mia e la
tua sulla nostra consolle o più grande come l'esperienza collettiva, mix
di vite lesbiche e non binary° che creano --- solo per il fatto di
esserci --- spazi politici nei club.

Cariche!

Fuori fa sempre freddo, ma la gente inizia ad arrivare...

il mattino arriva in fretta

## 2. Flusso

Sono al locale, è mattina, no pomeriggio, la gente ha fatto nottata
dal Pride, in consolle c'è L., arrivo presto credo che sono l'unica
sobria ma mi piace, basta un po' di empatia e sono già nel mood, non
si parla troppo, c'è stanchezza e fattanza, meglio, l'atmosfera è
piena d'amore e ci sono tante amichə, c'è anche M. ora è lei, sta
benissimo, risplende in un completino rosso stretch, ha un ventaglio
che agita sempre e generosamente lo sventola anche alle vicine di
posto tra cui io, finalmente un po' d'aria in quest'afosa domenica
bolognese. Inizia a suonare L. e ci alziamo tuttə per ballare, la
musica spacca, siamo le mejo mi dico, vado anche io un po' in
consolle a fare due chiacchiere, poi ballo con un gruppo di ragazzi
di Bolzano che mi riconoscono perché al Pride ho animato col
microfono la nostra carra lesbica come una pazza scatenata e li ho
tenuti incollati dall'inizio alla fine, quando mi sono buttata per
uno stage diving lunghissimo da vera rock star...

Ballando si parla di filosofia, ma senza troppe pippe, infatti
notando una pianta grassa appoggiata lì all'angolo constatiamo i
pericoli dello stare troppo a pensare, ti immobilizza. Meglio di no,
MAI pensare troppo, è deleterio. Quando i discorsi si fanno troppo
complicati meglio tornare alla musica, al movimento, seguendo il
ritmo, lì non si sbaglia, non si rischia la cristallizzazione. E la
povera pianta è sempre lì, la gente ci appoggia sopra le robe, e i
discorsi nostri sono sicuramente stupidi ma è quella la cosa più
bella, parlare senza dover arrivare a nulla, stare bene con gli
altri senza nemmeno poi ricordarsi chi sono, è incredibile il
feeling che si può raggiungere con le persone sconosciute e in una
serata è bello così, mi piace che siamo una collettiva, siamo tante
dj e ognuna ha il suo momento ma non siamo sempre in consolle, ed è
sempre la nostra serata anche quando siamo a ballare tra il
pubblico, la nostra serata Collettiva Elettronika, mi piace unire al
microfono le nostre musiche, presentarci e legare i nostri dj set: è
tutto fluido, come la musica.
