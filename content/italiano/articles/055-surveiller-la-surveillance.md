---
author: "Anonim*"
title: "Sorvegliare la sorveglianza"
subtitle: "Consigli pratici per la creazione di un collettivo di copwatch"
info: "Testo scritto per la raccolta svizzera romanda"
datepub: "Aprile 2022"
categories: ["DIY, tutorial", "polizia, repressione, amministrazione giudiziaria", "autogestione, autorganizzazione", "autodifesa"]
tags: ["arrestation", "caméra", "drone", "feu", "impossible", "justice", "logique", "mail, email", "procès", "stockage", "téléphone", "violence", "visage"]
langtxt: ["fr", "en", "it"]
zone: "Suisse romande"
num: "55"
quote: "osservare la polizia"
---

*Questo testo è stato scritto più di un mese prima della diffusione
delle immagini dell'assassinio di George Floyd e delle loro
ripercussioni politiche su scala mondiale, e quasi sette mesi prima del
progetto di legge francese "*Sécurité globale*" (sicurezza globale), che
vuole rendere illegale la diffusione di immagini riguardanti un
poliziotto identificabile.*

## A cosa serve un gruppo organizzato di copwatch?

Il copwatch° (termine che significa semplicemente "osservare la
polizia") compare negli USA in un contesto di autodifesa popolare per
far fronte alla violenza razzista e all'impunità delle forze
dell'ordine, specialmente nei quartieri più precari. Per quanto ne
sappiamo, le prime a organizzarsi per filmare la polizia sono state
delle brigate affiliate più o meno direttamente alle Black Panthers, che
cercavano di documentare questa violenza con le cineprese analogiche
dell'epoca. Il copwatch è nato organicamente, in una logica di
resistenza mirata a testimoniare di queste esperienze.

In seguito il fenomeno si è evoluto, ha generato nuove idee, si è
organizzato. In alcuni paesi, non solo occidentali, alcuni collettivi
hanno iniziato a fare delle ronde serali nelle città con l'intento di
sorvegliare le azioni della polizia, pronti a filmare in caso di
violenza. Esistono anche reti internazionali, siti internet, spazi per
condividere immagini, tattiche ed esperienze, che aiutano a organizzare
gruppi di copwatch decentralizzati. In queste reti è possibile inoltre
imparare gli strumenti di difesa giuridica nei confronti della polizia,
a costruire un dossier di testimonianze, ecc. Più tardi, all'inizio del
XXI secolo e ispirandosi a diverse brigate popolari, si sono formati dei
gruppi con l'obiettivo specifico di filmare la repressione dell\*
militanti: non solo la violenza quotidiana ma anche la violenza
politica, come la repressione delle manifestazioni. È di quest'ultimo
tipo di copwatch che parleremo qui.

In fondo l'idea è semplice: siccome nessuno sorveglia chi sorveglia noi,
è necessario che sia il popolo a filmare. Non possiamo fare affidamento
né sullo Stato né sul suo sistema giudiziario per controllare il loro
principale organo di controllo: la polizia. L'impunità della polizia
francese non è più da dimostrare (basta guardare il numero di condanne
dell'IGPN[^1] o l'insieme delle denunce di violenze poliziesche degli
ultimi anni). In Svizzera le cose non vanno meglio. Negli ultimi
vent'anni, l'ONU ha condannato la Confederazione a due riprese per la
mancanza di un organo di controllo indipendente della propria polizia.
In poche parole, se vuoi denunciare una violenza poliziesca, devi
rivolgerti... alla polizia. Sul terreno, l'impunità è anche la legge di
una polizia svizzera che ha causato più di una morte, che commette
violenze quotidiane e che spesso eccede i propri diritti. Il copwatch
può anche essere definito come un'offensiva militante che documenta e
dimostra la necessità di ripensare, disarmare e/o addirittura abolire la
polizia nella sua forma attuale.

Organizzarsi per filmare la violenza poliziesca cambia molte cose. Anche
se l'idea non ha niente di complicato, assicurare una presenza nei
momenti di lotta fisica porta a risultati concreti, producendo immagini
più complete e comprensibili che possono alimentare il dibattito
pubblico e/o essere utilizzate con maggior efficacia nella difesa legale
dell\* militanti. Molto spesso, in Svizzera, le immagini dei gruppi di
copwatch sono diffuse dalla stampa o presentate in tribunale.

Naturalmente durante le manifestazioni c'è sempre molta gente che filma,
ma spesso è troppo tardi (abbiamo il riflesso di tirare fuori il
cellulare quando assistiamo a una violenza ma non facciamo in tempo a
iniziare a riprendere che l'incidente è già concluso e restiamo senza
immagini). Le riprese sono spesso sfocate, incomprensibili. Invece,
mobilitando sul terreno un piccolo gruppo di persone attente,
concentrate e che sanno perché sono lì, le immagini che documentano la
violenza diventano immediatamente molto più efficaci perché si gioca di
*anticipo*, filmando prima ancora che la violenza si scateni.

Non è complicato creare un gruppo di copwatch nella tua città per
sostenere e difendere chi lotta: basta un minimo di organizzazione e di
discussione.

## Filmare per chi?

È importante che il gruppo si metta collettivamente d'accordo su
un'etica del copwatch: per chi stiamo filmando? Se ci si può coordinare
con gruppi anti-repressione o associazioni che organizzano eventi, è
essenziale che l\* militanti filmat\* siano prioritari\* nella gestione
di queste immagini: nell'ideale, dovrebbero sempre avere la possibilità
di esprimersi sulla loro diffusione. Nella maggior parte dei casi i
gruppi di copwatch decidono di non pubblicare nessuna immagine a proprio
nome, per evitare di rendersi troppo visibili ma soprattutto per non
interferire con il diritto di immagine di ognun\*.

## Procedura

Questa procedura è una specie di prototipo, una lista di cose che
possono essere utili. Naturalmente non si è possibile fare tutto a ogni
evento. La lista può essere completata, messa in discussione,
modificata...

### Prima del giorno dell'evento

Può essere una buona idea andare a farsi un giro nel luogo dove avrà
luogo la manifestazione per tracciare un piano delle possibili zone di
invisibilità. La polizia sa di essere filmata e cercherà spesso di
organizzare immediatamente lo spazio per rendere la violenza invisibile:
creando linee di furgoni, utilizzando un gruppo di agenti per
nasconderne altre, portando l\* militanti in strade bloccate in
precedenza, ecc. Queste zone permettono alla polizia di arrestare,
perquisire e sgarrare indisturbata. Si tratta spesso di luoghi che si
prestano a retate.

Il gruppo di copwatch deve studiare lo spazio: esiste una zona
sopraelevata dove appostarsi preventivamente? Un posto relativamente
sicuro dal quale osservare senza essere osservat\* (filmando attraverso
la vetrina di un negozio)?

Se si è in tant\* a filmare, bisogna anche riflettere a come
distribuirsi nello spazio anche se, quando giunge il momento, le cose
sono spesso più complicate del previsto. Spartirsi lo spazio permette di
evitare che tutt\* quell\* che stanno filmando finiscano in un'unica
retata e non siano in grado di riprendere le altre zone critiche, ad
esempio. Naturalmente il vivo dell'azione riconfigura lo spazio ed è
soprattutto essenziale osservare, improvvisare e rimanere reattiv\*, ma
il fatto di anticipare e prepararsi porta spesso a ottimi risultati.

### Ronde nel giorno dell'evento

Nelle ore che precedono l'evento, può essere interessante effettuare
delle ronde per valutare la mobilitazione preparata dalla polizia.
Alcuni segni permettono di anticipare la gestione dello spazio, come
delle barriere di deviazione piazzate all'angolo di determinate strade,
gli itinerari precisi delle auto civetta, i furgoni della polizia
parcheggiati discretamente, ecc. Attenzione a non farsi notare durante
queste ronde. Se ci sono in giro gli sbirri in uniforme o in civile, si
può cercare di ascoltare discretamente le loro conversazioni, che
potrebbero contenere informazioni preziose sulla pianificazione
dell'evento. Spetterà poi a ogni gruppo il compito di scegliere il
metodo migliore per comunicare queste informazioni in tempo reale all\*
militanti e/o alle organizzazioni per proteggerl\* al meglio. Per le
comunicazioni digitali, privilegiare le app con cifratura end-to-end
(Signal, Telegram e Riot sono tra le più gettonate. Signal cancella i
messaggi dopo un tempo predeterminato, cosa che può fare molto comodo).

### Organizzazione dell\* copwatcher

Esistono più tattiche, più scelte possibili...

Alcuni gruppi di copwatch scelgono di rendersi visibili indossando una
fascia da braccio o un altro segno identificativo, come fanno giurist\*,
avvocat\* o giornalist\* sul terreno. In questo caso ci si può portare
dietro degli stabilizzatori di telefono e dei bastoni per selfie per
filmare dall'alto, sostituire gli smartphone con cineprese, ecc. Ci si
rende visibili e ci si comporta come giornalisti. Questa scelta richiede
un comportamento particolarmente prudente per evitare di diventare un
facile bersaglio.

Altri gruppi optano per un copwatch discreto, fondendosi nella folla per
evitare di mostrare che sono organizzat\*. In un bloc radicale, si
rimane mascherat\* e non si mostra troppo apertamente che si sta
filmando: un bloc può essere ripreso solo se la sicurezza delle immagini
è garantita.

Raramente è utile che due copwatcher si trovino nello stesso punto. Gli
angoli di ripresa possono davvero fare la differenza durante un
processo: la leggibilità dell'immagine è cruciale. Valgono comunque le
regole di sicurezza in uso in qualsiasi tipo di manifestazione: si cerca
di non perdere mai di vista un\* copwatcher e a volte è opportuno
filmarsi e proteggersi a vicenda.

### Materiale consigliato

-   Lo smartphone è un buon compromesso tra efficacia e discrezione.

-   Se si usa uno smartphone, l'ideale è avere una password
    alfanumerica abbastanza lunga, ad esempio: SiamoOvunque\*1312\*.
-   Assicurarsi di avere abbastanza spazio di archiviazione.
-   Cancellare tutti i contenuti compromettenti (applicazioni e chat
    non necessarie, immagini, rubrica, ecc.).
-   È impossibile fare riprese per un giorno intero senza un power bank
    per ricaricare più volte.
-   Il solito occorrente per le manifestazioni (siero fisiologico,
    cibo, materiale di primo soccorso, maschera di plastica, ecc.).

### Abbigliamento consigliato

-   **Visibilità:** identificarsi come copwatcher con una fascia da
    braccio, un gilet o altro segno distintivo e restare molto prudenti.
    Questa tattica fa di noi un bersaglio privilegiato e rischiamo di
    essere tra l\* prim\* a essere interpellat\*. L'interrogatorio può
    costare caro.
-   **Immersione:** vestirsi come un\* manifestante come l\* altr\*,
    senza dare troppo nell'occhio per evitare di essere facilmente
    identificabile.
-   **Invisibilità:** vestirsi secondo i codici sociali
    dominanti/normativi di un\* borghese incuriosit\* che passa di lì
    per caso.

### Riprese professionali:

-   Gestire le immagini in tempo reale su un cloud autonomo e cifrato
    (dove tutte le immagini sono inviate direttamente e cancellate dal
    telefono).
-   Avere una persona nelle retrovie con un computer e un adattatore
    microSD per trasmettere le immagini.
-   Filmare con un drone (attenzione, di solito è illegale).
-   Procurarsi una tessera stampa e una grossa cinepresa se nel tuo
    paese ci sono dell\* giornalist\* compagn\*.
-   Pilotare un elicottero con una cinepresa per contrastare gli
    elicotteri della polizia, non si sa mai.

### Riprese

Può sembrare scontato ma imparare questi accorgimenti può fare davvero
la differenza (come testimoniano diversi casi in Svizzera e altrove).

Quando si filma una scena di violenza: tenere costantemente d'occhio
l'inquadratura e la luminosità dell'immagine, cercare di non tremare,
concentrarsi sulla ripresa durante le situazioni critiche. Assicurarsi
della qualità della presa di suono evitando di scandire slogan (anche se
è frustrante), parlare il meno possibile. Non preoccuparsi del diritto
di immagine durante le riprese (i volti e le matricole devono essere
chiaramente visibili per avere un valore legale e la questione
dell'identità si pone solo in caso di diffusione delle immagini).

Privilegiare campi lunghi per capire tutto quello che sta succedendo e
riprendere il maggior numero di movimenti possibile. Cercare di
anticipare e di filmare la situazione generale quando si sente che "le
cose si stanno scaldando" per creare un documento visivo chiaro.

### Assicurarsi di poter contattare le persone coinvolte e riunire l riprese di altr\*

Dopo aver filmato qualcun\* che si fa interpellare o arrestare dalla
polizia, cercare di ottenere discretamente un indirizzo email o un
numero di telefono per prendere contatto in un secondo tempo. Se non se
ne ha l'occasione, chiedere all\* su\* amich\*.

Spesso la stessa scena è stata filmata anche da altre persone. Il
problema è che le immagini vengono raramente raggruppate e consegnate
alla persona coinvolta. Inoltre capita che queste immagini circolino sui
social senza le opportune precauzioni (sfocatura dell\* militanti, ad
esempio). Un collettivo di copwatch organizzato può servire anche a
questo: a dissuadere le persone dall'incriminarne altre su Instagram e a
centralizzare le immagini delle persone presenti sul posto. Attenzione:
bisogna chiedere se si possono ottenere le immagini senza dire che si
appartiene a un collettivo, qualunque sia (nelle manifestazioni come
ovunque, non puoi mai sapere con chi stai parlando).

Una volta che si sono calmate le cose, si può anche chiedere all\*
testimoni oculari di raccontare di fronte alla cinepresa o a volto
coperto cosa hanno visto. Non si fanno troppe domande, si parafrasano le
affermazioni e si chiede all\* testimoni di confermarle. Questo genere
di documentazione "dal vivo" ha già cambiato il corso di diversi
processi.

## Dopo l'azione o l'evento

### Archiviazione

È importante archiviare le immagini e non cancellarle dopo averle
spedite alle persone coinvolte. Le immagini potrebbero servire mesi e
mesi dopo l'evento, anche a militanti divers\* da quell\* a cui si aveva
inizialmente pensato. L\* militanti diventano spesso l'oggetto di
inchieste approfondite e di lungo respiro che radunano più capi d'accusa
relativi a più eventi e azioni. Meglio conservare ogni cosa, anche
quando le immagini non sembrano violente: non si sa mai cosa potrebbe
servire di fronte a un\* giudice.

Non archiviare mai le immagini online ma usare chiavette USB o hard disc
esterni. Per assicurarsi di non perdere niente per strada, le immagini
possono essere archiviate in più copie su supporti diversi.

### Montaggio

Non sottovalutare la durata del lavoro.

Inviare un unico file montato che compila l'essenziale di un evento può
spesso essere di grande aiuto per un'organizzazione che cerca di
difendere l\* manifestant\* o per l\* avvocat\* impegnat\* in un'azione
legale collettiva.

Può anche esser utile includere nel montaggio dei testi esplicativi o
evidenziare, ad esempio con cerchi colorati, delle aggressioni che non
sono al centro dell'immagine ma in secondo piano.

Sfocare tutte le persone in un montaggio che può durare più di un'ora è
un lavoro colossale e raramente necessario: meglio lasciare questo
compito alle organizzazioni, precisandone però l'importanza quando si
inviano le immagini. Tuttavia, può essere prudente assicurarsi che i
volti dell\* copwatcher siano sfocati.

Per inviare i file di immagini in tutta sicurezza, fare riferimento
anche alle diverse guide di autodifesa digitale.

Un gruppo di copwatch deve inoltre auto-educarsi come meglio può sulle
leggi in vigore in un paese relative al diritto di immagine in uno
spazio pubblico, specialmente quando è coinvolta la polizia (questi
diritti sono spesso regolati da testi di legge complementari o
addirittura contraddittori).


[^1]: *Inspection Générale de la Police Française* (Ispezione generale
    della polizia francese) (NdT).
