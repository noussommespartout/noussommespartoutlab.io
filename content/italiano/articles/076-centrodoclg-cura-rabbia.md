---
author: "Centro di documentazione dei movimenti \"Francesco Lorusso — Carlo Giuliani\""
title: "Avere cura della propria rabbia"
subtitle: "Significati e senso di un archivio dei movimenti"
info: "Testo scritto per la raccolta bolognese"
datepub: "Aprile 2022"
categories: ["storia, memoria", "autoriflessione sulla militanza"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "76"
quote: "per non essere un sotterraneo, né un museo"
---

Costituire in prima persona, anche solo in piccola parte, un centro di
documentazione dei movimenti, e in particolare il Centro «Francesco
Lorusso --- Carlo Giuliani» di Bologna, cioè partecipare alle sue
attività, ai percorsi di costruzione delle sue iniziative, accedervi con
una certa regolarità, arrivare a percepirlo come un pezzo importante
della propria quotidianità fisica; tutto ciò assume vari significati.

A partire da quelli più complessivi: trasmissione della memoria
attraverso documenti originali, riaffermazione della storia, salvataggio
di momenti di snodo dall'oblio, dalla retorica, dalle narrazioni
mistificanti interessate. Potrà forse apparire metodo scolastico, di
nozionistica opposizione, e invece accende questo luogo come faro di una
visione (inevitabilmente, ma esplicitamente) di parte della storia nel
piattume generale della neutralità che va oramai imponendosi nelle
coscienze metropolitane da almeno venti anni --- di parte non per
tifoseria ma per amore di verità.

A un altro livello, più personale (ma non privato perché fatto sempre di
relazioni), si collocano altre categorie di valori. Partirei
evidenziando le sensazioni e i pensieri che scaturiscono dal contatto
diretto con i materiali conservati in quanto tali: in primis la bellezza
di trovarsi il gusto del passato tra le mani e sentirne il peso, la
grandezza. La responsabilità, anche. Quando si prende un foglio di
giornale da dentro un faldone di quelli blu che per venti anni sono
stati custoditi a Genova da chi si è fatto carico --- non in esclusiva,
per carità --- di non far sparire il G8 del 2001° dal mondo, non lo si fa
con la stessa noncuranza con cui si sfoglia una pagina qualsiasi di
«Repubblica» oggi, anche se se ne prova lo stesso disprezzo:
anzi, sicuramente un disprezzo maggiore, aggravato dalla storia che si
porta appresso. Ed ecco che si ricava un significato: avere cura della
propria rabbia, che è risorsa, forse la più grande.

Un altro significato che si coglie dal lavoro in questo tipo di archivio
deriva dalla consapevolezza di fare un lavoro che non è
autoreferenziale, anche se si è soli nell'attimo pratico della
catalogazione o della digitalizzazione di un documento, anche se non se
ne colgono immediatamente i risultati: si comprende in quei frangenti il
valore della lentezza, il senso del rallentare, l'arte della pazienza. E
si capisce perché andare piano è indispensabile per abbattere un modello
di vita che ci vuole iperproduttivi e iperconsumanti, vettori della
ipercircolazione dell'economia globale. Da questo genere di esercizio
emerge anche il valore della solitudine per il mondo, di quanto ognuno
di noi, pensando sé stesso all'interno di una comunità, possa avere una
funzione resistente, svolgere un ruolo di interdipendenza collettiva.

Ma le questioni più significative di militare in un centro di
documentazione dei movimenti che esiste per non essere un sotterraneo,
né un museo, sono probabilmente due: la prima, è l'incontro con persone
che sono intelligenze vive, giovani e meno giovani, storie in divenire
con proprie esperienze, immaginari, consapevolezze. A queste persone,
sensibilità pensanti e teste emozionalmente perturbabili, si possono
fare domande; di loro si possono carpire espressioni, insofferenze
celate o manifeste; ascoltare i racconti; dalla loro dedizione e
disponibilità ricevere apprezzamenti, rimproveri, apprendere pratiche e
parole, acquisire competenze, capacità di incisione al di là di sé
stessi; conoscere e distinguere fastidi, entusiasmi, coinvolgimenti,
inerzie, trasporti reali o fittizi; magari anche regalare qualcosa.

L'altra è la possibilità di entrare e partecipare alle storie a cui per
età o altre ragioni non si è potuto contribuire, seppur chiaramente si
entra nella sola coda. Ma è la coda l'ultima a vedersi prima che il
cavallo rientri nella scuderia dopo il galoppo e venga costretto a
riposare in cattività e a luci spente, e perciò va comunque illuminata.
