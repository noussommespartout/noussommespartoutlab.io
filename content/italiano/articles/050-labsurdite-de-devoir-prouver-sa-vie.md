---
author: "Anonim*"
title: "L'assurdità di dover dimostrare la propria vita"
subtitle: "Le difficoltà della lotta per l'asilo"
info: "Intervista trascritta per la raccolta svizzera romanda"
datepub: "Settembre 2020"
categories: ["migrazioni", "antirazzismi", "polizia, repressione, amministrazione giudiziaria"]
tags: ["confiance", "dublin", "feu", "force", "foyer", "gueule", "impossible", "limite", "parole", "pouvoir", "prix", "rage", "théâtre"]
langtxt: ["fr", "it", "en"]
zone: "Suisse romande"
num: "50"
quote: "Ci arrabattiamo di continuo, ai margini"
---

Siamo un collettivo che lotta nel campo dell'asilo. Svolgiamo un lavoro
di sostegno per le persone in procedura di asilo per rimanere in
Svizzera o al di fuori di essa. In un quadro istituzionale, quando
parliamo in difesa di chi si trova in situazioni difficili, tutto è ai
margini, ai limiti. Bisogna essere molto rilevanti, molto competenti.

Una delle cose che ci aiuta è avere una rete di contatti privilegiati.
Medici di fiducia, ad esempio, che ci forniscono informazioni utili per
instaurare un rapporto di forza con le autorità. Abbiamo anche
informatric\* nelle istituzioni, nelle case di accoglienza, nei servizi
governativi, negli ospedali, ma non sono molt\* e spesso non rimangono a
lungo; è complicato, quasi impossibile, lavorare in queste istituzioni
quando hai un certo tipo di convizioni.

Ci arrabattiamo di continuo, ai margini, sfruttando i piccoli spazi che
le istituzioni ci lasciano. Un esempio concreto: l'anno scorso, una
giovane donna di 16 anni è arrivata in Svizzera dopo essere passata
dall'Italia. Il suo percorso era doloroso, era stata vittima della
tratta. È un caso tipico del Regolamento di Dublino°: la Svizzera
rifiuta di partecipare alla procedura di asilo, perché ritiene che
l'Italia debba occuparsi del suo caso. Inoltre, le autorità avevano
deciso che non era minorenne. Per aiutarla a rimanere in Svizzera,
abbiamo collaborato con un centro di Zurigo specializzato in casi di
tratta. La ragazza ha potuto essere seguita e questo ci ha permesso di
"dimostrare" che aveva effettivamente vissuto un'esperienza
traumatica. È così assurdo che le istituzioni mettano le persone
nell'obbligo di dover "dimostrare" la propria vita. La dice lunga
sul genere di umanità che si crea quando la gestione è in mano a
un'ideologia puramente *manageriale*.

L'ORS (Organisation for Refugee Services), l'azienda che gestisce le
case di accoglienza, non fa il minimo sforzo per questo tipo di casi.
Lavorano con l\* psicologh\* con cui hanno concordato i prezzi più
bassi. Il referto medico che siamo riuscit\* a ottenere ha potuto
"dimostrare" che si trattava di una persona "vulnerabile". Se riesci
a "dimostrare" la tua vulnerabilità con certificati, relazioni,
perizie, a volte le autorità sono un po' meno brutali. E in questo
caso, le autorità hanno comunque insistito per effettuare esame osseo
per la determinazione dell'età° nel tentativo di "dimostrare" che la
ragazza non era minorenne. Questo essame è fortemente contestato dalla
Società Svizzera di Pediatria, ma anche più ampiamente a livello
europeo. Il margine di precisione di questi esami ossei è di circa due
anni, quindi non significano assolutamente nulla. Così hanno fatto il
l'esame e il risultato ha indicato che aveva tra i 16 e i 18 anni. Dopo
tutta questa procedura la ragazza è stata finalmente riconosciuta come
persona "vulnerabile" e ha potuto avviare una regolare procedura di
asilo. Ora la sua vita quotidiana è cambiata. Fa teatro, sport, le è
permesso di socializzare. Ha potuto lasciare la casa di accoglienza per
vivere in una famiglia.

Quando militi in un collettivo che si occupa di asilo ti trovi sempre
tra due fuochi, tra il sostegno individuale alle persone e il lavoro
politico. Per noi ha senso lavorare contemporaneamente su entrambi i
livelli. Anche se è una sfida: il lavoro di sostegno alle persone che
stanno davvero male è sempre una priorità, e questo ci lascia poco
tempo, date le nostre risorse limitate, per pensare attivamente ad
azioni più sistemiche°.

Ed è vero che le cose che siamo riuscit\* davvero a migliorare sono più
su scala individuale che su scala politica. Abbiamo bisogno di vedere
che ciò che facciamo ha un senso, che riusciamo comunque a ottenere
piccole vittorie, anche se non nei confronti del sistema nel suo
complesso. Nonostante tutto, diverse persone sono riuscite a ottenere,
se non necessariamente un permesso stabile, anche solo la possibilità di
accedere alla procedura di asilo o di ammissione provvisoria.
Personalmente, mi occupo di questioni relative all'asilo da circa 30
anni. Ho visto quanto è peggiorata la situazione. Oggi siamo felic\* se
qualcuno riesce a ottenere un permesso N (permesso per richiedenti
asilo). Prima lo eravamo quando qualcuno riceveva un permesso per
rifugiat\*°. È scioccante pensare che il più delle volte lottiamo perché
le persone possano integrare una normale procedura di asilo e nemmeno
perché possano davvero rimanere alla fine della procedura, mentre
normalmente dovrebbe partire tutto da lì. Oggi, il rifiuto generalizzato
da parte dello Stato di avviare la procedura è palese. Sono l\*
militanti della società civile a far dare una mossa ai comuni, ai
cantoni, all\* parlamentari.

Vorrei concludere dicendo che il problema della militanza è lo
sfinimento. È questa l'arma delle autorità. Mi piace la frase di Coluche
La dittatura ti dice "chiudi il becco", la democrazia ti dice "parla
pure". Per me è esattamente questo che accade. Quando andiamo a trovare
le autorità politiche arriviamo sempre pront\* con documenti, relazioni,
cifre; ci accolgono, discutiamo inseme, i media ne parlano. Ma alla fine
non succede assolutamente nulla. Allora noi pensiamo: andiamo avanti. Ci
lasciano organizzare manifestazioni, accolgono le nostre petizioni. Poi
niente. È molto difficile continuare resitere in un collettivo quando
non si ottengono risposte. A livello individuale ci sono state delle
vittorie, abbiamo permesso ad alcune persone di rimanere, e questo
umanamente parlando è importante, ma non è sufficiente. Ma nonostante
tutto, ho l'impressione che costringiamo le autorità ad andare oltre il
loro discorso repressivo e difensivo, hai presente quei discorsi
ipocriti della Svizzera che si definisce come un "Paese umanitario",
una "terra di accoglienza".

Ho l'impressione che senza la nostra pressione sarebbe ancora peggio.
La situazione delle persone in cerca di asilo, i maltrattamenti che
subiscono dalle istituzioni, sono invisibilizzate a livello politico,
mediatico e persino sociale. Senza la dimensione umana e le vittorie
nell'accompagnamento individuale, per me militare sarebbe molto
difficile. Ho visto espulsioni in diretta, ho accompagnato persone alla
polizia che sono state portate via sotto i miei occhi. E a quel punto
non c'è più niente da fare. A volte riusciamo a farl\* tornare.
Purtroppo non è sempre possibile, ma facciamo progressi. Dobbiamo
rimanere umili per mantenere vivo il fuoco della militanza. Per non
deprimerci troppo, dobbiamo assaporare ogni piccolo progresso,
soprattutto a livello politico. Dobbiamo preservare la nostra rabbia, la
nostra grinta, non dobbiamo mai arrenderci, perché questa è la strategia
dello Stato: avere ragione di noi per sfinimento.

