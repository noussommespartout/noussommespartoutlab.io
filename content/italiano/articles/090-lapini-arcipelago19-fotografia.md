---
author: "Michele Lapini e Arcipelago-19"
title: "La fotografia non è mai neutrale"
subtitle: "Pandemia, arcipelaghi e altre scorie"
info: "Testo scritto per la raccolta bolognese"
datepub: "Novembre 2022"
categories: ["corpo, salute, cura", "arti, musica, fotografia, performance"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "90"
quote: "ripartire meglio"
---

## Fotografare la pandemia

Durante la pandemia da Covid-19 abbiamo rivisto una centralità molto
forte della fotografia. Mentre eravamo tutti/e chiusi/e in casa, la
fotografia ci ha dato le prove di quello che stava succedendo, creando
la consapevolezza di ciò che stavamo vivendo. Parlavamo di virus, di
letalità, ma fino a quando non abbiamo visto le fotografie delle terapie
intensive, probabilmente non avevamo piena consapevolezza del periodo
che stavamo attraversando. In quel momento, noi fotograf\* eravamo una
delle poche categorie che poteva uscire, eravamo fortunat\* ma, almeno
personalmente, sentivo una forte responsabilità; sia perché io potevo
uscire e gli/le altri/e no, quindi stavo vivendo un privilegio, sia
perché attraverso il nostro lavoro noi eravamo gli occhi degli/lle
altri/e. A maggior ragione in un momento in cui noi potevamo vedere
certe cose e documentarle, mentre le altre persone no, e si fidavano di
noi, si aspettavano da noi qualcosa per capire meglio cosa stesse
succedendo.

Questo momento è durato poco. Dopo, la fotografia è tornata a quella
normalità che c'era prima della pandemia e che, in gran parte, è un
effetto della digitalizzazione. La digitalizzazione è uno dei tanti
cambiamenti che ha subito la fotografia nella storia, passando dalla
pellicola al digitale, e adesso addirittura ai cellulari. Da un lato,
questo è un processo che ha permesso una democratizzazione dello
strumento fotografico molto forte. Questo ovviamente è uno dei lati
positivi. Anche in pandemia la democratizzazione del mezzo ha permesso
un racconto molto più diretto. Le prime immagini, tra le più virali e
viste, non erano dei fotografi ma dal personale medico. Le fotografie
delle persone in terapia intensiva, della stanchezza degli operatori
sanitari erano fatte da\* protagonist\* stess\*, da chi stava lavorando,
che grazie a uno strumento che permetteva di scattare fotografie ci ha
restituito quelle immagini, comunicandoci quello che stava vivendo.
Questo è un punto positivo secondo me perché l'esclusività del mezzo e
del racconto non è mai positiva.

Ma la digitalizzazione ha anche dei lati più critici, perché prima del
digitale le fotografie erano poche, i fotograf\* erano pochi, e la
fotografia aveva una centralità molto più forte anche nell'editoria. I
reportage pubblicati nelle riviste erano di molte pagine e i fotograf\*
venivano inviat\* all'estero. Adesso tutto questo è un sogno, nonostante
ci siano alcune eccezioni positive in Italia anche oggi. Inoltre,
durante la pandemia abbiamo vissuto anche una sorta di anomalia
«all'italiana»: nel momento in cui eravamo al centro del mondo, ce lo
siamo fatto raccontare dall'estero. Avevamo e abbiamo un sacco di
fotograf\* bravissim\*, ma nessun giornale italiano ha pensato bene di
chiamarl\*, mentre le riviste estere hanno chiamato i loro fotograf\*, e
noi abbiamo ripubblicato il loro lavoro. In questo modo ci siamo fatti
scappare la possibilità di raccontare in prima persona.

Raccontare è molto importante ed è anche importante non delegarlo. Io,
per il lavoro che faccio, non posso delegare questo compito, perché per
fotografare una cosa devo essere lì, per me è impossibile delegarlo. È
anche una cosa molto bella, perché si riesce a raccontare in maniera
molto empatica, cosa che serve anche a chi legge un articolo, perché una
foto arriva in maniera molto diversa da un testo. Per questo credo che
noi, come fotograf\*, dovremmo fare un passo indietro, uscire un po'
dalla scena e far sì che la storia sia il soggetto, non tanto chi la
racconta. Sono le storie che fanno la fotografia e non il contrario.
Molto spesso, nelle storie che raccontiamo, mettiamo troppo «io»; fra
l'altro per noi occidentali questo è anche un retaggio coloniale o
eurocentrico, siamo sempre noi a raccontare le storie degli altri.
Questo tipo di sguardo non fa bene neanche noi: lo vediamo per esempio
quando si parla di cambiamento climatico, parliamo sempre di come
avviene altrove, perché è molto più facile parlare del Polo Nord
piuttosto che dell'alluvione a Livorno o in Pianura Padana.

Questo è secondo me un momento buono per ragionare su come la fotografia
possa essere diversa dalla normalità, a prescindere da chi la paga o la
commissiona. Ovviamente è un periodo storico, per la fotografia, molto
particolare, siamo invasi da uno tsunami di immagini quotidiane
fortissimo. Questo ci porta a essere meno «sensibili», come «sedati»,
tanto che alcune immagini, come quelle del piccolo Alan Kurdi o tante
altre, non ci sconvolgono più. Anche questa è una sfida per noi: trovare
delle strade per riuscire a far sì che la fotografia non diventi un
«sedativo», un «calmante», ma che sia qualcosa che dà la scossa. Perché
se non riesce a darti la scossa, se ti tranquillizza facendoti passare
incolume da una tragedia alla prossima, la fotografia ha perso. La
fotografia deve essere qualcosa che ti fa fermare e ti fa dire: «no,
adesso basta!». E questo è molto difficile in un momento in cui, quando
ognuno di noi apre qualsiasi social, viene invaso da una quantità di
immagini impressionante: non riusciamo più ad avere quella giusta
attenzione verso le immagini che ti permette di innescare reazioni.

Esistono molti tipi di fotografie e molti approcci al fotografare.
Quello che sento più vicino è una fotografia che sia il meno possibile
per me stesso e che sia in qualche modo a disposizione. Molto spesso
dico che sono le storie a dare senso alla fotografia, non il contrario.
E in questo c'è molto del mondo della fotografia che cerco di mettere in
pratica, ri-centrando tutto su quello che vedo e non su di me. Ogni
fotografia è parte di una delle verità, non è verità assoluta. Ogni
fotografia è una scelta, consapevole o meno, ma è una scelta che la
fotograf\* mette in campo. Prendere posizione con la fotografia non solo
è necessario, ma è assolutamente naturale.

Anche perché, anche all'interno di altri media più «autorevoli», non c'è
un uso della fotografia che dia strumenti per comprendere e leggere le
immagini. Se guardate le prime pagine dei quotidiani internazionali e li
confrontate con quelli italiani, noterete delle divergenze forti.
Partendo dalla prima pagina, c'è già una differenza incredibile:
all'estero la prima pagina è la foto, in Italia no, è molto più
importante il titolo. La foto serve a quel titolo, serve a giustificarlo
e confermarlo, è un rinforzo. Questo fa sì che la fotografia venga
considerata uno strumento secondario dell'informazione.

Sicuramente la fotografia non è verità, assolutamente, non è neutrale,
mai. Io una sala posso farla vedere vuota o piena in maniera molto
semplice. Dire che la fotografia, come il giornalismo, sia imparziale e
neutrale è una cazzata. Anche chi non lo fa apposta, chi dice di fare
una fotografia neutrale fa comunque una scelta. La posizione di scatto,
per esempio, fa sì che chi legge la foto vede il tuo punto di vista, che
non è mai imparziale. Non esiste un punto di vista imparziale. Questo è
sempre importante averlo presente ed esserne consapevoli. Perché chi
legge la fotografia dovrebbe sapere che quella fotografia è stata
scattata da una persona che ha scelto di scattarla in quel modo lì.

Lavorando per un giornale mi trovo a fotografare con obiettivi diversi.
Quando facevo le foto in pandemia, in quel momento in cui c'era la
polemica sulla «movida» e gli assembramenti, e magari usciva una gallery
su una via piena di gente a Bologna, veniva sempre messo in dubbio che
quelle foto fossero «vere». Ma il fatto che la foto abbia un preciso
punto di vista non vuol dire che la foto è «falsa». Le persone pensano
questo perché non abbiamo saputo spiegare, sono abituate a ragionare
solo in termini vero/falso. Questo sicuramente accade anche perché la
stampa e l'editoria hanno fatto grossi sbagli e hanno trattato i lettori
come persone che si bevono tutto. A un certo punto questa cosa è
esplosa, per cui si arriva a dubitare anche di quelle cose che sono
realmente successe.

Una cosa simile è successa quando siamo stati con Valerio Muscella in
Bosnia a gennaio 2021, a documentare la situazione delle frontiere
bosniache e dei migranti che provano ad entrare in Europa dalla rotta
balcanica. In quel momento tutti ne parlavano, mentre adesso no, e le
nostre fotografie sono girate molto, ma c'era sempre un grosso dubbio
sul fatto che fossero «vere».

Come possiamo far fronte a questo? Non possiamo semplicemente dire che i
lettori sono stupidi. Dobbiamo riflettere su come noi raccontiamo tutti
i giorni, su che ruolo e che autorevolezza diamo alla fotografia, sul
perché non investiamo di più sulla fotografia e la consideriamo solo
come un tassellino «decorativo» da mettere su una pagina perché
altrimenti magari è brutta. «Libération» ha fatto un numero senza foto
anni fa ed era impressionante. Sfogliandolo rimanevi incredulo. Però
molto spesso anche quando sfogliamo i quotidiani con le fotografie, vuoi
perché sono scelte male e piccole, vuoi perché sono foto vecchie, non
prestiamo molta attenzione alle fotografie. Questa non è solo colpa del
lettore, ma anche di un sistema che non permette alla fotografia di
potersi esprimere in maniera totale e libera.

È sempre un gran casino, soprattutto in un mondo incerto e complesso
come quello contemporaneo, rispondere alla domanda: «come andare
avanti?». Nascono sempre nuove contraddizioni, per cui io faccio molta
fatica a capire come andare avanti. Posso prendere questo periodo come
un'opportunità per capire cosa non andava bene e cambiare approccio o
stile.

## Le isole e l'Arcipelago

Proprio durante la pandemia insieme ad altre colleghe ci siamo chiamati,
visto che vedevamo bellissime fotografie di colleghe e colleghi da tutta
Italia che però non vedevamo sui giornali, e ci siamo chiesti: che
fare?. Non le vedevamo perché magari non c'era spazio, o perché in quel
momento c'era bisogno di fare cronaca e i giornali giustamente
pubblicavano le terapie intensive etc. Noi però vedevamo un sacco di
altre storie, di quel presente, che non riuscivamo a vedere altrove.
Abbiamo detto: «mettiamoci insieme e creiamo una sorta di finestra
quotidiana a cui tutt\* si possono affacciare». Eravamo abituati/e ai
balconi, ognuno si affacciava alla sua finestra e vedeva sempre la
stessa cosa, quello che abbiamo pensato è stato provare a far sì che
ognun\* si potesse affacciare ovunque, andando sul nostro profilo e
guardando le storie di altri.

Per noi è anche un modo di mettere in pratica una logica cooperativa e
non competitiva. Il nostro settore, vivendo nel capitalismo, è pieno di
competizione. Essendoci tantissima qualità nel fotogiornalismo italiano,
c'è molta competizione. I giornali che pubblicano bene sono pochi,
rischiamo sempre di competere fra di noi per le storie, e questa logica
non aiuta nessuno. La guerra fra poveri l'hanno sempre vinta i ricchi. E
quindi bisogna fare anche un passo indietro e capire cosa non va, anche
nel nostro settore la competizione è una cosa che non funziona. Da un
anno e mezzo a questa parte stiamo portando avanti Arcipelago-19, un
collettivo, una rete con la quale, in forma ibrida e fluida, stiamo
costruendo progetti collettivi. Il primo obiettivo era dare supporto e
visibilità --- parola che spesso è usata in un modo che non ci piace --- a
tanti lavori e a tante storie che non vedevamo, e che però noi facendo
Arcipelago scoprivamo.

Arcipelago-19 nasce all'inizio della pandemia e piano piano si allarga e
si deforma. Segue sempre un andamento pandemico, legato agli alti e
bassi della pandemia da Covid-19. Nasce per uscire dall'isolamento, per
ribaltare le logiche che viviamo quotidianamente all'interno del nostro
lavoro come fotograf\*, per creare una rete che sia più orizzontale
possibile senza un obiettivo preimpostato, ma con la possibilità di
crearne sempre di nuovi. Inizialmente abbiamo cercato di unire storie
che in quel periodo esistevano, ma non fuoriuscivano.

Tendiamo sempre ad andare fuori con lo sguardo, verso l'esterno. Mentre
in quel momento molte persone vivevano il proprio condominio
riscoprendolo. Ci sono arrivate tantissime storie, e abbiamo cercato di
dargli una piattaforma, uno spazio, un luogo comune dove ospitarle ma
anche dove farle parlare fra di loro. E da qui è nata una rete grande e
non ce l'aspettavamo. Abbiamo sempre fatto tutto gratuitamente, dando
interamente i soldi delle pubblicazioni ai fotograf\*. Ogni qual volta
un giornale, una rivista, un magazine o altro ci chiedeva delle
fotografie, noi ci limitavamo a fare da tramite con la fotograf\* e non
chiedevamo nessuna percentuale. Da subito abbiamo lasciato a tutt\* la
possibilità di contribuire al progetto per sostenere le spese vive che
abbiamo, che siano ess\* produttrici o lettrici di immagini. A maggior
ragione invitavamo chi riceveva un compenso per il suo lavoro attraverso
Arcipelago-19 a sostenere il progetto, così ci sarebbero state
possibilità anche per altr\*. Non siamo un'agenzia, non siamo niente di
formale in realtà, ma cerchiamo di essere un qualcosa di collettivo dove
ci si possa sentire comod\* e liber\*.

Abbiamo cercato di costruire progetti collettivi come quello sulle
persone che guarivano dal Covid-19, perché a un certo punto volevamo
raccontare le storie di persone che erano spesso considerate solamente
numeri. Siamo andati a parlarci, ascoltando la loro storia e
fotografandoli. Uno di questi è stato fatto a Manziana, vicino Roma,
dove il sindaco ci ha chiesto di fotografare i guariti del loro paese.
Così 4-5 fotograf\* di Arcipelago-19 hanno incontrato le persone con
calma, facendo scatti collettivi, dove in una logica collettiva non ci
sono nomi singoli delle fotografe e fotografi. Questa è una cosa
positiva della pandemia, Arcipelago-19 è una comunità che funziona e
collabora, un archivio vivo che ancora sta andando avanti e che sta
raccontando pezzi d'Italia e storie del nostro paese nel presente, ed è
riuscita ad andare avanti in uno spirito collettivo e non competitivo
affinché tutt\* potessero sentirsi bene, fare il proprio lavoro in
maniera serena, raccontando storie e spaccati di realtà. È un progetto
collettivo aperto, dove ognun\*può proporre storie, progetti, modalità o
qualsiasi altra cosa.

Spesso ragioniamo sul concetto di «dietro l'angolo», sia per un discorso
di prossimità, ma anche di storie nascoste o ancora non raccontate.
Questa metafora ci aiuta a capire cosa vogliamo raccontare. Quali tipi
di storie? Quelle storie che raccontano piccoli pezzi di comunità, che
spesso non vediamo perché magari non siamo abituati a guardare vicino,
essendo molto proiettati verso l'esterno.

Abbiamo provato a creare solidarietà attiva attraverso Arcipelago-19
sostenendo la lotta del Collettivo di Fabbrica della GKN attraverso
un'asta di fotografie, *Asta la vittoria*, in cui le persone potevano
fare un'offerta su una fotografia partendo da un prezzo base. Siamo
riusciti a sostenere la lotta mettendo a disposizione la fotografia e
creando così consapevolezza e aiuto concreto.

Nel corso del tempo il gruppo si è allargato, si sono creati cerchi
concentrici, si cerca di vedersi di persona tutt\* almeno una volta
l'anno per stare insieme e capire da che parte sta andando il mondo e
dove vogliamo invece andare noi. Ci sono stati progetti collettivi e
tentativi di collaborazioni con realtà più o meno istituzionali. Ciò che
rende particolarmente libero Arcipelago-19 è che nessun\* di noi è
strettamente vincolat\* al progetto e in tal senso difficilmente c'è
qualcosa che noi *dobbiamo* fare, mentre cerchiamo sempre di trovare un
qualcosa che *vogliamo* fare. Un lusso che ci vogliamo permettere e che
è sempre stato, anche senza dirselo, uno dei nostri obiettivi.

Il Covid forse avrebbe dovuto insegnarci anche questo: che fino a che
tutt\* non sono in salvo, allora nessun\* è in salvo, ma a me non sembra
che ci abbia lasciato molto su questo tema. Quindi un metodo forse è un
po' questo, tornare a guardare la prossimità, le storie che hanno tutta
la dignità di essere raccontate, affinché tutte quelle piccole cose, che
già prima non andavano bene, non torniamo a farle subito dopo. La
pandemia potrebbe essere un'occasione, una scusa, per eliminare un po'
di scorie e ripartire meglio.
