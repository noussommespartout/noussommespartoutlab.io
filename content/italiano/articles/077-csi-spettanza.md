---
author: "Csi (Centro di salute internazionale e interculturale)"
title: "La spettanza non coincide con l'erogazione"
subtitle: "Il dispositivo di ridistribuzione economica raccontato alla nonna"
info: "Testo scritto per la raccolta bolognese"
datepub: "Apprile 2022"
categories: ["lavoro, precariato, lotte sindacali", "corpo, salute, cura"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "77"
quote: "una bella cosa"
---

{{%epigraphe%}}

*«Se perdo il senso politico, non riesco più a legittimare a me stessa
il dispositivo in un'ottica sacrificale. Il dispositivo è troppo
avanzato rispetto a dove siamo».*

*«Con i suoi limiti e criticità mi sembra una delle cose più
innovative che abbiamo fatto».*

*(verbale riunione 15 gennaio 2018)*

{{%/epigraphe%}}

Pranzo con i nonni. Ancora una volta nonna mi chiede di cosa mi sto
occupando adesso e quando ho intenzione di cominciare una
specializzazione per diventare un «medico vero». Poi, per l'ennesima
volta, mi racconta di quando ha iniziato a lavorare lei, in fabbrica a
12 anni, di come c'è rimasta per 40 anni, la fatica che faceva, e però
il salario, il posto fisso... Io le spiego cosa sto facendo al momento
e quali sono gli obiettivi dell'associazione di cui faccio parte: la
promozione della salute intesa come un bene collettivo e la
trasformazione sociale nella direzione dell'equità, della giustizia
sociale e della sostenibilità.

Nonna: «Ah, è una bella cosa. Ma hai un contratto? È un'azienda? Quanto
ti pagano?»

Io: «Non è un'azienda!!!!! Siamo un gruppo interdisciplinare
orizzontale° e non abbiamo ruoli definiti.»

Nonna: «Orizzontale, in che senso?»

Io: «Nessuno è il capo di nessuno, siamo tutt3 allo stesso livello e
prendiamo tutte le decisioni insieme.»

Nonna: «Ah, quindi è volontariato?»

Io: «No, ci siamo costituite come associazione anche per rispondere al
bisogno di reddito di alcuni/e soci/e, soprattutto delle professioni che
non sono riconosciute a livello sociale, come lo sono quelle mediche.
Infatti, la sostenibilità economica dell'associazione non è scissa dalle
sue finalità politiche, e anzi è parte integrante della sua
realizzazione.»

Nonna: «Oddio, mo' la politica che c'entra?»

Io: «C'entra perché cerchiamo di lavorare sulla salute in un altro modo,
non ci occupiamo solo della malattia ma anche di tutte le condizioni che
influiscono sulla salute, come la casa, il lavoro... E poi cerchiamo di
contrastare una certa gerarchia di riconoscimento sociale delle
professioni e dei saperi e di costruire uno spazio di lavoro libero che
si discosti quanto più possibile dalle classiche relazioni di lavoro
basate su subalternità e sfruttamento. Insomma è uno spazio e uno
strumento di liberazione reciproca.»

Nonna: «Ah, quindi prendete tutt\* lo stesso?»

Io: «No, ognun\* fa una richiesta a seconda di quanto ha bisogno,
ovviamente guardando a quanti soldi abbiamo in cassa, e
indipendentemente da quanto lavora»

Nonna: «Mmm... ma i soldi da dove arrivano? Dallo stato?»

Io: «Anche. Oppure da associazioni private o fondazioni, però cerchiamo
di evitare aziende che sono coinvolte in attività che nuocciono alla
salute, tipo armi o multinazionali del cibo. Ovviamente i finanziamenti
dai privati ci permettono una più ampia libertà di manovra... ricordati
quando è tempo del 5x1000.»

Nonna: «Ma, alla fin fine, tu quanto guadagni?»

Io: «Adesso non prendo nulla, lavoro gratis, perché non ho bisogno...»

Nonna: «Va beh... vado a prendere il filetto prima che si bruci!»

## Io e il dispositivo di ridistribuzione economica

Rientro dopo un periodo di pausa in cui ho avuto dei problemi in
famiglia e dove ho continuato a chiedere soldi al dispositivo con una
cifra per me minima, 300€ al mese. Il resto dello stipendio è stato
coperto grazie alla disoccupazione che avevo accumulato da altri lavori
part-time.

Faccio parte del dispositivo fin dagli inizi, grazie all'esistenza di
questo meccanismo di redistribuzione economica ho potuto continuare a
lavorare all'interno dell'associazione con le compagne con cui ho
iniziato questo percorso anni fa, ho potuto portare avanti dei progetti
al di là delle tempistiche dettate dai bandi, ho vissuto molti momenti
critici in cui non si sapeva se saremmo riuscite a trovare dei soldi per
andare avanti, ma alla fine ce l'abbiamo sempre fatta.

Ricordo ancora il primo giro di interventi in cui ci chiedemmo di quanti
soldi avremmo avuto bisogno, io avrei voluto dire «almeno 1000€!» come
era la borsa di dottorato che avevo avuto fino a quel momento... ho
iniziato con 300€! Poi la richiesta variò col variare dei miei bisogni
di vita e non necessariamente con il tempo che dedicavo ai progetti. Per
ora ho sempre fatto richieste pensando ai bisogni primari, sapendo che
in caso di spese di emergenza posso far affidamento sui miei. Questo
spesso mi fa chiedere se tutto ciò è sostenibile solamente perché i miei
mi possono parare il culo...

Iniziare è sempre la parte più difficile. Mi ricordo quando sono
«entrata nel dispositivo» per la prima volta, quanta timidezza all'idea
di chiedere quei «due bruscolini» --- come li definivo fuori da quello
spazio --- quanta paura del giudizio e dell'autogiudizio, non solo e non
tanto di quanto avevo chiesto, ma di se e come il mio contributo si
rispecchiasse in quella cifra, del solo fatto di chiedere.

Iniziare è sempre la parte più difficile, ma anche reiniziare non
scherza, e di reinizi ce ne sono stati tanti, ce n'è proprio uno bello
fresco adesso qui davanti e tutto intorno a me. Nel tempo, dalla prima
volta, ho sentito tante cose: la specialità, la normalizzazione,
l'importanza, la pesantezza, la cura, l'assenza di giudizio, il
giudizio devastante, l'oppressione, la libertà estrema.

A volte sono grata che ci sia. A volte lo odio. È un percorso infinito
sempre e per sempre imperfetto. È un nuovo reinizio a ogni giro.
