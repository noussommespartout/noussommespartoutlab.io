---
author: "Elena & Rina"
title: "Il giorno doppo"
subtitle: "Vivere la città dopo una manifestazione"
info: "Testo scritto per la raccolta svizzera romanda"
datepub: "Settembre 2020"
categories: ["manifestazione, rivolta, protesta", "autoriflessione sulla militanza"]
tags: ["amour, love, amoureuxse", "corps", "crier", "eau", "empathie", "force", "gare", "horde", "mcdo", "rue", "thé, tisane", "usine", "voix"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "3"
quote: "di invadere tutti i muri"
---

Oggi ricomincia la routine, le macchine circolano, si cammina nelle aree
designate.

Fermarsi col rosso, ripartire col verde.

Attraversare sulle strisce pedonali, ben parallele.

Disinnescare qualunque contatto visivo.

Tracciare la propria strada in una città-fabbrica, funzionale,
ergonomica.

È strano. Ho l'impressione di essere all'estero o in un sogno, non
riconosco del tutto la "mia" città. La sensazione è quasi fisica. Mi
sento sola. Capisco ben presto il perché.

Oggi, in Piazza della Riponne (Losanna), il mio corpo si sorprende a
tracciare una diagonale in linea retta, quella dell'itinerario
quotidiano che mi porta al lavoro. Una diagonale efficace: il minor
numero di passi per il massimo della rapidità. Qualcosa mi spinge a
prendere una strada diversa, più caotica. Ma da sola, il caos non ha più
senso.

Uscendo dalla piazza vedo uno *stencil* viola sotto una panchina. Anche
lui è solo. La cosa mi rende un po' triste e mi conforta allo stesso
tempo. E poi mi ritrovo alla stazione, dove ieri facevo parte di una
banda, di un'orda, di uno sciame di api senza regina: è stata una delle
più belle manifestazioni della mia vita, la prima di quella portata. La
configurazione dello spazio, l'organizzazione della circolazione, tutto
appare trasfigurato. Poche ore fa la strada era nostra, la stazione era
nostra, tutta la città era nostra. Tornarci per andare al lavoro è un
piccolo esproprio, un giro sulle montagne russe in un luna park tutto
scassato.

Da quel giorno mi è successo un sacco di volte. La mappatura mentale
della città in cui vivo cambia. Ogni atto di militanza trasforma il mio
rapporto con le strade, le vetrine dei negozi, gli incroci. Ogni volta
che, in mezzo a una folla di persone, mi sono ripresa un pezzo di città,
l'ho riperso il giorno dopo. È paradossale, ma più la città mi
appartiene, più è duro l'esproprio del giorno dopo.

Lo spazio che, per qualche ora, era diventato una proprietà collettiva,
cioè uno spazio realmente pubblico, fa marcia indietro e torna a
frammentarsi: una manciata di metri quadri a starbucks, un'altra a
mcdonald's, un'altra alla coop, un'altra a h&m, un'altra ancora a
un'azienda senza nome che specula sulle materie prime. Tutto il resto
va allo Stato, col suo controllo, le barriere, i sensi unici, gli spazi
sorvegliati, riservati solo a una parte della popolazione, quella che è
"in regola" e che aspetta, in una democrazia inquadrata dalle lobby che
difendono quei metri quadri privati, di ottenere più diritti. Mi
verrebbe da dire: cose regalate, mai più ridate.

Il giorno prima eravamo una massa con un obiettivo comune, gridavamo
insieme gli stessi slogan, chiedendo diritti che non per forza ci
riguardavano personalmente, esigendo una società nuova per tutt\*. Il
collettivo va oltre la propria persona, i propri interessi individuali.
È una configurazione sociale e politica in cui si guadagna libertà. Si
possono scrivere graffiti sulle vetrine delle banche che difendono, dal
canto loro, i diritti delle imprese private: il diritto all'ecocidio,
alla precarietà impunita; si possono bloccare le autostrade per
costringere la gente a riflettere sulle violenze domestiche e di genere.

Il giorno dopo ti ritrovi a camminare all'ora di punta, in mezzo a una
massa di individui isolati che pensano agli affari propri. Una massa di
persone che, a ben vedere, si controllano a vicenda: non si può più
gridare, scrivere graffiti, costringere i multimiliardari a condividere
la ricchezza e a interrompere i loro genocidi commerciali.

Tutto sommato non siamo in tant\* a provare queste sensazioni, perché
non siamo in tant\* a gridare o ad aprire case occupate con l\*
senzatetto. E così si apre un baratro tra la tua squadra e il resto del
mondo, che ti appare incomprensibile e insensato. Sai che la tua
rappresentazione dello spazio è diversa. Vorresti condividerla con
tutt\* l\* passanti ma non è possibile. Ci hanno addestrat\* bene.

E così continui a camminare. Ecco la stazione dove sono stati arrestati
alcun\* compagn\*, il commissariato di polizia davanti al quale abbiamo
aspettato che uscissero per accoglierl\* calorosamente, la piazza dove
ho ballato tette al vento, il cespuglio dietro al quale ho buttato
bomboletta e *stencil* vedendo dei poliziotti, il bar dove ho bevuto un
tè caldo durante un blocco in pieno inverno, la strada dove ho contato
l\* agenti mobilitat\* per avvertire l\* compagn\* che stavano
preparando un'azione, un'altra strada dove ho perso la voce a forza di
gridare, il grande edificio vuoto da anni che abbiamo provato a occupare
-- nei bagni dell'ultimo piano c'è l'acqua corrente.

Ecco il vicolo dove una volta ho letto:

*NI*

*UNA*

*MENOS*,

scritto in rosso a lettere maiuscole su un foglio A4 bianco incollato in
fretta e furia su un muro a 20 metri dalla stazione di polizia. Questo
volantino mi riempie tanto di dolore quanto di forza e mi salgono
lacrime di rabbia al pensiero di tutte le persone che amo e di tutte
quelle che non conosco.

Mi viene voglia di ridipingere le facciate di tutti i palazzi della
città, di invadere tutti i muri; finché non si potrà più guardare
altrove, perché non ci sarà un altrove; finché non potranno più sentirsi
pensare, perché saremo ovunque. In tutte le strade, voglio che ci siano
atti di depredazione che siano opere d'amore. Sogno un atto di empatia
generale e un'insurrezione collettiva.

