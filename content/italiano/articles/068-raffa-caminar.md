---
author: "Raffa (Rete appenninica femminista)"
title: "Caminar para sanar"
subtitle: "In cammino contro il terricidio°"
info: "Testo scritto per la raccolta bolognese"
datepub: "Marzo 2022"
categories: ["femminismi", "transfemminismoqueer, queer", "manifestazione, rivolta, protesta", "corpo, salute, cura", "giustizia ambientale, ecologia", "internazionalismo"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "68"
quote: "Sete di acqua, sete di amore, sete di rivoluzione!"
---

Da poche persone si è creato un grande movimento che ha attraversato i
monti e le pianure. C'era un bel sole, avevamo molto caldo, i piedi
bruciavano di stanchezza, il cuore batteva forte verso un obiettivo
comune. Il suono dei tamburi accompagnava ogni passo. Poi finalmente
siamo arrivate piene di sete. Sete di acqua, sete di amore, sete di
rivoluzione!

C'è poco tempo non siamo molte ma ci stuzzica l'idea di lasciar correre
l'immaginazione, ci piace l'utopia, la grandezza ci motiva, sapere delle
compagne argentine ci sprona, decidiamo di impegnarci per due giorni di
cammino --- di connettere Appennino e bassa, lotte diverse in difesa del
territorio --- e una notte: campeggiare ai Prati di Caprara.

Ci dividiamo in piccoli gruppi: noi Raffa ci occupiamo della prima
giornata da Sasso Marconi a Bologna (percorso, comunicazione,
logistica...) mentre la Brigata solidarietà donna della seconda nella
bassa.

Avevo ascoltato molte interviste e testimonianze delle donne argentine,
in particolare sull'effetto devastante delle megaminiere sulla salute e
il territorio.

Mi aveva colpito in particolare una donna indigena che parlava di quanto
soffrono le persone e i bambini. Quando ha preso la decisione di
partecipare alla camminata si è sentita subito meglio: nel suo piccolo
non era più così impotente, aveva preso una decisione.

Questo è stato vero anche per me e ho ringraziato la possibilità di
poter condividere il sentire di persone tanto lontane.

Decidere di fare un gesto così piccolo faceva la differenza: non dovevo
più subire.

{{%ligneblanche%}}

Berta Caceres, Marielle Franco, Greta Thunberg i volti delle guardiane
della terra, le martiri della resistenza lesbica femminista ecologica
del nostro tempo e le antenate future, sono solo alcune delle donne che
ci hanno guidato spiritualmente nella marcia contro il terricidio. 

E così in pochissimo tempo la marcia ha preso corpo e gambe.
L'organizzazione leggera fatta di piccoli gruppi, alcune settimane di
scambi e di attivismo che finalmente sento ritornare vivo e pieno di
significato. Ed è quando vengo contattata da una compagna di Radio Onda
Rossa che capisco che da queste remote zone dell'Appennino, fuori dalle
dinamiche politiche urbane --- social-aperitivo --- abbiamo aperto uno
spazio di lotta dal basso, con le nostre risorse, che se le volessimo
quantificare dal punto di vista materialistico sono veramente
pochissime...

Cosi abbiamo camminato, gridato, suonato i tamburi, attraversato strade,
sentieri, parchi, piste ciclabili, parlato, letto comunicati durante le
pause, raccolto immondizia, fronteggiato gli sbirri alla partenza senza
farci intimidire e bloccare, dormito con le tende ai Prati di Caprara
che per una notte è stato il nostro SPAZIO SICURO, litigato su uno
stupro del recente passato.

{{%ligneblanche%}}

 Una delle cose per me importanti della camminata è stato il sapere che
i nostri corpi, le nostre voci, i nostri tamburi e stendardi, la nostra
postura, insieme e individualmente, hanno attraversato un territorio
informandolo. Non solo sul piano umano, informando il suolo, l'acqua, il
fiume, gli alberi, l'aria, gli animali, le piante e lasciandosi
informare a loro volta dall'ambiente.

Quando siamo arrivate a Caprara, ero stanca dopo la lunga giornata di
cammino. Felice ma stanca. I pochi resti delle strutture militari sullo
spiazzo di cemento crepato mi sono sembrati un paesaggio inquietante
dopo la camminata lungo il corso del fiume. Sapevo che solo un piccolo
gruppo di noi avrebbe passato lì la notte e mi sono sentita tesa,
insicura di voler restare. La paura che qualcuno potesse sorprenderci
addormentate o che le forze dell'ordine potessero venire a cacciarci e
la possibilità di un letto a casa di una delle compagne mi pareva una
prospettiva molto più allettante. Poi siamo state a lungo nel piazzale
ad ascoltare i racconti di lotta della gente del comitato per i Prati di
Caprara, compagne e compagni sono venuti a salutare, condividere cibo,
vino e il luogo pian piano si è fatto familiare. Dal prato alto e
incolto si sprigionava un meraviglioso profumo di menta, il cielo ---
solcato spesso dall'elicottero dell'Ospedale maggiore --- era pieno di
stelle. Nel piccolo gruppo rimasto si è creata un'intimità avvolgente e
ho sentito la gioia di una bella avventura condivisa, il diritto e
l'importanza di essere lì a dormire insieme, avvolte dai teli sottili
delle nostre tende sgargianti.

{{%ligneblanche%}}

Sono arrivata la domenica mattina alla stazione di Bologna. Faceva caldo
e brillava il sole. Un gruppo di donne con zainetti, scarponcini e
cartelloni aspetta all'ingresso della stazione. Mi sono avvicinata e ho
salutato le compagne che conoscevo, e subito dopo ho iniziato a parlare
con le due, poi tre, poi cinque e poi sette donne e ragazze che invece
non avevo mai visto e che avevano saputo della camminata attraverso la
radio, i social o un giornale. Non avevo capito che fossimo riuscite a
diffondere la notizia! «Bene cosi!» Ho pensato.

Mi piace molto sentire l'euforia che si diffonde prima di iniziare una
manifestazione e quella mattina ho avuto l'impressione di non essere
l'unica a goderne.

{{%ligneblanche%}}

Alla stazione di San Giorgio in Piano, ci hanno accolto i tamburi, le
donne della bassa gridavano festanti, ci aspettavano! Aspettavano il
nostro arrivo e ci hanno dato il benvenute, mostrando fiere i loro
cartelli, striscioni, bandiere, battendo forte sui loro tamburi, alzando
le voci per dirci, finalmente siete qui, finalmente siamo insieme!

{{%ligneblanche%}}

*No terricidio!*

*No ecocidio°!*

*No genocidio!*

*No femminicidio!*

*No epistemicidio°!*

{{%ligneblanche%}}

Ho conosciuto gruppi di donne in lotta da poco, quando andai alla marcia
era la prima volta che mi univo a una manifestazione di donne e sole
donne, anche se sentivo la mia lotta femminista nella mia vita
quotidiana. Vedere guidare la marcia da donne e andare verso un campo,
attraversare la pianura, la natura (seppur antropizzata). Questa visione
simboleggia due elementi, la donna e la terra che oggi mi sono più di
guida.

Eravamo in tante, tantissime. Tantissime e tantissimi, le persone si
affacciavano sui balconi e ascoltavano le nostre grida, la nostra
allegra confusione. Era una una protesta gioiosa e contagiosa. Una
protesta d'amore per un mondo migliore, al di là del male, al di là del
pregiudizio, al di là della distruzione.

A San Pietro in Casale la gente ci attendeva all'ingresso del paese,
riunita per vederci passare, per dirci siamo con voi, sostenere i nostri
passi. Ci siamo allargate nella piazza, cantando:

{{%ligneblanche%}}

*Anche per quest'anno ragazze ci han fregato,*

*un bel polo logistico ad Altedo ci han mandato,*

*promesse di lavoro in grande quantità,*

*ma un tipo di lavoro che nega libertà...*

*Ramarri e lucertole di qui dovran sloggiare,*

*il falco pellegrino se ne dovrà andare,*

*Le nostre care rane più voce non avran*

*E tutta la natura in ginocchio metteran*

...

{{%ligneblanche%}}

E scandendo: *Siamo il grido altissimo e feroce di tutte quelle donne
che più non hanno voce!*

{{%ligneblanche%}}

Ho partecipato solo a una piccola parte della camminata, ma è stato un
grande evento, C'era tutta la mia famiglia bolognese e c'era mia madre,
evento raro. La lotta per l'ambiente e delle donne è una delle poche
cose che accomuna me e mia madre. La marcia è stata un gesto unificante,
è stato bello camminare sul territorio. Camminare, fermarmi a parlare
con chi mi accompagnava, conoscere tante donne, cantare insieme,
attraversare.

Il caldo della bassa, asfalto senza alberi, lungo faticoso, la sete. Un
serpente di donne e corpi di genere dissidente che attraversano la
pianura. I bambini e le donne di Ponticelli ci vengono incontro per
dirci che siamo quasi arrivate, per sostenerci coi canti.

{{%ligneblanche%}}

*Bella ciao*

*Bella ciao*

*Bella ciao ciao ciao*

*una mattina*

*mi son svegliata*

*e ho trovato l'invasor!*

{{%ligneblanche%}}

Alla casa ci offrono acqua e cibo per riuscire a proseguire fino alla
risaia.

Cantare insieme ai cori delle mondine, sedute davanti a loro come
conclusione della camminata, arrivate finalmente vicino alla risaia.

Oh! Ma il progetto del nuovo hub logistico lo hanno sospeso o proprio
cancellato? Comunque la camminata è davvero servita, unite alle altre
mobilitazioni sul territorio per fermare un piccolo tentacolo di questo
schifoso capitalismo terricida.

{{%ligneblanche%}}

Il 22 e il 23 maggio 2021 la Raffa - rete appenninica femminista e
Brigata solidarietà donna hanno organizzato --- con il sostegno e la
partecipazione di tante associazioni, gruppi territoriali, collettivi,
gruppi informali, singole donne e persone di genere dissidente, tra cui
i cori delle mondine --- due giorni di cammino da Sasso Marconi ad
Altedo, per denunciare il terricidio che avviene quotidianamente sul
nostro territorio. La Camminata è nata in risposta al appello di Moira
Millán --- cofondatrice del Movimiento de Mujeres indigenas por el buen
vivir --- di unirsi ovunque alla marcia argentina *Caminar para sanar*
per far riconoscere il terricidio come crimine contro la natura e di
lesa umanità.
