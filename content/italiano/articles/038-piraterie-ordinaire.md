---
author: "El."
title: "Pirateria quotidiana"
subtitle: "Qualche idea di sabotaggio creativo"
info: "Testo scritto per la raccolta svizzera romanda"
datepub: "Aprile 2020"
categories: ["sabotaggio, azione diretta", "DIY, tutorial"]
tags: ["alcool", "bouffe", "coller", "graines", "internet, web", "logo", "nuit, obscurité", "rire, se marrer, rigoler"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "38"
quote: "per dirottare la rete, puoi..."
---

Non mi sono mai piaciute le ideologie. Hanno il vantaggio di fornire un
quadro di riferimento, di funzionare come isolotti dove orientarsi ,
come spazi da esplorare con qualche certezza in più in mezzo al grande
lago dei possibili. Le ideologie delimitano ciò che si può dire e
all'interno di quale linguaggio le parole possono avere un senso. Ci
aiutano a parlare, a pensare, ma quando le pensiamo fino in fondo,
finiamo sempre per situare ciò che pensiamo, per dirci che i limiti di
ciò che è giusto sono quelli del linguaggio che abbiamo scelto per
descrivere il mondo.

Preferisco quindi agire localmente, usare la mia persona per distrarre
il quotidiano, disturbare le evidenze oppressive, trovare il contrordine
dell'ordine delle cose.

Agire non è così diverso dal pensare: anche l'azione ha bisogno di
essere situata. Il senso della tua azione è relativo alla tua posizione,
e se la tua posizione è quella di essere un numero, una percentuale
vagamente in cerca di lavoro, una pedina nel grande gioco funebre della
merce totalitaria, tanto vale agire come un\* pirata.

Da qui l'idea di mettere in pratica una pirateria quotidiana e
facilmente condivisibile. Bisognerebbe fare l'elenco dei microgesti
capaci di interrompere il segnale, di creare una perturbazione locale
della rete, di squarciare l'ideologia dominante per svuotarla dalle
fondamenta.

Quello che succederà in seguito, lo vedremo. Lo inventeremo
collettivamente. Non abbiamo bisogno di aiuto per inventare le nostre
vite. Se lo spazio in cui vivi ti definisce come un\* utente è perché ti
domina, è perché la grande rete della disciplina non ti fornisce le
forbici che ti permetterebbero di ritagliarti una zona vivibile.

L'elenco che segue è costituito in parte da idee rubate altrove, in
parte da scoperte. Ma non si scopre mai nulla, al massimo si riproducono
strategie di cui si ignorava l'esistenza.

Per dirottare la rete, puoi...

... fare un calendario mensile con un elenco di eventi che propongono
cibo gratis (inaugurazioni, conferenze, prime di spettacoli, ecc.) e
distribuirlo alle persone che ti circondano.

... viaggiare in autostop.

... creare orti nei parchi pubblici.

... disegnare le tue proprie striscie pedonali e piste ciclabili con
nastro adesivo e vernice.

... stampare più volti sulle tue magliette per ostacolare il
riconoscimento facciale.

... cambiare gli slogan sui manifesti pubblicitari.

... con uno stencil, disegnare un logo "disabile" su tutti i parcheggi
di un supermercato locale.

... usare fucili ad aria compressa per disattivare le telecamere di
sorveglianza della tua città.

... rispedire al mittente le buste senza affrancatura degli annunci
pubblicitari dopo averci infilato dentro ancora più pubblicità.

... con un semplice cacciavite e un telone, trasformare la panchina di
un parco pubblico in un riparo gratis per la notte.

... costruire fioriere spontanee lungo i marciapiedi usando vecchi
pneumatici, cassonetti dei rifiuti o pallet recuperati.

... mettere del colorante alimentare nelle fontane pubbliche per far
ridere bambin\* e passanti.

... usare le casse automatiche e scansionare solo i due terzi dei
prodotti che compri.

... piantare semi "indistruttibili" (come il kudzu) nei parchi pubblici
o intorno agli edifici sfitti per accelerare la rivitalizzazione delle
città.

... rendere inutilizzabili le auto della polizia e le macchine edili
infilando una patata nel tubo di scarico.

... inceppare una serratura con una siringa piena di colla epossidica
mischiata con un po' di alcol.

... frugare nelle spazzature dei supermercati e fare mercatini gratuiti
sui marciapiedi.

... rovinare i codici a barre dei monopattini elettrici con un
cacciavite o del nastro adesivo per moquette.

... incollare un pulsante "*skip ad*" sui manifesti pubblicitari.

... intrufolarti di notte nei cantieri per costruire castelli e sculture
con la sabbia o la ghiaia. Faranno sorridere l\* operai\* l'indomani
mattina.

... dare ai tuoi vicini il codice della tua rete wifi.

...mettere dei vecchi tappetini sui fili spinati per scavalcarli in
tutta sicurezza.

... avvolgere un panno intorno ai dissuasori anti-piccioni, in modo che
gli uccelli ci si possano di nuovo posare sopra.

... installare altalene sotto i ponti, sotto gli alberi, sotto le
cornici dei parcheggi.

... proteggerti dai cani poliziotti usando il pepe di cayenna, che
blocca momentaneamente il loro olfatto (non farà loro male).

... mescolare semi, terra e argilla per creare bombe di semi che
rinverdiscano la tua città.

... recuperare biciclette dalle discariche o dalle officine, dipingerle
tutte dello stesso colore. Scrivere "biciclette gratis" sui telai e
disseminarle per la città.

...rubare merce nuova e tornare il giorno dopo per farti rimborsare i
soldi e distribuirli.

...rispondere a tutte le offerte di lavoro con lettere in cui spieghi
quanto non vuoi lavorare per loro.

...innestare alberi da frutto su alberi pubblici in modo che inizino a
fruttificare (cerca su internet le specie compatibili).

...lasciare recensioni negative e commenti sprezzanti a tutte le
stazioni di polizia della tua città su google maps.

...rubare le luci di Natale dai negozi di lusso e montarle in posti
inaspettati.
