---
author: "Franco Sicilia"
title: "Solidarietà allo stato puro"
subtitle: "Cronaca del presidio al parcheggio Tanari"
info: "Testo scritto per la raccolta bolognese"
datepub: "Marzo 2022"
categories: ["manifestazione, rivolta, protesta", "lavoro, precariato, lotte sindacali", "solidarietà, mutualismo"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "74"
quote: "chi tocca uno tocca tutti"
---

Chi tocca uno tocca tutti. In Usb questo è lo slogan, la frase che
spesso si sente, si legge in questo o quel comunicato attinente una data
rivendicazione, una lotta da sostenere con presidi o altre azioni
sindacali. Non è un motto simbolico fine a se stesso. È molto sentito, e
travalica il mero gergale sindacalese e, soprattutto, è trasversale a
tutte le fasce di età. Appunto, dai più giovani ai meno giovani e ai non
più giovani come me. Anzi il concetto insito in questo slogan parifica e
corrobora tutti.

Alla base del presidio di 12 giorni, con occupazione di fatto del
parcheggio comunale Tanari a Bologna, c'è, ancora una volta, questo
slogan. Forse pure di più in quanto i 4 lavoratori licenziati tramite
sms dalla multinazionale BoMob, che gestisce l'appalto, non sono
iscritti al nostro sindacato e due di loro a nessun altro. Vale a dire
solidarietà allo stato puro.

Tutta questa premessa un po' per sottolineare che il presidio al Tanari
è partito da subito sotto un buon auspicio. Quattro lavoratori da
difendere e nient'altro.

Ci si è avvicendati in tanti secondo le personali disponibilità. È
naturale quindi che ci sia stato chi ha dato di più, soprattutto di
notte, dormendo sul posto, sul pianale di un rimorchio per automobile
allestito a mo' di tenda da camping. Le temperature, essendo in
febbraio, non sono state certo miti pur disponendo di un piccolo camino,
debitamente preparato da un compagno e ricavato da un fusto di metallo,
continuamente alimentato con bancali in legno che in tanti si andava a
prendere dalle varie aziende della zona (gratis perché vengono gettati),
ma anche con l'acquisto di vera e propria legna da ardere ben più
duratura. Tanti compagni hanno portato vettovaglie di vario genere ed in
generale abbiamo avuto la solidarietà di tante persone, dal locale
circolo del Pd (?!) agli autisti degli autobus (che spesso invitavamo a
prendere un caffè), cui pure abbiamo bloccato l'ingresso al parcheggio
essendo questo dotato di apposita fermata all'interno per prelevare gli
utenti che vi lasciano l'auto in sosta. Gli utenti
stessi si sono mostrati abbastanza comprensivi, quando non solidali, con
l'eccezione di pochissimi casi totalmente refrattari al concetto di
solidarietà. Finanche gli agenti di una pattuglia di polizia ci hanno
manifestato il loro sostegno verbalmente.

Ci sono stati un paio di momenti di convivialità, una grigliata e una
serata con musica e cantante. E naturalmente non sono mancate le
discussioni tra noi compagni sia per la gestione del presidio stesso ma
anche sulle altre iniziative e lotte parallele che come sindacato si
porta avanti. Di certo il presidio non è stato un impegno da poco,
proprio per la sua durata e per le forze che si è dovuto mettere in
campo. Quando è arrivato il momento di decidere se proseguire o smettere
eravamo tutti indubbiamente stanchi. C'era stato un incontro online con
una delegazione del comune, presenti alcune associazioni cittadine e un
delegato della stessa BoMob, piuttosto deludente perché a parte le
solite belle parole non si era concretizzato niente. Per tale ragione,
nella stessa giornata, un gruppo di compagni si era spostato sotto la
sede dell'azienda manifestando un vivace disappunto. Ne è seguito un
incontro con una delegazione di compagni e un convincente impegno da
parte di BoMob a incontrarsi nuovamente all'indomani per risolvere la
questione. Su questa base si è deciso di sospendere l'occupazione del
parcheggio. C'è stato chi avrebbe preferito aspettare ancora qualche
giorno, ma ha prevalso forse la stanchezza. In ogni caso, sono seguiti
altri due incontri e la questione sembra ormai in via di soluzione.

In ultimo mi sembra non superfluo sottolineare come il presidio al
Tanari sia stato chiaramente emblematico di quanto la lotta paghi (anche
in termini di calore e affiatamento) e di quanto «chi tocca uno tocca
tutti» non sia in Usb solo un semplice slogan.

{{%ligneblanche%}}

*P.S. Nell'aprile 2022, quel «sembra ormai in via di soluzione»
utilizzato nella cronaca, scritta praticamente in presa diretta, si è
trasformato in accordi concreti. Dei quattro lavoratori coinvolti nella
vertenza, i primi due hanno raggiunto la disoccupazione (in un caso con
buonuscita), gli altri due la riassunzione a tempo indeterminato e
pieno, con il riconoscimento dell'anzianità pregressa.*
