---
author: "Anonim*"
title: "Bello come..."
subtitle: "Testimonianze fuori e dentro il carcere all'indomani delle rivolte di marzo"
info: "Testo scritto per la raccolta bolognese"
datepub: "Aprile 2022"
categories: ["carcere", "manifestazione, rivolta, protesta", "polizia, repressione, amministrazione giudiziaria", "solidarietà, mutualismo"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "73"
quote: "la solidarietà è rivolta"
---

La sera prima a casa di alcune compagne si era discusso sul da farsi.
Erano appena tornate da Modena, dove nel carcere era scoppiata una
rivolta, con tanto di morti, parecchi, sette, otto forse. Le notizie
erano confuse, si parlava di spari da dentro e di un tentativo di
evasione di massa. Le carceri italiane sono in subbuglio da un paio di
giorni ormai. Salerno è il primo penitenziario ad essersi rivoltato,
seguito il giorno dopo da Reggio Emilia, Frosinone, Pavia, Napoli
Poggioreale, il Pagliarelli a Palermo e il Sant'Anna appunto a Modena.
Un compagno da poco uscito da lì ce ne aveva dette di robe che
succedevano a Modena. Un carcere spietato, caotico, un reclusorio puro,
senza servizi di quelli definiti «riabilitativi» o che semplicemente
migliorano un po' la reclusione e che proprio là dentro fosse partita
una dura rivolta non pareva affatto strano.

Le rivolte in carcere non sono un fatto inconsueto. Nelle carceri
italiane ne sono sempre accadute, ma un tale ritmo e una tale forza
testimoniavano una rabbia nuova e un sentire molto diffuso fra la gente
che lì dentro non voleva più stare. Soprattutto in una condizione in cui
sai che chi dovrebbe proteggerti da un nuovo virus sconosciuto sono
quelle stesse persone che mai hanno dimostrato di avere particolare cura
per il tuo benessere. Colloqui sospesi, permessi di uscita sospesi,
pacchi da fuori sospesi, attività educative, sportive e didattiche
sospese, celle chiuse, aria ridotta o azzerata, contingentamento delle
sezioni e in tutto questo secondini e personale che entrano ed escono
come sempre. Indovina chi trasmette il virus a chi? La propria vita
letteralmente nelle loro mani, quelle di secondini e amministratori
penitenziari, carcerieri e burocrati. Da mettersi le mani nei capelli.
E, tanto per integrare, sia d'esempio come pochi giorni dopo si saprà
che il dirigente locale di medicina penitenziaria dell'Ausl aveva
disposto a fine febbraio il non uso delle mascherine per il personale
interno della struttura, per non allarmare chi vi era recluso. Ecco se
questa è la situazione, al posto dei detenuti voi che avreste fatto?

Il telefono squilla, un messaggio, apro e leggo: «È in corso una rivolta
al carcere, stiamo andando». Porca merda, è successo, anche qui è
partita una rivolta... una rivolta in un carcere, ma adesso che si fa?
Non è facile essere all'altezza dei propri desideri e di sicuro non mi
sentivo alla loro altezza proprio quel giorno. Vabbè, poche seghe, si va
di pancia. Prendiamo la macchina e andiamo lì sotto. Qual è il tuo ruolo
storico? Essere dove si fa casino, o fare casino altrove se ci riesci.
La seconda è se brilli per organizzazione, ma quel giorno
d'organizzazione ce n'era poca, e gli eventi non li dettavamo noi,
quindi la prima opzione andava benissimo.

Al nostro arrivo c'è già un capannello di gente, per lo più militanti,
non i soliti dei presidi sotto al carcere però, mi sa che in più persone
hanno *nasato*[^1] che la situazione è grossa. Una volta lì sotto, nel
giro di poco inizia ad arrivare altra gente, fra cui qualche parente.
Saremo un centinaio di persone, forse un po' di più. Di sbirri ce n'è,
anche se sono ben di più di fronte all'entrata, è quel che succede
dentro che li preoccupa davvero. Ecco, infatti, che succede dentro?
Difficile dirlo, gli sbirri non parlano, né con chi ha lo stomaco di
chiederglielo, né coi parenti. Blocchiamo pure un mezzo dei pompieri in
uscita, ma di informazioni ne ricaviamo poche.

Non riusciamo ad avvicinarci granché. Il solito posto, quello dove coi
presidi solidali riusciamo a parlare con i detenuti all'ultimo piano
delle sezioni comuni e AS3, non è raggiungibile, gli sbirri si mettono
nel mezzo e di forzare le cose, il corteino che si forma non se la sente
granché.

Un paio di compagni si arrischiano a fare un giro lungo, passando per i
campi, provando ad arrivare sotto alle mura. La missione ninja fallisce,
i due spariscono e ore dopo ci diranno che non hanno avuto successo,
troppe guardie.

Dopo qualche ora di baccano là fuori la gente inizia a defluire.
Rimangono poche persone, l'entusiasmo scema e si rimane lì, in attesa.
Un'attesa ben ripagata, perché a un certo punto la vediamo. Una nube
comincia a salire, si fa sempre più grande, parte della struttura è
chiaramente in fiamme. Cazzo, quante volte ho urlato «fuoco alle
galere», quante volte c'ho sperato... stavolta è successo davvero. Non è
proprio come me l'immaginavo, ma la gioia è immensa. Nel frattempo
alcune persone compaiono sul tetto del carcere. Gridiamo a squarciagola,
facciamo cori, fischi, chissà se ci sentono. Un gruppo di detenuti ha
occupato la cima del penitenziario. Ci passeranno tutta la notte, fino
al giorno dopo, quando si è di nuovo là sotto. I detenuti sul tetto li
vediamo bene adesso con la luce del giorno, hanno appeso striscioni, in
uno c'è scritto «Indulto» e urlano: «Libertà». Dopo qualche ora però
spariscono. Si saprà solo più tardi che la rivolta è rientrata. Appare
al posto loro un gruppo di guardie coi manganelli che urlano: «lo Stato
ha vinto». Bella vittoria del cazzo, rivolte e proteste in oltre trenta
penitenziari in tutta Italia, uno di questi, a Modena, reso inagibile e
chiuso; negli altri sezioni distrutte, macchine incendiate, guardie
contuse. È vero lo Stato ha vinto, come sempre dopo tutto, altrimenti
non saremmo qui a raccontar di certe storie, ma in questa partita non è
il risultato finale che conta, ma il prezzo che riesci a far pagare. Dal
7 al 10 marzo 2020 il prezzo pagato dallo Stato è stato alto, più alto
del solito quanto meno. Ma c'è anche il prezzo pagato dal campo dei
reclusi. Il conto si chiude qualche giorno dopo: 13 morti, uno proprio a
Bologna. Quasi tutti archiviati per overdose.

Ci penso spesso nei giorni seguenti. A che sarà servito andare là sotto,
tornarci e ritornarci ancora nei giorni seguenti, anche a costo di multe
e denunce, in maniera ostinata, a volte forse un po' ottusa? Forse a
nulla, ma umanamente ci pareva il minimo e poi altre idee --- con le
strade e le piazze deserte, i negozi chiusi e tutto bloccato --- non ne
avevamo. Qualche anonimo --- si è poi saputo --- è salito agli uffici del
Dipartimento dell'Amministrazione Penitenziaria per l'Emilia Romagna e
gli ha spaccato le vetrate; beh non male come idea. Simili notizie
scaldano il cuore, così come le scritte che nel tragitto casa-spesa-casa
di qualche amico si iniziano a vedere in giro. «Fuori a un metro di
distanza dentro otto in una stanza», «Coi detenuti in lotta», «Fuoco
alle galere», «Secondini assassini». Oppure quel «Merde» che campeggia
sulla saracinesca dell'ufficio del sindacato dei secondini, il Sappe. Si
parla di carcere, finalmente.

Nei giorni successivi uscire diventa un'impresa, percorsi studiati,
vestiario preciso, busta della spesa sempre in mano se vai a piedi, bici
scattante per le lunghe distanze. È difficile vedersi, comunicare,
scambiarsi impressioni. I gruppi di affinità si delineano così su base
di vicinato, ci si muove autonomamente con chi è più facile da
raggiungere e poi, alla meglio, si prova a comunicare con chi fa lo
stesso, anche perché di assemblee o confronti a distanza, sulle
piattaforme online che ci stanno insegnando a usare, non se ne parla.
Piuttosto ognun per sé e poi ogni tanto provare a raccordarci, con
qualcuno che in stile staffetta va a capire che succede dall'altra parte
della città, schivando la Digos[^2] che ronza tutto il giorno sotto
casa. Case con giardino. Le poche assemblee collettive le facciamo lì,
sperando che il vicino non ci infami perché vede troppa gente sotto casa
sua. E da quelle assemblee esce fuori sempre il solito: provare a
tornare lì, sotto al carcere, e inventarsi qualcosa per spezzare la
narrazione che delle rivolte sta facendo lo Stato, che le vorrebbe far
passare come il prodotto della brama di farmaci di snervati detenuti
tossicodipendenti, manipolati dai mafiosi e sobillati dagli anarchici.
Così alla chetichella la notte ogni tanto c'è chi decide di farsi una
scappata fuori, attaccare uno striscione, dei manifesti, fare qualche
scritta. Poca roba di fronte a quello che accade attorno, ma almeno si
prova a comunicare anche solo l'esistenza organizzata di qualche
refrattario in giro per la città.

{{%asterisme%}}

Passano le settimane, il governo decide che si riapre, si può uscire di
casa, riprende gradualmente la normalità, che parola odiosa. Cos'è
normale? Andare al cinema è normale, bersi una birra in piazza è
normale, lavoro e sfruttamento sono normali e pure la repressione,
quella ciclica, che butta la gente giù dal letto nel cuore della notte
arrestando compagni e compagne lo è. Ecco perché mi fa schifo la
normalità e non aspiro a riottenerla, perché la serata al cinema, al
ristorante o il giro in centro si reggono sulla pace sociale e la pace
sociale a sua volta si regge sul fatto che chi disturba troppo prima o
poi, perché la situazione non degeneri, deve finire in galera. E
stavolta tocca a me.

Proprio una normalissima operazione repressiva quella che ci tocca.
Sospesa e congelata alla vigilia della chiusura generalizzata e
riattivata appena le cose tornano normali, appunto. Chi cazzo è tutta
quella gente che gira per casa? Situazione anomala vedersi dei tamarri
vestiti casual col passamontagna che gironzolano e frugano, ma ti ci
abitui in fretta. In casa siamo in un bel po' (d'altronde l'affitto e le
bollette sono cari!), ma tutti si prendono a cuore la cosa e tengono gli
occhi aperti, perché di certa gente va guardato bene quel che fa quando
ti entra in casa e quando osserva, deve sentirsi altrettanto osservata.
A una certa il dirigente di turno mi chiama in disparte. Ordinanza di
custodia in carcere. Wow! Stavolta tocca a me, prima o poi d'altronde
doveva capitare, se sei anarchico lo metti in conto. Ma ero in buona
compagnia, scoprirò poi leggendo le carte. Siamo in sette che finiamo
dentro e in cinque con misure come firme e obblighi di dimora lasciati a
piede libero.

Durante le attese burocratiche ho il tempo di leggermi le carte che
motivano gli arresti. Associazione con finalità di terrorismo, per
presidi sotto alle carceri, mobilitazioni contro i centri di detenzione
per immigrati, manifestazioni in solidarietà con compagni e compagne e
un fatto, alcune antenne incendiate in solidarietà con anarchici colpiti
dalla repressione. E poi istigazione a delinquere, attraverso manifesti
e scritti diffusi sui muri e su internet, con cui venivano dette come
stavano le cose nel momento in cui la gente era più incazzata del
solito. Come ben detto da uno slogan scandito a seguito di questi
arresti: «Se la solidarietà è terrorismo, siamo tutti terroristi».
Perché in fin dei conti è questo il succo, si tratta di pratiche
solidali messe sotto processo.

Nella routine dei primi due giorni cerco di capire come funzionano le
cose. Ok, prima cosa, comunicare con l'esterno. Devo comprare buste,
penne e bolli, come faccio? Posso fare la spesa, ma devo rimediare la
lista, riesco a ottenerla, ma come pago? Devo fare domanda per farmi
mettere i soldi con cui sono entrato sul conto. Bene, ma devo aspettare,
fare domanda per il modulo. Ottengo il modulo per la domanda, ma non è
corretto, ne ottengo un altro, aspetto ancora, chiamo per avere
informazioni, mi ignorano. Ho già capito che la cosa andrà per le
lunghe. Io sono qui, in relativa tranquillità, la cosa peggiore che sto
scontando è la pessima scelta del libro che mi sono portato da casa, ma
fuori ci sono compagne e compagni miei, che chissà che situazione del
cazzo si stanno vivendo, con sette dei loro in galera cui pensare e
altrettanti con misure restrittive. Inizio a pensare che ci metterò un
bel po' prima di avere contatto diretto con qualche persona fuori. Poi a
una certa, dopo un paio di giorni dal mio arrivo la guardia si presenta
e mi consegna della posta. Posta? Chi mi ha scritto? Apro e dalla busta
saltano fuori delle buste da lettera, dei bolli e una penna. Tutto
quello che mi ero penato di ottenere nei giorni prima, magicamente lì
tutto insieme e subito. In quel momento ho capito per la prima volta in
vita mia il concreto, vivo e reale significato della parola solidarietà.
Un gruppo di compagni da fuori, che non conosco nemmeno troppo bene, mi
aveva spedito quello di cui necessitavo, perché, essendoci già passati,
sapevano cosa mi serviva e perché aiutarsi in simili momenti è quello
che fanno gli anarchici, semplice. In quel momento ho realizzato che
essere anarchico non comportava nella mia vita solo quel senso di
insofferenza alle ingiustizie che mi portava a mettermi spesso nei guai,
ma era una risorsa che a volte te la può svoltare e a cui non tutti
hanno la fortuna di accedere. Nel giro di tre giorni dall'entrata riesco
a parlare con l'avvocato, avere aggiornamenti e materiale per scrivere e
spedire lettere, cibo, vestiti e libri che arrivano coi pacchi di amici
e solidali, insomma praticamente tutti i miei immediati problemi
materiali risolti, senza nemmeno sbattermi troppo fra domandine e
richieste alle guardie. Quanti altri detenuti hanno queste possibilità?
Pochi, davvero pochi. E le guardie lo sanno, perché quando iniziano a
portarti tutta quella roba in cella, se ne accorgono che non sei da
solo, che non sei proprio l'ultimo fra i dannati della terra, sulla cui
pelle è tutto permesso. E se ne accorgeranno ancor di più quando pochi
giorni dopo fuori dal penitenziario avrà luogo un presidio, in
solidarietà con due di noi reclusi qui dentro e con tutti i detenuti
della struttura.

Dalla nostra cella si sente poco e male, non siamo nemmeno sicuri che
fuori ci sia qualcuno. La certezza arriva quando dai piani alti partono
le battiture e qualcuno grida: «Avanti popolo detenuto!». E lì capiamo
anche noi che c'è del macello da fare e che l'insofferenza dilaga anche
in questa galera. Prendiamo la prima cosa che ci passa a tiro e iniziamo
a battere sulle inferriate. Dobbiamo alimentare quel casino, farci
sentire anche noi, partecipare al subbuglio generale. Merda, e pensare
che poche settimane prima dall'altra parte delle inferriate, in presidio
c'ero io. Adesso mi trovo dalla parte opposta e faccio quel che devo,
quello che spero sempre facciano le persone dentro, una sonora
battitura.

Pochi giorni dopo, l'inconsistenza dell'operazione repressiva e la
presenza di compagni e compagne in strada, ci porta ad essere fuori.
Sebbene il tutto avrà e ha tutt'ora strascichi tanto repressivi quanto
umani per cui conclusa non la si può dire, deve essere questo forse il
punto della storiella. La cui morale è... eh, boh, qualcosa che riguarda
la solidarietà direi. Già, proprio quella, quante volte ho urlato
«solidarietà»... cazzo è successo davvero.

{{%asterisme%}}

Una chiamata tra le due e le tre del mattino, un passaparola di quelli
con il cuore in gola --- non sei sicuro di aver capito --- qualcuno ti
sveglia nel cuore della notte e, a causa di qualche birra di troppo
prima di andare a dormire, faccio fatica a capire nell'immediato, tanto
mi sembra assurdo (ma ahimè non scontato, col passare del tempo) quello
che le mie orecchie stanno sentendo. Ancora mezzo frastornato dal sonno
e dall'alcol mi giunge voce che le guardie stanno perquisendo le case di
dodici persone, dodici compagne e compagni, dodici amici e amiche con
cui condivido l'odio e la rabbia nei confronti di questa società di
merda e della sua organizzazione subdola e violenta. Sette persone se le
porteranno in carcere. Col trascorrere dei minuti è sempre più chiaro
quello che sta accadendo: un'operazione antiterrorismo, poco dopo il
primo lockdown stabilito dal governo per la pandemia dovuta al covid.
Dopo una notte pesante passata in bianco a cercare di capire cosa sta
succedendo, sul fare del giorno l'assurda situazione comincia a essere
meno confusa dal trambusto notturno e si delinea in modo più nitido,
anche in seguito alle affermazioni pubbliche degli esecutori
dell'operazione repressiva, che si susseguono sui giornali locali e
nazionali, in radio e nei telegiornali.

Sebbene le ragioni siano sempre più profonde e spesso più nascoste nelle
intenzioni di chi reprime, dalle dichiarazioni ufficiali riportate nei
bollettini dei giornali emerge una questione fondamentale da ormai tempo
immemore: sotto inchiesta è la solidarietà, che in questo particolare
frangente storico coincide con la vicinanza mostrata ai detenuti e alle
detenute rinchiuse nelle patrie galere durante le rivolte del marzo
2020. Perché le persone arrestate e inquisite erano sotto le mura di
alcune delle decine di carceri italiane che prendevano letteralmente
fuoco grazie alla rabbia dei reclusi, che ancora una volta si alzavano
contro la gestione disumana e omicida dei penitenziari italiani. Anche
se non è la prima volta che operazioni repressive di questo tipo si
abbattono su coloro che si mostrano più refrattari a questo mondo e alle
sue regole, fa un certo effetto il fatto che abbia riguardato la vita di
persone a me molto vicine. Probabilmente si tratta di un'eterna prima
volta, per lo meno quando questo genere di cose ti accadono quasi
direttamente e riguardano il tuo intorno più prossimo, anche se sono
anni, secoli, che la repressione di Stato imperversa contro chi si
ribella.

Dopo l'iniziale tuffo al cuore la testa comincia a girare di nuovo per
il verso giusto, perché ci sono amiche e amici reclusi a cui far sentire
la propria vicinanza, che manco a farlo apposta sono finiti proprio nel
posto che odiano di più. Quasi automaticamente si sviluppa una rete di
persone vicine e lontane, spesso accomunate da destini e modi simili di
vivere la vita, che si occupa di tutte le questioni più impellenti,
pacchi da inviare dentro con cibo, vestiti, francobolli, fogli, tutto
quello che potrebbe servire, magari anche qualche libro (sperando che
non ci sia il tempo per leggerli, per lo meno non tutti). Prende piede
man mano in modo sempre crescente e non dal nulla proprio quel
particolare temperamento dello spirito, quella condizione di mutuo aiuto
e sostegno, che vive al di fuori dei percorsi sociali stabiliti nei
palazzi e che spesso a questi si oppone, un modo di agire e di sentire
che i potenti di tutto il mondo hanno sempre voluto annientare, perché
della solidarietà il potere ha sempre nutrito una paura profonda. Da
ogni dove giungono messaggi di vicinanza a chi per l'ennesima volta sta
subendo la mannaia della repressione, comunicati di rabbia contro chi
arresta e imprigiona, soldi per chi hanno messo in arresto e per le
spese legali, parole di incoraggiamento e chi più ne ha, più ne metta.

In tutto ciò c'è poi bisogno che le persone appena recluse capiscano
cosa sta succedendo fuori (e anche quei compagni e compagne dietro le
sbarre da un bel po') e in questi casi non è semplice mantenere un
livello fluido di comunicazione tra dentro e fuori: oltre ai bisogni
materiali c'è un contatto vitale e costante da intrattenere con chi
viene privato della libertà, e non è scontato che ciò accada.

I giorni passano e pesano come macigni, non è solo una questione
politica, ci sono in ballo le vite reali di chi viene costretto tra
quattro mura. Le famiglie, gli affetti di ogni tipo e sorta, il gruppo
di compagni e affini, bisogna «mantenere la barra dritta», come si sente
dire in giro da queste parti, e non è semplice. Per tutto il periodo
della detenzione --- circa venti giorni densi di sofferenza, ma pieni di
forza che dà luogo a iniziative e spunti di ogni sorta --- i luoghi più
frequentati sono gli uffici postali per inviare pacchi, lettere,
raccomandate, cartoline; si tenta in ogni modo di gestire questa
interruzione anomala della vita delle persone appena private della
libertà, la loro mancanza nei percorsi di lotta di tutti i giorni e vari
aspetti della loro vita personale che non possono essere da loro
direttamente curati durante il periodo di detenzione: mantenere un
contatto con i familiari --- e c'è bisogno di molto tatto! --- gestire le
relazioni con i datori di lavoro --- per chi ne ha uno --, parlare con
gli avvocati. E mi viene da pensare alla pesantezza di questi venti
giorni in rapporto alle condanne con decine di anni di detenzione che
compagni e compagne stanno scontando nelle galere!   Diventa tutto un
po' una scommessa e bisogna mettersi d'impegno, gestire bene le proprie
energie, mantenere un contatto con sé stessi. E poi bisogna farsi
sentire per le strade, fare casino, tanto casino. Arrivano compagni e
compagne da altre città, spostano la loro vita nel luogo in cui ce n'è
più bisogno per il lasso di tempo necessario a supportare coloro che
sono stati investiti da questa ondata di violenza istituzionale. Proprio
durante il periodo più restrittivo nella strana e recentissima storia di
questo paese, le strade ritornano a diventare casa per chi ha sempre
cercato di viverle controcorrente, per chi le ha sempre considerate come
i luoghi più adatti ad accogliere l'esplosione di rabbia contro questo
mondo. La strada, da qualche mese oramai quasi deserta, si ripopola di
gente incazzata, che nell'immediato si autorganizza° nei modi che ho
sempre immaginato, che poche volte ho toccato con mano e ora finalmente
di nuovo si possono realizzare. Si sussegue così un periodo fitto e
pieno di ogni sorta di iniziative: il posto frequentato dai compagni e
dalle compagne sotto inchiesta pullula di gente come non mai e piccoli
ma intensi cortei prendono vita per le vie della città; in diversi
momenti della giornata e a più riprese vengono improvvisati comizi nelle
piazze e nei luoghi maggiormente frequentati, cercando di dare
informazioni e stimolare riflessioni sull'accaduto anche nel quadro più
ampio del contesto storico attuale. E poi i rumorosi, folti e
pirotecnici presidi sotto le carceri dove sono recluse le persone
arrestate.

Ed è così che il ventesimo e ultimo giorno di detenzione coincide questa
volta con un grande corteo che attraversa gran parte della città,
centinaia sono le facce amiche, di gente conosciuta e da tempo lontana
da contesti del genere ma anche volti nuovi, di chi ha sentito il
bisogno impellente di esserci, perché non esserci in una situazione di
questo tipo può significare continuare a cedere spazio e linfa a chi
cerca di sedare ulteriormente e magari spegnere per sempre lo spirito di
chi anela alla rivolta come soluzione alle ingiustizie della vita
quotidiana. E anche se non si avrà mai la certezza che sia stato proprio
questo grande abbraccio solidale a inceppare momentaneamente la macchina
della repressione, mi piace pensare che l'essersi ritrovati in strada in
un crescendo durato settimane, con l'aiuto di tantissime persone,
culminato in un grande corteo solidale proprio lo stesso giorno in cui
le sette persone incarcerate venivano liberate --- sebbene ancora
indagate e in futuro sotto processo --- beh, mi piace pensare che tutto
questo, anche se magari in piccolissima parte, abbia contribuito ad
aprire le porte della prigione. La solidarietà è rivolta.

[^1]: Regionalismo che ha il senso di: presentire, intuire, fiutare,
    capire prima del tempo, ndr.

[^2]: La Divisione investigazioni generali e operazioni speciali è una
    divisione della polizia italiana creata nel 1978 con compiti di
    controllo di manifestazioni pubbliche e movimenti politici, a cui si
    sono poi aggiunte tifoserie e terrorismo.
