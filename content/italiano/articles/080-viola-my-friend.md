---
author: "Viola"
title: "«Ehi, my friend!»"
subtitle: "Frammenti di diario fra Bologna e il Mediterraneo"
info: "Testo scritto per la raccolta bolognese"
datepub: "Maggio 2022"
categories: ["migrazioni", "corpo, salute, cura", "solidarietà, mutualismo", "polizia, repressione, amministrazione giudiziaria"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "80"
quote: "A-C-C-O-G-L-I-E-N-Z-A"
---

*«Di dov'è che erano? Iran?»*

*«No, Iraq.»*

*«Ah beh, vicino»*

*Accoglienza.*

*A-C-C-O-G-L-I-E-N-Z-A.*

*Nero.*

*Occhi.*

*Cani. Bestie.*

*Regole.*

*Acqua.*

*Acqua che riempie gli occhi.*

*Acqua che entra nelle narici.*

*Acqua che schiaccia i polmoni.*

*Sale.*

*Smartphone.*

*Bimbi. Bimbi spaventati. Bimbi raggianti.*

*Denti. Bianchi.*

*File e file e file.*

*Docce.*

*Numeri.*

*Croste. Fori. Proiettili. Cicatrici.*

*Cicatrici.*

*Cicatrici.*

*Cicatrici.*

{{%ligneblanche%}}

Di cicatrici ne ho viste tante, in quell'estate del 2016 a P.

Su bambini, uomini, donne.

Ricordo una piccola lesione circolare crosticizzata in un ragazzo tra i
20 e i 30 anni. «Gun, gun». È l'unica cosa che riesco a capire, e di
balistica non ne so nulla.

Ricordo tante, troppe, lesioni da ustioni su gambe e glutei dovute alle
tante ore seduti nell'acqua salata che quasi sempre bagna il fondo delle
barche. A volte è acqua mista a gasolio, mi spiegano.

Ricordo infezioni, pustole, ascessi.

Ricordo un ragazzo che aveva diverse cicatrici sulla pancia che
riproducevano una sorta di disegno, una specie di sole attorno
all'ombelico e una serie di rombi a sinistra. Gli chiedo cosa siano e
credo di capire «I mag». «Quando stai male --- mi dice --- ti tagli».
Quando inizia ad uscire il sangue, inizi a stare meglio». «Ma che
religione è?» --- chiedo. «Non è religione, è cultura».

E poi tante --- un numero infinito --- donne stuprate. A volte ragazzine,
annientate, schiacciate, lacerate. Cerco rabbia e vendetta in quelli
sguardi, ma quasi sempre trovo passività, rassegnazione, incomprensione.
Vi hanno preso tutto, sorelle. E la vita vi condanna, e condanna il
bambino o la bambina che vi portate in grembo.

Aveva solo 16 anni, H. Era venuta in ambulatorio lamentando dolori
diffusi. Si siede, lo sguardo verso il basso. Qualcosa mi dice che vuole
confidarsi, e senza troppi giri di parole le chiedo se può essere
incinta. «Je ne sais pas» --- mi dice. «J'ai été violée. On a été toutes
violées»[^1]. In Libia? Prima di imbarcarsi? Sulla barca? Le propongo un
test rapido di gravidanza, l'unica cosa che posso fare in ambulatorio.
Positivo. Le prendo le mani e le spiego che in Italia l'aborto è legale,
e che forse siamo ancora in tempo. Non aveva alcun dubbio. Lei, quel
bambino, non lo voleva. «J'ai seize ans. Je veux étudier»[^2].
L'abbraccio ma abbracciandola mi dispero. Anche attivando tutti i
servizi che ero in grado di raggiungere farla abortire in S. sarebbe
stata un'impresa, anche perché arrivavano in tantissimi e tantissime, e
nel marasma della disorganizzazione generale perderli e perderle era
quasi la norma.

Quasi sempre lo si sapeva prima, quando una nave avrebbe attraccato. Noi
aspettavamo in banchina, a volte per ore sotto un sole soffocante prima
che le procedure di sbarco potessero iniziare. Sanitari, associazioni
umanitarie ben più grandi di quella a cui ero affiliata io, mediatori,
Protezione civile, Croce rossa, poliziotti, poliziotti, poliziotti.
Alcune persone venivano già visitate a bordo della nave prima di
scendere. Tutti gli altri erano smistati in due gruppi. I casi più gravi
venivano mandati da noi sanitari e in genere inviati da noi in ospedale.
Gli altri passavano un rapido screening e poi via, sul pullman che li
avrebbe mandati al centro «d'accoglienza». Una volta arrivati, file e
file per farsi identificare e indicare le ragioni per cui erano arrivati
in Italia. Quell'estate gli sbarchi furono innumerevoli, il più grosso
durante la mia permanenza a P. fu di 628 persone. Ricordo che quando
arrivavano si accalcavano davanti alla porta della polizia, in attesa di
compilare moduli, e i poliziotti uscivano spesso urlando che si
sedessero, che rispondessero in maniera adeguata, tutti saranno
ricevuti! Ricordo che una volta pensai ai cani di mia zia, in campagna a
Monteveglio, che latravano davanti alla porta finché lei non usciva
lanciando loro del pane secco e vecchie ossa.

Ci fu uno sbarco di 221 persone, 170 uomini, 35 donne e 16 minori. Ma un
cadavere come si conta? 170 uomini meno 1 morto, o 171 uomini? Ma poi è
uomo, donna, bambino? Spesso cantavano, arrivando al porto. Li potevi
sentire da lontano. Ma a quello sbarco no, nessun canto. Lunghi
preparativi per far scendere prima la salma. Ad attenderla, un feretro
grigio e un'auto delle pompe funebri che l'avrebbe portata all'obitorio
dove un medico legale avrebbe cercato di attestare la causa del decesso.
Il corpo era chiuso in un sacchetto di plastica bianca. Durante la
discesa verso il feretro, un odore acre di ferro marcio si era diffuso
nell'aria. Era talmente intenso che credetti mi si attaccasse alla
pelle. Ricordo che poi i ragazzi e le ragazze iniziarono a scendere. Un
ragazzo sudanese dopo pochi passi cadde a terra. Pianse.

Piangono in tanti durante quello sbarco. Molti svengono. L'uomo che
soccorro io è cosciente, mi sorride ma sembra assente. Mi dice che gli
gira la testa e fatica a stare in piedi. La scientifica incalza:
«Dobbiamo fargli la foto». Il ragazzo non può andare in ospedale senza
essere stato prima identificato. Lo fanno sedere ma lui tende a cadere.
«Ehi, my friend, su con la testa, dai. Solo un attimo». Gli mettono un
cartello davanti con su scritto un numero e gli fanno una foto di fronte
e una di profilo. Sorridi, benvenuto in Europa.

I bambini invece riuscivano comunque a rimanere bambini. Ricordo 5
ragazzini egiziani, tutti tra i 10 e i 14 anni, arrivati soli. «Noi
saremo sempre insieme» mi disse uno di loro mentre lo visitavo in
ambulatorio. Sorriso sdentato, pelle macchiata dal sole. «Siamo 5, come
le dita di una mano, vedi?». Uno di loro dimostrava sì e no 9 anni ma mi
disse di averne 12. Spiegò a me e alla mediatrice che fino all'anno
prima pesava 35 chili, ma quando lo pesai io ne trovai solo 28. Mi
chiese come si fa a tornare grande, come si fa a crescere.

Ricordo una bimba con lunghe treccine che si aggirava a piedi scalzi tra
i carabinieri. Avrà avuto tra i 4 e i 5 anni. In inglese mi chiese dove
fosse sua sorella. Mi chinai verso di lei che in risposta mi prese per
mano e iniziò a portarmi in giro. Un uomo di Frontex passò accanto a
noi, e indicando la bambina si rivolse a me in francese dicendo: «Le ho
dato dei biscotti e le ho insegnato a dire grazie. Almeno grazie
dovrebbero imparare a dirlo».

{{%ligneblanche%}}

Accoglienza. A-C-C-O-G-L-I-E-N-Z-A. Per tutto il periodo in cui sono
stata a P., più che in un centro d'accoglienza mi sembrava di essere in
un centro smistamento.
Sbarco-pullman-hotspot-identificazione-doccia-trattamento
antiscabbia-cibo-letto, quanti giorni? Poi pullman verso chissà quale
meta a sud, in centro o nel nord d'Italia. Il tutto in una profonda
disorganizzazione generale, con pochissimi mediatori, pochissimi accessi
linguistici che ci permettessero di farci capire e far capire,
pochissime informazioni, solo una grande macchina con innumerevoli e
laboriosi passaggi dagli ingranaggi oliati male, puzza di giochi di
potere e scambi di favori o soldi che prima o poi a qualcuno sarebbero
tornati. Ricordo B, un operatore sulla cinquantina. Lavorava nel centro
da diversi anni. Come mi aveva già raccontato un'altra operatrice, mi
disse che prima nel centro erano tutti volontari della Protezione
civile. Venivano a pulire, a sistemare e a fare cose che non avrebbe
fatto nessun altro ma che erano essenziali nella gestione del fenomeno
«emergenziale» degli sbarchi.

Successivamente, una qualche prefetta più lungimirante di altri, mi
disse B., ad un certo punto avrebbe detto: «Ma perché far fare questo
lavoro gratuitamente? Ormai è un lavoro vero e proprio» e da lì venne
istituzionalizzata la figura dell'operatore. Qualche operatore lavorava
nel centro a tempo pieno, ma B. no. Lui aveva un chiosco sul lungo mare
che dal porto va verso il centro di P. La sera facevano il karaoke. Però
B. restava anche un volontario della Protezione civile. Ricordo che ci
metteva anima e corpo, nella relazione con i migranti. Era anche nel
sindacato degli operatori, e nel centro era un punto di riferimento per
tutte le questioni pratiche. «Per me l'importante è che non mi tocchino
loro. Se me li toccano lì, sono guai», mi disse una mattina indicando un
ragazzo che attendeva di entrare in ambulatorio. Ma non tutti la
pensavano come B.

Numeri, numeri numeri. I migranti non avevano più nome e cognome, erano
solo numeri. Numeri scritti sui braccialetti bianchi che venivano dati
loro al momento dello sbarco. Tutti abbiamo una storia che ci portiamo
dietro. Loro no. Non importa la lingua, l'origine, la terra, non
importano le lacrime versate da tua madre al momento dell'addio, le
lacrime versate per i fratelli morti annegati, le speranze, gli amori,
il lavoro, le strade che puoi aver percorso. Per noi sei solo un numero,
un problema da spostare di centro in centro.

Scoprii ben presto che i medici come me erano addetti anche ai
certificati «di buona uscita». Uno dei primi giorni che ero a P., una
sera un funzionario dell'ufficio immigrazione entrò in ambulatorio. «Ho
bisogno di un certificato per tutti, devono partire domani». Espressione
sgomenta sul mio viso. Si trattava di 100 persone. Li avrebbero fatti
partire in due gruppi da 50, uno verso l'Emilia, l'altro non ricordo
dove. Tutti via, perché per il giorno successivo era previsto uno sbarco
di 450 migranti e bisognava fare posto. Reagii con veemenza. «Io sono
qui da due giorni, non li conosco. So che c'è una ragazza incinta,
praticamente a termine che non può partire. Poi c'è un altro ragazzo che
fa una terapia antibiotica endovenosa, anche lui dovrebbe restare».
Degli altri non avevo idea. Il funzionario pressava, disse che entro
mezz'ora doveva partire per andare a gestire due espulsioni a R. Io non
li conosco, insistevo, e non posso mettermi a visitare 100 persone alle
sei di sera. L'infermiere mi spiegò che bastava guardare nel quaderno
delle terapie. Chi fa una terapia e può continuarla in viaggio e nella
destinazione d'arrivo, può partire con i farmaci nello zaino. Chi ha
condizioni che controindicano la partenza, ovviamente resta. A fine
giornata, di quelle 100 persone solo la ragazza a fine gravidanza
sarebbe rimasta. L'infermiere passò mezz'ora a spiegarle che non poteva
partire. Lei continuava a dire che non voleva che suo figlio nascesse in
Italia. E l'infermiere si offese, perché la ragazza sembrava rifiutare
l'Italia quasi con repulsione «quando l'Italia è l'unico paese che li
accoglie tutti!».

{{%ligneblanche%}}

*Questi sono frammenti del diario che ho scritto durante la mia
permanenza nell'hotspot di P. come medico volontario di una piccola
associazione di pediatri nell'estate 2016.*

*Ero partita da Bologna nel mese di luglio. Ero entusiasta, piena di
energie, volevo aiutare, curare, rimarginare ferite. Ogni giorno
rientravo a casa e annotavo frasi, pensieri, dialoghi che mi erano
passati accanto o mi avevano vista protagonista con operatori del
centro, forze di polizia, colleghi e i volontari di altre associazioni
che, come la mia, erano lì nell'illusoria idea di poter aiutare.*

*La mia esperienza è durata circa un mese. Rientrata a Bologna, non ho
più aperto il diario. Ho continuato la mia vita cercando di formarmi e
di diventare una buona pediatra. Non ho più letto le parole che avevo
scritto credo per non rileggermi e farmi male ulteriormente. Dal primo
giorno mi sono sentita complice. Forse, non lo nego, come mi dicono
alcuni sono riuscita ad aiutare qualcuno o qualcuna. Ma per di più sono
stata complice. Complice di un sistema che rinchiude e non accoglie, che
respinge e non accoglie, che infantilizza, che giudica, che separa, che
uccide.*

*A distanza di sei anni credo sia cambiato molto poco. Decidiamo sempre
chi può entrare e chi no, in base al colore della pelle, alla
provenienza, all'esperienza lavorativa. Decidiamo quale forma di
assistenza umanitaria è permessa e quale non lo è. Decidiamo quanti,
come, da dove. Facciamo accordi con chi uccide e si nasconde, con chi
segrega, sequestra, stupra.*

*Siamo complici di una lenta e sanguinosa strage.*

*E ora*

*non possiamo più*

*restare*

*in silenzio.*

[^1]: «Non lo so. Mi hanno violentata. Ci hanno violentate tutte».

[^2]: «Ho 16 anni, voglio studiare».
