---
author: "Vivaietto"
title: "Storie che raccontano altre storie"
subtitle: "Diario di campo"
info: "Testo scritto per la raccolta bolognese"
datepub: "Ottobre 2022"
categories: ["lavoro, precariato, lotte sindacali", "giustizia ambientale, ecologia"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "87"
quote: "il soggetto rimane una realtà produttiva"
---

Abbiamo imparato che un apparato produttivo funziona, o dovrebbe
funzionare come un'equazione matematica; ce lo hanno detto ma in realtà
non lo abbiamo imparato, tutt'al più studiato. I coefficienti sono
scambiabili, mutabili, sacrificabili. Di norma non viventi. Quando hai a
che fare con il vivente le cose dovrebbero essere differenti. Un vivaio
non dovrebbe essere una fabbrica di piante, né tantomeno un parcheggio.
Allora cos'è un vivaio? Come ci si relaziona a un seme o a un fascio di
radici?

Quello che segue è il nostro modo di raccontarvi questo mondo e abbiamo
deciso di farlo da dentro. Vi gettiamo all'improvviso nel respiro di un
vivaio, nei suoi pensieri estemporanei, tra gli scarabocchi dei suoi
ricordi. Vi troverete tra le foglie in procinto di cadere e quelle
cadute, in cima a un capolino insieme a una farfalla o sotto terra con
un lombrico. Incontrerete l'universo di incontri e relazioni che
costruiscono il suo corso e capirete che il suo delta frastagliato ---
posizionamenti, scelte, contingenze e congiunture --- è comunque il
mercato. Il soggetto rimane una realtà produttiva. Doppia difficoltà.

## Trame di un vivaio-giardino

Trame estetiche e trame narrative. Il confine è labile, perché la forma
è la storia, il carattere e l'intenzione di ogni specie. Ogni
prospettiva ha un suo corpo. Che non si ferma mai e continua la sua
trama.

## Storie che si allontanano da altre storie

Siepi geometriche, perfettamente pettinate. Prati di un'unica specie,
perfettamente falciati. Alberi capitozzati, perfettamente contenuti.
Aiuole da pronto effetto, perfettamente imbalsamate. La devitalizzazione
del vivente congelato nel bon ton del disegno cartesiano. Quindi
integralmente scalabile, intercambiabile. Può essere ovunque. Stare con
chiunque. La merce rimarrebbe tale e quale. Anestetizzata da temperature
regolate, ambienti protetti, prodotti di sintesi.

Una rosa fiorita tutto l'anno. Nessun insetto nelle vicinanze. Petali
lucidati e fragranze medicinali. Senza spine. Geneticamente modificata.
Ibridata. Classificata. Nessuna variabilità. Un'evoluzione già
inscritta. Da altri. Lei, sola, imprigionata nel suo silenzio. Privata
della sua storia. Come anche i suoi giardinieri. Condannati dentro
pratiche atemporali. Non situate. Prevedibili. Senza perturbazioni. E
quindi sprovviste anche di rigenerazioni.

Dalla mania del conservatorismo astorico per una mercificazione
industrializzata alla sua apparente controparte: l'utopia di una
«natura» incontaminata. Le forze dell'ordine o gli ambientalisti. Due
posizioni e lo stesso risultato.

Non si intende neanche inseguire la poetica del selvatico eco-anarchico
a tutto spiano. Senza intervenire e lasciar fare solo alla flora e alla
fauna. Facendo finta di non esistere. Prendendo in giro ogni organismo
agente. Perché volente o nolente ci siamo anche noi. E il vivaio, il
giardino, il mondo, sono zone di interazione. Di negoziazioni. Storie
che si incontrano. E che non si allontanano. Spesso entrano in
conflitto. E così raccontano altre storie.

Il margine di indeterminatezza dato da più traiettorie di vita che si
intrecciano apre uno spazio di storia condivisa non progettata. O
almeno, non esclusivamente progettata. Il progetto c'è sempre, ma poi le
congiunture inaspettate trasformano i paesaggi comuni.

È questa la storia a cui ci rivolgiamo. Una storia, quindi, che non avrà
mai fine, che non sarà mai sola; narrata da più voci, umane e non;
reinterpretata da altre storie che costruiscono luoghi che poi si
trasformano e via via così... il gioco incessante delle vite non
musealizzate.

## Deterritorializzazioni socio-geografiche

Nella mappa del Vivaietto esistono degli spazi suddivisi. Presunzione di
forme identitarie, categorie definitorie. Disilluse al primo sussurro di
libertà vegetale. Insistente e indissolubile. Una garanzia di
r-esistenza alla standardizzazione dello spazio.

La serra, il luogo protettivo della germinazione. Il kindergarten, il
primo passo verso il mondo, uno spazio vicino alla serra, con un po' di
ombra protettiva al mattino e luce diretta al pomeriggio. Piccole
varietà botaniche avvolte dall'abbraccio materno delle grandi
*arizonica* confinanti ed irrigate tutte in compagnia da un sistema
dolcemente nebulizzato. La prateria, l'emancipazione socio-botanica in
pieno sole. Il sottobosco, quella in mezz'ombra. Annaffiate
individualmente da capillari serpeggianti.

Ogni zona possiede delle caratteristiche ambientali favorevoli agli
specifici processi di crescita. Ma il vento, gli spostamenti, gli
impollinatori creano sfasamenti. E incontri seduttivi. O conflittuali.
C'è chi transita da una zona all'altra e germina ospite in un vaso di
una specie che abita in spazi altri. Chi in serra soffre l'umidità e chi
in prateria soffre il pieno campo. Chi dal pieno sole emigra verso il
sottobosco perché trova uno spiraglio di luce dato dall'essiccamento di
un ramo. Chi si intrufola in una zona con più irrigazione perché ha
sete. Chi si lamenta del vicinato perché quella del vaso accanto si è
innalzata velocemente creando ombra nei dintorni.

Decostruzioni continue di regole biologiche generali e di pianificazioni
territoriali giardiniere. E così ogni organismo crea una storia.
Custodendo uno spazio. Abitando e quindi costruendo casa. Quella casa
peculiare. E da qui, una storia particolare. La sua storia. Che poi
racconterà altre storie.

## Osservazione di vite altre

Arrivare in vivaio pieni di piani. E rassegnarsi presto al principio di
indeterminazione. L'odg ce lo fa ciò che ci sfugge. Quante informazioni
che si raccattano in itinere, sul campo, con il campo! *Percezioni
locomotrici*. Congiunture contingenti: chi deve venir rinvasata, chi
deve ancora essere divisa e spostata, chi ospita intruse, chi ha bisogno
di un'integrazione di terra, di concime, di acqua, di trattamenti per
funghi, o batteri o insetti, chi soffre l'esposizione in cui è situata,
chi non è con le sue specie sorelle, chi ha già i semi pronti da
raccogliere, chi ha costruito architetture stilose ospitando intruse
selezionate. E da qui si avvia una concatenazione di pratiche. Nessun
programma aprioristico. Ma solo l'input dato dai processi di crescita.
Rielaborati contingentemente. Noi interpretiamo e rispondiamo. Con
successi e fallimenti. Ma tanto poi non è mai finita. Solo mondi
condivisi e non progettati. Le nostre tracce, i loro sentieri.
*Meshwork* di umani e non umani. Spore, radichette, semi, gemme,
tessuti, pigmenti, molecole odorose che raccontano storie facendo casa e
quindi creando parti di mondo. Che si intrecciano con le nostre
intenzioni e stravolgono la presunzione delle nostre identità.

## Incontri multispecie

«Ma guarda quella composizione che si è autocostruita nel vaso della
Miscanthus insieme a lei si sono insediate l'Agastache e il Lythrum. Un
accostamento cromatico e di tessiture difficile anche per il paesaggista
più erudito! Il viola intenso delle spighe del Lythrum che confluisce in
quello delicato dell'Agastache e che a sua volta si scaglia verso lo
sfondo denso e filiforme della graminacea. Lasciamole insieme,
piantiamole in un vaso più grande, quindi agevoliamo questa convivenza
progettuale connessa a forme di autogestione e di coordinazione
collettiva.»

Le relazioni ecologiche di un vivaio-giardino si sviluppano da intrecci
di luce e ombra stimolati da tempo e movimento, suggeriti a loro volta
da proiezioni multidirezionali. Tra umani e non umani. Progetti
multispecie spesso scaturiti da incontri non intenzionali. Trame
socio-biologiche che producono sintesi disgiuntive.

## Semina socio-botanica

«Quei semi hanno bisogno di periodi di vernalizzazione, mettiamoli al
fresco. Quelli si possono seminare direttamente in superficie,
lasciamoli al sole. Gli altri hanno bisogno di ombra, costruiamo
un'impalcatura ombreggiante. C'è il bambuceto in fondo, prendiamo i rami
secchi su cui mettiamo l'ombreggiante. Ci sono semi di tutti i tipi:
circolari, “lanceolati”; verdi, rossi, neri, marroni, bianchi; piatti,
spessi; pieni, vuoti.»

Il seme trattiene il tempo. Può mantenere la sua capacità germinativa
per anni, in condizioni improbabili. Può attraversare continenti. Via
acqua, terra, aria, fuoco. Poi una serie di intrecci socio-biologici può
favorire il suo attecchimento. E così, in caso decide di far partire la
radichetta, poi lo stelo verso la luce, le prime foglioline e scatenare
quindi l'intero schema complessivo che ha congelato al suo interno.
Ognuno ha le sue caratteristiche fisiologiche che diventano anche
socio-culturali dal momento in cui si trovano a dover rispondere al moto
delle cose. Se non vengono tartassati di tecniche strategiche di
monitoraggio artificiale, loro viaggiano con lo spazio, con il tempo,
con noi giardinieri. Ma a patto di esercitare le loro intenzioni. Spesso
imprevedibili, scomode per le logiche di mercato, sfasate rispetto ai
nostri piani, conflittuali rispetto al loro patrimonio genetico. È il
prezzo della libertà. Vegetale e giardiniera.

## Divisioni del sottosuolo

La madre è la pianta che si decide di non vendere per garantire la
moltiplicazione della specie. Da qui la necessità di farla ributtare a
più non posso, stimolare le radici al vagabondaggio, preparare loro le
condizioni che preferiscono. Interrogarle, ispezionarle, interpretarle e
attendere una risposta.

Radici fascicolate, bulbi, rizomi, tuberi, fittoni. Colori anche qui:
chi rosee, chi rosse, chi beige, chi marroni chiare, chi marroni scure,
chi bianche, chi bicolor, o tricolor o sfumate. Tentacolari, verso
l'alto, verso il basso, laterali, intorcolate, annodate, parallele,
incistate, fibrose, flagellate. Difficilmente individualiste. Fluidi
barattati, sostanze trasferite. Informazioni locomotrici.

La divisione dei cespi di erbacee è un viaggio anatomico verso i
bassifondi calpestati. Mai considerati. Ogni specie ha il suo modus di
mappare la terra. Le graminacee richiedono accette affilate e
ostinazione giardiniera. I Geranium delicatezza minuziosa di mani
esperte. Alcune esigono un terriccio drenato, altre più compatto. Da qui
si misura il grado di intensità di vermiculite, perlite, pomice, argilla
espansa. Per aiutare il tragitto peculiare di radichette consapevoli.

## Diserbo con

Prima di mettere nuovamente il cippato dobbiamo diserbare tutte le
graminacee e tutta la zona sole: è una pratica indispensabile per
monitorare dall'interno lo status quo delle varie zone. Intrufolandosi
tra tutti i vasi per diserbare si individuano: le intruse anche nei
vasi, i capillari dell'irrigazione non inseriti bene, o assenti, o
staccati, i vasi che ricevono troppa poca acqua, le piante che
fioriscono precocemente, le foglie che stanno ingiallendo, i parassiti
che attaccano una specifica, una specie troppo esposta alla luce, una
troppo all'ombra, una senza etichetta botanica, l'altra con il nome
della specie sbagliata. Si individuano tutti gli elementi singoli,
peculiari, dettagli che sfuggono allo sguardo complessivo.

Entrare da compagne e da compagni. Giardinieri tra le vagabonde. Negli
interstizi delle loro configurazioni. Verso il basso perché la terra è
bassa. Una danza in base alle architetture mobili delle erbacee.
Ispezioni privilegiate immerse nel campo. Dentro il campo, con il campo.
Faccia a faccia con loro. E da qui l'intersoggettività che prende forma.
Con i suoi vincoli e le sue possibilità. Equivoci interrogati.
Riposizionamenti stimolati. Indeterminati a priori e storicamente
contingenti a posteriori.

## Etichettamento storico

«Bisogna etichettare tutti i bulbi che stanno sfiorendo. Fra poco
spariranno e fino al prossimo anno non avremmo modo di rintracciarli!»

Che pratica indispensabile. L'etichettamento botanico. Il riconoscimento
nell'assenza. La maggior parte delle erbacee perenni si propagano
esponenzialmente tra la primavera e l'autunno rimodellando e deformando
lo spazio per poi sparire in inverno congelandosi in architetture
dorate. L'estetica del secco. Espansione e contrazione. Senza targhetta
di riconoscimento in primavera saremmo disorientati. Le foglie basali
spesso si assomigliano tra loro e spesso non assomigliano a quelle
cauline successive. La bellezza della sorpresa. Ma la disperazione dei
giardinieri. Arrestati nella pianificazione. Obbligati all'attesa
prolungata. Quante volte abbiamo sbagliato il riconoscimento delle
basali! Ritrovandoci a coltivare un Aster credendo fosse un Solidago,
oppure un'intrusa sconosciuta al posto del Lythrum. Giochi e scherzi
socio-botanici. Disattenzioni mai depenalizzate. Perché ogni azione
diventa una traccia bio-locomotrice che influisce sul reale. E a quel
punto non possiamo più scamparla.

## Attrezzi bio-culturali

Cesoie, vanghette, forche, seghetti, troncarami, mestoli, irrigatori,
etichette, corde, cordini, guanti, vasi, secchielli. Protesi incarnate.
Terriccio, vermiculite, argilla espansa, cornunghia. Pozioni
equilibriste. Una gigantesca biblioteca di cultura materiale.
Specificità pratiche rizomatiche. Interazioni tattili. L'habitus
costitutivo delle cose. Conoscenze incorporate. Attraverso abilità
tattiche in base alle quali i giardinieri intrecciano la loro storia
professionale e non agli input del vivaio. Relazioni di tipo tecnico
coinvolte nelle relazioni sociali. Calate nel corpo. Negli oggetti. Che
provocano cambiamenti. Ogni azione, una concatenazione di eventi.
Risposte continue. Tracce storiche che stravolgono l'hic et nunc. Spesso
prodotto di progetti non intenzionali. Che poi agiscono attraverso
incontri pratici trasformativi. Patterns intrinsechi che si plasmano
processualmente. Imbricati nel rimbalzo dialogico tra la tecnica
incorporata e l'improvvisazione fenomenologica.

## Storie che raccontano altre storie

Un po' come un libro, il vivaio diventa il luogo in cui si incontrano
tante vite differenti e ognuna diventa una storia dentro altre storie.
Ognuna agisce nello spazio, partecipando alla sua costruzione alla sua
trasformazione e innescando altri movimenti e pratiche da parte di altri
abitanti. È in questo modo che ogni storia racconta altre storie,
intessendo con esse una trama imbrigliata, mescolandosi e
imbastardendosi, tradendo qualunque prerogativa di unicità e di
uniformità. Lo spazio è un rizoma. Il processo è lo scopo e la relazione
il contesto di riferimento.
