---
author: "Militant* di Extinction Rebellion in Svizzera"
title: "Una rivoluzione rivoluzionaria ma non troppo"
subtitle: "Discussione autorganizzata tra militant* di Extinction Rebellion"
info: "Trascrizione di un dibattito orale"
datepub: "Giugno 2020"
categories: ["giustizia ambientale, ecologia", "autogestione, autorganizzazione", "violenza, non violenza"]
tags: ["autocritique", "bienveillance", "blague", "bouffe", "crier", "empathie", "erreur", "force", "France", "gouvernement, gouverner", "gueule", "injustice", "internet, web", "justice", "logique", "logo", "merde", "peur", "pizza", "pouvoir", "rage", "rue", "sabot, saboter, sabotage", "silence", "solution", "théorie", "violence", "vote"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "44"
quote: "eh no, a noi la violenza non piace mica, brutta la violenza"
---

## Capitolo 1 --- Una rivoluzione rivoluzionaria ma non troppo

--- OK, l'argomento del giorno riguarda il livello accettabile di
radicalità all'interno di Extinction Rebellion (XR) e il motivo per cui
gli altri movimenti militanti si stanno distanziando da noi.

--- È semplice. Al momento XR è un robo di borghesi bianch\* che se ne
fregano altamente di tutto quanto non riguarda il clima. Di base XR non
è per niente fatto per quello che fa attualmente. Il progetto dovrebbe
fare tutto il possibile per innescare ribellioni; ad esempio formando la
gente alla disobbedienza civile. XR dovrebbe essere un movimento aperto
alle altre lotte, che dovrebbero poter farci sapere quando stiamo
facendo cazzate: più numeros\* saremo, più facile sarà identificarci con
una lotta contro tutte le forme di oppressione.

--- Partiamo col botto! (*Risate.*)

--- XR si è sviluppato a partire da tre rivendicazioni... le
ricapitoliamo?

--- Uff... (*Risate.*)

--- C'è la dichiarazione dell'emergenza climatica, l'obiettivo di
eliminare completamente le emissioni di CO2 entro il 2025 e la creazione
di assemblee cittadine sovrane per risolvere la questione ambientale.
Chiaramente bisognerebbe aggiungere una quarta rivendicazione di tipo
sociale, perché al momento XR non ha niente di un movimento che vuole
cambiare la società.

--- Oggi, in Svizzera, il movimento è ripiegato su se stesso e ha
rinunciato a qualunque forma di autocritica. Rimane nella sua comfort
zone di disobbedienza ecologica non-violenta, senza accettare le
critiche degli altri gruppi militanti.

--- Il circolo vizioso sta nel fatto che il movimento attira solo
persone che si riconoscono nella dottrina, e questa massa di consenso
dell\* militanti XR soffoca chiunque cerchi di portare avanti
un'autocritica.

--- Tra di noi lo chiamiamo il "XR Social Club".

--- E cosa sarebbe?

--- Tipo una specie di *Lions Club*. Un punto di incontro sociale, che
in sé va benissimo, ma non è così che viene presentato. Lo presentano
come una roba radicale per il clima mentre spesso in realtà si sta lì a
cazzeggiare mangiando pizze...

--- Scoraggia le persone che vorrebbero essere più radicali, è una cosa
rilassante quando invece non è affatto il momento di rilassarsi.

--- È un vero problema per tutto l'ambiente militante in generale.
All'inizio lanci una cosa, no, hai un'energia di partenza, e poi diventa
tutta una questione di affinità. L\* compagn\* diventano amici e così i
momenti di lotta si trasformano in eventi super simpatici a cui non vedi
l'ora di andare. Ridendo e scherzando diventate un gruppo di amici,
lontano dalla logica militante e ripiegato su se stesso... già è
difficile entrare in un gruppo militante, ma se poi la cosa si trasforma
in un momento di grande socialità, con tutti che si conoscono benissimo,
è ancora peggio.

--- Anche il pensiero è molto ripiegato su se stesso, cioè non c'è
niente che può rimettere in discussione questo *social club* di XR. È
diventata una cosa completamente ermetica. Il movimento non è più in
grado di generare l'autoriflessione necessaria per andare avanti.

--- La cosa che davvero mi disturba in questa storia del *social club* è
che XR diventa la tua identità, qualcosa a cui sei legat\*
affettivamente e che difendi con tutte le tue forze, ma con ragionamenti
che a volte sono irrazionali.

--- Di fronte a una critica, a volte, non stai più difendendo XR e le
sue idee ma te stess\* e il tuo giro e questo cambia tutto.

--- Il problema sono anche i "Principi e Valori" che devi accettare
quando entri in XR...

--- Li ricapitoliamo?

(*Un momento di silenzio*.)

--- Di regola chiunque può aderire a XR a condizione di accettare dieci
principi e valori. Poi quei principi e valori li puoi interpretare un
po' come ti pare, ma c'è un'interpretazione dominante che diventa
dogmatica, intoccabile e socialmente condivisa. Idem per le strategie di
azione. Siccome in passato abbiamo bloccato dei ponti, ora non facciamo
altro che bloccare ponti perché la cosa da fare è quella. Il resto non
si conosce e quindi non si fa. Non si prendono rischi. All'inizio si
collabora con la polizia e un anno dopo quell\* che si radicalizzano e
non vogliono più collaborare con la polizia o che mettono in discussione
questo metodo vengono sconfessat\*.

--- È interessante questa cosa che dici, perché questo ripiego dei
movimenti militanti impedisce di esplorare strategie di azione diverse.
Sappiamo che le azioni dirette° più radicali hanno funzionato, come per
le suffragette° che davano fuoco alle banche per il diritto di voto alle
donne. Sappiamo anche che i grandi movimenti cosiddetti pacifisti, come
la decolonizzazione indiana o il movimento per i diritti civili negli
Stati Uniti, sono stati ampiamente accompagnati da azioni violente che
hanno stabilito un certo rapporto di forza. Ma la maggioranza di XR
rifiuta questo divello di radicalità. Non si fanno azioni con gli altri
movimenti, non c'è nessuna azione popolare di solidarietà.

--- Però questo c'entra col fatto che un sacco di gente mette un *like*
alla nostra pagina facebook o si appunta una spilla XR, ma ce n'è
pochissima che organizza veramente qualcosa.

--- Il problema non sta anche nel progetto di base? XR è questa roba
nuova, interessante ma anche problematica: una ribellione e un movimento
che ti vengono rifilati chiavi in mano, ti basta recuperare i principi e
i valori formulati da altre persone, in un contesto diverso. Quando
aderisci a XR ti spiegano ogni cosa: principi e valori, tattiche, come
funzionano le cose. Su internet trovi di tutto: idee per organizzare
formazioni, liste di azioni, consigli, discorsi pronti, comunicati
stampa. Hai già la pappa pronta. Non è affatto una struttura che si
presta a discussioni di fondo: è un progetto tattico. Si bloccano le
città, si cerca di mobilitare il più velocemente possibile una piccola
percentuale della popolazione per generare un cambiamento rapido. Se non
funziona, si passa ad altro.

--- Ma è proprio questo che mi piace un sacco di XR, questo lato "chiavi
in mano": ci sono nodi locali che si organizzano spontaneamente nel
mondo intero. Esistono gruppi XR in California, India, Svezia, Ghana,
Giappone, Marocco, ecc. Il problema è che i discorsi e le tattiche sono
stati sviluppati da XR in Inghilterra e non si prestano a un
copia-incolla in qualsiasi contesto. Le condizioni politiche e
ambientali non sono le stesse, così come la repressione poliziesca e il
quadro giuridico. E poi, come vedi, in alcuni paesi il logo XR è stato
ripreso spontaneamente da collettivi impegnati nelle lotte decoloniali o
anti-industriali nel sud-est asiatico, nelle lotte femministe in Africa
centrale, e altre realtà. È diventato un simbolo di resistenza globale,
e questo è una cosa fantastica.

(*Un momento di silenzio*.)

--- Beh certo, quando ho aderito a XR ho seguito le formazioni che il
movimento propone alle nuove reclute e tutto mi sembrava super ben
organizzato. Ho la stessa sensazione con i dibattiti su questioni di
fondo, spesso ti senti rispondere: "OK ma c'è una regola superiore e
quindi non la si può rimettere in discussione, è così e basta". Il
protocollo di azione è presentato come l'unica opzione legittima. E
soprattutto c'è questa idea del "consenso sulle azioni°": quando
partecipi a un'azione di blocco XR accetti implicitamente determinate
regole, tipo non bere alcool o non insultare la polizia. Se rifiuti
questo consenso, è un problema.

--- Uno dei problemi, ad esempio, è la posizione del movimento nei
confronti della polizia. Non si possono urlare degli slogan contro la
polizia, bisogna rispettare il suo lavoro. Per non parlare del rapporto
di forza fisica. Se cerchi di lanciare un vero dibattito sulla cosa,
quelli che hanno creato questo "consenso" dogmatico si volatilizzano. In
Svizzera c'era stata una giornata per parlare di questo, della polizia,
ma come al solito quando apri una discussione del genere, partecipano
solo le persone già convinte. Nessuno ne vuole parlare e non si va
avanti.

--- E un sacco di persone sono super attente all'immagine del movimento,
un po' per se stesse, perché non gli va di far parte di una cosa troppo
radicale, un po' perché tengono particolarmente a quell'immagine
mediatica di radical chic che non scassano troppo.

--- Beh, ovviamente non la formulano proprio così. (*Risate*.)

--- Le azioni di XR sono talmente basate sull'impatto mediatico che
rimani per forza incastrat\* in discorsi, metodi e azioni accettabili e
ricevibili dai media.

--- Praticamente siamo un fenomeno mediatico.

--- Già.

--- L'approvazione dei media ti pone immediatamente in una prospettiva
riformista, una prospettiva di lobbying in cui fai pressione sullo Stato
nella speranza che lo Stato trovi delle soluzioni perché in fondo ci
credi, credi che la soluzione dipenda dallo Stato. E tatticamente hanno
ragione, all'interno di questa logica. Se vuoi interagire con lo Stato
non puoi creare un grosso blocco rivoluzionario che spacca tutto e non
ascolta la polizia: lo Stato non ti ascolterebbe. Lo Stato è convinto di
essere la volontà del popolo, o per lo meno si nasconderà sempre dietro
a questo argomento. Insomma o stai al gioco truccato del dibattito
democratico, o la democrazia ti sbatte in galera.

--- La forza e la debolezza di XR risiedono proprio in questo, nel fatto
di voler piacere al maggior numero possibile di persone. Se vuoi
convincere le masse non puoi essere troppo radicale, ti tocca attenerti
a una semi-radicalità un po' equivoca. Quando ho aderito a XR non ero
per niente radicale, ho incontrato persone, imparato un sacco di cose e
questa è una forza incredibile. Ma secondo me nel migliore dei casi si
tratta di un movimento che può aprirti la strada verso altre forme di
militanza.

--- Ma in questa logica bisognerebbe che le persone più radicali
rimanessero in XR per continuare a fare da ponte verso altre forme di
azione e verso i movimenti più rivoluzionari.

--- Esatto. Però non è mica facile.

--- Io non ne sono tanto sicuro. Può darsi che le persone si
radicalizzino lungo il percorso ma non è garantito e penso che XR non
vada sopravvalutato. In fin dei conti, tutto riposa su pochissime
persone, più un enorme ventre molle di simpatizzanti che esitano tra
l'andare al mercatino bio o a un'azione.

--- E questa è una cosa che alimentiamo, in realtà: XR è andato a
vendere magliette al mercatino.

--- E a quel punto ti dici che allora non abbiamo proprio capito il
problema. Finiremo con centinaia di persone che vanno in giro con la
maglietta XR ma che non parteciperanno mai alle azioni, che non
parteciperanno mai al movimento, che non si radicalizzeranno ma si
limiteranno a ostentare questa specie di parola, "ribellione", senza
capirne minimamente il senso.

--- C'è del disprezzo in quello che dici.

--- Certo ma siamo talmente nella merda che a quest'ora dovremmo già
aver iniziato a sabotare lo Stato, parliamoci chiaro.

--- Sì.

--- Sì.

--- Sì.

--- Già.

(*Un momento di silenzio.*)

## Capitolo 2 --- Bruchi, panda e complessità dei sistemi di dominazione

--- Quindi il problema starebbe nel discorso portato avanti da XR.

--- Tipo le "formazioni" sull'emergenza climatica, il "*talk*" che ti
viene proposto per unirti al movimento.

--- Ecco, il *talk* è una presentazione gestita dall\* militanti, aperta
a tutt\*, durante la quale si presentano le cause del riscaldamento
climatico, le previsioni scientifiche come il rapporto del GIEC[^1]
(Gruppo intergovernativo sul cambiamento climatico), l'inazione dei
governi, i limiti del consumo individuale responsabile, ma anche la
strategia di XR e l'efficacia della disobbedienza civile non violenta.

--- A quelle conferenze dovrebbero semplicemente spiegare cosa sta
succedendo, e spiegarlo talmente bene da far venire voglia alla gente di
dare fuoco alle infrastrutture dell'oppressione. Perché nella realtà è
così che stanno le cose, la situazione è talmente orribile che nessuno
dovrebbe restare passiv\*. Anche chi non ha voglia di militare per il
clima dovrebbe farsela sotto dalla paura, è questa la realtà.

--- Da questo punto di vista il *talk* proposto da XR è un fiasco
totale.

--- Personalmente a me sembra una bella cosa, questo approccio del
*talk* e della formazione. Solo che i discorsi e i dati ambientali e
scientifici che vengono sempre messi in primo piano dovrebbero avere una
portata politica più ampia. Non si parla mai della complessità e della
sistematicità dell'apparato oppressivo, ad esempio. Ok, gli argomenti
scientifici sono indiscutibili, ma non bastano per un'analisi sociale e
politica della situazione.

--- E poi è tutto molto di parte. Va bene, c'è stato Gandhi, ma durante
la lotta per la decolonizzazione in India ci sono state violenze
pazzesche. Le suffragette° hanno incendiato delle banche per ottenere il
diritto di voto. Luther King sosteneva l'armamento di una maggioranza
dell\* militanti Black Panther. Mandela è stato letteralmente il
co-fondatore e dirigente di Umkhonto we Sizwe, l'ala militare del
Congresso Nazionale Africano,[^2] conducendo centinaia di azioni di
sabotaggio e difendendo la necessità della lotta armata.

--- A livello di violenza non è certo come scrivere ACAB° (*All Cops Are
Bastards*) sui muri. Se fai una cosa del genere durante un'azione XR, ti
becchi una ramanzina ed è già tanto se non ti rimandano a casa. Idem
quando gridi cose contro la polizia.

--- Questi *talk* dovrebbero presentare XR come un movimento sistemico°.
Perché la crisi climatica è il risultato dello sfruttamento del mondo
vivente e delle forme di dominazione nel loro insieme: capitalista,
patriarcale, razzista, specista. E poi tra lotte sociali e ambientali
c'è una convergenza naturale, ad esempio, lottando per le condizioni di
lavoro, i sindacati sono spesso tra i primi a lanciare l'allerta sui
pericoli ambientali nelle fabbriche.

--- La formazione dovrebbe parlare dello Stato e del clima. Basta
guardare tutte le leggi che lo Stato ha creato con tanto impegno. È
semplice: nessuna viene rispettata, nessuna. Il modo migliore di
spingere le persone alla disobbedienza è di mostrare che lo Stato stesso
non rispetta le leggi che ha creato.

--- In altre parole lo Stato non rispetta le leggi che il popolo si
illude di aver creato.

--- E poi mi colpisce il fatto che le lotte ambientaliste si appoggino
sempre sulle scienze dure (biologia, scienze ambientali ecc.) ma che non
si senta quasi mai parlare delle scienze sociali che descrivono e
spiegano, appunto, l'incastro di tutte queste forme di oppressione. Nei
nostri movimenti ambientalisti la teoria politica è scomparsa e la
sociologia è pochissimo presente. Anche i gruppi più radicali avranno
tendenza a comunicare a tappeto sullo scioglimento dei ghiacci e altri
fatti che sembrano convincenti. Il resto scompare, come se fossimo
convint\* che il discorso sociopolitico non serva più.

--- Io sono talmente convinta che non vinceremo mai che non me ne frega
niente, possono dirmi quello che gli pare, possono dirmi "vogliamo la
cacca rosa" e io scendo a manifestare, non è questo che... ma forse è
che sono coinvolta in queste lotte da così tanto tempo che non mi pongo
nemmeno più la domanda di cosa stiamo rivendicando, è stupido. Comunque
secondo me non vinceremo mai, ormai è così tardi che a questo punto
milito solo per salvare la mia dignità di essere umano.

(*Un momento di silenzio*.)

## Capitolo 3 --- Tutti odiano la... violenza

--- E poi certe cose devi sperimentarle per renderti conto che non
funzionano. Essere confrontat\* a un'ingiustizia o beccarti un
proiettile di difesa non è come leggere un articolo di giornale. Un
sacco di persone si radicalizzano dopo essere state arrestate. Insomma
certe violenze, finché non le hai vissute, ti sembrano fittizie,
irreali, mediatiche.

--- E qui sta il paradosso del rapporto tra XR e l'azione offensiva.

--- Intendi dire "violenta"?

--- Eh no, a noi la violenza non piace mica, brutta la violenza.

--- XR è un movimento che promuove la disobbedienza non violenta. È una
tattica che non crea problemi, è inclusiva, può funzionare quanto le
altre, dare visibilità alla violenza di Stato ecc. Il problema in realtà
e che abbiamo realizzato di non essere tutt\* d'accordo su cosa è
violento e cosa non lo è, anche all'interno del movimento.

--- Del resto non c'è quasi dibattito su questa cosa. La non-violenza è
diventata una specie di ossessione. Ogni cosa viene soppesata,
alleggerita al massimo, lisciata.

--- Durante le azioni ogni militante ha un compito preciso, tipo fare da
*peacekeeper*. Ancora una cosa importata senza riflettere dalla sezione
britannica di XR. Quest\* *peacekeeper* sono lì per assicurarsi che le
azioni non degenerino. Cercheranno di calmare le acque non appena le
cose si scaldano con la polizia o con l\* passanti che ci prendono a
male parole.

--- Finché XR continuerà a esercitare questo tipo di controllo sulle
azioni, sempre meno persone faranno l'esperienza, come dicevi, della
violenza di Stato, un'esperienza che sta alla base della tattica della
non-violenza.

--- È proprio un paradosso.

--- E poi non diciamoci cazzate: durante i blocchi di XR, io la violenza
di Stato l'ho sentita a malapena. L'ho sperimentata in altri contesti.
Io la conosco, la teoria della disobbedienza non violenta. So che è una
tattica, che la non-violenza è un modo di rendere visibile e
ufficializzare la violenza di Stato, solo che finché rimani super
obbediente e disciplinat\* l'unica cosa che vedi sono quelle vaghe
immagini di persone sedute portate vie dalla polizia.

--- In realtà è stato un errore pensare che lo Stato, almeno in
Svizzera, avrebbe espresso la propria violenza davanti a militanti
bianch\* che si definiscono non-violent\*. La repressione subita da XR è
di tipo diverso. La polizia può pedinarti, metterti sotto pressione,
umiliarti in commissariato, e il sistema giudiziario è durissimo con
noi. Ma questa violenza non si esprimerà mai allo scoperto. È stato un
errore megagalattico credere che lo Stato e la polizia ci avrebbero
trattat\* in maniera spettacolare.

--- Guarda cosa è successo in Francia: una volta la polizia ha gasato un
*sit-in*° pacifico di XR ed è scoppiato un tale casino mediatico che
adesso il governo ha sviluppato strategie diverse, più insidiose. Lascia
che il movimento muoia nella sua disobbedienza inattiva.

--- E poi quando la polizia interviene durante un'azione XR c'è chi dice
"ma dai, i poliziotti sono buoni, fanno semplicemente il loro lavoro".
L'intervento potrebbe già sembrare violento ma invece no, figuriamoci,
ti dicono tutt\* che in realtà non è violento per niente, è "normale"
che la polizia ci cacci via. (*Risate amare.)*

--- Anche nell'organizzazione dei blocchi XR cerca di pianificare ogni
cosa, polizia compresa. Addirittura nominano preventivamente delle
persone che saranno responsabili di dialogare con la polizia, come
dicevamo prima. La polizia arriva e non viene minimamente perturbata, sa
con chi parlare, è tutto pronto e diventa quasi una routine, sempre
uguale.

--- La polizia non ha nemmeno più bisogno di microfoni.

--- Giusto, praticamente si trova con la pappa pronta.

--- Io non sono d'accordo con te quando dici che la polizia non sarà mai
violenta contro l\* militanti bianch\* radical chic. Magari è vero
quando si blocca un ponte o cose così, ma nella storie dell'attivismo
ambientalista non è vero. La polizia è violenta quando la becchi
impreparata. Ma durante un blocco XR invece la polizia sa che filerà
tutto liscio e quindi è tranquilla e può reprimere davanti alle
telecamere.

--- Il giorno in cui inizieremo a circondare la polizia e a incatenarci
ai loro furgoni, quando inizieremo a imporre delle cose che non
capiscono, senza violenza fisica, sarà tutta un'altra storia. In realtà
la polizia svizzera non è molto preparata e organizzata per gestire
azioni di massa.

--- Eccoci tornat\* al problema dell'ossessione mediatica all'interno
del movimento. Abbiamo questa fissa di reclutare il massimo di adesioni
e di mostrare che XR è una figata, e la cosa si ferma lì. Mangiamo pizze
e sulla pizza siamo tutt\* d'accordo. Ma appena ti metti a spaccare
vetrine non ti sostiene più nessuno.

--- E così il movimento diventa naturalmente sempre più pacifico,
presentabile e mediatico. La polizia ci rende telegenich\*.

--- Secondo me vi sbagliate su una cosa, cioè sul fatto che la
maggioranza di chi milita in XR non pensa che la disobbedienza non
violenta serva a esporre la violenza di stato.

--- Ma la strategia di XR è proprio questa. Ad ogni modo è la prima cosa
che mu hanno detto durante il *talk* di formazione.

--- Beh, per quanto mi riguarda, se non vi avessi incontrat\* credo che
non avrei mai capito questa strategia.

--- Possono coesistere strategie diversissime all'interno di un
movimento, specialmente un movimento grande come questo, dipende anche
dai gruppi di affinità che incontri.

--- È anche una questione visiva. Tipo noi ci sediamo subito, ma anche
solo rimanere in piedi cambierebbe le cose. Bisogna disobbedire, magari
praticando la non violenza ma comunque con forza e potenza.

--- Nella maggior parte dei casi le azioni XR sono pensate come vetrine
per attirare nuove reclute. In Svizzera è sicuramente un errore
strategico pensare che cresceremo fino a raggiungere una massa
abbastanza grande da cambiare le cose. Non abbiamo il bagaglio delle
lotte sociali francesi o tedesche. Dobbiamo cercare approcci diversi.

--- Non so se lo sapete ma a Losanna il movimento si è spaccato perché
un'unica persona aveva fatto una scritta *ACAB* durante una
manifestazione. Basta un *ACAB* e tutto il movimento vacilla. Alla
faccia delle basi solide...

(*Un momento di silenzio*.)

## Capitolo 4 --- Quando il neo-sciamanesimo bianco e radical chic compra la pace sociale

--- Ok, stiamo arrivando alla fine del dibattito e dopo abbiamo un'altra
riunione.

--- Quando ho iniziato a militare avrei voluto essere avvertita che
avrei passato in riunione il poco tempo libero che mi concede il
capitalismo. (*Risate.*)

--- Di cosa vogliamo parlare per concludere?

--- Di perché XR è una figata?

--- Potremmo, ma è già chiaro: per il suo impatto, il rapporto
all'azione diretta° di disobbedienza, il potere di aggregazione e
soprattutto per il fatto di offrire un inquadramento a chi si affaccia
al mondo della militanza e non sa come partecipare al di fuori dei
vicoli ciechi dei partiti e delle ONG.

--- Potremmo parlare di perché dovremmo smettere di comportarci come una
start-up che fa *branding*[^3] spiattellando ovunque bandiere e loghi?

--- E se parlassimo del dopo, della resilienza, di un modo di vivere
diverso?

--- È quello che XR chiama "cultura rigenerativa".

--- Uff...

--- L'idea di base è fantastica: XR si organizza e crea un rapporto di
forza per arrivare a costituire delle assemblee cittadine° sovrane e
emancipate dallo Stato. E intanto il movimento cerca di creare una
cultura diversa, una cultura della decrescita, non alienata, empatica,
sociale e accogliente. Dopo la rivoluzione, XR si dissolverà e
rimarranno solo le assemblee cittadine e la cultura rigenerativa.

--- Sulla carta suona bellissimo.

--- Il problema è la pratica.

--- Già.

--- Oggi quello che il movimento promuove come cultura rigenerativa è
una barzelletta.

---XR tende a confondere buona volontà e sentimentalismo. La buona
volontà significa anche dire la sua quando bisogna farlo, riconoscere i
propri privilegi e le capacità di ognun\*, costruire un'offensiva senza
degenerare nella militanza violenta nei confronti delle altre persone.
Vuol dire riconoscere e cercare di lottare contro il sessismo e il
razzismo che dilagano inevitabilmente in qualunque organizzazione
collettiva che esclude dalla lotta le persone interessate.

--- Oggi all'interno di XR c'è addirittura chi parla di buona volontà
nei confronti dei padroni e della polizia.

--- Vabbè, ma è una minoranza.

--- Ho capito ma è comunque sintomatico del problema. Quando mi incazzo
contro questo sistema che distrugge il mondo vivente e voglio
attaccarlo, mi sento dire: "Dovresti farti aiutare per superare questa
rabbia che hai dentro". (*Risate*.)

--- Una volta hai detto che la cultura rigenerativa di XR al momento è
una roba per radical chic e fricchettoni neo-buddisti, lo yoga
energetico prima delle riunioni... per un po' va bene ma alla lunga...

--- La cultura rigenerativa di oggi significa prendersi delle sbronze,
meditare e passeggiare nei boschi. Non c'è empatia. Solo empatia verso
se stessi.

--- È proprio questo il problema di tutto questo mondo fissato con lo
sciamanesimo e il benessere: tutto è centrato su se stess\* e non sulle
altre persone. Io combatto e lotto per la collettività, mica per me,
tanto più che sono bianca e ho un mestiere socialmente valorizzato. Non
ho bisogno che ci si prenda cura di me. Ho bisogno di distruggere le
strutture che ci impediscono di prenderci collettivamente cura l\* un\*
dell\* altr\*.

--- È proprio uno schifo perché in teoria la cultura rigenerativa
dovrebbe darti l'energia per fare delle azioni, e quindi dovrebbe essere
una cosa che ci permette di essere arrabbiat\* insieme. Dopo aver
parlato con voi mi sento più rassicurata, rigenerata e motivata che dopo
qualunque evento rigenerativo XR. Mi sembra di aver dato un giro di
ruota.

--- La ruota non gira se non girano le balle.

--- La vogliamo chiudere così, con questo gioco di parole del menga?

--- Ci sono proposte migliori?

(*Silenzio.*)


[^1]: Gruppo di esperti creato nel 1988 dall'ONU. I suoi rapporti
    dettagliati sullo stato attuale delle conoscenze scientifiche,
    tecniche e socioeconomiche sui cambiamenti climatici, le loro cause,
    le potenziali ripercussioni e le strategie di prevenzione, sono una
    referenza scientifica internazionale.

[^2]: Il Congresso Nazionale Africano (in inglese *African National
    Congress*) è un partito politico sud-africano fondato nel 1912 per
    difendere gli interessi della maggioranza nera contro la minoranza
    bianca. Dichiarato fuorilegge dal Partito nazionale nel 1960,
    durante l'apartheid, è stato legittimato di nuovo nel febbraio 1990.
    Gli Stati Uniti lo hanno classificato come organizzazione terrorista
    tra il 1986 e il 2008. Nel 1994, grazie alle prime elezioni
    legislative multirazziali e a suffragio universale permettono
    all'ANC di prendere il potere e al suo presidente, Nelson Mandela,
    di diventare presidente della Repubblica Sudafricana. Da allora,
    l'ANC domina la vita politica del paese.

[^3]: Strategia marketing per la gestione dell'immagine di marchi e
    aziende.
