---
author: "Jupiter"
title: "Spezziamo l'isolamento"
subtitle: "Racconto di un'organizzazione contro le carceri"
info: "Testo scritto per la raccolta svizzera romanda"
datepub: "Febbraio 2021"
categories: ["carcere", "polizia, repressione, amministrazione giudiziaria", "antirazzismi"]
tags: ["isolement", "pouvoir", "silence", "rue", "voiture, bagnole", "torture", "permanence", "caméra", "force", "torture", "isolement"]
langtxt: ["fr", "it", "en"]
zone: "Suisse romande"
num: "39"
quote: "l'isolamento è una delle principali armi della repressione"
---

*Jupiter è un collettivo di sostegno all\* detenut\* vittime della
polizia, costituito per la maggioranza da persone nere.*

Sono giorni, mesi, anni, che vediamo amic\* scomparire per qualche
giorno, qualche mese, qualche anno. Ogni giorno, delle persone vengono
arrestate e incarcerate. Molte di loro non le vediamo nemmeno. Senza
documenti validi in Svizzera --- anche se provviste di documenti
rilasciati da altri paesi europei per poter viaggiare --- queste persone
sono considerate in situazione di illegalità. Si tratta di un sistema
diretto per incarcerare una certa categoria di persone, rendendola
invisibile al punto di farla scomparire in fondo a delle celle.

L'isolamento è una delle principali armi della repressione.

Dal posto di polizia al carcere, ogni cosa è organizzata un modo che la
persona arrestata non abbia risorse. A Losanna, l'operazione inizia
sempre con la detenzione nel quartiere penitenziario della prefettura o
al posto di polizia di Blécherette --- zone di non-diritto in cui le
condizioni di detenzione si avvicinano alla tortura. Le persone detenute
vengono regolarmente picchiate nelle loro celle, umiliate, minacciate di
morte e costrette ad assumere farmaci. Le celle sono illuminate 24 ore
su 24, le telecamere filmano costantemente, senza luce naturale, senza
diritti di visita, senza possibilità di tradurre le lettere ufficiali
ricevute. Nessuna informazione se non il fatto che non c'è posto in
carcere e ti tocca aspettare... La durata legale massima di una
detenzione in un posto di polizia sarebbe di 48 ore, ma spesso si
estende tra i 15 e i 30 giorni. Poi la detenzione prosegue in carcere.
Nessuno sa quando potrà uscirne. E i mesi passano. Nei giorni che
precedono il giorno previsto per il rilascio, i guardiani arrivano con
nuove lettere che annunciano giorni di detenzione supplementari, decisi
arbitrariamente dalla Procura, e la persona rimane in carcere. Senza
possibilità di comunicare con l'esterno, le persone arrestate e portate
via scompaiono per giorni, mesi, anni.

Tutti i giorni, la polizia perseguita le persone di colore per strada.
Tutti i giorni, la polizia picchia e tortura queste persone al riparo
dagli sguardi del pubblico, in macchina, tra i cespugli, nelle stradine,
per il colore della loro pelle. La polizia sale sui trasporti pubblici e
fa scendere solo le persone nere per i controlli. Entra nei ristoranti
frequentati da clienti dell'Africa occidentale e pescano a caso delle
persone per portarle fuori e perquisirle. Le pattuglie irrompono in
massa in città e per strada, interpellano l\* ner\*, l\* mettono in fila
contro un muro circondandol\*, spesso ammanettat\*. Chiedono i permessi°
di soggiorno e l\* perquisiscono. Nel corso di questi innumerevoli
controlli portano via i documenti di soggiorno, li distruggono e rubano
il denaro che hanno addosso. Di fronte a questa situazione, a questo
silenzio, all'invisibilità voluta dallo Stato, vogliamo parlare e far
vedere come stanno le cose. Abbiamo formato questo gruppo per
documentare ciò che vediamo, abbiamo visto e, in alcuni casi, vissuto
sulla nostra pelle. Abbiamo deciso di organizzare degli eventi, prima di
tutto per raccogliere fondi in sostegno a chi ne ha bisogno,
specialmente tra l\* detenut\*. Ad esempio, il semplice fatto di poter
comprare una scheda telefonica permette di rimanere in contatto con il
mondo esterno. Diffondiamo informazione e, in conclusione, ci prendiamo
del tempo per riunirci, incontrarci e pensare a chi non è con noi.

L'isolamento è la loro arma, spezziamo l'isolamento.
