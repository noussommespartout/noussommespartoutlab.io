---
author: "Marco Palma, Bologna for Climate Justice"
title: "Convergere per insorgere"
subtitle: "L'esperienza di Bologna"
info: "Testo scritto per la raccolta bolognese"
datepub: "Gennaio 2023"
categories: ["autogestione, autorganizzazione", "giustizia ambientale, ecologia", "manifestazione, rivolta, protesta"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "94"
quote: "ma sfilano allegramente"
---

Il 22 ottobre 2022 trentamila persone hanno manifestato per le strade di
Bologna, invadendo la tangenziale e bloccando per alcune ore ogni
movimento sul passante autostradale che attraversa la periferia del
capoluogo emiliano-romagnolo. Un corteo "per questo, per altro, per
tutto", aperto dallo striscione "Fine del mese, fine del mondo: stessa
lotta!", al quale hanno contribuito decine di associazioni, comitati,
centri sociali, gruppi formali e informali, e che è nato sotto il segno
della convergenza, facendo tesoro e pratica dell'esperienza maturata in
tanti mesi di mobilitazione dal Collettivo di fabbrica GKN di Campi
Bisenzio (Firenze).

Una manifestazione che, nel contestare il progetto di allargamento di
una grande infrastruttura --- il Passante di Mezzo, ovvero il sistema di
autostrada e tangenziali che, da ovest a est, attraversa la città, e che
Società Autostrade vuole allargare fino a 18 corsie --- ha saputo trovare
non solo nessi, ma vere intersezioni tra lotte e mobilitazioni che
parlano di lavoro, crisi climatica, diritti sociali e civili, cibo e
sovranità alimentare, territorio, casa, spazi, socialità. Una giornata
che in tante e tanti hanno vissuto come la possibile apertura di una
fase nuova, e che ha le sue radici in un percorso allo stesso tempo
inclusivo e contraddittorio, che ha mosso i primi passi nella torrida
estate del 2022.

Inclusivo, perché lo stesso corteo ha rappresentato il coro di voci che
in quelle settimane hanno preso parola, con linguaggi e vocabolari
diversi. Contraddittorio, perché la convergenza è stata --- ed è --- un
processo nuovo e inesplorato, non codificato e regolamentato, sempre in
bilico tra ritualità e innovazione. Ne è risultato un percorso
accidentato e tortuoso, per certi versi impianificabile, ma che forse
proprio nella sua precarietà intrinseca ha regalato la suggestione di un
altrove possibile. Una vicenda, quella che ha portato al corteo
bolognese, che possiamo provare a narrare in tre fasi, ognuna non
lineare: la scommessa, il carsismo, la convergenza.

## La scommessa

Tra Firenze e Bologna, i telefoni hanno iniziato a squillare
ripetutamente nei primi giorni di luglio. Da una parte il Collettivo di
fabbrica GKN, con alle spalle un anno di mobilitazione che ha visto
Firenze riempirsi di manifestanti più di una volta, l'ultima il 26
marzo. Una vertenza di fabbrica che ha saputo rendere protagonista il
proprio territorio, e che nel farlo ha stretto legami sempre più fitti
con una pluralità di realtà che si battono per la giustizia sociale e
climatica.[^1] Dall'altro lato della cornetta, attiviste/i bolognesi che
in questi mesi hanno cercato di rendere visibile l'invisibile, ovvero
l'opposizione a un progetto di allargamento autostradale definito dai
promotori "l'opera simbolo della transizione energetica nazionale". Una
posizione, quella contraria, che nei mesi passati non aveva determinato
un'opposizione diffusa e popolare, e che pure, per i tanti significati
che porta con sé l'allargamento del Passante di Mezzo,[^2] rappresentava
e rappresenta una delle contraddizioni più evidenti sulle quali mettere
in discussione il sistema economico e sociale che produce sfruttamento,
precarietà, crisi ecologica e negazione dei diritti.

La scommessa nasce dalla convergenza di queste esperienze e dei loro
bisogni: da una parte la necessità di non fare di Firenze un unicum,
ovvero il solo territorio in cui la pratica della convergenza si
sperimenta e crea eccedenze; dall'altra, la voglia di far sentire forte
la voce contraria all'ennesima opera infrastrutturale che perpetua
l'economia fossile. Una scommessa, quella nata nell'estate 2022, capace
di scrollare Bologna da un torpore frutto anche del tentativo, praticato
dal governo locale sotto l'ombrello dell'immaginazione civica, di
istituzionalizzare, regolamentare e incanalare i conflitti lungo
percorsi di facilitazione, indicando tra l'altro nell'allargamento di
un'autostrada il modello per la transizione ecologica e caratterizzando
così la sfida del cambiamento climatico come salvaguardia e rinnovamento
del sistema economico e sociale che l'ha determinata.

La sfida collettiva non è stata soltanto convocare una manifestazione in
un momento di grande incertezza, a cavallo tra la caduta del governo
Draghi e la nuova tornata elettorale, ma anche di farlo a partire da
donne, uomini e realtà che, fino a quei giorni, si conoscevano poco, e
che hanno individuato nell'insorgere la motivazione per convergere. Ne è
nato un appello, diffuso dopo Ferragosto, che ha aperto la strada verso
il 22 ottobre. Non è un segreto --- perché è stato ribadito nelle tante
assemblee --- il ruolo che in questo percorso ha avuto il Collettivo di
fabbrica GKN che, grazie all'autorevolezza conquistata in fabbrica e
nelle strade, è stato il perno simbolico (e non solo) intorno al quale
si è plasmato il processo successivo che, per altro, ha ben presto
evidenziato i punti di forza di un appello alla convergenza, ma anche le
sue inevitabili insufficienze.

## Il carsismo

Con settembre, il processo di convergenza percorre due binari che si
incrociano spesso: il primo, di scala territoriale ampia, che vede tante
realtà sociali italiane accettare l'invito alla convergenza, e che si
fonda sulla pratica dell'*Insorgiamo Tour, *già sperimentato prima della
manifestazione del 26 marzo a Firenze. Il secondo, specificatamente
bolognese, lungo il quale matura il protagonismo cittadino. Se il primo
ha alle spalle l'autorevolezza del Collettivo di fabbrica GKN, il
secondo prende invece le mosse su crinali instabili come lo sono i
calanchi della collina bolognese. Sotto le due torri, infatti, le
differenze e le divergenze hanno scavato solchi importanti, e --- con
l'esclusione del movimento trans-femminista° --- non vi sono soggettività
sociali capaci di essere perni aggregatori. In settembre, la convergenza
è un processo carsico, e l'acqua che la alimenta prende più volte
direzioni inaspettate. Come alla prima riunione, convocata come un
momento per coinvolgere le realtà organizzate locali, e che invece si
trasforma in un'assemblea cittadina di circa 200 persone, durante la
quale emergono due spinte che caratterizzeranno le settimane successive:
la volontà comune di costruire un momento di mobilitazione capace di
segnare una svolta, dando ossigeno e creando nuovi spazi d'azione; e la
discussione sul metodo, che si pone sui confini tra organizzazione o
relazione, somma o moltiplicazione, ritualità o innovazione. È in questa
fase che inizia a sprigionarsi l'energia del processo di convergenza,
che proprio nel suo essere un terreno ignoto permette esplorazioni che
si concretizzano nella ricerca di forme, parole, modi nuovi, e che pone
al centro la multi-dimensionalità delle rivendicazioni, che non possono
limitarsi al terreno sul quale ognuna/o ha fondato la propria *comfort
zone*, ma che hanno bisogno di ripercorrere i nodi della tela, per
identificare ciò che accomuna tante lotte, ovvero la sfida di mettere in
discussione il sistema. Non a caso, sono proprio le contraddizioni del
processo di convergenza che si sperimenta a Bologna a far emergere
questa dimensione, per esempio con le critiche mosse da parte del
movimento trans-femminista°, che, peraltro legittimamente, colloca le
proprie parole chiave non tra i tanti 'per questo'[^3] --- ovvero tra le
molte lotte che si intrecciano nella mobilitazione --- del percorso verso
le 22 ottobre, ma rivendica per la lotta al patriarcato l'essere
premessa e non una delle parti di un processo di convergenza. Se alcune
posizioni identitarie, espresse anche durante la grande assemblea
regionale del 5 ottobre, richiamano alla memoria l'auto-rappresentazione
pubblica, critiche come quelle appena descritte aprono invece uno spazio
di riflessione, sedimentando la consapevolezza che non è possibile
limitare la convergenza al mettere insieme spezzoni diversi per
costruire un corteo più lungo, ma che la vera sfida è contaminare
identità diverse e spesso plurali, cercando di rompere lo schematismo
dell'accordo negoziale per produrre un "in più" capace di innescare
protagonismo collettivo. E così, non sono le mega-assemblee, più
retaggio di un rituale politico che strumento reale di costruzione di
condivisione, ma le relazioni che si dipanano quotidianamente tra
iniziative, incontri, e confronti, a creare le condizioni per
convergere, rompendo il meccanismo del coordinamento tra realtà più o
meno organizzate, che pure alcuni rivendicavano con forza, e aprendo
squarci di complicità inusuali.

## La convergenza

Non tutto è oro quel che luccica, naturalmente. E se nelle sale
pubbliche, nelle strade e nelle piazze di Bologna si misura una
crescente aspettativa verso la manifestazione, il percorso di
convergenza vive nelle settimane di ottobre di continui sussulti. Ne è
testimonianza l'ultima assemblea, convocata una settimana prima del
corteo, durante la quale diverse faglie di tensione si intrecciano,
evidenziando che la convergenza non è un metodo con una ricetta
prestabilita, ma un processo che si dà, si esprime e si plasma sulle
caratteristiche del momento. E, quindi, anche sui conflitti che quel
momento produce. Nelle ultime settimane, il processo perde la sua
dimensione carsica, per diventare pienamente pubblico. E proprio
l'ultima assemblea ne è testimonianza, con lo straniamento prodotto in
molte/i da un dibattito che vede emergere contraddizioni multiple,
legate al metodo del processo e ai diversi punti di vista che su di esso
si esprimono, ma anche alle forme di possibile rappresentanza della
pluralità promotrice della manifestazione nei confronti della Questura.
Anche in questo caso, è dalla stessa dimensione conflittuale che
emergono le soluzioni che poi si riveleranno efficaci, come avviene nel
caso del dibattito su chi e come dovesse dare a nome di tutte/i formale
comunicazione della manifestazione alle istituzioni, che si risolve
attraverso la scelta di una comunicazione pubblica, plurale e "di
piazza", non conforme alla prassi, ma efficace nel garantire
l'irrappresentabilità della convergenza che si sta muovendo. E così,
invece del modulo prestampato da protocollare negli uffici, decine di
persone consegnano in forma pubblica nella piazza antistante la Questura
bolognese una stampa del percorso del corteo --- già comunicato anche
online --- rivendicando la pluralità di un percorso politico che non può
avere delegate/i.

Ottobre è pienamente la ribalta di questo processo, grazie a una
pluralità di iniziative organizzate da soggetti diversi, ma capaci, in
modi differenziati, di cercare complessità e aprire nuovi confronti. Tra
questi, gli incontri promossi sotto la firma "ConvergenX" producono
momenti di dibattito e di approfondimento, che rafforzano la dimensione
delle relazioni politiche disinnescando i meccanismi di
auto-rappresentazione e gli identitarismi.

Il resto sono cose che conosciamo da decenni, ma dalla cui intensità si
misura la profondità di un percorso: volantinaggi, articoli online,
contributi, eventi che si moltiplicano e che fanno sì che la scadenza
del 22 ottobre diventi il tema da bar, di cui molta parte della città
parla. E quando questo avviene, ciò che succede va oltre le più rosee
aspettative, e in corteo sfilano trentamila persone che non puntano alla
piazza del comizio, ma sfilano allegramente su una delle infrastrutture
più trafficate d'Italia: il Passante di Mezzo.

[^1]: Un racconto della vicenda è disponibile in Collettivo di fabbrica
    GKN, *Insorgiamo. Diario collettivo di una lotta operaia (e non
    solo)*, Edizioni Alegre, 2022

[^2]: Alcuni articoli di approfondimento sono disponibili all'indirizzo
    https://www.bolognaforclimatejustice.it/no-passante.

[^3]:  La manifestazione è stata convocata a partire dallo slogan 'Per
    questo, per altro, per tutto', dove il 'per questo' indica il tema
    locale --- nel caso bolognese, il Passante di Mezzo --- sul quale si
    concretizza la convergenza; il 'per altro', le tante lotte,
    rivendicazioni e ragioni che si legano al 'per questo', e che
    concretizzano la convergenza; il 'per tutto', l'ambizione di
    costruire una visione complessiva capace di mettere in discussione i
    rapporti di forza.
