---
author: "D(i)ritti alla Città (Rete per gli spazi pubblici dismessi)"
title: "Je suis un espace abandonné"
subtitle: ""
info: "Poesia scritta per la raccolta bolognese"
datepub: "Aprile 2022"
categories: ["spazi, occupazione, diritto all'abitare"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "79"
quote: "je suis partout"
---

Non ho bisogno di significato, in quanto spazio esisto semplicemente.

Vivo nel mio intreccio con gli elementi, con la natura. Con le persone
--- e le loro attività --- che hanno bisogno di inserirmi in una cornice
di senso.

Eppure questo senso ha bisogno di economie per riattivarmi, come
macchina di produzione.

Oppure questo senso ha bisogno di ecologie mentali e sociali per
rifrequentarmi, come corpo in relazione.

Je suis partout[^1].

Ed è ovunque la stessa storia. Resto uno spazio sospeso. Resto uno
spazio conteso.

Je suis un espace abandonné[^2].

Ma dipende da chi mi guarda, come capirmi.

Se come oggetto. O come soggetto.

Je suis partout.

Un ingranaggio delle transitorie visioni urbanistiche? O una dimensione
di re-invenzione collettiva?

Nous sommes abandonnés.

Nous sommes partout.

Nous sommes ensemble.

Nous sommes des gisements de possibilités[^3].

[^1]: Sono ovunque, ndr.

[^2]: Sono uno spazio abbandonato, ndr.

[^3]: Siamo abbandonati / Siamo ovunque / Stiamo insieme / Siamo
    giacimenti di possibilità, ndr.
