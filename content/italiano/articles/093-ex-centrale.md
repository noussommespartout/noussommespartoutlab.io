---
author: "Le compagne e i compagni di Ex Centrale"
title: "Ex Centrale"
subtitle: "Un hub delle periferie nella zona nord di Bologna"
info: "Testo scritto per la raccolta bolognese"
datepub: "Gennaio 2023"
categories: ["autogestione, autorganizzazione", "spazi, occupazioni, diritto all'abitare", "arti, musica, fotografia, performance"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "93"
quote: "un processo di rigenerazione e autorecupero"
---

Ex Centrale nasce in via di Corticella 129 a Bologna, in una zona
periferica a nord della città. Lo spazio in cui questo progetto
si sviluppa è stato abbandonato per quasi 70 anni, dagli anni
'50 in cui fungeva da centrale del latte. Dal
giugno del 2019 è in corso un processo di rigenerazione e
autorecupero che, a partire dalle istanze del quartiere, vuole
restituire alla città uno spazio in disuso trasformandolo in un luogo
pieno di cultura, politica, arte, musica, aggregazione e
protagonismo sociale.

Anche se nuova come esperienza di intervento sociale, Ex Centrale
nasce a seguito di --- almeno --- 15 anni di storia delle reti che hanno
gravitato attorno al Laboratorio Crash!, esperienza politica di
riferimento per il precariato° metropolitano bolognese e non solo. Dopo
ben 14 sgomberi, lo spazio di via di Corticella è una vittoria
conquistata negli anni di trattative con le amministrazioni locali per
il riconoscimento della valenza politica e sociale delle attività del
Lab. Crash!.

Lontani da una visione prettamente mutualistica e di vita
comunitaria che ha segnato buona parte dei centri sociali a partire
dagli anni '90, Ex Centrale, sin dalla sua apertura, si è voluta
identificare come nuova esperienza politica estremamente permeabile alla
realtà in cui si inserisce, si sviluppa e su cui vuole intervenire.

Lo scoppio della pandemia nel 2020 (a pochi mesi
dall'apertura) ha inevitabilmente influito sul ragionamento sul come
creare reti sociali attorno al progetto, portando in primo piano
tematiche rilevanti come la salute di prossimità e l'autotutela
collettiva. Proprio a partire da queste riflessioni, uno dei primi
interventi sociali di Ex Centrale è stato promuovere la creazione di una
rete di abitanti e associazioni del quartiere che si volevano attivare
per contrastare la problematica chiusura del CUP di Corticella, nel
pieno di una pandemia. L'intelligenza collettiva ha fatto sì che tale
rete di intervento politico si ampliasse quasi automaticamente a
tematiche cittadine e, in seguito, nazionali. Il susseguirsi di
iniziative pubbliche e convegni nazionali è ciò che ha permesso un
avanzamento teorico e pratico nella pretesa di una salute pubblica, di
prossimità e basata sulla prevenzione. Il confronto con la rete attivata
sulla tematica ha sviluppato un nuovo concetto di sanità universale che
promuova una salute non come assenza di malattia ma come benessere
fisico, psicologico, sociale ed economico.

Ci si è ritrovati, quindi, a partire da questa prima positiva
esperienza di interazione con la città, a ragionare sulle modalità di
intervento e di relazioni sociali da provare ad instaurare in una zona
di Bologna che ha le sue specificità. Si è constatato positivamente
il modus operandi che era stato adottato ed è nata una riflessione
attorno al concetto di "hub", una parola che oggi è centrale per
il panorama capitalista globale, ma che in realtà viene coniata dalle
lotte politiche di Minneapolis degli anni '30 portate avanti dai
lavoratori/rici della logistica che praticarono blocchi del
principale snodo commerciale della regione. In questo senso,
riappropriarsi del concetto di hub vuol dire immaginare Ex
Centrale come un centro di collegamento, sempre in evoluzione, di rete
di relazioni politiche, sociali, culturali, musicali, artistiche.
A tale parola viene poi aggiunta quella di "periferie", luogo in cui
siamo e vogliamo agire, per poi estenderci. Non a caso scrivevamo:
«Ex Centrale vuole essere un punto di riferimento autonomo nella
trama urbana: vogliamo costruire un luogo in grado di calamitare lotte,
sperimentazioni e nuove forme di vita, e un hub in grado di rilanciare i
conflitti nei circuiti della metropoli. Ex Centrale è situata nella
periferia bolognese e rivendica con fierezza questo posizionamento, e
vuole lavorare sui bordi urbani per costruire nuove forze e nuove
centralità».

Ci si è poi interrogati sul come declinare pragmaticamente una simile
connessione. Il primo passo che abbiamo messo in atto è stato quello di
rendere pubblica una campagna politica di sostegno al progetto,
articolata in un manifesto in 10 punti.[^1] Il manifesto voleva essere
un'interfaccia da cui partire per incontrare persone, realtà politiche,
gruppi sociali, associazioni, e chiunque altro fosse interessato a
collaborare nella costruzione dell'hub delle periferie. Proprio per
questa tendenza all'apertura e alla permeabilità del progetto di Ex
Centrale, si è deciso di non lanciare pubblicamente la campagna negli
spazi di via di Corticella 129, ma di organizzarne diverse in giro per
la città. Degli incontri informali in diversi spazi e locali di Bologna
durante i quali si potevano incontrare le attiviste e gli attivisti del
progetto per scambiare opinioni, riflessioni, dubbi, prospettive.

Crediamo, e speriamo, che tale pratica politica possa essere
d'ispirazione per chi, come Ex Centrale, ambisca ad avere una
riconoscibilità sociale e uno spazio di agibilità politica nella propria
città e non solo. Scriviamo questo testo in quanto, a seguito di questa
prima fase di campagna pubblica, i riscontri che sono stati ottenuti
sono stati nettamente positivi. Difficili da interpretare e da
coadiuvare, ma positivi.

In primo luogo vi è l'attenzione pubblica sollevata dall'apertura di un
simile percorso politico, che ci ha fatto pensare che i nodi politici
che abbiamo sollevato attraverso la stesura del manifesto fossero quelli
giusti e quanto mai urgenti. Fra tutti vi sono sicuramente quelli della
salute e dell'autotutela collettiva, come già ampiamente accennato in
precedenza, ma anche il modo stesso di immaginare la vita di uno spazio
sociale al giorno d'oggi che contamina e si fa contaminare da ciò che la
realtà è. La tematica dell'autorecupero e dell'ecologia politica come
linea guida della costruzione di uno spazio fisico. Il macro ambito
della cura, sollevato dai movimenti transfemministi° globali, come
strumento per affrontare e gestire uno spazio attraversato dalla realtà,
con tutte le sue contraddizioni di genere e dei generi e per scioglierle
dentro e fuori i propri confini. L'autogestione° come continuo confronto
e possibilità di migliorare sempre più per inventare nuovi modi di stare
insieme, di vita in comune e di costruzione di nuovi luoghi.

Questi punti sono tra i principali su cui abbiamo voluto aprire uno
spazio di dibattito continuo, di analisi, di elaborazione critica e di
confronto attraverso seminari, assemblee, discussioni pubbliche dentro
Ex Centrale.

In secondo luogo, la pratica politica del "tour di presentazione
dell'hub delle periferie in città" ci ha permesso di entrare in contatto
e stringere rapporti con un substrato associativo e artistico del
quartiere molto spesso senza possibilità di espressione o senza spazio
fisico. Non è un discorso specifico del quartiere Corticella o
dell'esperienza di Ex Centrale, ma crediamo che sia una condizione
generalizzabile alla città di Bologna e non solo. È proprio da questi
rapporti in profondità che, secondo noi, emerge il valore sociale dei
legami di quartiere.

Infine, restituire uno spazio fisico alla città è stata l'opportunità
che ha permesso di sperimentare nuove pratiche collettive di
autogestione. L'essersi messi in relazione con diverse persone nella
vita quotidiana dello spazio ci ha insegnato a non dar nulla per
scontato e a non adottare "modelli preconfezionati", ma rimanendo sempre
permeabili a tutte le suggestioni che provengono da chi ha a cuore un
progetto d'intervento sociale.

Su queste direttrici ad Ex Centrale sono nati diversi percorsi,
specialmente artistici e musicali, ragionati insieme a chi ha voluto
confrontarsi sui nodi appena esposti nella creazione di eventi pubblici
che non siano semplicemente "fruibili da utenti terzi", quanto piuttosto
mirano alla cogestione di tali momenti.

Il mondo della cultura e dello spettacolo è forse tra i più colpiti
dalle restrizioni anti-covid di questi anni. Lo è stato sia per
l'impossibilità fisica di organizzare eventi, ma anche per lo scarso
riconoscimento del lavoro artistico nel nostro paese. Per questo motivo,
crediamo sia uno dei settori sociali sul quale è prioritario investire e
nel quale creare relazioni virtuose.

Una tale sperimentazione porta con sé la necessità di una costante
autocritica rispetto a quanto fatto e a una serie di interrogativi sul
futuro dei progetti messi in campo.

Alcuni di questi sono sicuramente i seguenti: come è possibile
intervenire sulla decisionalità metropolitana a partire da una piccola
esperienza politica di periferia? Come mettere in connessione i vari
punti periferici dell'hub? Ovvero, come far relazionare i vari progetti
nati all'interno di Ex Centrale in maniera non forzata?

Queste sono alcune delle domande che ci stiamo ponendo attualmente.
Anche se in divenire, una direzione crediamo di averla presa, che è
quella, per l'appunto, di essere brave e bravi nel lasciare spontaneità
ai processi ma al tempo stesso saper dare chiare indicazioni, senza
creare esperienze artificiali. Un primo esperimento che abbiamo portato
avanti è quello di creare un festival autogestito che è stato pensato
completamente e collettivamente da tutti i soggetti che hanno proposto
iniziative nello spazio. Durante tale festival, oltre ad aver dato vita
a intrecci interessanti tra vari settori culturali, abbiamo affrontato
insieme il nodo della decisionalità politica. Discutere di cosa è la
città oggi insieme a chi più ne soffre i suoi sviluppi e trasformazioni
è un primo passo che abbiamo fatto per riconquistare la nostra
possibilità di decidere sui territori in cui viviamo.

Il futuro del processo costituente di una nuova esperienza sociale in
città dipenderà dal saper giostrarsi in un complesso intreccio sociale
reticolare che stiamo provando ad intessere. La cosa positiva è che
dipende interamente da noi e da chi in futuro si riconoscerà in questo
progetto politico.

[^1]: V. https://excentralebologna.it/il-manifesto.
