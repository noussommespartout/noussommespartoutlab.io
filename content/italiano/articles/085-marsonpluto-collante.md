---
author: "Mars on Pluto"
title: "Una sorta di collante che unisce"
subtitle: ""
info: "Intervista trascritta per la raccolta bolognese"
datepub: "Luglio 2022"
categories: ["lavoro, precariato, lotte sindacali", "storia, memoria", "arti, musica, fotografia, performance"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "85"
quote: "Mars on Pluto?"
---

### Come nasce Mars on Pluto?

David --- Mars on Pluto nasce nel febbraio 2016, quando Antonello del
centro sociale Vag 61 mi ha telefonato dicendomi: «Stiamo organizzando
un pranzo domenicale seguito dalla presentazione del terzo volume del
*Sol dell'avvenire* di Evangelisti. Valerio sarà presente e vorrebbe che
qualcuno suonasse e cantasse le canzoni citate nel testo... ti va di
venire?». È in quell'occasione che ho proposto a Margherita di
collaborare, ma allora non la conoscevo, né avevo mai suonato con lei.
Da subito, abbiamo trovato un'ottima intesa e, visto che il concerto era
andato molto bene, abbiamo deciso di andare avanti, di continuare a
suonare insieme.

All'inizio ci siamo concentrati soprattutto su alcune canzoni scritte da
Margherita e quindi il duo ha preso una direzione un po' diversa, più
cantautorale. Allora avevamo deciso di proporci ai locali con il nome
Thanks, Pluto e di inserire questo concerto tra le nostre attività
lavorative. Tuttavia, alcuni spazi sociali di Bologna e dintorni
continuavano a chiamarci e a chiederci di partecipare a iniziative
politiche per interpretare canti di lotta, tratti dal repertorio
anarchico, comunista e popolare, ovvero di continuare sull'onda di
quanto avevamo fatto per accompagnare Valerio Evangelisti.

È qui che ci si è subito posto un problema: come musicisti che fanno del
suonare la propria professione, avevamo in qualche modo bisogno che
venisse riconosciuto il nostro lavoro. Ovviamente ci rendevamo conto
che, contrariamente a un'attività commerciale, come un locale o un
teatro, nel circuito militante le disponibilità economiche sono basse:
bisognava trovare un compromesso che rispettasse il nostro bisogno di
riconoscimento professionale e al contempo non gravasse sulle casse di
chi ci «ingaggiava». Abbiamo così deciso di creare un progetto tutto
dedicato alla canzone politica, con un nome diverso ma in qualche modo
collegato all'originale: è nato così Mars on Pluto.

A chi ci chiede di esibirci in questo concerto, noi proponiamo il
rimborso di tutte le spese vive, a cui si può aggiungere un'offerta
libera secondo le possibilità dello spazio: che sia in denaro, con
l'acquisto del nostro CD o in altre forme di scambio... per noi non ha
importanza. Il punto non è trarre un profitto personale, ma fare
militanza attraverso la musica contrastando l'idea, purtroppo molto
diffusa anche negli ambienti antagonisti, che suonare è un passatempo e
non un lavoro. Se invece a chiedere un concerto di Mars on Pluto sono i
comuni per le commemorazioni del 25 aprile, o altri enti pubblici,
chiediamo un trattamento professionale. Il che ci consente, magari il
giorno dopo, di poter suonare per uno spazio sociale che non ha la
possibilità di pagarci.

Ma questo è per noi un criterio orientativo, non una regola
inderogabile. Ad esempio l'anno scorso ci è stato chiesto di suonare al
25 aprile del Pratello per un omaggio collettivo a Sante Notarnicola,
poco dopo la sua scomparsa. È questo il tipico caso in cui, ovviamente,
non ci siamo neanche sognati di chiedere rimborsi.

### Come sono collegate la musica e la militanza?

Margherita --- La parola militanza collegata alla nostra musica si
riempie di diversi significati e sfumature. Da una parte ci rendiamo
conto che i pezzi che proponiamo rischiano di finire nel dimenticatoio:
l'azione militante, quindi, consiste nel far vivere, far ascoltare e
cantare insieme parole pregne di valori e ideali che per noi sono
fondamentali. Tramandare quelle musiche e quelle parole svolge, in un
certo senso, una funzione educativa su chi non conosce quel repertorio,
magari per età o cultura familiare.

Anche per me è una continua scoperta. Infatti, sono cresciuta in una
famiglia democristiana che, pur avendomi trasmesso alcuni valori come
l'uguaglianza e la solidarietà, era ben lontana da qualsiasi forma di
riflessione politica assimilabile a quella praticata negli ambienti
antagonisti. Non a caso, se da un lato mi si mandava a «lezioni di
partigianesimo» dall'ex staffetta Tina, dall'altro mi si taceva tutto un
pezzo di storia locale, legata ai movimenti degli anni '70 e alla
repressione subita da alcune persone molto vicine alla mia famiglia.
Racconto questo per spiegare che, non avendo ascoltato a casa queste
canzoni, scoprire, imparare e cantare pezzi come *La Badoglieide* o
*Katanga* permette anche a me di approfondire alcune storie e praticare
quei valori assorbiti da bambina fuori dalla cornice del cattolicesimo.

Prima parlavo di funzione educativa perché questo termine per me è
indissolubilmente legato alla parola musica, sia come cantante di Mars
on Pluto, sia come formatrice in ambito scolastico (tra le mie attività,
insegno anche musica ai piccolissimi e mi occupo di progetti di
formazione agli insegnanti). Questo «pallino» mi ha portato a cercare
dei maestri che avessero in passato percorso la stessa strada: uno di
quelli a cui sono più riconoscente è senza dubbio Boris Porena.
Compositore e didatta, militante del Partito comunista italiano della
Bassa Sabina, negli anni '70 si trasferì da Roma nella campagna a nord
della città per sperimentare un fare musica lontano da specialismi e da
intellettualismi, con bambini e adulti del luogo, di qualsiasi
estrazione sociale e culturale. Anche nel suo modello, fondamentale per
la mia crescita, interagiscono queste parole: militanza, educazione,
musica.

Quando arrangiamo i brani della tradizione sono queste stesse parole che
ci guidano: cerchiamo di muoverci rispettando il loro piglio popolaresco
con scelte musicali che possano avvicinare l'ascoltatore al messaggio
che queste canzoni portano con sé. E, consci del potere di diffusione
culturale che in quel momento esercitiamo, prestiamo molta attenzione
anche alla scelta del repertorio: prediligiamo brani che ci piacciono,
che ci interessano, e ne scartiamo altri, più conosciuti, perché
vogliamo che i nostri concerti siano anche un'occasione per ampliare la
propria cultura e per scoprire qualcosa di nuovo, esattamente com'è
successo e succede a me.

### Prima dicevate che a volte vi risulta difficile spiegare, anche in ambienti antagonisti, che quella del musicista è una professione come le altre: potete chiarire questo punto? E come gestite il fatto che il vostro tipo di militanza coincida con il vostro lavoro?

David --- Noi siamo musicisti, ed è questo quello che facciamo per
lavoro. La nostra professione si svolge soprattutto in orari serali e
nei giorni festivi. Momenti che per le persone con un lavoro ordinario,
sono momenti di svago. È forse qui che si crea l'incomprensione. Molt\*
compagn\* sono educator\*, lavorano nella sanità, nella scuola, alcun\*
in fabbrica. Insomma sono persone che solitamente ci vedono lavorare nel
momento esatto in cui loro si riposano. Capita spesso che quando noi
diciamo: «Ieri non sono riuscito a venire alla tale iniziativa perché
lavoravo», ti guardino e ti dicano: «Ma che lavoro fai scusa?», perché
magari erano le 9 di sera, o era una domenica...

Quindi, parte della nostra militanza riguarda anche il contribuire a
creare una cultura dove il lavoratore dello spettacolo è un lavoratore
con una dignità pari agli altri. Ciò non toglie che possiamo decidere,
ad esempio durante un'iniziativa politica, che il nostro compenso sia
soltanto la cena o le spese di viaggio. Quello che facciamo però rimane
il nostro lavoro e ha la stessa importanza di chi sta dietro al bancone
del bar del centro sociale o a quella di chi volantina. È la stessa
cosa, ha la stessa sostanza. Non dico, quindi, che stiamo «regalando»
una cosa più importante, ma che stiamo portando una roba che cerchiamo
di fare con una certa cura, in un certo modo.

Ovunque suoniamo, curiamo la nostra competenza strumentale, la scelta
dei brani e la qualità anche della parte tecnica. La richiesta che
facciamo agli spazi in cui suoniamo è che la resa di un nostro concerto
sia accettabile, meno approssimativo possibile, con microfono e cassa
funzionanti, ecco. A volte capita di suonare in un contesto in cui la
gente fa casino e non si capisce nulla, oppure a orari impossibili
davanti a quattro persone mezze addormentate, per errori di valutazione
di chi organizza la serata.

Per fortuna però, ci sono anche molti esempi positivi. Posti in cui
veniamo pagati, subito e anche più di quanto pattuito. O luoghi in cui
capita che gli organizzatori si dispiacciano di non poterci dare di più,
o si facciano in quattro per soddisfare le nostre richieste tecniche.
Questa non è una cosa scontata e forse dipende dalla sensibilità di
ognuno e da come viene recepita la nostra proposta.

### Ma secondo voi, la faccenda sarebbe diversa se foste un gruppo «famoso»?

Margherita --- La questione è che con la musica, come con la scrittura o
il teatro, se tu sei un nome famoso allora automaticamente sei tenuto in
una certa considerazione, indipendentemente dalla tua bravura. Perché il
nome importante fa sì che in automatico scattino dei meccanismi per cui
deve esserci un trattamento speciale, con tutti i riguardi.

Questo lo verifichiamo nel nostro lavoro quotidiano; sia nel cosiddetto
mainstream, dove a contare è l'apparenza più che quello che sai fare,
sia negli ambienti politicizzati. Ci sono lo scrittore o l'attore o il
fumettista politicamente noti e loro, a prescindere da quello che fanno,
attireranno gente. È anche vero che se fossimo «famosi», forse ci
toccherebbe interfacciarci più spesso con le istituzioni e con l'annoso
problema dell'accettare o rifiutare un lavoro ben pagato per uno
«sgradevole» committente, che magari potrebbe strumentalizzare il nostro
messaggio e utilizzarlo per sostenere scelte che non condividiamo.

Per esempio, a Bologna, nel 2019, si è tenuto un concerto di
presentazione del cd *Garofani rossi*, ovvero *Canti e musiche della
Resistenza e delle rivoluzioni nel mondo*, reinterpretate da un
quartetto di musicisti abbastanza noti a livello nazionale, e pubblicato
dall'etichetta di Paolo Fresu. Presentato dall'Istituto Gramsci in
collaborazione con l'Arci, è un album dedicato a un nome inattaccabile,
il fotografo Mario Dondero. La serata sicuramente sarà stata accurata
dal punto di vista tecnico, ma è stata preceduta dal saluto ufficiale
dell'allora assessore alla cultura, oggi sindaco della città; lo stesso
assessore che, in quota Pd, ha contribuito a sgomberare molti spazi
occupati e che non ha promosso certo una politica congruente ai testi
delle canzoni di quel concerto, anzi.

Non avremmo mai voluto essere nei panni di quei musicisti!

### Quindi ci sono luoghi dove declinate l'invito a suonare? E invece altri che andate a cercare? E perché?

David --- Una nostra regola, ad esempio, è quella di non partecipare mai
a campagne elettorali, proprio per non dare l'impressione di essere
identificati come elettori di uno specifico partito. Anzi, da
osservatori delle forti divisioni all'interno dei movimenti di sinistra,
è interessante notare che noi sul palco, senza essere ufficialmente
«organici» a un gruppo preciso, rappresentiamo una sorta di collante che
unisce anziché dividere, attraverso le musiche che suoniamo.

Quando veniamo ingaggiati, decidiamo di volta in volta se aderire o no.
Sicuramente avremmo rifiutato di partecipare a un evento come quello dei
*Garofani rossi* citato prima: non sosteniamo la giunta bolognese e non
ci mettiamo al servizio della loro narrazione; però ci è capitato di
suonare per piccoli comuni, in occasione di eventi istituzionali,
laddove abbiamo trovato un minimo di coerenza politica. Quando si tratta
di spazi antagonisti, difficilmente rifiutiamo, perché aderiamo
completamente al tipo di militanza praticata in quei luoghi.

### Come avete intenzione di sviluppare questo vostro progetto in futuro?

Margherita --- Per quanto riguarda il repertorio ci piacerebbe continuare
come stiamo già facendo: raccogliendo le richieste del pubblico che
puntualmente arrivano a ogni fine concerto e integrandole, riarrangiate
alla nostra maniera. Per quanto riguarda i concerti futuri, invece, ci
piacerebbe andare a suonare in posti che ancora non conosciamo, o in
luoghi in cui avevamo programmato di suonare in periodo di lockdown e in
cui non siamo ancora riusciti a recuperare i concerti annullati.
