---
author: "Anonim*"
title: "Il pranzo dell'avvenire"
subtitle: "Una settimana di vita della mensa autogestita di Losanna"
info: "Testo scritto per la raccolta svizzera romanda"
datepub: "Luglio 2022"
categories: ["solidarietà, mutualismo", "autogestione, autorganizzazione", "sabotaggio, azione diretta"]
tags: ["amende", "amour, love, amoureuxse", "arrestation", "autocritique", "banque", "barricade", "bouffe", "café", "coller", "colère", "compost, composter", "coulant", "cuisiner, cuisine", "doute", "eau", "erreur", "foot", "fourmi", "gueule", "huile", "huile", "jardin", "justice", "logique", "machine", "mail, email", "mercredi", "outil", "peur", "peur", "pizza", "prix", "réunion", "salade", "stockage", "sucre", "sueur", "visage", "voiture, bagnole"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "47"
quote: "questa dimensione sociale della schifezza"
---

## Lunedì & Martedì

In teoria, i lunedì e i martedì dovrebbero essere i giorni di riposo
della mensa, ma non sempre è così. Non puoi mai sapere se a inizio
settimana arriverà una consegna imprevista o se capiterà un altro di
quei piccoli incidenti caotici che fanno aumentare le provviste. Ci
arrivano messaggi a qualunque ora del giorno: "Ehi avrei due chili di
zucchine, vi interessa?" Quasi sempre la risposta è sì: con una
cinquantina di persone in contatto permanente, c'è sempre qualcun\*
disponibile per spostarsi, recuperare le zucchine e metterle in fresco
fino a sabato. Col tempo, il cibo ci arriva da orizzonti sempre più
diversi: siccome rifiutiamo di affiliarci alle centrali di raccolta
statali, l'unica è arrangiarsi. Perché certe forme di solidarietà non
sono dal lato giusto della barricata, e si vede.

Una volta le brigate della solidarietà di Ginevra, una realtà che
rivendica l'antifascismo e l'autodifesa popolare, ci hanno consegnato
una quantità assurda di barrette di cioccolato. Tutte stipate in dei
gran contenitori industriali. Non sapevamo da dove provenivano e non
volevamo saperlo. Quelle barrette di cioccolato le abbiamo distribuite
per settimane e settimane. Poi ci sono i rifugi antispecisti che si sono
messi d'accordo con i negozi della zona per recuperare la frutta o la
verdura, il più delle volte intatta. Questi rifugi ci portano il cibo
che le persone animali da loro ospitate non mangiano. Chiaramente non
amano i broccoli e gli agrumi perché l\* compagn\* ce ne portano casse
intere. La pasta al pesto di broccoli è diventata il marchio di fabbrica
della mensa.

E poi, soprattutto, ci sono gli imprevisti: la scuola che non è riuscita
a smaltire le scorte di cereali o di marmellata industriale, la fattoria
locale che si appresta a buttare sette chili di pomodori, una compagna
che sta traslocando, un ristorante che chiude e dà via la roba per la
cucina, l'alimentari del quartiere che ci regala il caffè e le spezie
per sostenerci, quello più lontano, ma gestito da una compagna, che
crede in noi e che si fa in quattro per aiutarci a raccogliere soldi o
della farina bio, l\* amich\* che ci aiutano a organizzare una grossa
operazione di recupero pentole...

Una volta ci hanno proposto di andare a recuperare l'invenduto di un
cinema. Ci siamo ritrovat\* ad attraversare la città con un carrello
pieno di coca cola e m&m's. Ci sentivamo come venditori diretti di
zucchero industriale e la cosa ci ha messo talmente a disagio che, dopo
averne discusso, abbiamo deciso collettivamente di lasciar perdere.

Nei primi tempi questa forma di approvvigionamento spontaneo era
difficile da gestire: abbiamo dovuto recuperare in tutta fretta tre
frigoriferi e un congelatore, stampare dei volantini e le etichette per
le conserve, chiedere aiuto all\* compagn\* per installare alla riscossa
l'impianto elettrico del locale, sistemare il forno e i frigoriferi e
fare dei lavori in una parte del locale per non dover più ammucchiare le
scorte sui tavoli.

### Come sono arrivato qui? (1)

*Ero tornato a Losanna da pochi mesi. Nel mio piccolo sono sempre stato
attivo nelle cause della trasformazione alimentare, dell'agricoltura e
dell'artigianato, ma di solito lo facevo da solo e la cosa non aveva
molto senso per me. In generale, non riuscivo a trovare un collettivo
affine in cui investire le energie.*

*Era aprile e stavo mangiando con due amici a Losanna. Anche loro erano
coinvolti in diversi progetti o attività. Stavamo parlando delle nostre
esperienze nelle cucine collettive e nelle mense militanti, nella
distribuzione di pane fatto in casa e di verdura, e della nostra
frustrazione di non poter fare queste esperienze a Losanna. Dal canto
mio, avevo iniziato a riflettere a chi avrebbe potuto essere
interessat\* a mettere su un collettivo che combinasse cucina e
militanza.*

*Due giorni dopo, uno di loro riceve un messaggio da un amico che non
vedeva da anni. Da qualche settimana era nata una mensa autogestita° e
stavano cercando persone per unirsi al progetto. Quante coincidenze...
Ci siamo andat\* il sabato dopo per informarci, dare un'occhiata e
vedere come andavano le cose. Da allora ci andiamo tutti i sabati.*

## Mercoledì

*Il 25 luglio 1943, a Casa Cervi (un luogo importante della militanza
antifascista in Emilia-Romagna) si venne a sapere dell'arresto di
Mussolini. Nel libro* I miei sette figli*, Alcide Cervi ricorda la gioia
che riempì i cuori e come suo figlio gli avesse annunciato:* "*Papà,
offriamo una pastasciutta a tutto il paese*"*. I paioli di pasta,
preparati da e per tutti, furono portati in piazza. 380 chili. Quella
della "pastasciutta antifascista" è una storia fantastica, un gran bel
mito antifa: è con questo nome che etichettiamo le confezioni di pasta
fatta in casa del nostro mercatino gratuito.*

I mercoledì organizziamo una piccola autoproduzione di pasta fatta in
casa. Un amico ha recuperato una macchina professionale che sputa sei
chili di pasta all'ora. Ogni settimana ci lavorano due o tre persone, ed
è sempre una figata. Accompagniamo con le mani la pasta fresca che esce
calda calda dalle fauci della sfogliatrice, la tagliamo e la stendiamo a
essiccare su dei telai da serigrafia. La pasta è bellissima e
buonissima, lo dicono tutti. È una cosa che abbiamo iniziato a fare fin
dalla prima mensa. Era un 25 aprile, data che per il nostro compagno
italiano era particolarmente significativa: era una bella commemorazione
dell'insurrezione di Milano che ha portato alla fuga delle truppe
nazifasciste il 25 aprile 1945, noto in Italia come il Giorno della
Liberazione.

### Come sono arrivato qui? (2)

*Di origini italiane, sono capitato in Svizzera un po' per caso. Il
lockdown è stato un periodo molto particolare per me, il tempo era come
sospeso. E mi sentivo sospeso anch'io, tra il sollievo di non essere a
casa mia in Italia e il timore di rimanere incastrato qui, in casa
altrui. Tanto più che nonostante gli anni passati qui, ho sempre avuto
l'impressione di essere un turista, un po' a disagio. E sapevo che non
sarei riuscito ad accettare di aver lasciato l\* mie\* amich\* in Italia
finché non avessi trovato il modo di immergermi nella città in cui abito
e vivo.*

*Siccome mi piace il canto sociale, sono entrato a far parte del Coro
Anarchico di Losanna. Ed è sulla mailing list del Coro che sono venuto a
sapere per la prima volta dell'idea di una mensa autogestita:
distribuzione di beni alimentari d'emergenza. Avevano bisogno d'aiuto
per recuperare il cibo e per qualunque risorsa ed esperienza che potesse
contribuire alle attività della mensa. Io non avevo né risorse né
esperienza ma era un'occasione troppo bella di integrarmi concretamente
nelle lotte locali, di contrastare collettivamente quel senso di
impotenza imposto dalla pandemia --- specialmente quando seguivo quello
che stava succedendo in Italia --- e di contribuire ad aggiungere un
pizzico di solidarietà al luogo in cui vivevo.*

## Giovedì

I giovedì sono un caposaldo della mensa. Di sabato pianifichiamo le
spedizioni di recupero del giovedì sera. In generale si fanno in due, in
macchina. Se non hai mai fatto recupero nella tua zona, ecco un mini
tutorial. Prima di tutto devi studiarti il territorio e localizzare gli
obiettivi: i supermercati. Un primo giro ti permette di fare la cernita
dei negozi accessibili: quelli che lasciano i cassonetti aperti, che non
tritano gli alimenti e non li irrorano di candeggina. Se sei
motorizzato, i supermercati delle zone industriali sono spesso
preferibili: meglio lasciare i cassonetti dei supermercati del centro,
accessibili a piedi o in bicicletta, a chi non ha la macchina. In
Svizzera, la coop e la migros meriterebbero la medaglia dell'eccesso
securitario per la loro gestione logistica dei rifiuti, impenetrabile e
rigorosa. I container vengono generalmente chiusi a chiave in depositi o
caricati direttamente sui camion diretti alla discarica. OK, a volte
riusciamo a entrare nei camion, ma le multe possono essere salate. Qui
non si scherza con gli yogurt scaduti e le confezioni di banane
strappate.

Una volta stabilita la lista dei negozi, si passa alla localizzazione
dei container e dei parcheggi riparati dalle telecamere di sicurezza,
per evitare che i volti e le targhe siano identificabili. Se non lasci
tracce del tuo passaggio, i negozi non hanno motivo di visionare le
riprese. Ebbene sì, in un regime capitalista anche i rifiuti sono
proprietà privata, e il fatto che altr\* li usino per nutrirsi è
considerato un furto. Ci sono negozi che se ne fregano altamente e sono
molto aperti al "furto" di questa ingombrante proprietà. L'ideale è fare
una mappatura dettagliata delle telecamere di sicurezza e dei parcheggi.
Può sembrare *too much* ma è un bell'aiuto per chi non lo ha mai fatto!
Per non sentirvi a disagio privilegiate i negozi accoglienti, così non
vi dovrete travestire da black bloc per andare a recuperare due
pomodori.

Durante le spedizioni di recupero, non dimenticare di portarti dietro
qualcosa per nascondere il viso all'occorrenza (poi ognun\* gestisce il
rischio come meglio crede), scarpe impermeabili, vestiti a cui non tieni
troppo, guanti di gomma o addirittura da cantiere, una lampada frontale,
cassette da supermercato e sacchi di plastica. Eccoti pront\* a
sguazzare nei container. Per rifornire una mensa bisogna recuperare il
più possibile, il che a volte significa perlustrare il fondo del
cassonetto. Quant\* compagn\* abbiamo visto lanciarsi in missioni di
speleologia monnezzara, rovistando tra i pannoloni di plastica e i
barattoli di marmellata spiaccicati per trovare dei tesori! C'è chi fa
fatica: siamo tutt\* condizionat\* a schifarci per tutto quello che non
è calibrato, asettico, standardizzato, per la roba sfondata,
asimmetrica, sbrodolante, viscida. Ma se decostruisci un attimo questa
dimensione sociale della schifezza, il recupero diventa semplicemente
l'attività del giovedì, una specie di caccia al tesoro che quasi quasi
consiglieresti alle famiglie. Non si può esprimere la gioia collettiva e
trionfale che si prova nello scovare decine di albicocche impeccabili,
sottratte al loro funesto destino industriale per diventare marmellata.

Alla fine della spedizione si riparte, cercando di non fare caso alla
puzza di spazzatura che emana dalla ventina di cassette stipate nel
cofano. Si torna al locale di stoccaggio, dove ci aspettano l\*
compagn\* iscritt\*. Mettiamo la musica a manetta e laviamo tutto il
cibo recuperato con acqua e bicarbonato per poi immagazzinarlo.
Guardando in piena luce la roba che hai recuperato, a volte realizzi che
forse sei stat\* un po' ottimista. Quello che alla luce della torcia
frontale e attraverso i guanti sembrava fresco e succoso è in realtà
mezzo marcio. Pazienza, andrà nel compost. Dopotutto il suo destino era
quello: se il capitalismo non lo ha voluto, saranno i ricci e le
formiche a mangiarlo. Faranno la cacca sulle insalate dell'orto, che
forse un giorno potremo servire in mensa. Il cerchio si chiuderà. Con
quello che tre negozi buttano via in una sola giornata, un centinaio di
persone mangia alla nostra mensa tutti i sabati e un'altra ventina fa la
spesa al mercatino gratuito.

### Come sono arrivata qui? (3)

*Un po' di tempo fa ho iniziato a militare in un sindacato studentesco.*

*Mi sentivo fuori posto in un ambiente scolastico opprimente e c'era
molta rabbia dentro di me. Il sindacalismo era uno strumento che mi
permetteva di trasformare questa rabbia in una lotta collettiva, una
realtà in cui impegnarmi che si è imposta a me come un'evidenza. Tant'è
vero che un anno dopo ho fatto un burn-out della militanza e ho iniziato
a mettere in discussione un impegno politico che non mi stava dando ciò
a cui aspiravo. E così con alcun\* compagn\* abbiamo iniziato a sognare
cooperative dove coltivare la terra, fare il pane e organizzare pranzi
sociali. Ne abbiamo parlato un sacco ma non ne nasceva niente, eravamo
troppo impegolat\* nei conflitti interpersonali e nella ricerca di nuove
energie. È stata una grossa delusione. Guardavo con desiderio le realtà
di movimento, alle quali l\* compagn\* del sindacato non si
identificavano per niente. Sentivo di aver bisogno di cambiamento, di
incontrare persone nuove e di fare cose diverse, altrove. Finché un
giorno mi sono imbattuta in un volantino della mensa autogestita.*

*Il sabato successivo mi sono presentata con 5 chili di pane che avevo
preparato all'alba. Il seguito della storia è pieno d'amore. È stato
come trovare una grande famiglia in cui mi sentivo bene. Ci diamo il
tempo di intrecciare piano piano dei legami forti e di organizzare
progetti collettivi sempre più numerosi. Usare le mani come arma
politica è un piacere immenso. Poco importa chi sei e da dove vieni: in
mensa non c'è questo senso di essere giudicat\* in permanenza, puoi
andare lì ed essere te stess\*. Per me è un vero e proprio lavoro di
ricostruzione: il cibo non è più una sofferenza solitaria ma una fonte
di gioia collettiva, solidale e politica. È una cosa che mi fa bene.*

## Venerdì

Anche il venerdì dovrebbe essere un giorno di pausa, ma non sempre è
così. A volte la spedizione di recupero del giorno prima non è bastata e
tocca fare un altro giro. Però di solito se facciamo qualcosa di venerdì
e perché abbiamo voglia di pianificare sul lungo termine. La
distribuzione del cibo ogni sabato crea dei legami. Mangiare è
un'attività che avvicina le persone, le aiuta a organizzarsi, e non è
concepibile che una mensa autogestita ignori le lotte che la circondano.
Uno dei progetti che stiamo portando avanti collettivamente è quello di
una mensa ambulante. Naturalmente ci vogliono soldi per una roba del
genere. E a far soldi non siamo proprio delle cime. Una volta ci siamo
fatt\* in quattro per organizzare una giornata pizza. Inizialmente
volevamo chiamarla "Pizzacab", ma per finire, scoraggiat\* dalla nostra
stessa mancanza di immaginazione, abbiamo optato per "Pizz@more", che è
molto meglio: non c'è niente di più rivoluzionario dell'amore. Ci siamo
organizzat\* per trovare forni, impasto e salsa. Gli sbirri hanno dato
un'occhiata ai nostri forni da paura e hanno chiesto: "Cosa sono questi
cosi giganteschi e non a norma?". E il cuoco ha risposto: "Boh, se avete
un transpallet potete spostarli, noi intanto facciamo le pizze". Sono
filati via. È venuto un buon centinaio di persone a mangiare la pizza e
abbiamo distribuito una trentina di calzoni per strada. Era la decima
mensa che organizzavamo, un grande momento. L'unica cosa è che nessun\*
dell\* volontari\* della mensa ha pensato a organizzare un bussolotto
per le offerte di sottoscrizione°. E così a livello di raccolta fondi
non abbiamo tirato su quasi niente.

Durante le grandi manifestazioni femministe di giugno abbiamo
personalizzato un carrello della spesa per circolare in mezzo al corteo
e ci siamo fatti un po' di soldi vendendo la nostra pasta antifascista
autoprodotta e... viola! C'è stata la volta in cui abbiamo preparato la
pasta in un calderone al parco, l'abbiamo chiamata la *Festa della santa
strega.* O quando abbiamo improvvisato un'altra pizzeria alla Promenade
de la Solitude a Losanna insieme a un gruppo che lotta contro le
repressioni subite dall\* militant\*: *Contre la répression judiciaire,
pizzas solidaires* (Contro la repressione giudiziaria, pizze solidali).
Quella volta un po' di soldi li abbiamo fatti. Per la giornata falafel,
il gioco di parole più a cavolo che abbiamo trovato è stato
*Antifa-lafel.* Cuciniamo per le casse di solidarietà anti-repressione o
di mutuo soccorso giudiziario, per aiutare a pagare le multe di chi
occupa case abbandonate e per sostenere chi partecipa ai blocchi
rivoluzionari, rischia l'espulsione dalla Svizzera o lo sfratto o non ha
un'assicurazione sanitaria. Naturalmente è una cosa che fa incazzare il
governo, il fatto che distribuiamo del cibo gratuitamente, che non
abbiamo bisogno di loro, che non ci serve il loro accordo per esistere,
rifornirci e occupare i parchi pubblici, al di fuori di ogni regola
tranne quelle che stabiliamo noi. L'amministrazione fa di tutto per
romperci ma per ora non ci lamentiamo. Una volta la macchina di
un'associazione era rimasta parcheggiata di traverso per pochi minuti,
giusto il tempo di recuperare tre cassette di verdura da distribuire
l'indomani a della gente che aveva perso il lavoro a causa del Covid.
Appena le guardie hanno visto che metà della gomma oltrepassava la linea
bianca, si sono precipitate ad appiopparci 200 bombe di multa.

Vorremmo che la nostra mensa mobile fosse in grado di reagire
velocissimamente, di raggiungere in pochi minuti i luoghi delle
occupazioni, delle manifestazioni, degli arresti, davanti ai
commissariati dove sono detenut\* l\* compagn\*. Più c'è da mangiare,
più la gente rimane. Bastano un banchetto di crêpes vegane e due casse
acustiche per raddoppiare il numero di persone che manifestano davanti
al posto di polizia e per triplicare il loro tempo di permanenza.

### Come sono arrivata qui? (4)

*Ho partecipato a diverse manifestazioni e assemblee ma non ho mai
militato sul serio. Ho votato per un po', poi ho smesso, poi ho votato
scheda bianca, ma in fin dei conti non mi sono mai veramente impegnata
in un movimento. In Svizzera, non ho mai trovato una rete dove mi
sentivo a mio agio. E così investivo le mie energie in altre cose, ma
continuando a domandarmi dove fossero quell\* "altr\*" con cui sarei
stata bene.*

*Durante il lockdown ho sentito il bisogno di ritrovare una realtà
concreta per agire anziché restarmene a distanza a pensare da sola. Mi
sentivo arrabbiata, ferita dall'insidiosità di tutte quelle subdole
forme di dominazione ordinaria. E poi negli ultimi tempi ho iniziato a
farmi delle domande sull'anarchia, e la cosa mi è sembrata di una logica
evidente. E proprio mentre questi pensieri si stavano facendo strada
dentro di me, ho sentito parlare della mensa autogestita.*

*Ci sono andata così, semplicemente, un sabato mattina alle nove. La
lotta per il cibo era una cosa alla quale non avevo mai riflettuto, ma
col senno di poi mi sembra un'idea geniale ed essenziale, che ho
scoperto cucinando e mangiando con tutta la squadra della mensa. Poi
vabbè, l'idea può essere sensata quanto vuoi ma l'unica cosa che
cambierà sono le facce dei buffoni sulle banconote. Ma noi andiamo
avanti comunque.*

## Sabato

Di sabato, in attesa che sorga il "sol dell'avvenire", facciamo il
pranzo dell'avvenire. Quante persone vengono a cucinare in mensa? Ecco
un'informazione che farebbe gola a quei pagliacci in uniforme che
detengono il monopolio della violenza legittima: diciamo quindi tra 1 e
1000. Questo mix eterogeneo e ad alta mobilità si ritrova la mattina, si
beve un caffè e inizia a portare su le cassette di cibo raccolte di
giovedì. Facciamo una specie di mucchio, una piramide di cassette verdi
con la pasta autoprodotta, la frutta, la verdura e i prodotti
impacchettati. Il menù viene elaborato collettivamente facendo
attenzione a cucinare i prodotti più rovinati, in modo che quelli
preservati meglio possano essere distribuiti al mercatino gratuito.
Basta una breve discussione il sabato mattina per designare una persona
di contatto in caso di visita della polizia, una che tenga d'occhio il
mercatino gratuito, un gruppo pronto a distribuire cibo per strada e una
persona disponibile per andare a fare qualche spesa aggiuntiva (certi
prodotti di base sono difficili da recuperare, specialmente il sale, lo
zucchero e l'olio).

E poi, sotto a cucinare. Ovvio, si potrebbe pianificare ogni cosa, le
squadre, l\* responsabili, la divisione dei compiti ai fornelli, i
taglieri, il cotto e il crudo. Ma funziona tutto in maniera spontanea.
Ti senti motivat\*? Mettiti ai fornelli. Hai voglia di chiacchierare?
Vai a tagliare le verdure sul tavolone. Preferisci startene tranquill\*?
Pulisci i frigoriferi e le provviste. Hai voglia di stare all'aria
aperta? Vai a sistemare i tavoli o a svuotare il compost. Ti va di
cazzeggiare? Fatti una partita a biliardino, prepara il caffè per
tutt\*, lacca le unghie a un\* compagn\*. Un paio d'ore dopo iniziamo ad
allestire la mensa, all'esterno se fa bello, all'interno se fa brutto.
Da un lato dell'edificio ci sono i pasti caldi che verranno distribuiti
sul posto, con sedie e tavoli a sufficienza per 30-40 persone.
Dall'altro lato si allestisce il mercatino gratuito o a offerta libera,
aperto a tutt\*, con i prodotti da cucinare. L'idea di questo *freeshop*
ci è venuta in un secondo tempo. Abbiamo realizzato che un'iniziativa di
solidarietà popolare doveva lavorare su più fronti: ci sono le persone
che hanno voglia o bisogno di un pasto caldo perché non hanno modo di
prepararselo da sole o perché vogliono farsi quattro chiacchiere
all'aria aperta. Per quelle invece che hanno voglia o bisogno di
cucinare per se stesse o per l\* loro car\*, c'è il mercatino gratuito.
E poi non è che a tutt\* piacciano le stesse cose: non cuciniamo tutt\*
allo stesso modo, non usiamo le stesse spezie, non abbiamo le stesse
allergie o intolleranze.

A volte l\* habitué del mercatino gratuito ripartono, a volte si fermano
a mangiare al parco. Invitiamo sempre tutt\* a tornare il sabato dopo,
dalla mattina, per partecipare alla cucina e al menù. E spesso la gente
lo fa. Durante le nostre riunioni, si percepisce questa volontà
condivisa di creare uno spazio anticommerciale di quartiere e quindi di
lottare per una vera diversità sociale senza rinunciare all'autocritica
necessaria a evitare qualunque forma di logica oppressiva. Non vuol dire
che ci riusciamo sempre, ma ci proviamo. Ovviamente, la socialità e le
affinità di un luogo di militanza favoriscono un certo ripiego su se
stess\*, ma questa è una cosa che cerchiamo costantemente di mettere in
discussione.

Verso mezzogiorno i pasti sono pronti: pasta, tortillas, insalatone,
couscous, verdure saltate, macedonia, smoothies, ecc. Ci sono perfino
compagn\* che portano l'olio al tartufo, spugnole e altre amenità
rubacchiate. Roba dell'altro mondo.

Qualunque sia il menù, prima di servire le persone sul posto
organizziamo la distribuzione nelle strade del centro: tra 30 e 40
sacchetti contenenti un pasto caldo, posate e una macedonia. Questi
sacchetti sono per chi non ha voglia di spostarsi o di socializzare. Le
ore che seguono sono il momento migliore: mangiamo tutt\* insieme. La
voce è girata all'interno delle reti militanti, delle associazioni più
ufficiali e addirittura in alcuni servizi sociali governativi.

C'è stata la volta in cui un'assidua frequentatrice del *freeshop* è
arrivata carica di vaschette di un piatto che aveva preparato a casa.

La volta in cui un'altra ci ha "pagat\*" con un po' di maria.

C'è stata una sarta indipendente che, venendo a pranzare, ci ha portato
una scorta di mascherine cucite a mano per la mensa. Ottimo, le
mascherine e il disinfettante per le mani costano caro.

Una volta è arrivata un'anarchica di fama internazionale. Eravamo lì lì
per farle un picchetto d'onore. Lei fa: "Ciao, vi ho portato dei
tupperware. Che si mangia, pasta?". E si è seduta nell'erba.

La volontaria di un'altra associazione una volta è venuta a recuperare
tutta la nostra frutta per fare marmellate e sciroppi per il *freeshop*
del sabato successivo.

A tavola una militante ci ha raccontato di aver conosciuto Evo Morales
quando era ancora un giovane attivista per gli indigeni, prima che
diventasse il presidente della Bolivia che avrebbe tenuto testa agli
USA.

Quando ci siamo accort\* che qualcun\* aveva pescato nella cassa del
*freeshop*, abbiamo avuto un lungo dibattito per decidere se anche la
cassa fosse self-service. Non male come concetto, proporre soldi a
offerta libera.

Una volta ci hanno detto che nei rifugi della zona mancavano i prodotti
di pulizia e abbiamo organizzato un workshop° per confezionare
detersivi.

C'è stata la volta in cui una persona ha organizzato un mercatino
dell'usato spontaneo.

O quella in cui dell\* amich\* squatter ci hanno portato 200 conigli di
cioccolato da mezzo chilo. Un quintale di cioccolato. Per settimane li
abbiamo distribuiti, mangiati, sciolti, spostati da un magazzino
all'altro.

Una volta (anzi, più di una) delle persone del vicinato che stavano
traslocando ci hanno portato tutto il cibo avanzato dai loro armadi.

C'è stato il compagno italiano che ci ha spiegato che tutte le insalate
verdi che recuperavamo andavano saltate in padella con l'aglio. Molti di
noi lo hanno preso per il culo, ma gli stessi poi hanno assaggiato
l'insalata cotta. Da allora la facciamo quasi tutte le settimane.

Una volta, un compagno della mensa per sbaglio ha messo al *freeshop* la
scorta di birre recuperate che avevamo messo da parte per la riunione
del pomeriggio. Se avessimo creduto alla giustizia punitiva, il compagno
se la sarebbe passata brutta. Ma fanculo la giustizia punitiva, abbiamo
fatto la riunione bevendo succhi di frutta. E da quella volta abbiamo
smesso di bere alcool durante le riunioni.

Una volta (anzi, più di una), le guardie hano parcheggiato di fronte a
noi. Lo fanno per spaventare la gente che viene a mangiare. Vogliono che
circoli la voce che la mensa non è un posto sicuro per chi non può
permettersi di farsi controllare i documenti. Pura strategia di
intimidazione e divisione. Quando la massa mangia insieme ha più
possibilità di parlare e organizzarsi, e questo non piace a tutt\*.
Quando fa caldo e mangiamo fuori, non possiamo fare molto per evitare la
sorveglianza. Siamo più tranquill\* quando fa freddo e mangiamo dentro.

Dopo aver sparecchiato, è l'ora della riunione. Facciamo il debriefing,
pianifichiamo la settimana successiva, contiamo i soldi, parliamo dei
prossimi progetti.

Chi può diventa parte integrante della mensa e contribuisce a
migliorarla costantemente, semplicemente e collettivamente.

Sudat\* e felic\*, spesso facciamo fatica a salutarci il sabato sera.
Restiamo a preparare conserve o marmellate, puliamo più del necessario
per stare insieme, ci appisoliamo l\* un\* sull\* altr\*, facciamo
tornei di calcetto, workshop di circo o di cosmetica, sbevazziamo.

## Domenica

Il sabato sera, il cibo che non è partito con il mercatino gratuito
viene recuperato da un'associazione che di domenica lo distribuisce in
una città vicina. Di quella frutta e verdura che la società destinava
alla spazzatura non viene buttato quasi niente.

Dal nostro canto, se succede qualcosa di domenica di solito si tratta di
una manifestazione, di un sit-in o di qualche altra attività militante.
A volte ci tratteniamo più del solito il sabato sera oppure ci
ritroviamo presto la domenica mattina per cucinare. Quando sappiamo che
da qualche parte si sta preparando un blocco rivoluzionario°, a volte
prepariamo la merenda. Dovresti vederl\* quando arrivano, si levano i
passamontagna e si rilassano dopo l'azione divorando chili di banane e
cioccolata recuperata, mica male. Dopo una manifestazione selvaggia che
era sfuggita di mano un po' a tutt\*, alcun\* compagn\* sono finit\* in
commissariato. Noi eravamo lì con il dessert. Siamo andat\* a Delémont a
distribuire tartine per sostenere la mensa (la mensa di Delémont è un
centro autogestito fantastico che, speriamo, esisterà ancora al momento
della lettura o della pubblicazione di questa raccolta).

Vorremmo essere lì anche per le prossime manifestazioni. E distribuire
cibo fino al crollo definitivo di tutti i sistemi di dominazione.
Speriamo di essere abbastanza.

Speriamo di essere sempre più numeros\*.

Speriamo che, un giorno, saremo ovunque.

## Ricette

### Pesto ai broccoli

Ingredienti: broccoli, aglio (tanto!), mandorle, pepe, sale e, perché
no, qualche fico secco.

Pulire bene i broccoli. Annusarli per assicurarsi che il liquido della
spazzatura non si sia infiltrato nelle cime.

Tagliare molto, molto grossolanamente i broccoli e metterli a bollire
nell'acqua salata.

Una cottura al dente è l'ideale. Quando sono pronti, mischiare gli
ingredienti in un grande recipiente finché la consistenza non vi
soddisfa. Condire con sale e pepe.

### Insalata cotta

Ricetta da fare con l'insalata vecchia e rovinata e il pangrattato fatto
col pane secco.

Ingredienti: insalata, olio d'oliva, aglio, cipolla, pepe, sale. Piccolo
extra

→ pangrattato.

Lavare bene. Annusare l'insalata per assicurarsi che il liquido della
spazzatura non si sia infiltrato tra le foglie.

Tagliare molto, molto grossolanamente l'insalata e lavarla
abbondantemente.

Sminuzzare l'aglio e tagliare la cipolla a rondelle, a listarelle, come
vi pare.

Soffriggere l'aglio e la cipolla, poi aggiungere l'insalata. Ci vogliono
delle pentole grandi: l'insalata sembra sufficiente per 300 persone ma
poi si restringe (come gli spinaci).

Aspettare che l'acqua di cottura evapori e aggiungere il pangrattato:
darà consistenza all'insalata e la asciugherà.

### Bomba di compost da lanciare sui piedi piatti

Il compost di ortiche o di lumache produce le bombe migliori. Puzzano da
morire.

