---
author: "Anonim*"
title: "Droni"
subtitle: "Grandezza e miseria del fuoco d'artificio antipolizia"
info: "Testo scritto per la raccolta svizzera romanda"
datepub: "Maggio 2020"
categories: ["polizia, repressione, amministrazione giudiziaria", "manifestazione, rivolta, protesta", "sabotaggio, azione diretta", "autoriflessione sulla militanza", "violenza, non violenza"]
tags: ["adrénaline", "amour, love, amoureuxse", "bienveillance", "bouche", "colère", "corps", "doute", "drone", "eau", "empathie", "feu", "force", "horde", "merde", "parole", "peur", "pouvoir", "rage", "rue", "rue", "théorie", "théâtre", "violence", "visage", "voix"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "1"
quote: "un modo di riprendere in mano la tua realtà sociale"
---

Una volta ho lanciato un fuoco d'artificio contro una schiera di sbirri.
È stato probabilmente uno dei momenti più intensamente politici della
mia vita.

È successo in Svizzera, durante una manifestazione. Camminavamo da quasi
un'ora e sinceramente il corteo non era niente di impressionante, non
preoccupava nessuno. Dalla massa ribollente di rabbia, però, si
staccavano dei gruppetti più mobili che si infiltravano nelle strade,
schivando i furgoni per distrarre le forze dell'ordine e conquistare
l'inconquistabile attaccando le squadre antisommossa o ridipingendo
l'arredo urbano (è così che i media chiamano le vetrine delle banche).

La polizia sfoggiava le uniformi nere, i cannoni ad acqua, i
*flash-ball*.[^1] Il messaggio era chiarissimo, come sempre: "siamo il
braccio armato, la milizia naturalmente legittimata alla violenza,
sempre coperta, sempre impunita. La strada ci appartiene, siamo i
guardiani dell'accettabile e se osate valicare i confini che abbiamo
assegnato ai vostri movimenti, se contestate la zona in cui vi
autorizziamo ad esercitare il potere, vi massacriamo di botte". Non
hanno un volto né un corpo riconoscibile. È proprio questo l'obiettivo
degli esperti di design che progettano le loro uniformi: nascondere
l'umanità di chi le indossa. Li rendono terrificanti, ma più che a
questo, le uniformi servono a renderli accettabili. Quando li
mostreranno in televisione mentre pestano e sparano a distanza
ravvicinata sull\* manifestanti, non sembreranno persone. Se lo
sembrassero, le immagini sarebbero molto più difficili da accettare,
lascerebbero un sapore più amaro in bocca. Si vedrebbero, nelle strade
svizzere, degli esseri umani aprire il fuoco su altri esseri umani.
Finché restano senza volto, senza corpo e senza nome, invece, vediamo
solo dei droni bipedi, una sorta di strumenti, tutt'al più un tipo
ideale di funzionari in azione sul campo, mai degli esseri senzienti.
L'interesse dei robot, oltre al fatto di essere telegenici, è che non
mettono in scena il libero arbitrio ma solo l'immagine di una società
freddamente organizzata che ci accontentiamo di accettare lascivamente
prima di cambiare canale.

È piuttosto difficile, quasi doloroso, ammettere che un misero fuoco
d'artificio senza conseguenze sia stato uno dei momenti più intensi
della mia vita politica. Lo dico sinceramente, perché non voglio
rinnegare questa sensazione, ma trovo comunque la cosa un po' deludente.
Il fatto che questo episodio sia stato così intenso non mi rallegra
affatto.

In primo luogo, soprattutto, perché ho fatto ricorso alla violenza. Il
mio fuoco d'artificio li ha appena toccati, ma avrei potuto ferire un
viso, squarciare una bocca, cavare un occhio, colpire la carne
dissimulata sotto l'uniforme di quei droni. Il petardo avrebbe potuto
andare a incastrarsi dietro a uno scudo antisommossa o, peggio ancora,
dietro a una visiera. È già capitato, qui in Svizzera, che la rabbia
politica di un individuo ne sfigurasse un altro, trasformando per sempre
la sua vita in un'esistenza realmente senza volto solo perché un giorno
quella persona aveva deciso di mettersi al servizio dello Stato. Ed è
una cosa tristissima. È tristissimo vivere in un mondo in cui la
violenza è una risposta legittima e necessaria alla violenza. A questo
siamo arrivat\* dopo circa tre millenni di teoria politica.

Sono deluso anche perché ci sono state tante altre esperienze politiche
che avrei potuto vivere molto più intensamente. Ho preso parte a
progetti collettivi incredibili, avuto la fortuna di partecipare a
cerchi di condivisione in cui le persone cercavano di prendersi cura
l'una dell'altra, collaborato a iniziative che mi hanno fatto
intravedere la possibilità di altre forme di organizzazione sociale,
fondate sull'accoglienza e sull'amore. Tutto questo per poi avere
finalmente la sensazione di esistere accendendo per qualche secondo un
petardo *Made in China* in una strada senza importanza. C'è chi si
indignerà parlando di "vandal\* sociopatic\*", di una gioventù senza
punti di riferimento che si sfoga saccheggiando vetrine, di una
generazione da "Arancia Meccanica", senza rendersi conto che l\* unic\*
ver\* nichilist\* di questa farsa sono loro. Chi dice queste cose non
capisce niente. L'essenziale delle nostre vite militanti è fatto di
amicizia, accoglienza, messa in discussione, riflessione, trasformazione
di sé e ricerca di un'armonia tra realizzazione personale e libertà
collettiva. Novantanove percento di amore, un percento di fuochi
d'artificio. Secondo motivo di tristezza: sogniamo un mondo senza fuochi
d'artificio, eppure lanciarne uno è ciò che mi ha gratificato di più.

Per finire, questa esperienza mi rattrista perché è impossibile negare
il rush di adrenalina, la soddisfazione morbosa del combattimento
urbano. Se ne parla molto raramente, tra chi prende parte agli scontri,
di cosa si prova a tuffarsi in questo tipo di euforia. La gioia della
mobilitazione collettiva è una cosa fondamentale, ma c'è un piccolo lato
oscuro di cui non amiamo parlare. A volte, dopo, viene da piangere:
affrontarsi per strada è doloroso, fa paura, anche quando è un teatro,
un simulacro di combattimento (come la maggior parte delle
manifestazioni in Svizzera). Anche se ne faremmo volentieri a meno, nel
mondo che verrà, conviviamo con questa violenza. È un dato di fatto, fa
parte della lotta. A volte ce ne vantiamo, c'è chi sviluppa persino una
sorta di dipendenza. Chi privilegia le maniere forti ne fa addirittura
l'elogio, e qui si sfiora l'esaltazione virile del fuoco d'artificio. A
livello sociologico, circa un terzo dei black bloc° è composto da
persone che non sono uomini cisgender° (mentre più di due terzi sono
universitari, ma questa è un'altra storia). Il problema di fondo si
situa a livello di immaginario. Abbiamo voglia di farci valere, di
mostrare che non siamo sottomess\* al loro dominio e incatenat\* alla
loro rappresentazione del mondo. Abbiamo voglia si annientare il loro
sentimento di impunità. Abbiamo voglia che la paura cambi campo. Allora
li affrontiamo, mostrando loro che se vogliono giocare al gioco della
violenza siamo pronti anche noi, che se il combattimento è l'unico
linguaggio che comprendono, lo parleremo anche noi. Vogliamo mostrare
che il loro potere non riposa sull'accoglienza, l'empatia o il
dibattito, ma su un'orda di droni sguinzagliata quando la farsa non può
più essere nascosta. E questo succede solo quando iniziano a volare i
fuochi d'artificio. Ma così cadiamo irrimediabilmente nella trappola
dell'adrenalina e del virilismo. La loro rete immaginaria ci cattura e
ci spinge a vivere la lotta riproducendo una relazione machista e
autoritaria con le nostre emozioni. È estremamente difficile esercitare
la violenza senza mobilitare un insieme di riferimenti affettivi e
intellettuali associati alla virilità e alla legge del più forte. Non
conosciamo altri modelli per capire, sperimentare e raccontare la lotta
alla quale ci costringono. E il risultato lo conosciamo: gare di piscio
tra militanti sul numero di detenzioni o di colpi scambiati con gli
sbirri. Se ti metti a raccontare che piangi dopo ogni manifestazione un
po' agitata, ci sarà sempre una voce pronta a sminuirti e farti sentire
una merda invocando valori guerrieri come coraggio e forza. E questa
voce, che aderisce (senz'altro inconsciamente) all'immaginario
dell'oppressione, con tutte le discriminazioni che comporta, questa voce
troppo virile non si rende conto di essere anch'essa diventata la voce
di un drone. Nonostante tutto, in ciò che ha reso il mio gesto così
intensamente politico c'è una punta di virilismo che faccio fatica ad
affrontare.

Ecco a che punto siamo, ecco le cause della tristezza, le radici di
quella fallimentare organizzazione dei sentimenti che ti porta a vivere
così intensamente un gesto tanto insignificante. Ma a prescindere dalla
tristezza, quali sono le cause di questa intensità? Senz'altro il fatto
che la sommossa è una forma di esperienza politica concreta alla quale
non puoi avere accesso in nessun'altra situazione della vita quotidiana,
per militante che sia. È un modo di riprendere in mano la tua realtà
sociale, un blocco di sensazioni che combina la possibilità di agire
come vuoi con le strade, i muri, tutto lo spazio che ti viene imposto, e
la sensazione di autodeterminarti collettivamente, seguendo la direzione
che credi e senza obbedire a nessun altro sentimento se non
l'appartenenza a un gruppo. Ti riconnetti con la consapevolezza di
essere parte di un insieme, ed è proprio questo che ne fa l'opposto del
nichilismo. Ciò la dice lunga sull'insieme dei processi che cercano di
annientare il nostro sentimento di esistere, sulla costruzione sociale
dell'angoscia, dell'alienazione più abietta e dell'odio di sé. Accendere
la miccia di un fuoco d'artificio rappresenta al contempo la vertigine
di trovarti di fronte alla vera libertà e una decisione che ti àncora
profondamente al suolo, che ti rimette i piedi per terra con la
sensazione di avere, per la prima volta, piena consapevolezza di chi
sei. Più che una legge, un ordine sociale o un sistema politico, stai
contestando quello che hanno fatto alla tua vita, quello che l'umanità
intera si è messa d'accordo per definire come "vita": una traiettoria
esistenziale che assume la forma di una merce vendibile, una somma di
gesti quotidiani che ti sono profondamente estranei, tutta una socialità
concepita come l'immagine di un mondo al quale collabori senza crederci,
senza riconoscerti, senza avere la sensazione di viverci.

Le mie sensazioni mi disturbano, ma non ci posso fare niente. Non
potrebbe essere altrimenti, considerando l'attuale configurazione delle
presunte società democratiche, la perdita di ogni presa sulla realtà, il
soffocare del sentimento critico e affettivo che dà il sentirsi padroni
della propria esistenza, la predominanza del grigiore e dell'assetto
securitario. Un giorno ho lanciato un insignificante fuoco d'artificio
contro una schiera di sbirri senza disturbarli minimamente. È stato
probabilmente uno dei momenti più intensamente politici della mia vita.
E lo rifarei senza esitazioni.

[^1]: I *flash-ball* sono dei fucili che sparano grossi proiettili di
    gomma in dotazione alla polizia francese \[NdT\].
