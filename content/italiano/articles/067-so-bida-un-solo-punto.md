---
author: "Collettivo Bida"
title: "Un solo punto all'ordine del giorno"
subtitle: "Un'assemblea onirica sui social network"
info: "Testo scritto per la raccolta bolognese"
datepub: "Maggio 2022"
categories: ["hacking, autodifesa digitale", "anticapitalismo", "autogestione, autorganizzazione"]
tags: []
langtxt: ["it"]
zone: "Bologna"
num: "67"
quote: "immaginare nuove interfacce"
---

Giovanni era appena entrato nello stanzone dove a breve si sarebbe
svolta l'assemblea. Non ricordava esattamente come fosse arrivato lì,
forse perché era molto, molto concentrato. Era mentalmente preparato, si
sarebbe parlato di social network, e voleva vender cara la pelle.

Aveva già sentito diversi compagni e compagne parlare di quelle
piattaforme che a lui facevano accapponare la pelle: Instagram,
Facebook, Twitter... No, non poteva essere vero. Anni e anni di
workshop° sembrava che non avessero avuto alcun effetto, forme e
strategie di comunicazione erano rimaste identiche.

Ma stavolta era riuscito a convincere diversi compagne dell'hacklab° a
venire per aiutarlo.

--- Bene, apriamo l'assemblea --- proclamò Nicola.

--- Come sapete, oggi abbiamo solo un punto all'ordine del giorno, ovvero
la nostra presenza sui social network, che facciamo?

Subito si alzarono diverse mani, Nicola iniziò a scrivere i nomi su un
foglio. Andrea, Maria, Nicola, Jenny, Nika, Caronte, Zazzo. «Pietà»,
pensò tra sé e sé, «stasera facciamo le 2».

--- Ok raga, parto io allora --- incominciò Andrea.

--- Tutte noi vogliamo raggiungere più persone possibili, credo che
questo sia un punto fondamentale della nostra attività di compagni.
Siamo nel 2022, la maggior parte delle persone fra i trentacinque e i
settanta sta su Facebook; i ventenni su Instagram, molti dei compagni si
informano attraverso Twitter. Questi sono strumenti di cui non possiamo
fare a meno. Senza di questi siamo tagliati fuori! Fuori! --- ripeté
Andrea, con gli occhi iniettati di sangue, consapevole già di discorsi
fatti in altre sedi, e pronto a nessuna mediazione.

--- Cos'è, la svolta machiavelliana? --- canzonò Jenny.

--- Il fine non può essere tutto, altrimenti perché non mandare i
comunicati stampa a «Il Tempo» o su altri quotidiani ancora più a
destra? E poi dimentichi la fascia tra i quindici e i venti, loro sono
su Tik Tok, che facciamo, le canzoncine di *Bella ciao* in perizoma?

Instagram, dici? Davvero? Ma che dobbiamo fare concorrenza a Diletta
Leotta e alle altre divette della radio--tivù?

A me sembra che qui tutte noi siamo anticapitaliste. Le aziende che
sviluppano le piattaforme che hai citato sono ai vertici della piramide
che la nostra attività politica vorrebbe combattere. Non possiamo
separare mezzi e fini, ce lo diciamo sempre! --- esclamò Jenny.

--- Iniziamo col botto! --- Cercò di sdrammatizzare Nicola. Poi rilanciò:

--- Ok Jenny, va bene, ci sta. Ma tu che proponi? --- Jenny non si tirava
mai indietro:

--- Semplice, una soluzione coerente. Un sito, e una fanzine° cartacea. E
tanti volantini.

--- Guarda che i Nirvana si sono sciolti da tempo. --- fece Andrea. Jenny
non era immune al dileggio gratuito.

--- Aho ma vaffanculo! --- Rispose piccata.

Dall'assemblea si levarono voci che esortavano alla calma.

Gli animi però si stavano scaldando: la lingua batte dove il dente duole
e pareva chiaro che continuava a dolere assai. Jenny era una delle più
critiche verso i social network. Fondatrice dell'hacklab. Grande
smanettona. Ma il suo odio verso i social network di qualsiasi tipo era
viscerale.

Dall'improvviso brusio che seguì il richiamo alla calma prese la parola
Nika, un'altra compagna dell'hacklab.

--- Raga, è ovvio che abbiamo posizioni molto diverse, ma dobbiamo
cercare di trovare una sintesi. Ma voi avete mai sentito parlare del
Fediverso°? Di Mastodon? Di Bida?

Mormorii...

--- Esiste da qualche anno una realtà tutta nuova sul web chiamata
Fediverso, ovvero un insieme di social network che comunicano tra loro.
--- fece una pausa. Aveva carpito l'attenzione dell'assemblea. Continuò,
consapevole di dover spiegare concetti non semplici.

--- Ok provo a spiegarmi, seguitemi anche se vi sembra complicato.
Conosciamo bene Facebook o Instagram. Come utenti andiamo su
facebook.com, quindi su quel sito, oppure su un altro sito tipo
instagram.com e così via. Allo stesso modo, potremmo senz'altro farci il
nostro sito centrosocialeanticap.net che sarà un vero e proprio social
network. Con persone che si iscriveranno, che scaricheranno una App, che
comunicheranno tra di loro e che a loro volta comunicheranno con altre
comunità come Bida.im, il social network bolognese, oppure con
nebbia.fail che è quello di Milano. Poi c'è Cisti di Torino e Puntarella
di Roma. Ne stanno nascendo tantissimi. --- dagli sguardi le sembrava che
la faccenda fosse chiara.

--- Bida.im? Ah ok, ora sono sul social network. --- disse Ciccio, che
aveva sempre uno smartphone a portata di mano e non si perdeva
un'occasione per navigarsela.

--- Sì, dategli un occhio. --- anche Nika non si faceva sfuggire le
occasioni per rinforzare il concetto. Dare i numeri non era fuori luogo,
pensava, perciò continuò:

--- Ci sono più di diecimila iscritti solo su Bida, e anche nelle altre
istanze ce ne sono parecchi.

--- Si vabbè! Ma qui siamo in un bolla con i soliti compa, non
raggiungiamo altri. So quattro gatti. --- esclamò Romoletto. Aveva
ascoltato attentamente, ma gli sembrava davvero un paragone senza senso:
il MegaFacebook vs. il MiniMastodon!

--- Non voglio fare un botta e risposta --- riprese Nika --- ma se guardi i
numeri di Facebook dopo tutti i vari ban delle pagine dei compagni si
viaggia ormai su numeri simili se non peggiori. Inoltre devi considerare
che dell'algoritmo di Facebook tu non sai niente. Non conosci realmente
i numeri, non sai chi raggiungerai davvero. Non puoi verificare se
effettivamente il tuo volantino è stato visualizzato dall'utente X. Ad
ogni modo ora mi taccio. --- concluse Nika.

--- È il mio turno? --- chiese Caronte, guardando Nicola.

--- Sì.

--- Bene raga, io sono stato su Bida, ora sono su Cisti dopo aver avuto
qualche allegro dibattito con gli admin che mi hanno fatto girare le
palle.

--- Motivo? Chiese Nicola.

--- Un dibattito sul femminismo che mi ha praticamente costretto a
insultare mezza istanza, e gli admin che ti vengono a chiedere di
eliminare i post. Non parliamo poi quando si discute di conflitto
israelo-palestinese, in quel caso è un attimo essere bollati come
antisemiti. Poi però ecco, certo, mi rendo conto che rispetto a Twitter
si sta molto meglio.

«Ci credo, ti comporti da misogino e pure stronzo. Se non cambi registro
è meglio che stai zitto», pensò Nika sentendo le parole di Caronte. Tra
i due non c'era mai stato molto feeling. Anzi, era chiaro che Nika non
lo sopportava proprio.

--- Però raga, se devo dare i miei due cent° --- continuò Caronte,
imperterrito --- Basta con Facebook, Instagram etc. Esiste finalmente
questa alternativa, che non sarà perfetta, ma per ora è il meglio che
possiamo avere. È autogestita°, ci sono compagn\* dietro. Se hai un
problema, hai persino qualcuno da mandare affanculo. Non è cosa da poco.
E poi tutti stanno uscendo dai social network. Anche i Wu Ming pensa un
po'.

--- La loro esperienza su Mastodon l'hanno classificata non diversamente
da quella di Twitter. --- Jenny non le mandava a dire, mai.

--- Vabbè, dai però! Si sono sorbiti tutti i compagni contro la loro
posizione sulla pandemia. Li sfottevano di continuo. --- ribatté Caronte.

--- Come vedi anche lì ci sono delle dinamiche tossiche e alla fine hanno
fatto bene ad andarsene.

--- Sì, per poi andare ad aprire un account su Telegram. Non mi sembra
una gran mossa di coerenza togliersi da una piattaforma di compagni, con
tutti i suoi difetti, per spostarsi su una piattaforma di gestione
senz'altro non autogestita.

--- Sì ma devi considerare che sono strumenti differenti, forse i Wu Ming
non avevano più voglia di interagire su strumenti che non controllano.
Il loro blog, se ci pensi, è molto attivo.

--- Possiamo evitare i botta e risposta? --- incalzò Nicola.

--- Passo la parola --- concesse Caronte.

--- Ok, mi sembra che abbiamo tanti elementi per ragionare. Provo a fare
una sintesi.

Zazzo saltò su: --- Guarda che c'ero anche io prenotato.

--- Sì vero --- confermò Nicola: --- Vai!

--- Per me, dobbiamo tenere un sito e riversare i contenuti su Mastodon.
Meno sbatta, si pubblica nel nostro posto, si diffonde dove la gestione
è fatta nel modo che ci piace. Stop, il resto è merda. Teniamoci fuori.
Fine.

--- Ok, sintetico come sempre.

--- Altri vogliono dire la loro? O proviamo a fare una sintesi?

--- Vai con la sintesi! --- consenso generalizzato.

--- Provo. Mi sembra che si sono espresse tutte le posizioni, si va da
chi vorrebbe stare principalmente sui social network commerciali a chi
non vorrebbe niente. Che ne dite se proviamo per un po' ad avere solo il
sito e Mastodon, vediamo se ci piace e magari, dato che abbiamo
l'hacklab, proviamo a pensare anche di fare un'istanza nostra. Poi se
non ci piace vediamo di trovare altro, o magari riscrivere il social
network. Immaginare nuove interfacce, nuove applicazioni, sperimentare
nuovi software. Giocare, guardare dentro la scatola e romperla. Fare
hacking°!!

--- SÌÌÌÌ!!! Dai! --- un coro unanime. L'unanimità: bei momenti! Sembrava
un sogno!

{{%ligneblanche%}}

Twaa, Twaa, Twaa, Twaa...

Twaa, Twaa, Twwa, Twaa

{{%ligneblanche%}}

Giovanni si stropicciava gli occhi dal sonno. «Che ore sono?», pensò
agguantando il cellulare. Lesse la notifica da Facebook dell'incontro
che aveva organizzato assieme agli altri alle 12. L'unanimità avrebbe
dovuto fargli capire che era proprio un sogno! Ma chissà che la realtà
non potesse ispirarsene...

--- %@\*\*\* pofforbacco, è tardissimo!

E scappò via verso lo spazio occupato.
