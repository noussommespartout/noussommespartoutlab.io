---
author: "Una militante del collettivo R con documenti svizzeri"
title: "Sabbia nell'ingranaggio"
subtitle: "Occupare, organizzarsi, informare, tenere duro, militare"
info: "Testo scritto per la raccolta svizzera romanda"
datepub: "Agosto 2020"
categories: ["spazi, occupazione, diritto all'abitare", "migrazioni", "polizia, repressione, amministrazione giudiziaria", "solidarietà, mutualismo"]
tags: ["Dublin", "adrénaline", "art, artiste", "coller", "fatigue", "foot", "force", "injustice", "internet, web", "justice", "machine", "permanence", "pouvoir", "solution", "théâtre", "violence"]
langtxt: ["fr", "en", "it"]
zone: "Suisse romande"
num: "7"
quote: "non resta che fare saltare tutto in aria"
---

Se racconto la mia esperienza usando il "noi", è perché sarebbe
pretenzioso dire "io" per raccontare una storia intessuta di azioni,
atmosfere e decisioni collettive di cui *sono stata solo* una piccola
parte. Questo testo è solo un punto di vista tra tanti altri su
un'esperienza di lotta *durata* oltre tre anni, che continua ancora in
altre forme.

## Occupare

È l'inizio del 2015, un anno che passerà alla storia per il numero
record di richieste di asilo in Europa. L'Italia e la Spagna non sono
in grado di occuparsi delle tante persone che arrivano via mare. Il
sistema di accoglienza è disfunzionale. L\* richiedenti asilo vivono in
condizioni indegne. La Svizzera si ostina a rimandare sistematicamente
ogni richiedente asilo nel primo paese europeo che ha attraversato. Come
consentito dal Regolamento di Dublino°, le decisioni negative sono
comunicate senza fornire motivazioni: molte persone ricevono decisioni
traumatiche relative a un' imminente espulsione verso l' Italia.

È sulla base di queste testimonianze che inizia una mobilitazione
intorno all'idea di un "rifugio", di un luogo dove nascondersi per
evitare l'espulsione e dell'occupazione di una chiesa. La chiesa è un
luogo dove la polizia non può entrare, almeno non senza fare polemiche.
Occupare una chiesa è una cosa che fa parlare: ci sono i sostegni, le
opposizioni veementi, i famosi "sì, ma..." che condannano queste
pratiche. Almeno se ne parla nella stampa, nella società civile e anche
all'università, dove in seguito c'è stata anche l'occupazione di un
auditorium. Questa grande visibilità permette di spingere l\* militanti
a unirsi al collettivo, organizzare manifestazioni, ecc. È stata anche
una leva per essere ascoltat\* e per avviare negoziati con le autorità,
anche se queste ultime hanno sempre fermamente condannato l'occupazione
e anche se i negoziati, nel complesso, non hanno mai avuto successo.

Questo porta anche le chiese, in quanto istituzioni, a prendere
posizione sull'inasprimento della politica di asilo e sulle espulsioni
effettuate in condizioni disumane. Fra denuncia e carità, non possono
più relegare la realtà fuori dalle loro mura se questa realtà entra a
casa loro senza invito. Le opinioni, i sostegni e le azioni della chiesa
sono eminentemente politiche e, in questo caso specifico, i legami erano
tanto più espliciti in quanto il Consigliere di Stato cantonale
responsabile della migrazione è un membro di un partito cattolico, il
Partito Popolare Democratico (PPD).

## Organizzarsi

Nel "rifugio" si organizzano partite di calcio, videogiochi, laboratori
teatrali, pranzi sociali e altre attività. Per giorni, settimane o mesi,
nella sala parrocchiale si sviluppa tutta una vita collettiva con e per
le persone che vivono "nascoste". Questo include anche il recupero degli
invenduti alimentari, la pulizia, la definizione delle regole di
convivenza e l'accesso alle cure. L\* militanti e le persone che hanno
uno status legale si alternano sul posto per fare da scudo in caso di
intervento della polizia, ma anche per creare legami sociali e fornire
una presenza. Le uscite dalla chiesa si fanno solo su accompagnamento,
per evitare l'arresto. Nonostante tutto ciò, le condizioni di vita sono
molto difficili per l\* residenti e prolungano un percorso di migrazione
già traumatico di per sé.

## Informarse

La mobilitazione porta molt\* destinatari\* di decisioni "Dublino"
negative a chiedere sostegno. C'è un servizio di consulenza settimanale
e, un poco alla volta, sviluppiamo delle competenze orali e collettive
sulle pratiche delle autorità. La realtà cambia rapidamente e le
informazioni e le azioni vanno costantemente aggiornate.

Durante i primi due anni, le autorità svizzere hanno un termine di sei
mesi dalla presentazione della domanda di asilo per espellere le
persone. Di conseguenza, a partire dalla risposta negativa e dalla
decisione di espulsione l\* richiedenti devono nascondersi per sei mesi,
in modo che questo termine sia superato e la procedura possa essere
riaperta. L'esperienza ci insegna che è assolutamente necessario
fornire un indirizzo di domicilio per ogni persona nascosta, altrimenti
viene amministrativamente considerata come "scomparsa", il conteggio dei
sei mesi si interrompe e la domanda di asilo viene, per così dire,
annullata. Aggiriamo questa regola comunicando sistematicamente
l'indirizzo del "rifugio", senza rischiare l'espulsione visto che la
polizia non entra nella chiesa. Al termine dei sei mesi, la procedura di
asilo può essere riaperta con una semplice lettera, dato che la persona
ha risieduto a un indirizzo noto alle autorità ma non è stata espulsa.
Abbiamo affinato sempre più questa strategia, che ha funzionato per
circa 300 persone nel Canton Vaud. Le loro domande di asilo vengono
esaminate in Svizzera e sfuggono alle espulsioni negli altri paesi
europei.

Con il tempo, la situazione si fa più difficile e il margine di manovra
si riduce notevolmente. Il giudice di pace inizia a emettere arresti
domiciliari, che costringono le persone a rimanere nei centri di
espulsione. Poi una proroga estende il termine per l'espulsione da sei
a diciotto mesi, costringendo le persone a nascondersi per un periodo
insostenibile (e senza risorse finanziarie). Infine, la legge viene
completamente riformata, vengono costruiti nuovi centri federali per
l'asilo, dai quali le persone non escono più.

## Tenere duro

Il crescente irrigidimento da parte delle autorità rende le azioni
sempre meno utili. Diventa difficile trovare un senso alla nostra
consulenza quando non riusciamo più a proporre nulla di concreto. L\*
migranti vengono da noi in attesa di una soluzione e noi continuiamo a
dire che non possiamo cambiare la loro situazione. Questo ci costringe a
mettere in discussione la nostra posizione. Scegliamo di dare valore
all'ascolto. Troppo spesso l\* richiedenti asilo dicono di non avere la
possibilità di esprimersi e che nessuno l\* ascolta. Né il personale che
l\* "controlla" e provvede alla sicurezza o all'alloggio, né chi
prende decisioni che l\* riguardano, né talvolta il personale sanitario
che si occupa dei loro problemi di salute. Spesso l\* richiedenti
nascondono la verità alla famiglia rimasta a casa per evitare di farla
preoccupare o per salvare la propria immagine.

E così abbiamo deciso di cambiare il ruolo delle consulenze. Rispondiamo
a domande sulle strategie valutate e scriviamo testi su situazioni che
ci indignano o sulla nostra impotenza. Soprattutto, ci definiamo come un
luogo di incontro. Per noi è importante creare legami con queste persone
in situazioni di vulnerabilità. Alcun\* di loro tornano regolarmente
rendendo l'atmosfera ancora più accogliente, ci riuniamo in piccoli
gruppi e abbiamo meno lavoro amministrativo da svolgere.

## Militare

Nei primi tempi, o quando si fanno azioni forti, c'è effervescenza:
tanta visibilità, tanto da fare, da gestire, sempre e ovunque. Ci sono
anche speranze, incontri e nuove possibilità che si presentano
all'immaginazione. Stimoli che fanno vibrare. La lotta dà senso alla
vita, profondità alla quotidianità e senso di appartenenza al gruppo.

È faticoso, ma in quei momenti la motivazione e lo stimolo prendono il
sopravvento. L'immediatezza dell'azione lascia poco spazio alla
stanchezza: c'è la sensazione che tutto si stia giocando in quel momento
preciso, una sensazione di urgenza e di grande importanza. Quando non ci
sono orari e con la commistione tra lavoro e socialità, i limiti sono
difficili da stabilire. Sono momenti magici, ma a volte anche dannosi.

Le azioni "forti" e i momenti di iperattività durano poco, al massimo
qualche mese. Lasciano il posto a fasi di transizione, momenti in cui i
membri del collettivo continuano a lavorare, ma senza molta visibilità.
Il riconoscimento sociale di questi lavori e incontri meno visibili si
esprime nei legami personali tramite un sorriso, una discussione
interessante o la nascita di una nuova amicizia.

Per il resto, si tratta di un lavoro a volte poco gratificante, senza
stipendio e con scarso valore sociale. Quando l'adrenalina o l'euforia
sono calate, bisogna andare avanti comunque. Riunioni interminabili,
eterni conflitti e alleanze interne, lotte di potere, le stesse
problematiche che spuntano fuori a ogni discussione. In queste
condizioni alcune persone si ritirano, specialmente chi è abituat\* a
una valorizzazione attiva del proprio lavoro, ad esempio attraverso il
riconoscimento pubblico sulla scena politica, o chi è meno abituat\* al
lavoro domestico gratuito e non complimentato.

## Epilogo

Non è facile decidere se fermarsi, se sciogliere o meno il collettivo,
se mantenere o meno determinati gruppi o attività. Il contesto attuale è
molto diverso da quello in cui il Collectif R ha iniziato a militare. I
centri federali d'asilo sono situati in aree geografiche isolate con
scarsi mezzi di trasporto pubblico. In questi centri non è possibile
ricevere visite, nemmeno da parte della stampa o delle associazioni (ad
eccezione dell'associazione incaricata dallo Stato per la
rappresentanza legale, e anche in questo caso a condizioni molto
rigide). I legami tra la società civile e l\* richiedenti asilo
ospitat\* nei centri federali sono quasi inesistenti. Quest\* ultim\*
possono essere espuls\* direttamente dai centri, senza aver mai messo
piede fuori da quelle ignobili scatole nere. La lotta è ancora e sempre
necessaria, anche se il sistema vuole renderla impraticabile.

La realtà dell'asilo sta diventando così dura che nessuno vuole più
affrontarla.

Non resta che fare saltare tutto in aria.

Forza, vieni !

## Altre azioni del Collectif R in breve

-   Le **mailing list**, tra cui una molto estesa (circa 2000 persone)
    che viene utilizzata più volte all'anno per trasmettere appelli a
    manifestare, testimonianze o osservazioni sull'evoluzione delle
    pratiche delle autorità in materia di espulsione e asilo.
-   **Adesivi** preparati alla fine delle sessioni informative per l\*
    richiedenti asilo. Ogni settimana, una frase breve, semplice e
    d'impatto tratta da una testimonianza del giorno viene stampata su
    carta adesiva, sempre con la stessa tipografia e la stessa identità
    visiva. Ogni persona parte con degli adesivi da incollare durante i
    suoi spostamenti e l'immagine viene anche pubblicata sul sito web.
    [www.desobeissons.ch](http://www.desobeissons.ch)
-   **Presenza** alle udienze del giudice di pace. Una delle pratiche di
    indurimento delle autorità è quella di mettere le persone agli
    arresti domiciliari in attesa della loro espulsione. Questo
    impedisce loro di "nascondersi". L'esito dell'udienza è già noto
    in anticipo, poiché l\* giudice pronuncia gli arresti domiciliari
    senza prendere in considerazione la situazione individuale della
    persona espulsa. Abbiamo partecipato come membri del pubblico,
    ponendo domande all\* giudice o reggendo cartelli in aula per poi
    dare una conferenza stampa fuori dall'edificio. Sebbene queste
    azioni non abbiano avuto un impatto concreto sulle decisioni legali,
    rendono visibile l'ingiustizia della procedura. Aumentano la
    consapevolezza che ogni anello, ogni ingranaggio di questa macchina
    che distrugge vite umane è composto da esseri umani che hanno un
    margine di manovra e che, se unissero le loro forze per usarlo,
    potrebbero cambiare la situazione.
-   **Azioni visive d'impatto**: durante le manifestazioni ci siamo
    incatenat\* l'un\* all'altr\* per denunciare l'uso di catene ai
    piedi sull\* rifugiat\*, una pratica illegale. Ci siamo legat\* alle
    sedie e imbavagliat\* per mostrare la violenza dei voli speciali,
    abbiamo montato una recinzione e del filo spinato in Place de la
    Riponne per simboleggiare la violenza alle frontiere e altre
    immagini di forte impatto simbolico.
-   **Cartoline inviate alle autorità**: invii di massa, con una
    selezione di messaggi comuni sul fronte e la possibilità di firmare
    o scrivere sul retro.
-   **Graffiti e poster**

