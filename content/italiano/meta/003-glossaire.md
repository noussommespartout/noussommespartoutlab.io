---
title: Glossario
meta: true
withoutindent: true
num: "0-3"
metatext: |
  Le parole che rimandano a una voce di glossario sono segnalate all'interno dei testi con il simbolo dei gradi (°).
---

<div class="glossary">

## ACAB, 1312

ACAB, acronimo di All Cops Are Bastards (Tutti gli sbirri sono bastardi), è uno slogan contro la polizia. L'acronimo è talvolta sostituito dal numero 1312, in riferimento alla posizione delle lettere A, C, A, B nell'alfabeto latino. Ci sono persone che sono state condannate per oltraggio a pubblico ufficiale per aver usato questa parola. Sono state proposte diverse varianti al fine di aggirare questa censura (All Cats Are Beautiful) oppure per includere altre lotte (All Colours Are Beautiful; All Clitoris Are Brave, ecc.).

## Azione diretta

Nell’uso militante, l’azione diretta indica il fatto di agire senza mediazioni per una trasformazione sociale, senza attendere e in opposizione ai canali tradizionali di rappresentanza politica e istituzionale, che costituiscono dunque delle azioni “mediate”, “indirette”. Questa pratica può avere un’accezione più o meno larga, a seconda dell’interpretazione che se ne fa: squat e occupazioni, disobbedienza civile, attacchinaggi, sabotaggi, gruppi operai e/o autogestiti, sportelli giuridici gratuiti, ecc. A seconda di come lo si usa, si può applicare solo alle azioni illegali e/o considerate violente.

## Adelfi e adelfia

Dal greco adelphòs (uterino), il concetto di adelfìa indica il legame fra persone nate dalla stessa madre o padre, permettendo così di racchiudere in una sola parola i significati di “sorellanza°” e “fratellanza”. Nell’ambito del femminismo queer di lingua francese, permette di indicare il forte senso di appartenenza, mutuo appoggio e alleanza evitando di ricorrere a concetti che ripropongono il binarismo maschio/femmina.

## Assemblea cittadina

L’assemblea cittadina è un concetto la cui valenza politica varia a seconda dei contesti. L’idea di fondo è creare un’istanza capace di dare voce a singole persone e di esercitare direttamente la propria sovranità politica, ad es. nella gestione delle risorse di un dato territorio o delle forme giuridiche. Le sue applicazioni spaziano ampiamente nello spettro politico: gruppi partecipativi senza potere decisionale creati dai governi o assemblee di cittadin* che si autorganizzano per rovesciare quegli stessi governi. Il termine è stato usato per indicare forme di autogestione operaia, battaglie giuridiche con l’obiettivo di instaurare meccanismi di democrazia diretta o ancora varie sistemi di governance (come l’olocrazia e la sociocrazia).

## Autogestione / Autorganizzazione

L’autogestione è una pratica politica attraverso la quale una struttura o un gruppo di persone si gestiscono senza l’intermediazione decisionale di un governo o di una gerarchia. L’autogestione è una modalità organizzativa che implica l’abolizione di qualsiasi forma di gerarchia, la ricerca del consenso come modalità decisionale, la collettivizzazione delle ricchezze, dei saperi, dei diritti e dei doveri. Nella teoria come nella pratica l’autogestione implica spesso il rifiuto, o quanto meno la limitazione, delle funzioni di rappresentanza all’interno di un’organizzazione.

## Black bloc (anche: blocco nero)

Il blocco nero è una forma d’organizzazione collettiva in contesti di manifestazione di piazza. L’allusione al colore deriva dai vestiti scelti da chi vi partecipa per cercare di essere il più possibilmente non identificabil*. Di solito, il blocco si distingue all’interno di un corteo pacifico per il suo modo di stare in piazza con pratiche più “violente” e determinate. È una scelta tattica che contesta il monopolio della violenza legittima detenuto dallo Stato e permette di rispondere alla repressione, ad esempio evitando le cariche e gli arresti. Nell’uso militante, termini come “blocco rivoluzionario” o “blocco radicale” stanno a formare degli omologhi, mentre esistono variazioni tematiche, come “green bloc°” (manifestazioni ecologiste), pink bloc (femministe/queer°) o addirittura clown bloc.

## Butch

Termine riferito a donna lesbica che ha stili e/o atteggiamenti considerati prettamente maschili.

## Cisgender / cisgenere / cisessualità

L’aggettivo cisgender è un neologismo per indicare un tipo di identità di genere in cui il senso di appartenenza di una persona a un determinato genere corrisponde a quello che le è stato assegnato alla nascita. La parola è costruita in contrapposizione a transgender. Quando si parla di uomini cisgender, si parla di persone che, quando sono nate, sono state assegnate al genere maschile e si identificano in questo stesso genere. In quanto posizione dominante nei sistemi patriarcali, gli uomini cisgender godono di privilegi rispetto alle altre identità di genere e alle persone transgender.

## Clown army (anche: clown bloc)

La clown army (letteralmente: armata di clown) o clown bloc (v. Black bloc°) è una modalità di presenza durante cortei e manifestazioni di piazza che si basa sull'utilizzo di tattiche di disturbo (sonoro, visivo, ecc.) o di abbassamento della tensione nel confronto con le forze di polizia. Il gruppo britannico Clandestine Insurgent Rebel Clown Army (Circa), nato all'interno dei movimenti no-global e altermondialisti, è senza dubbio il più noto a fare uso di questa tattica.

## Consenso d'azione

Con consenso d’azione si fa riferimento alla cornice delineata da chi organizza o prende parte a un’azione per definirne i confini teorici e pratici. Di esso possono far parte i piani di svolgimento dell’azione stessa, il comportamento da adottare al suo interno, il ricorso o meno ad azioni illegali/violente, il rischio giuridico che si assume chi vi partecipa o la strategia di difesa da assume pubblicamente. Il consenso d’azione serve a fornire un quadro all’interno del quale ogni militante è consapevole di ciò a cui va incontro, al fine di permetterne la preparazione in termini pratici, materiali e psicologici dell’azione stessa.

## Copwatch

Il copwatch consiste nel filmare gli agenti di polizia durante lo svolgimento delle loro mansioni - secondo una prospettiva militante. Si possono filmare le forze dell’ordine al fine di prevenire la loro violenza mostrandogli che sono osservati, al fine di creare un rapporto di forza con le autorità, per documentare le violenze della polizia a fini informativi e come arma legale, ecc.

## Dark room

Conosciuta anche come backroom, blackroom, or playroom, si tratta in genere di una stanza in un nightclub, in un cinema per adulti o in una sauna gay in cui i clienti intraprendono un’attività sessuale di qualche tipo in maniera anonima.

## Due cents (dare i propri)

Deriva da «my 2 cents», espressione idiomatica diffusa in ambito anglofono e usata per chiudere il proprio intervento in un’assemblea o una discussione per evitare la supponenza o ridimensionarne la portata di una proposta. Parafrasando, significa: «questo il mio umile contributo (alla discussione)» (concetto espresso a volte anche con l’acronimo IMO o IMHO: «In my (humble) opinion» equivalente all’italiano «a mio (modesto) parere»).

## Dublino (Regolamento di)

Si tratta del regolamento relativo al diritto di asilo politico all’interno dello "spazio di Dublino" (composto dai 27 Stati dell’UE, più la Svizzera, il Liechtenstein, la Norvegia e l’Islanda), la cui ultima versione è entrata in vigore nel 2013. Gli accordi sono criticati in particolare perché stabiliscono che “una domanda di asilo può essere esaminata solo una volta all’interno dello spazio di Dublino: nel paese di primo arrivo” (1. Rapport pour les droits et la mobilité des personnes noires africaines en Suisse et en Europe, collettivo Jean Dutoit.). Ciò consente a un Paese senza frontiere marittime, come la Svizzera, di rimpatriare facilmente tutt* l* richiedenti asilo nel Paese attraverso il quale sono entrat* in Europa. “Tra il 2009 e il 2014, la Svizzera è stata la vincitrice di Dublino: ha rimpatriato 19.517 richiedenti asilo, mentre gli altri Stati hanno mandato in Svizzera solamente 2523 richiedenti” (2. Duc-Quang Nguyen et Stefania Summermatter, « La Suisse défend l’accord de Dublin et ce n’est pas un hasard », [Swissinfo, 19.02.2016](www.swissinfo.ch/fre/politique/politique-de-l-asile_la-suisse-défend-l-accordde-dublin-et-ce-n-est-pas-un-hasard/41969008).

## Ecocidio / ecocidario

Termine che indica ogni atto di distruzione degli ecosistemi. Il suffisso -cidio, come nel termine genocidio, vuole connotare queste azioni come esplicitamente criminali e/o letali.

## Epistemicidio

v. Terricidio°

## Esame osseo per la determinazione dell'età

Risalente al 1959, l’esame osseo è un mezzo utilizzato dai tribunali e dalle autorità per determinare l’età di una persona – nello specifico per determinare se è o meno maggiorenne. Nel campo dell’asilo politico viene utilizzato per concedere o negare la protezione all* minori non accompagnat*. Tale esame si svolge tramite delle radiografie della mano e del polso che vengono poi confrontate con immagini simili contenute in un atlante di riferimento. La crescita ossea degli individui viene valutata in maniera soggettiva tramite il confronto di immagini, ovvero “a occhio” e senza una quantificazione precisa. Questo metodo è scientificamente molto contestato, in particolare perché non tiene conto dei fattori socioeconomici e nutrizionali, e il margine di errore dei test è di uno o due anni per l* adolescenti tra i 16 e i 18 anni. Tuttavia, è ancora ampiamente utilizzato dai tribunali e dalle amministrazioni che si occupano di questioni migratorie.

## Eteropatriarcale

Aggettivo derivato da “eteropatriarcato”, un sistema culturale e sociale nel quale il genere maschile cisgender° (che si identifica nel patriarca che domina sul resto della famiglia) e l’eterosessualità prevalgono sugli altri generi e sugli altri orientamenti sessuali. Il predominio è quindi dell’uomo sulla donna e degli eterosessuali sugli altri orientamenti.

## Fanzine (fanzina)

Contrazione di “fans magazine”, ovvero “rivista di/per appassionati”, è in genere una pubblicazione, periodica o meno, artigianale, auto-prodotta e senza fini commerciali.

## Fediverso

Dall’inglese “federation” e “universe” identifica un insieme di server indipendenti, i cui utenti sono in comunicazione tra loro (grazie alla struttura “federata”), utilizzati per la pubblicazione web e gestiti da persone e gruppi diversi in un’ottica decentralizzata, autonoma e indipendente.

## Fratella (-e) / Sorello (-i)

Gioco di parole che mira a invertire i generi agendo sulla desinenza e creando consapevolmente due nuove parole apparentemente “sbagliate”. In ambito femminista è spesso usato per designare le componenti di base delle alleanze di fratellanza e sorellanza. In altre lingue (come il francese) un concetto simile, ma ancor più slegato da una logica binaria, è reso con l’uso del termine “adelfo/adelfità°”.

## Frocia / finocchia

Da “frocio” e “finocchio”, termini utilizzati generalmente in maniera dispregiativa per definire uomini dai comportamenti considerati femminili, che è stato ampiamente oggetto di riappropriazione e fiera rivendicazione da parte dei movimenti LGBTQIA+°. In questo stesso senso sono usati anche al femminile.

## Gatekeeping

In ambito comunicativo, il termine inglese «gatekeeping» (lett.: mantenimento dei cancelli) fa riferimento al processo attraverso il quale determinati attori, governativi e non, decidono le modalità e i tempi per far arrivare determinate informazioni a un pubblico vasto. Si tratta quindi di una maniera di controllare e influenzare l'opinione pubblica.

## Genova 2001 (G8 di)

Dal 19 al 22 luglio 2001 si tenne a Genova il 27° summit dei governi del Gruppo degli 8 (abbreviato in G8), forum che raggruppava i principali paesi industrializzati del mondo. Sull’onda dei movimenti contro la globalizzazione nati a Seattle nel 1999, centinaia di migliaia di manifestanti confluirono nella città ligure per bloccare il vertice. Ne seguì quella che Amnesty International ha definito «la più grande sospensione dei diritti democratici in un paese occidentale dopo la Seconda guerra mondiale», obiettivo perseguito scientemente dalle forze dell’ordine con una violenza brutale e onnipresente. I fatti più gravi furono il pestaggio all’interno della scuola Diaz, le torture perpetrate nella caserma di Bolzaneto e, non ultimo, l’omicidio di Carlo Giuliani, ucciso il 20 luglio dalla pistola di un carabiniere.

## Green bloc (blocco verde)

Termine apparso all’interno delle lotte ecologiste per indicare la pratica di praticare azioni “violente” da parte di gruppi anonimi all’interno delle manifestazioni per il clima, sul modello della pratica del black bloc (che si rifà invece al movimento antiautoritario, anticapitalista e sindacale).

## Hacking / Hacklab

Da “hacking” – attività di qualsiasi persona curiosa, interessata alla tecnologia e al funzionamento delle macchine, digitali o analogiche secondo un’attitudine hands-on («metterci le mani sopra») – e “laboratory”. Ritrovo fisico o virtuale per hacker (a volte tradotto con “acar*”), laboratorio dove portare le proprie conoscenze, condividerle, acquisirne di nuove, sperimentare e mettere tutto ciò a servizio di una causa.

## Media di movimento

Termine generico usato per indicare dei media alternativi e politicizzati che diffondono informazioni diverse dai media mainstream e/o istituzionali. Il tipo di informazione proposta, che rivendica l’idea dell’impossibilità della neutralità, si rifà al concetto di contro-informazione, termine molto usato negli anni Settanta.

## Mixité choisie

La mixité choisie è un termine francese che non esiste in italiano. Questo termine si riferisce a una pratica che consiste nell’organizzare incontri riservati a persone appartenenti a uno o più gruppi sociali oppressi o discriminati, escludendo la partecipazione dei gruppi considerati come potenzialmente discriminanti (o oppressivi), al fine di non riprodurre modelli di dominazione sociale. Questa pratica è uno strumento militante utilizzato nei movimenti femministi, antirazzisti, LGBTQIA+ e movimenti in difesa delle minoranze di genere e dei disabili.

## Non binary / Persone non binarie

La non binarietà è un concetto usato per designare la categorizzazione di genere di persone che non si riconoscono nella norma binaria, ovvero che non si sentono né uomini né donne, ma che si riconoscono piuttosto fra questi due poli o nel loro mix o ancora in nessuno dei due poli. Questa categorizzazione si oppone alla binarietà uomo/donna, alla gerarchizzazione che ne deriva all'interno di un sistema patriarcale, nonché all'assegnazione dell'identità sessuale al momento della nascita della nascita.

## Orizzontale (orizzontalità)

Riferito a una struttura organizzativa, nella quale i rapporti al suo interno sono messi su un piano di parità, in opposizione ai rapporti gerarchici, che invece sono “verticali” (v. anche autogestione°, autorganizzazione°).


## Permessi di soggiorno per l\* rifugiat\*

Le persone che chiedono asilo in Svizzera ottengono uno status giuridico e diritti diversi a seconda dell’esito della procedura:- il permesso N per richiedenti asilo è una conferma che la persona ha presentato una domanda di asilo in Svizzera ed è in attesa di una decisione da parte della Segreteria di Stato per la Migrazione (SEM – Secrétariat d’État aux Migrations);- il permesso B per * rifugiat* riconosciut* viene concesso quando l* richiedente asilo ha spiegato in modo "plausibile e giustificato di essere perseguitato nel suo Paese d’origine ai sensi della Convenzione di Ginevra"1 e quindi l* viene concesso l’asilo;- il permesso F per l’ammissione provvisoria come rifugiat* viene concesso quando una persona ha i requisiti per ottenere lo status in base al diritto internazionale, ma la sua domanda di asilo è stata respinta dalla SEM in base alla legge svizzera sull’asilo. Da un punto di vista formale, la SEM ordina l’espulsione dalla Svizzera. Tuttavia, per ragioni di diritto internazionale, l’espulsione è illegale e la persona è quindi provvisoriamente accettata come rifugiata in Svizzera;- il permesso S è uno status giuridico introdotto per poter reagire in modo appropriato, rapido e pragmatico a situazioni di esodo di massa. Alle "persone bisognose di protezione" è concesso il diritto di soggiorno temporaneo in Svizzera. Questo status, introdotto nel 1999, è stato utilizzato per la prima volta solo nel 2022, per le persone in fuga dall’Ucraina;- se l* richiedente asilo non è soggett* a "persecuzioni rilevanti ai fini dell’asilo" nel suo Paese d’origine, la SEM ordina l’espulsione. Viene quindi rilasciato un certificato di ritardo della partenza.

## Precariato

Termine utilizzato per descrivere la "classe sociale" di persone che vivono in situazioni di precarietà. Si sostiene, a volte, che abbia sostituito il termine "proletariato" nelle democrazie occidentali contemporanee. In questa prospettiva il termine si riferisce a una realtà specifica: mentre la "classe media" cresce, emerge una classe precaria che non è più omogenea (in termini di posto di lavoro, condizioni di vita, ecc.) come lo era la classe "operaia". L’idea di precarietà cerca generalmente di designare un insieme di realtà molto diverse: student*, lavoratric* senza documenti, lavoratric* esternalizzat*, ecc.

## Queer

V. Transfemminismoqueer°.

## Sistemico

Termine derivato dagli approcci olistici/strutturalisti nelle scienze sociali che insiste sull’influenza delle strutture sociali, culturali e politiche sugli individui e le pratiche sociali, così come sulle istituzioni e le rappresentazioni sociali. In ambito militante è utilizzato spesso per svincolare una questione dal livello puramente individuale per metterne in luce le componenti fondamentali su una scala più larga (anche: sistemico, causato da un sistema e non da caratteristiche e comportamenti individuali).

## Sit-in

Il sit-in è un tipo di manifestazione nella quale chi vi partecipa si siede pacificamente per bloccare uno spazio, come una strada o un edificio. L'antica pratica indiana del Sit Dharna è per molti versi assimilabile al moderno sit-in.

## Socializzazione

In sociologia il termine socializzazione si riferisce ai processi attraverso i quali un individuo acquisisce e interiorizza le norme, i ruoli e i valori che strutturano la vita sociale, costruendo così la propria identità sociale e psicologica. La socializzazione avviene attraverso molteplici istanze tra cui l’istruzione o il contatto con i coetanei. Il concetto di socializzazione di genere permette di spiegare la trasmissione e l’interiorizzazione delle norme e dei codici sociali relativi alla mascolinità e alla femminilità, nonché lo sviluppo delle relative identità di genere.

## Sorellanza

Il termine sorellanza deriva da sorella e indica la solidarietà fra donne. Designa i legami di affinità e alleanza fra donne che si riconoscono affini e con un vissuto comune in quanto persone che hanno avuto un percorso di socializzazione femminile con tutto ciò che ne deriva a livello di status sociale. In francese, un equivalente non binario è il concetto di adelfìa.

## Sottoscrizione (prezzo di) / Offerta libera

Modo di definizione di un prezzo da parte di chi compra: il bene o il servizio scambiato, cioè, non ha un prezzo fissato da chi lo offre. È un modo per dare l’accesso a qualcosa in funzione dei mezzi di chi acquista e di uscire da una pura logica mercantile. A volte può accompagnarsi a un’offerta minima o consigliata. Fra le varie possibilità risultanti può essere contemplata la gratuità della merce o servizio.

## Suffragette

Con suffragette ci si riferisce a una serie di movimenti di rivendicazione del diritto di voto alle donne sorti all’inizio del Novecento nel Regno Unito. Il termine trae origine dalla Nationa Union of Woman’s Suffrage Societies (NUWSS), fondata nel 1897 da Millicent Fawcett in Inghilterra. Di stampo prettamente borghese e bianco, il movimento si è poi allargato ad altre classe sociali e rivendicazioni, mantenendo come denominatore comune la lotta per l’allargamento del suffragio alle donne. Due anime possono essere distinte al suo interno: la linea cosiddetta moderata e costituzionalista della NUWSS e quella a favore dell’azione diretta° rappresentata dalla Women’s Social and Political Union (WSPU) di Emmeline, Christabel e Sylvia Pankhurst.

## Terricidio / Terricida

Definito dal Movimiento de Mujeres Indígenas por el Buen Vivir come una sintesi di diverse forme di sterminio: genocidio (riferito ai popoli), ecocidio (all’ecosistema), epistemicidio (alla conoscenza) e femminicidio (alle donne), ovvero lo «sterminio sistematico di ogni forma di vita tangibile e spirituale».

## Transfemministaqueer

Queer è un termine che deriva dall’inglese “bizzarro”, “strano”, “curioso”. Originariamente utilizzato come insulto omofobo e transfobo, è stato oggetto di riappropriazione e risignificazione agli inizi degli anni Novanta. Utilizzato per persone che hanno un orientamento sessuale° non eterosessuale o si riconoscono in un genere diverso rispetto alla loro cisidentità°.Tranfemminista (da transfemminismo) identifica quella corrente di pensiero, all’interno del movimento femminista, che si è aperta alle soggettività LGBTQIA+°  per dare voce ad altri generi e ad altre sessualità che non siano il cisgender e l’eterosessualità.

## Workshop

In ambito militante si tratta di un incontro di persone che si ritrovano per imparare qualcosa di nuovo svolgendo collettivamente un lavoro pratico. Nella sua dimensione prettamente “laboratoriale” si struttura come un momento di condivisione orizzontale di saperi e competenze.

</div>
