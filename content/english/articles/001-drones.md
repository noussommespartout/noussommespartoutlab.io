---
author: "Anonymous"
title: "Drones"
subtitle: "The grandeur and misery of anti-police fireworks"
info: "Text written for the Swiss-French collection"
datepub: "May 2020"
categories: ["police, repression, justice", "demonstration, riot", "sabotage, direct action", "activist introspection", "violence, non violence"]
tags: ["adrénaline", "amour, love, amoureuxse", "bienveillance", "bouche", "colère", "corps", "doute", "drone", "eau", "empathie", "feu", "force", "horde", "merde", "parole", "peur", "pouvoir", "rage", "rue", "rue", "théorie", "théâtre", "violence", "visage", "voix"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "1"
quote: "To riot is to re-take control of your social reality"
---

I once shot a firework at a line of cops. It was probably one of the
most intensely political moments of my life.

It was in Switzerland, during a demonstration. We'd been walking for
about an hour and, to be honest, we weren't a very impressive
procession --- not likely to alarm anyone. But out of that seething mass
of anger came small, more mobile groups who rushed through the streets,
dodging police vans, distracting the troops, trying to seize the
uncatchable by attacking the riot police or redoing the paint on the
"urban furniture" (that's what the media calls bank windows).

The police had brought out the black uniforms, the water cannons, the
plastic bullets. The message was clear, it is always very clear: we are
the armed wing, the militia whose violence is by definition legitimate,
always covered up, always unpunished, and the streets are ours; we are
the guardians of what is acceptable, if you dare to cross the
"permitted" boundaries, if you challenge the area authorized for the
exercise of your power, we will blow you to bits. They have no face, no
recognizable body. The designers who give them their appearance create
their uniforms for this very purpose: to hide their humanity. Even when
they look terrifying, it's not really to scare people; it's to keep
them inside the acceptable. When we see them on TV, breaking limbs or
shooting demonstrators at point-blank range, they don't look human. If
they did, the images would be much harder to take; they would leave a
nastier taste in the viewers' mouths. On the streets of Switzerland, we
would see human beings opening fire on other human beings, at close
range. But without faces, bodies and names, we only see bipedal drones,
a kind of tool, possibly an ideal type of civil servant, never sentient
beings. The point of robots, apart from the fact that they look good on
TV, is that they don't call to mind free will; they manifest a cold
society that we are supposed to just accept lasciviously before flipping
to another channel.

It's difficult, almost a little painful, to speak of a little,
inconsequential fireworks display as one of the most intense moments of
my political life. I say this sincerely, because I don't want to deny
this feeling --- yet my own reaction disappoints me. I don't find it
particularly pleasant that I found it so intense.

First and foremost, it's because I used violence. My rocket barely hit
them, but I could have wounded a face, torn a mouth, poked out an eye,
hit some of the flesh that the drones hide under their uniforms. The
explosive charge could have gotten stuck behind a riot shield, or worse,
behind a visor. It's already happened here in Switzerland that one
person's political rage has disfigured another, made the rest of his
life truly faceless, and all because one day he decided to put himself
at the service of the state. It makes you want to cry. It makes you want
to cry to live in a world where violence is a legitimate and necessary
response to violence. Three millennia of political theory and here we
are.

Next, I'm disappointed in how this has all shaken out, because I've
had other moments that might easily have felt much more intensely
political. I've taken part in incredible collective projects,
participated in discussions where people work mutually on repairing
themselves, joined initiatives that made it clear that another social
organization, based on benevolence and love, is possible. All that to
finally have a true sense of existence come from lighting a Chinese
rocket in an unimportant backstreet. People will complain, they will
talk about "sociopathic thugs," shiftless youths who let off steam by
smashing shop windows, about the "Clockwork Orange" generation, all
without noticing that they themselves are the true nihilists. They
understand nothing. The essence of our activist lives consists of
friendship, benevolence, questioning, reflection, self-transformation,
and the search for a harmony between individual fulfilment and
collective freedom. Ninety-nine percent love, one percent fireworks. A
second disappointment: we dream of a world without fireworks, and here I
am, with a great sense of fulfillment because I set one off.

Finally, it makes me sad because you can't deny the adrenaline rush,
the morbid satisfaction of street fighting. We connaisseurs of rioting
rarely talk about it amongst ourselves --- what it feels like to throw
yourself into that particular joy. Of course it's basic, the joy of
collective action, but there's this little dark side we don't like to
talk about. Sometimes we cry afterwards, it's painful; it's scary to
face off in the streets, even when it's just theatre, a simulacrum of
combat (as it mostly is in Switzerland). Sometimes, even though we don't
want it in the future, we live with it in the present: that's how it
is, it's part of the struggle. Sometimes we brag about it, some become
addicted to it, there are even a few overdetermined he-men who praise
it --- and here we touch on the macho side of fireworks. Sociologically,
about a third of the people in the "black block" are not cisgendered°
males (and more than two thirds are university students, but that's
another topic). The basic problem is one of imagination. We want to make
an impression on them, to show that we are not prostrate in the face of
their domination and shackled to their representation of the world. We
want to destroy their feeling of impunity. We want fear to change sides.
So we confront them, we show them that if they want to play the violence
game, we'll play, too, that if combat is the only language they
understand, that's the one we'll speak. We want to show them that
their power is based not on benevolence, nor on empathy, nor on thought,
but on a swarm of drones they let loose when it turns out they can no
longer hide the masquerade; and it is only when we start shooting off
fireworks that they can no longer hide it. But in doing so, we
inevitably fall into a macho adrenaline trap. Their imaginary web snares
us and pushes us to live for the fight, reproducing a masculine and
authoritarian relationship to our emotions. It is very difficult to
commit violence without feeling yourself in contact with a set of
emotional and intellectual reference points linked to virility and the
law of might-makes-right. We don't have any other model for
understanding, experiencing, and recounting the struggle they force us
to engage in. And that takes us back to the old, familiar militant
pissing contests over the number of times we've been arrested or the
number of punches we've exchanged with the cops. If you let on that you
cry after each demonstration that gets even slightly edgy, there will
always be someone who responds by invoking warlike values like courage
and strength, who makes you feel like shit, who devalues you. And this
voice, with its unconscious adherence to the worldview of the oppressors
(with all the discrimination this implies), this male voice, doesn't
seem to get that it, too, has turned into the voice of a drone. However
that may be, I have to admit that among the qualities that made my
little action so intensely political to me is a little touch of this
maleness-- something I have some trouble facing.

So this is where it leads us, the source of the sadness, the root of the
whole messed-up emotional structure that makes such a stupid little
gesture so powerful. But leaving aside the sadness, what are the reasons
for the intensity? It is probably that rioting is a form of palpable
political experience that is not accessible anywhere else in one's
normal life, however militant it may be. To riot is to re-take control
of your social reality, to experience an emotion that begins with the
possibility of doing what you want with the streets, the walls, all the
socially determined spaces around you, and mixes it with an experience
of collective self-determination, of going the direction you want and
obeying nothing other than the feeling of being part of a group. You
reconnect with the feeling of being alive; that's why it's the
opposite of nihilism. And it says a lot about the process that kills the
feeling of existence in us, it says a lot about the social construction
of anguish, of alienation and self-hatred. Setting off a firework is
both the vertigo you feel when confronted with true freedom and a
decision that plants you firmly on the ground, puts your feet back on
the floor, and makes you feel as if you are walking the earth for the
first time in full awareness of who you are. You are challenging more
than a law, a social order, or a political system; you are challenging
what they have done to your life, what all of humanity has agreed to
define as 'life', an existential trajectory of commodities and
exchange, a collection of daily gestures that are profoundly foreign to
you, a socially agreed-upon world which you take part in without
believing or seeing yourself in it, without having the sensation of
actually being alive.

The feeling bothers me a little, but that's how it is. In the current
arrangement of our supposedly democratic societies, where we have no
purchase on reality, where the critical and emotional feeling of having
power over our own lives has been drowned, where everything is grey and
secure, that's how it is. I once shot a silly firework at a row of cops
who weren't even upset by it. It was probably one of the most intensely
political moments of my life. And I would do it again in a minute.

