---
author: "Anonymous"
title: "The Absurdity of Having to Prove Your Life"
subtitle: "The Difficulties in the struggle for asylum"
info: "Transcribed interview for the Swiss-French collection"
datepub: "September 2020"
categories: ["migration struggles", "anti-racism(s)", "police, repression, justice"]
tags: ["confiance", "dublin", "feu", "force", "foyer", "gueule", "impossible", "limite", "parole", "pouvoir", "prix", "rage", "théâtre"]
langtxt: ["fr", "it", "en"]
zone: "Suisse romande"
num: "50"
quote: "we're always tinkering around on the margins"
---

We are a collective committed to various asylum struggles. We try to
support people both inside and outside the official framework for
staying in Switzerland. In an institutional context, when you speak up
to defend people who are in difficult situations, everything matters.
You have to be both relevant and competent.

One of the things that helps us is having a network of privileged
contacts. Trusted doctors, for example, who give us information we can
use in the power struggle with the authorities. We also have informers
in institutions, in hostels, in government departments, in hospitals.
But not many; often they don't stay long. It's complicated, if not
impossible, to work in these institutions when you have convictions.

We're always tinkering around on the margins. We're always using the
small spaces the institutions leave us. To give a concrete example: last
year, a young woman of 16 arrived in Switzerland after having passed
through Italy. Her journey was painful; she was a victim of trafficking.
This is a typical "Dublinization" case: Switzerland refused to start
the asylum process because it maintained that Italy should have done it.
Moreover, the authorities had decided she was not a minor. To help her
stay in Switzerland, we worked with a Zurich centre specializing in
trafficking. A knowledgeable person took on her case and this allowed us
to "prove" that she had indeed been traumatized. It's absurd that
institutions put people under an obligation to "prove" their own
lives. It says a lot about the attitude of the authorities that they
leave the handling of these cases to standard-issue "managers".

The ORS, the company that runs the residential homes for asylum-seekers,
does not go to any trouble with this kind of case. They work with
psychologists on the basis of price. The medical report that we were
able to obtain "proved" that this was a "vulnerable" person. If you
manage to "prove" vulnerability in this way, then sometimes the
authorities are a little less brutal. In this case, however, the
authorities still insisted on testing her bones. They were trying to
"prove" that she was not a minor, even though the validity of this
kind of test is contested by the Swiss Society of Paediatricians, as
well as more broadly on the European level. The margin of error of bone
tests is about two years, so they mean nothing. In any case, they did
the test, and the result said she was between 16 and 18. After that, she
was finally recognized as a "vulnerable" person and could start the
normal asylum process. This allowed big changes in her daily life. Now
she is involved in theatre, sports; she is allowed to socialize. She has
been able to leave the hostel and live with a family.

When you are a militant in a collective working on asylum issues, you
are always in the middle, between supporting individuals and political
work. For us, it makes sense to work on both levels at once --- even if
it is a challenge, because supporting people who are not doing well is
always a priority. That leaves us little time, given our limited
resources, to think actively about more systemic actions.

And it's true that the things we've actually managed to improve are
more on an individual than a political one. We need to see that what we
are doing has meaning, we need to see that we can still achieve small
victories, even if it is not against the system as a whole. Despite
everything, several people have managed to obtain, if not a stable
permit, then at least a chance to enter the official asylum process or
receive provisional admission. Personally, I have been active in asylum
issues for about 30 years. I have been able to see how bad the situation
has become. Today, we are delighted if someone manages to obtain an N
(asylum-seeker) permit. Whereas before, we rejoiced when someone
received an actual refugee permit. It's shocking to think that most of
the time we fight for people just to be allowed to engage in the normal
asylum process and not even for them to be able to stay at the end of
the procedure, which would normally be the goal. Today, on the part of
the state, there is a blatant refusal to play ball. However, it is often
the grassroots activists who get the municipalities moving, the cantons
moving, the parliamentarians moving.

I would like to finish by saying that the problem with activism is
attrition. It is the weapon of the authorities. I like Coluche's phrase
"dictatorship means shut up, democracy means empty chatter". For me,
this is exactly the point. When we go to see the politicians, the
authorities, we arrive, we are ready, we have documents, reports,
figures; they welcome us, we discuss; the media talk about it. But in
the end, absolutely nothing happens. So we say to ourselves, we're
going to continue. They let us hold our demonstrations, they accept our
petitions. Then nothing. It's very difficult to stay attached to a
collective when you don't get an answer. On an individual level, we
have victories, we allow people to stay, and in humane circumstances.
It's important, but it's not enough. Although in spite of everything,
I have the impression that we are forcing the authorities to go beyond
their repressive and defensive discourse, you know, this hypocritical
discourse of Switzerland as a "humanitarian country", as a "welcome
land". I have the impression that without our pressure it would be even
worse. The situation of people involved in the asylum process, the way
they are mistreated by institutions, is invisible in the political,
media and even societal domains. Without the human dimension and the
individual victories, it would be very difficult for me to keep going.
I've seen live deportations, I've accompanied people to the police and
seen them taken away before my eyes. And that's it, there's nothing
more we can do. Sometimes we manage to get them back. It's not always
possible, unfortunately, but we make progress. You have to stay humble
to keep the fire of activism going. In order not to get too depressed,
you have to savour every bit of progress, especially at the political
level. We have to keep our rage, our pugnacity; we must never give up,
because that is the state's strategy: to make us give up.
