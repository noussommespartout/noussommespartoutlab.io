---
author: "Anonymous"
title: "Let's Stop \"Defending\""
subtitle: "Reflections from occupied zones"
info: "Text written for the Swiss-French collection, translated into English May 2022"
datepub: "December 2020"
categories: ["self-organization", "climate justice", "sabotage, direct action"]
tags: ["absrude, absurdité", "courage", "doute", "détruire, destruction", "fatigue", "force", "impossible", "logique", "machine", "permanence", "pouvoir", "sabot, saboter, sabotage"]
langtxt: ["fr", "en", "it"]
zone: "Suisse romande"
num: "28"
quote: "we could be the ones deciding when, where, and how to attack"
---

In our militant cultures, we have internalized the idea that our role is
to defend. Our vocabulary leaves no doubt: "Zone to Defend", "Protection
of nature", "Defense of the environment". This is the core logic of our
language and therefore of our thinking, of our action. And what's wrong
with that? After all, it's all about stopping the capitalist system
from taking us all along with it as it self destructs, isn't it? In
reality, it's not so clear. To me, our goal is more ambitious. We must
recover what the system has destroyed in order to build something new.
We must go on the offensive against this enormous machine and sabotage
its destructive potential. We must drop our defensive posture and go on
offense. By only defending, we are dooming ourselves to ineffectiveness.

## Living the defense

I have a lot of admiration for the courage and dedication of the people
who set up Zones to Defend (ZADs) and similar projects. I am also very
grateful for all I have experienced in these places, for the social
experiments carried out in them. The ZADs are good tools for
communication and consciousness-raising, laboratories of ideas and
practices. They show that other possibilities exist --- but they are a
limited defensive strategy that should not satisfy us.

It is an illusion to believe that, just by living, you defend. Living is
relaxing, defending is exhausting. I lived in a ZAD; I remember that the
company we were opposing came to pay us a visit. At such times you feel
contradictory things, because you are welcoming your enemy into your
living space, putting on a smile for form's sake, trying to look
correct. When you think about it, it's absurd to let our adversaries
walk around freely in our living space. It allows our enemy to know
where to find us --- which changes everything. They can check out our
organization, they can attack us whenever they want.

I have lived in squats, which are also Zones to Defend. I know what
it's like to receive an eviction notice, a piece of paper stating that
your house is no longer your home. That living there and giving the
place life have absolutely no value in the eyes of society, the police,
or the courts. And I know how it goes. Most of the time, we try to warn
other militants who live nearby, in the hope that they will mobilize and
that their reaction will be swift. Unfortunately, time and uncertainty
work against us. The police often intervene at five in the morning,
after a run of cold days, or during the holidays, to make sure they will
have to confront fewer people and that they will all be tired or
surprised. In most cases, no one comes to help. This is understandable.
First, I know that most of us can't take big legal risks to defend
houses we don't live in. But mostly, the police have all the power:
when you receive an eviction notice, you don't know if it's going to
happen in two hours, in two days, or in two months. From then on, you
are steeped in tension and anxiety. You can't stop thinking about how
the eviction will go, and about (often unrealistic) self-defense
scenarios. The moments of uncertainty follow one another, the
uncertainty becomes stress, then anxiety, then insomnia. Often, these
situations create conflicts and tensions in the collective. Everyone is
tired. How can you stay mobilized for so long and in such a state of
uncertainty without burning out?

At this point, I have decided to question our lack of effectiveness and
leave little room for my personal emotions. But don't think of me as an
insensitive person. I live with a knot in my stomach at the thought of
what mankind is destroying: this river surrounded by greenery, destined
to become a parking lot; or this living and mysterious forest, cut down
because it happens to sit on a lithium deposit. It is because these
defeats are as exhausting as they are demoralising that I question the
effectiveness of my struggle.

## Weaknesses of a defensive strategy

### loss of mobility

Defending a squat or protecting a ZAD makes it easy to find us,
compresses us into a small space. If the police or an enemy group wants
to attack us, they know where we are. To catch all the militants in an
area at the same time, there is nothing better than a raid on the
nearest ZAD. Infiltration becomes easier, too: confronted with a long
haircut and a few tattoos, militant vigilance quickly collapses, not to
mention that the life of a "defender" is too busy for them to always
be on their guard.

### loss of initiative and surprise

It is probably one of the most stressful points in an occupation: once
we are well established in a place, the enemy is free to choose the
ideal moment to attack. This is the benefit of having the initiative:
choosing the most favorable conditions for yourselves and the less
favorable for the defenders. Having the initiative also allows you to
plan your attack in advance, with chosen means and a determined plan.
Conversely, not having the initiative means waiting and reacting to the
opponent's plan, trying to anticipate it as best as you can.

### staying mobilized over time

Defending a position requires you to hold onto it physically, and for a
long time. It even becomes an indicator of success: how long did we hold
out? A month? Two years? But this leads to an unbalanced situation. The
"defenders" have to stay mobilized every day until they are evicted
(losing a considerable amount of time; prevented from concentrating on
other tasks). The police, on the other hand, only have to mobilise for a
day, just long enough to dislodge the militants. Due to this imbalance,
the police will always be able to attack in many places, since they hold
the balance of power and can decide when and where to act. We, on the
other hand, will have to choose which places and causes are worth
defending, necessarily abandoning others for lack of time, energy,
and/or people.

### making choices

This is also true at a higher level, such as a country. There are not
enough of us to defend everything that should be defended. Because
militants are limited in numbers, we have to choose our struggles: we
can't oppose every new land development, every new big, unnecessary
project. So we choose the most important ones, the ones that have the
greatest symbolic value, especially as these are the ones that are
likely to mobilize the greatest number of people. How do you choose
between this meadow about to be paved over and this forest about to be
cut down? I've been through impossible choices like these many times,
and they are particularly hard to live with emotionally. With a strategy
of pure defense, for each project we challenge, dozens of others go
forward with no worries, without being attacked. In Switzerland, we give
up 2,700 square metres *per hour* to housing construction and
paving-over. So many struggles that are not engaged in because we lack
of resources.

### absence of defeat is not the same as victory

The best we can hope for in defending a place is that it will not be
destroyed, or at least that the destruction will be less than the state
had intended. This is not a victory; it is an absence of defeat. As
Geoffroy de Lagasnerie writes: "We have resisted an attack, but we have
not launched an attack of our own. If we call this situation a
"victory", we participate in a kind of transmutation of values: we
assume the present order as desired (we are happy to have kept it),
which is in fact a kind of regression." For example, what about our
last victory (Notre-Dame-des-Landes)? Did we destroy any airports? Did
we reduce the number of planes landing in France? No. So what did we
win? Only that an airport was not built. Furthermore, Vinci, the
construction company, was compensated and alternative projects are
already underway, including the expansion of existing airports. Although
it saddens me deeply to say it, Notre-Dame-des-Landes is not a victory,
in fact it is barely an absence of defeat, despite costing us a lot of
time and energy.

### the environment strikes back

By only defending, we are reversing what should be the balance of power.
It is not us but the system that should be in a static position: its
most critical infrastructure (mines, refineries, transport and
communication networks) cannot be moved. If we were to attack, they
would be forced to play defense. We could choose our targets based on
our strengths and weaknesses. We could be the ones deciding when, where,
and how to attack, selecting whatever approach will be most unexpected.
It is the system that should have to choose which places to defend and
which to sacrifice. To me it's clear: we could be more effective, we
could win, we could get good news more often.

We need real victories, and these victories can only be achieved by
going on offense. To go on offense --- for example by organizing in small
groups to sabotage specific targets --- is to counter the flaws of a
defensive strategy. To start with, if we decide when to attack, it
becomes the problem of the destructive companies to worry, to spend time
and money on permanent defense, to never know when we will attack. Next,
we remain mobile: if we take necessary precautions, we can move the
conflict wherever we want, taking advantage of the element of surprise
to left-foot our adversaries. Furthermore, we make the offensive
choices, and it becomes the problem of the ecocidal° system to decide
what to defend and what to let go - at least, this will be true if we
manage to become really numerous. And attrition becomes much less of a
problem, since we can arrange to have all the time we need to recuperate
between actions. Each of us can resume the conflict when we are rested
and ready. Finally, it is we who have the initiative and can surprise at
will. We can choose the most favorable conditions for ourselves and the
less favorable for our opponents. They become the ones who have to wait,
to react, to "defend".
