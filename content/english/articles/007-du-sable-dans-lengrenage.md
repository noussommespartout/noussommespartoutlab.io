---
author: "A Collectif R activist who has Swiss papers"
title: "Sand in the Gears"
subtitle: "Occupy, organise, inform, hold, act: R's call"
info: "Text written for the Swiss-French collection"
datepub: "August 2020"
categories: ["squats, occupation, housing", "migration struggles", "police, repression, justice", "solidarity, mutual aid"]
tags: ["Dublin", "adrénaline", "art, artiste", "coller", "fatigue", "foot", "force", "injustice", "internet, web", "justice", "machine", "permanence", "pouvoir", "solution", "théâtre", "violence"]
langtxt: ["fr", "en", "it"]
zone: "Suisse romande"
num: "7"
quote: "the only thing left is to blow it all up"
---

If I tell my story using "we" and "us", it is because it would be
pretentious to use "I" in recounting a story woven from actions,
circumstances, and collective decisions of which I was only a tiny part.
This text is only one point of view among many on a more than three-year
experience of struggle, one which still continues in other forms.

## Occupy

It is the beginning of 2015, a year that will go down in history for the
record number of asylum applications filed in Europe. Italy and Spain
are unable to deal with the huge numbers of people arriving by sea; the
system receiving them is dysfunctional. Asylum seekers live in
deplorable conditions. Switzerland persists in systematically expelling
every asylum seeker to the first European country they entered. As
permitted by the Dublin regulations°, decisions to send people back are
made without offering any reason: many people receive traumatizing
orders sending them back to Italy immediately.

This situation mobilized us around the idea of a "refuge", a place to
hide to avoid deportation: the occupation of a church. A church is a
place where the police can't come in, at least not without controversy.
Occupying a church makes people talk: support, fervent opposition, "yes,
buts" that question less the goal than the manner in which it is being
pursued. Or in any case, it is talked about in the press, the civil
society, the universities (where an auditorium was also under
occupation). This high visibility helped mobilize activists to join the
collective, to organize demonstrations, etc. It was also a lever to be
heard by and to start negotiations with the authorities, even though
they have always strongly condemned the occupation and negotiations
have, on the whole, never been successful.

The occupation also forces churches, as institutions, to take a stand on
the tightening of asylum rules and on deportations under inhumane
conditions. Whether they simply denounce or actively help, they can no
longer leave reality outside their walls when it walks in of its own
accord. The church's opinions, support, and actions become eminently
political and, in this case, the links were all the more explicit as the
cantonal state councillor responsible for migration is a member of the
Christian Democratic Party (CDP).

## Getting organised

Football matches, video games, theatre workshops, free meals and other
activities take place at the "refuge". A whole common life is organized
with and around the people who live "hidden" in the parish hall for a
few days, weeks or months. This includes the recovery of unsold food,
cleaning, the definition of rules for living together, and access to
care. Militants and other people with legal status take turns in the
room, mainly to act as a buffer in case of police intervention, but also
to create a bond and provide a presence. People who leave the church
must be accompanied to avoid arrest. Despite this, living conditions
remain very difficult for the inhabitants, prolonging an already
traumatic migratory journey.

## Inform

The mobilization leads many persons who have received negative "Dublin"
decisions to come and ask for support. Weekly assistance meetings are
set up and, little by little, a form of oral and collective expertise on
the authorities’ practices grows. Reality changes quickly and
information and action must constantly adapt.

During the first two years, the Swiss authorities have a period of six
months from the time the asylum application is submitted to send people
back. This means that refugees have to hide for six months after a
deportation decision so that the deadline is exceeded and, thus, the
process can be restarted. Through experience we have learned that it is
absolutely necessary for each refugees to have a home address. Otherwise
they are considered as officially "disappeared", the six-month
countdown stops, and the asylum application is more or less cancelled.
We get around this rule by giving the address of the "refuge", which
carries minimal risk of expulsion since the police do not come into the
church. At the end of six-months, the asylum process can be restarted by
means of a simple letter, since the person has resided at an address
known to the authorities but has not been sent back. This strategy is
continuously refined and has worked for around 300 people in the canton
of Vaud. Their asylum applications were processed in Switzerland and
they avoided being sent back to another European country.

Later on, the situation gets harder, our capacity for action
considerably reduced. Judicial authorities begin mandating house arrest,
which forces people to stay in deportation centers. Then the
deportation's deadline is increased from six to eighteen months, forcing
people to go into hiding for an almost unsustainable length of time (and
always without financial resources). Finally, the law is completely
changed, new federal asylum centres are built, and people can no longer
leave them at all.

## Hold on

The ever-increasing severity of the authorities makes our actions less
and less helpful. It becomes difficult to see the meaning of our
presence at the weekly assistance meetings when we no longer have
anything concrete to offer. Refugees come to us for a solution and we
have to tell them that we have no power to change their situation. This
forces us to question what we are doing. We choose to prioritize
listening. All too often, asylum seekers say they don't have the
opportunity to express themselves. That we don't listen to them --- not
the employees who "supervise" them and look after their security and
housing, nor those who make decisions about them, nor sometimes even the
providers who look after their health problems. And often they hide the
truth from their relatives back home to avoid worry or to save face.

So the role of the assistance meetings is changed. We answer questions
about strategies they are considering and write texts about situations
that we find appaling or about our sense of powerlessness. Above all, we
define ourselves as a place of gathering and mutual aid. What is
important to us is to create links with people in vulnerable situations.
Some of them come back regularly and the atmosphere becomes warmer in
small groups with fewer administrative demands.

## Act

At the beginning, and each time we take strong action, there is a kind
of effervescence: great visibility, many things to do, to manage, all
the time, everywhere. A lot of hope, comings-together, worlds of
possibilities that open up to the imagination. Stimuli that make you
vibrate. Struggle gives life meaning, a sense of depth to daily life,
and a feeling of group belonging.

It is exhausting, but at such times, motivation and drive take over. The
urgency of our actions leaves little room for fatigue: we have the
feeling that everything is happening right now, a sense of immediacy and
great significance. Combined with the absence of schedules and
deadlines, this mixture of work and sociability means that boundaries
are hard to set. It's magic, but it can also run you down.

These big actions and times of hyperactivity don't last --- a few months
at most. They give way to phases of transition, moments when the members
of the collective are still working, but without much visibility. The
social reinforcement we receive from this activity begins to come from
individual contacts: a smile, an interesting discussion, a developing
friendship. Aside from that, it is a job that is rarely gratifying, with
no salary and little social recognition. The adrenaline and euphoria
have subsided, but we have to carry on. Endless meetings, eternal
conflicts and internal alliances, the same power struggles, unending
discussion of the same problems. Under these conditions, some people
withdraw. This includes those who are used to active celebration of
their work, for example through public recognition by other politically
engaged people. Or those who are less accustomed to unpaid, unrecognized
domestic work.

## Epilogue

Deciding whether to stop, to dissolve the collective or not, to maintain
certain groups or activities, is not easy. The current context is very
different from the one in which Collectif R began. The CFAs (federal
asylum centers) are located in isolated geographical areas with little
public transport. Visits to the centers are forbidden, including by
journalists and NGOs (except for the organization chosen by the state to
provide legal representation, and even then under very strict
conditions). Links are thus almost nonexistent between civil-society
actors and asylum seekers housed in these centers. The latter can be
sent back directly from the centers, without ever having set foot
outside these despicable black boxes. The struggle is still and always
necessary, even if the system tries to make it as hard as possible.

The realities of asylum are becoming so harsh that no one wants to put
their time and energy into it anymore.

The only thing left is to blow it all up.

Come on --- let's go!

## Other actions of Collectif R, in brief

-   **Mailing lists**, one of which is very large (about 2000 people)
    and is used several times a year to relay calls for demonstrations,
    testimonials, or observations on the evolution of the authorities’
    practices in relation to deportations and asylum.

-   **Stickers** written at the end of information sessions for asylum
    seekers. Each week, something short, simple and striking that was
    said that day is printed, with a consistent typography and style, on
    sticker paper. Each person leaves with stickers to put up along the
    course of their journey and an image of the sticker is also
    published on the website. www.desobeissons.ch

-   **A presence** at justice-of-the-peace hearings. One of the more
    troublesome constraints imposed by the authorities is placing people
    under house arrest pending deportation. This prevents them from
    "hiding". The outcome of these hearings is a foregone conclusion,
    as the judge imposes house arrest without taking into consideration
    the individual situation of the person concerned. We have attended
    as members of the public, asking questions of the judge or holding
    up signs in the courtroom, and have then held a press conference
    outside the building. While these actions did not have any concrete
    impact on the decision handed down, they do highlight the injustice
    of the procedure. They let people see that each link, each gear in
    the life-ruining machine that is the asylum system consists of human
    beings who have room to manoeuvre and who, if they joined forces to
    use it, could change the situation.

-   **The theater of images**: during demonstrations, we have put
    ourselves in chains to denounce the (illegal!) use of foot chains
    on refugees. We have tied ourselves to chairs and gagged ourselves
    to show the violence of deportation flights, laid a fence and barbed
    wire on the Place de la Riponne to symbolise the violence at the
    borders, and created other symbolically powerful images.

-   **Postcards to the authorities**: mass mailings, with a variety of
    common messages on the front and space to sign or write on the back.

-   **Tags and collages**.
