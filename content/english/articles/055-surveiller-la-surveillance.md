---
author: "Anonymous"
title: "Watching the Watchers"
subtitle: "Practical suggestions for starting a copwatching collective"
info: "Text written for the Swiss-French collection"
datepub: "April 2020"
categories: ["DIY, tutorial", "police, repression, justice", "self-organization", "self-defence"]
tags: ["arrestation", "caméra", "drone", "feu", "impossible", "justice", "logique", "mail, email", "procès", "stockage", "téléphone", "violence", "visage"]
langtxt: ["fr", "en", "it"]
zone: "Suisse romande"
num: "55"
quote: "film the police"
---

*This text was written more than a month before the release of the
images of George Floyd's murder and their global political
repercussions and almost seven months before the "Global Security" bill
in France which seeks to make it illegal to broadcast images of a
recognizable police officer.*

## What is the purpose of an organised copwatching group?

The idea of copwatching° arose in the USA as a part of popular
self-defence in the face of racist violence and police impunity,
particularly in the most at-risk neighbourhoods. It appears that the
first people to attempt to film the police in an organized manner were
groups affiliated with the Black Panthers, who sought to document this
violence with the analogue cameras of the time. Copwatching developed
organically, following the logic of a resistance that sought to document
the lived experience of these communities.

It has since evolved and become more organized. In some countries, and
not just in the West, groups are beginning to set up nightly patrols to
monitor police activity, ready to film in the event of violence. There
are also international networks and websites, including sites for
sharing images, tactics, and experiences, which help those trying to
organize decentralized copwatch groups. These networks also teach how to
mount a legal defense against the police, how to assemble witness
testimony, etc. At the beginning of the 21st century, inspired by a
number of popular organizations, groups began to form with the specific
intention of filming the repression of militants: documenting not just
everyday violence, but also political violence like the suppression of
demonstrations. It is on this anti-repression copwatching that we will
focus here.

The basic idea is simple: nobody watches those who watch us, so we need
"people's cameras." Neither the state nor its justice system can be
trusted to control their main organ of control: the police. That the
French police operate with impunity is obvious (just look at the data on
convictions by the Inspection Générale de la Police Nationale (IGPN) and
at the many recent denunciations of police brutality). Switzerland is no
better. In the last twenty years, the UN has twice condemned the Swiss
Confederation for having no independent oversight organ for its own
police. Simply put, if you want to fill a complaint about police
violence, you have to go and do it...to the police. In practice the
Swiss police, who are known to have murdered many people, function with
impunity. They are violent on a daily basis and often go beyond what
they are legally permitted to do. Copwatching can also be defined as
a form of proactive militancy that has the goal of documenting police
misconduct and demonstrating the need to rethink, disarm and/or abolish
the police in its current form.

Organised filming of police violence changes more than you would think.
The idea may seem simple, but having watchers present during moments of
physical struggle gives tangible results: more complete footage that is
easier to understand, and which can contribute to the public debate
and/or be used more effectively in the legal defence of activists. Very
often, in Switzerland, it is the images of copwatching groups that
appear in the press or are brought before the courts.

Of course, at demonstrations these days, everyone takes videos. But
it's often too late (we have the impulse to take out our phone when we
see violence, but by that point, poof, the violence has passed and we
haven't recorded anything). Furthermore, demonstrators' footage is often
blurry and incomprehensible. But if we mobilize a small group who pay
attention, concentrate, and know why they are there, the images become
more effective --- because we *anticipate*, we start filming before the
violence starts.

It's not very hard to organize a local copwatching group to support and
defend activists. You just have to be systematic and talk over what you
want to do with interested parties.

## Filming for whom?

It is important for a group to collectively agree on an ethic of
copwatching: who are we filming for? While it is possible to coordinate
with anti-repression groups or event organizers, it is essential that
copwatchers themselves have priority in the management of these images:
ideally, they should always have a say in their distribution. Copwatch
groups usually decide not to publish any images under their own names so
as to reduce visibility, but above all so as not to interfere with the
right of everyone to images.

## A suggested procedure

This approach is a kind of prototype, a list of things that can be
useful. (Obviously, you can't do everything at every event.) It is
something to be tried, questioned, and modified.

### Before the day of the event

It might be a good idea to walk around the area where things will unfold
and make a list of possible hiding places. The police know they are
being filmed and will often try, as early as they can, to organize the
area so as to make their violence invisible: by lining up police vans,
hiding one group of officers behind another, taking activists into
previously blocked off alleys, etc. Such techniques allow the police to
do whatever they want with the activists, then slip away in peace. They
often make use of areas that can easily be broken into.

The copwatching group should study the area: is there a spot with some
elevation where one could lie in wait (a café terrace)? Is there a
relatively safe place where one can see without being seen (filming
through a shop window)?

If there are several of us, we also need to think about the distribution
of the watchers --- it can be more complicated than you might think.
Spreading ourselves out makes it less likely that everyone will film in
the same place and ensures that there will be people to cover other
critical areas of an event. Of course, in the rush of the moment
everything changes, and you always have to observe, improvise, and be
ready to react. But anticipation and preparation can really help.

### Rounds on the day of the event

In the hours leading up to the event, it can be worthwhile to walk
through the space and see how the police presence is developing.
Specific findings can help you "read" police intent: barriers stacked
at the corners of certain streets, routes taken by unmarked cars,
collections of parked police vans. Be careful to keep a low profile
during these rounds. If you encounter uniformed or plainclothes cops,
you can discreetly try to listen in on their conversations, which
sometimes contain valuable information. It is then up to each group to
find the best way to communicate this information to militants and/or
organizations in real time, to allow them to try and protect themselves
as much as possible. For digital communications, always use end-to-end
encrypted applications (Signal, Telegram and Riot are good choices;
Signal deletes messages after a set time, which can be very useful).

### Organizing the copwatchers

There are many tactics, many choices...

Some groups of copwatchers choose to identify themselves by wearing an
armband or other insignia, as lawyers and members of the press often do.
This allows you to bring phone stabilizers, selfie sticks (useful to
film from above a crowd), cameras instead of smartphones, etc. You make
yourselves visible and behave like journalists. This choice entails a
lot of attentiveness, so as not to become an easy target.

Other groups prefer to copwatch discreetly, to blend in with the crowd
to avoid letting on that they are organized. If you are filming a black
bloc, stay masked and don't make it obvious what you are doing: only
film a bloc if you are sure that the images will be secure.

It is rarely useful for two copwatchers to be in the same place. Camera
angle can make a real difference during a trial: it's all about the
legibility of the images. However, security rules apply, as they do for
any event. We try not to lose sight of other copwatchers, and we
sometimes have to film, or do things to protect, each other.

## Equipment

### Some advice on equipment

-   A smartphone is a good compromise between effectiveness and discretion.
-   If you use a smartphone, it is best to use an alphanumeric password
    that is pretty long, for example: WeAreEverywhere\*1312\*.
-   Before you go out, check that you have enough available storage.
-   Delete all compromising content (unnecessary applications,
    unnecessary newsgroups, pictures, files etc.).
-   To spend a full day filming, you will need an external battery
    capable of recharging your phone or camera many times.
-   Ordinary requirements for any event (saline solution, food, first aid, plastic mask, etc.).

### Some advice on clothing

-   **Visibility**: identify yourself as a copwatcher with an armband, a
    vest, a distinctive insignia, and be very careful. By doing this you
    become a priority for the police, at risk of being targeted for
    pickup. Any encounters may be rough.
-   **Immersion**: dress like other protestors, try not to stand out too
    much, so you can't be easily identified.
-   **Invisibility:** dress like a curious onlooker who just stumbled on
    the demonstration.

### If you want to be as professional as possible

-   Manage images in real time on a stand-alone, encrypted cloud (all
    images are sent directly to it and deleted from the phone).
-   Have a support person with a computer and a microSD adapter to
    transfer the images.
-   Film with a drone (beware, this is usually illegal).
-   Get a press card and a big camera, if there are journalist-comrades
    in your country.
-   Fly a helicopter with a camera to counter the police helicopters (who knows!).

### Taking pictures

This is a moment when a very basic thing can make a real difference (as
a number of cases in Switzerland and elsewhere attest).

When filming violence: you must pay constant attention to the frame and
the brightness of the image. Try not to shake, to become distracted at
critical moments; get the clearest sound possible and don't chant
slogans yourself (yes, it's frustrating). Speak as little as possible;
don't worry about consent (you have to film faces and identifying
features clearly for the images to be legally useful - the question of
consent only comes up when the images are broadcasted).

We aim for wide shots, to understand everything that is going on and to
cover as many different encounters as possible. To make the most
comprehensible visual document, try to think ahead and film the overall
situation when you sense that "things are heating up."

*Make sure you can contact the people involved and collect videos taken
by others.* After you have filmed an altercation or an arrest, try
discreetly to obtain email addresses or phone numbers of the relevant
parties. If you don't manage to reach them in time, you can see if they
have friends at the demonstration who can provide contacts.

Most of the time, others have filmed the scene as well. Unfortunately,
these images are rarely collected and given to the people involved.
Moreover, they are sometimes posted on social networks without adequate
precautions (e.g. blurring of activists' faces). An organized
copwatching collective can also serve these functions: to dissuade
people from incriminating others in their insta-stories and to collect
images from those who were present. Be careful: ask for the images
without acknowledging that you belong to a collective (at protests, as
elsewhere, you never know with whom you may be speaking).

When things settle down, you can also ask eyewitnesses to tell what they
saw (on camera or with their faces covered). Don't ask too many
questions; present the witnesses with a summary statement and ask them
to validate it. This type of "on-the-spot" documentation has already
proven its usefulness in several trials.

## After the action or event

### Storage

-   It is important to store the images and not to delete them after
    sending them to the persons concerned. They can be used for a long
    time after the event, and activists you haven't even thought of may
    need them later. Protestors are often subject to lengthy and
    thorough investigations, resulting in multiple charges stemming from
    multiple events. It is best to keep everything, even when the images
    don't appear to include violence. You never know what might be
    needed in court.

Never store images online; use USB sticks and external hard drives
instead. To ensure that nothing gets lost, multiple copies of images can
be stored on diverse media.

### Editing

Do not underestimate the time involved.

Sending a single edited file compiling the essence of an event is often
a great help to an organization seeking to defend its protesters, or to
lawyers handling class action suits.

It may also be useful to include explanatory text panels in the edit, or
to highlight attacks outside the centre of the image/in the background,
for example with colored circles.

Blurring out all of the faces in an edit that may be more than an hour
long is a mammoth task and rarely necessary; it is best to leave it to
the organizations concerned, while making clear to them the importance
of blurring any relevant face before they release the video. (It may
nevertheless be a good idea to ensure, yourselves, that the copwatchers'
faces are blurred.)

Before sending images, consult digital self-defense guides that explain
how to send files securely.

Any copwatching group should also educate itself as thoroughly as
possible about the laws in effect in its country relating to images ---
in the public space in general and more specifically in connection with
the police (who are often covered by separate and even contradictory
laws).

