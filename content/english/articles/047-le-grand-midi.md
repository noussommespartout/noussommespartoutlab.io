---
author: "Anonymous"
title: "Le Grand Midi"
subtitle: "A week in the life of an autonomous canteen in Lausanne"
info: "Text written for the collection"
datepub: "July 2020"
categories: ["solidarity, mutual aid", "self-organization", "sabotage, direct action"]
tags: ["amende", "amour, love, amoureuxse", "arrestation", "autocritique", "banque", "barricade", "bouffe", "café", "coller", "colère", "compost, composter", "coulant", "cuisiner, cuisine", "doute", "eau", "erreur", "foot", "fourmi", "gueule", "huile", "huile", "jardin", "justice", "logique", "machine", "mail, email", "mercredi", "outil", "peur", "peur", "pizza", "prix", "réunion", "salade", "stockage", "sucre", "sueur", "visage", "voiture, bagnole"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "47"
quote: "this socialization of tidiness"
---

## Monday & Tuesday

In the canteen, we're meant to be off duty on Mondays and Tuesdays, but
that's not always how it goes. If something happens at the beginning of
the week, an unplanned delivery or some other little chaotic accident,
we end up with extra food in our storeroom. We get messages at all
hours: "Hey, I've got two kilos of squash, are you interested?" Most
of the time, we are interested: with about fifty people in constant
contact, there is always someone to go and get the two kilos and store
them in a cool place until Saturday. As time goes by, more and more food
comes in from different places: we refuse to join the official state
food recovery centers, so we have to put a premium on resourcefulness.
Because it's starting to become apparent that certain solidarities are
not on the right side of the barricades.

Once, the solidarity brigades in Geneva, which stand for anti-fascism
and self-defense, gave us absurd quantities of chocolate bars. They
arrived in big industrial boxes; we didn't know where they came from and
didn't want to know. We distributed them in the streets for weeks and
weeks. There are also anti-speciesist shelters, some have agreements
with local shops to collect their fruits or vegetables, often in good
shape. So they bring us what the non human animal in their shelters
don't eat. We've learned how little they like broccoli and citrus
fruit, because comrades bring us crates and crates of them. It's become
the hallmark of the canteen: pasta with broccoli pesto.

Above all, there are the unpredictable things, a school that can't do
anything with its stocks of chemical cereals or industrial jam, a local
farm that is about to throw away seven kilos of tomatoes, a comrade who
is moving away, a restaurant that is closing and giving away its kitchen
equipment, a local grocery store that supports us with donations of
coffee and spices, another grocery store further away but run by a
friend who believes in us and bends over backwards to help us collect
money or organic flour, friends who help us set up a big pot-and-pan
drive....

Once, someone invited us to pick up unsold goods from a movie theater.
We ended up traipsing across town with a grocery cart full of M&Ms and
Coke. We felt like we were wholesaling industrial sugar; we got so fed
up with this that, after talking about it, we decided just to drop the
plan altogether.

This random provisioning was hard to manage at first, we had to rush to
get three fridges and a freezer, print flyers and labels, call in
comrades to quickly electrify the space, jury-rig the ovens and fridges,
set things up so we didn't have to just lay everything out on tables.

### How I got here (1)

*I had only been back in Lausanne for a few months. I've always been
active, on a personal level, around issues of agriculture and
transforming how we eat, as well as with doing crafts, but I was doing
it more or less alone, which didn't make much sense to me. I couldn't
find an affinity group to get involved with.*

*In April, we had dinner with two friends in Lausanne. They are also
involved in various projects and activities. We talked about experiences
in collective kitchens and struggle canteens, about distributing
homemade bread and vegetables, and about the frustration of not having
participated in this kind of thing in Lausanne. On my side I started to
think about who might be interested in setting up a collective mixing
cooking and struggle.*

*Two days later, one of these friends received a text message from
someone he hadn't seen in years. An autonomous canteen had been
functioning for several weeks and they were looking for people to join
the project. What a coincidence... We went there the following
Saturday, to get information, have a look, and see what was going on.
From that point on, we've come every Saturday.*

## Wednesday

*On 25 July 1943, at the "casa Cervi" in Emilia-Romagna --- a place of
anti-fascist militancy --- they learned of Mussolini's arrest. Alcide
Cervi recounts the joy that was in everyone's heart that day in "I miei
sette figli." He tells how his son said to him, "Dad, today we're
making pasta for the whole region." And they brought big pots of pasta
to the village square to give out to everyone. 380 kg of pasta! It's a
nice story, a nice antifa legend: "La pastasciutta antifascista." We
stick labels on the bags of fresh pasta we give away, we give it a name:
"Pasta antifascista."*

Every Wednesday, we have a small fresh-pasta production line. A friend
has found a high-end machine that spits out six kilos of homemade pasta
per hour. Two or three people go there every week and each time it's
great. By hand, we take the warm pasta pouring out of the mouth of the
machine, cut it, and spread it out on drying racks. It is beautiful and
really amazingly good, everyone says so. We've been making the pasta
since the canteen first opened. And one Italian friend in particular has
found it very meaningful. The canteen first opened on April 25. In his
eyes, it commemorated the uprisings in Milan that led to the flight of
the fascist troops on April 25, 1945, which has since become
"Liberation Day" in Italy.

### How I got here (2)

*I'm originally from Italy and arrived in Switzerland somewhat by
chance. The Covid confinement was very strange for me, it was like time
was suspended. And so was I, between the relief of not being at my
house, in Italy, and the fear of being stuck in other people's houses,
here. All the more so as I still had the impression, despite having been
here for years, of being a sort of tourist, of not feeling at ease. I
knew that I wouldn't be able to accept that I had left my friends in
Italy until I found a way of immersing myself in the city where I am,
where I live.*

*Having been an activist singer in the past, I got involved in the
Lausanne Anarchist Choir. And it was on the Chorale's email list that I
first saw information about the autonomous canteen project: emergency
food distribution. And they needed help with food recovery as well as
any other plans or experience that could support the canteen in its
actions. I had no plans and no experience, but this was too good an
opportunity to become concretely involved in local struggles, to
collectively counteract the feeling of powerlessness imposed by the
pandemic - especially when I saw what was happening in Italy - and to
participate in the creation of a little more solidarity here, where I
live.*

## Thursday

Thursday is a big day for the canteen. Every Saturday, we plan the
Thursday evening collection rounds. It's usually done by two people in
a car. If you've never collected food in your area, here's a little
tutorial on how to do it well. First, you need to study the terrain,
locate the targets: the supermarkets. A first pass lets you identify the
accessible stores, those that leave their dumpsters open, that do not
grind up their food or soak it in bleach. Often, supermarkets in
outlying areas are preferable, if you have a car: the dumpsters at
supermarkets in the city center, which are more accessible on foot or by
bike, have already been picked over, and it's cool to leave them for
people who live in town and don't have a car. In Switzerland, the coop
and migros deserve a medal for security: their management of leftovers
is tight and rigorous. The dumpsters are usually stored in double-locked
warehouses, or even directly on the trucks that will take them to the
dump. I mean, sometimes you can get into the trucks, but the fines can
be stiff. If all you are going to get is some yogurts past their sell-by
dates or bananas in ripped packaging, it's not worth it.

Once you've settled on a list of stores, you locate the dumpsters, and
where to park so that license plates and faces won't be visible to
surveillance cameras. If you don't leave any traces, there's very
little reason for the stores to go over the surveillance videos. Yes, in
a capitalist regime, your garbage is also your private property, and
others taking it to eat is considered theft. Some stores actually don't
care very much and are quite open to the "theft" of this property
which, after all, is just a burden to them. The best thing is to draw up
a precise map, showing the location of the cameras and where to park; it
may seem too much, but when new people, who have never collected before,
are arriving regularly, it's a cool thing to do! To put everyone at
ease, prioritize friendly shops; that way, you don't have to dress all
"black bloc" just to go out and collect tomatoes.

When you go food collecting, don't forget to bring something to cover
your face if that will make you feel more comfortable (each person has
to manage their own risks), waterproof shoes, clothes you're not very
attached to, latex or even construction gloves, a headlamp, crates, and
plastic bags. After that, it's time to dumpster dive! To supply a
canteen, you have to collect as much as possible, and that sometimes
means digging to the very bottom of the bins. We've seen some of our
colleagues doing real "dumpster caving," turning over layers of plastic
or crushed jelly jars to find treasures. Some people have a hard time:
we're all conditioned to be disgusted by anything that is not ordered,
sanitized, standardized, that is penetrated, asymmetrical, fluid,
viscous. Once this "socialization of tidiness" has been broken down a
bit, salvaging becomes *the* Thursday activity, a treasure hunt we would
almost recommend to families. It's hard to express the collective,
triumphal joy of discovering dozens of perfect apricots that we can
rescue from a grim industrial fate, to make jam with them instead!

At the end of the run, we head out again, trying not to mind the smell
of garbage emanating from the twenty crates stashed in the trunk. We
arrive at the storage area, where the scheduled canteen comrades await
us. With music blasting, we clean everything we have collected with
water and baking soda and put it away. Sometimes, when we take a moment
to look at what we've collected in better light, we see that we've been
a bit over-optimistic. What looked, under a headlamp, like a fresh,
juicy fennel is actually half rotten. Too bad, it's going to the
compost, after all that was always its destiny, industrial capitalism
didn't want it, the hedgehogs and ants will eat it. They will poop on
the greens in the garden, which we hope to one day serve in the canteen.
The circle will be complete. What three stores throw away in a single
day feeds almost a hundred people every Saturday, while another twenty
or so pick things up at the free store.

### How I got here (3)

*I became a student-union activist a while ago. I felt out of sync in an
oppressive school environment and had a lot of anger in me. Joining the
union seemed like a tool that would allow me to transform this anger
into collective struggle --- something I obviously should invest in. So
much so that the following year I had an activist burn-out and started
questioning my political engagement, which was not giving me what I
wanted. Some comrades and I started dreaming of cooperatives that would
cultivate the land, make bread, cook food for people. We talked about it
a lot but nothing happened, we were too caught up in interpersonal
conflicts and in looking for new members. A big disappointment. I envied
the autonomous movement, with which my comrades in the union did not
identify at all. I felt that I needed a change, to meet new people, to
do new things, somewhere else. And that's when I saw the flyer for the
autonomous canteen.*

*I arrived the following Saturday carrying 5 kg of bread that I'd baked
at dawn. And what followed is a lot of love. I feel like I've met a big
family in which I feel good. We take our time, gently weaving firm
connections, and the collective projects multiply. There is an immense
pleasure in using your hands as a political weapon. And whoever you are
and whatever your background, in the canteen there is no sense of
permanent judgement, you can come and be yourself. It's a real work of
re-making for me, where food is no longer a source of isolated
suffering, but of collective, unified, and political joy. That does me
good.*

## Friday

We're also supposed to rest on Fridays, but it doesn't always work out
that way. Sometimes the previous day's collection was too small and we
have to go out again. But most of the time, if we do something on
Friday, it's because we want to plan for the long haul. Working
together every Saturday build bonds. Eating brings us closer, helps us
organize ourselves, and no one imagines that an autonomous canteen can
ignore the struggles around it. One project we are all engaged in is a
mobile canteen.

Of course, you need some cash to put that together. And we're not so
good on the cash front. Once, we busted our asses to organize a pizza
day; first we wanted to call it "Pizzacab", but, a little chagrined by
our lack of imagination, we opted for "Pizz@more" --- and it's better,
there's nothing more revolutionary than love. We organized ourselves to
get ovens, dough, sauces. The cops looked at our huge ovens and asked:
"Who do these hot, rickety things belong to?" The cooks answered: "I
don't know, you can move them if you have a truck, in the meantime
we're going to cook some pizzas". They left. A good hundred people
came to eat, and we distributed thirty or so calzones in the streets.

It was the tenth canteen, a great moment. On the other hand, of all the
canteen members present, not one thought of putting up a contribution
box. So we didn't really raise any money.

During the big anti-sexist demonstrations in June, we hauled a modified
shopping cart around the march and made some money selling our homemade,
anti-fascist --- purple! --- pasta. Once, we put out cauldrons of pasta
for Walpurgisnacht. Another time, we set up an improvised pizzeria on
the Promenade de la Solitude with a group of people organizing against
anti-militant repression. We called it "No to judicial oppression, yes
to pizza expression!" There, we were able to make some money.

Another time, it was falafel day, and we couldn't think of a pun any
lamer than "antifa-lafel". We cook for the anti-repression and legal
aid funds, to help pay the fines of people occupying abandoned houses,
who join revolutionary blocs, who are at risk of being expelled from the
country or from their houses, who don't have health insurance. Of
course, it pisses the State off that we cook for free, that we don't
need them, don't need their agreement to exist, to supply ourselves, to
set ourselves up in public parks, not following any norms or rules other
than our own. The administration tries to get under our skin however it
can, but for the moment we're not complaining. Once, our group parked a
car on the wrong side of the road for a few minutes, the time it took to
collect three crates of vegetables to distribute the next day to people
who had lost their jobs because of Covid. The cops hanging around seized
the opportunity, half of one of our tires was sticking out beyond the
white line, they fined us 200 francs.

Our mobile canteen, we would like it to be able to react very quickly,
to --- on the spur of the moment --- be at occupations, demonstrations,
places where people are being arrested, in front of police stations
where comrades are being detained. The more there is to eat, the more
people stay. With a vegan pancake stand and two loudspeakers, you can
double the number of people who come out to show support, and triple the
time they stick around.

### How I got here (4)

*Even though I participated in demonstrations, attended meetings, I
never really got involved. I voted for a while, then I stopped, then I
cast blank ballots, but in the end I was never really involved. I never
found, in Switzerland, a network in which I could feel comfortable, so I
put my energy elsewhere, all the time asking myself where the "others"
were, the ones with whom I could feel at ease.*

*During the Covid confinement I felt the need to get back to a concrete
reality so I could act, rather than watching from far away and thinking
alone. I felt angry, sensitive to all these treacherous dominations, so
dreadfully ordinary. Moreover, lately I had started to think about
anarchism, and it seemed to me to have an obvious logic. And while these
thoughts were going through my head, I saw something about the
autonomous canteen.*

*I went there, just like that, on a Saturday morning at nine o'clock. I
had never thought about carrying on the struggle via food, but in
retrospect it seems like a great and fundamental idea that I discovered
while cooking and eating with the whole canteen team. Of course, even if
it makes sense, nothing is about to change except the faces of the fools
on the banknotes. We keep going nonetheless.*

## Saturday

On Saturday, in place of a Big Night, it's the Big Noon. How many
people come to cook at the canteen? This is information that would no
doubt be interesting to the fluorescent blue clowns with a monopoly on
legitimate violence, so we'll say between one and a thousand. This
heterogeneous assembly of highly mobile individuals meets in the
morning, drinks a coffee and starts to put out the boxes of food brought
back on Thursday. We make a kind of pile, a pyramid of green boxes with
our homemade pasta, fruit, vegetables, packaged goods. Collectively, we
draw up a menu, with a thought for cooking whatever is most damaged, so
that whatever is best preserved can be distributed at the free store. A
short discussion suffices: we have to choose a contact person in case of
a police visit, a person who will keep an eye on the free store, a group
of people ready to distribute in the streets, a person to go on a
possible backup shopping trip (there are basic ingredients that are hard
to collect, especially salt, sugar and oil).

Then it starts to happen. Of course, we could make schedules, teams,
managers, distribute the tasks among the stoves, the cutting boards, the
raw and the cooked. But everything falls into place organically. You're
in the mood: go to the stoves. You want to chat: go cut vegetables at
the big table. You want some peace and quiet: go wash the fridges and
the storerooms. You want to be outdoors: go set up the tables or empty
the compost. You don't want to do anything: go play foosball, make
coffee for everyone, paint your friends' nails. After one or two hours,
we start setting up the canteen --- outside if the weather is good,
inside if it's bad. On one side of the building, there is the hot food
that will be served right there, with enough chairs and tables to
accommodate 30 to 40 people. On the other side, there is the free store,
a free or free pricing° shop open to everyone, which offers ingredients
to take home and cook yourself. The idea for the free store came later.
We realized that a community solidarity initiative should have several
faces: there are people who want or need to be served hot meals, because
they don't have the means to cook them themselves, or feel like a chat
in the open air. There are people who want or need to cook for
themselves or their friends and relatives --- they are the ones who come
to the free store. And then, we don't all like the same things, we
don't all cook in the same way, we don't all flavor our meals with the
same spices, we are not all allergic or intolerant to the same foods.

Sometimes the regular customers at the free store go home, sometimes
they stop for lunch in the park. We always invite everyone to come in
the morning the following Saturday, to participate in the cooking, in
making the menu. People often come back. In the canteen meetings there
is a shared desire to create a neighborhood anti-market space, and
therefore to work to create a real social mix while maintaining the
self-critical perspective necessary in any attempt to avoid reproducing
oppressive thinking. This doesn't mean that we always succeed; it means
that we're trying. Of course, it's a militant place, socialization and
affinities sometimes push us toward private encounters, but it's
something we're always trying to question.

Then, as noon gets closer, the hot food is ready: pasta, tortillas,
mixed salads, couscous, sautéed vegetables, fruit salads, smoothies,
etc. Some of the canteen comrades may even bring truffled olive oil,
morels, or other expensive stolen stuff. Things that come from
beyond....

Whatever the menu, before serving at the canteen, we make our rounds.
Between 30 and 40 sacks are given out in the streets, with a complete
hot meal, utensils, a fruit salad. These sacks are for those who don't
feel like moving or socializing. The next few hours are the best: we all
eat together. The news has spread through activist networks and more
formal organizations, and even through certain state social services.

Once, a regular at the free store brought us several trays of a dish she
had cooked at home.

Once, a free-store regular 'paid' in weed.

Once, a freelance dressmaker came to eat and brought us a stock of
hygienic masks that she had sewn for us by hand. Good, masks and hand
sanitizer are expensive.

Once, an internationally renowned anarchist showed up. We weren't sure
if maybe we should all stand when she came in. She said, "Here, I
brought you some tupperware. Is there any pasta?" She sat down on the
grass.

Once, a volunteer from another organization came and took all our fruit
to make jams and syrups that we put in the free store the following
Saturday.

Once, an activist told us at the table how she knew Evo Morales when he
was just a young indigenous activist before he became the Bolivian
president who stood up to the US.

Once we noticed that someone had taken money from the cash register of
the free store and we had a long debate about whether the cash register
was also self-service. Offering free-pricing --- a good concept!

Once someone told us that the local shelters were short of hygiene
products, so we organized a detergent-making workshop.

Once, someone set themselves down in the park and organized a
spontaneous yard sale.

Once, some friends from a squat brought us 200 500-gram chocolate
rabbits. 100 kilos of chocolate. We gave them away, ate them, melted
them down, moved them from one storeroom to another for weeks.

Once (several times, in fact), neighbors who were moving out brought all
the food that was left in their cupboards.

Once, an Italian comrade explained to us that we had to cook all the
salads we had collected in a pan with garlic. Of course, most of us made
fun of him, but afterwards, most of us tasted it, too. Since then,
we've been making it almost every week.

Once, a canteen friend mistakenly put the beer we had collected and were
saving for the afternoon meeting in the free store. If we believed in
punitive justice, he would have had a bad day. But fuck punitive
justice, we held the meeting and drank fruit juice. And since then, we
don't drink alcohol during our meetings.

Once (several times in fact), the cops parked near the canteen. They do
this to scare people who come to eat. The police want the word to
spread, so that people become uncertain whether or not the canteen is a
safe space for those who can't risk an identity check. A pure strategy
of fear and division. The more the masses eat together, the more they
discuss, the more they organize, and that's not to everyone's benefit.
When it's hot and we eat outside, we can't do much about surveillance.
When it's cold and we eat indoors, we're more at ease.

Every time, after putting the equipment away, we get together: it's the
canteen meeting. We debrief, we plan the next week, we count the money,
we propose future projects.

Every time, the people who are able to be there *become* the canteen and
pursue its constant, simple, and collective improvement.

Often, on Saturday evening, sweaty and happy, we find it hard to leave
each other. We stay to make preserves and jams, we wash more than we
have to in order to stay together, we take naps on top of each other, we
play foosball, we do circus or makeup workshops, we have a drink.

## Sunday

On Saturday evening, the food that is left at the free store is taken by
an organization that distributes it in the neighboring town on Sunday.
We almost never throw away any of the fruits and vegetables that society
has rejected.

As for us, if something happens on a Sunday, it's often a
demonstration, a blockade, or some other militant activity. Sometimes we
stay late on Saturday night or meet early on Sunday morning to prepare
food. When we know that a revolutionary bloc is mobilizing somewhere, we
sometimes make them a snack. Seeing a bloc come and unwind, relax after
an action by devouring kilos of recovered bananas with recovered
chocolate, that's something. And when, after a wild demonstration which
has overwhelmed everyone, some comrades have ended up arrested, we are
there to serve dessert. We went to Delémont to serve sandwiches to
support the canteen (the Delémont canteen is a wonderful autonomous
center which we hope will still exist when this collection is read or
published). And we hope to be at future demonstrations. We'd like to
serve food until the final collapse of all systems of domination. We
hope there will be enough of us.

We hope that there will always be more of us.

We hope that one day, we will be everywhere.

## The Recipes

### Broccoli pesto

Ingredients: broccoli, garlic (lots of it!), almonds, pepper, salt and,
why not, a few dried figs.

Wash the broccoli well. Smell the broccoli, check that no garbage juice
has crept into the florets.

Cut the broccoli very, very, very roughly and boil it in salted water.
Al dente is best.

When ready, put all the ingredients in a large bowl and blend until you
like the consistency. Season with salt and pepper!

### Cooked salad

To be made with old, damaged greens and breadcrumbs from dry bread.

Ingredients: greens, olive oil, garlic, onion, pepper, salt, breadcrumbs
(if you have them).

Wash well. Smell the lettuce, check that no garbage juice has settled in
between the leaves. Cut the salad very, very, very roughly and wash
thoroughly.

Chop the garlic and cut the onion into strips, rings, julienne, whatever
you like.

Sauté the garlic and onion and add the greens. You need large
containers, it will seem like there is enough to feed 300 but then it
reduces (like spinach).

Let the liquid from the greens reduce a little, then add the breadcrumbs
at the end. This will give a good consistency and dry the greens out a
bit.

### Compost bomb to throw at the cops

Mashed nettles or slugs make the best bombs. Peeeee-yew!
