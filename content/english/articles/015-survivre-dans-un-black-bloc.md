---
author: "Anonymous"
title: "Surviving in a Black Bloc"
subtitle: "Theory, arguments, tear gas, repression"
info: "Text written for Swiss-French the collection"
datepub: "February 2021"
categories: ["police, repression, justice", "DIY, tutorial", "demonstration, riot", "violence, non violence", "self-defense", "sabotage, direct action"]
tags: ["bouche", "café", "colère", "corps", "câlin", "eau", "feu", "France", "liberté", "limite", "lunettes", "parole", "peur", "pouvoir", "rue", "thé, tisane", "théâtre", "violence", "visage"]
langtxt: ["fr", "it", "en"]
zone: "Suisse romande"
num: "15"
quote: "It's been twenty years that the death of the black bloc° was announced and twenty years that it keeps reappearing all over the world"
---

{{%epigraphe%}}
*\[\...\] the Black Blocs are today's best political philosophers.*

--- Nicolas Tavaglione, Le Courrier, Geneva, 2003[^1]
{{%/epigraphe%}}

{{%ligneblanche%}}

It's been twenty years that the death of the black bloc° was announced
and twenty years that it keeps reappearing all over the world, including
in Switzerland. It is being reborn because it suits our times. With the
rise of mass surveillance, the increasing militarization of the police,
and the ever-more visible disconnect between the people and the elites
who elect themselves through the ballot box, any movement for social
transformation that hopes to make even a slight difference seems to
demand anonymity, violent confrontation with the authorities, and
establishing a position of power within the city.

In response to the crisis of globalized capitalism and the beginnings of
the coming eco-social crisis, bloc strategies are being increasingly
adopted, especially in countries and cultural contexts where they are
new. These strategies have developed in a decidedly hostile environment.
Often clandestine, they are confined to counter-media°, without any real
organization or open knowledge sharing, against a stable power structure
and without much support from pacifist social movements or the rest of
the population. And yet it is this 'rest of the population' that keeps
joining the most radical form of urban presence there is: the black
blocs.

This text mixes the French-Swiss experience with some elements of
*Défense du black bloc ("Defense of the Black Bloc")* (2010) by
anti-racist activist Harsha Walia and *Les black blocs, la liberté et
l'égalité se manifestent (Who's afraid of the black blocs?: Anarchy in
action around the world)* (2016) by anarchist theorist and activist
Francis Dupuis-Déri.

## Surviving in theory

What is a radical bloc for? Well --- several things:
- it is a tactic, which may or may not be effective, but whose legitimacy in itself is not up for discussion.
- in principle, it serves the idea that we are a single anonymous group ; in practice, it allows everyone to demonstrate without being captured by state surveillance, preventing the creation of a "file" that could easily follow them for the rest of their life.
- In many cases, the bloc allows people to be "dis-arrested" by spreading out, using the element of surprise, and going on offense. The bloc is one of the best realization of the expression "no one left behind".
- it shakes up the ritualization of disobedience and protest: the tendency to obey the limits of authorized space, to stay within supposedly democratic conventions (demonstrations, sit-ins°, petitions,
etc.).

## Surviving criticism

The radical bloc is often criticized for opening the door to police
agitators, who dress up as protesters, carry out fake break-ins
(attacking insignificant targets, such as small shops instead of big
banks), or entrap real demonstrators. This doesn't hold water: the
history of protest makes it plain that the police have always deployed
provocateurs of this kind, and that they often disguise themselves as
journalists or unmasked protesters.

The bloc also gets a lot of criticism for being ineffective, which is
often true --- but we fail to notice that other tactics (like challenging
politicians by marching around with signs) are equally ineffective. Yet
most of the criticism for 'ineffectiveness' falls on the bloc, which
means that the debate itself is shaped by an internalization of dominant
value systems relating to the representation of private property and
violence.

The bloc is often taken to task for its violence. But there are three
kinds of violence. First, primary violence: that of capital, of
extortion, of exploitation, of killing, of racism, of patriarchy. Next,
defensive violence, which exists only to react legitimately to the
first. Finally, the violence of repression, which occurs when the state
wants to ensure the perpetuation of the first violence. When we break
the windows of banks that make their money from dictatorships,
neo-colonial networks, and sometimes genocide; that by themselves emit
up to 20 times more CO2 than the entire Swiss population --- is this not
an expression of the defensive violence of the living? The bloc's
targets are never chosen at random: the World Trade Organization, the
International Monetary Fund, the G8, the G20, the WEFF, banks, insurance
companies, and multinationals. We should never forget whom the bloc is
trying to destabilize, nor the power of the police. A handful of people
with rocks and scarves against hyper-trained troops with riot gear that
costs tens of millions of francs. Who does the police defend?

Nor should we forget that protest violence becomes legitimate and
necessary in the eyes of the majority as soon as it becomes a part of
history, a "past" violence upon which essential progress has been built.
In France, the guillotining of the king is a good example --- and today,
Jeff Bezos is far more powerful and oppressive than Louis XVI ever was.
The direct actions of strikers, suffragettes and Black Panthers,
particularly the violent ones, have led to a more feminist, less racist
world, a world in which people no longer work 12 hours a day, 360 days a
year. And this violence, this violence of "yesterday," is widely
accepted, even justified. If it is more just, more anti-racist, more
feminist, more ecological, our future will justify the violence of
today's black blocs.

The bloc is criticized for divisiveness, but experience shows that by
creating a distinction in the public mind between "good" and "bad"
protestors, it keeps police focus away from open and alternative
community initiatives and induces the media to view the latter (as
opposed to the bloc) positively. Very often --- almost always --- people
who participate in blocs support diversity of tactics, defend the need
for community support, and understand the usefulness of peaceful and
non-violent strategies.

On the other hand, the spokespeople for the "reformist" movements, who
play the game, trying to improve their image in the press by distancing
themselves from the bloc, only end up contributing to the legitimization
of state violence. Moreover, it is not uncommon to hear politicians,
too, condemning bloc violence. This is a central tactic for co-opting
and dividing the struggle.

A small real-life drama, demonstrating a good response to media demands
for "peace:"

> *David Pujadas: Of course we understand your concerns, but isn't this
> going too far? Do you regret the violence?*
> 
> *Xavier Matthieu (CGT delegate from Continental): I hope you're
joking?*
> 
> *...*
> 
> *David Pujadas: Well, we get your anger, but are you calling for calm
> this evening?*
>
> *Xavier Matthieu: I'm not calling for anything. It hasn't crossed my
> mind to call for calm. People are angry, and anger must be expressed.
> He who sows misery reaps rage.*
> 
> *(France 2 television news, 21 April 2009)*

It is worth noting in passing that what Pujadas calls "concerns" are
the emotions of hundreds of employees being laid off by a multinational
company which is simultaneously making enormous profits. By denigrating
violent tactics, the media reinforces the image of a chaotic and divided
movement, while our job is to create the image of a coherent movement
which is comfortable with a variety of tactics. The more the media,
politicians, and other movements marginalize certain approaches, the
more freedom the police have to violently repress them.

The bloc is often criticized for being an insular minority, but,
throughout history, struggles have relied on the use of direct action by
small groups (for example, during the Swiss strikes of 1917). Direct
action emerges when people defend themselves. If we always had to wait
for millions of people to mobilize simultaneously, or for politicians to
listen to the demands of militant collectives and trade unions, the
world we live in today would be much worse. It's not even clear that the
"radical minority" argument holds water, since there is in fact a
continuum between emancipatory demands and bloc action: in practice, a
green bloc° often makes the same demands as pacifist movements like
Swiss Climate Strike.

The black blocs are called out for their desire to break things: people
opposed to them call them "hooligans", obfuscating their political
identity. They are said to have "an apparently pointless destructive
fury \[...\] without political or ideological motivation."[^2] If this is true, then how do you explain the banners the black blocs carry, or
the specificity of their targets?

The bloc is often accused of legitimizing and reinforcing the brutality
of the police state. If this is true, then we might as well abandon any
idea of collective action: the police state justifies itself. As soon as
a demand gets big enough, the entire repressive apparatus is activated,
and in all situations. Since when do we hold our allies responsible for
the police decision to intensify their violence?

The bloc is often blamed for undermining social movements and costing
them credibility in the mainstream media. The logic is the same: since
when is the mainstream media on the side of social movements?

In short, there is no reason to idealize the black bloc, which is just
another tactic (not always relevant or effective). The problem is that a
majority of the arguments against it are essentially based on the *a
priori* internalization of dominant and oppressive values (respect for
private property, presentability, administrative responsibility,
legitimacy in the eyes of the media, etc.).

## Handling tear gas

Swiss riot police usually resort to tear gas to disperse large (often
festive) demonstrations. They seem to be more inclined to use flash
grenades or water cannons when they have to disperse small, organized
groups.

Tear gas happens, but you can be prepared for it:
-   Don't panic, it's horrible, but it passes in 10-15 minutes.
-   It will be better when you'll be in a warm place, with some tea and
    a hug.
-   You may feel nauseated, but it will pass.
-   If you have cramps or spasms, or if you have respiratory discomfort
    that persists for over an hour, go see a doctor.
-   If you are expecting a child and are exposed to tear gas, you should
    also see a doctor.
-   An excess of secretions (saliva, mucus) is a biological defense
    mechanism; don't hesitate to blow your nose and spit as much as
    possible, otherwise you can quickly start to feel like you are
    drowning in your own secretions.
-   Try wrapping your mouth and nose in a moistened scarf (even under a
    balaclava) if it's moisture from perspiration, it will also do. Make
    sure you always have water in your bag.
-   Protect your scalp; tear gas can get into your pores.
-   In general, the less exposed your skin is, the less the gas will
    hurt you.
-   Avoid shaving any exposed part of your body (including your face)
    just before a demonstration --- gas is very irritating to raw pores.
-   Touch your eyes as little as possible.
-   Don't forget to bring swimming goggles.
-   Have plenty of saline solution in your bag (you can buy it in a
    pharmacy without a prescription and it's not expensive).
-   If possible, wear glasses instead of contact lenses, even if it's
    sometimes difficult under a balaclava (contact with the gas can melt
    them). If you are wearing contacts, as soon as you see the slightest
    evidence of gas, remove them (before you get gas all over your
    hands) and put on your glasses. If you get gassed while you are
    wearing lenses, flood your eyes and hands with saline, remove the
    lenses, and then wash your eyes again.
-   You can also try to counteract the tear gas right after it is
    launched, but be careful: this involves getting in close. There are
    two approaches :
    1.  Take a tennis racket, or any long elastic object, and use it to
        throw the canister back (aiming at the sender, of course).
    2.  A canister generates gas via an explosive reaction. Basically,
        something burns inside, and as long as it burns, gas comes out. So
        you can try putting it out like you would a fire (you can also start
        fires), for example by covering it with a construction cone and
        pouring a lot of water through the hole. (This tends not to be very
        effective, as it requires a lot of water and you rarely have a lot
        of water at hand.). Or you can simply put a cardboard box over the
        canister, or even throw it into a garbage can (which you have to
        seal up, of course). Nowadays, many models are waterproof and so
        none of this may work.
-   Finally, when you get home, keep your clothes, as much as possible,
    from touching anything in your house (the gas residue can stay
    active for up to five days) and take a cold shower for 20 minutes,
    which will minimize the irritation.

## Surviving police repression

-   First and foremost, your actions can expose others to repression ---
    so THINK about what you plan to do: if it's just going to put people
    who are already particularly vulnerable in even greater danger (for
    example, in an undocumented workers' movement), you might as well
    stay home. Labor and/or environmental struggles are often more
    conducive to radical and autonomous action.
-   In general, never show up at a demonstration looking like those cool
    people on the pictures, wearing all your gear. First, because you
    won't look so smart when you're on the subway, and second because
    if you are easy to identify, the police won't shoot or gas you,
    they'll probably just surround and arrest you. To avoid this, it's
    better to be mobile and blend in with the crowd.
-   Never go it alone, always go with a group you feel allied with and
    try never to lose sight of them. If you need to move through a crowd
    together, always hold hands. If you can't find a group, at least
    come with a "buddy" so you can look out for each other.
-   Always engage at the beginning or middle of a demonstration, never
    at the end. First, to set the tone for the action (which will often
    become more radical if it is stirred up from the start) and second
    to avoid being isolated at the end of the day. The police are much
    more violent when there is no longer a large number of "nice"
    citizens to watch them.
-   The timing of masking is crucial and a difficult issue: too late and
    you risk being identified, too early and you risk being caught.
    Small masked groups walking alone before an event are ideal targets.
    In large demonstrations, it is best to blend in with the crowd, with
    people around you whom you trust, until you arrive at a meeting
    point and can mask up.
-   Using layers of clothes, prepare to change what you're wearing
    several times as you walk, and to change again before you leave.
-   Try to always have a scout (someone who wants to have a lower risk
    profile) who can keep an eye on the situation and warn the bloc of
    mass movements of the police.
-   Try to keep the bloc compact, to prevent small groups of cops from
    sneaking in and grabbing individual people. Large reinforced banners
    at the front and sides are also good for preventing this type of
    arrest. Be careful --- you will need several people to hold them.
-   Choose one-day-only nicknames for each member of the group, to avoid
    addressing each other with identifiable names.
-   When communicating, stick to the rules of digital security,
    especially if you use a chat channel on a smartphone. Simply leaving
    your smartphone at home is always safer.
-   A banner or sign can effectively hide a stronger shield.
-   Athletic pads (shin pads, knee pads, etc.) can be useful.
-   Flashlights, or even toy lasers, can blind.
-   Most often, the police will disperse a crowd (with gas, flash
    grenades, etc.) when they are not prepared to make arrests. Once
    they are ready, they will probably charge, in line and in successive
    waves (advancing then retreating). Their first step is usually to
    make a few violent arrests of individuals, to impress and discourage
    other protesters. If the crowd is compact, they will probably keep
    their distance. If the crowd looks timid, chaotic, or passive, they
    will try to form lines to break up the larger mass and isolate
    smaller groups, which they can then surround (this is known as the
    "noose").
-   If you are caught by the police, keep in mind that even the smallest
    action in defense of yourself can make the charges considerably
    worse (assaulting a police officer). A good approach is to stop all
    movement and instantly become dead weight. This limits what you can
    be charged with and often complicates the work of the cops.
-   At the end of the action, the final challenge is to get out quietly.
    Having layers of clothing is useful: find a quiet place to change,
    or do it in the middle of the crowd or in a parallel street, but
    always check for cameras. If you don't know the city, don't
    hesitate to follow groups you see running away, or to sit down in a
    café and let some time pass. It is a good idea to disband the group
    and stay just in pairs.
-   Prepare yourself for the legal aspects of repression. Go to the
    demonstration with a lawyer's number memorized or written on your
    arm, and consult a good anti-repression guide written by activists
    in your country --- a recent one, if possible (laws change quickly).
    These guides help you understand, for example, what you can and
    cannot say in police custody, etc.
-   Finally, the first rule of direct action: stop when things are still
    going well, don't do too much, and save yourself for the next
    action.

## In conclusion

Rioting is not in itself a political project, it should never become
one, it is only a form of protest (indeed, there are even historical
examples of police riots). Remembering that we act this way *because we
have not been given the choice whether to use violence or not*, we
remain focused on what matters: building a benevolent world of mutual
care. Everything else is incidental and tactical. Sometimes it's an
outlet, sometimes it's a reparation, but the experience of the bloc
always reminds us of *what really matters*: being together, resisting,
loving each other, and not giving up on the dream of a world less bad
than the one we currently live in.

[^1]: This quote opens the book *Les black blocs, la liberté et
    l'égalité se manifestent* (Black Blocs, Freedom, and Equality at
    the Barricades) *by Francis Dupuis-Déry*.

[^2]: Federal Office of the Police, Federal Department of Justice and
    Police, Analysis and Prevention Service, "The Potential for
    Violence in the Anti-Globalisation Movement", Bern, July 2001.
