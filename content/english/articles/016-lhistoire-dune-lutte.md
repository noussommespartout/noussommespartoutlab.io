---
author: "Samba"
title: "The Story of a Struggle"
subtitle: "A new life to learn"
info: "Transcript of an oral interview for the Swiss-French collection"
datepub: "April 2020"
categories: ["migration struggles", "police, repression, justice", "anti-racism(s)", "solidarity, mutual aid"]
tags: ["administration, administratif", "cuisiner, cuisine", "gare", "impossible", "pouvoir", "prix", "rire, se marrer, rigoler", "rue", "violence"]
langtxt: ["fr", "en", "it"]
zone: "Suisse romande"
num: "16"
quote: "living without papers is a true struggle"
---

My name is Samba, I come from Gambia. It was an ordeal to come to
Europe. We encountered many difficulties as we made our way across
numerous African countries. It wasn't always easy to be with all these
people I didn't know, to live a life I had never lived before. The
hardest place was Libya. There, no one will give you anything to eat or
help you. We tried to find food, work to survive, a place to sleep. You
can get stabbed for no reason. Your life is always in danger. After a
while you stop believing you will ever escape. Every day when you go
out, you don't know whether you'll come back or not. Every morning,
you wake up saying to yourself: "today, maybe I'll die, maybe I'll
live". I've been in prison, I've been mistreated, I've experienced
horrible things, things you can't imagine.

And then you arrive in Europe. You have to re-learn to live, in a
different society, in a different culture, with different people, all
while trying to overcome the language barrier. You have to find your way
in a new life and you go through a lot. My life in Europe, it started in
a camp in Italy. You live in a camp with crowds of other people, who
don't come from the same country as you or who don't speak the same
language. One problem follows another. People run into all kinds of
difficulties, some have serious psychological problems. You have no
choice, you have to share your living space. After you leave the camp,
there are other problems. You are no longer in an organized space where
people give you something to eat, you are all alone. If you don't have
papers, or even if you do, how do you find a job if you don't speak the
language? How do you find a job if you don't know anyone? Some days
it's very hard. If you don't have papers, you can't have a house, you
can't pay rent... For me, the real struggle started when I left the
camp. I didn't know anyone, I arrived in Switzerland and asked for
asylum. But my application was rejected. I don't know why, but I think
it's because there is no war in my country. Switzerland told me to go
back to Italy, where they had taken my fingerprints[^1]. So I said to
myself: what am I going to do with my life now? You have to fight to
stay a human being. If you don't forget, at least a little, what you
have been through, you'll lose your mind. It happens: people go crazy on
the street.

I met the collective[^2] through a number of events. I went over to
where they were, to cook with them, and someone asked me if I wanted to
practice my French. This person also asked me if I wanted to come every
Wednesday, because there would be food and people. It's a cool place: I
don't go to restaurants or cafés because I don't have money. Here
it's free-pricing and since I don't have anything, I don't pay
anything. And I can relax. Here I started to feel part of society, to
organize events, to laugh. I realized that learning French would be good
for my future. I'm making progress: *Je parle maintenant un peu
français*[^3]. So anyway, I was coming here more and more and one day
they asked me if I wanted to join the collective. I was happy to accept,
but it took me a while to really feel part of the group. Because of the
language barrier, but also because of difficulty understanding the
system, how it works. It takes time... From a political and
administrative point of view, it's really hard if you don't have the
right to live in the country. Sometimes you don't feel comfortable
going out because the police might ask you for your papers and you might
end up in jail. Not having papers changes everything. It keeps you from
doing a lot of things. There is also the ever-present racism, you face
it in the street, you are constantly attacked and you have no weapons to
defend yourself. You are denied the right to defend yourself. If the
cops come, even if you're in the right, you'll end up in jail.
Whatever the situation, the cops will only see one thing: you have no
right to be here, you have no rights at all. So you give up, you don't
have the weapons to defend yourself. It affects me in lots of ways, but
being part of an organized collective is new for me, it's cool, it
gives me more opportunities, more ways of connecting. Together, we can
take part in a protest, and it gives us visibility, as black people and
as people who are denied both the right to live in this country and the
means to defend ourselves. The only thing we can do is demonstrate in
the streets, so that people can hear it from our own mouths. It's
important to make people aware, even if it takes time. It brings change,
step by step.

Living without papers is a true struggle. It's been five and a half
years now. Wherever you go, whatever you do, you have to think hard: how
do I get from point A to point B? What's the safest route? Even going to
the park, you have to be careful. The cops might be waiting to catch
you. Sometimes you have to ask if it's even worth going out: you're
not free. I have friends with papers, when we meet, I am the one who
decides where we're going. Because they don't have to think about these
things, they don't experience the city like I do.

Every time I get a job, I go there for a day and then they tell me that
I can't stay, that I can't work there anymore. They say I am a good
worker, but without papers it's impossible, and they send me back home.
My life is always at the mercy of others, whether I like it or not.
Without others, I would have to sleep on the street and do illegal
things - I would have no choice. I hate it, I don't want to
straight-out ask people for money. My goal is to live, to find a job, to
have a future. I'm happy to be part of the collective, it's changed my
life, it's still difficult but at least I don't sleep outside anymore.
Anyway, even if it's difficult, I don't see how I can live in any
other country except Switzerland now. This is the place I know, this is
where I want to be, and I would be terrified to have to live on the
streets again.

Being black is yet another struggle. You run into racial profiling all
the time, even though there are laws against racism. You can tell
someone that what they are saying is racist. You can call it what it is,
just to make people realize that what they are saying is racist, even if
you can't change them. But when you don't have papers, it's like you
don't have a weapon to defend yourself with. A few days ago, I was at
the station and a guy hit me on the arm. I know very well he wouldn't
have done that to a white person. I couldn't just take it, I said to
him "Why did you do that to me? I don't know you." I explained to him
how I felt and he ended up apologizing. But instead of apologizing, you
should think about what you are doing and just not do it. If I had had
papers, I could have had a real conversation with him, a longer
conversation. But I didn't want to get into trouble, even though it
wasn't me who created the problem. And of course, before the people
around me had even understood what was going on, they would have called
the cops. And even if it was me who was the victim, I would have been at
fault, because I have no rights. So I just left. What he did wasn't
right, he ended up apologizing, but that's not good enough for me. It's
an anecdote, but it shows the whole situation: I want to fight for my
rights, but I don't have the weapons to do so because I don't have
papers.

[^1]: Reference to the so-called Dublin Regulations°

[^2]: An autonomous collective that militates for welcoming the
    undocumented immigrants.

[^3]: In French in the original.
