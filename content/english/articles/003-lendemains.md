---
author: "Elena & Rina"
title: "The Days After"
subtitle: "Living the postprotest city"
info: "Text written for the Swiss-French collection"
datepub: "September 2020"
categories: ["demonstration, riot", "activist introspection"]
tags: ["amour, love, amoureuxse", "corps", "crier", "eau", "empathie", "force", "gare", "horde", "mcdo", "rue", "thé, tisane", "usine", "voix"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "3"
quote: "to haunt the walls until you can't look elsewhere"
---

Today, the daily round starts again, cars circulate normally, we walk on
the sidewalk.

Red --- stop ; green --- go.

Walk on the parallel yellow lines of the crosswalk.

Avoid any eye contact.

Make our own way through an ergonomic and efficient factory town.

It's weird. I feel like I'm abroad or in a dream, I don't quite
recognize "my" city. The sensation is almost physical. I feel lonely.
I quickly understand why.

Today, in a central square of Lausanne, my body is almost shocked to be
following a straight line, the line of my daily route to work. An
efficient line: as few steps as possible, as fast as possible. Something
tells me to take a different, more unpredictable path. But chaos by
itself is meaningless.

As I leave the square, I see a purple stencil abandoned under a bench,
which makes me a little sad, but also settles me down. And then I am at
the station, where yesterday I was part of a mass, a horde, a queenless
swarm of bees: one of the most beautiful demonstrations of my life, the
first on that scale. The configuration of space, the movement of
traffic, everything is changed. A few hours ago, the street was ours,
the station was ours, the whole city was ours. Going back through on my
way to work is a small expropriation, a rollercoaster in a blasted
amusement park.

Since that day, I've felt that many times. The mental map of the city I
live in changes. Each militant action transforms my relationship to
certain streets, to certain storefronts, to certain intersections. Each
time I've been part of a crowd that has taken back a piece of the city,
I've had to give it back later. So it's paradoxical. But the more I
take it back, the harder it is to let it go the next day.

A space that was, for a few hours, collective property --- in other
words, truly public space --- becomes fragmented all over again: a few
square meters to starbucks, a few to mcdonalds, a few to the coop, to
the h&m, to a commodity broker. The rest to the state, fullfilled with
its surveillance, its barriers, its one-way streets, its police
presence. Reserved for only one part of the population, the one that
follows the rules and waits patiently, in a democracy controlled by the
lobbyists for all these private spaces, to obtain more rights. I feel
like saying: to give is to give, to take is to steal.

While just the day before, we were a mass with a common goal, united,
shouting together the same slogans, slogans demanding rights that don't
necessarily concern us, that imply a new society for all. The collective
goes beyond your person, your particular interests. In a sociopolitical
configuration like this, you become r. You can tag the windows of banks
that defend the rights of private companies, the right to act without
regard to ecological destruction, to increase precarity without
consequences. You can block roads to force people to think about
domestic and gender violence.

The day after, you find yourself walking at rush hour, among a mass of
isolated individuals doing their private business. An ultimately
self-limiting mass: no longer can you shout, tag, force the
multi-billionaires to share, stop their commercial genocide.

At the end of the day, few of feel these things, because few of us shout
or open squats for homeless people. So, a gap stretches up, between your
side and the rest of the incomprehensible and senseless-seeming world.
You know that your conception of space is very special. You'd like to
share it with all these people, but you can't. We've been diabolically
well trained.

So, you keep walking. In front of this station, some friends got
arrested; in front of this police post, we waited for them to come out;
in this square, I danced with my breasts out; behind this bush, I dumped
a smoke bomb and a stencil when I saw the cops; in this bar, I drank a
hot tea during a blockade in midwinter; in this street, I counted cops
so I could warn comrades who were preparing an action; in this other
street, I lost my voice because I shouted so much; this building that's
been empty for years, we tried to occupy it (in the toilets on the top
floor there's running water).

In this alley I saw:

*NI*

*UNA*

*MENOS*

written in red A4 capitals on white paper, hastily pasted to a wall 20
meters from the police station. And this collage fills me with grief and
strength, it brings tears of rage to my eyes for all the people I love,
and all those I don't know.

It makes me want to transform every facade in the city. To haunt the
walls until you can't look elsewhere, because there won't be anywhere
else to look; until they can't hear themselves think, since we will be
everywhere. In all the streets, I want there to be acts of depredation
that are works of love. I dream of an act of universal empathy ; an
insurrection of us all.

