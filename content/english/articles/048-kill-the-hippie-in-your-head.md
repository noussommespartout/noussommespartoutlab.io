---
author: "Anonymous"
title: "Kill the Hippie in Your Head"
subtitle: "How I gave up on pacifism"
info: "Text written for the Swiss-French collection"
datepub: "September 2020"
categories: ["demonstration, riot", "activist introspection"]
tags: ["adrénaline", "colère", "dissonance", "feu", "gueule", "horde", "injustice", "merde", "peur", "pouvoir", "rage", "rire, se marrer, rigoler", "soleil", "théorie", "violence", "visage", "voix"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "48"
quote: "a masks-and-molotov thing"
---

{{%epigraphe%}}
Hippie: Young adherent, in the United States and later in Western
Europe, of an ethic based on the rejection of consumer society and
expressed in non-violence and an unconventional way of life. The hippie
is not politicized.
{{%/epigraphe%}}

{{%ligneblanche%}}

It was during a demonstration against Monsanto. There were quite a lot
of bobos. Quite a lot of enviros. Children and old people. A bit of
everything, a social mix, but it definitely wasn't a masks-and-molotov
thing. I want to talk about it, even if my militancy has changed in form
and content since then, because I think that was one of my first
encounters with insurrectionary potential. That day, I discovered that
one can protest against harmful economic choices and against those who
make them, with no guarantee of success, of course, and maybe even
knowing that it won't lead to much. But I also want to tell this story
because I tell myself that, among those who come across this book, there
will be some for whom the story might be useful...

So, I was at a demonstration against Monsanto and the measures deployed
to keep the protesters away from the building were impressive --
especially given that we were in a relatively small town, with a soft,
bourgeois population, a "lefty" town, the kind that can host Monsanto
without too much cognitive dissonance.

I only had American movies as a reference, so the deployment didn't
seem that strange to me. On the other hand, those around me, with more
experience, were surprised. These forces (they included cops from all
the surrounding towns), which in theory should have been used to defend
the public interest, had been mobilized entirely for the purpose of
defending a private company.

With their weapons and helmets, the officers were spread out in the
shadow of the building's entrance. It was only when we had made it past
the rickety police barriers that they came forward. I could see others
further away throwing eggs and other things at the shuttered windows. I
think I would have liked to be with them. I found it pretty
exhilarating. I hadn't done this very much. I remember I was sweating a
lot, so much that it was dripping down my back and under my arms.
Looking back on it, I can see I took some pretty stupid risks that day.
My face was not covered, and I didn't have a partner.

I felt a real but vague anger. These robocops in front of me interested
me as much as they irritated me. It seemed wrong that they had put
themselves between us and this shitty company. There was one who seemed
very young under his visor. I wanted to look at him more closely, and
particularly to understand how close I could get to his face. It was
provocation, definitely, but also curiosity. I wanted to see if their
"zone" was really as impregnable as it looked. I must not have scared
him very much, because he hardly flinched when I got up in his face. I
think it bugged me a little that I didn't upset him more. I decided
that I would stand there like that for a few hours.

It was a long time.

I think that after a while he felt a bit put off, the cop, by this kid
riveted to the ground five centimeters away from his bulletproof vest
and assault rifle. It was like trying to see who would blink first. He
winked at me, I scowled at him. After an hour or two, his commander came
and offered to let him move. He refused, said he was happy to be in
front of "such a pretty girl". I was seriously beginning to doubt the
value of what I was doing. He obviously found it pleasant, which wasn't
the effect I had hoped for. Even so, I think he would have preferred to
be somewhere else, especially as photographers were snapping pictures of
us. I should have spit at him, I think; that would have worked. I was
close enough for it to hit his face. On the one hand I was disgusted, on
the other I was seriously starting to get pissed off. I was still
sweating, but at this point it was more from the sun than the
adrenaline. I should have at least laughed in his face, given him the
finger, told him he was a piece of shit. I didn't. But I just stood
there like a useless idealist.

I think I was still filled with this pacifist idea of the revolution.
This hippyish ideal, where you imagine yourself slipping flowers into
cannons, stopping tanks by standing stoic and proud. But in practice,
the tanks crush you. These images are great and all, but I think I've
figured out that it doesn't work like that. Really challenging
authority is not that easy; you can't knock it off balance with simple
and symbolic gestures. The images are powerful, they create a moving,
acceptable discourse that is easy for the media to follow, but the truth
is that social change has not come, or only rarely, from non-violence,
from a kind of soft consensus stoked by images of heroic acts performed
in the face of an ultimately conciliatory government. Often, it has been
underground, sometimes armed, struggles that, with a voice however
muffled or muted, have shaken the foundations, and even broken the
pavement so that a crowd could pick up the pieces and hurl them in the
face of the helmeted government. What we perceive of the struggles,
privileged people like me, is too often the tip of the iceberg. So,
often out of ignorance, we are surprised, even offended, by the reaction
of the oppressed. It seems exaggerated, too radical, too violent.

The other problem with this Hollywoodization of protest is that all too
often, it leads to struggles being taken over by people who have little
stake in them. Let me explain. The story I've just told resulted in a
photo, one that was used again in the swiss media not long ago to make a
point about something, without taking note of the fact that it was my
privilege that allowed me to do this, to stand for several hours in
front of a robocop. This is a recurring problem with sensational images
like this. On the one hand they make collective struggles seem
individual, and on the other they tend to highlight the same struggles
and the same white, cis, able-bodied, hipster activists, the ones who
have their papers in order and their big mouths open, while concealing
the racialized, trans, non-binary, disabled, impoverished, undocumented
people who are the ones actually defending their rights and the ones who
should be visible.

But at the time, I didn't know that the only reason why this jerk cop
wasn't kicking my ass was because I was a privileged young white girl
who was defending a politically acceptable cause. I didn't know yet
that these people kill, that real people die from their beatings and
that they are never punished. I didn't know yet. I didn't know yet how
ACAB°. Nor did I realize that putting myself forward like that could be
problematic.

But never mind. These are things you learn by participating in a
community, by listening to stories, by seeing the images of police
violence that have become increasingly available, filling your belly
with rage. Yeah, actually it's really by listening that you learn. Just
for two minutes, you shut up and listen. You look for how you can be
useful. Sometimes the best we can do is stand back, make food, leave the
field to others, film the cops, wait outside a police station for people
to come out, organize benefits. Basically, make use of our privileges by
making them available to those who need them. Just for two minutes, we
privileged people have to stop grabbing the limelight and recounting to
each other the spectacular myth of our own struggle. That is not where
the truth resides. We have to silence that side of us which has been fed
a reformist, universalist, non-violent and therefore truncated vision of
social reality. When we see the reality of injustice, when we meet those
who are confronted with vertical violence every day, whether because of
their actions or their identity, we understand a little better that
it's not about us. At least that's what I hope. Slowly, we let the
hippie in our head dry up and shrivel away.

Then the next time, it might be a little less oppressive, less
problematic, less lame.

The next time, we'll go back.

With balaclavas.

At the back, not the front.

With cobblestones maybe.

With siblings, for sure.

And we'll go en masse; we'll go with our mismatched imaginations and
our asymmetric privileges, but we'll try.

We will be hordes and multitudes and faceless.

Above all, we'll try.
