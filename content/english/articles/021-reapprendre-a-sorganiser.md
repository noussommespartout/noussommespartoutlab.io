---
author: "Anonymous"
title: "Relearning How to Organize"
subtitle: "Some tools for \"horizontal\" communication and organization"
info: "Text written for the Swiss-French collection"
datepub: "October 2021"
categories: ["self-organization", "DIY, tutorial"]
tags: ["administration, administratif", "amour, love, amoureuxse", "confiance", "corps", "horizontal, horizontalité", "outil", "parole", "pouvoir", "réunion", "silence", "théâtre", "voix"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "21"
quote: "silence is welcome"
---

The modes of communication and organization that we find
"natural" arise from the society which has constituted us and in which
we live. Instinctively, we reproduce the codes and assumptions of a
society that organizes itself hierarchically, that values
specialization, and in which certain voices are accorded more importance
than others.

When we want to create alternatives and act together "horizontally",
we have to reinvent ourselves, to learn new forms of listening and
sharing. In anarchist or autonomous self-management, the point is not to
create spaces without rules or structures, but rather to build and
organize alternatives that allow for a distribution of power, of boring
as well as rewarding tasks. It's about inventing tools that are not
imposed by bosses, leaders, states and police, but created from below,
by everyone --- flexible, adaptable tools, not laws set in stone.

People have written about various theoretical tools touching on these
issues, but I'm not going to rehash all that here. In the collectives
I've been in, we trained ourselves (albeit more slowly than if we had
based our practice on the work of others) without consulting books, by
trial and error, creating the tools that worked for us and our
particular situations and objectives.

Here are some tools that I have used, and helped to put in place in
various situations, to create more horizontal modes of organization and
communication. How do we communicate when we do not speak the same
language and do not have the same educational or experiential
background?

## Silence

### Why silence?

In a society where speaking loudly and taking up space are valued,
welcoming silence can be a powerful tool: a tool of those who aren't
eager to raise their hands; a tool that allows time to reflect and
listen. Silence can help to restore a semblance of horizontality.

It took us a while to welcome silence into our interactions. Someone
said the other night, during a meeting, "if the quality of our
connections is measured by the quality of our silences, then we're
fine". And yes, the silences during that meeting were of high quality.
They are real moments of sharing, moments during which we can digest
what is said.

### How can we welcome silence?

-   At the beginning of a discussion or a meeting, simply announce that
    silence is welcome and that we can collectively try to give it a
    real place in our interactions. Saying this will remove much of the
    awkwardness that can arise during moments of silence. In fact, in no
    time you find that you don't need to say it anymore: silence becomes
    a real participant.
-   Limit how much time people are allowed to speak: five minutes per
    person, for example. If a person goes longer than that, you
    interrupt them. If a person stops after three minutes, the remaining
    time is passed in silence. If something needs to be translated,
    don't do it during this quiet period. Wait until the speaker's full
    five minutes have passed.
-   In discussion sessions, rather than going around the table and
    having each person speak in turn, you can use the phrases "I
    start" and "I finish." "I start" when I want to speak and "I
    finish" when I am done. This tool can allow room for silence if you
    say at the beginning that it is possible to keep "speaking" even
    without saying anything. Moreover, since there is no predefined
    order of speakers, when someone "finishes", there is no pressure
    for someone else to "start."

## Writing

### Why writing?

We are not all equal when it comes to writing --- but even less so when
it comes to speaking. I am not talking about writing long texts, more
about leaving space so that anyone who wants to can take time to write
out an idea or a wish without the stress of having to speak quickly,
having to respond immediately, having to speak in public.

### How do you do it?

-   **A shouting box** is a container where we can put ideas, words of
    love or disappointment, desires and dreams. It is a physical object
    in a specific place, where people can put their words over the days
    or weeks of collective organization. We can then set up communal
    times of sharing, during which we pass the box around. Each person
    picks out a note and reads it aloud, allowing all voices to be
    heard. Sometimes people write about specific situations to be
    addressed by the group. You can choose to react, or sometimes you
    can just listen.

    -   The shouting box can be used for collective emotional sharing,
        but it is also a participatory way to put together an agenda or
        to share ideas and issues to be discussed at a meeting. It is
        sometimes easier for people to reflect privately than to speak
        up in front of everyone.

    -   In my opinion, the shouting box should never be used as a forum
        for criticism. There are better ways to deal with conflict.
        Criticizing a specific person via a shouting box is not helpful.
        It can be emotionally overwhelming. A shouting box can be used
        to speak critically about collective issues, but not
        individuals.

-   **Honey-gathering**. In honey-gathering, large sheets of paper are
    spread out on tables. After a written or oral brainstorming session,
    each issue that the group has decided to address is written on a
    separate large sheet. People then walk around, pen in hand, from
    table to table, from sheet to sheet, writing words, sentences,
    longer discussions, then pass on to the next, and a collection of
    thoughts comes together naturally. Each person has had a chance to
    "nourish" each theme; each word has been able to mature in a quiet
    mind.

## Small groups

### Why?

Public speaking is more difficult for some people than for others.
Speaking in front of 10, 15 or 20 people is not easy. Traditional
meetings can often devolve into power grabs, where those who are most
articulate or have the most information take space and focus. Making
decisions during meetings can be particularly difficult because the
discussion often becomes unbalanced, focused around a handful of people.
Dividing into small groups helps avoid these kinds of power plays.

### How can we do it?

-   It's simple: you divide into small groups to discuss one or more
    topics before returning to the general meeting. When everyone is
    back together, one person from each small group can summarise what
    was said in their goup.\
    When it comes to sensitive topics, there is no obligation to reveal
    everything to the general meeting. In small groups, we can confide in
    each other more easily, or allow ourselves to go in many directions
    before circling back, etc.

-   In groups of 3: one person addresses a topic for a set period of
    time while a second person actively listens. The second person asks
    questions or encourages the speaker if they run out of steam, but
    does not comment or judge. The third person takes notes of what is
    said. At the end of the agreed-upon time, the roles are shuffled.
    After each person has spoken, the group takes a moment to
    synthesize, to understand where the members agree and where their
    ideas differ.
-   This method is good for dealing with substantial or very open
    questions. It allows us to go deep and to weave links between our
    diverse experiences. It also allows us to express ourselves in
    confidence, with only a few people, and to make sure that everyone
    feels really listened to and taken into consideration.

## Body and space

### Why?

Image theatre is a popular education technique that we have tested in
our groups on several occasions. It has always been conducive to the
expression of emotion, as well as a good means of foregrounding topics
of great importance to the participants. It allows us, with the help of
our bodies and various objects, to express things that we would not
necessarily say in words. It also allows us the time to think
differently about our relationship to the group --- a topic to which the
tool is particularly suited, although it can also be used to address
other subjects.

### How?

We set the scene with what we have at hand. Often we use chairs or sofas
to define different locales. The set is designed with the question we
are trying to address in mind. If we want to explore the relationship
between an individual and the group, it is useful to play with
"centrality" by placing certain elements in the center and others on
the periphery or in corners. You can also play with "verticality" by
placing, for example, a chair on a table right in the center of the
space and low chairs on the surrounding floor.

The chairs can then be turned in different directions: towards the
center or, on the contrary, with their backs to the center. You can also
unbalance a chair by placing it on an uneven surface, etc., to create
sets tailored to the subjects you want to deal with.

When the set-up is ready, the group is asked three questions and each
person can then go and stand at a spot they choose, according to their
feelings with regard to the group. First question: where do you stand in
the group? Second question: where do you think others place you? Third
question: where would you like to be? After the first question, each
person moves around the space and positions her/himself quietly. Then
the second and third questions are asked, with people moving around
accordingly. At the end of the exercise, everyone leaves the set.

When the time comes to share, each person in turn retraces their route,
explaining to the others the reasons that led them to take up the
various positions. This tool makes it possible to see things that are
often invisible. Sometimes it brings up strong emotions. It can be
useful in many contexts, but it is particularly interesting to try in
groups that have already been together for some time.

## Mental burden and thankless tasks

As we know, undone dishes, a fridge full of moldering food, or a filthy
toilet can be the death of a group. Beyond ensuring that these thankless
and often unseen tasks get done, how can we distribute the mental
burdens that build up in collective spaces? The challenge is to avoid
'specialisation' and to give value and visibility to everything we do
in order to head off frustration and resentment.

But how can we do this concretely? Especially as there are conflicting
views: some people are allergic to regulations and constraints while
others need rules and a division of labor so as not to feel guilty or to
do too much.

There are a number of approaches, none of which is accepted by everyone.
However, one in particular is interesting and relevant and has been, for
me, very satisfying.

How can we make all tasks visible and develop an awareness of their
distribution inside the group?

-   Distribute post-it notes to everyone so they can write down all the
    tasks they take on inside the collective: cleaning the fridge or the
    toilets, running the bar, shopping, repairs, reception, answering
    emails, writing newsletters, finances, etc. Everyone writes down
    everything they do, from the smallest thing to the most
    time-consuming, the least rewarding to the most. The post-its are
    then placed on a large table. You can try to divide them up into
    'clusters': administration, housekeeping, external communication,
    etc.
    -   Taken together, all these post-its make it possible to see
        everything that gets done in the collective and to give value to
        each task. You also quickly realize that there are tasks that
        are performed by many people and others that are carried out by
        only one. The idea is not to compare the number of tasks each
        person performs (to avoid this, the post-its can be anonymous).
        The idea is simply to be aware of everything that gets done, to
        recognize it, and to become aware of the distribution of work
        within the group.
-   The next step is to draw lots to select groups of people that will
    be in charge of each of the "clusters" for a given period of time,
    say three months. If none of the people selected have the knowledge
    or skills needed to manage the cluster, you draw again. The group
    thus formed will take on the responsibility --- the mental burden as
    well as part of the workload --- of managing the cluster in question
    over the defined period. Of course, everyone should also help out
    elsewhere, and the selected group does not have to do all the tasks
    related to their particular cluster. But they have the
    responsibility for knowing what has already been done and what is
    still to do. Each group can organize itself as it wishes. Then,
    every three months, we switch.
-   This technique helps spread the mental burden and facilitates
    organization. It also allows everyone to learn new things, since we
    change clusters regularly. Furthermore, it helps us be aware of all
    the various things that are done by members of the collective, which
    helps us value the role of each person and avoid the frustration
    attendant on "invisible" work. Finally, this random assortment to
    small groups allows people to work alongside people they might not
    naturally otherwise gravitate to.

It's up to us to invent, adapt, try this and that, and look for
solutions that suit as many people and as many different sensibilities
as possible. And doing this has an added benefit: these new approaches
to sharing, listening, and organizing also help us maintain the desire
to stay politically active and involved.

