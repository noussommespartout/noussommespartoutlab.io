---
author: "Anonymous"
title: "When Space S t r e t c h e s"
subtitle: "What we call in French \"Mixité choisie°\""
info: "Text written for the Swiss-French collection"
datepub: "August 2020 (translated in may 2022)"
categories: ["self-organization", "activist introspection"]
tags: ["brèche", "crier", "outil", "silence"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "27"
quote: "it's the excitement of existing together"
---

\[*Mixité choisie* is the practice of organizing gatherings of people
from one or more social groups subject to oppression or discrimination,
while excluding those from groups considered potential agents of
oppression or discrimination. The goal is to avoid reproducing patterns
of social domination. It is an activist practice used by groups with a
range of priorities including feminism, anti-racism, LGBTQIA+,
disability, and gender minority issues. The word "mixité choisie" is
used in French to frame the concept of identity-based affinity groups
(like you would say in english "no cis° men" or "POC Only") regardless
of the identities in question. It has no specific translation in
English\].

 

When you get into *mixité choisie*, all of a sudden you
create space.

The space that opens up, that becomes available, expands.
Things suddenly look wide open.

 

{{%centre%}}
s   p   a   c   i   o   u   s
{{%/centre%}}

 

In my experience, there are sometimes silences.

\[   \]

<span style="margin-left: 2em;"></span>\[   \]

<span style="margin-left: 4em;"></span>\[   \]

And these silences,

<span style="margin-left: 6em;"></span>\[   \]

<span style="margin-left: 8em;"></span>\[   \]

they are a void.

<span style="margin-left: 10em;"></span>\[   \]

A void can be filled,

a void means the possible,

it's a space to occupy,

it's like fallow land,

it's like a rift.

 

Sometimes there is no silence,

there is euphoria,

the kind we never allow ourselves,

anywhere,

all the time.

 

It's the excitement of existing together.

It's the joy of refusal:

the refusal to accept,

at all times,

living with systemic discrimination.

 

Sometimes there is magic,

things become clear,

obvious,

they flow naturally,

when before they seemed

so difficult to express

and to explain

elsewhere,

in other time-spaces

that were so constricting.

 

Sometimes it's intense,

and we cry

the warmest

the most comforting

 

t

e

a

r

s

from our ravaged

<div><span aria-hidden="true" style="visibility: hidden;">from our ravaged</span>l</div>

<div><span aria-hidden="true" style="visibility: hidden;">from our ravaged</span>a</div>

<div><span aria-hidden="true" style="visibility: hidden;">from our ravaged</span>c</div>

<div><span aria-hidden="true" style="visibility: hidden;">from our ravaged</span>r</div>

<div><span aria-hidden="true" style="visibility: hidden;">from our ravaged</span>i</div>

<div><span aria-hidden="true" style="visibility: hidden;">from our ravaged</span>m</div>

<div><span aria-hidden="true" style="visibility: hidden;">from our ravaged</span>a</div>

<div><span aria-hidden="true" style="visibility: hidden;">from our ravaged</span>l</div>

<div><span aria-hidden="true" style="visibility: hidden;">from our ravaged</span>glands.</div>

 

*Mixité choisie*,

is not a social project,

it is a tool,

it is a strategy,

it is rest.

 

*Mixité choisie*,

is not a social project,

it is a tool,

it is a strategy,

it is rest.

Being in *mixité choisie*

means always being aware of your specific socialization°.

We are there to socialize differently,

to speak with new words,

and with new intonations,

to reconfigure human relationships,

to build social structures for better tomorrows,

to create care strategies,

to look each other in the eye with the joy of self-discovery,

or rather, with the joy of re-discovering ourselves,

to find ways of repairing,

to learn to love ourselves, because often,

we have been taught to distrust each other,

to find ourselves less good,

less cool,

less strong than the others.

 

Sometimes we feel uncomfortable.

Sometimes we feel weak.

It's difficult to admit.

 

It's difficult to admit,

that in a group of 15 people,

when there are no cis men,

we lack certain skills,

because no one has passed them on to us,

we've been denied them.

 

It's difficult to admit,

that in a group of 15 people,

when there are no whites,

we lack certain resources,

because we've been refused access to them,

because they've been stolen from us.

 

But we invent solutions,

we learn,

the ecology of means

and above all,

we learn,

we take back,

we search,

we steal.

 

Sometimes we organize

fantastic retaliations,

we move on to action.

It's a shame,

but it goes faster,

in *mixité choisie*.

So -- it's the joy of SHOUTING,

 

{{%centre%}}
<span style="word-break: break-all;">toactdirectlywithoutintermediarieswithoutaskingwithoutexplainingtoactquickly</span>
{{%/centre%}}

 

{{%centre%}}
IT IS

{{%cadre%}}
SELF
{{%/cadre%}}

EMANCIPATION
{{%/centre%}}

 

{{%centre%}}
IT IS BY US

AND IT'S FOR US
{{%/centre%}}

 

{{%centre%}}
IT IS A WEAPON

OF WAR

AND LOVE
{{%/centre%}}
