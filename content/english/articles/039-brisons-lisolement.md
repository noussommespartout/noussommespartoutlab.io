---
author: "Jupiter"
title: "Let's Break the Isolation"
subtitle: "An account of anti-prison organizing"
info: "Text written for the Swiss-French collection"
datepub: "February 2021"
categories: ["police, repression, justice", "anti-carceralism", "anti-racism(s)"]
tags: ["isolement", "pouvoir", "silence", "rue", "voiture, bagnole", "torture", "permanence", "caméra", "force", "torture", "isolement"]
langtxt: ["fr", "it", "en"]
zone: "Suisse romande"
num: "39"
quote: "isolation is one of the main weapons of repression"
---

*Jupiter is a collective in support of prisoners who are victims of the
police. The majority of collective members are Black people.*

For days, months, years, we have seen friends disappear for days,
months, years. Every day, people are arrested and imprisoned. In fact,
in many cases, we don\'t even see them. Even if some of them have
documents issued by other European countries, which allow them to
travel, without residence papers valid in Switzerland they are
considered illegal. This is a direct way of sending a certain category
of people to prison, a category of people that the system makes
invisible to the point of disappearing them into cells.

Isolation is one of the main weapons of repression.

From the police station to the prison, everything is organized so that
the person arrested has no resources. In Lausanne, this always begins
with detention in the prison section of the police headquarters or at
the Blécherette gendarme station, rights-free zones where the conditions
of detention are akin to torture. Those arrested are regularly beaten in
their cells, humiliated, threatened with death, and forcibly medicated.
The cells are lit 24 hours a day, a camera is always on, there is no
daylight, no visitation rights, no translation of official letters
received and no information other than the fact that there is no room in
the prison and that you have to wait\.... The maximum legal period of
detention at the police station is 48 hours, but many stay between 15
and 30 days. After that, they are detained in prison. Inside, no one
knows when they will be able to get out. Months go by. In the days
leading up to a scheduled release, guards bring new letters adding days
to the sentence, following decisions that the prosecutor makes off his
or her own bat, while the person remains in prison. With no possibility
of establishing contact with the outside world, those arrested and
imprisoned disappear for a few days, a few months, a few years.

Every day the police harass people of color on the streets. Every day
the police beat, kick and torture people, out of sight, in their cars,
in the bushes, in back alleys, because of the color of their skin. The
cops come onto public transport and only pick out Black people for
checks. They go into West African restaurants, pick people at random,
and take them out to frisk them. Large numbers of patrols come into the
city and onto the street, stop Blacks, line them up against the wall and
encircle them, sometimes handcuffed, asking them for their residence
permits and searching them. Frequently, they take residence papers and
destroy them while stealing any money the people have on them.

Faced with this situation, faced with silence, faced with the
invisibility that the state desires, we want to speak out, we want to
make plain. We have formed this group in response to what we see, have
seen, and, for some of us, have directly lived. We have decided to
organize events, first of all to raise money to support those in need,
especially those in prison. For example, simply being able to buy phone
cards allows contact with the outside world. We disseminate information
and, at the end of the day, we take time to meet together, to encounter
one another and to think about those who are not there.

Isolation is their weapon. Let's break the isolation.
