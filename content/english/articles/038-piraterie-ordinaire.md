---
author: "El."
title: "Everyday Piracy"
subtitle: "Some ideas for creative sabotage"
info: "Text written for the Swiss-French collection"
datepub: "April 2020"
categories: ["sabotage, direct action", "DIY, tutorial"]
tags: ["alcool", "bouffe", "coller", "graines", "internet, web", "logo", "nuit, obscurité", "rire, se marrer, rigoler"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "38"
quote: "to hack the ordinary network, you can..."
---

I've never really liked ideologies. They have the advantage of
providing a framework, of offering a kind of island on which you can
find your bearings, a place you can explore the great lake of
possibilities with some assurance. Ideologies limit what can be said,
determine the language within which words can have meaning. They help us
speak and think, but when we think them through to the end, we realize
that they circumscribe our thoughts. We think we have decided for
ourselves what is and isn't right but realize that in fact our
conclusions are conditioned by the limits of the ideology and its
attendant language.

So, I prefer to act locally, to use my person to challenge the ordinary,
to disrupt daily oppression, to find the counter-order of things. Acting
is not that different from thinking, because one has to situate the
action. The meaning of action is relative to your position, and if your
position is as a number, a percentage looking vaguely for a job, a pawn
in the great funereal game of the totalitarian marketplace, it's in
your interest to act like a pirate. From this arises an everyday piracy,
one which can be easily shared. We'd have to make a list of
microgestures capable of interrupting the signal, of creating a local
disruption of the network, of piercing the dominant ideology so that it
spills out through its foundations. What happens next, we'll see.
We'll invent it collectively. We don't need help to invent our lives.
If the space you inhabit defines you as a user, it's because you are
dominated by it, because the great net of discipline does not provide
you with the scissors you need to cut out a liveable zone.

The list below is partly ideas pirated from elsewhere, partly
discoveries. But you never find anything; at best you reproduce
strategies you didn't know already existed.

To hack the ordinary network, you can...

...make a monthly calendar with a list of events that give out free
food (openings, conferences, shows, etc.) and distribute it to people
around you.

...hitchhike.

...set up vegetable gardens in public parks.

...draw your own pedestrian crossings or bike paths with tape and
paint.

...print multiple faces on your t-shirts to defeat facial recognition.

...change the slogans on advertising posters.

...stencil a 'disability' logo on every parking space in a local
supermarket.

...use air rifles to disable your city's surveillance cameras.

...stuff postage-paid envelopes from advertisements full of other ads
and send them back in the mail.

...with a simple screwdriver and a tarpaulin, turn a public bench into
a free shelter for the night.

...install spontaneous planters along sidewalks, using old tires,
garbage cans, or pallets.

...put food coloring in public fountains, to make children and
passers-by laugh.

...use automatic checkouts in stores, and only scan two-thirds of what
you take.

...plant "indestructible" seeds (like kudzu) in public parks or
around vacant buildings to speed up urban renewal.

...disable police cars and construction equipment by sticking a potato
in the exhaust pipe.

...freeze a lock using a syringe filled with epoxy glue and a little
alcohol.

...go through department store dumpsters and offer what you find, for
free, on the sidewalk.

...destroy barcodes on electric scooters with a screwdriver or carpet
tape.

...stick 'skip ad' buttons on billboards.

...sneak into construction sites at night to make castles or sculptures
with sand or gravel. In the morning, they will put a smile on workers'
faces.

...give your neighbors the code to your wifi.

...put old rugs on barbed wire to create safe crossing points.

...wrap cloth around anti-pigeon spikes so birds can land on them
again.

...install swings under bridges, under trees, under the frameworks of
parking garages.

...protect yourself from police dogs with cayenne pepper --- it
temporarily blocks their sense of smell and doesn't hurt them.

...mix seeds, soil and clay to make "seed-bombs" - green up your
city.

...take bikes from dumps or repair shops, paint them all the same
color, write "free bikes" on their frames, and distribute them around
your city.

...steal things and return the next day to "get your money back."
Distribute the proceeds.

...reply to all job offers with letters detailing how much you don't
want to work for them.

...graft fruit trees onto city trees so they start to bear fruit (look
on the internet for species that go well together).

...on google maps, leave bad reviews and offensive comments on all the
police stations in your town.

...steal Christmas lights from local shops and put them up in
surprising places.

