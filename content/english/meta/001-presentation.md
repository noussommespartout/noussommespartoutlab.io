---
title: Presentation
meta: true
num: "0-1"
metatext: |

  <figure class="centre--image image--presentation"><a href="https://abrupt.cc/nsp/nous-sommes-partout/" target="_blank" rel="noopener"><img class="lazy image" alt="le livre Nous sommes partout" src=/img/livre-nsp-1.jpg sizes="(min-width: 50em) 50vw, 100vw" srcset="/img/livre-nsp-1-small.jpg 400w, /img/livre-nsp-1-medium.jpg 800w, /img/livre-nsp-1-large.jpg 1200w, /img/livre-nsp-1.jpg 2400w"></a></figure>

  **Collectives from French and German speaking Switzerland**

  <a href="https://abrupt.cc/nsp/nous-sommes-partout/" target="_blank" rel="noopener">The first backup</a> was published in the form of a book, published by Abrüpt in October 2021. A second collection is underway, in view of a second book. The collectives can be contacted at: noussommespartout \[@\] riseup.net.

  The collectives continue to organize pirate and institutional readings:
  - 8-13.12.2020, Le Grütli, Geneva
  - 8-10.10.2021, Le Grütli, Geneva
  - 23.10.2021, Ferme collective de la Touvière, Meinier
  - 08.01.2022, Aargauer Kunsthaus
  - 20.02.2022, Les Amazones, Geneva
  - 10-12.03.2022, La Grange de Dorigny
  - 24-26.03.2022, MCBA, Lausanne
  - 06-08.06.2022, Rencontres raisons sensibles, Toulouse
  - 25.06.2022, Centre syndical Pôle Sud, Lausanne
  - 19.10.2022, Festival des Libertés, Brussels

  **Collective of Bologna (Northern Italy)**

  A first collection is in progress, in view of a first book, to be published in 2022. The collective can be contacted at: siamo_ovunque \[@\] inventati.org.

  The collective is organizing pirate and institutional readings:
  - 09-10.07.2022, Santarcangelo Living Arts Festival
  - 02.12.2022, Zona K, Milano

  **Collective of Lower Normandy (North-West of France)**

  A first collection is in progress, in view of a first book, to be published during 2023. The collective can be contacted at: noussommespartoutbn \[@\] riseup.net.

  **Free distribution**

  *We are everywhere* is a database that can be freely distributed. All the texts published on this site are therefore made available under the terms of the [Creative Commons BY-NC-SA 4.0 License](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.en).

  **Technique**

  *We are everywhere* relies on the power of the [free Git tool](https://gitlab.com/noussommespartout/noussommespartout.gitlab.io/sources) to distribute and version its texts.

  The [source code](https://gitlab.com/noussommespartout/noussommespartout.gitlab.io) of the website is available under the terms of the [MIT license](https://gitlab.com/noussommespartout/noussommespartout.gitlab.io/-/blob/main/LICENSE-TOOLS).
---

## Object of words

*We are everywhere* (*Nous sommes partout*, in french) collects and shares voices originating from antifascists, feminists, anti-capitalists, antiracists, antispeciesists, hacker movements, voices fighting for migrant rights, against all forms of oppression in our societies, for LGBTQIA+ rights, fighting against ecocides, fighting for sex workers rights, against police brutality, for the rights of the undocumented, against ecocide, for the rights of sex workers, against police violence, for the rights of undocumented migrants, for self-determination and emancipation of all workers, against precarization, against the prison system and for all free zones.

The project is conceived as a collection of resources, a constellation of words. In *We are everywhere*, one reads essentially testimonies written for the occasion, reworked texts or transcriptions of self-organized oral discussions.

All of the activists and collectives who wrote for *we are everywhere* received money to support them and their struggles. The economic model of the project is that of subsidized performing arts: *We are everywhere* also takes the form of collective listening-reading sessions, inspired by anarcho-syndicalist popular readings. The creation of this scenic form, co-produced by institutions, as well as its touring, allow to raise the necessary funds.

In this way, the project tries to create a platform for distribution and collectivization of its own institutional financing. The reading sessions, situated in the cultural field as "performances", but whose concept tries to remain anti-spectacular, opens the possibility of a material and financial support to social struggles, including anti-institutional struggles.

All the people contributing to this database are fully informed of this cultural financing, as well as of the scenic form. They write acknowledging the fact that their text can be read aloud by anyone who has registered to participate in one of these listening-reading sessions.

The *We* that gives the project its title claims the dream of an agglomerated energy, of a sum of I's writing in their name and other We's writing collectively, here and elsewhere, a constellation that the pronoun would not standardize. Fortunately, the We does not have the power to reduce the parts to a whole. In this project, the We enlightens the convergences, the synergies of hundreds of voices which are so many vectors of a vast movement of transformation.

If the struggle is like a circle, without beginning or end, then everywhere and at all times We are there.



## Expanding

Originally, *We are everywhere* was the initiative of an editorial collective made up of seven people based in French-speaking Switzerland. The first phase of the text collection was therefore focused on this political territory. In October 2021, the database contains 57 texts, all published in the book "Sauvegarde n°1 / Suisse romande" (Backup number 1 / French-speaking Switzerland), published by Abrüpt.

More backups will come, because as long as the editorial collective manages to find new fundings, new texts will be added to the database. This process makes the edition augmentative; more books will come out as the collection expands: a second backup, a third backup, etc. The increasing size of the book materially signifies the weight of the struggles, and makes the book everytime more likely to pass through windows.

We are everywhere is destined to extend beyond its original french-speaking swiss territory. Other collectives, accomplices of the original group but autonomous in their editorial work, have undertaken new gatherings of struggling texts, notably in German-speaking Switzerland, in Lower Normandy (France) and in Bologna (Northern Italy). Each collective is working to produce its own autonomous books, but all the texts are hosted on this platform. The database will also try to offer the translated texts in as many languages as possible, always in an incremental way and over time. During the readings, whether institutional or pirate, the different editorial collectives propose the texts they have collected as well as a free selection of those collected by other groups.

For the first edition in French-speaking Switzerland, the editorial group first drafted a call for contributions that was circulated in certain activist circles, and then took care of transcriptions, proofreading, typographical harmonization and exchanges with funding channels. The group is also in charge of organizing the listening-reading sessions in the institutions (and some pirate sessions held outside the institutions).

The editorial collectives try to keep authoritarian editorial intervention to a minimum so that *We are everywhere* remains focused on its original gesture: welcoming voices. We want to avoid drawing lines of force that sculpt the project too strictly, we try to let it take the form that the contributions give it, without intervening. Our intention is to socialize reflections on activist practices - in the terms chosen by the many authors. We believe in the necessity of exchange and in the agentivity of the readers to whom we address these heterogenous collections as a social, critical and poetic interrogation on the struggles that structure society and on the way in which one can fight to change reality.



## Fields

*We are everywhere* is neither a collection of critical theory, nor an investigation problematized around specific axis, nor a structured history of the struggles of a selected territory. It is an attempt at participatory publishing, a sum of words that relates the immediacy of contemporary struggles, a textualization of practical and emotional experiences.

Each text, in its own way and to its own degree, is linked to the experience of activist action. This experientiality is revealed in different forms throughout the texts, from sensitive sharing within collectives, to ways of living direct action, or narratives focused on certain life moments considered by the authors as particularly political.

Centered on the experience of radical political action, *We are everywhere* is not organized around predefined ideological perspectives. That said, in order to accommodate voices, ideas and modes of action that are quite invisible in the dominant inter-discourse, a certain ideological environment imposed itself. We can try to grasp it by the constellation of adjectives the contributors used to describe themselves: non-institutional, radical, libertarian, intersectional, anti-capitalist, anti-fascist, anti-state. *We are everywhere* tries to avoid labels, as words refers to different realities according to individual histories, imaginaries, cities, countries, terrains, ideologies or vocabularies. The editorial collectives do not establish stable ideological boundaries in which each contributor would be forced to recognize themselves by participating in the project (all contributors can withdraw their text at any time from the database, and thus from future printed editions).



## Formats

Concretely, the texts circulate in three forms: this database, “backup” books and participative listening-reading sessions.

The database (noussommespartout.org) is constantly growing, as contributions come in. All texts are available free of charge, online and in their entirety, and all can also be downloaded individually in formats that facilitate printing and distribution in booklets. The database centralizes all the texts from the different linguistic and geographical versions of *We are everywhere*.

The books are black boxes, backups of the database, oriented around specific territories. They are printed at the lowest possible price and we have chosen to open them to the traditional distribution circuit (bookstores, digital sales, etc.) in order to favor their circulation and visibility. Neither the publishing collectives nor the publishing houses make any profit on the sales.

The public listening-reading sessions take place in institutional and non-institutional environments. The practice of collective reading has a rich and plural history, protean according to context and usage. These sessions are inspired by "surveying," a technique developed in struggles, particularly labor struggles, to compensate for the lack of time available for legislative and political education. A surveying session uses collective intelligence: we take a book, tear out each chapter and share them. Each person reads the pages they hold in their hands and then collectively summarizes what they have read.

The readings of *Nous sommes partout* adopt a simple performative form. The spectators are invited to a listening-reading session during which they give voice to the collected texts, accessible to all in the form of torn chapters. The readings in institutional spaces are the heart of the project's material interface, as they raise the funds that will be shared with the authors.

