---
title: Keywords
meta: true
type: taxonomyindex
weight: 2
printcategories: true
num: "0-4"
metatext: |
  The keywords invite to read the texts of the database by following the different struggles addressed in the texts. The [index](/en/tags) offers more offbeat itineraries for exploring the database. All the editorial groups (French-speaking Switzerland, German-speaking Switzerland, Bologna, Lower Normandy) use the same keywords.
---

