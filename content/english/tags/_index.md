---
title: Index
meta: true
type: taxonomyindex
weight: 1
printtags: true
num: "0-5"
metatext: |
  The index offers an unconventional entry into the database. It invites us to shift our gaze on the corpus, to open the back doors of the words of everyday life - of an everyday life to be transformed. Each editorial collective (French-speaking Switzerland, German-speaking Switzerland, Bologna, Lower Normandy) has established its own list. The four lists are mixed on the database. This index is meant to be a surprise.

  To navigate through the texts with more structure, according to the struggles addressed, it is better to go to the [keywords](/en/categories/).
---

