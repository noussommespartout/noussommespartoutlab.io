---
title: Wörter-Index
meta: true
type: taxonomyindex
weight: 1
printtags: true
num: "0-5"
metatext: |
  Der Wörter-Index bietet einen unkonventionellen Einstieg in die Textsammlung. Er lädt dazu ein, den Blick auf den Textkorpus zu verlagern und den im Alltag verwendeten Wörter eine neue Bedeutung zu geben --- Wörter aus einem Alltag, den es zu verändern gilt.

  Um die Texte anhand der (thematischen) Kämpfe zu entdecken, empfiehlt sich die Suche über die [Schlagwortliste](/de/categories/).
---

