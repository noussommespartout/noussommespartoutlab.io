---
author: "Ada Amhang und M. Lautréamont"
title: "Sich in unserer Klasse verankern"
subtitle: "Das *Corona-Solifon*: Wie solidarische Unterstützung von unten aufgebaut wird"
info: "Für den deutschen Sammelband verfasst"
datepub: "Im November und Dezember 2021 verfasst"
categories: ["Syndikalismus, Arbeiter*innenkampf", "Selbstorganisation, kollektives Experimentieren", "Selbstreflexion zum Aktivismus"]
tags: [""]
langtxt: ["de"]
zone: "Deutschschweiz"
num: "64"
quote: "Es führt nichts am Kampf in der unmittelbaren Lebensrealität der Prekarisierten vorbei!"
---

Als im März 2020 eine Handvoll Aktivist\*innen unseren[^1] Sitzungsraum
betraten, betrachteten sie uns misstrauisch. Ungläubig suchten sie
unsere Blicke, schmunzelten im Vorbeigehen und schüttelten ihre Köpfe.
Zu absurd schien die Szenerie, die sich vor ihren Augen abspielte: Acht
Leute, weit voneinander entfernt, an verschiedenen kleinen Tischen
sitzend, ihre Nasen und Münder hinter Schutzmasken verdeckt. «Sind die
jetzt völlig durchgedreht? Das ist masslos übertrieben», dachten sie
wohl im Vorbeilaufen.

Was heute eingefleischte Routine ist, konnte vor fast zwei Jahren noch
als hypochondrisches Verhalten abgetan werden --- ein vorsichtiges
Abwägen der Situation hatte im linksradikalen und anarchistischen Milieu
gerade erst begonnen.

Wir sassen also vermummt da, eine historische Situation über uns
einbrechend und wussten nicht, wie uns geschah. Die Stimmung war von
Ernsthaftigkeit, Ungewissheit, Besorgnis und Neugier geprägt. Täglich
erschienen neue Informationen über das Virus, weltweit schien das
staatliche Vorgehen widersprüchlich und chaotisch. Die Lage war äusserst
volatil --- die Sicherheit morgen noch einen Job zu haben ebenfalls.
Dennoch war auch eine gewisse Entschlossenheit zu spüren, diesen
historischen Moment nicht als passive Betrachter\*innen über sich
ergehen zu lassen, sondern aktiv einzugreifen. Wir diskutierten viel und
ahnten, dass ein Lockdown auf uns zukommen würde. Was für Konsequenzen
würde das für unsere Klasse haben, wie gehen wir gestärkt aus dieser
Situation heraus?

Wir waren uns schnell einig, dass sich die Situation der Prekarisierten
verschlechtern würde und dass eine emanzipatorische Perspektive sich
nicht auf den Staat verlassen sollte. Viel eher war es an der Zeit,
solidarische Strukturen der gegenseitigen Hilfe aufzubauen, um durch
kollektive Verantwortung und nicht durch staatlichen Zwang der Pandemie
zu begegnen.

Wir mussten uns zunächst eingestehen, dass viele von uns den Aufbau von
ebenjenen Strukturen jahrelang vernachlässigt hatten. Doch zum Glück
waren wir nicht alleine. Einige von uns waren schon seit einigen Jahren
in der Basisgewerkschaft IWW[^2] organisiert, wir sind gut in der
radikalen Linken und im anarchistischen Milieu vernetzt und in ganz
Zürich schossen Nachbarschaftshilfegruppen wie Pilze aus dem Boden. Wir
entschlossen uns dazu, bestehende Strukturen zu unterstützen und
zugleich Forderungen zu formulieren, sie auf Plakate zu drucken und in
verschiedenen Städten zu verteilen --- alles flankiert durch einen Blog
(coronasoli.ch).

Ab März 2020 wurden Plakate mit folgendem Inhalt in verschiedenen
Schweizer Städten verteilt:

## Was tun in Zeiten von Corona?

*[Gegenseitige Hilfe!]{.smallcaps} Vernetzen wir uns in unserer
Nachbarschaft. Unterstützen wir Menschen der Risikogruppe und Kranke.*

*[Unterstützen wir Arbeitende im Gesundheitsbereich!]{.smallcaps}
Pandemiezulagen und mehr Lohn. Die Ökonomisierung der Gesundheit
stoppen. Kostenlose Gesundheitsversorgung für alle.*

*[Solidarische Organisation von Care-Arbeit°!]{.smallcaps} Die
zusätzlich anfallende Betreuungsarbeit nicht den Frauen zuschieben.
Männer, übernehmt Haus- und Sorgearbeiten!*

*[Arbeit verweigern!]{.smallcaps} Bleiben wir zuhause und verweigern die
Arbeit, wenn wir keine dringlichen Jobs haben. Vernetzen wir uns und
unterstützen wir uns bei Repression.*

*[Voller Lohnausgleich und Unterstützung für Arbeitende]{.smallcaps},
die von den staatlichen Massnahmen betroffen sind. Helfen wir denen, die
mit finanziellen Einbussen kämpfen. Keine Sanktionen und Massnahmen in
der Sozialhilfe und auf dem RAV.*

*[Aussetzung der Mieten, Zwangsräumungen verhindern!]{.smallcaps}
Öffnung von Leerstand und sichere Unterkünfte für alle. Besonders für
Obdachlose, Geflüchtete und für Betroffene von häuslicher Gewalt.*

*[Öffnung der Ausschaffungsknäste!]{.smallcaps} Evakuierung der
Geflüchteten aus EU-Flüchtlingslagern.*

## Kollektive Verantwortung statt staatliche Zwangsmassnahmen!

*Bauen wir jetzt solidarische Strukturen auf! Bereiten wir uns auf die
Angriffe vor, die im Zuge der ökonomischen Krise auf uns zukommen.*

Die Idee war durch Plakate, Stickers und den Blog eine Präsenz auf den
Strassen zu markieren und eine Debatte anzustossen, um zu versuchen
möglichst viele Leute zusammenzubringen. Denn, so unsere Analyse
dazumal, es war in diesem historischem Moment wichtig zu versuchen,
einen breiten, anti-autoritären Zusammenschluss zu etablieren, anstatt
in kleinen Splittergruppen isoliert zu bleiben. Verschiedene Gruppen und
Einzelpersonen aus unterschiedlichen Orten der Schweiz hatten ähnliche
Schlüsse gezogen und sympathisierten mit den Forderungen. Doch wir
merkten schnell, dass es gesamtgesellschaftlich betrachtet in jenem
Moment keine kollektive und soziale Kraft gab, die in der Lage war,
diese Forderungen durchzusetzen. Die jahrelange Selbstisolation eines
grossen Teils der linksradikalen und anarchistischen Szene und der damit
verbundene selbstreferenzielle Aktivismus wurde deutlicher denn je. Doch
anstatt in Resignation zu verfallen, vertieften sich die Diskussionen
und Debatten. Wohlgemerkt fanden alle Auseinandersetzungen zu jenem
Zeitpunkt online statt, was ungewohnt war und im ersten Moment
entfremdend wirkte. Als ob das Internet in unserem Alltag nicht schon
genug Zeit auffressen würde. Bei mehreren Online-Sitzungen pro Tag
schlägt dieses «Polito-Home-Office» mächtig auf das Gemüt.

Da wir keinen Online-Aktivismus betreiben wollten, suchten wir nach
verschiedenen Formen der Praxis, die vor allem während der Pandemie,
aber auch über sie hinaus, von Bedeutung sein könnten. In unserer
kollektiven Auseinandersetzung schlossen wir an Debatten an, die seit
Jahren in der Deutschschweiz geführt werden: Wie können wir uns stärker
an Klassenauseinandersetzungen beteiligen, wie durchbrechen wir die
subkulturelle Isolation, die einen Grossteil der linksradikalen und
anarchistischen Szene ausmacht? Anstatt mit grossen Parolen dem
Verbalradikalismus° zu verfallen oder utopische Lebensentwürfe zu
skizzieren, waren wir uns einig, dass wir an der Lebensrealität der
Menschen unserer Klasse --- und somit unserer eigenen unmittelbaren
Lebensrealität --- anschliessen sollten. Und da dreht sich nun mal, egal
ob zu Zeiten der Pandemie oder nicht, vieles um die Arbeit. Sie ist der
unmittelbare Ort der ökonomischen Ausbeutung, sie bestimmt wie und mit
wem wir viele Stunden unserer Tage verbringen und ist im hiesigen
Kontext leider meist Terrain sozialpartnerschaftlicher Abkommen anstatt
klassenkämpferischer Auseinandersetzungen. Auch wenn der Arbeitsfrieden
für die Prekarisierten einer Zwangsjacke gleichkommt und die
Gewerkschaften mit ihrer ideologischen «swissness» stolz auf die
Zähmung der Arbeiter\*innen sind, ist die Lage nicht komplett
hoffnungslos. Obwohl die Situation alles andere als rosig war, war eins
klar: Es führt nichts am Kampf in der unmittelbaren Lebensrealität der
Prekarisierten vorbei!

Dies hatten auch andere Gruppen und Einzelpersonen erkannt. In den
letzten Jahren wurde in verschiedenen Städten versucht, eine
klassenkämpferische Praxis auf den Arbeitsplätzen zu etablieren. In
Zürich versuchten beispielsweise Leute in verschiedenen Betrieben eines
der grössten Catering-Unternehmen der Schweiz --- bekannt durch miserable
Arbeitsbedingungen --- zu intervenieren. Die Angst der Arbeiter\*innen
war zu gross, um effektiv Widerstand zu leisten, doch es wurden viele
Informationen gesammelt und dokumentiert und Kontakte geknüpft. Zugleich
bildeten sich die letzten Jahre auch neue Gruppen wie das *Gastra
Kollektiv*, das im Juni 2019 im Zuge des feministischen Streiks
entstand, und Basisgewerkschaften wie die *IWW* und die *FAU Bern*[^3]
erfreuten sich über Mitgliederzuwachs. Es braute sich also seit längerem
etwas zusammen und erst im Zuge der Pandemie und im Rahmen des
Corona-Soli-Projekts entwickelten sich all diese verschiedenen
Erfahrungen, Ideen, Analysen und Praxen zu einer städteübergreifenden
Zusammenarbeit. Inspiriert durch verschiedene Projekte wie z. B. das
*Telefono Rosso* aus Napoli oder das *Solifon Basel* wurde das
*Corona-Solifon* gegründet. Zu den beteiligten Gruppen haben wir bereits
im Mai letzten Jahres in einem Interview mit dem *Ajour Magazin*[^4]
festgehalten:

«Das *Corona-Solifon* ist ein Zusammenschluss verschiedener Basisgruppen
und Basisgewerkschaften. Das *Gastra Kollektiv* besteht aus
FINT-Personen, die in der Gastronomie tätig sind. Sie vereint die Wut
gegen den Sexismus und die Ausbeutung in der Gastronomie. Die *FAU Bern*
und die *IWW* sind (anarcho-)syndikalistische Basisgewerkschaften. Das
*Solnet* ist ein Solidaritätsnetzwerk, welches Lohnabhängige bei
Problemen mit Chefs oder Vermieter\*innen unterstützt und versucht
gemeinsam mit ihnen Perspektiven des Widerstands zu entwickeln.»

Eines der neueren Solifon-Plakate enthielt folgenden Text:

*Die Coronakrise beeinträchtigt den Alltag von uns allen. Wir hören viel
und oft Falsches darüber, die Menschen sind verunsichert, eingesperrt
und ohnmächtig. Wir von Corona-Soli finden es wichtig, dass sich
Arbeiter\*innen in dieser Zeit gegenseitig solidarisch unterstützen.*

*Ist dir unrechtmässig gekündigt worden oder hast du Angst, dass mensch
dir kündigt?*

*Arbeitest du auf Abruf und bekommst keine Arbeit mehr?*

*Weisst du nicht, wie du für deinen Erwerbsausfall Geld beantragen
kannst?*

*Hast du Unregelmässigkeiten in denen Lohnzahlungen?*

*Werden an deinem Arbeitsplatz die Schutzmassnahmen nicht richtig
eingehalten?*

*Musst du viel mehr arbeiten als sonst und wirst dafür nicht bezahlt?*

*Du bist damit nicht alleine. Ruf uns an und wir versuchen dir zu
helfen.*

## Corona-Solifon: Anfänge und erste Schritte[^5]

Der Gedanke, eine arbeitsrechtliche Hotline zu gründen, wird bei den
meisten Leuten aus der linksradikalen und anarchistischen Szene wohl
kaum Gefühle der Euphorie auslösen. Eine am Telefon sitzende vermummte
Gestalt mit einem Gesetzesbuch anstatt einem Stein in der Hand, hat
wenige Chancen das ästhetische Empfinden der Radikalen zu befriedigen.
Ganz im Gegenteil: Eine arbeitsrechtliche Hotline klingt langweilig und
mühsam. Das mag daran liegen, dass mensch sich dabei ein
unübersichtliches bürokratisches Gestrüpp vorstellt, das einem die Haare
zu Berge stehen lässt. Die zusätzliche Aussicht auf innige Stunden mit
allerlei Gesetzesartikeln macht das Ganze nicht besser. Doch die
Geburtsstunde des *Solifons* wies erstaunlich pragmatische Züge auf.
Neben einer Gruppe motivierter Leute, von denen einige arbeitsrechtliche
Erfahrung hatten und andere nicht, brauchte es lediglich Plakate, ein
Handy und ein Schichtplan.

Wohlgemerkt war uns zweierlei klar: 1. Wenn wir Leute wirklich erreichen
wollten, mussten wir von Montag bis Freitag erreichbar sein ---
dementsprechend errichteten wir unseren Schichtplan. 2. Ein Grossteil
derjenigen Menschen, welche die beschissensten Jobs ausführen und die
ökonomischen Konsequenzen der Pandemie am deutlichsten spüren würden,
sind Migrant\*innen. Daher druckten wir die Flyer in circa neun
verschiedenen Sprachen.

Diese wurden auf den Strassen verschiedener Quartiere, in Apotheken, an
Bahnhöfen und in Einkaufszentren aufgelegt, in Briefkästen geworfen und
vereinzelt an Passant\*innen verteilt --- zu jenem Zeitpunkt waren immer
noch genügend Menschen auf der Strasse und an Bahnhöfen anzutreffen.
Unsere Plakate zierten bald Mauern und Anschlagbretter in mehreren
Städten, lokale Radios im Raum Zürich spielten unseren Jingle und Social
Media übernahm den Rest: Die Nachricht der Gratis-Beratungshotline
verbreitete sich schnell und schon in den ersten Wochen nach unserem
Start, Anfang April 2020, klingelte zum ersten Mal das *Solifon*.

## Interne Struktur und kollektive Zusammenarbeit

Da wir keine Räumlichkeiten für die Arbeitsrechtsberatungen hatten,
leiteten wir das Telefon jeweils auf die schichthabende Person um, die
dafür zuständig war, die Telefonate entgegenzunehmen und das erste
Gespräch zu führen. Währenddessen hielten sich andere bereit, um
gegebenenfalls mit Rat zur Seite stehen zu können. In den einfacheren
Fällen konnten bereits beim ersten Anruf die Fragen beantwortet oder
der\*m Anrufer\*in geholfen werden; beispielsweise beim Ausfüllen des
Antrags auf Kurzarbeitsentschädigung (KAE). Dies war jedoch nicht oft
der Fall. In den meisten Fällen war es erst nach viel Recherchearbeit
und nach Rücksprachen mit anderen der Gruppe möglich, ein Vorgehen zu
empfehlen. Dazu richteten wir einen gruppeninternen Support-Chat ein, in
dem offene Fragen gepostet und zeitnah von den anderen beantwortet
wurden. Dieses Ausnutzen der Schwarmintelligenz war von enormer
Bedeutung, denn wir hatten den Anspruch, Menschen kompetent zu beraten
und solidarisch zu unterstützen: Durch den gruppeninternen Support-Chat
konnten wir zeitnah Erfahrungen und Wissen austauschen. Denn einige
Leute hatten einiges an Erfahrungen mit dem Arbeitsrecht, andere sahen
sich zum ersten Mal mit rechtlichen Fragen konfrontiert, während einige
hingegen eher Erfahrung mit Organisierungsprozessen hatten und
vielleicht eher ein Gespür dafür besassen, wann ein Kontakt über eine
rein individuelle Beratung hinausgehen konnte. Und obwohl wir auf diese
Weise viel voneinander lernten und mit jedem im Chat besprochenen «Fall»
auch selbst mehr Selbstvertrauen in die eigenen Beratungsfähigkeiten
fassten; Mut brauchte es allemal, sich für einen Dienst zu melden und am
Telefon auf Anrufe zu warten.

Da viele von uns Mitte April 2020 nicht mehr zur Arbeit mussten oder
konnten, wurde einiges an Ressourcen frei, die für den Aufbau und die
Betreuung der Hotline genutzt werden konnten. So war der Support-Chat
immer eine zuverlässige Echokammer für Fragen in den Beratungen, die
wöchentlich stattfindenden Onlinesitzungen waren gut besucht und es
fanden sich genügend Leute, sowohl für die Telefondienste selbst, wie
auch dafür, Fälle zur Betreuung zu übernehmen --- sei dies weil sie die
Sprache des\*r Anrufenden besser sprachen, mehr Kenntnisse in gewissen
Bereichen mitbrachten oder einfach, damit die Arbeitslast auf mehr
Menschen verteilt wurde. Manchmal bedeutete nämlich die Betreuung einer
Person eine Empfehlung für ein Vorgehen abzugeben und weiter aber auch
die Person über einen längeren Zeitraum hin zu begleiten, Gespräche zu
führen und auf Veränderungen in der Situation reagieren zu können.
Obwohl diese Arbeit viel zeitliche Ressourcen brauchten und oft Stress
verursachten, empfanden viele von uns die teils fast freundschaftlichen
Kontakte mit den Anrufenden als sehr bereichernd. Ebenfalls war und ist
unsere Basisarbeit° dadurch motiviert, Menschen die Erfahrungen zu
ermöglichen, dass es sich lohnt, sich --- im besten Fall kollektiv --- zur
Wehr zu setzen. Gerade Letzteres braucht eine gewisse Vertrauensbasis
und ein (politisches) Verständnis dafür, dass es sich immer lohnt, sich
gegen jene stark zu machen, die sonst immer entscheiden wo's langgeht.
Solche Prozesse sind langwierig und bedürfen viel kommunikative Arbeit,
die wir aber alle bereit waren, zu leisten.

## Wieso das *Solifon* kontaktieren?

In den ersten Wochen beschäftigten viele Anrufende das Thema der
Kurzarbeit und die Unsicherheit darüber, ob sie Anspruch darauf hätten.
Gerade für Arbeiter\*innen im Stundenlohn oder (Schein-)Selbstständige
änderten sich gefühlt täglich die Auflagen, die für den Bezug von KAE
erfüllt sein mussten. Zudem wurden unter dem Vorwand der Pandemie Leute
entlassen, um Firmen zu restrukturieren. Viele standen ohne Lohn da und
wussten nicht wie weiter, sie hatten Konflikte mit der regionalen
Arbeitsvermittlung (RAV) bzw. der Arbeitslosenkasse (ALK). Eine junge
Mutter, die als Poolmitarbeiterin im Hortsystem der Stadt Zürich eine
Anstellung hatte, bekam beispielsweise aufgrund der Situation keine
Stunden mehr, während die Stadt ihr aber nicht kündigte. Gerade in der
Situation als Alleinerziehende und im fortbestehenden Arbeitsverhältnis
mit der Stadt galt sie fürs RAV als «nicht vermittlungsfähig», bekam
keine Arbeitslosengelder und stand ohne nichts da. Sie war nicht
alleine: Je länger der Lockdown anhielt, desto mehr Menschen riefen an,
weil sie ihren Job verloren hatten oder durch die Pandemie in arge
Geldnot geraten waren. Unser wachsendes Netz an Kontakten zu anderen
Gruppen und Initiativen erlaubte uns Menschen weiterzuvermitteln, wenn
deren Anliegen unsere Kompetenzen überstiegen. Zugleich ermöglichte es
uns genügend Informationen zu haben, um zu wissen, wo Menschen in akuter
Geldnot direkt geholfen werden konnte.

Mit dem Fortschreiten der Krise erreichten uns aber auch immer mehr
Fragen zur Kinderbetreuung, da die Kitas und Kinderhorte geschlossen
waren. Andere Anrufer\*innen hingegen hatten Probleme mit ihrer Wohn-
oder Mietsituation, oder mit ihren Chef\*innen, die, um Kosten zu
sparen, nicht genügend Schutzmassnahmen einführten --- an einigen Orten
wurden nicht mal Masken oder Desinfektionsmittel zur Verfügung gestellt.
An anderen Orten hingegen bedeuteten die neuen Sicherheitsmassnahmen
einen Mehraufwand für die Arbeiter\*innen, für den sie jedoch nicht
entlohnt wurden. Ab Mitte Mai studierte eine kleine Gruppe unter uns
diverse Sicherheitskonzepte an Arbeitsplätzen und versuchte anhand der
verzeichneten Anrufe und der Konzepte zu erkennen, was wirklich benötigt
würde, Sicherheit gab und nützlich wäre.

Mit der Zeit wurden die Fälle, die wir selbst bearbeiteten, komplexer
und wir verbrachten mehr Zeit mit den Menschen. Ab Mitte Mai hatten wir
erste Fälle, wo wir Leute vor den\*die Friedensrichter\*in begleiteten,
teils als Unterstützung in den Verhandlungen, teils begleitet von
kleinen Solidaritätsaktionen.

Es gelang uns sogar, kollektive Ansätze mit einigen Anrufenden zu
verfolgen: Wir standen ihnen solidarisch und unterstützend zur Seite und
gaben ihnen Tipps, wie sie am besten vorgehen könnten, um gemeinsame
Forderungen aufzustellen und diese mit möglichst viel Druck an die
Vorgesetzten heranzutragen. In Fällen von Lohnklau, das heisst, wenn
ein\*e Arbeitgeber\*in KAE vom Staat kassiert, aber nicht die ganzen
Beträge an die Arbeiter\*innen ausbezahlt, leuchtete es ein, dass sich
organisieren und kollektives Handeln wichtig ist, da dies alle
Arbeiter\*innen an einem Arbeitsplatz betrifft.

## Selbstkritischer Rückblick

Das *Solifon* bedeutete für viele von uns, der Schockstarre zu
entkommen, in der sich viele Linksradikale und Anarchist\*innen zu
Beginn der Pandemie befanden. Wir alle waren begeistert von der grossen
Solidarität innerhalb der Gruppe, der Verbindlichkeit und dem
kollektiven Tragen von Verantwortung. Die Kommunikation untereinander
funktionierte und die Anfänge des Projekts waren für viele der Gruppe
wohl einer der wenigen motivierenden Aspekte angesichts der sonst
dominierenden Unsicherheit. Die aus Unsicherheit und Pathos geborenen
aber doch für viele als purer Hohn daherkommenden Aktionen, wie das
Beklatschen von Arbeiter\*innen in «systemrelevanten» Berufen durch die
Bevölkerung, waren genauso wenig hoffnungsvoll wie die wenigen Aktionen
aus dem linksradikalen Milieu. Wir suchten vergebens nach Beispielen
einer Praxis, die an der veränderten Lebensrealität der Prekarisierten
anknüpfte und ihre Probleme und Ängste angesichts der Pandemie ernst
nahm. Einige behaupten, das sei reformistische Arbeit, die von der
Sozialdemokratie und dem Staat übernommen werden sollte. So waren es
auch ähnliche Aspekte, die innerhalb der Gruppe Ungeduld oder
Unzufriedenheit hervorriefen, die auch kritische bis negative Stimmen
aus dem linksradikalen und anarchistischen Milieu über unser Projekt
lautwerden liessen. Eine davon ist die Kritik, dass das *Solifon* --- wie
auch viele andere Basisprojekte --- in eine Dienstleistungsfalle gerät.
Das heisst, dass wir gratis eine individuelle Dienstleistung anbieten
würden, für die andernorts bezahlt werden muss und die Arbeit aber
darüber hinaus keine politische Dimension hätte, nicht zu politischer
Aktion oder sonstigen Formen von direktem und vor allem kollektivem
Widerstand führe. Unsere Antwort auf solcherlei Kritik war immer klar:
Das stimmt so nicht. Nur weil wir nicht in der dritten Woche des
*Solifons* bereits einen Streik unterstützen konnten, medienwirksame
Aktionen oder Demos organisiert hatten oder eine Systemanalyse
präsentierten und Forderungen anbrachten, heisst das nicht, dass diese
Arbeit reine Dienstleistungserbringung ist. Für uns war und ist immer
klar: Wir müssen Teil von sozialen Klassenbewegungen sein. Wir müssen
uns mit den Menschen, so unterschiedlich sie sein mögen, ihren Problemen
und Anliegen aus den gelebten proletarisierten Realitäten
auseinandersetzen. Nur so können wir verstehen, was unsere Klasse
ausmacht, wie sie zusammengesetzt ist und im konkreten Fall, wie und wo
sich die Pandemie auf die Menschen auswirkt. Wir denken nicht, dass uns
maximalistischer Verbalradikalismus zu diesem historischen Zeitpunkt
weiterbringt und begrüssen Ansätze, welche die Lebensbedingungen der
Prekarisierten verbessern, auch wenn dadurch der patriarchale
Kapitalismus nicht unmittelbar überwunden wird. Es ist, was die
Organisationsform und kollektive Kampferfahrung unserer Klasse angeht,
ein Unterschied, ob diese Verbesserungen von einer Partei ausgehen oder
selbstorganisiert und in gegenseitiger Hilfe fernab politischer
Repräsentationen geschehen. Da der Staat kein materielles Objekt,
sondern ein soziales Verhältnis ist, dessen Überwindung u. a. an die
kollektive und horizontale Selbstorganisation sämtlicher Lebensbereiche
gekoppelt ist, wird es auf unterschiedlichen Ebenen Strukturen der
gegenseitigen Hilfe brauchen, um den Staat zu zerstören. Diese können
nicht Teil einer ideologisch festgefahrenen subkulturellen Identität
sein, sie müssen an der materiellen Realität der Menschen anschliessen.

Der Vorwurf, wir wären nur Laien, welche die gleiche Arbeit machen
würden, wie Gewerkschaften oder andere professionelle Anlaufstellen oder
Organisationen, nur einfach schlechter, finden wir ebenfalls nicht
gerechtfertigt. Natürlich sind nicht alle unserer Gruppe geschult in
Arbeitsrechtsfragen und es bringen nicht alle die gleiche Sicherheit
mit, wenn es darum geht Empfehlungen zu Fragen abzugeben, wie am besten
vorgegangen werden soll in den Gesprächen mit den Mitarbeiter\*innen.
Dennoch haben wir durchaus sehr erfahrene Leute dabei und die Qualität
unserer Beratungen durch das Schwarmintelligenz ausnutzende Verfahren im
Chat stets gegeben. Wir funktionierten aber im Gegensatz zu den
bekannten Gewerkschaften des Schweizerischen Gewerkschaftsbund (SGB)
aber nicht wie eine Versicherung, wo mensch über eine gewisse Zeit
hinweg zahlendes Mitglied gewesen sein muss, um Hilfe oder Unterstützung
zu bekommen: Wer bei uns anrief, wurde ernst genommen und wir taten, was
wir konnten. Ebenfalls anders als professionellere Anlaufstellen oder
Gewerkschaften machten wir nie eine Abwägung, ob sich das Verfolgen
eines Falls «lohnte», ob die Ressourcen aufgebracht werden konnten
beziehungsweise, ob es nicht strategisch klüger wäre, einen Fall
zugunsten eines vielleicht lukrativeren oder erfolgversprechenderen
fallen zu lassen. Es hat sich gezeigt, dass dies wirklich inklusiv ist,
dass wir Ungelernte, Temporäre, migrantische oder andere in den
Systemgewerkschaften untervertretene Gruppen genauso abholen und
unterstützen konnten, wie Facharbeiter\*innen oder Leute in Branchen
oder Situationen, die typischerweise vielleicht nicht so
gewerkschaftsnah sind.

Natürlich war beim *Solifon* nicht alles rosig und wir sind nur ein
kleines Beispiel einer kollektiven Selbstorganisation. Obwohl von der
Organisation des *Solifons* gerade am Anfang und über lange Zeit
ziemlich viel sehr gut funktionierte, trat nach einigen Monaten auch ein
wenig Frust auf. Wir hatten gehofft, dass die Leute, die uns
kontaktierten, nicht bloss an einer arbeitsrechtlichen Dienstleistung
interessiert sind, sondern auch längerfristig mit uns zusammenarbeiten
möchten und sich zum Beispiel selbst am Arbeitsplatz organisieren
würden. Leider war das relativ selten der Fall, obwohl die Leute
wussten, dass viele in ähnlichen Situationen waren wie sie selbst. In
vielen Fällen bestand bei den Anrufenden wenig Verständnis dafür, wieso
wir ihnen einfach so, ganz ohne Bezahlung halfen und entsprechend war
auch ihr Interesse an kollektiven Aktionsformen gering bis nicht
vorhanden. Das führte zu Frust und Ermüdung und wir hatten auch teils
schwierige Diskussionen innerhalb der Gruppe. Hinzu kam eine gewisse
Erschöpfung unsererseits, denn Basisarbeit frisst extrem viel Zeit. Wir
mussten viel lesen, recherchieren, diskutieren und dann noch mit den
betroffenen Menschen alles besprechen. Das geht mit einer grossen
Verantwortung einher, denn die Lebensbiographien und Lage all der
verschiedenen Menschen gingen uns sehr nahe und wir wollten auf keinen
Fall Fehler machen und ihre Situation verschlechtern.

Wir haben versucht in die politischen, sozialen und ökonomischen
Verwerfungen der Pandemie einzugreifen --- manchmal mit Erfolg und sehr
bereichernden Erlebnissen, manchmal blieb nur Erschöpfung übrig.
Insgesamt haben wir viel gelernt, Kontakte geknüpft und
städteübergreifende Netzwerke gestärkt. Zudem hat sich u. a. durch das
*Solifon* und die Arbeitsrechtsberatungen von *Solnet*, die Gruppe
*Zürich Solidarisch* konstituiert, die nun in Zürich im Treffpunkt für
Armutsbetroffene *Kafi Klick* Arbeitsrechtsberatungen anbietet, Abende
zum Austausch und solidarischer Vernetzung organisiert oder Workshops zu
immer wiederkehrenden Fragen durchführt, wie aktuell der Problematik von
Temporärarbeit. Leute vom *Solifon* sind bei diesem Projekt ebenfalls
dabei und können viel von den beim *Solifon* gemachten Erfahrungen
nutzen, seien sie im rechtlichen Bereich oder in der Begleitung von
Menschen in Auseinandersetzungen mit den kapitalistischen Widersprüchen.
Insgesamt können wir ganz lapidar festhalten: «Es lohnt sich aufs Tor zu
schiessen, denn auch wenn mensch vielleicht zwanzig Mal danebenschiesst,
so gibt's doch auch manchmal ein Treffer.»


[^1]: Damit ist eine anti-autoritär kommunistische Theoriegruppe
    gemeint.

[^2]: Die *Industrial Workers of the World (IWW)* ist eine von
    Arbeiter\*innen geführte, radikale, internationale
    Basisgewerkschaft, die 1905 in den USA gegründet wurde. In der
    Schweiz gibt es aktive Gruppen in verschiedenen Städten, in denen
    die Mitglieder branchenübergreifend organisiert sind. Es sind
    verschiedene theoretische Strömungen in der (historischen) IWW
    vertreten, am stärksten ist ein syndikalistisches Verständnis von
    Gewerkschaft verbreitet. Das Ziel der IWW ist es, das
    Lohnarbeitssystem abzuschaffen. Sie tut dies über Organisierung und
    direkte Aktion am Arbeitsplatz aber auch in der Nachbarschaft, in
    Mietshäusern oder Gefängnissen. Es geht darum, selbstbestimmt und
    kollektiv handeln zu lernen, um die neue Gesellschaft in der Schale
    der alten aufzubauen.

[^3]: Die Freie Arbeiter\*innen Union Bern (FAU Bern) ist eine
    anarchosyndikalistische Gewerkschaft im Raum Bern. Sie ist ein
    Ortssyndikat der [FAU Schweiz](https://faunion.ch/). Die FAU
    versteht sich als basisdemokratische und kämpferische Alternative zu
    den sozialpartnerschaftlichen Gewerkschaften.

[^4]: Das *Ajour Magazin* ist ein selbstorganisiertes Onlinemagazin aus
    der anarchistischen und kommunistischen Bewegung in der Schweiz.

[^5]: Das Solifon wurde anfangs Dezember 2021 eingestellt.
