---
author: "Anonym"
title: "Wenn sich Raum a u s d e h n t"
subtitle: "Mixité choisie"
info: "Im August 2020 für den französischen Sammelband verfasst"
datepub: "Im Herbst 2021 auf Deutsch übersetzt"
categories: ["Selbstorganisation, kollektives Experimentieren", "Selbstreflexion zum Aktivismus"]
tags: ["Bresche", "schreien", "Werkzeug", "Stille"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "27"
quote: "es ist die Aufregung, gemeinsam zu existieren"
---

*\[*Mixité choisie°: *Für den Begriff gibt es auf Deutsch kein exaktes
Äquivalent, weshalb hier der französische Begriff beibehalten wird.*
Mixité choisie bezeichnet *eine Form der Organisation, bei der Menschen
zusammenkommen, die einer oder mehreren unterdrückten oder
diskriminierten sozialen Gruppen angehören. Um keine Muster sozialer
Dominanz zu reproduzieren, werden Menschen ausgeschlossen, die einer
Gruppe angehören, die als diskriminierend oder unterdrückend betrachtet
wird. Diese Praxis wird von Gruppen ausgeübt, die sich einer bestimmten
Bewegung, insbesondere dem Feminismus, dem Antirassismus, LGBTQIA+°,
Gender-Minoritäten oder Personen mit einem Handicap zuordnen.*
*Auf Deutsch entspricht dem Begriff am ehesten dem Konzept von
FLINTA\*-Räumen. FLINTA\*° steht für **F**rauen (meist CIS-Frauen),
**L**esben, **I**nter Menschen, **N**icht-binäre Menschen,
**T**ransmenschen°, **A**gender; der Stern \* steht für alle, die sich
in der Bezeichnung in keinem der Buchstaben wiederfinden.*

*Im Gegensatz zu* mixité choisie*, macht der Begriff aber nur eine Art
von diskriminierten Gruppen, nämlich jene, die sich über den
Genderbegriff definieren, sichtbar.\]*

 

Wenn wir uns in einer *mixité choisie*-Gruppe treffen, schaffen wir ganz
plötzlich Raum.

Der Raum, der dabei entsteht und den wir einnehmen können, ist dehnbar
und unendlich.

 

{{%centre%}}
g   e   r   ä   u   m   i   g
{{%/centre%}}

 

Ich habe die Erfahrung gemacht, dass manchmal Schweigen eintritt.

\[   \]

<span style="margin-left: 2em;"></span>\[   \]

<span style="margin-left: 4em;"></span>\[   \]

Und dieses Schweigen

<span style="margin-left: 6em;"></span>\[   \]

<span style="margin-left: 8em;"></span>\[   \]

ergibt Leere.

<span style="margin-left: 10em;"></span>\[   \]

Und Leere kann gefüllt werden,

Leere schafft Möglichkeiten,

es ist Raum, der eingenommen werden kann,

es ist wie Brachland,

wie eine Bresche.

 

Manchmal brechen wir das Schweigen,

wir sind euphorisch;

überall und ständig

erlauben wir uns das nicht.

 

Es ist die Aufregung, gemeinsam zu existieren.

Es ist die Freude,

systemische° Diskriminierungen abzulehnen,

sie zurückzuweisen,

und zwar jederzeit.

 

Manchmal liegt Magie in der Luft,

die Dinge werden kristallklar,

fallen wie Schuppen von den Augen,

sind plötzlich ganz logisch.

Und das, nachdem sie doch

so schwierig auszudrücken,

so schwierig zu erklären waren.

Das war aber nicht hier,

sondern woanders,

dort, wo die Raumaufteilung

sehr eng ist.

 

Manchmal ist es intensiv,

wir weinen.

Ganz warme

und tröstende

 

T

r

ä

n

e

n

kullern dann

von unseren

abgenützten

<div><span aria-hidden="true" style="visibility: hidden;">abgenützten</span>T</div>

<div><span aria-hidden="true" style="visibility: hidden;">abgenützten</span>r</div>

<div><span aria-hidden="true" style="visibility: hidden;">abgenützten</span>ä</div>

<div><span aria-hidden="true" style="visibility: hidden;">abgenützten</span>n</div>

<div><span aria-hidden="true" style="visibility: hidden;">abgenützten</span>e</div>

<div><span aria-hidden="true" style="visibility: hidden;">abgenützten</span>n</div>

<div><span aria-hidden="true" style="visibility: hidden;">abgenützten</span>d</div>

<div><span aria-hidden="true" style="visibility: hidden;">abgenützten</span>r</div>

<div><span aria-hidden="true" style="visibility: hidden;">abgen</span>ü</div>

<div><span aria-hidden="true" style="visibility: hidden;">abgen</span>s</div>

<div><span aria-hidden="true" style="visibility: hidden;">abgen</span>e</div>

<div><span aria-hidden="true" style="visibility: hidden;">abgen</span>n.</div>

 

*Mixité choisie*-Räume sind

kein Gesellschaftsprojekt,

sondern ein Werkzeug,

eine Strategie,

Erholung --- für uns.

 

Sich in einer *mixité choisie*-Gruppe auszutauschen,

heisst auch, sich seiner eigenen Sozialisierung° bewusst zu werden.

Wir sind gekommen, um uns anders zu sozialisieren[^9],

uns mit neuen Worten auszudrücken,

in einem anderen Tonfall,

um menschliche Beziehungen neu zu (er-)leben,

um die sozialen Strukturen einer hoffnungsvollen Zukunft aufzubauen,

um uns gegenseitig zu helfen,

um uns gegenseitig in die Augen zu schauen, mit Freude und Neugier,

sich zu offenbaren und *neu* zu entdecken,

um etwas wiedergutzumachen ,

um zu lernen, einander zu lieben,

weil uns Misstrauen

gegenüber den Anderen

beigebracht wurde,

weil uns eingetrichtert wurde,

wir seien weniger gut,

weniger cool

weniger stark

als die Anderen.

 

Manchmal ist einem nicht wohl,

manchmal fühlen wir uns schwach.

Dies einzugestehen, ist nicht einfach.

 

Es fällt nicht leicht, sich einzugestehen,

dass in einer 15-köpfigen Gruppe,

ohne einen einzigen Cis-Mann°,

gewisse Kompetenzen fehlen.

Kompetenzen, die uns nicht beigebracht wurden.

Kompetenzen, die uns verweigert wurden.

 

Es fällt nicht leicht, sich einzugestehen,

dass in einer 15-köpfigen Gruppe

ohne einen einzigen Weissen Menschen,

gewisse Ressourcen fehlen.

Der Zugang zu Ressourcen wurde uns verwehrt,

sie wurden uns weggenommen.

 

Doch wir erfinden unsere eigenen Lösungen,

wir lernen,

über die Ökologie unserer Fähigkeiten,

wir lernen,

ganz bewusst,

beginnen wieder von Neuem,

begeben uns auf die Suche,

und stehlen Ideen.

 

Manchmal organisieren wir uns,

holen zum Gegenschlag aus,

werden aktiv.

Es geht schneller,

leider,

wenn wir in einer *mixité choisie*-Gruppe sind.

Freudentaumelnd SCHREIEN wir dann

 

{{%centre%}}
<span style="word-break: break-all;">direkteshandelnohnelangediskussionenundfragensowieerklärungenschnelleshandeln</span>
{{%/centre%}}

 

{{%centre%}}
DAS IST

{{%cadre%}}
SELBST-
{{%/cadre%}}

EMANZIPATION
{{%/centre%}}

 

{{%centre%}}
VON UNS

FÜR UNS
{{%/centre%}}

 

{{%centre%}}
EINE

KÄMPFERISCHE

UND MIT LIEBE ERFÜLLTE

WAFFE
{{%/centre%}}

[^9]: Einige Ideen, um sich in einem Kollektiv selbstbestimmt und
    alternativ zu organisieren, gibt der Text *Lernen, sich selbst zu
    organisieren* \[n. 21\].
