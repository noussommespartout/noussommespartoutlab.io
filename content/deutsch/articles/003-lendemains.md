---
author: "Elena & Rina"
title: "Die Tage danach"
subtitle: "Wie wir die Stadt nach einer Demo erleben"
info: "Im September 2020 für den französischen Sammelband verfasst"
datepub: "Im Herbst 2021 auf Deutsch übersetzt"
categories: ["Demonstration, Riot", "Selbstreflexion zum Aktivismus"]
tags: ["Liebe, Verliebtheit", "Körper", "schreien", "Wasser", "Empathie", "Kraft", "Bahnhof", "Horde", "mcdo", "Strasse", "Tee, Teesorte", "Fabrik", "Stimme"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "3"
quote: "Auf allen Mauern herumgeistern"
---

Heute kehrt wieder der Alltag ein, der Verkehr ist zurück, wir bewegen
uns in den dafür vorgesehenen Zonen.

Stopp bei Rot, Abmarsch bei Grün.

Auf den gelben, parallelen Streifen bleiben.

Jedem Augenkontakt ausweichen.

Seinen eigenen Weg gehen, wie in einer Fabrikstadt, funktional und
ergonomisch.

Merkwürdig. Ich habe das Gefühl, ich sei im Ausland oder in einem Traum
-- "meine" Stadt erkenne ich überhaupt nicht wieder. Das wirkt sich auch
auf meinen Körper aus. Ich fühle mich einsam. Weshalb wird mir schnell
klar.

Heute ist mein Körper in all seinen Facetten überrascht, dass er den
*Place de la Riponne* schnörkellos, auf einer Diagonale überquert -- es
ist die alltägliche Diagonale, die zur Arbeit führt. Eine effiziente
Diagonale: so wenig Schritte wie nötig, so schnell wie möglich. Ich
komme in Verlockung, einen anderen -- chaotischeren -- Weg zu nehmen.
Doch alleine ergibt das Chaos keinen Sinn mehr.

Beim Verlassen des Platzes sehe ich eine violette Schablone unter einer
Sitzbank. Es ist alleine. Wie ich. Das macht mich ein wenig traurig,
doch es beruhigt mich auch. Dann erreiche ich den Bahnhof. Dort, wo ich
gestern Teil einer Truppe, einer Horde, eines Bienenschwarms ohne
Königin war: Eine der schönsten Demonstrationen meines Lebens, die erste
in dieser Grösse. Die Raumkonfiguration, die Verkehrsregelung, alles hat
sich verändert. Vor ein paar Stunden gehörte die Strasse, der Bahnhof
uns -- die ganze Stadt gehörte uns. An diesen Ort zurückzukehren, um zur
Arbeit zu gehen, ist wie wenn mensch enteignet wird, wie eine
Achterbahnfahrt in einem abgefuckten Lunapark.

Seit diesem Tag ist mir das oft passiert. Die mentale Karte der Stadt,
in der ich lebe, mutiert. Alles aktivistische Handeln verändert meine
Beziehung zu gewissen Strassen, zu bestimmten Schaufenstern bestimmter
Läden, zu bestimmten Kreuzungen. Jedes Mal wenn ich inmitten einer Horde
bin, nehme ich mir ein Stück Stadt, um es später wieder loszulassen. Das
ist doch paradox: Je mehr mir die Stadt gehört, desto schwieriger ist
die Enteignung am Tag danach zu ertragen.

Jeder Raum, der noch vor einigen Stunden kollektives Eigentum war, also
ein echter öffentlicher Raum, wird wieder privatisiert und zerstückelt:
einige Quadratmeter für starbucks, für den mac, einige Quadratmeter dem
coop, einige für h&m und einige Quadratmeter für ein Unternehmen, das
mit Rohstoffen spekuliert und dessen Namen wir nicht kennen. Der Rest
gehört dem Staat, seiner Überwachung, seinen Zäunen, seinen "Einfahrt
verboten"-Schildern, seinen polizeiüberwachten Räumen, die nur einem
Teil der Bevölkerung offenstehen, jenem Teil, der *in Ordnung* ist und
wartet, bis ihm von der Demokratie mehr Rechte verliehen wird. Eine
Demokratie, die von Lobbys beherrscht wird und die zuvor erwähnten
Quadratmeter schützt. Da möchte ich nur sagen: Geschenkt ist geschenkt,
wiederholen ist gestohlen.

Tags zuvor waren wir eine Masse mit einem gemeinsamen Ziel. Gemeinsam
skandierten wir Parolen. Slogans, die Rechte einfordern, die uns nicht
zwingend betreffen, aber eine neue Gesellschaft für uns alle fordern.
Das Kollektiv steht über der Einzelperson, über deinen persönlichen
Interessen. In einer solchen sozialen und politischen Konstellation,
öffnen sich Freiräume, Freiheiten. Du kannst die Schaufenster von Banken
besprayen. Banken, welche die Rechte von Privatunternehmen verteidigen
und für sich das Recht auf Ökozid° und Prekarisierung° beanspruchen --
und dabei erst noch unbestraft bleiben wollen. Du kannst Autobahnen
blockieren, um auf häusliche und geschlechtsspezifische Gewalt
hinzuweisen.

An den Tagen danach bewegst du dich während den Stosszeiten inmitten
einer Masse von isolierten Individuen, die sich um ihre persönlichen
Angelegenheiten kümmern. Eine Masse von Menschen, die sich
schlussendlich selbst kontrolliert: Du darfst nicht mehr schreien,
sprayen und von Multimilliardär\*innen fordern, dass sie teilen und
ihren marktwirtschaftlichen Genozid endlich stoppen sollen.

Wenn wir Bilanz ziehen, dann wird klar, dass wir nicht viele sind, die
diese Eindrücke teilen. Wir sind nur ein paar wenige, die schreien und
Häuser zusammen mit Menschen, die kein Dach über dem Kopf haben,
besetzen. So entsteht ein Graben zwischen deiner Gruppe und dem Rest der
Welt, den du so ganz und gar nicht verstehen kannst und der überhaupt
keinen Sinn in deinen Augen ergibt. Du weisst, dass dein räumliches
Vorstellungsvermögen ganz eigenwillig ist. Du möchtest es mit all den
Passant\*innen teilen, doch das ist nicht möglich. Zu gut wurden wir
erzogen.

So gehst du halt einfach weiter. Vor dem Bahnhof wurden deine
Genoss\*innen verhaftet, vor diesem Polizeikommissariat haben wir auf
sie gewartet und sie nach ihrer Entlassung herzlich in Empfang genommen;
auf diesem Platz habe ich oben ohne getanzt; hinter diesem Strauch habe
ich hastig eine Spraydose und eine Schablone verschwinden lassen, als
plötzlich Bullen auftauchten; in dieser Bar habe ich während einer
Blockade im Winter einen warmen Tee getrunken; in dieser Strasse habe
ich einsatzbereite Bullen gezählt, um meine Genoss\*innen, die eine
Aktion vorbereiteten, vorzuwarnen; in einer dieser Strassen habe ich
meine Stimme verloren, so laut hatte ich geschrien; dieses leerstehende
Gebäude haben wir zu besetzen versucht -- in den Toiletten im obersten
Stock gibt es fliessend Wasser.

In diesem Strässchen habe ich gelesen:

*NI*

*UNA*

*MENOS*

in A4-grossen, roten Grossbuchstaben auf weissem Papier, hastig auf eine
Mauer geklebt, nur zwanzig Meter vom Polizeikommissariat entfernt. Diese
Collage füllt mich mit Kummer und gleichzeitig mit Stärke. Sie lässt
wutentbrannte Tränen fliessen, für all jene Menschen, die ich liebe und
all jene, die ich nicht kenne.

Am liebsten würde ich alle Fassaden der Stadt bemalen. Auf allen Mauern
herumgeistern, bis mensch nirgendwohin mehr schauen kann. Ein Anderswo
wird es nicht mehr geben, denn wir werden überall sein. Ich wünsche mir,
dass alle Strassen -- als ein Werk der Liebe -- geplündert werden. Ich
träume von einem allumfassenden Akt der Empathie und einem gemeinsamen
Aufstand.
