---
author: "Ding"
title: "Dingsda, Dingsbums und Ding auf dem Bauernhof"
subtitle: "Bericht von einer antispeziesistischen Aktion im hohen Gras"
info: "Im November 2020 für den französischen Sammelband verfasst"
datepub: "Im Herbst 2021 auf Deutsch übersetzt"
categories: ["Antispeziesismus", "Sabotage, direkte Aktion"]
tags: ["kleben", "Körper", "Mut", "Wasser", "Kraft", "Internet, Web", "Scheisse", "Nacht, Dunkelheit", "Wort", "Macht", "Stille", "Telefon", "Auto"]
langtxt: ["de", "fr"]
zone: "Suisse romande"
num: "29"
quote: "im Schatten der Schlachthäuser"
---

Über die Kopfhörer, die rauschen, höre ich wie Dingsda am Herumnörgeln
ist. Die Kaninchen sind sehr schwer und nicht besonders erfreut, dass
sie von so netten Veganer\*innen wie wir es sind, mitgenommen werden.
Dingsda beruhigt sie mit sanfter Stimme, doch das scheint nicht zu
funktionieren. Es müssen drei oder vier, der hier wie Sardinen in der
Dose eingepferchten Kaninchen ausgewählt werden. Ich bin froh, muss ich
es nicht tun. Dingsda nörgelt immer heftiger.

Ich habe mich im hohen Gras versteckt. Es ist das erste Mal, dass ich so
etwas mache. Weil ich die Dunkelheit unter die Lupe nehme --- ich spähe,
ob niemand auftaucht ---, fange ich an, die Augen zuzukneifen. Dringend
pinkeln sollte ich übrigens auch. Weil wir die Walkie-Talkies vergessen
haben, passe ich mithilfe eines gesicherten Telefons auf, was alles
andere als optimal ist.

Ich höre Dingsbums atmen. Nimmermüde erwähnt er\*sie, wie ekelhaft und
klein die Kaninchenställe sind. Das glaube ich aufs Wort. Ich rieche es
ja sogar von Weitem. Die Luft stinkt wie Scheisse, verrottetes Fleisch
und nasses Heu. Überall hat es Insekten.

Ich höre, wie geflucht wird. Dingsda ist ein Kaninchen entwischt,
er\*sie versucht mithilfe einer Infrarot-Lampe, das Kaninchen in der
Dunkelheit wiederzufinden. Es ist Juni. Hinter unseren Sturmmasken,
Jacken und Kapuzenpullis kommen wir vor Hitze fast um. Dingsbums
schnauft wie ein Walross ins Headset. Dann wird es still. Erst so
realisiere ich, dass wir eigentlich drei Schwachköpfe sind, die im
entlegensten Winkel irgendwo auf dem Land damit beschäftigt sind, vier
arme Kaninchen von einem nicht besonders liebenswerten Typen zu retten.
Das macht mir ein wenig Angst. Dann fragt mich Dingsbums, ob die Luft
rein sei. Ich kneife die Augen noch mehr zusammen, um ganz sicher zu
sein, dass auch ja kein Viehzüchter auf der Lauer liegt. Ich sehe
keinen. Dingsda trippelt mehr schlecht als recht in meine Richtung, den
Sack voller Kaninchen. Dingsbums folgt dicht dahinter mit den Lampen und
dem restlichen Zeug. Wir kehren zum Auto zurück. Es kommt mir vor, als
hätten wir dieses ganz weit weg parkiert. Wir verlassen das Gelände,
unsere Scheinwerfer sind ausgeschaltet. Erst als mir das Herz nicht mehr
bis zum Hals klopft und ich wieder normal zu atmen beginne, werfe ich
einen Blick auf diese Kaninchen. Ihre Körper sind riesig. So gross, dass
mensch meinen könnte, es seien Hunde.

Dass die Tiere so unverhältnismässig gross sind, überrascht mich immer
wieder, wenn wir Tiere retten oder wir in Viehzuchten Aufnahmen machen.
Sie sind aufgeblasen und mensch könnte meinen, ihre Haut würde gleich
platzen wie eine überreife Frucht. Oft verletzen sie sich an den
metallenen Käfigen. Oft trampeln sie aufeinander herum. Oft essen sie
sich gegenseitig. Das täten wir wahrscheinlich auch, wenn wir in ihrer
Haut stecken würden. Ich kann überhaupt nicht nachvollziehen, dass es
Menschen gibt, die es für normal und natürlich halten, einem Ding so
etwas anzutun --- denn so ein Ding ist ja lebendig und empfindet, das ist
doch klar! Bilder, die uns diese Zustände vor Augen führen, sind dank
meinen Genoss\*innen im Internet abrufbar --- tippe "Viehzucht Schweiz"
oder "Schlachthöfe Schweiz" ein.

Ein anderes Mal spionierten wir einen Milchvieh- und Schweinebetrieb
aus, um später dort zu filmen. Wie so oft blieb ich im Hinterhalt, im
hohen Gras, wie eine Kröte mit Kapuze. Ich kniff die Augen im
Dämmerlicht zu, während die anderen in den abgeschlossenen Bereich des
Betriebs eindrangen. Ich war einzig und alleine da, um zu überprüfen,
dass mensch von aussen kein Licht sieht. Über die Kopfhörer bekam ich
mit, wie Dingsda und Dingsbums feststellten, was sie fast immer
feststellten: sich selbst zersetzende Tierkadaver unter Blachen,
Gestank, neugeborene Schweinchen beim Sterben, Wunden und Abszesse auf
den Füssen und Bäuchen. Ein Bild zum Kotzen.

Von etwas weiter weg, aus einem anderen Gebäude, hörten sie später ein
Brüllen, das hinter riesengrossen Strohballen hervorkam. Erst schwiegen
sie lange. Dann hörte ich sie etwas murmeln, konnte ihre Worte aber
nicht verstehen. Was sie in diesem Moment entdeckten, erfuhr ich erst
später: Der Viehzüchter hatte hinter den Strohballen drei junge Kälber
eingesperrt. Sie hatten weder Wasser noch Nahrung. Offenbar wollte er
sie verstecken. Er überliess sie ganz einfach sich selbst... oder anders
ausgedrückt: dem Tod. Nicht rentabel und zu teuer, um zu schlachten. Wir
entschieden uns, Wasser zu holen. Und weil der Zustand der Kälber ein
Handeln erforderte, haben wir uns entschieden, den Züchter beim
kantonalen Veterinärdienst anzuzeigen. Ich glaube, die Kälber sind
schlussendlich wohl nicht verdurstet, wahrscheinlich wurden sie
geschlachtet --- was für ein Erfolg.

Das Problem ist natürlich nicht gelöst, indem drei Kaninchen gerettet
werden und in Viehzuchtbetrieben gefilmt wird --- am zugrundeliegenden
Problem ändert dies nichts. Genauso wenig wird das Problem der
Viehzüchter\*innen gelöst, die es nicht einfach haben. Dessen bin ich
mir bewusst. Manchmal ändert sich einfach nichts --- so zumindest mein
Eindruck. Doch ich bin einverstanden, und das schätze ich auch, wenn
Menschen der Ansicht sind, dass jedes gerettete Tier ein kleiner Erfolg
ist. Und genau das gibt doch Mut.

Natürlich erscheint das vielen lächerlich, dass wir uns um das Los von
Tieren kümmern. Es gibt sogar Aktivist\*innen, die in anderen Bereichen
für andere Rechte kämpfen und finden, dass Antispeziesismus etwas für
wohlhabende Bürger\*innen ist.

Da ist schon was Wahres dran. Es gibt Veganer\*innen, die sich nicht um
Menschenrechte kümmern, und wenn ich das so sagen kann, nicht erkennen,
dass es ein Glück ist, sich für andere Arten als die ihrige einsetzen zu
können. Ich kenne Veganer\*innen, die nicht verstehen, dass es mit
Rechten und Privilegien zusammenhängt, dass sie sich für
nicht-menschliche Lebewesen engagieren können. Aber sicher ist, dass wir
unsere Kämpfe zusammenführen müssen. Nur so sind wir eine grosse Masse
und kommen auf einen grünen Zweig.

Bis es aber soweit ist, wache ich, die Augen zusammengekniffen, gerne im
Gras mit Dingsda und Dingsbums und vielen anderen, die dies --- im
Schatten der Schlachthäuser, der Viehzuchten und der Transportwagen ---
genauso oder noch besser machen als wir.
