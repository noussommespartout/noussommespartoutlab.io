---
author: "El."
title: "Gewöhnliche Piraterie"
subtitle: "Ein paar Ideen für kreative Sabotage"
info: "Im April 2020 für den französischen Sammelband verfasst"
datepub: "Im Herbst 2021 auf Deutsch übersetzt"
categories: ["DIY", "Sabotage, direkte Aktion"]
tags: ["Alkohol", "Fressen", "kleben", "Samen", "Internet, Web", "Logo", "Nacht, Dunkelheit", "lachen, trinken"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "38"
quote: "vive le vent, vive le vent, vive le vandalisme"
---

Ideologien waren nie etwas für mich. Es sind Inseln, anhand derer sich
mensch in einem Meer von Möglichkeiten zurechtfinden und orientieren
kann. Ideologien setzen aber auch Grenzen und legen fest, welche Wörter
in welchem Kontext welche Bedeutung haben. Oder anders ausgedrückt: Was
gesagt werden darf und was nicht. Ideologien helfen uns, zu denken und
uns mit Worten auszudrücken. Doch wenn wir sie zu Ende denken, verorten
wir doch irgendwie auch immer das, was wir denken. Denn die Grenzen, die
wir uns setzen, die wir als richtig erachten und mit der wir die Welt
beschreiben, sind immer auch sprachliche Grenzen.

Deshalb bin ich viel lieber lokal aktiv. Ich setze mich dafür ein, um
vom Gewöhnlichen abzulenken, die uns unterdrückenden Verhältnisse zu
stören, schlicht einfach ein Gegengewicht zur herrschenden Ordnung zu
schaffen und aufzubauen. Handeln ist nicht viel anders als denken: Jede
Aktion muss auch verankert bzw. situiert werden können. Ob die Aktion
Sinn macht, hängt einzig allein von deiner Denk- und Sichtweise ab. Wenn
du einfach eine Nummer bist, eine Stellenprozentzahl, die vage einen Job
sucht, eine Figur im grossen Todesspiel des totalitären Warenhandels,
dann ist es höchste Zeit, als Pirat\*in aktiv zu werden und das
Pirat\*innendasein mit anderen zu teilen, tagtäglich. Wir sollten einmal
eine Liste mit all den kleinen (Alltags-)Gesten machen, die den *courant
normal* stören, damit die Ideologie ins Wanken kommt und ihre Grundwerte
zu bröckeln beginnen. Was dann passiert, das werden wir sehen. Wir
werden gemeinsam Neues erfinden, denn zur Gestaltung unserer Leben
brauchen wir doch keine Hilfe. Wenn dich der Raum, in dem du dich
bewegst, als Nutzer\*in betrachtet, dann wirst du von ihm dominiert.
Dann befindest du dich im grossen Netz --- jedoch ohne Messer ---, um
dieses zu zerschneiden, dich zu befreien und kreativ zu werden.

*Vive le vent, vive le vent, vive le vandalisme ---* ein Lob dem
Vandalismus! Die nachfolgende Liste besteht teils aus geklauten Ideen,
teils aber handelt es sich auch um wahrhafte Geistesblitze. Doch seien
wir ehrlich, wir finden nur selten etwas Neues, meistens reproduzieren
wir Handlungen und Strategien, deren Existenz wir womöglich ignoriert
haben. Um das grosse Ganze im Kleinen zu sabotieren, kannst du...

... einen Monatskalender machen, der Events auflistet, wo es überall
Gratis-Essen gibt (Vernissagen, Konferenzen, Theater-Erstaufführungen,
usw.). Und nicht vergessen: Die Termine unbedingt mit deinen
Freund\*innen teilen.

... per Autostopp unterwegs sein.

... in öffentlichen Parkanlagen einen Gemüsegarten bzw. Gemüsebeete
anlegen.

... deine eigenen Zebra- und Velostreifen mit Klebeband und Malerfarbe
zeichnen.

... mehrere Gesichter auf deine T-Shirts drucken, um die
Gesichtserkennung aufs Kreuz zu nehmen.

... die Slogans von Werbeplakaten kreativ verändern.

... mit einer Schablone das "Handicap-Logo" auf alle Parkplätze eines
Supermarktes sprayen bzw. malen.

... mit dem Luftgewehr die Überwachungskameras deiner Stadt ausschalten.

... die frankierten Umschläge, denen Werbeprospekte beiliegen, dem\*r
Absender\*in mit zusätzlichem Werbematerial zurückschicken.

... mit ein wenig Werkzeug und einer Blache eine öffentliche Sitzbank in
einen kostenlosen Schlafplatz verwandeln.

... entlang von Trottoirs, z. B. in alten Pneus, Abfallbehältern oder
recycelten Paletten, *guerilla gardening* betreiben.

... Lebensmittelfarbe in öffentliche Brunnen giessen, um Kinder und
Passant\*innen ein Lachen auf die Lippen zu zaubern.

... an Selbstbedienungskassen in Supermärkten nur einen Teil der
Produkte scannen, die du einkaufst.

... starkwüchsige Pflanzen bzw. ihre Samen (wie Kuzu) in öffentlichen
Parkanlagen und entlang unbewohnter Wohnhäuser ausbringen, um die
Revitalisierung der Städte voranzutreiben.

... Polizei- oder Baustellenfahrzeuge gebrauchsunfähig machen, indem du
eine Kartoffel in den Auspuff stopfst.

... ein Schloss blockieren, indem du mithilfe einer Spritze, ein Gemisch
aus Epoxy-Kleber und Alkohol hineinspritzt.

... in Abfallcontainern von grossen Supermärkten nach "Müll" tauchen und
mit dem "gefundenen Fressen" einen Gratis-Stand auf einem Trottoir
machen.[^10]

... die Strichcodes von elektrischen Trottinetten mit einem
Schraubenzieher oder Klebeband ruinieren.

... *SkipAd*-Stickers auf Werbeplakate kleben.

... in Nacht- und Nebelaktionen auf Baustellen aus Sand und Kies
Schlösser und Skulpturen formen --- die Arbeiter\*innen, welche die
Kunstwerke am nächsten Tag entdecken, werden sich freuen!

... deinen Nachbar\*innen gratis deinen Wifi-Code anbieten.

... alte Teppiche über Stacheldrahtzäune legen, um sie ohne Risiko
übersteigen zu können.

... Taubenabwehr-Spikes mit Stoff umwickeln, damit Vögel wieder landen
können.

... unter Brücken, Bäumen oder in Parkhäusern Schaukeln installieren.

... dir Polizeihunde vom Leib halten, indem Cayennepfeffer zur Anwendung
kommt, der den Geruchssinn der Hunde für kurze Zeit lahmlegt (weh tut
ihnen das nicht!).

... mit Samen, Erde und Lehm Samenbomben herstellen und so die Stadt
begrünen.

... Velos aus der Mülldeponie oder aus Reparatur-Ateliers sammeln, sie
streichen (alle in der gleichen Farbe!), "Gratis-Velo" auf den Rahmen
schreiben und sie in der Stadt verteilen.

... nigelnagelneue Produkte klauen und am nächsten Tag das Geld
zurückfordern --- und dieses dann grosszügig verteilen!

... dich auf Jobs bewerben und im Motivationsschreiben darlegen, warum
du überhaupt gar keine Lust hast, für das Unternehmen X zu arbeiten.

... mithilfe von Edelreisern Bäume im öffentlichen Raum pfropfen, damit
sie später essbare Früchte tragen (informiere dich, welche Arten
miteinander kompatibel sind).

... auf *google maps* negative und abfällige Kommentare bei allen
Polizeiposten deiner Stadt hinterlassen.

... die Weihnachtsdekorationen von luxuriösen Bonzen-Läden klauen und
sie an unerwarteten Orten wiederaufhängen.

[^10]: Der Text *Das grosse Mittagessen* \[n. 47\] schildert die Aktivitäten und das Innenleben einer selbstorganisierten VoKü.
