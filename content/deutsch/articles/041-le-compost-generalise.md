---
author: "Die Kompostierer*innen"
title: "Kompost überall"
subtitle: "Compost's revolution"
info: "Im Dezember 2020 für den französischen Sammelband verfasst"
datepub: "Im Herbst 2021 auf Deutsch übersetzt"
categories: ["Sabotage, direkte Aktion", "Ökofeminismus", "Ökologie", "Demonstration, Riot"]
tags: ["Kaffee", "Wut", "Kompost, kompostieren", "Kreativität", "Arsch", "Wasser", "Dünger", "Kraft", "Samen", "Maul", "Werkzeug", "Wut", "Trauben", "Sonne", "Speicherung", "Urbanismus"]
langtxt: ["de", "fr"]
zone: "Suisse romande"
num: "41"
quote: "die Welt ist ein riesiger Komposthaufen"
---

Die Welt ist ein riesiger Komposthaufen.

Das A und O unserer Vergangenheit liegt unter unseren Füssen, gut
kompostiert. Einzig die Dinosaurier haben sich in Erdöl verwandelt, doch
das ist keineswegs ihr Fehler. Unsere Gegenwart ist reif, um sich selbst
zu kompostieren. Auch wir sind Kompost, denn jeder Organismus wird
lebendig, wenn er aus Kompost entsteht. Ob unsere Zukunft auch wirklich
kommt (oder nicht), werden wir sehen. Derzeit handelt es sich um eine
Quantenzukunft, sie ist zwar da, aber irgendwie auch nicht --- kommt
drauf an, was wir mit unseren Leben anfangen. Was aber sicher ist: Wir
werden alle zu Dünger kommender Welten und künftiger Lebewesen.

Wir laufen auf Kompost, wir sind Kompost, wir essen Kompost, wir kacken
Kompost. Es ist ungefähr das, was uns Donna Haraway beschreibt, selbst
wenn sie es eleganter und komplexer sagt.

Es gibt immer einen Moment zum Nachdenken und einen für das Chaos.

Heute herrscht überall Chaos-Stimmung.

Deshalb: Chaos überall, Kompost überall[^11].

Die Gelbwesten haben die Chaos-Kompost-Stimmung bereits getestet: Gülle
vor der Präfektur in *Manosque*, Gülle und Müll vor der Steuerverwaltung
in *Challans* und *Limoges*, Mist vor der Finanzbehörde in *Lesparre*,
Mist auf einem Kreisel in *Mirecourt*, Mist in *Douai*, Mist in
*Haute-Vienne*, verdorbene Früchte in *Thuir*. Sie haben damit begonnen,
den Kapitalismus zu kompostieren.

Wenn wir alles kompostiert haben werden, wird unser Lebensraum zu
fruchtbaren Feldern, eine neue Ära wird beginnen, die Ära des Komposts,
Humus und der Fruchtbarkeit. In diesem Kontext werden wir unsere
künftigen Kinder willkommen heissen, es wird eine neue Menschheit geben,
eine die ganz *bio* ist. Das müsste auch den Bobos° munden. Die Slogans
stehen schon für sie bereit: *be more bio, be more compost*. Wir können
Medaillen und Labels verteilen. So lange sie an unserer Seite
demonstrieren, ist das für uns in Ordnung.

## ONE SOLUTION, REVOLUTION

Nachfolgend findest du eine Gebrauchsanweisung, um den Kompostierprozess
überall zu lancieren bzw. fortzuführen. Auf dem Weg zur ökologischen und
feministischen Revolution wird er wegeweisend sein.

---

***TRIGGER WARNING***

An alle Bullen, verantwortliche Politiker\*innen, multinationale
Unternehmen und Banken unserer liebenswerten Schweiz: Nein, ihr irrt
euch nicht, wir schwafeln kein dummes Zeug. Wir häufen derzeit grosse
Mengen an düngender Kacke und Kompost an. Damit wir euch begraben können
--- wortwörtlich. Es ist eine riesige Aktion, die wir für nächstes oder
übernächstes Jahr vorbereiten, je nachdem wie der Fermentierungsprozess
abläuft. Vergesst nicht, dass ihr Lebewesen und deshalb bestens
kompostierbar seid.

---

Bäuer\*innen, Landwirt\*innen, städtische und ländliche Bobos,
Grossfamilien, Student\*innen-WGs, besetzte Häuser, Quartiertreffpunkte:
Seid bereit, massenhaft Kompost zu produzieren, wenn wir euch den
Startschuss geben. An alle Quartiere, Weiler, Dörfer: Lanciert einen
kollektiven Kompost. Er wird nicht nur den sozialen Austausch fördern,
sondern auch eure Gemeinschaftsgärten düngen. Häuft an und sammelt ---
für den internationalen Tag des Komposts, der sich fein und fruchtbar
ankündigt.

Füllt die grossen grünen Tonnen mit Gemüse- und Fruchtschalen ---
tagtäglich und immer dann, wenn ihr am Kochen seid. Schauen wir diesen
grossen Bergen beim Vergammeln zu, kompostieren und leben wir weiter, um
ein fruchtbares Terrain aufzubauen. Häufen wir geduldig Kompost an,
atmen wir seinen Duft ein und nehmen wir ihn freudig unter die Lupe.
Sorgfältig ausgesuchte Samen werden darin wachsen, Samen der Wut und des
Zorns.

## Nun wird's konkret. Nachfolgend einige Rezepte für die Herstellung eines nervigen, ekligen und widerlichen Komposts, der auf die sturen Hüter\*innen des Kapitalismus geschleudert werden kann

### Ekelhafter Kompost, der zu lange fermentiert hat

- Sucht euch einen gut belichteten Ort mit direkter Sonneneinstrahlung.
- Um grosse Mengen an Kompost herzustellen, eignen sich geräumige Orte
am besten (draussen oder in einem geschlossenen Raum, beispielsweise
einem Parlament).
- Dichtet den Ort mit Blachen und Decken gut ab. Nur so gewinnt ihr
später ein Maximum an Saft.
- Wenn ihr wollt, dass euer Kompost nach Schwefel riecht, müsst ihr ihn
täglich bewässern.
- Wenn ihr euch stattdessen einen Ammoniak-Duft wünscht, dann mischt
viel Grünzeugs bei (z. B. Grasschnitt).
- Damit euer Kompost nicht an Kraft verliert, verzichtet lieber auf
Laub, Kaffee oder Eierschalen (so oder so: verzichtet auf Eier beim
Essen, so schont ihr die unschuldigen Hühner). Diese Substanzen sind für
die Entwicklung übel riechender Gerüche hinderlich.

### Brennnesselgülle

Brennnesselgülle eignet sich bestens, um den Kompost zu aktivieren,
riecht aber scheusslich. Das gute dabei: Brennnesseln sind einfach zu
finden. Die Pflanze ist für neoliberale Reinigungsfanatiker\*innen im
urbanen Raum der Alptraum --- sie will einfach nicht gehorchen. Ohne
unser Einwirken wächst sie überall wild und verteidigt sich mit ihren
Brennhaaren ganz von alleine.

- Fülle einen nicht metallischen Behälter mit Brennnesseln.
- Giesse Wasser dazu: 10 Liter für 1 kg Brennnesseln.
- Lasse das Ganze während 1-2 Wochen einwirken und rühre alle 2-3 Tage
kurz um.
- Fertig ist die Gülle. Absieben und in Behälter füllen.

### Wutentbrannter Abfallsaft

Es gibt Orte, an denen es mehr an Mülltonnen als Brennnesseln wimmelt.
Wie die brennenden Cousinen sind sie auch ganz einfach zu finden. Die
Zutaten für einen guten Saft sind: milde Peperoni aus Spanien, die von
den Supermärkten gleich dreifach eingepackt und meistens weggeworfen
werden, weil eine der drei Früchte zu faulen begonnen hat; Zitronen,
welche die wochenlange Reise schlecht vertragen haben und samt Netz mit
den anderen acht weggeworfen werden; die weichen Trauben (meisten sind
nur vereinzelt ein paar ganz weich), die ausgemustert werden, weil sie
nicht mehr den Ansprüchen der Supermarkt-Logik entsprechen;
schwarz-braun gefärbte Bananen oder andere Früchte und anderes Gemüse,
mit denen Mülltonnen beschenkt werden.

- Öffnet die Mülltonnen der Supermärkte und erfreut euch am widerlichen
Gestank. Je mehr es stinkt, desto stärker wird die Zielscheibe
parfümiert.
- Holt das Gemüse und die Früchte heraus, das noch intakt ist. Wenn
Mülltonnen von Supermärkten etwas Gutes tun können, dann ist es, jene
Menschen zu ernähren, die es nötig haben.[^12]
- Werft nichts weg, behaltet alle Schalen, abgeschnittenen Stücke usw.,
um sie zu kompostieren. *Zero waste!*
- Folgt dem Rezept für den *ekelhaften Kompost, der zu lange
fermentiert hat,* und giesst ausgiebig. Ein (zu) schwach bewässerter
Kompost ergibt nämlich holzigen Humus, und das wollen wir auf keinen
Fall.
- Erntet den Saft der Wut und schüttet ihn an den Ort, den ihr
gemeinsam ausgesucht habt. Noch besser, sprüht ihn nach Lust und Laune
über Individuen, die ihr ausgesucht habt... oder sollten wir eher sagen,
die "ausgewählt" wurden.

{{%ligneblanche%}}

Kompost verfolgt keinen Selbstzweck. Wir entscheiden gemeinsam, was wir
damit machen. Er hilft uns einerseits bestens, um unsere Projekte
wachsen zu lassen. Andererseits hat er seine Daseinsberechtigung genauso
wie Randalierer\*innen: Was ausgemistet werden muss, wird in Stücke
zerkleinert. Kompost hat die Kraft, die bestehende Ordnung umzustossen,
um neue Lebensformen zu erschaffen.

An alle Bäuer\*innen, welche die Schnauze voll haben von einer Politik,
welche die industrielle Landwirtschaft, Monokulturen und die Natur
zerstörende Praktiken fördert: Holt eure Baggerlader, Lastwagen,
mittelalterliche, aber in Zukunft wieder nutzbare, Gerätschaften und
versammelt euch für einen dreckigen Umzug.

An alle besetzten Häuser und kreativen Konstrukteur\*innen: Bauen wir
gemeinsam sogenannte KAW (Kompost-Abwehr-Werfer), um unsere stinkende
Munition in den Raum zu ballern.

Es ist die Gelegenheit, um unsere aufständischen Ideen zu erneuern.

In Flammen setzen, na klar, aber auch kompostieren.

[^11]: Im Text *Gewöhnliche Piraterie* \[n. 38\] finden sich einige
    andere, kreative Ideen in Verbindung mit Kompost.

[^12]: Der Text *Das grosse Mittagessen* \[n. 47\] gibt praktische Tipps,
    wie eine VoKü selbst verwaltet werden kann.
