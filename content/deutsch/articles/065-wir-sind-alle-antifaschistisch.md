---
author: "Stuart"
title: "Wir sind alle antifaschistisch"
subtitle: "*Basel Nazifrei*: vom Protest in den Gerichtssaal"
info: "Für den deutschen Sammelband verfasst"
datepub: "Im Dezember 2021 verfasst"
categories: ["Antifaschismus", "Demonstration, Riot", "Gefängnis, Justiz, Repression", "Polizei"]
tags: [""]
langtxt: ["de"]
zone: "Deutschschweiz"
num: "65"
quote: "Prost und hoch die internationale Solidarität…"
---

Schon etwas angedüselt vom Glühwein, Bier und dem einen oder anderen Zug
von Hip-Hop-Zigaretten sass ich in meinem königsblauen,
massgeschneiderten Anzug aus Hoi-An (Vietnam) und den dazu passenden
weissen Lackschuhen in Basel im Gerichtssaal. Das vor mir liegende,
geheftete dreiseitige Dokument mit dem Urteil habe ich kurz angeschaut,
um während dem gewohnt desinteressierten und monotonem Gelabere der
Gerichtsvorsitzenden gedanklich abschweifen zu können. Denn was heute
passiert ist, hat mich einen Scheiss interessiert. Doch vor drei Jahren
war das anders. Da war ich mit jeder einzelnen Faser meines Körpers
dabei.

## Basel Nazifrei

*Im Herzen von Basel traf sich am 24. November 2018 ein organisierter
Trupp von Faschist\*innen, Rassist\*innen und Antisemit\*innen um die
Partei national orientierter Schweizer (PNOS) zu einer Demonstration.
Vorwand dazu war die Ratifizierung des sogenannten UNO-Migrationspakts.
Mit von der Partie waren auch militante Kameradschaften*[^1] *und
Mitglieder des in Deutschland verbotenen
Terrornetzwerks *Blood & Honour*. Marschierende Nazis? «Nie wieder!»,
sagten sich über 2000 Antifaschist\*innen jeden Alters und stellten sich
den Braunen in den Weg. Diese massenhafte und vielfältige Zivilcourage
der Antifaschist\*innen war ein voller Erfolg: Den Nazis blieb nichts
anderes übrig, als ihre Hassparolen in einer abgelegenen Ecke an eine
Hausmauer zu brüllen.*

*Doch anstatt den «Prix Courage» gab es für viele Antifas eine absurde
und politisch motivierte Hexenjagd seitens der wild gewordenen Basler
Staatsanwaltschaft: Razzien und Verhaftungen landesweit und 40
kostspielige Gerichtsverfahren! Freisprüche gab es bisher keine.
Ungewöhnlich hohe, teils sogar unbedingte Haftstrafen dafür umso mehr.
Insgesamt werden sich die Kosten für die Verfolgten auf mindestens
500'000 Franken belaufen, also exorbitante* 500k *oder eben* e halbi
Chischte*. So viel kostet heute offenbar Zivilcourage.*

Als die Nazis der PNOS bekannt gegeben haben, dass sie sich in der
drittgrössten Stadt der Schweiz versammeln wollen, war für mich als
Antifaschist der Fall klar: *No Pasaran!* Auf keinen Fall darf es
passieren, dass Nazis in unseren Städten aufmarschieren. Denn die Parole
«Nie wieder Faschismus!» ist für mich keine hohle Phrase, mit welcher
man[^2] Feigenblattpolitik betreiben sollte, oder die anderweitig
verdreht oder entwertet wird. Nein, da wo Faschist\*innen auftreten,
gilt es mit aller Entschlossenheit gegen sie vorzugehen. Dafür muss man
nicht Geschichte studiert oder viele Bücher gelesen haben. Wer ein wenig
Verstand und das Herz am linken Fleck hat, weiss, auf welcher Seite zu
stehen ist. Um es mathematisch zu erklären: Es ist wie zwischen minus
und plus. Es gibt das Eine oder das Andere, dazwischen ist nichts.
Genauso ist es eigentlich mit dem Faschismus und dem Antifaschismus.

Über diese mathematischen Erkenntnisse sollte natürlich auch die
Bevölkerung auf den Strassen an diesem Tag informiert werden. Es musste
also ein Flugblatt geschrieben werden. Um den Menschen in Basel
vermitteln zu können, warum man so wütend auf der Strasse ist, musste
man ihnen erklären können, wer denn da auf der anderen Seite steht. Ich
las also das Parteiprogramm der PNOS. Dazu auch noch jenes von der
Nationaldemokratischen Partei Deutschlands (NPD) von welchem die
Schmocks der PNOS gar nicht so wenig abgeschrieben haben, ebenso wie von
der NSDAP[^3]. Eigentlich kann man es gar nicht glauben, was da
geschrieben steht, und auf der anderen Seite ist man so abgestumpft von
den Unmenschlichkeiten, die auf der ganzen Welt geschehen, dass einem
die Existenz solcher stumpfsinnigen Ansichten dann doch nicht wirklich
verwundern. Das Gute im Schlechten: Es war dafür nicht so schwer
aufzuzeigen, warum es *gopferdammi* sehr viele gute Gründe gibt, sich
den Nazis der PNOS in den Weg zu stellen. Die sehr breite Mobilisierung
hat man im Vornherein durchaus wahrgenommen. Egal ob auf den Strassen,
in den unterschiedlichen Medien oder im Freundeskreis, *Basel Nazifrei*
war in aller Munde. Auch ich war am Abend zuvor schon angespannt.
Wechselwirkend informieren, koordinieren. Hat man irgendwas vergessen?
Sind alle Leute mit den für sie relevanten Informationen versorgt? Wie
wird das Wetter? Habe ich noch Wäsche im Keller? Und immer wieder alle
Szenarien noch einmal durchgehen, die erfahrungsgemäss ja dann doch
immer anders rauskommen. Bereits früh bin ich mit Freund\*innen mit dem
Zug nach Basel gereist an diesem Tag, denn wir wollten davor noch
irgendwo was essen und trinken.

Wir haben natürlich als Vorbereitung auf die Demo nicht nur den braunen
*Brunz*[^4] der Pnösler gelesen, sondern uns auch Gedanken gemacht, wie
wir unsere Botschaften auch noch sichtbarer machen können. In unserer
Stube wurde also der Stoff ausgerollt. Wir erinnerten uns an den Refrain
von «Support your local Antifa» der Hip-Hop-Crew *Clan Des Dinos*:
*Geschter-hüt-und-morn chum mir wehred ois*! Diese Parole war es dann
auch, die wir geeignet für ein Transpi an diesem Tag fanden.

Ein anderer Grund, warum wir uns bereits so früh auf den Weg nach Basel
machten, war das Ziel, vor der PNOS auf dem Messeplatz einzutreffen.
Schlussendlich tröpfelten beide Seiten etwa gleichzeitig ein, was die
Situation etwas hektisch und diffus machte. Nur wenige Meter trennten
die immer grösser werdende antifaschistische Mobilisierung von den
strammstehenden Nazis. Sichtlich nervös und angespannt standen sie da.
In ihren Händen hielten sie ihre Morgensternfahnen --- eingerollt und
jederzeit als Waffe verwendbar. Bereits da zeigte sich die sehr
heterogene Zusammensetzung des antifaschistischen Widerstands. An
vorderster Front mit dabei waren beispielsweise viele türkische und
kurdische Antifas. Sie waren es, die als erste den Druck erhöhen und den
Pnösler klar machen, dass rückwärts nicht nur die Richtung in ihren
Köpfen ist, die sie an diesem Tag gehen. Als die Nazis sich schon
ziemlich zusammengepfercht auf dem Rückzug befanden, kam es wie zu
erwarten war, und die Bullen stellten sich dazwischen. Auch dieses Bild
verwundert nicht. Der oftmals gehörte Spruch: «Schweizer Polizisten
schützen die Faschisten», mag zwar für Aussenstehende etwas überspitzt
klingen, trifft es im Kern dann aber leider doch ziemlich genau.[^5]

Es war darum auch an diesem Tag schnell klar, dass man sich (wie Ester
Bejarano[^6] schon gesagt hat) beim Kampf gegen den Faschismus nicht auf
den Staat verlassen kann, soll und darf! Dies verdeutlichte auch die
behördliche Bewilligung eines von der JUSO initiierten Protests fernab
des Geschehens auf dem Messeplatz.

So standen wir also geschlossen mit unserem Transpi neben vielen anderen
vor der Polizeireihe. Dahinter dicht in ein Eckchen gedrängt der braune
Haufen. Dazwischen gab es hie und da kurze hektische Situationen, in
dessen Verlauf auch mal ein paar ihrer selbsternannten Sicherheitsleute
den antifaschistischen Widerstand zu spüren bekommen haben. Das Transpi
bis über die Nase gezogen standen wir also erst einmal vor den mit ihren
unterschiedlichen Waffen provozierenden Bullen und machten Stimmung
gegen die Nazis.

Der Rest des Tages ist schnell erklärt. Während die Polizei weiterhin
ihren Dienst verweigerte, versuchten wir von allen möglichen Seiten
Druck auf die Versammlung der Nazis auszuüben. Über mehrere Stunden war
es eine äusserst dynamische, lautstarke und kämpferische
antifaschistische Bewegung, die sich so den Raum in Basel an diesem Tag
genommen hat. Die Nazis standen, wie oben bereits erwähnt, wie bestellt
und nicht abgeholt, in einer kleinen Ecke auf dem Messeplatz und
verbreiteten da unter sich ihre antisemitischen Verschwörungstheorien.

Dazwischen wurde seitens der Polizei (ohne dass eine reelle Gefahr für
sie bestand), wie immer unkontrolliert, Gummischrot eingesetzt.
Daraufhin flogen natürlich diverse Gegenstände in Richtung der Polizei,
die den Nazis so versuchte, den Abzug zu ermöglichen. Dieses Spiel
wiederholte sich während mehreren Stunden.

Was geblieben ist, ist das Gefühl, das Richtige getan zu haben. Überall
wo man war, spürte man die Entschlossenheit der Menschen, Naziaufmärsche
gestern, heute und auch morgen nicht zu tolerieren. Ob aus den Fenstern
der Wohnungen in den Seitengassen, auf dem Platz selbst oder in dem
Klang der ertönenden Parolen. Aus dieser Entschlossenheit, dem Wut und
dem Hass auf den Faschismus sowie der Liebe zum guten Leben[^7],
resultierte der Erfolg des Tages. Denn für die PNOS markierte dieser
Tag, der für sie kurz vor ihrem Wahlkampf den Höhepunkt ihrer politisch
und aktivistischen Karriere seit der Neuformierung darstellen sollte,
ihr erneuter Abgang in die mediale (politisch waren sie es schon immer)
Bedeutungslosigkeit. *Mission failed*, kann man aus ihrer Sicht da wohl
sagen. Ganz im Gegensatz auf der Seite von uns Antifaschist\*innen. Da
hiess es: Prost und hoch die internationale Solidarität, beim
wohlverdienten Bier im Restaurant Hirscheneck.

## Fünf Monate später

Das Bier im Hirschi war längst verdaut und die Mobilisierung gegen die
Generalversammlung der PNOS in Bern vorbei, als die Basler
Staatsanwaltschaft vom Aff gebissen wurde. Eine für die meisten von uns
bis dahin unbekannte politische Hexenjagd gegen Antifaschist\*innen in
der Schweiz wurde losgetreten. Der damalige Basler Sicherheitsdirektor
(zum Glück nun abgewählt), der bereits dafür bekannt war,
unverhältnismässige und jeglicher Rechtslehre widersprechende Praxis
gegen alles was halbmässig links daherkommt anzuwenden, fühlte sich wohl
in seinem Paralleluniversum zu Höherem berufen. Mit ihm an vorderster,
oder besser gesagt rechtester Front: ein berüchtigter Basler
Staatsanwalt. Seine Hobbys sind mir nicht bekannt, aber ausser
unschuldige Leute an den Pranger zu stellen und strafrechtlich zu
verfolgen hat er wohl nicht viele. Dem ordnet er nämlich alles unter,
koste es, was es wolle. Dafür sprechen geschwärzte Dokumente, bewusst
verbreitete Lügen, fehlende Spuren in Polizeifunkprotokollen usw. Die
Liste ist beliebig erweiterbar, denn Fehltritte der Staatsanwaltschaft
sowie der Polizei (wobei die Trennlinien sehr unscharf verlaufen) gab es
wie Strafverfolgungen: Reichlich genug.

Es wurde also mit zweifelhaften und aufwändigen polizeilichen Mitteln
versucht, möglichst viele Antifaschist\*innen zu identifizieren und zu
verfolgen. Mehrere Hausdurchsuchungen wurden in der ganzen Schweiz
durchgeführt, etliche Personen in verschiedenen Kantonen zu Hause
abgeholt und nach Basel verfrachtet. Flächendeckend wurden DNA-Proben
entnommen und mittels Öffentlichkeitsfahndung wurde sogar nach 20
Personen gesucht --- eigentliche die *ultima ratio* in einem Rechtsstaat,
die nur in besonders gravierenden Fällen zur Anwendung kommen soll.

Wie es mich erwischt hat? Tja, wie gesagt, versuchte man schlussendlich
mit wahllosen Fotos über die Öffentlichkeit nach den ach so bösen
Antifas zu suchen. Auch ich schaute mir die Fotos an, um mich in letzter
Sicherheit zu wägen, nicht auf dem Scheiterhaufen zu landen.[^8] Und
tatsächlich, ich war darauf nicht zu finden. Hat es sich also
tatsächlich für einmal bewahrheitet, dass es nützt, sich an Demos von
Beginn an konsequent unkenntlich zu verkleiden? Obwohl ich der Meinung
war, dazumal gegen Ende des langen Tages in Basel nachlässig geworden zu
sein. Ist doch die Gegend um den Messeplatz bestimmt gut videoüberwacht,
geschweige denn voll von den ganzen Schlapphüten (so genannten
Geheimdienstmitarbeiter\*innen), die hinter jeder Ecke und auf jedem
Dach versteckt waren. Über das wahre Ausmass der Überwachung war ich mir
zu jenem Zeitpunkt ja gar noch nicht bewusst. Beruhigt tauschten wir uns
in unserer Gruppe darüber aus. Wir waren zum einen froh, uns nicht in
der Galerie zu finden, waren aber im gleichen Zug besorgt um die weniger
glücklichen Freund\*innen.

## Nochmals ein paar Wochen später...

Der Alltag verlief also wieder in seinen mehr oder weniger unnormalen
Bahnen. Dazu gehört auch, dass mir meine Briefeule mal einen
eingeschriebenen Brief vorbeibringt. Der Absender Basel könnte auch ein
Zufall sein, dachte ich. *Fifty-fifty* Chancen also. Doch als das Licht
anging, stand ich wohl in der falschen Reihe. Der Absender war die
Staatsanwaltschaft und der Inhalt eine Vorladung. Zuerst dachte ich
immer noch, dass es vielleicht auch ein Bluff sei, und sie mal wahllos
Leute vorladen. Ich war ja schliesslich nicht auf den Fahndungsfotos und
diese Personen waren ja angeblich die letzten, die sie noch suchten.

Wie immer in solchen Situationen fühle ich mich am sichersten, wenn ich
mich so bürgerlich verhalte wie nur möglich. Es kommt nicht von
irgendwoher, dass selbsternannte wichtige Leute Anzüge und Hemden
tragen. Es gehört irgendwie zum *game* der Macht. Um also so
selbstsicher wie möglich aufzutreten, versuche ich nach ebendiesen
Regeln und Normen mitzuspielen. Den Anzug habe ich da aber noch nicht
ausgepackt. Ich brauchte ja eventuell noch ein Ass im Ärmel. Eventuell
wäre das dann schon wieder *too much* und würde zu viel Aufmerksamkeit
erregen. So wie sie mir dann vor Gericht vorgeworfen haben, dass es ein
Beweis sei, dass ich auf nullrasiert vor Gericht erscheine und auf den
ihnen vorliegenden Fotos einen Dreitagebart trage. Bist du deppert! Ich
bin also bei der Staatsanwaltschaft reinmarschiert und habe den
Uninteressierten und Genervten gespielt, der um seine Freizeit beraubt
worden ist. Viele Fragen und keine Antworten waren das Ergebnis, wofür
meine Steuergelder verschleudert wurden. Die Entnahme von
Fingerabdrücken und DNA verweigerte ich zuerst. Nach ein paar Minuten
schweigend gegenübersitzen und ein paar versuchten Telefonaten an den
Staatsanwalt, erklärte mir der Polizist, dass es die Verweigerung der
DNA-Entnahme so nicht gäbe. Ich wägte nochmals ab und glaubte ihm dann
(was mir mein Anwalt später auch bestätigte)[^9].

Wie sich an diesem Tag herausstellte, war der Grund, warum ich nicht auf
den Fahndungsfotos gelandet bin, dass mich ein Szenekenner aus dem
Fan-Umfeld angeblich auf den national an alle kantonale Polizeikorps
verschickten Fotos erkannt hatte.

Es verging wieder eine ganze Weile, bis dann die Ankündigung für einen
Prozess vor einem Dreiergericht kam. Darin auch die Aufforderung, dass
ich ein paar Tage Zeit habe, um mir anwaltschaftliche Vertretung zu
organisieren. Wenn nicht, wird mir eine Pflichtverteidigung gestellt.
Gesagt, getan. Aus meiner Erfahrung wusste ich, dass es immer Sinn
macht, eine\*n kantonal ansässige Anwält\*in anzufragen. Kantönligeist
ahoi. Ab jetzt begann das grosse Warten. Doch in diesem Falle habe ich
zusammen mit vielen anderen alles andere als nur gewartet, bis uns der
bürgerliche Staat in sein Affenhaus zitierte[^10].

## Solidarity Forever

Das Bündnis *Basel Nazifrei* hatte seine Arbeit noch lange nicht
erledigt, wie sich herausstellte. Es stand ein Haufen Antirepressions-
und Solidaritätsarbeit an. Es ist unglaublich, wie es dieses Bündnis
geschafft hat, das Thema über die ganze Zeit aktuell zu halten und
nebenbei mit den Betroffenen zusammen eine Strategie zu erarbeiten.
Treffen unter ebenfalls Betroffenen können Kraft geben und die
Auseinandersetzung über die unterschiedlichen Methoden von politischer
Prozessführung waren sehr lehrreich.[^11]

Für mich war aber vor allem eine andere Kampagne interessant, die als
erklärtes Hauptziel hatte, 500'000 Franken zu sammeln. *500k* war das
über den Daumen gepeilte Vermögen, das notwendig sein würde, um so viel
finanzielle Unterstützung wie möglich für die Angeklagten zur Verfügung
zu stellen. Eine schier unvorstellbare Summe, die aber notwendig sein
wird. Geht es doch in Zeiten von reaktionärem Rollback und dem Aufstieg
extrem rechter Parteien und Bewegungen darum, den Antifaschismus zu
stärken und als das Normalste das es gibt wieder ins linke Licht zu
rücken.

Ich war von Beginn an mit dabei, als ganz unterschiedliche Menschen sich
zusammengetan haben, mit dem Ziel eine solche mega-giga Solikampagne aus
dem Boden zu stampfen. Der *vibe* war von Beginn an unglaublich. Voller
Tatendrang und Ideen und mit einem unglaublichen Grad an
Professionalität stürzten wir uns in die Arbeit.

Die Kampagne *500k: heillos verschuldet*[^12] war omnipräsent und
erreichte beinahe Kultstatus. Mir persönlich hat diese Arbeit, die
ständige überregionale Vernetzung, der Austausch untereinander, das
beinahe schon rauschhafte Entwerfen von neuen Ideen, Nutzen von
Synergien und Verbinden von Kämpfen unglaublich viel Kraft gegeben und
alles andere in den Hintergrund rücken lassen. Zwischenzeitlich kam die
Soliarbeit einem 40-Prozent-Pensum gleich, wobei ich Glück hatte und nur
60 Prozent Lohnarbeit verrichten musste. Ich denke auch, dass dies einer
der Hauptschlüssel war: Sehr viele Leute haben sehr viel Zeit und
Herzblut in die Kampagne gesteckt. Dazu zähle ich auch alle, welche die
Kampagne in irgendeiner Form unterstützt haben.

Immer wieder auch die Unterstützung für die Angeklagten frühmorgens vor
dem Gericht, die ich als den wichtigsten emotionalen Support für die
Angeklagten betrachte. Das ist ein starkes, kämpferisches politisches
Signal an den bürgerlichen Staat. Denn mit Geld alleine lässt sich keine
Revolution führen.

## Drei Jahre später

Endlich war es soweit und auch ich hatte nun endlich meinen Termin
erhalten, um mir das Theater im Gericht *live* vor Ort zu geben. Am
Abend zuvor hatte ich witzigerweise das erste Mal direkten Kontakt mit
meinem Anwalt. Normalerweise sicherlich nicht optimal, doch in diesem
Fall nicht weiter tragisch. Waren doch die Urteile schon längst gefällt,
und dies auch nicht der erste Prozess von meinem Anwalt in dieser
unsäglichen Farce eines Prozesses.

Auf eine Prozesserklärung meinerseits verzichtete ich, dies auch aus
Gründen von Zeitmangel und Lust. Ich habe jetzt zu diesem konkreten Fall
zwei Jahre mit viel Liebe und Freude politische Arbeit geleistet. Dazu
muss ich sagen, dass ich mich allem Gesagtem von den unterschiedlichen
Angeklagten anschliessen möchte und mich mit ihnen solidarisiere. Es
wurde schon alles gesagt, was gesagt werden musste, und dies mit
Intelligenz, Feinfühligkeit und endlosem Kampfeswillen, sodass ich gar
nicht mehr wusste, was noch zu sagen wäre. Ich wollte einfach einen
guten Tag verbringen in Basel und meinen Anzug auch mal ausserhalb einer
Hochzeit tragen. Ein Bierchen trinken, eins rauchen, und dem ganzen
Schauspiel so wenig Aufmerksamkeit wie nötig geben. Das ganze natürlich
zusammen mit meinen Freund\*innen, welche die Sache des Antifaschismus
immer unterstützt haben und werden. In diesem Sinne: Vorwärts! *Siamo
tutti antifascisti! Siamo tutte antifasciste!*

[^1]: Unter anderem die Kameradschaft *Heimattreu* sowie die *Nationale
    Aktionsfront*, die massgeblich für die Gründung der Nazigruppe (mit
    identitärem Touch) *Junge Tat* verantwortlich ist.

[^2]: Bemerkung der schreibenden Person zur Verwendung von «man»
    anstelle von «mensch»: Ich verstehe die Absichten hinter der
    Verwendung des Begriffs «mensch» anstelle des neutralen «man». Ich
    betrachte es jedoch lediglich als identitätspolitische Spielerei
    denn als logisch nachvollziehbare sprachliche Lösung eines Problems,
    das eigentlich gar keines ist. Daher möchte ich diese Form in meinem
    Text nicht verwenden.

[^3]: Nach Strafrechtsprofessor Alexander Niggli habe die PNOS ihr 20
    Punkte-Programm zu grossen Teilen dem 25 Punkte Programm der NSDAP
    abgekupfert, siehe hierzu:
    <https://www.antifa.ch/pnos-parteiprogramm-bei-hitler-abgekupfert/>

[^4]: Das schweizerdeutsche *Brunz* steht für «Scheiss» bzw. Stumpfsinn.

[^5]: Auch hier reicht ein Blick über den kleinen Tellerrand zum
    Beispiel nach Deutschland: Nazitelegramgruppen von
    Sicherheitskräften, Behördenleaks, direkte Zusammenarbeit von Nazis
    und Polizist\*innen, Relativierung von Nazipropaganda und im
    Gegenzug Kriminalisierung von linkem, antifaschistischem Widerstand.
    Dies sind bei weitem keine Einzelfälle. Ganz im Gegensatz dazu, was
    die Behörden mantraartig zu glauben machen versuchen. Aus
    historischen Begebenheiten gewachsen, sind die unterschiedlichen
    Sicherheitsbehörden und Korps anfällig für rechte Gesinnungen
    jeglicher Art.

[^6]: Ester Bejarano war eine deutsche jüdische Überlebende des KZ
    Ausschwitz-Birkenau. Sie ist im Sommer 2021 verstorben.

[^7]: Der Text *Aus Liebe zum Leben* \[n° 61\] schildert ebenfalls
    Zivilcourage an vorderster Front.

[^8]: Schaut, wenn immer möglich, Fahndungsfotos auf den jeweiligen
    Seiten nur *safe* an. Sprich über Tor, saubere Geräte, VPNs usw.

[^9]: Die einzige Option wäre gewesen, mich unter Polizeigewalt dazu
    zwingen zu lassen. Im Gegensatz zu Fingerabdrücken braucht die
    DNA-Entnahme durch die Polizei keine richterliche Anordnung und darf
    daher unter Polizeizwang abgenommen werden. Dies wird damit
    begründet, dass die DNA-Entnahme an sich nichts bringt und erst noch
    analysiert werden muss, wofür dann wiederum eine richterliche
    Anweisung nötig ist.

[^10]: Dieses abgeänderte Zitat entstammt aus der Feder der Kommunistin
    Andrea Stauffacher.

[^11]: Im Text *Solidarität, Solidarität, Solidarität* \[n° 66\] wird
    über den politischen Prozess im Rahmen von *Basel Nazifrei*
    nachgedacht.

[^12]: Im Frühling 2022 wird dazu eine Broschüre publiziert, welche die
    geführte Kampagne beleuchtet und Anregungen für künftige
    Solidaritätskampagnen gibt.
