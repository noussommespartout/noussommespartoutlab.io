---
author: "Anonym"
title: "Realistische Ziele für radikale Utopien"
subtitle: "Persönliche Überlegungen zu einer linksautonomen Politik in Kleinstädten"
info: "Für den deutschen Sammelband verfasst"
datepub: "Im Oktober 2021 verfasst"
categories: ["Selbstorganisation, kollektives Experimentieren", "Institutionelle Kämpfe", "Squat, Besetzungen, Wohnen"]
tags: [""]
langtxt: ["de"]
zone: "Deutschschweiz"
num: "58"
quote: "…nicht alles ist so ausweglos, wie es auf den ersten Blick scheint."
---

Die Voraussetzungen für revolutionäre Politik in Aarau scheinen auf den
ersten Blick eher weniger gut, wenn sich Leute in Aarau, in anderen
Kleinstädten oder in ländlichen Gebieten zusammentun, um zu versuchen
die Gesellschaft entscheidend zu verändern --- und damit ist nun nicht
gemeint, irgendeine Partei zu wählen, sondern revolutionäre Politik zu
betreiben. Es gibt weniger Menschen, mensch ist weniger anonym, der
Repressionsapparat scheint einfacheres Spiel zu haben und die
Bevölkerung denkt darüber hinaus auch häufiger konservativer als in den
Grossstädten. Aber nicht alles ist so ausweglos, wie es auf den ersten
Blick scheint.

Oft sind es nur wenige Personen, die sich einer radikalen Politik widmen
(wollen). Einzelpersonen haben es schwer, sich mit Gleichgesinnten zu
organisieren, denn Anlauf- oder Treffpunkte gibt es nicht immer. So
erstaunt es nicht, dass sich Menschen dann oft in bestehenden Projekten
in den nächst grösseren Städten engagieren. Von Aarau aus ist es ja
nicht weit nach Zürich oder ins Kulturzentrum Bremgarten (KuZeB). Finden
sich die Leute aber erstmal zusammen, ist die Anzahl der aktiven
Personen gar nicht mehr so wichtig. Denn es braucht nicht hunderte
Aktivist\*innen, um coole Projekte auf die Beine zu stellen. Das zeigte
sich erst kürzlich mit dem alternativen Stadtrundgang in Aarau, welcher
Ende Oktober von Einzelpersonen organisiert wurde.

Die Herausforderung ist dann aber, innerhalb dieser Gruppe einen
gemeinsamen Konsens über die Art und Weise, wie Politik gemacht werden
soll, zu erzielen. Diese Gruppen sind oft eine Art «Zwangsgemeinschaft».
Damit meine ich, dass es meist notwendig ist, Kompromisse einzugehen,
damit eine solche Gruppe überhaupt funktionieren kann. Die ist
sicherlich auch sonst möglich, jedoch denke ich, dass in Kleinstädten
solche Überlegungen eher eine Rolle spielen. Denn wenn alle
«innerlinken» Diskussionen ausgetragen würden, die Gewaltfrage geklärt
und vielleicht noch persönliche Beziehungen mit ins Spiel gebracht
würden, stünde mensch wohl schnell alleine da. Dies soll auf keinen Fall
bedeuten, dass Diskussionen nicht geführt werden sollen. Eine
Ausdifferenzierung zu allen möglichen politischen Ansichten wird aber
nicht möglich sein. Eine Heterogenität an Meinungen und Ansichten kann
zudem ein grosses Potenzial mitbringen. Ein Austausch über verschiedene
revolutionäre Strömungen könnte theoretisch weniger dogmatisch geführt
werden, als dies in grösseren Zentren unter Umständen möglich ist. Auch
das Verständnis für verschiedene Lebenslagen oder persönliche
Situationen muss gegeben sein, damit eine Gruppe funktionieren kann.
Verschiedene Ansichten oder Lebenssituationen bieten aber auch viele
unterschiedliche Anknüpfungspunkte für die Aktionen einer solchen
Gruppe.

Daraus lässt sich möglicherweise folgern, dass es schwierig ist, eine
konkrete gemeinsame Utopie zu entwerfen und zu versuchen, die Revolution
auszurufen. Wobei es mit der Revolutionären Linken Aargau (RLA) einen
Versuch gibt. Aber anders gesagt: Meiner Meinung wäre es wahrscheinlich
sinnvoller, alltägliche Kämpfe zu führen, die auf gemeinsamen Interessen
basieren. In Aarau war eines dieser Bedürfnisse stets die
Freiraum-Thematik. Von 2002 bis 2004 gab es den Verein für alternative
Kultur Aarau (fak), der unter anderem das ehemalige Restaurant Gais
besetzte oder legal für vier Tage ein Autonomes Jugend Zentrum (AJZ) im
Restaurant Krone umsetzte. 2006/07 war die überregionale Gruppe *WySuAL*
aktiv, die das «Tink and Move»-Festival in Lenzburg plante, dies aber
schlussendlich ins KuZeB verschieben musste. In den Jahren 2008/09
sorgte die Gruppe *Klaustrophobia* mit Demonstrationen und mehreren
Hausbesetzungen für Aufsehen. 2011 fand das erste nächtliche
Tanzvergnügen, eine «Tanzdemo» für Freiräume statt. In den
darauffolgenden zwei Jahren beteiligten sich über 1000 Personen an
diesen Demonstrationen. Aus diesen nächtlichen Tanzvergnügen entstand
2013 zudem die Kampagne für ein autonomes Zentrum (KAZ). Egal ob
Infoladen oder autonomes Zentrum --- ein Ort sollte her, wo mensch sich
selber organisieren und entfalten kann. Gleichzeitig sollte der Ort aber
auch als Anlaufstelle für neugierige Personen dienen. Dies ist ein
konkretes Projekt, für das sich gemeinsam kämpfen lässt, auch wenn
mensch sich über einem Nebensatz von Marx uneinig ist.

Wurde sich auf ein konkretes Thema geeinigt, das Ziel sowie auch die
Methoden klar definiert, ist dies eine solide Grundlage, worauf sich
auch eine revolutionäre Politik aufbauen lässt. Der Kampf für autonome
Freiräume ist zudem ein Ziel, das nicht nur für die eigene Bewegung
erkämpft werden würde, sondern auch bei vielen anderen Bewohner\*innen
der Stadt auf Anklang stossen könnte. Denn das Bedürfnis nach Freiraum
ist gross. Mit dem Thema können auch Menschen ausserhalb der eigenen
Bewegung angesprochen werden und so an andere revolutionäre Ideen
herangeführt werden. Und hier findet sich eventuell auch ein grosser
Pluspunkt kleinstädtischer Politik. Der «Zwang» konkrete Arbeit zu
leisten, die über den Tellerrand der eigenen Szene hinaus geht. So wäre
es unter Umständen einfacher möglich, revolutionäre Ideen in andere
Gesellschaftsschichten hineinzutragen. Etwas, das in Städten mit einer
grösseren Szene oft vergessen geht.

Natürlich gibt es auch andere Themen, die sich gut mit der lokalen
Situation verknüpfen lassen könnten und über die eigene Szene hinaus
gehen: der Kampf gegen Nazis, konkrete Arbeitskämpfe,
Gentrifizierungsprozesse usw. Egal bei welchem Thema, es sollte stets
bedacht werden, wie damit auch andere Bewohner\*innen der Stadt oder des
Stadtteils damit «abgeholt werden» können, nicht damit die politische
Arbeit zum reinen Selbstzweck wird.

Wieso hat Aarau nach Jahren an Kämpfen für einen autonomen Freiraum dann
immer noch keinen solchen? Neben der Frage, für was mensch sich
engagiert, ist die andere zentrale Frage wie dies geschieht. Oft habe
ich es erlebt, dass versucht wurde, Konzepte von anderen Städten zu
adaptieren. Es war irgendwie immer cool, zu sehen, was in Städten wie
Zürich, Hamburg oder Kopenhagen passierte. Der Fokus lag eher bei der
Reproduktion von Aktionsformen oder der eigenen Subkultur an sich als
auf konkreter bzw. zielgerichteter politischer Arbeit. Der Versuch einer
Hüttensiedlung mit dem Namen «UTOPIA» 2009 in Aarau war klar an die
«Shantytown»-Aktion aus Zürich angelehnt, im gleichen Zeitraum gab es
sogenannt autonome Vollversammlungen, die zuvor vor allem in Hamburg und
Berlin aufkamen und die Idee des ersten nächtlichen Tanzvergnügen in
Aarau, also eine eher offene gestaltete *Reclaim The Streets*, wurde von
Geburtstags-RTS des AZ Köln inspiriert. Wie viele Demonstrationen oder
Hausbesetzungsversuche braucht es wohl noch bis dabei Erfolg --- also ein
Freiraum --- rausschaut? In den 00er-Jahren gab es rund zehn
Hausbesetzungen oder viel mehr Besetzungsversuche in Aarau. Nur wenige
gab es für mehrere Tage und keine länger als eine Woche. Dafür
Polizeirepression und zum Teil Geldbussen. Ich möchte auch gar nicht
sagen, dass bei einem Kampf für Freiräume nicht auch Häuser besetzt
werden dürfen oder sollen. Dies kann für die eigene Gruppe auch durchaus
toll sein und Kraft verleihen. Aber wahrscheinlich wird damit kaum ein
Autonomes Zentrum (AZ) erschaffen werden. Gruppen in Kleinstädten müssen
sich vielmehr mit anderen Strategien behelfen. Und womöglich müssen sie
nicht nur innerhalb der eigenen Gruppe, sondern auch gesamthaft erstmal
mehr Kompromisse eingehen. Ist es so schlimm, sich mit Vertreter\*innen
von der Stadt an den Tisch zu setzen? Wäre es nicht irgendwie auch
möglich, sich formell zu organisieren, um zum Beispiel einen Mietvertrag
zu unterschreiben. Muss es wirklich direkt die ganze Bäckerei sein oder
genügt zu Beginn nicht auch Mehl und ein Ofen, um die ersten Brote zu
backen?

Dazu war mensch in Aarau über all die Jahre jedoch nie bereit. Das
Ergebnis: Auch wenn die geführten Kämpfe sicherlich nicht für nichts
waren, ein autonomes Zentrum ist trotzdem nie entstanden. Es ist nach
wie vor alles so, wie es schon immer war. Immer wieder neue Leute haben
sich engagiert und die meisten haben sich dann längerfristig doch
anderen Projekten gewidmet oder sogar resigniert.

Was gäbe es also für Alternativen? Die Situation in Thun glich sehr
lange jener in Aarau. Die Gruppe *Raumfänger*, die Aktion *Hausgeist*
oder das Kollektiv *A-Perron* organisierte Demonstrationen, Sauvages
oder Hausbesetzungen. Wie in Aarau resultierte jedoch nichts Konkretes.
Immerhin schafften es die Thuner Aktivist\*innen, das Thema so in die
Bevölkerung zu tragen. Es gab sicher Phasen in Aarau, wo dies auch der
Fall war. Die Thuner\*innen liessen sich dann aber auch zu Gesprächen
mit Vertreter\*innen der Stadt ein. Und dies war bestimmt nicht einfach.
Das Ergebnis: Im Winter 2013/14 eröffnete das Alternative Kulturzentrum
Thun (AKuT). Vielleicht ist das AKuT nicht das Gelbe vom Ei --- diese
Analyse überlasse ich den Thuner\*innen ---, aber es wurde das
realisiert, was mensch in Aarau wollte. Ein Treffpunkt, wo es möglich
ist, kulturelle Veranstaltungen durchzuführen und darüber hinaus
politische Vernetzungsarbeit zu machen. Das Autonome Kulturzentrum in
Langenthal (LaKuZ) bestünde nicht bereits seit 20 Jahren, wenn die dort
engagierten Personen keine Kompromisse mit der Stadt eingegangen wären.
Natürlich sind die vielen Auflagen nervig, mit denen die
Betreiber\*innen und Besucher\*innen des LaKuZ sich da rumschlagen
müssen. Aber sind diese im Vergleich zur Wichtigkeit eines solchen
Treffpunktes nicht auch ein wenig vernachlässigbar?

Ein anderer Weg wurde in Solothurn eingeschlagen. 2015 wurde dort der
Infoladen *Cigno Nero* eröffnet. Dafür wurde eine Lokalität gemietet. Es
ist natürlich nicht einfach, geeignete Räume zu bezahlbaren Mieten zu
finden. Jedoch konnte sich erst so ein Treffpunkt in Solothurn
etablieren.

Die Option Räumlichkeiten zu mieten, kam in Aarau zwar ab und an auf,
wurde aber nie wirklich verfolgt. Zu klein wurden die Chancen angesehen,
so etwas umsetzen zu können. Und zu wenig war mensch bereit, die dafür
nötigen Massnahmen anzugehen. Also sich damit zu befassen, wie eine
solche Lokalität bezahlt werden könnte und ob einzelne Aktivist\*innen
auch ein Teil der Miete übernehmen würden. Vielleicht war es auch
einfach die Angst und die Unsicherheit, wenn mensch sich für ein solches
Projekt verpflichtet hätte. Nun müsste mensch wirklich schauen, dass der
Laden auch funktioniert, Leute vorbeikommen und Veranstaltungen
stattfinden. Die Lokalität müsste zumindest finanziell nicht zu hundert
Prozent selbsttragend sein, da es heutzutage verschiedenste
Finanzierungsmöglichkeiten gibt. Aber das Geld für ein Freiraum-Projekt
auch mittels Crowdfounding, Spendenanlässen oder Stiftungen
aufzutreiben, war mensch in Aarau nie bereit. Diesen Weg ging 2019 die
offene Werkstatt *Prozessor*. So konnten die ersten Jahre einer
Zwischennutzung realisiert werden, in der nun auch aufgezeigt werden
soll, wie wichtig dieser Ort ist, um künftig auf eine breite Solidarität
der lokalen Bevölkerung zählen zu können.

Dies hört sich nun vielleicht alles nicht so krass und revolutionär an.
Die Geschichte in Aarau hat jedoch zumindest gezeigt, dass sich kein
Projekt etablieren oder schon nur mittelfristig halten konnte, wenn
dieses sich nicht in einer Form der Legalität bewegte. Es scheint mir
aber gerade eine Kontinuität wichtig, um Veränderungen herbeizuführen.
Dafür wäre es sicher wichtig, innerhalb von zum Teil regulierten Räumen
Strukturen zu schaffen, die dafür sorgen, dass sich das Projekt nicht
immer mehr an den gesellschaftlichen Bedingungen anpasst. Es gilt ganz
konkret aufzuzeigen, wie anarchistische Praxis gelebt und sie in unseren
Alltag integriert werden kann. Auch von Menschen, die nicht in
Wagenburgen wohnen oder es sich sonst in der linken Blase gemütlich
eingenistet haben. Dafür braucht es viel Geduld und auch Verständnis.
Ein solches Projekt gab es 2009. Personen aus dem linksradikalen
Spektrum arbeiteten während fünf Wochen mit verschiedenen Menschen aus
anderen kulturellen Projekten zusammen und realisierten so «Raumlos!».
In den Sommerferien des Wenk wurde ein eigenständiger Kulturbetrieb
realisiert.

Momentan entsteht die Interessensgemeinschaft *Rockwell*. Da finden sich
Personen aus ganz verschiedenen Bereichen zusammen, um zu schauen,
inwiefern eine Zwischennutzung von Teilen des Rockwell-Gebäudes im
Torfeld Süd möglich wäre. Wenn es zu einer Umsetzung käme, würde dies
für Aarau ganz neue Chancen geben. Es könnte ein Projekt sein, wo viele
Sachen ausprobiert werden könnten und zudem ein Austausch mit
verschiedenen anderen Gruppen stattfänden. Das würde sicher nicht immer
einfach werden, bietet aber Möglichkeiten. Über die Zwischennutzungen
des Gebäudes soll im Dezember 2021 entschieden werden.

Ob dies nun klappt oder nicht, ist noch unklar. Dass sich in den letzten
Monaten wieder vermehrt kleine politische Bezugsgruppen in Aarau
gebildet haben, macht zumindest Hoffnung, dass künftig versucht wird,
radikale Politik auf die Strasse zu tragen --- sei dies nun für einen
autonomen Freiraum oder zu einer anderen Thematik. Ich hoffe jedenfalls,
dass versucht wird, neue Wege auszuprobieren und nicht die immer
gleichen politischen Aktionsformen und Methoden zu reproduzieren.

Denn ganz egal ob Dorf, Klein- oder Grossstadt: Zu einer relevanten
politischen Kraft werden wir erst wieder, wenn wir es schaffen die
«normale» Bevölkerung zu erreichen. Dies geschieht eher selten durch
brennende Autos oder klirrende Scheiben, sondern eher durch solidarische
Hilfe in Alltagskämpfen. Lasst uns versuchen, den Menschen konkrete
Möglichkeiten aufzuzeigen, setzen wir realistische Projekte um, die das
Leben aller Bewohner\*innen verbessern und zeigen so das Potential einer
anarchistischen Gesellschaft auf.
