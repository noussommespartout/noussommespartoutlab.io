---
author: "Anonym"
title: "Lernen, sich selbst zu organisieren"
subtitle: "Tools für horizontale Kommunikations- und Organisationsformen"
info: "Im Oktober 2020 für den französischen Sammelband verfasst"
datepub: "Im Herbst 2021 auf Deutsch übersetzt"
categories: ["DIY", "Selbstorganisation, kollektives Experimentieren"]
tags: ["Verwaltung, administrativ", "Liebe, Verliebtheit", "Vertrauen", "Körper", "horizontal", "Werkzeug", "Wort", "Macht", "Treffen", "Stille", "Theater", "Stimme"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "21"
quote: "Momente des Schweigens willkommen sind"
---

Die Kommunikations- und Organisationsformen, die wir instinktiv
anwenden, stammen von der Gesellschaft, in der wir aufgewachsen sind und
leben. Instinktiv reproduzieren wir die Codes und Reflexe einer
Gesellschaft, die hierarchisch organisiert ist, die Wert auf
Spezialisierung legt und in der bestimmte einzelne Stimmen mehr Gewicht
haben als andere.

Wollen wir aber Alternativen aufbauen, müssen wir uns gemeinsam und
mithilfe basisdemokratischer Organisation für neue Formen des Zuhörens
und Teilens einsetzen. Es ist ganz einfach: Wir müssen uns neu erfinden.
Bei einer anarchistischen, autonomen Selbstverwaltung° geht es nicht
darum, Räume ohne Regeln und Grenzen zu schaffen. Vielmehr geht es
darum, Alternativen, welche die Verteilung von Macht und unangenehme,
aber auch positiv bewertete Aufgaben ermöglichen, aufzubauen und
umzusetzen. Es geht darum, sich *tools* \[Werkzeuge\] anzueignen, die
uns nicht von Vorgesetzten, Chef\*innen, Staaten oder der Polizei
vorgeschrieben werden. Vielmehr müssen wir ein Instrumentarium aufbauen,
das aus der Basis der Bevölkerung, von unten, entsteht und dauerhaft,
leicht anzuwenden sowie flexibel ist.

Natürlich behandeln bereits etliche theoretische Lehrbücher diese
Fragen. Ich will sie an dieser Stelle aber ganz bewusst nicht
reproduzieren. In den zahlreichen Kollektiven, in denen ich aktiv war,
haben wir auf Literatur verzichtet. Wir haben uns Wissen --- vielleicht
etwas langsamer, als wenn wir auf bestehendes Wissen aufgebaut hätten
---, und *tools* angeeignet, indem wir von unserer eigenen Lebenswelt und
ihren Besonderheiten, Ansprüchen und Zielen ausgegangen sind.

Nachfolgend sind Werkzeuge aufgelistet, die ich an verschiedenen Orten
eingeführt und ausprobiert habe. Sie haben zum Ziel, horizontale
Kommunikations- und Organisationsformen zu fördern. Denn: Wie wollen wir
miteinander kommunizieren, wenn wir nicht die gleiche Sprache sprechen
und nicht den gleichen Bildungs- und Lebenshintergrund haben?

## Schweigen

### Warum?

Wer in unserer Gesellschaft eine grosse Klappe hat und sich in den
Vordergrund zu stellen weiss, geniesst soziale Anerkennung. Wenn
geschwiegen wird und wir dieses Schweigen willkommen heissen, erweist es
sich als ein mächtiges Werkzeug. Es gehört denen, die anderen nicht ins
Wort fallen. Mit Momenten des Schweigens schaffen wir Zeit zum
Nachdenken und Zuhören. Schweigen kann helfen, eine Art Horizontalität
wiederherzustellen.

Bis wir das Schweigen in unser soziales Zusammensein integriert hatten,
hat es aber etwas gedauert. Kürzlich hat eine Person während einer
Sitzung gesagt: "Wenn wir die Qualität unserer Beziehungen an den Anzahl
Schweigemomenten bemessen, dann sind wir auf gutem Weg." In der Tat:
Momente, in denen geschwiegen wurde, waren während dieser Sitzung von
grosser Bedeutung. Es sind Momente des Teilens. Momente, in denen das
Gesagte verdaut wird.

### Wie kann Platz für Momente des Schweigens geschaffen werden?

-   Ganz einfach: Am Anfang einer Diskussion oder einer Sitzung wird
    angekündigt, dass Momente des Schweigens willkommen sind. Im Rahmen
    unserer Interaktionen versuchen wir gemeinsam, dem Schweigen bewusst
    Raum zu geben. Weil wir es benennen, kommt niemand in Verlegenheit,
    wenn es plötzlich da ist. Mit der Zeit wird es sich sogar erübrigen,
    darauf hinzuweisen. Momente des Schweigens werden so zu
    Teilnehmenden.

-   In Diskussionsrunden wird die Redezeit pro Person beschränkt (z. B.
    fünf Minuten). Hält sich eine Person nicht an die Vorgabe, wird sie
    unterbrochen. Hat die Person hingegen bereits nach drei Minuten
    nichts mehr zu sagen, oder eine Übersetzung des Gesagten ist nötig,
    werden die restlichen zwei Minuten bewusst verstrichen gelassen.

-   Anstelle einer klassischen Tischrunde können für die Vergabe des
    Wortes die Sätze "ich nehme" und "ich gebe" verwendet werden. "Ich
    nehme", wenn ich zu Wort kommen will, und "ich gebe", wenn ich
    nichts mehr zu sagen habe. Wenn eine Person das Wort hat, darf sie
    ganz bewusst auch schweigen, bevor sie weiterspricht. Und weil die
    Reihenfolge der Tischrunde nicht vordefiniert ist, fällt die
    Verpflichtung weg --- sofern ein Person fertig gesprochen hat ---, das
    Wort gleich wieder aufzunehmen.

## Schreiben

### Warum?

Wenn es ums Schreiben geht, bringen wir nicht alle dieselben
Voraussetzungen mit --- die Unterschiede im Vergleich mit dem
gesprochenen Wort sind eher noch grösser. Es geht hier nicht darum,
lange Texte zu schreiben. Eher geht es darum, Raum zu schaffen, dass
sich jede\*r Einzelne Zeit nehmen kann, um eine Idee oder ein Bedürfnis
aufzuschreiben --- ganz ohne den Stress unmittelbar auf Gesagtes
reagieren oder es öffentlich kommunizieren zu müssen.

### Wie?

-   **"Klangwelt".** In die "Klangwelt" können Zettel deponiert werden,
    die Liebe oder Enttäuschung ausdrücken, Bedürfnisse oder Träume
    mitteilen. Die "Klangwelt" kann ein physisches Objekt (z. B. eine
    Schachtel) an einem bestimmten Ort sein. Und dort bleibt sie auch.
    Sie wird während den Treffen des Kollektivs kontinuierlich mit
    Zetteln gefüllt. Später wird bestimmt, wann der (bis anhin geheime)
    Inhalt der Zettel enthüllt wird. Jede\*r fischt einen Zettel heraus
    und liest ihn mit lauter Stimme vor. Alle Anliegen werden so gehört
    und geteilt. Manchmal geht es darum, bestimmte Situationen gemeinsam
    zu diskutieren. Manchmal erfordert das Anliegen eine Reaktion. Und
    manchmal geht es einzig darum, dass das Anliegen gehört wird.

    -   Die "Klangwelt" kann dazu dienen, kollektiv ein emotionales
        Anliegen zu behandeln, eine gemeinsame Traktandenliste zu
        erstellen oder Ideen und Themen während einer Sitzung zu sammeln
        und zu diskutieren. Manchmal ist es einfacher, für sich alleine
        über ein Anliegen nachzudenken als es in der Vollversammlung
        auszudrücken.

    -   Achtung: Die "Klangwelt" darf meiner Meinung nach nie als
        öffentliche Bühne zur Anprangerung von Problemen dienen.
        Existieren interne Konflikte, ist es besser, diese auf eine
        andere Art zu lösen. Es soll vermieden werden, dass
        Einzelpersonen öffentlich durch anonyme Nachrichten einer
        "Klangwelt" denunziert und so emotional herausgefordert werden.
        Die "Klangwelt" kann dazu dienen, kollektive Mängel
        anzuprangern, nie aber individuelle.

-   **"Nektarsammeln".** Während dem "Nektarsammeln" werden grosse
    Blätter auf einem Tisch verteilt. Nach einem (mündlichen oder
    schriftlichen) Brainstorming wird auf jedes Blatt ein Thema
    geschrieben, das diskutiert werden soll. Mit einem Stift in der Hand
    geht dann jeder Einzelne von Tisch zu Tisch, von Blatt zu Blatt und
    schreibt Stichwörter und Sätze auf. Damit entsteht automatisch eine
    Diskussion --- das "Nektarsammeln" geschieht ganz von allein! Und was
    kommt dabei heraus? Alle haben die Möglichkeit zu jedem Thema ihre
    Ideen beizutragen. Jedes Wort konnte in den Köpfen der anwesenden
    Personen reifen und in Ruhe auf dem Blatt notiert werden.

## Arbeiten in kleinen Gruppen

### Warum?

Nicht jede Person mag es, vor Publikum zu sprechen. Für manche ist es
einfacher, für manche weniger. Vor 10, 15 oder 20 Personen zu sprechen,
ist nicht einfach. In traditionellen Vollversammlungen nehmen Personen,
denen das Sprechen vor Publikum leichtfällt bzw. die einen grossen
Wissensschatz haben, viel Platz ein. Dadurch entstehen automatisch
Machtbeziehungen. Deshalb ist es kompliziert, in Vollversammlungen
Entscheidungen zu treffen. Die Diskussion findet oft nur zwischen
einigen wenigen Personen statt, es entstehen unweigerlich
Ungleichheiten. Um diesen Machtungleichheiten entgegenzuwirken,
empfiehlt es sich, in kleinen Gruppen zu arbeiten.

### Wie?

-   Zuerst teilen wir uns in kleine Gruppen auf, diskutieren ein oder
    mehrere Themen und kehren erst nachher in die Vollversammlung
    zurück. Dort fasst dann im Namen der Gruppe eine Person das Gesagte
    zusammen.

Handelt es sich um sensible Themen, muss nicht zwingend alles in der
Vollversammlung diskutiert werden. In kleinen Gruppen stellt sich
automatisch ein Vertrauensklima ein. Wir öffnen uns schneller, schweifen
eher vom Thema ab, kommen aber auch wieder schneller zum Thema zurück.

-   Besonders geeignet sind Dreiergruppen. Eine Person spricht während
    einer festgelegten Zeit über ein Thema, eine zweite Person hört ihr
    aktiv zu. Sie stellt Fragen und unterstützt die Person, sollte sie
    den Faden verlieren. Auf Kommentare und wertende Urteile wird
    bewusst verzichtet. Die dritte Person macht Notizen. Nach Ablauf
    einer festgelegten Zeitspanne wechseln die Rollen. Nachdem alle zu
    Wort gekommen sind, wird eine Zusammenfassung gemacht, um zu sehen,
    wo sich die Ideen überschneiden bzw. auseinandergehen.

    -   Diese Methode eignet sich gut, um grundlegende oder sehr offene
        Fragen zu behandeln. Sie ermöglicht es, in die Tiefe zu gehen,
        Beziehungen zwischen den persönlichen Erfahrungen zu knüpfen und
        sich in einem *safe space°* mit ein paar wenigen Personen
        auszudrücken. Alle Personen werden berücksichtigt und können
        sich äussern.

## Körper und Raum

### Warum?

Bildertheater ist eine Methode der *Popular education*. Ich habe es in
mehreren Kollektiven ausprobiert. Jedes Mal konnten damit Emotionen frei
gemacht und tiefgründige, ernste Themen aufgedeckt werden. Dank dieser
Methode können wir mithilfe unserer Körper und verschiedener Objekte
Sachen ausdrücken, die wir mit Worten nicht zum Ausdruck bringen können.
Dadurch ist es auch möglich, sich Zeit zu nehmen, um anders über unsere
Beziehung zur Gruppe nachzudenken. Diese Methode kann zur Behandlung von
verschiedenen Themen eingesetzt werden. Sie eignet sich besonders gut,
um das Verhältnis jedes\*jeder Einzelnen zur Gruppe anzuschauen.

### Wie?

-   Der zur Verfügung stehende Raum wird mit dem gestaltet, das spontan
    zur Verfügung steht. Oftmals werden Stühle und Sofas verwendet, die
    verschiedene Positionen im Raum symbolisieren. Wie der Raum
    gestaltet wird, ist eng mit der Frage verbunden, die von der Gruppe
    gestellt und behandelt werden will. Wenn das Verhältnis der
    Individuen zur Gruppe behandelt werden soll, empfiehlt es sich, mit
    der "Raummitte" zu spielen, indem einige Objekte sehr zentral
    platziert werden und andere eher in der Peripherie bzw. in den
    Ecken. Es kann auch mit der Vertikalität experimentiert werden.
    Beispielsweise indem in der Mitte des Raumes ein Stuhl auf einen
    Tisch *und* rundherum niedrige Stühle auf dem Boden platziert
    werden.

Die Stühle können dann unterschiedlich angeordnet werden: zur Mitte hin
oder mit der Rückenlehne zur Mitte. Indem ein Stuhl auf eine unebene
Fläche gestellt wird, kann er aus dem Gleichgewicht gebracht werden. Die
Szenografie kann ganz einfach an das zu behandelnde Thema angepasst
werden.

Wenn die Szenografie bereitsteht, werden der Gruppe drei Fragen
gestellt. Jede\*r platziert sich dann dort, wo er\*sie will, je nach
Gefühlslage mit der Gruppe. --- Erste Frage: Wo siehst du dich innerhalb
der Gruppe? Zweite Frage: Wo denkst du, dass dich die anderen in der
Gruppe sehen? Dritte Frage: Wo möchtest du sein? --- Nachdem die erste
Frage gestellt wurde, bewegt sich jede\*r ruhig durch den Raum und
positioniert sich. Das gleiche Prozedere wird nach der zweiten und
dritten Frage wiederholt. Am Ende der Übung wird die Szenografie
aufgelöst. Die Erfahrungen werden nun zusammengetragen. Jede\*r erklärt
den anderen, was für eine Strecke er\*sie zurückgelegt hat. Dabei wird
auch erklärt, welche Gründe ihn\*sie dazu veranlasst haben, sich an den
verschiedenen Orten zu positionieren. --- Dieses Werkzeug soll helfen,
Unsichtbares sichtbar zu machen. Dadurch entstehen manchmal sehr
emotionale Momente. Seine Wirkung entfaltet diese Methode in ganz
verschiedenen Kontexten. Für eine Gruppe, die schon länger besteht, ist
sie ganz besonders interessant.

## Mentale Belastung und undankbare Aufgaben

Dreckiges Geschirr, das herumsteht; ein Kühlschrank voll mit
angeschimmelten Lebensmitteln; schmutzige Toiletten. Wir sind es uns
doch alle bewusst: Das reicht manchmal schon aus, um in einem Kollektiv
einen Knall zu provozieren. Jenseits dieser undankbaren Aufgaben, die
oft im Verborgenen erledigt werden, lautet die Frage aber eher: Wie kann
die mentale Belastung in einer Gemeinschaft fair verteilt werden? Die
Herausforderung besteht darin, "Spezialisierungen" zu vermeiden und jede
erledigte Aufgabe zu würdigen und sichtbar zu machen, um Frustrationen
und Groll zu vermeiden.

Doch wie kann das konkret umgesetzt werden, wenn mehrere Ansichten
vertreten sind? Die einen werden sagen: "Ich will keine Regeln und
Einschränkungen." Wogegen die anderen sagen werden: "Ich brauche Regeln
und wünsche mir, dass die Aufgaben verteilt werden, damit ich mich nicht
schuldig fühle oder zu viel mache."

Es gibt verschiedene Organisationsformen, allerdings ist keine je zur
Siegerin erkoren worden. Aus meiner Sicht gibt es aber eine ganz
interessante Organisationsform, dessen Anwendung mich sehr überzeugt
hat.

Wie können Aufgaben und ihre Verteilung innerhalb einer Gruppe sichtbar
gemacht werden? Wie können wir ein Bewusstsein dafür schaffen?

-   Jede Person erhält mehrere Post-it, auf denen alle Aufgaben notiert
    werden, die innerhalb des Kollektivs anstehen: Kühlschrank/WC
    putzen, Bar aufräumen, Einkäufe erledigen, Sachen reparieren, Gäste
    empfangen, Mailbox verwalten, Newsletter schreiben, Buchhaltung usw.
    Jede Aufgabe wird notiert, egal wie viel Zeit und Wertschätzung sie
    in Anspruch nimmt. Später werden alle Post-it auf einem grossen
    Tisch verteilt und nach Aufgabenbereichen geordnet: Administration,
    Unterhalt der Räume, externe Kommunikation usw.

    -   Erst indem eine Übersicht über alle innerhalb des Kollektivs zu
        erledigenden Aufgaben erstellt wird, werden sie auch sichtbar.
        Dadurch wird schnell klar, dass einige Aufgaben von mehreren
        Personen und manche nur von einer einzigen Person erledigt
        werden. Ziel ist nicht, zu ermitteln, wer wie viele Aufgaben
        erfüllt. Aus diesem Grund empfehle ich, die Post-it anonym zu
        verteilen. Es geht einzig darum, einen Überblick zu schaffen,
        welche Aufgaben überhaupt erledigt werden. Bereits das schafft
        Anerkennung. Zudem wird auch klar, wie die Aufgaben innerhalb
        des Kollektivs verteilt sind.

-   In einem nächsten Schritt werden dann Personen ausgelost, die sich
    während einem bestimmten Zeitraum, z. B. einem Quartal, um einen
    Aufgabenbereich kümmern. Wenn niemand der ausgelosten Personen das
    nötige Wissen bzw. die erforderlichen Kenntnisse für die
    "Verwaltung" eines Aufgabenbereichs hat, wird die Auslosung
    wiederholt. Die ausgelosten Personen bilden dann eine Gruppe, welche
    die Verantwortung, die mentale Belastung und einen Teil der Arbeiten
    übernimmt. Idealerweise fühlt sich aber jede\*r Einzelne unabhängig
    der zugewiesenen Aufgabenbereiche ebenfalls für andere Aufgaben
    verantwortlich. Die gebildete Gruppe weiss vor allem darüber
    Bescheid, was bereits gemacht wurde und was noch gemacht werden
    muss. Jede Gruppe ist frei, sich nach ihren Vorstellungen zu
    organisieren. Nach jedem Quartal wird gewechselt.

-   Dadurch kann die mentale Belastung verteilt und die Organisation
    verbessert werden. Dieser Wechsel bietet allen die Gelegenheit, neue
    Sachen zu erlernen. Alle werden sich bewusst, was jede\*r Einzelne
    leistet. Jede Aufgabe bzw. Rolle wird gewürdigt. Frustration infolge
    verrichteter, unsichtbarer Arbeit fällt weg. Der Wechsel in kleinen
    Gruppen ermöglicht es zudem, dass jede\*r mit Personen
    zusammenarbeitet, mit denen er\*sie nicht zwingend eine Gruppe
    gebildet hätte.

Es ist an uns, neue Werkzeuge zu erfinden, sie anzupassen und Lösungen
zu suchen, die möglichst vielen entsprechen und auf alle Rücksicht
nehmen. Denn schlussendlich kommt die Lust, weiterhin politisch aktiv zu
sein, auch daher, dass es Freude bereitet, neue soziale Konfigurationen,
Zuhör- und Organisationsmethoden auszuprobieren.
