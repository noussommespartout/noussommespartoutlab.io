---
author: "Anonym"
title: "Drohnen"
subtitle: "Was an Feuerwerkskörpern gegen die Polizei so jämmerlich, aber doch so grossartig ist"
info: "Im Mai 2020 für den französischen Sammelband verfasst"
datepub: "Im Herbst 2021 auf Deutsch übersetzt"
categories: ["Demonstration, Riot", "Polizei", "Gewalt, Gewaltlosigkeit", "Selbstreflexion zum Aktivismus", "Sabotage, direkte Aktion"]
tags: ["Adrenalin", "Liebe, Verliebtheit", "Wohlwollen", "Mund", "Wut", "Körper", "Zweifel", "Drohne", "Wasser", "Empathie", "Feuer", "Kraft", "Horde", "Scheisse", "Wort", "Angst", "Macht", "Wut", "Strasse", "Strasse", "Theorie", "Theater", "Gewalt", "Gesicht", "Stimme"]
zone: "Suisse romande"
num: "1"
quote: "*Riots* machen es möglich, dir deine eigene soziale Realität wieder anzueignen."
---

Einmal habe ich einen Feuerwerkskörper auf eine Horde Bullen abgefeuert.
Das war gefühlsmässig wohl einer der intensivsten politischen Momente in
meinem Leben.

Es war in der Schweiz, während einer Demo. Wir demonstrierten während
gut einer Stunde. Um ehrlich zu sein, war der Demoblock nicht sehr
beeindruckend, nur wenige Menschen wurden auf uns aufmerksam. Aus dem
aufgebrachten Demoblock formierten sich immer wieder kleine, mobile
Gruppen, die in die Gassen ausschweiften und den Kastenwägen auswichen,
um das Sicherheitsdispositiv auszuhebeln. Die Anti-Riot-Polizei wurde
angegriffen und die Schaufenster von Banken mit frischer Farbe versehen
--- die Absicht dabei: sich das scheinbar Unerreichbare aneignen.

Die Polizei stand in Vollmontur bereit, ausgerüstet mit Wasserwerfern
und Gummischrot. Die Botschaft war wie immer eindeutig: Wir sind die
Bewaffneten, die Streitkräfte, deren Gewaltanwendung von Natur aus
legitim ist und von der Justiz gedeckt wird. Wir sind die Hüter der
etablierten Ordnung, die Strassen gehören uns. Wenn ihr es wagt, die
euch eingeräumten Grenzen zu überschreiten, wenn ihr es wagt, eure
Machtansprüche auszudrücken, dann schlagen wir euch kurz und klein. Die
Polizei hat kein Gesicht, menschliche Körper sind nicht erkennbar. Die
Design-Experten, die sich um ihr Erscheinen kümmern, entwickeln ihre
Uniformen zu genau diesem Zweck: Ihre Menschlichkeit verstecken. Doch
selbst wenn die Polizisten in Vollmontur furchteinflössend wirken, Angst
verbreiten sollen sie nicht. Ihr Erscheinen soll in der Bevölkerung
Akzeptanz geniessen. Wer am Fernseher mitbekommt, wie die Polizisten auf
Leute einschlagen und aus nächster Nähe auf Demonstrant\*innen zielen,
der vermutet keine Menschen. Würden die Polizisten den Menschen
gleichen, dann wären die Bilder viel schwerer zu verdauen. Sie würden
beim Fernsehpublikum einen bitteren Geschmack hinterlassen. Dann nämlich
wären am Fernseher Bilder von Menschen zu sehen, die --- auf Schweizer
Strassen und aus allernächster Nähe --- auf andere Menschen schiessen.
Doch ohne Gesicht, ohne Körper und Namen sind nur zweibeinige Drohnen zu
sehen. Eine Art Werkzeug, vielleicht so etwas wie der Idealtyp eines
Staatsbeamten, aber sicher keine Wesen, die Gefühle haben. Der Vorteil
von Robotern, abgesehen davon, dass sie gut aussehen, ist, dass sie das
Spektakel des freien Willens nicht inszenieren, sie reproduzieren einzig
das Bild einer kalt organisierten Gesellschaft, die wir ohne
nachzudenken akzeptieren, um dann einfach weiterzuzappen.

Es fällt mir schwer und ist fast ein wenig schmerzhaft, einen
belanglosen Feuerwerkskörper als einen der intensivsten politischen
Momente meines Lebens zu betrachten. Das meine ich ganz ehrlich. Ich
will dieses Gefühl nicht leugnen, doch meine persönliche Erfahrung
enttäuscht mich ein wenig. Ich freue mich nicht darüber, dass ich das so
intensiv erlebt habe.

Zunächst und vor allem, weil ich Gewalt angewendet habe. Mein
Knallkörper hat die Drohnen zwar kaum berührt, doch ich hätte ein
Gesicht oder einen Mund verletzen, ein Auge zerstören, etwas von der
Haut verletzen können, die sich hinter der Uniform dieser Drohnen
versteckt. Der Knallkörper hätte sich hinter einem dieser
Anti-Krawall-Schilder oder, noch schlimmer, einem Helmvisier einklemmen
können. Das ist in der Vergangenheit, hier in der Schweiz, schon
vorgekommen. Die politische Wut einer Person hat eine andere derart
deformiert, dass sich ihr ganzes Leben in ein Leben ohne Gesicht
verwandelte. Und all das nur, weil die Person eines Tages entschieden
hatte, dem Staat zu dienen. Es ist zum Weinen, so traurig ist das.
Traurig in einer Welt zu leben, in der Gewalt eine legitime und
notwendige Antwort auf Gewalt ist. Drei Jahrtausende politische Theorie,
um an diesen Punkt zu gelangen.

Dann bin ich aber auch von meiner persönlichen Erfahrung enttäuscht,
weil ich viele andere Momente erlebt habe, die ich als genauso politisch
intensiv hätte erfahren können. Ich habe an unglaublich interessanten
kollektiven Projekten mitgewirkt; nahm an Gesprächsrunden teil, bei
denen sich die Leute halfen, sich aufzurappeln; durfte Initiativen
begleiten, die mir aufzeigten, dass eine andere soziale Organisation,
basierend auf gegenseitiger Hilfe und Liebe, möglich ist. Doch das
Gefühl zu existieren, lebte erst dann in mir auf, als ich einen
Knallkörper aus chinesischer Produktion in einer belanglosen Strasse
anzündete. Einige werden sagen, es handle sich um eine "soziopathische
Schlägertruppe", eine verlorene Jugend, die sich austobt, indem sie
Schaufenster zerstört. Eine Generation, die sie *Clockwork Orange*
nennen werden, ohne zu bemerken, dass eigentlich sie die einzigen
Nihilist\*innen dieser ganzen Posse sind. Gar nichts haben sie
verstanden. Denn im Zentrum eines Aktivist\*innen-Lebens stehen
Freundschaft, gegenseitige Hilfe, das Sich-infrage-stellen, das
gemeinsame Nachdenken, sich selbst verändern sowie die Suche nach
Harmonie zwischen persönlicher Entfaltung und kollektiver Freiheit[^1].
Neunundneunzig Prozent Liebe und ein Prozent Feuerwerk. Was mich
ebenfalls traurig macht: Wir träumen von einer Welt ohne Feuerwerk, doch
mich erfüllt genau dieses Feuerwerk.

Schliesslich macht mich meine Erfahrung traurig, weil ich den
Adrenalinkick nicht wegreden kann. Es ist sozusagen die morbide
Genugtuung des Strassenkampfes. Innerhalb der militanten Linken sprechen
wir ganz selten darüber, was das mit uns macht, wenn wir uns diesem
Glücksgefühl hingeben. Die Freude, kollektive Mobilisierung zu erfahren
und zu spüren, ist grundlegend --- doch diese dunkle Seite thematisieren
wir nur ungern. Manchmal brechen wir in Tränen aus, nachdem wir den
Drohnen in den Strassen gegenüberstanden, wir sind von Angst geprägt,
auch wenn es sich bei den allermeisten Demonstrationen in der Schweiz
eigentlich nur um ein grosses Theater, eine Art Scheingefecht handelt.
Manchmal versuchen wir schlicht und einfach damit klar zu kommen, dass
es Teil unseres Kampfes ist, selbst wenn wir in unserer Utopie lieber
darauf verzichten würden. Manchmal schwingen wir grosse Reden, einige
sind richtiggehend abhängig geworden, die Muskelprotze singen ihr
eigenes Loblied --- willkommen bei der Männlichkeit des Feuerwerks. Aus
soziologischer Perspektive betrachtet, besteht der Schwarze Block aus
ungefähr einem Drittel Menschen, die keine Cis-Männern sind. Zudem haben
mehr als zwei Drittel einen (hohen) Universitätsabschluss --- doch das
ist ein anderes Thema. Das eigentliche Problem liegt aber in unserer
Vorstellungswelt. Wir wollen den Drohnen zeigen, dass wir nicht
niedergeschlagen sind, dass wir uns weder ihrer Dominanz unterwerfen,
noch ihre Weltvorstellung tatenlos akzeptieren wollen. Wir wollen ihr
Gefühl der Straffreiheit zerstören. Wir wollen, dass die Angst die
Seiten wechselt. Deshalb bieten wir ihnen die Stirn. So signalisieren
wir, dass wir das gewalttätige Spiel mitspielen und den Kampf annehmen.
Ganz nach der Logik, dass wenn es die einzige Sprache ist, die sie
sprechen, auch wir wissen, wie antworten. Wir wollen ihnen aufzeigen,
dass ihre Macht weder auf Wohlwollen, Empathie noch auf Austausch
beruht, sondern sie vielmehr eine Horde Drohnen sind, die erst dann ihr
wahres Gesicht zeigen, wenn das Maskenspiel zu Ende ist --- und das ist
erst der Fall, wenn wir damit anfangen, Feuerwerkskörper zu zünden. Doch
indem wir das tun, tappen wir unweigerlich in die Falle von Adrenalin
und (toxischer) Männlichkeit. Ihre Vorstellungswelt nimmt uns gefangen
und zwingt uns --- entgegen unserer Gefühlswelt --- einen Kampf zu führen,
der männliche und autoritäre Beziehungen reproduziert. Es ist extrem
schwierig, Gewalt auszuüben, ohne dabei in sich selbst affektive und
intellektuelle Bezugspunkte zu mobilisieren, die sich an (toxischer)
Männlichkeit und dem Gesetz des Stärkeren orientieren. Wir kennen kein
anderes Modell, um den uns aufgezwungenen Kampf zu verstehen, ihn zu
erproben und so seine Geschichte anders zu erzählen. Und das Resultat
kennen wir --- leidenschaftliche "Schwanzvergleiche": Wer wurde öfters
verhaftet, wer prügelte sich öfters mit den Bullen. Wenn du zugibst,
dass du nach jeder hektischen Demo weinen musst, dann wird es immer eine
Stimme geben, die sich auf kriegerische Werte wie Mut und Kraft beruft
--- damit du dich auch ganz sicher scheisse fühlst. Und genau diese
Stimme, die unbewusst die diskriminierende Vorstellungswelt der
Unterdrückenden stützt, genau diese männliche Stimme merkt nicht, dass
auch sie zu einer Stimme der Drohnen geworden ist. Und trotz allem
schwingt in meiner Geste, die ich als so unheimlich politisch erachte,
auch etwas von dieser (toxischen) Männlichkeit mit; dies zu akzeptieren,
fällt mir unheimlich schwer.

Darin liegt also der Grund der Traurigkeit und des Gefühlschaos, der uns
dazu veranlasst, einen lächerlichen Akt so intensiv zu (er)leben. Doch
abgesehen von der Traurigkeit: Was sind die Gründe, warum ich und andere
dies so intensiv erfahren? Zweifelsfrei stellen *riots* eine politisch
aussergewöhnliche Erfahrung dar, die nirgendwo sonst im täglichen Leben
präsent ist, egal wie militant wir sind. *Riots* machen es möglich, dir
deine eigene soziale Realität wieder anzueignen. Ein Gefühlsknall, der
dich spüren lässt, dass in den Strassen und zwischen den Mauern alles
möglich ist, was die Gruppe will. Eine kollektive
Selbstbestimmungserfahrung, dorthin zu gehen, wo es die Gruppe gerade
hinzieht, und dabei einzig dem Wir-Gefühl zu gehorchen. Und was passiert
dabei? Du empfindest dein Leben als lebendig, weshalb es genau das
Gegenteil von nihilistisch ist. Das spricht nicht nur Bände über all die
Bemühungen, die versuchen, in uns den Sinn der Existenz zu zerstören,
sondern auch Klartext über die soziale Konstruktion von Angst,
Selbsthass und angefaulter Entfremdung. Einen Feuerwerkskörper zu
zünden, entspricht einerseits der Höhenangst, die du gegenüber der
totalen Freiheit spürst, und andererseits dem Entscheid, mit beiden
Füssen auf dem Boden zu bleiben, deine Existenz zu beleben und durch den
du erst spürst, wer du bist. Was du anprangerst, ist viel mehr als ein
Gesetz, eine soziale Ordnung oder ein politisches System. Du prangerst
an, was deinem Leben angetan wurde. Ein Leben, das die gesamte
Menschheit übereinstimmend nun mal so definiert hat. Ein existenzieller
Werdegang in Form einer verhandelbaren Ware, die Summe von
Alltagshandlungen, die dir zutiefst fremd sind. All das gipfelt in der
Geselligkeit einer Welt, an der du teil nimmst ohne daran zu glauben,
ohne dich darin wiederzuerkennen, ohne dich dabei am Leben zu fühlen.

Meine Gefühle sind mir unbequem, doch es ist nun mal so. Es ist nun mal
so, in unseren sogenannten "demokratischen Gesellschaften", dass wir
daran gehindert werden, unser Leben in die eigenen Hände zu nehmen. Es
ist nun mal so, dass jegliches kritisches und affektives Gefühl im Kern
erstickt wird. Und es ist nun mal so, dass alles grau und abgeriegelt
ist. Einmal habe ich einen lächerlichen Feuerwerkskörper auf eine Horde
Bullen geworfen --- beunruhigt waren sie deswegen nicht. Das war
gefühlsmässig wohl einer der intensivsten politischen Momente in meinem
Leben. Und ich würde es ohne zu zögern wieder tun.

[^1]: Der Text *Wenn sich Raum a u s d e h n t* \[n. 27\] beschreibt diese Aspekte des Aktivist\*innenlebens ebenfalls.
