---
author: "Aladin Dampha"
title: "They don't see us"
subtitle: "Für sie sind wir unsichtbar"
info: "Transkription eines mündlichen Interviews --- Januar 2021 --- Original: Englisch"
datepub: "Im Herbst 2021 ins Deutsche übersetzt"
categories: ["Gefängnis, Justiz, Repression", "Squat, Besetzungen, Wohnen", "Migration und Kampf", "Antirassismus", "Polizei"]
tags: ["Verhaftung", "Kunst", "Vertrauen", "Film", "Fussball", "Kraft", "Ameise", "Frankreich", "Ungerechtigkeit", "Gerechtigkeit", "Maschine", "Nacht, Dunkelheit", "Pass", "Angst", "Macht", "Strasse", "Sabotage, sabotieren", "Telefon", "Gewalt", "Stimme"]
langtxt: ["de", "fr"]
zone: "Suisse romande"
num: "4"
quote: "Wird hierzulande Gewalt angewendet, handelt es sich in der Regel um Staatsgewalt. Und der Staat existiert nur wegen dem Volk."
---

Mein Kampf gegen Polizeigewalt nahm seinen Anfang im *Sleep-In*. An
diesem Ort schloss ich mich dem Kollektiv *Jean Dutoit* an, das sich für
Obdachlose einsetzt. Dort gab es ganz viele verschiedene Menschen:
hauptsächlich Geflüchtete und Migrant\*innen, die meisten aus
Westafrika, insbesondere aus Nigeria und Gambia. Irgendwann wurde der
*Sleep-In* zu klein, um allen Unterschlupf zu bieten. Deshalb hat sich
eine Bewegung aus uns, jenen Menschen, die draussen schliefen, und dem
Personal des *Sleep-In* formiert. Alle waren sich einig: Wir wollten
gemeinsam ein Haus besetzen. Das erste Haus, das wir besetzten, befand
sich in Lausanne, im *Quartier de Fourmi*. Nach drei Wochen mussten wir
von dort aber schon wieder weg, weil anstelle des Hauses eine Schule
gebaut werden sollte. Vor einigen Tagen hat mir eine Person erzählt,
dass die Räumlichkeiten noch immer leer stehen würden. Unterdessen waren
wir aber bereits in die ehemalige heineken-Fabrik in Renens
weitergezogen. Dort sind wir ein Jahr lang geblieben.

Erst dort haben wir so richtig angefangen, uns gegen Polizeigewalt und
die damit verbundenen Ungerechtigkeiten zu wehren und zu organisieren.
Durch mein Mitwirken im Kollektiv *Jean Dutoit* hatte ich die
Gelegenheit, mich mit Bullen, Regierungs- und
Verwaltungsvertreter\*innen usw. auszutauschen. Das war für mich zwar
etwas riskant, doch es war wichtig, diese Leute mit unseren Problemen zu
konfrontieren und ihnen unsere Forderungen näher zu bringen. Wir waren
bereit, dieses Risiko auf uns zu nehmen. Denn wir sind nicht
einverstanden, dass Weisse Menschen im Vordergrund stehen und den
Gesetzeshüter\*innen erklären, was wir brauchen. Denn wir, die in den
besetzten Häusern leben, wissen es doch viel besser. Ich persönlich nahm
an diesen Diskussionen immer teil und war dadurch in den politischen
Verhandlungen auch jeweils involviert. Einmal wurde ich sogar in der
Gratiszeitung *20 Minuten* abgebildet (lacht).

Später habe ich mich anderen Kämpfen und Kollektiven angeschlossen, die
sich gegen Rassismus und das Strafvollzugssystem einsetzen. Ich war Teil
eines *soundsystem*, das mit Partys Geld für Menschen im Gefängnis
sammelte; Menschen, die weder Geld noch eine Familie haben. Mit dem Geld
finanzierten wir u. a. Prepaidkarten fürs Telefon, Tabak oder Batterien.
Manchmal auch Anwält\*innen, sofern das nötig war.

Später haben wir das Kollektiv *Kiboko* gegründet, um
Zeug\*innen-Aussagen zu sammeln, die von Polizeigewalt berichteten.
Dabei haben wir uns gefragt, wie wir diese Erfahrungen öffentlich machen
können. Wir wollten etwas unternehmen, um unsere Rechte gegenüber der
Polizei zu verteidigen. Viele wissen gar nicht über ihre Rechte
Bescheid. Sie wissen nicht, was sie dürfen und was nicht. Die
Erfahrungsberichte schildern ein paar Vorfälle und wie die Polizei damit
umging, was dabei korrekt bzw. nicht korrekt ablief. Damit kann
verständlich gemacht werden, wie Schwarze Menschen Auseinandersetzungen
mit der Polizei erleben und wie sie sich bei Polizeipräsenz verhalten
sollen. Mehrere Personen haben vorgeschlagen, einen Film zu machen, der
die Situation und Aktionsformen zeigt, mit dem das System bekämpft
werden kann. Der Film heisst *No Apologies*. In ihm kommen mehrere
Personen zu Wort, die ihre Erfahrungen mit der Polizei schildern: Wie
sie mit deren Brutalität konfrontiert sind, wie sie Diskriminierung,
*racial profiling*, Ungerechtigkeit und Gewalt erleben.

Als ich in die Schweiz kam, war ich erstaunt darüber, wie wenig Leute
eine Ahnung haben, wie Polizeigewalt entgegengetreten werden kann und
welche Rechte sie haben. Wenn du keine Papiere hast, dann hast du weder
Privilegien noch Argumente in der Hand, um dich der Polizei
gegenüberzustellen. Eher fühlst du dich verantwortlich, weil du nicht
das Recht hast, hier zu sein. Doch wir dürfen nie vergessen, dass es ein
Menschenrecht ist, sich dort aufzuhalten, wo mensch will.

Auch wenn ich mich unterdessen offiziell in der Schweiz aufhalten darf,
hat sich die Situation nicht verändert. Die Polizei verhält sich
gegenüber mir, aber auch gegenüber anderen Schwarzen Personen, noch
immer gewalttätig. Wir werden als Menschen ohne Rechte betrachtet. Ich
erlebe noch immer Gewalt, mit dem Unterschied, dass ich mich nun
irgendwie dagegen wehren kann.

Einmal, es war Abend, so gegen 19 Uhr, war ich mit dem Zug aus Mailand
auf dem Weg in die Schweiz. Ich hatte alle nötigen Papiere dabei: mein
Ticket, meine ID, meinen Pass. Da ich nur zwei Tage in Italien war,
hatte ich nur wenig Gepäck dabei. Ich war der einzige Schwarze Mensch im
Zugabteil. Ein Kontrolleur bat mich um mein Ticket, das ich ihm ohne
Weiteres zeigte. Ich erinnere mich: Ich war gerade dabei, ein
Fussballspiel auf meinem Handy zu schauen. Der Kontrolleur machte den
Zollbeamten dann umgehend ein Zeichen und signalisierte ihnen:

--- Es ist ein Schwarzer im Zug.

Sofort kamen zwei Zollbeamte näher und sprachen mich auf Deutsch an. Ich
sagte ihnen:

--- Tut mir leid, ich spreche kein Deutsch.

Sie versuchten es mit Italienisch. Ich antwortete:

--- Ja, ich gehe nach Lausanne.

Dann baten sie mich um meine Papiere. Ich erwiderte:

--- Diese zeige ich ihnen sicher nicht. Ich sitze inmitten des
Zugabteils. Es ist also nicht so, als wäre ich die erste Person, die
ihr beim Betreten des Zugs kontrollieren müsst. Es gibt genügend
andere Leute, die ihr zuerst kontrollieren könnt, falls ihr die ID
überprüfen wollt.

Ich liess nicht locker:

--- Bitte sehr, ich bin doch nicht die erste Person im Zugabteil. Wenn
ihr eine Kontrolle machen wollt, dann wäre es doch nur normal, dass
ihr alle anderen kontrolliert, bevor ich an der Reihe bin.

Sie erwiderten:

--- Ja, aber wir müssen sie durchsuchen.

--- Nein, kontrollieren sie bitte alle anderen Leute, bevor ich an der
Reihe bin. Das wäre nur fair!

Sie liessen nicht locker und meinten, sie müssten mich durchsuchen. Ich
musste meinen Fussballmatch unterbrechen, das Handy ausschalten und
ihnen meine Papiere zeigen. Es ging ihnen einzig und allein darum, den
anderen Leuten zu zeigen, dass sie ihre Arbeit pflichtbewusst erledigen
würden. Ich zeigte ihnen meinen gambischen Pass. Dabei wusste ich ganz
genau, dass sie etwas anderes sehen wollten --- ich wollte sie ganz
einfach aufhalten.

Als ich auf meinem gambischen Pass beharrte, meinte einer der zwei
Zollbeamten:

--- Folgen Sie mir bitte!

--- Nein, ich bin nicht verpflichtet, Ihnen zu folgen. Geben Sie mir
meinen Pass zurück und ich gebe Ihnen was Sie suchen.

Er meinte:

--- Was wollen Sie mir geben?

--- Geben Sie mir meinen Pass zurück und ich gebe Ihnen was Sie suchen.

Schliesslich zeigte ich ihnen meinen Ausweis B. Doch damit war die Sache
nicht gegessen. Denn sie dachten, es handle sich um einen gefälschten
Ausweis. Sie überprüften ihn, tätigten einige Anrufe, und merkten dann,
dass alles in Ordnung ist. Der Kontrolleur musste mir also mein Gepäck
und meine Papiere zurückgeben.

Am Ende sagte ich ihm noch ganz beiläufig, dass er vielleicht auf Weisse
Personen ohne gültige Papiere gestossen wäre, hätte er zuerst andere
Leute kontrolliert und sich nicht auf mich gestürzt. Darauf reagierte er
nicht. Nach einigen Minuten, als der Kontrolleur schon gegangen war,
setzte sich ein Mann neben mich und meinte:

--- Wir haben keine Schweizer Papiere. Wir sind Italiener\*innen. Warum
wurden wir nicht kontrolliert?

--- Ihr seid nicht Schwarz, ihr seid Europäer\*innen. Deshalb ist es
ihnen egal.

--- Ja, aber wir sind in der Schweiz und sind keine Schweizer\*innen. Du
aber hast Papiere, die mehr wert sind als unsere.

Eine andere Geschichte. Sie ereignete sich in Lausanne. Meine
Waschmaschine war kaputt. Deshalb ging ich in einen Waschsalon in der
Nähe von mir. Dort musste ich eine Stunde warten. Um die Zeit zu
überbrücken, ging ich in die *Bibliothèque Chauderon*. Ich schaute mir
einen Film an. Auf dem Rückweg hielt ich kurz vor der Treppe, die zur
Busstation führt, an und zögerte den Bus zu nehmen. Ein Bulle hatte mich
beobachtet. Weil mir das Zögern wohl anzusehen war, dachte er, ich hätte
was zu verbergen und wollte abhauen. Forsch kam er auf mich zu und
sagte:

--- Kontrolle! Sie versuchen mir aus dem Weg zu gehen, Sie verkaufen
Drogen hier.

--- Wie bitte?

--- Ja, Sie sind hier am Dealen.

--- Nein, ich verkaufe keine Drogen. Ich vertreibe mir einzig die Zeit
und warte, bis meine Wäsche fertig ist.

--- Öffnen Sie Ihren Rucksack!

Weil ich mich weigerte, dies zu tun, drückte er mich sogleich heftig an
die Wand. Eine Freundin beobachtete die Szene, näherte sich uns und
fragte:

--- Was machen Sie mit ihm?

--- Er verkauft Drogen.

Er sagte zu mir:

--- Du lügst, nicht wahr?

Ich zeigte ihm meinen Ausweis, nahm meine Aufenthaltsbestätigung aus dem
Portemonnaie und schmiss sie auf den Boden. Er las sie auf und sagte:

--- Warum schmeisst du sie auf den Boden? Wolltest du weglaufen?

--- Nein, ich will ganz einfach nicht nett zu Ihnen sein. Sie haben
meine Papiere, doch das scheint ihnen völlig egal zu sein.

Er weigerte sich, meine anderen Papiere anzuschauen, mit uns zu sprechen
und meinte nur, wir sollten verschwinden. Damit waren wir nicht
einverstanden, denn dieser Ort gehört nicht ihm. Es war eine skurrile
Szene, denn alle Leute, die an der Busstation waren, beobachteten uns.
Das gab uns Vertrauen. Die Leute bekamen mit, wie sich die Polizei
verhielt. Sie sahen die Ungerechtigkeit mit ihren eigenen Augen und
verstanden, dass es keinen Grund gab, mich festzuhalten. Er sprach von
Drogen, obwohl Drogen überhaupt kein Thema waren. Die Leute konnten
meine Papiere sehen. Das tat mir persönlich sehr gut. Es war das erste
Mal, dass ich eine Auseinandersetzung mit der Polizei hatte und es
Augenzeug\*innen gab. Der Bulle blamierte sich und alle konnten sehen,
dass sein Verhalten falsch war.

Bereits vor der Veröffentlichung des Films *No Apologies* im Jahr 2015
kannten mich bereits einige Polizisten. Aus diesem Grund hatte ich keine
Angst, beim Film mitzuwirken und darin aufzutreten. Einige Bullen
schrien jedes Mal wenn sie mich sahen: "Dampha, du weisst doch, ich will
dich hier nicht mehr sehen." Und das im öffentlichen Raum. Manchmal
brachten sie mich in öffentliche WCs, manchmal schlugen sie mich,
manchmal liessen sie mich grundlos auf Bahnhöfen zurück. Sie liessen
mich oft leiden. Seit ich aber gültige Papiere habe und auf einen Bullen
treffe, bitte ich ihn, mich zu kontrollieren.

Ich hoffe, dass der Film *No Apologies* dazu beiträgt, die
Machtverhältnisse zu hinterfragen und zu verändern. Der Film will die
Meinung derjenigen Menschen ändern, die das Gefühl haben, wir seien
nichts wert. Die meisten Leute in der Schweiz wollen nichts dagegen
unternehmen und schon gar nicht helfen. Sie sprechen immer nur von der
Polizeigewalt in Frankreich oder den USA, denken aber weiterhin, dass in
der Schweiz alles "in Ordnung" wäre. Doch die Situation in der Schweiz
ist ganz und gar nicht "in Ordnung". Die Polizei ist gewalttätig, doch
das streiten alle ab.

Ich hoffe, dass der Film auch der Polizei die Augen öffnet. Natürlich
bietet der Film alleine keinen Schutz für uns. Ein einziger Film kann
nicht ein komplettes System umstürzen --- schon gar nicht eines das auf
Gewalt beruht und in dem sich die Leute unsicher fühlen. Menschen ohne
Papiere haben nicht das Recht zu arbeiten. Der einzige Ort, an dem sie
arbeiten dürfen, ist im Gefängnis. Oft kommen sie ins Gefängnis, weil
sie die Bussen nicht bezahlen können, die sie erhalten, wenn sie sich
illegal auf der Strasse aufhalten. Im Gefängnis verdienen sie ungefähr
drei Franken pro Stunde. Und sind sie einmal raus aus dem Gefängnis,
verweigert ihnen das Gesetz das Recht auf Arbeit. Das ist ein
Teufelskreis. Ihre einzige Möglichkeit ist es, illegal zu arbeiten. Sich
dieser Gefahr tagtäglich auszusetzen, ist aber ein grosses Risiko. Das
Asylsystem sieht vor, dass Sans-Papiers in dafür vorgesehenen Orten in
der Stadt untergebracht werden --- das ist ein rein politischer
Entscheid! Es wird alles unternommen, damit wir Schwarze uns unsicher
fühlen.

Die Geschichte des US-Amerikaners George Floyd wird ständig von allen
zitiert. Genau das gleiche Schicksal erfuhr aber auch Mike Ben Peter aus
Lausanne. Die Schweizer Medien berichteten monatelang zum Fall Floyd,
nicht aber zum Fall Mike. Umso mehr kämpfen wir dafür, dass im Fall Mike
Gerechtigkeit hergestellt wird. Wenn ein Schwarzer Mensch auf einen
Weissen Polizisten schiessen würde, dann würde das die Titelseiten der
Medien während mindestens einem Jahr füllen. Doch wenn ein Weisser
Polizist auf einen Schwarzen Menschen schiesst, dann ist das nicht
wirklich Thema, denn es war ja nur ein Schwarzer Mensch. So funktioniert
nun mal das System, das von der Justiz und den Anwält\*innen gestützt
wird.

Stell dir vor: Die Polizei hält dich wegen einem Gramm Gras fest. Vor
Gericht aber kreuzt die Polizei dann gar nicht erst auf. Ich kenne einen
Schwarzen Menschen, der wegen eines Gramms Gras angehalten wurde. Ein
Polizist hat daraufhin einen Rapport geschrieben, um festzuhalten, dass
er den Typen angehalten hat. Bei der Verhaftung war der Polizist dann
aber gar nicht dabei. Der Schwarze Mensch kommt also vor Gericht und
muss sich vor den Richter\*innen verantworten. Und alle denken, dass die
Polizei im Recht ist. Die Polizei wird nie verurteilt, sie schreibt
einzig Rapporte. Die Richter\*innen denken nach wie vor, dass Polizisten
die Wahrheit sagen. Egal was kommt bzw. was du sagst, der Entscheid der
Richter\*innen steht bereits fest.

Ich kenne noch eine andere Geschichte, in der die Bullen gelogen haben.
Als wir die Fabrik in Renens besetzten, wartete die Polizei jeweils bei
der Busstation auf uns. Einmal stieg ich in den Bus, wurde dann aber
sogleich von zwei Zivilpolizisten mit Gewalt aus dem Bus gezerrt. Sie
durchsuchten mich, fanden aber nichts. Dennoch nahmen sie mich auf die
nächstgelegene Polizeistation mit und sperrten mich ein. Dabei bekam ich
mit, wie ein Bulle am Telefon sagte:

--- Er hat zwei Gramm Gras.

Ich sagte:

--- Das ist eine Lüge. Sie haben mich doch soeben durchsucht und ich
hatte kein Gras dabei.

Diese Nacht musste ich auf dem Polizeiposten verbringen und am folgenden
Morgen brachten sie mich auf einen grösseren Posten, um mich komplett zu
durchsuchen. Ich meinte nur:

--- Verdammt noch mal, ihr seid Lügner. Warum bringt ihr mich hierhin
als wäre ich ein Schwerverbrecher? Wird das in meinem
Strafregisterbüchlein bis ans Ende meiner Tage vermerkt bleiben,
oder was?

Dann liessen sie mich gehen. Doch am nächsten Tag, am selben Ort,
hielten mich die gleichen Polizisten erneut an. Ich meinte nur:

--- Zum Teufel! Ihr habt mich gestern kontrolliert und gelogen. Was habt
ihr jetzt vor?

Sie antworteten:

--- Das ist unser Job. Unser Chef hat uns beauftragt, dies zu tun.

--- Nein, das ist nicht euer Job. Und euer Chef ist gar nicht hier. Hier
geht es nur um mich und euch.

Das ist nur eines von vielen Beispielen. Ich weiss, dass sich viele in
ähnlichen Situationen befinden, grundlos angehalten werden, obwohl sie
nichts gemacht haben. Schon ein Gramm Gras reicht, um ins Gefängnis zu
kommen. Wenn du keine Papiere hast und sie dich deswegen nicht ins
Gefängnis stecken können, lügen sie und sagen, du hättest Drogen
besessen. Das ist an der Tagesordnung, selbst wenn es illegal ist. Die
Statistiken zeigen, dass wegen solchen Vorkommnissen viele Schwarze
angehalten werden und ins Gefängnis kommen: Sie sind unschuldig und
landen nur dort, weil die Bullen gelogen haben. So läuft das in der
Schweiz!

Wird hierzulande Gewalt angewendet, handelt es sich in der Regel um
Staatsgewalt. Und der Staat existiert nur wegen dem Volk. Natürlich gibt
es auch in der Schweiz Leute, die hilfsbereit sind und dir die Hand
reichen. Doch der Staat leugnet die Existenz gewisser Geflüchteten. Aus
meiner Sicht sind Schweizer\*innen als Individuen cool, zumindest die
meisten. Doch der Staat macht es jenen, denen er keine Rechte zuspricht,
sehr schwer. Sie haben keine Priorität! Staatsentscheide werden nicht
zugunsten von Schwarzen oder *People of Color°* gefällt. Sie werden
ignoriert.

Im Alltag kannst du eine Polizeikontrolle sabotieren, indem du
Polizisten aufhältst und ihnen wertvolle Zeit nimmst. Doch aus meiner
Sicht müssen wir das gesamte System abschaffen. Dieses sorgt dafür, dass
jene unterdrückt werden, die kaum Privilegien haben: jene, die keinen
Schweizer Pass haben, Minderheiten und Schwarze.

Doch können wir tatsächlich gegen ein System kämpfen, dass Leute
grundlos einsperrt? Wir müssen unsere geistige Frische, unsere Stimmen,
unsere Schreibstifte und die Kunst dafür einsetzen, um öffentlich zu
machen, was diesen Leuten widerfährt. Ich hoffe, dass viele diesen
Sammelband lesen werden und andere wiederum Lieder schreiben oder Filme
machen, um auf die Missstände hinzuweisen. Wir müssen versuchen, alles
in Waffen für unseren Kampf zu verwandeln. Du kannst dich beispielsweise
fragen: Wo stehe ich selbst innerhalb dieser Gesellschaft? Was liegt in
meiner Macht? Wenn du beispielsweise als Jurist\*in tätig bist, dann
hast du viel Macht.

Das System kaschiert Dinge, um seine Legitimität zu wahren. Wir müssen
die Wahrheit aufzeigen und sie von Grund auf neu erschaffen. Wir müssen
alles ändern, auch die Polizei. Warum werden eigentlich Polizisten nie
bestraft? Nehmen wir sie fest und bringen wir sie vor Gericht. Das
sollte doch möglich sein! Ich werde für ein Gramm Gras festgenommen und
die Polizei tötet grundlos einen Schwarzen Mann! Doch Polizisten werden
nie zur Rechenschaft gezogen. Ist das wirklich gerecht? Wird das Gesetz
für alle gleich angewendet? Die Polizisten, die Mike getötet haben,
patrouillieren nach wie vor in den Strassen Lausannes. Solange sie nicht
verurteilt werden, glauben sie, dass sie ihre Hände in Unschuld waschen.

Die Leute sagen, dass Polizisten, und das ist ganz nett ausgedrückt,
"schlecht" seien. Doch sind wir ehrlich: Wenn du eine "gute" Person
bist, dann wirst nicht Polizist. Wenn du eine "gute" Person bist, dann
wirst du nicht zu einer Person, die sagt: "Ich habe dich getötet, Folgen
hat das keine. Doch damit kann ich gut leben."
