---
author: "Stuart"
title: "Solidarität, Solidarität, Solidarität"
subtitle: "Überlegungen zur Theorie und Praxis eines politischen Prozesses am Beispiel von *Basel Nazifrei*"
info: "Für den deutschen Sammelband verfasst"
datepub: "Im Dezember 2021 verfasst"
categories: ["Antifaschismus", "Gefängnis, Justiz, Repression", "Institutionelle Kämpfe"]
tags: [""]
langtxt: ["de"]
zone: "Deutschschweiz"
num: "66"
quote: "Entweder man ist antifaschistisch oder nicht."
---

Als Vorbereitung auf meinen Prozess nahm ich an Treffen von Betroffenen
teil.[^1] Da die Prozesse bekanntlich alle einzeln geführt wurden, war
dies ein wichtiger Schritt, um den Kampf auch weiterhin kollektiv zu
führen. Es war auch ein guter Ort, um sich auszutauschen betreffend
allerlei Fragen, die sich durch die ganze Korrespondenz mit der
Staatsanwaltschaft, den Gerichten oder der\*dem Anwält\*in ergaben und
bei denen ich oftmals immer nur noch Bahnhof verstehe. Ein Input einer
erfahrenen Genossin der *Roten Hilfe* klärte uns über den politischen
Prozess auf. Dazu orientierte sie sich an der Broschüre der *Roten
Hilfe* aus dem Jahr 2011 «Der politische Prozess: Theorie und Praxis,
Prinzipien und Taktik».

Ich möchte demzufolge anhand einiger Ausschnitte dieser Broschüre
aufzeigen, wie ich den politischen Prozess in Basel erlebte und
wahrnahm.

> «Der Prozess ist ein Moment der Konfrontation mit der Bourgeoisie und
> dem Staat. Der Prozess dient nicht dazu, die 'Wahrheit' zu ermitteln,
> sondern die Macht der Klasse zu bestätigen und fortzuschreiben. Dies
> muss der Ausgangspunkt jeder Analyse des politischen Prozesses im
> Allgemeinen und zur Wahl einer Strategie eines bestimmten politischen
> Prozesses sein.
>
> Die Bourgeoisie unterscheidet zwei Arten von Repression: diejenige,
> die als Ziel die Zerstörung jedes Versuchs hat, eine revolutionäre
> Organisation aufzubauen und diejenige, die versucht, die
> Klassenkonflikte in den Grenzen der Legalität, die sie selbst
> abgesteckt hat, zu halten.»

In diesem Fall trifft eher zweites ein. Es handelt sich um keinen
Klassenkonflikt, sondern um den antifaschistischen Kampf, der nur in dem
vom Staat vorgesehenen Rahmen ablaufen soll. Das lässt sich dadurch
illustrieren, dass die Gegenkundgebung der JUSO nur fernab vom Geschehen
stattfinden durfte. Würden doch die meisten Richter\*innen von sich
behaupten, nichts mit Faschismus am Hut zu haben und dass Widerstand
dagegen in irgendeiner Form wohl doch ganz gut sei.

> «Kein bürgerliches Gericht wird jemals auf der juristischen Ebene
> einem revolutionären Kampf irgendeine Gültigkeit zusprechen können,
> einem Kampf, der die Verneinung dessen ist, worauf es sich begründet.\
> Ein Prozess ist aus seinem Kern heraus politisch, erst recht ein
> Prozess, der eine politische Aktivität, also ein politisches Projekt
> zum Inhalt hat. Es gibt keine neutrale Position.»

Ich bin der Meinung, dass es zwischen Faschismus und Antifaschismus
eigentlich nichts gibt. Entweder man[^2] ist antifaschistisch oder
nicht. Aber 2/3 Faschist\*in und 1/3 Antifaschist\*in zu sein,
widerspricht jeder Logik. Sowohl jener des\*der Faschisten\*Faschistin,
wie jenes von Antifaschist\*innen. Diese Position sollte auch in der
Verteidigung zum Ausdruck kommen. Dazu ist es ratsam, sich Anwält\*innen
auszusuchen, die politisch als links einzuordnen sind. Dafür wendet man
sich am besten an lokale Antirepressionsstrukturen oder lokale
politische Akteur\*innen. Solidarische Anwält\*innen sind meistens auch
günstiger als andere. Die Position, die vor Gericht vertreten werden
sollte, muss dann gemeinsam mit der Verteidigung erarbeitet oder
besprochen werden.

In meinem Fall reduzierten wir diese Besprechung auf den Vorabend des
Prozesses. Dies ist sicherlich nicht die optimalste Lösung, gerade wenn
man vielleicht nervös ist. Da mein Anwalt jedoch schon mehrere Prozesse
in dieser Reihe geführt hatte, mussten wir das Rad nicht neu erfinden.
Die positive Seite an wenig Korrespondenz mit der Verteidigung ist
lediglich die tiefere Rechnung.

> «Worum es in einem politischen Prozess wirklich geht, ist die
> politische Identität des\*der Angeklagten. Durch den Prozess, vor der
> richterlichen Sanktion, zielt die bürgerliche Macht, vertreten durch
> die Staatsanwaltschaft, auf die politische Zerstörung der kollektiven
> revolutionären Identität, indem sie versucht, den 'kriminellen'
> Charakter zu betonen, auf der Basis der Verteufelung der Ziele (die
> 'demokratische' Ordnung, die 'bestmögliche' Ordnung umzustossen) und
> der Taten (spezifische Straftaten), die zur Erreichung der Ziele
> angewandt werden.»

Die wildgewordene Staatsanwaltschaft verkörpert dies absolut. Aus einer
halbleeren Bierdose wurde eine gefährliche Waffe konstruiert, während im
gleichen Zug die Ansichten der Partei national orientierter Schweizer
(PNOS) als wirres Geschwafel legitimiert wurden. Auch das alle Prozesse
einzeln geführt wurden, schlägt in diese Kerbe. Es wurde versucht die
kollektive Identität zu umgehen. Genauso wie die an den Tag gelegte Art
von Protest und Haltung als antidemokratisch delegitimiert wurde.

Übertriebene Repressionsmassnahmen wie im vorliegenden Fall haben ganz
klar zum Ziel, die antifaschistische Bewegung als Ganzes zu schwächen
und einzuschüchtern. Drakonische Strafen können Betroffene abschrecken.
Ebenso das persönliche Umfeld oder einfach alle, die davon Wind
bekommen. Als gutes Beispiel dient hier die Öffentlichkeitsfahndung, die
in Basel angewendet wurde, um einige vermutliche Teilnehmer\*innen zu
identifizieren. Auf der Arbeit im Betrieb am Tisch zu sitzen, während
die Mehrheit sich über dieses «Pack», das in der Zeitung abgebildet ist,
auslässt, kann junge Menschen nachhaltig prägen. Der lange Arm der
Strafverfolgung und ihrer Propaganda reicht somit bis in gewisse Medien.
Diese versuchen dann die bürgerlichen Ideale zu verteidigen, indem sie
zum Beispiel extremismustheoretisch argumentieren und die konsequenten
linken Antifaschist\*innen in die gleiche Ecke stellen wie die Nazis.

Gerade darum ist es wichtig, sich zu organisieren. Es ist unsere
politische Aufgabe, der ganzen Hetze in der Öffentlichkeit und vor den
Gerichten ein starkes Zeichen entgegenzuhalten. Dies ist zum einen
wichtige Öffentlichkeitsarbeit für den (revolutionären) Antifaschismus
und zum anderen wichtige Solidaritätsarbeit für die Angeklagten. Ziel
sollte es sein, durch diese politische Prozessführung von unserer Seite
Menschen für zukünftige Aktionen zu gewinnen, und antifaschistische
Kämpfe auf der ganzen Welt zu verbinden.

> «Eine Schwachstelle für die Bourgeoisie bei der legalen Repression,
> vor allem was den politischen Prozess angeht, kommt vom öffentlichen
> Charakter der gerichtlichen Repression und also vom Risiko, dass die
> involvierten Personen und der ganze Prozess durch den Klassenkampf und
> die 'äusseren' Kräfteverhältnisse beeinflussbar sind.»

Hier kommt die Stärke der Solidaritätsarbeit zum Tragen. Mit den
öffentlichkeitswirksamen Kampagnen wie *500k* und *Basel Nazifrei*
konnte ein Diskurs losgetreten werden. Unsere Anliegen waren und sind
omnipräsent. Dies konnte die Betroffenen mental unterstützen und den
Antifaschismus in der Öffentlichkeit stärken. Auf die Urteile hatte dies
nur geringen Einfluss, jedoch ist es unbestritten, dass nach dem
skandalösen Urteil einer unbedingten Haftstrafe der Druck von unserer
Seite nochmals gestiegen ist. In diversen Medien wurde dieses Urteil
thematisiert und kritisiert. Durch diese Präsenz konnte eine objektive
Darlegung der Situation Raum einnehmen. Denn in den bürgerlichen Medien
wird zu oft die Sicht der Polizei bzw. der Staatsanwaltschaft unkritisch
übernommen. So kam an die Öffentlichkeit, dass es betreffend den zu
fällenden Urteilshöhen vermutlich Absprachen zwischen den Richter\*innen
gegeben haben soll. In unterschiedlichen Zeitungen und auch im Fernsehen
wurde dies zum Thema, was die Justiz gewissermassen ein wenig in die
Defensive zwang. Dies führte in den kommenden Verfahren noch immer zu
hirnrissigen Urteilen, jedoch lediglich zu hohen Bewährungsstrafen.

> «Auf proletarischer Seite misst sich das politische Resultat vor
> allem ausserhalb des Gerichtshofes, und es betrifft die Stärkung oder
> Schwächung der bürgerlichen Macht, die der Prozess im allgemeinen
> Rahmen des Klassenkonflikts bestimmen kann. Zweitens misst er sich im
> Innern mit der technisch-politischen Verteidigung, die darauf
> hinzielt, den von der Anklage geführten Angriff im Rahmen zu halten
> (eventuell verringerte Strafen, Freisprüche).»

Ausserhalb des Gerichts konnte die Solidaritätsarbeit zu einer
Diskussion über die Notwendigkeit eines praktischen Antifaschismus
beitragen und diese prägen. Die bürgerliche Macht wurde dabei nicht
geschwächt, jedoch unsere Position gestärkt und einer breiten Masse
zugänglich gemacht, was als politischer Gewinn betrachtet werden kann.
Durch unterschiedliche Kampagnen wie *500k: heillos verschuldet*, die in
erster Linie das Ziel hatte 500'000 Franken für die Angeklagten zu
sammeln, oder die offizielle *Basel Nazifrei*-Kampagne, die viel
Antirepressionsarbeit und in erster Linie politische Solidaritätsarbeit
verrichtete, konnte der Kontakt mit solidarischen Menschen hergestellt
werden. Die unzähligen Gespräche mit Menschen an unseren
*500k*-Infoständen an Festivals oder (eigenen) Anlässen,
Infoveranstaltungen usw. sowie Diskussionen in meinem privaten Umfeld
sind für mich das beste Beispiel dafür, was erreicht werden kann. Die
Verteidigung im Gerichtsaal ist jedoch wieder eine individuelle Sache.
Klar gibt es Grundsätze einzuhalten wie keine Distanzierung und
Delegitimierung des Protests, keine Zusammenarbeit und Aussagen usw. Es
gibt jedoch Menschen, die unter keinen Umständen (wieder) ins Gefängnis
oder ausgeschafft werden möchten, oder für Taten angeklagt werden, die
sie schlicht und einfach nicht begangen haben.

> «Die Waffen der Angeklagten sind ihre politische und ideologische
> Entschlossenheit (und was das beinhaltet: Gemeinschaft,
> Opferbereitschaft usw.), die Unterstützung von ausserhalb und die
> Widersprüche des Gegners.»

Gerade was das Mentale betrifft, war dieser Punkt für mich von grosser
Bedeutung. Überzeugt zu sein, das Richtige getan zu haben, und dies auch
zu erfahren, stärkte mich in der ganzen Zeit. Durch die
Solidaritätsarbeit vergass ich die eigene Betroffenheit, und das
Politische, die antifaschistische Arbeit, trat wieder in den
Vordergrund. Mein eigener Prozess war für mich zum Beispiel in dieser
ganzen Zeit das absolut Irrelevanteste.

## Unterschiedliche Strategien

Es gibt unterschiedliche Strategien, wie ein Prozess geführt werden
kann. In der Broschüre sind verschiedene Arten aufgelistet. Ich werde
zwei herausgreifen, die meiner Ansicht nach am besten auf meine
Erfahrungen zutreffen.

> «Der kombinierte politische Prozess
>
> Im Falle, dass Prozesse es erfordern, mehr politisch-ideologische als
> juristische Ziele zu erreichen --- wegen verschiedenen Umständen
> (politische Situation, Qualität der Militant\*innen, öffentliche und
> mediale Resonanz\...) ---, aber die juristischen doch rücksichtsvoll
> behandelt werden sollten, ist es die angewandte Strategie, am Prozess
> 'teilzunehmen', indem er in eine politische Bühne verwandelt wird und
> jede Gelegenheit zu sprechen benutzt wird, um das juristische System
> als Funktion eines unterdrückenden und ausbeuterischen Systems
> anzuprangern und die revolutionären Thesen zu verteidigen.\
> Die rücksichtsvoll zu behandelnden juristischen Ziele können alle
> oder einen Teil der Angeklagten betreffen, wie im Fall von
> 'Maxiprozessen', in denen die juristische Situation der Angeklagten
> extrem unterschiedlich sein kann. Die Kombination verschiedener Formen
> von Prozessen erlaubt es, sich den Anklagen mit den vom juristischen
> Apparat gelieferten Waffen zu widersetzen, gleichzeitig aber die
> politisch-ideologische Schlacht zu führen.»

Diese Strategie trifft wohl am ehesten zu, da es sich quasi um einen
«Maxiprozess» handelte. Was hervorzuheben ist in diesem Fall, ist sicher
die öffentliche und mediale Resonanz, die absolut gegeben war. Dadurch
konnte ein permanenter politischer und juristischer Druck
aufrechterhalten werden. Für die meisten Prozesse wurde öffentlich vors
Gericht mobilisiert, Transparente gehängt, Parolen gerufen und
Botschaften der Angeklagten vorgelesen. Genauso wurde individuell
berücksichtigt, wo es Sinn machte die juristische Situation mehr zu
gewichten (natürlich ohne sich politisch zu distanzieren). Die ganze
Prozessbegleitung konnte über die ganze Zeit aufrecht erhalten werden
und hat unzählige Menschen zur Solidarität bewogen.

> «Der legalistische politisierte Prozess
>
> Es handelt sich um eine Variante des vorher behandelten. Die
> Angeklagten und ihre Verteidigung unterwerfen sich den normalen
> Prozeduren, d. h. der technischen Untersuchung der beurteilten Taten,
> aber sie gehen detailliert auf die Natur und die politischen Motive
> der Taten ein. Diese Strategie versucht also, den juristisch-
> strafrechtlichen und den politischen Gewinn zu kombinieren. Sie ist
> naheliegend für radikal-reformistische Kräfte. Es ist ein Modell, das
> sich gut an eine Vielzahl von Prozessen der 'Bewegung' anpasst,
> während deren spezifische Episoden des Kampfes beurteilt werden (Demo,
> die in Zusammenstössen endete; Schläge, die einem Fascho verabreicht
> wurden; Strassenblockaden usw.), aber es ist eine fast unmögliche
> Formel für Angeklagte, die sich gegenüber dem Gericht zu einem
> globalen revolutionären Projekt bekennen und dieses verteidigen. In
> Wirklichkeit geht der revolutionäre Prozess von einer Zurückweisung
> des Gesetzes und des Gerichtsapparates aus, der ein Zahnrad der
> Klassenunterwerfung ist.»

Meinen eigenen Prozess könnte man, wenn man ihn isoliert betrachtet,
auch in diese Kategorie stecken. Mein Anwalt stellte die politische
Situation ganz klar dar und versuchte aufzuzeigen, was die PNOS ist und
für was sie steht. Ebenso verurteilte er den Polizeieinsatz und die
strafrechtliche Verfolgung. Dennoch plädierte er auf einen Freispruch
und erklärte, dass ich nicht die Person auf den unkenntlichen Fotos sei.
Ich bekannte mich so quasi vor den drei Richter\*innen und dem
Staatsanwalt nicht zu einem revolutionären, internationalen
Antifaschismus, präsentierte mich aber als Teil der ganzen politischen
Solidaritätskampagne (öffentliche Prozessmobilisierung mit circa 80
solidarischen Menschen vor Gericht) und als revolutionärer politischer
Mensch, mit klarer antifaschistischer Haltung. Für mich war die
Verhandlung und alles, was im Gericht geschah einfach nicht wichtig ---
ich hätte da sowieso gegen eine Wand geredet. Ich denke, dies ist auch
der Tatsache geschuldet, dass aufgrund von Covid keine Besucher\*innen
zugelassen wurden, was den Raum ausserhalb des Gerichts für mich als
politisch viel relevanter werden liess.

[^1]: Der Text *Wir sind alle antifaschistisch* \[n° 65\] erzählt was
    bei *Basel Nazifrei*, vom Protest bis in den Gerichtssaal, alles
    ablief.

[^2]: Bemerkung der schreibenden Person zur Verwendung von «man»
    anstelle von «mensch»: Ich verstehe die Absichten hinter der
    Verwendung des Begriffs «mensch» anstelle des neutralen «man». Ich
    betrachte es jedoch lediglich als identitätspolitische Spielerei
    denn als logisch nachvollziehbare sprachliche Lösung eines Problems,
    das eigentlich gar keines ist. Daher möchte ich diese Form in meinem
    Text nicht anwenden.
