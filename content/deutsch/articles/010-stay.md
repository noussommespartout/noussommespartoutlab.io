---
author: "Emmathegreat"
title: "Stay"
subtitle: "Gedicht"
info: "Im Januar 2021 für den französischen Sammelband verfasst"
datepub: "Im Herbst 2021 auf Deutsch übersetzt"
categories: ["Antirassismus", "Selbstreflexion zum Aktivismus", "Migration und Kampf"]
tags: ["Sex", "Liebe, Verliebtheit"]
langtxt: ["de", "fr"]
zone: "Suisse romande"
num: "10"
quote: "Zeig mir die Abgründe deiner Seele, ich akzeptiere sie"
---

If I knew what I know now, I would have walked up to Her and say
“Stay”.

These could be the worst parts of me, I have some best parts. You want
them? then “Stay”.

There were some adorable ones. They left, they couldn't wait, maybe
because I never said “Stay”.

Show me Your worst side, I'll take it, because if I don't, then I
don't deserve your best side. I'll never leave as long as you
“Stay”.

“Stay!” Like it's the only thing I ask for.

“Stay!” Like every good sex we have, we want more.

“Stay!” Just like how we forget about the bad times while having some
good times.

“Stay!” Like you're the only road I took that leads me Home. Not Rome.

“Stay!” I'll remember how long you stay. How you do the things that you
do, that make me call you “Chouchous” In Switzerland we say “Coucou”. My
native name is Ebuka, but relatives call me “Bubu”.

“You're a nice guy”. That's what they all say.

“You have a big heart”. But they can have the same.

“l love You”. That's what we all say.

“Stay!” That's what I'll do.

## Bleib

Wenn ich gewusst hätte, was ich heute weiss, dann wäre ich Ihr
gegenübergetreten und hätte gesagt: "Bleib".

Das ist vielleicht die schlimmste Seite von mir, doch ich habe auch gute
Seiten. Du willst sie entdecken? Dann "Bleib".

Es gab mal ein paar bezaubernde. Sie sind gegangen, sie konnten nicht
warten, vielleicht weil ich nie gesagt habe: "Bleib".

Zeig mir die Abgründe deiner Seele, ich akzeptiere sie. Denn wenn ich es
nicht tue, dann verdiene ich nicht dein Bestes. Ich verlasse dich nicht,
solange du "Bleibst".

"Bleib!" Als wäre es das Einzige, was ich von dir verlange.

"Bleib!" Als würden wir nach jedem guten Sex noch mehr wollen.

"Bleib!" Als gingen die schlechten Tage vergessen, wenn alles in Ordnung
ist.

"Bleib!" Als wärst du der einzige Weg, der mich zu mir selbst führt.
Nicht nach Rom.

"Bleib!" Ich erinnere mich an die gemeinsam verbrachte Zeit. An all das,
was du für mich getan hast. Deshalb nenne ich dich "Chouxchoux". In der
Schweiz sagen wir "Coucou". Mein richtiger Name ist Ebuka, meine Lieben
nennen mich "Bubu".

"Du bist ein sympathischer Typ." Das sagen sie alle.

"Du hast ein grosses Herz." Auch sie können eines haben.

"Ich liebe dich!" Das sagen wir alle.

"Bleib!" Das werde ich.
