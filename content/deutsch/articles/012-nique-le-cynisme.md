---
author: "Anonym"
title: "Fuck Zynismus"
subtitle: "Und alle Zyniker*innen, die auf dem Mars leben wollen"
info: "Für den französischen Sammelband im Februar 2021 verfasst"
datepub: "Im Herbst 2021 auf Deutsch übersetzt"
categories: ["Selbstreflexion zum Aktivismus"]
tags: ["Liebe, Verliebtheit", "Bank", "Arsch", "Maul", "Logik", "Mutter", "Haar"]
langtxt: ["de", "fr"]
zone: "Suisse romande"
num: "12"
quote: "der Mars ist uns doch scheissegal"
---

Linksradikaler Denkweise wird oft vorgeworfen, sie sei zu starr, wolle
immer Recht haben und hätte für alles eine Lösung parat. Ich denke ---
und das gebe ich gerne zu ---, dass ich in vielen Sachen recht habe. Wenn
du politisch aktiv bist und etwas bewegen willst, dann musst du davon
überzeugt sein, dass du zumindest ein klein wenig recht hast. Es ist
doch nichts falsch daran, zu meinen, dass mensch recht hat. "Überzeugt
von etwas sein", so könnte ich es auch ausdrücken, ist doch eine gute
Eigenschaft. Etwas, das mensch sogar in seinen Lebenslauf schreiben
könnte.

Um einen blassen, billigen Schweizer Konsens zu erzielen, möchte ich
zwei Beispiele anfügen:

In meinem tiefsten Inneren glaube ich, dass ich recht habe, wenn ich
gegen Multimilliardäre rebelliere. Während sie sich ein schönes Leben
machen, stecken viele Arme in der Scheisse. In meinem tiefsten Inneren
glaube ich auch, dass ich recht habe, wenn ich die Todesstrafe ablehne,
und mich für eine gerechtere, sozialere und glücklichere Gesellschaft
einsetze.

Mensch wird mir entgegenhalten: "Aber natürlich finden das alle
ungerecht. Das ist doch die klassische linke Argumentation. Das Monopol
für feinfühlige Argumente gehört aber nicht euch." Okay, aber was heisst
denn schon "die Linggi" ("die Linke")? Damit eines klar ist: Falls dies
die vorherrschende Argumentation ist und wir diese teilen, hiesse das
dann nicht, dass wir uns alle einig sind? Doch wenn wir uns schon alle
einig sind, warum gibt es denn noch immer Multimilliardäre, die sich ein
schönes Leben machen, und Arme, die in der Scheisse stecken? Meine Logik
ist gnadenlos. Doch wenn du mir in diesem Sinne antwortest, dann heisst
das doch eigentlich, dass du diese Argumentation nicht *wirklich* teilst
und sie nicht *wirklich* ungerecht findest.

"Ja, aber es ist doch alles viel komplexer." Okay, was vielleicht
komplexer scheint, ist, jene Strukturen zu verändern, welche die
bestehenden Verhältnisse stützen. Doch wenn du grundsätzlich
einverstanden bist, warum bist du denn nicht am Rebellieren, so wie ich?
Und vor allem: Warum bist du nicht einverstanden mit mir? Warum begnügst
du dich mit der Aussage "ich meistere die Komplexität", statt mit mir zu
rebellieren?

Dem zynischen Banker, der seine Kernfamilie und damit die Arbeiterklasse
verraten hat, möchte ich am liebsten einen Pflasterstein in die Fresse
schmeissen. Er, der meint, es sei normal, dass seine Mutter nur ein
Fünftel seines Einkommens verdient... Anstatt zu studieren, hat sie ihn
auf die Welt gebracht und mit Muttermilch und Liebe gefüttert. Ich habe
den Eindruck, dass er sich für eine Seite entschieden hat. Klar spielt
da auch die Werbung mit, seine eigene Bank, die grosse ideologische
Trägheit der Demokratie, doch schlussendlich hatte er doch die Wahl.
Seine Logik funktioniert und dank seiner Intelligenz kann er üble,
meritorische Prinzipien von sich geben. Er hat gut lachen, weil er alle
Zügel in der Hand hält, um einverstanden zu sein. Ich verlange nicht,
dass er sich politisch engagiert. Und schon gar nicht, dass er
70 Franken an Greenpeace spendet. Ich bitte ihn doch nur, einverstanden
zu sein, mir nicht zu widersprechen und ständig freudig zu wiederholen:
"Ja, aber es ist doch alles viel komplexer", um dann sorgenfrei in seine
Bank zurückzukehren.

Ich bin traurig und genervt, weil ich diese Aussage nicht verstehe. Ich
schaffe es nicht, mir selbst zu erklären, warum nicht alle meiner
Meinung sind, warum nicht alle rebellieren. Doch eigentlich ist diese
Denkweise bescheuert: Ich sage nicht "ich bin intelligent", ich sage nur
"ich habe recht".

Von den armen Bankern, die fünfmal mehr verdienen als meine Tante, gibt
es ganz viele; Multimilliardäre dagegen deutlich weniger. Ich glaube,
wir wären in der Lage, all dieses Geld zu verteilen. Geld, das in
schwachsinnige Projekte gepumpt wird, um in weissen, glatten, hässlichen
Kapseln auf den Mars zu reisen. Seien wir ehrlich: Der Mars ist uns doch
scheissegal. Solange es auf dieser Erde Menschen gibt, die vor Hunger
sterben, die kein Dach über dem Kopf haben und sich nicht frei bewegen
dürfen, scheissen wir doch auf den Mars. Dort gibt es doch nichts, das
uns wirklich weiterbringen könnte, einzig roter Staub, der sein *life*
ausserhalb des Anthropozäns lebt. Dort gibt es (noch) keine
Multimilliardäre zu plündern, keine Unternehmen zu verändern, keine
Wälder zu retten. Und auch keine Menschen zu lieben!

So... das ist mein täglich wiederkehrender, andauernder Protestschrei.
Er muss raus, wenn ich die Nachrichten schaue, wenn ich in
Bonz\*innenquartieren spazieren gehe, wenn ich den Klimawandel hautnah
erlebe, wenn ich lese, trinke, esse, wenn ich einkaufen gehe, wenn ich
all diese leidenden Tanten und fröhlichen Banker-Cousins sehe.

Und manchmal halte ich es einfach nicht mehr aus. Dann blähe ich mich
wie ein Heissluftballon auf, möchte den Ballast abwerfen, in die Höhe
steigen, um mich mit ihren Raketen zu messen. Doch ich schaffe es nicht.
Ich schaffe es nicht, den Ballast abzuwerfen. Er ist zu schwer. Die
Knoten, die ihn halten, sind zu fest. Sich dessen bewusst zu werden, ist
nicht einfach, schon gar nicht, wenn mensch nicht wie ein Gutmensch
klingen möchte.

Es geht nicht um standardisierte, floskelhafte Argumentationen, es ist
real und vor allem *fucking* rational.

Doch nur Zynismus ist ir-rational.

Das wär's von mir!

Politisch aktiv zu sein, heisst: Fuck Zynismus.
