---
author: "Verein *Medina*"
title: "Für eine Welt, in die viele Welten passen"
subtitle: "Einblicke in das Projekt *Medina*"
info: "Für den deutschen Sammelband verfasst"
datepub: "Im Dezember 2021 verfasst"
categories: ["Selbstorganisation, kollektives Experimentieren", "Migration und Kampf", "Gefängnis, Justiz, Repression"]
tags: [""]
langtxt: ["de"]
zone: "Deutschschweiz"
num: "63"
quote: "Es geht um ein würdiges Leben."
---

> Mein Name ist Abdallah und ich bin 26 Jahre alt. Ich bin seit dem
> 2. Oktober 2018 in der Schweiz. Seit Juni 2019 lebe ich in Bern. Zuvor
> war ich in verschiedenen Asylzentren in Vallorbe, Neuenburg, Freiburg
> und Schaffhausen. Ich war lizenzierter Journalist in Djibouti. In
> Europa zählt mein Diplom nichts. An der Universität von Grenoble
> begann ich Journalismus zu studieren. Später musste ich Frankreich
> verlassen. Mein Studium konnte ich nicht abschliessen. Momentan bin
> ich auf der Suche nach Arbeit. Ich würde gerne als Journalist
> arbeiten, spezialisiert auf Politik. Eine Lehre in einem sozialen
> Beruf würde mir sicher auch gefallen.

Wir haben den Verein *Medina* im September 2019 als Reaktion auf
ungedeckte soziale Grundbedürfnisse in der Stadt Bern gegründet. Anfangs
wurde mit einfachsten Mitteln Tee und Suppe gekocht, kurz darauf folgte
ein Container, das Herzstück unserer Tätigkeit. Seit Herbst 2019 bietet
dieser Container nun Anlaufmöglichkeit und Treffpunkt für alle Menschen
auf und rund um die Schützenmatte in Bern. Der Container als
Ausgangspunkt wird nicht nur zum Kochen genützt, sondern dient auch als
Basis, um viele weitere Projekte auf die Beine zu stellen. So
organisieren wir *drawing sessions*, Fussballturniere, Kerzenziehen,
Solikonzerte, Aktionswochen usw. Durch diese Projekte ist es uns
gelungen, ein Vertrauensverhältnis zu den Menschen vor Ort aufzubauen.

> Ich besuche die Schützenmatte erst seit kurzem. Ich komme gerne hier
> her. Mensch beobachtet da so einiges. Einerseits ist es ein Treffpunkt
> vieler Kulturen. Viele Menschen hier hatten das gleiche Schicksal. Sie
> mussten wegen widrigen Umständen aus ihrer Heimat flüchten. Mensch
> versteht sich, schliesst Freundschaften, spricht zusammen und kommt
> über die schwierigen Momente des Lebens hinweg. Zusammen geht es
> einfacher. Andererseits entstehen hier auch viele Probleme. Dieser
> Platz ist ein Elend. Uns mangelt es an adäquater Kleidung, Nahrung
> (!), Hygieneartikel und sanitären Anlagen. Ständig gibt es Streit. Die
> Leute sind frustriert, viele haben kaum Perspektiven und sind
> resigniert. Das macht mich traurig.

Weil niederschwellige städtische Angebote fehlen, wollen wir vom Verein
*Medina* als Mediatorin funktionieren, die marginalisierten Menschen den
Zugang zur Stadt Bern erleichtert. Dafür betreiben wir freiwillige
Sozialarbeit mit dem Ziel der Inklusion sozial benachteiligter
Gruppierungen, wie unbegleitete jugendliche Migrant\*innen, Obdachlose
oder Menschen in anderen schwierigen Lebenssituationen. Wir wollen
aufzeigen, dass ein solidarisches Miteinander möglich ist und vertreten
die Überzeugung, dass es Raum für Vielfalt geben soll. Unser
übergeordnetes Ziel lautet: «Für eine Welt, in die viele Welten passen».

> Ey! Das ist das reichste Land der Welt! Viele Menschen hier würden
> auch lieber arbeiten und Steuern zahlen als Dealen und im Knast zu
> landen. Es geht nicht einmal darum, reich zu werden. Es geht um ein
> würdiges Leben. Viele Flüchtlinge in der Schweiz leben ein miserables
> Leben und sind somit anfälliger, den Drogen zu verfallen. Da gibt es
> diese Wundermittel und deine Sorgen sind vergessen. So beginnt die
> Negativspirale. Es braucht Prävention. Der Verein *Medina* leistet
> hier grosse Arbeit. Sie unterstützen die Menschen und begegnen ihnen
> auf Augenhöhe. Ich denke das ist der richtige Ansatz, um die Situation
> nachhaltig zu verbessern. Ich wünsche mir, dass es mehr solche
> Angebote geben würde.

Gut zwei Jahre sind nun vergangen seit es unseren noch jungen Verein
gibt und wir haben im Jahr 2021 vieles dazugelernt. Alle, die *Medina*
unterstützen, haben weiter viel ehrenamtliche Zeit und Herzblut in das
Projekt investiert, dabei neue Freundschaften geschlossen und schöne
Momente erleben dürfen. Das Jahr 2021 hat aber auch einige
Herausforderungen mitgebracht, teils neue wie unser Umgang mit der
geplanten Nutzung des Raumes Schützenmatte durch die Stadt Bern, teils
altbekannte wie Obdachlosigkeit. Um diesen Aufgaben besser gewappnet zu
sein, haben wir uns neuorganisiert, was teilweise schwierige
Entscheidungen und schmerzhafte Prozesse mit sich brachte. Die
Neuorganisation gab uns aber auch neue Kraft und Motivation und bringt
einige spannende Projekte für das Jahr 2022 hervor, wie solidarische
Siebdrucke, Caterings aus aller Welt, mehr Vernetzungsarbeit,
niederschwellige Beratungsangebote direkt auf dem Platz,
Kunstaustellungen und einen kurzzeitigen Umzug des Containers auf den
Kornhausplatz, wo er im Rahmen eines Ausstellungsprojekts Ende Januar
2022 stehen wird. Unser Verein lebt vom endlosen Engagement unserer
ehrenamtlichen Mitglieder sowie der zahlreichen Ideen und
Unterstützungen aller Helfenden.

Die Tatsache, dass unser Verein mit dem Sozialpreis 2020 der Stadt Bern
ausgezeichnet worden ist, zeigt die Notwendigkeit unserer Arbeit. Die
Stadt Bern braucht ein solidarisches Miteinander. Wir werden uns
weiterhin für eine solidarische Stadt Bern einsetzen: «Für eine Welt, in
die viele Welten passen!»
