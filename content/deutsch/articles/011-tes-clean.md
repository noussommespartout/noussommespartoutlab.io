---
author: "Gleisson Juvino"
title: "Bist du clean?"
subtitle: "Audio-Journal einer Person, die mit HIV lebt, und ihrem Kampf gegen alle Formen von Diskriminierung"
info: "Audio-Journal --- April 2021"
datepub: "Im Herbst 2021 auf Deutsch übersetzt"
categories: ["Sexualität", "Trans-Schwul-Lesbisch, Queer", "Selbstreflexion zum Aktivismus"]
tags: ["Adrenalin", "Wohlwollen", "Dekonstruktion", "Kraft", "unmöglich", "Internet, Web", "Werkzeug", "Angst", "Macht", "Prozess", "Sex"]
langtxt: ["de", "fr"]
zone: "Suisse romande"
num: "11"
quote: "es ist ein Teil von mir, jedoch nicht einzig und allein das, was mich zu dem macht, was ich bin"
---

## 1. --- Audioaufnahme ---

Mein Name ist Gleisson. Es ist der 11. April 2021. Ich möchte zuerst
etwas über mich erzählen, da es mir für den Erfahrungsbericht, den ich
hier mache, wichtig erscheint. Ich betrachte mich als eine nicht-binäre
Person°. Und selbst wenn ich überzeugt bin, dass ich es schon immer war,
habe ich erst seit kurzem die Ressourcen gefunden, mich zu diesem Teil
meiner Identität zu bekennen.

Seit bald sechs Jahren lebe ich mit HIV. Weil ich zahlreiche Stigmas und
Autostigmatisierungen dekonstruieren konnte und ich mich für andere
Genderidentitäten interessierte, wurde mir klar, wer ich eigentlich bin
--- ohne mich dafür zu schämen.

Der Kampf gegen die Diskriminierung von HIV-positiven Menschen, ist für
mich ein Kampf, der sich gegen alle Formen von Diskriminierung richtet.
Wir können keine Epidemie bekämpfen, ohne dass wir zu verstehen
versuchen, was die Menschen dazu bringt, sich vom Gesundheitssystem
abzuwenden. Meine Erfahrungen mit HIV haben mich dafür sensibilisiert
und es mir möglich gemacht, mich für andere Identitäten einzusetzen und
Kraft für mein eigenes Leben zu finden. Dabei wurden viele Fragen
aufgeworfen und haben zu Einsichten über meine Privilegien geführt. Als
hier lebende Weisse Person habe ich beispielsweise Zugang zu
antiretroviraler Therapie. Die pädagogische Vermittlung, die Nähe zu
meiner Gemeinschaft, hier zu sein und für unsere Sichtbarkeit in der
Öffentlichkeit zu kämpfen: Das alles hat für mich mit dem HIV begonnen,
das alles hat mich dahin geführt, wo ich heute bin.

## 2. --- Audioaufnahme ---

Es ist Montag, der 15. April 2021. Ich möchte zuerst etwas zu meinem
inneren Befinden sagen. Ich bin froh und dankbar, dass ich meinen
Aktivitäten nachgehen und helfen kann, die negativen Vorstellungen über
das Leben mit HIV in unserer Gesellschaft abzubauen.

Im Speziellen kämpfe ich gegen festgefahrene Ideen, insbesondere bei den
jüngeren Generationen. Mit ihnen komme ich entweder im Rahmen von
Freiwilligenarbeit oder im Internet in Kontakt. Dabei höre ich Sachen
wie: "HIV und Aids, das ist das Gleiche, nicht?" Diese Verwechslung[^3]
ist stigmatisierend und diskriminierend. Sie verbreitet negative Bilder,
die zu Beginn der Epidemie in den 80er Jahren durch die Medien
geisterten. Um die Behandlung von HIV als Präventivmassnahme zu
verstehen, ist der Unterschied zwischen HIV und Aids grundlegend: Bei
einer HIV-positiven Person, die eine Therapie verfolgt, kann es sein,
dass die Virenlast im Blut nicht mehr nachweisbar ist. Das heisst, dass
die mit dem HI-Virus lebende Person den Virus nicht mehr übertragen
kann. Das Virus löst also nicht Aids aus. In diesem Stadium ist das
Immunsystem zu schwach, um die tödlichen Infektionen zu bekämpfen.
Diesen Unterschied zu verstehen, ist grundlegend, um gegen
Diskriminierungen und Vorurteile anzukämpfen, aber auch um im Rahmen von
Diskussionen auf die zeitgenössische Realität dieser Epidemie
hinzuweisen.

Mir werden auch Sätze zugetragen wie: "AIDS kann doch heute behandelt
werden." Genau da argumentiere ich mit meinen Erfahrungen, um
aufzuzeigen, dass dies falsch ist. Ja, ich lebe gut mit HIV, trotz aller
medizinischer und sozialer Schwierigkeiten. Ja, wir können HIV dank
antiretroviraler Therapie in den Griff kriegen, jedoch nur mit teuren
und schweren, lebenslangen Behandlungen. Was aber "behandelt" werden
kann, sind die Wissenslücken rund um das Thema HIV und Aids. Wir wissen
doch, dass Prävention durch Angstmacherei nicht mehr funktioniert und
wir es vermeiden sollten, die betroffenen Personen noch mehr zu
stigmatisieren. Deshalb spreche ich immer aus der Ich-Perspektive und
verweise auf meine eigenen Erfahrungen.

## 3. --- Audioaufnahme ---

Es ist Samstag, der 17. April 2021. Ich mache meine dritte
Audio-Aufnahme. Wie gewohnt möchte ich zuerst ein Wort zu meinem inneren
Befinden verlieren. Ich fühle mich glücklich und stolz. Glücklich, dass
ich Mitstreiter\*innen° in meinem sozialen Umfeld habe, auf die ich mich
verlassen kann, wenn ich *care*° brauche und etwas teilen möchte.
Glücklich, dass ich als Person, die mit HIV lebt, mich unterstützt fühle
. Ich empfinde viel Dankbarkeit gegenüber jenen Menschen, die seit
Beginn der Epidemie vor 40 Jahren gegen die Diskriminierung von HIV und
Aids ankämpfen. Dieses aktivistische Gemeinsamkeitsgefühl gibt mir Kraft
weiterzumachen und motiviert mich, meinen Teil beizutragen.

Heute kenne ich die Gründe, warum ich das Fremdouting[^4] ablehne und
warum ich Hilfsmittel entwickeln will, die wohlwollend und schützend
sind. Dies möchte ich anhand einer kürzlich erlebten Situation
illustrieren. Trotz der Tatsache, dass ich im Internet oder an
Veranstaltungen offen über mein Leben mit HIV spreche, gibt es in meinem
familiären Umfeld noch immer Menschen, beispielsweise meine Grosseltern,
die nichts davon wissen. Irgendwer hat es ihnen aber erzählt. Heute bin
ich glücklich, sagen zu können --- im Gegensatz zu ähnlichen Erfahrungen
in der Vergangenheit ---, dass es mich weniger betroffen macht. Für mich
war es eher die Gelegenheit, mit ihnen den Austausch zu suchen. Ich bin
stolz sagen zu können, dass ich mich nicht geoutet fühle. Ich verfüge
unterdessen über die nötigen Ressourcen, um in solchen Momenten Klarheit
zu schaffen. Ich erachte es als notwendig und dringlich, offen darüber
zu sprechen und die Realität mit jenen Worten zu benennen, die sie
verdient. Beispielsweise verwende ich den Begriff "Personen, die mit HIV
leben" anstelle von "Infizierten", "Kranken" oder "Leidenden".

## 4. --- Audioaufnahme ---

Es ist der 23. April. Seit ein paar Tagen fühle ich mich emotional nicht
sehr gut. Ich mache trotzdem eine Aufnahme, weil es Teil meines Lebens
mit HIV ist. In den Jahren nach der Diagnose habe ich sehr stark
gelitten. Die Einsamkeit verleitete mich manchmal dazu, absolute
Adrenalinkicks zu suchen, um mich lebendig zu fühlen. Manchmal aber war
es unmöglich, meinem Leben und meinem Umfeld einen Sinn zu geben. Es
kommt vor, wie heute, dass ich mich nicht gut fühle und ich das Gefühl
habe, wieder denselben Weg wie vor Jahren zu beschreiten. Mit dem
Unterschied, dass ich nicht mehr die gleiche Person bin wie während der
ersten Jahre nach der Diagnose. Das will ich nicht mehr erleben, ich
weiss, wohin mich das geführt hat, und deshalb verletzt es mich auch so.

## 5. --- Für den Sammelband aufgenommene Diskussion ---

Es ist Samstag, der 24. April 2021, 14:06 Uhr. Wie steht es um mein
inneres Befinden? Ich fühle mich stärker als letzte Woche, und ich bin
glücklich, das sagen zu können. Die gestrige Aufnahme war sehr
persönlich und emotional. Dadurch habe ich begriffen, dass mich mein
Werdegang noch immer berührt. Es war eine schwierige Woche. Ich war
während sieben Jahren mit einer Person zusammen; jetzt sind wir dabei,
uns rechtlich zu trennen. Es gibt Hochs und Tiefs, was meine Erfahrungen
mit dem HIV betreffen. Eine Sache, an der ich stark gelitten habe, war,
dass mein Ex-Partner mich geoutet hat. Ich war verletzt und machte
deshalb fürchterliche Momente durch. Das Ganze hat ungeahnte Dimensionen
angenommen: Mein HIV-Status wurde benutzt, um mich auf persönlicher und
beruflicher Ebene anzugreifen. Ich habe Anzeige erstattet. Weil das
Fremdouting aber im Freundeskreis stattfand, wurde das Verfahren
eingestellt. Es ist sehr schwierig zu erreichen, dass eine solche
Angelegenheit in einem Prozess behandelt wird.

Ich hatte gegen meinen Ex-Partner und seine Freund\*innen Anzeige
erstattet. Sie wurden vorgeladen und auf dem Polizeiposten befragt. Die
Auswertung der Aussagen blieb aber ohne juristische Folgen. Während den
folgenden drei Jahren habe ich die Beziehung noch aufrechterhalten. Das
war sehr kompliziert. Denn zu diesem Zeitpunkt lebte ich bereits seit
zwei Jahren mit HIV und versuchte, wie es Junge mit 23 Jahren halt so
machen, mein Leben in den Griff zu kriegen. Keine\*r meiner Familie
wusste es, ich hatte ihnen nichts davon erzählt. Das Fremdouting und die
Attacken, die folgten, haben mich zutiefst entmutigt. Ich hatte keine
Unterstützung, kein soziales Umfeld, einzig meine Schwester, die in Genf
wohnte, doch auch sie wusste von nichts. Ich hatte keine Freund\*innen,
ich war erst seit vier Jahren hier. Das alles hat in einem
Selbstmordversuch geendet. Ich musste hospitalisiert werden und musste
in Behandlung.

Heute, wenn ich an diese Zeit zurückdenke, sage ich mir, dass dies ein
sehr wichtiger Erfahrungsschatz ist. Das ist paradox: Ich habe das
Schlimmste durchgemacht und weiss nun, wie ich nicht wieder in so eine
Situation geraten kann. Sogar in Momenten wie gestern, wenn es mich in
einen Strudel von Gefühlen reisst, versuche ich, Abstand zu gewinnen und
mich an die Unterstützung zu klammern, die mir entgegengebracht wird. In
diesen Momenten hilft mir mein politischer Aktivismus, um Kraft zu
tanken.

Ich arbeite unter anderem mit jungen Erwachsenen und Student\*innen der
postobligatorischen Schule in Genf. Dabei versuche ich zu
dekonstruieren, Prävention zu betreiben, ohne dabei auf Angst- und
Schuldgefühle zurückzugreifen; ich versuche so gut es geht, Sexualität
zu entstigmatisieren. Die Art und Weise, wie wir Beziehungen leben, hat
sich während dem 40-jährigen Kampf gegen HIV stark verändert. Dennoch
hat die Denkweise der 80er Jahre heute noch immer einen Einfluss auf
unsere zeitgenössischen Vorstellungen: Es gibt immer noch Leute, die
glauben, mensch könne sich mit dem HIV infizieren, wenn er aus dem
gleichen Glas trinkt oder ein Person küsst.

Seit zweieinhalb Jahren engagiere ich mich freiwillig in der Genfer
Aids-Gruppe. Mit der Gruppe *Regard Croisés*, die sich aus HIV-positiven
Menschen zusammensetzt, behandeln wir das Thema Prävention im
ausserschulischen Kontext (mit Student\*innen der postobligatorischen
Schule). In einem informellen Rahmen führen wir mithilfe der
"Photolangage-Methode[^5]" Gespräches. Während diesen Begegnungen
treten oft andere Themen in den Vordergrund. Die Student\*innen ziehen
oft Parallelen zwischen der Diskriminierung gegenüber HIV-positiven
Menschen und anderen Formen von Diskriminierung, aufgrund des Genders
oder der Herkunft. Neben dieser Arbeit organisiere ich Events,
Präventionskampagnen und Diskussionen zum Leben mit HIV. Aktuell
engagiere ich mich auch in einem neuen Tätigkeitsbereich: eine
Transmenschen-Gruppe, die sich für *mixité choisie*°-Räume[^6] einsetzt,
Workshops und Treffen zwischen Transmenschen° und Mitstreiter\*innen
organisiert.

Als uns die Coronapandemie einholte, war ich in Brasilien und musste
notfallmässig in die Schweiz zurückkehren. Ich war bei mir zuhause
eingesperrt und war von all diesen Aktivitäten abgeschnitten. Das war
hart. So habe ich angefangen, online aktiv zu werden. Ich hatte bereits
grindr[^7] benutzt, um anonym Prävention zu betreiben. Auf der App kann
mensch seinen serologischen Status angeben. Umgangssprachlich wird oft
gefragt: "Bist du *clean*?". In den zahlreichen Diskussionen, die ich
dort geführt habe, wurde ich oft direkt attackiert, weil ich meinen
HIV-Status offen kommunizierte, oder auch, weil ich dies anonym tat. Es
gibt so etwas wie eine sexuelle Aura um Menschen, die mit HIV leben,
oder besser gesagt, über ihr Sexualverhalten. Ich erhielt offenkundige
Angebote für ungeschützten Sex, als würde die Tatsache, dass ich
HIV-positiv bin, heissen, dass ich mir alles erlauben würde. Ich wurde
als risikofreudige Person wahrgenommen.

Seit dieser Erfahrung versuche ich vermehrt zu schreiben. Ich habe den
instagram-Account \@goodhivvibesonly ins Leben gerufen, um meine
Erfahrungen zu teilen. Es hat Zeit gebraucht, bis ich bereit war, mich
öffentlich zu äussern und Fragen von meiner Familie oder Freund\*innen
zu beantworten, insbesondere, weil ich in einigen Fällen keine Antwort
parat hatte. Das brauchte Zeit. Auf grindr habe ich die Anonymität
gewählt, um mich auf dieser Datingseite nicht outen zu müssen. Auf
solchen Plattformen werden Menschen, die mit HIV leben, nicht immer sehr
nett behandelt. Auf instagram habe ich anonym begonnen, um zu lernen und
sehen wie ich reagiere, um mich selbst zu schützen, aber auch, um mich
mit diesem *tool* vertraut zu machen. Heute betreibe ich auf grindr
keine Prävention mehr, an anderen Orten aber schon. Auf instagram bin
ich nicht mehr anonym. Ich bin stolz, mich --- ohne ein Blatt vor den
Mund nehmen zu müssen --- ausdrücken zu können. Es ist wichtig, dass dies
zur Normalität wird: Ich verstehe nicht, warum unsere Gesellschaft mit
dem Outing noch immer so Mühe hat. Es ist ein Teil von mir, jedoch nicht
einzig und allein das, was mich zu dem macht, was ich bin. Ich sehe es
nicht mehr wie eine Bestrafung. Ich bin mir bewusst, dass wir davon
nicht sicher sind, dass wir uns immer wieder Risiken aussetzen. Die
moralische Last von Sexualität in Verbindung mit dem HIV ist sehr
wichtig, ihr liegt ein Schuldgefühl zugrunde, dass infrage gestellt
werden muss.

Als ich meine HIV-Ansteckung entdeckte, war ich eben erst in der Schweiz
angekommen. Ich war also eine Person mit Migrationshintergrund. Ich
werde mir immer mehr bewusst, dass die Anhäufung von Diskriminierungen
sehr gefährlich ist: Es ist bekannt, dass die Suizidrate bei
Transmenschen, nicht-binären Menschen, bei Menschen mit
Migrationshintergrund oder bei Menschen, die mit HIV leben, viel höher
ist. Der medizinische Bereich ist nur wenig oder gar nicht auf die
Betreuung solcher Menschen vorbereitet, wobei doch gerade diese
Bevölkerungsgruppen entscheidend für die Prävention sind. Ich habe
versucht, dem nicht zu viel Aufmerksamkeit zu schenken. Ich liess Dinge
durchgehen, weil ich mit der Müdigkeit kämpfte und während der
Behandlung viel einstecken musste. Auch nach 40 Jahren Kampf gegen das
HIV sind wir noch immer daran, die Betreuung zu verbessern, obwohl wir
doch wissen, welche Bevölkerungsgruppen am meisten leiden. Es ist
schockierend, dass wir erst so weit sind und in diesem Kampf die
finanziellen Mittel noch immer knapp sind.

Im Grunde genommen hätten wir ja eigentlich die Mittel, um das HIV und
Aids zu bändigen: das PrEP[^8], das Kondom, die Behandlung als
Prävention und die (Früh-)Erkennung. Doch der Informations- und
Ressourcenmangel ist ganz klar politisch bedingt. Wenn es um Prävention
geht, sind die betroffenen Bevölkerungsgruppen auch jene, die am
stärksten marginalisiert und diskriminiert werden: Sexarbeiter\*innen°,
Transmenschen, die LGBTQIA+-Community°, Migrant\*innen usw. Abgesehen
von den gesundheitlichen Herausforderungen ist die eigentliche
Sackgasse, um diese Epidemie zu beenden, eine gesellschaftspolitische
Frage.

[^3]: HIV (Humane Immundefizienz-Virus, auch: Menschliches Immunschwäche-Virus) ist das Virus, das die Krankheit Aids (Akquiriertes Immun-Defizienz-Syndrom) auslösen kann.

[^4]: Das Fremdouting bezeichnet die Tatsache, gegenüber Drittpersonen die Homo- oder Bisexualität, Transidentität oder im vorliegenden Fall den serologischen Status einer Person ohne dem Einverständnis oder gegen den Willen der betroffenen Person zu enthüllen.

[^5]: 1968 erfundene pädagogische Methode, die es einer Gruppe ermöglichen soll, ihre Vorstellungen zu einem Thema mithilfe eines *tools* auszudrücken, das den mündlichen Ausdruck fördert. Das Ziel dieser Methode ist es, jedem Gruppenmitglied das Sprechen zu erleichtern, ausgehend von seinem Wissen, seinen Einstellungen, seinen Werten, seiner Praxis und seiner Erfahrung.

[^6]: Um einen poetischen Eindruck der Kraft von *mixité choisie* zu erhalten, lies den Text *Wenn sich Raum a u s d e h n t* \[n. 27\].

[^7]: Grindr wurde 2009 gegründet und ist eine Dating-App, die für homosexuelle, bisexuelle oder für diese Art von Sexualität neugierige Männer entwickelt wurde.

[^8]: Die PrEP (PräExpositionsProphylaxe; auch: Vorsorge vor einem möglichen HIV-Kontakt) betrifft HIV-negative Personen, die sich einem hohen Infektionsrisiko aussetzen. Sie verhindert eine HIV-Übertragung und wird seit 2014 von der WHO als wirksames Präventionsmittel anerkannt. Dank der Verfügbarkeit der Medikamente sind beispielsweise seit 2014 die HIV-Neudiagnosen in San Francisco um 28 % oder in London um 25 % zurückgegangen. In der Schweiz wird das Medikament Truvada seit 2006 als Teil einer Dreifachtherapie verschrieben, um den Virus in den Griff zu kriegen. Da das Medikament nicht für eine Präventionstherapie registriert ist, wird es von der Krankenkassen-Grundversicherung nicht zurückerstattet. Es kostet Fr. 900.-- pro Monat (65 % mehr als in Frankreich). Da in der Schweiz keine Generika zugelassen sind, beschaffen sich viele Menschen das Medikament im Ausland. Am 1. April 2019 senkte Swissmedic die Maximalmenge von Medikamenten, die für den persönlichen Gebrauch eingeführt werden dürfen, wie z. B. Medikamente, die bei einer PrEP verwendet werden, von drei Monaten auf einen Monat. Im Januar 2020 wurde dem Gesundheitsminister Alain Berset eine Petition übergeben, welche die Aufhebung dieser Beschränkung fordert.
