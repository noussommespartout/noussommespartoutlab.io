---
author: "S. & P."
title: "Versteckt eure Brüste oder pusht sie auf, und lasst uns endlich in Ruhe"
subtitle: "Gedanken zum Begriff der Brust"
info: "Im August 2020 für den französischen Sammelband verfasst"
datepub: "Im Herbst 2021 auf Deutsch übersetzt"
categories: ["Feminismus, Genderfragen", "Sexualität"]
tags: ["Schweiss", "Trauben", "Haar", "Körper"]
langtxt: ["de", "fr"]
zone: "Suisse romande"
num: "24"
quote: "doch unsere Brüste sind aus politischer Sicht eben nicht gleich"
---

Brüste sind ein Körperteil, das sich auf der Höhe des Brustkorbes,
zwischen Hals und Bauch befindet. Es ist ganz einfach der Busen. Doch
entgegen was oft verzapft wird und in den Wörterbüchern steht, haben wir
alle Brustwarzen, Titten, Nippel, *boobies*, Euter, Busen, Melonen,
Möpse, Brustmuskeln, Brüste, Lutscher, Zitzen, Lollies, Törtchen, "Houz
vor dr Hütte", "Möst" und andere physische Dinge, die sich unter den
zwei Schlüsselbeinen befinden.

Ob du eine Cis-Frau, eine Transfrau, ein Transmann, eine nicht-binärer
Person° oder ... ein überaus lästiger Cis-Mann° bist: Wir alle haben
Brüste. Doch unsere Brüste sind aus politischer Sicht eben nicht gleich.

Unsere Brüste schaut mensch mit ganz anderen Augen an.

Sie werden ganz anders erzogen.

Und über sie wird ganz anders gesprochen.

Liebe Cis-Männer, wir haben eure Brüste verdammt nochmal zur Genüge
gesehen. Enthüllt sie doch bitte sehr mit Vorsicht, spielt doch bitte
sehr das Versteckspiel mit uns, bewegt eure Dekolletés hin und her ("du
siehst mich, du siehst mich nicht"), zensiert sie --- so machen wir das
doch, oder nicht? *Pusht* euren Oberkörper (auf), nicht im Fitness,
sondern vor dem Spiegel. Bewundert eure femininen Kurven, bevor ihr euch
bei einem *Wet-T-Shirt-Contest* mit anderen vergleicht. Wir freuen uns
schon jetzt. Seid doch einfach fabelhaft!

Wir träumen von grossen Oben-ohne-Happenings in den Strassen,
splitternackt; wer sich dabei bedecken will, der kann. Diese Meute wäre
auf Social Media nur schwer abzubilden --- ganz sicher! Und sowieso: Wenn
wir im Wald oder in der Stadt spazieren gehen, schmeissen wir doch weg,
was unsere Vorfahren Korsett nannten, genauso unsere bauchfreien
Oberteile und Sweatshirts. Gehen wir splitternackt zur Arbeit. Trinken
wir unseren Apéro nackt. Erst dann nämlich könnte Herr Zuckerberg keine
Zensur mehr betreiben, und würde --- endlich, hoffentlich --- Konkurs
machen. Bilden wir einen Block, zerschlagen wir die Kameras von
mühsamen, aufdringlichen Journalisten und skandieren wir gemeinsam
unsere Hymnen, Slogans und was uns gerade einfällt und antörnt.

Es wird ein grosses Rudel von Titten sein, die wie Freudentränen
fliessen, so verschwommen wie der Nebel, so belaubt wie Bäume im Sommer,
bedeckt im Winter wie die Landschaft unter dem Schnee, aufgeblasen wie
ein Demoblock, frisch vernarbt wie einige unserer Herzen, klein wie
Trauben, uneben wie Wälder, rot wie heisse Zungen, so mächtig wie unsere
Solidarität, gerunzelt wie zerknittertes Papier, flach wie die Steppen
im Norden, aufgedunsen wie saftige Orangen, von Akne befallen wie die
Gestirne, in einer Schönheits-OP gemacht, neu gemacht, zusammengeklebt
und wieder aufgelöst wie unsere Gedanken, hart wie Stein, alt wie die
Weisheit unserer Schwestern, milchig wie die Milchstrasse, von der
Schwangerschaft so wellenförmig gedehnt wie die Ozeane, asymmetrisch wie
das Leben, weich wie Meerschaum, schwitzend wie der Morgentau, trocken
wie kalifornische Zwetschgen, schwarz wie Pupillen, bedeckt wie die
Stadt im Herbst, spitzig wie Nadeln von Koniferen, explosiv wie Vulkane,
kantig wie (männliche) Brustmuskeln --- und überbordend wie unsere Wut.
