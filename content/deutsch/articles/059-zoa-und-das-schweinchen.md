---
author: "Mirjam Berger"
title: "Zoa und das Schweinchen"
subtitle: "Antispeziesismus im Alltag mit einem Kind"
info: "Für den deutschen Sammelband verfasst"
datepub: "Im Herbst 2021 verfasst"
categories: ["Antispeziesismus", "Ökofeminismus", "Ökologie"]
tags: [""]
langtxt: ["de"]
zone: "Deutschschweiz"
num: "59"
quote: "Kein Mensch wird als Rassist*in, als Sexist*in oder als Speziesist*in geboren."
---

Zoa --- mein 2,5-jähriges Kind --- macht Mittagsschlaf. Ich habe nun circa
eine Stunde Zeit. Ich sollte auch eine Pause machen, geht manchmal aber
einfach nicht. Dann halt müde durch den Nachmittag. Aber was essen, das
geht parallel zum Schreiben.

Ja, ich habe mich für ein Kind entschieden. Der gesellschaftliche Druck
auf mich war gefühlt nicht gross, aber unterbewusst bestimmt präsent.
Ich war teilweise eher gehemmt, es Teilen von meinem Umfeld zu sagen. In
der Linken kommen Kinder nicht nur gut an, auch aus verständlichen
Gründen. Da ist der miserable Zustand unserer Welt, den wir dem
Kapitalismus zu verdanken haben und der Widerstand gegen das stereotype
Frauenbild, in dem wir nur ganz «Frau» sind, wenn wir irgendwann --- und
bitte nicht zu spät --- Mutter werden. Trotzdem habe ich mich dafür
entschieden.

Kinder bedeuten für mich Hoffnung und somit gebe ich die Hoffnung für
diese Welt nicht auf. Ganz im Sinne von Hannah Arendt, die sagte, jede
Geburt ist eine kleine Revolution, weil «die potenzielle Rettung der
Welt allein darin begründet liegt, dass sich die menschliche Gattung
immer wieder und für immer erneuert». Bis jetzt habe ich die
Entscheidung nicht bereut, obwohl mir die Zukunft unserer Welt grosse
Sorgen bereitet. Und klar, es ist viel, es ist stressig. Wir leben als
Kleinfamilie zusammen und der sanfte Versuch, unsere Familie grösser zu
denken und zu machen, scheitert an den verschiedenen Lebensentwürfen,
die vollgepackt sind mit Arbeit und anderen Prioritäten --- da bleibt
wenig Zeit für Kinderbetreuung. Ich sehe, dass das Modell Kleinfamilie
nicht optimal funktioniert. Es braucht mehr Menschen, um ein Kind zu
begleiten, um nicht ständig gestresst und übermüdet zu sein und doch
stecken wir darin fest. Im Moment jedenfalls noch.

Noch 40 Minuten Zeit. Maximal.

Heute Morgen haben wir drei Kinderbücher mitgenommen, die wer zum
Mitnehmen auf eine Bank gelegt hat. Und schwups haben wir eines
erwischt, das rassistisch ist. Gerade bei älteren Kinderbüchern geht es
kaum ohne Sexismus, Rassismus oder Speziesismus. Auch meine
Kinderheldinnen wurden davon nicht verschont --- ich meine, ich habe Pipi
Langstrumpf geliebt, aber der Kontext ist leider stark geprägt vom
Kolonialismus. Es ist durchaus anstrengend, alle Medien, die Zoa
konsumiert, zu hinterfragen und auszusieben, aber ich glaube das ist
immanent wichtig. Wir alle wurden dadurch geprägt und das ist Teil von
unserer diskriminierenden Gesellschaft. Immerhin geht was, sagt meine
Buchhändlerin des Vertrauens. Es erscheinen viele Kinderbücher mit
vielfältigen Lebensentwürfen. Bücher, in denen die Familien nicht aus
Mama, Papa und zwei Kindern bestehen. Bücher mit BIPoC° (*Black,
Indigenous and People of Color*) und Bücher über Kinder, die sich nicht
im binären Geschlechtermodell einordnen lassen. Sie führen (noch) ein
Nischendasein, was sich in der Haltung der Gesellschaft widerspiegelt.
Wenn Zoa rosa oder ein Röckchen trägt, gehen die meisten Menschen davon
aus, dass Zoa ein Mädchen ist. So sind wir gepolt. Immer noch. Und die
Wirtschaft treibt das voran. Es ist lukrativer, einer Familie mit zwei
Kindern mit unterschiedlichem biologischem Geschlecht zweimal den ganzen
Satz Kleider und Spielsachen zu verkaufen. Und doch gibt es Hoffnung und
gerade die Themen Rassismus und Sexismus werden mehr und mehr diskutiert
und reflektiert. Schwieriger wird es, wenn es um den Speziesismus geht.
Wir möchten Zoa beibringen, dass die Leben anderer Tiere[^1] genauso
wertvoll sind wie die unsrigen und jedes Lebewesen ein Recht auf Leben
hat bzw. haben sollte. Und genauso wie wir dagegen sind, dass Menschen
aufgrund von äusserlichen Merkmalen diskriminiert werden, sind wir
dagegen, dass andere Tiere diskriminiert werden, alleine weil sie einer
anderen Spezies angehören. Dementsprechend ernähren wir uns vegan, Zoa
ausserhalb von zuhause vegetarisch. Das geht vielen zu weit. Im
Mainstream heisst es immer noch, dass vegane Ernährung eine
Mangelernährung und somit besonders für Kinder schädlich ist. Die
Herausforderung stellt sich bereits bei der Suche nach einer\*m
Kinderärzt\*in, der\*die dieser Ernährungsform nicht komplett abgeneigt
ist. Bei den Grosseltern und ihrem Umfeld geht es weiter. Ich musste mir
schon Fragen anhören wie: «Aber gell, du zwingst deinem Kind deinen
Ernährungsstil nicht auf?». Damals wusste ich noch nicht richtig, wie
ich darauf antworten soll; heute habe ich zum Glück gute und vor allem
fundierte Argumente und ein gesundes und wohlgenährtes Kind, das in
keinster Weise so aussieht, wie sich die Mehrheit der Menschen ein vegan
ernährtes Kind vorstellen.

Noch 30 Minuten, soll ich doch noch eine Pause machen? Ja, vielleicht
später.

Klar, vegane Ernährung geht nicht ohne Supplemente, aber das ist bei
omnivoren Kindern nicht anders. Viele, wenn nicht sogar die meisten
Kinder in der Schweiz, kriegen mindestens bis zum zweiten Lebensjahr
Vitamin D --- von der Krankenkasse bezahlt. Viele speziell für Kinder
entwickelte Produkte enthalten zugesetzte Vitamine und Mineralstoffe,
häufig sind diese Produkte erst gar nicht vegan (vgl. jemalt).

So oder so ist es mir lieber, Zoa kriegt kontrolliert B12 und Omega 3,
als dass Zoa all das mitisst, was sich in tierlichen[^2] Produkten
befindet. Angefangen bei Antibiotika, Schwermetallen, Dioxinen.
*Fun-Fact*: das BAG hat vor kurzem eine Studie herausgegeben, die sagt,
dass in der Schweiz gestillte Babys den zugelassen Wert von Dioxinen
(sogenannte Umweltgifte) um das 150-fache überschreiten. Nicht erwähnt
wurde, dass wir diese Umweltgifte vor allem durch tierliche Produkte zu
uns nehmen, da sich diese Umweltgifte besonders in tierlichen Fetten
ablagern, beispielsweise in der Kuhmilch. Dadurch gelangen sie zu uns
und landen wiederum in der menschlichen Muttermilch. Was viele weiter
nicht wissen, ist, dass anderen Tieren ebenfalls Supplemente verabreicht
werden. Unsere Böden sind beispielsweise B12-, selen- und jodarm.
Ausserdem erhalten die meisten sogenannten Nutztiere Kraftfutter aus
Monokulturen, welche die Böden ausgelaugt haben. Darum werden die
genannten Vitamine und Mineralstoffe dem Futter beigemischt. Wir
konsumieren also so oder so ein Supplement, entweder direkt oder durch
ein anderes Tier.

Noch 20 Minuten und ich bin leergeschrieben. Ich mache ein anderes Mal
weiter.

Heute ist Zoa bei den Grosseltern. Ohne sie ginge es nicht. Wir leben in
einem System, indem wir auf die *Care-Arbeit°* der Grosseltern
angewiesen sind, wenn beide arbeiten oder eine erwachsene Person alleine
die Kinder grosszieht (was eigentlich ein Ding der Unmöglichkeit ist).
Selbst mit ihnen kann es psychisch anspruchsvoll werden. Auf eine Art
leben wir ständig am Limit, fällt wer von uns aus, fällt das schöne
Kartenhaus in sich zusammen.

Aber zurück zum Thema. Mir geht es nicht darum, einzelne Personen an den
Pranger zu stellen. Ich bin mir sicher, dass die meisten Eltern jeweils
das beste geben und dass ich das Privileg habe, mir über solche Dinge
Gedanken machen zu können. Das System ist das Problem, und meine Art zu
leben ist meine Kritik daran. Dabei ist es mir wichtig, dass nicht der
Veganismus das Privileg ist, sondern die Tatsache, dass wir tierliche
Produkte masslos und zu unverschämt tiefen Preisen konsumieren können.
Der Veganismus ist die Antwort auf diese Dekadenz. Es ist aber
schwierig, daraus auszubrechen. Ich selber habe die meiste Zeit meines
Lebens tierliche Produkte konsumiert, weil es als normal gilt, als
wichtig, als gesund. Die Fleisch- und Kuhmilchlobby hat hier ganze
Arbeit geleistet. Und Gewohnheit ist eine starke Kraft.

Die Zeit verfliegt, ich muss los, erst noch ein bisschen das Chaos
aufräumen, dann Pyjama und Zahnbürste packen und ab zu den Grosseltern.

Geschafft, eine Pizza, ein Bilderbuch und eine grosse Krise später ist
Zoa auf dem Weg von den Grosseltern nach Hause eingeschlafen. Ich habe
nun bisschen Zeit und erstaunlicherweise Energie, um zu schreiben.

Mit diesem Thema fühle ich mich häufig sehr alleine. Ich kenne nur eine
andere, sich vegan ernährende Familie. Viele können es sich nicht
vorstellen, besonders wenn Kinder ins Spiel kommen, dabei gibt es in
meinen Augen viel Positives, darüber wird viel zu wenig gesprochen.
Neben dem, dass es --- wenn richtig gemacht --- gesünder ist, fällt auf
eine Art eine seltsame Doppelmoral weg. Zoa liebt andere Tiere und macht
(noch) keinen Unterschied zwischen Katzen, Schweinchen oder Hühnern: Zoa
mag sie alle. Und ich kann mir nicht vorstellen, dass Zoa es gut heissen
würde, gäbe ich Zoa ein totes Stück eines dieser Tiere zu essen. Kinder
sind empathisch und haben ein Gefühl für Ungerechtigkeiten. Ich habe das
Gefühl, dass wenn wir offen und ehrlich den Kindern sagen würden, was
sie da essen und wie das für die anderen Tiere ist, sie sich häufig
gegen diesen Konsum entscheiden würden.

Ich bin sehr gespannt auf diesen Prozess mit Zoa, aber so weit sind wir
noch nicht. Zoa weiss seit kurzem anhand von einem toten Falter, was tot
bedeuten könnte und dass wir keine Muttermilch von anderen Tieren
trinken, weil wir deren Babys die Milch nicht wegnehmen möchten. Aber
Zoa ist noch zu klein, um die weiteren Zusammenhänge herzustellen. Wir
haben aber ein Kinderbuch, indem es um eine Schweinchen-Familie geht.
Der Bauer will das Papa Schwein mit ans Schlachtfest nehmen und das
kleine Schweinchen will nicht, dass sein Papa nie mehr zurückkommt. Also
erfindet er alles Mögliche von pflanzlichen Würsten über Agar-Agar bis
hin zu pflanzlichem Leder. Wir schauen uns das ab und zu an, weil Zoa
mag, wie das Schweinchen kocht. Ich versuche es so altersgerecht
abzuändern, dass Zoa eine Geschichte verstehen kann. Das letzte Mal habe
ich Zoa gefragt, was denn der Bauer mit dem Papa Schwein anstellen will
und die Antwort war: «spielen». Diese Antwort hat mich sehr berührt und
mir wieder einmal gezeigt, wie positiv und unvoreingenommen kleine
Kinder sind. Seit Zoas Geburt ist mir wahnsinnig eingefahren, wie frei
Babys zur Welt kommen. Kein Mensch wird als Rassist\*in, als Sexist\*in
oder als Speziesist\*in geboren. Wir, unsere Gesellschaft und die
Medien, schreiben diese Dinge in die Kinder ein. Das ist eine enorme
Verantwortung, derer ich mir früher nicht bewusst war. Heute versuche
ich diese voll wahrzunehmen, mit allen Schwierigkeiten, die da kommen:
Beispielsweise zu thematisieren, aus welchen Gründen wir keine
tierlichen Produkte essen und gleichzeitig mitgeben, andere Menschen
deswegen nicht abzulehnen.

Veganismus und Anti-Speziesismus sind Systemkritik und nicht einfach
eine Lifestyle-Entscheidung. Und das geht auch mit wenig Geld, denn
davon hatten wir nie viel. Ich glaube, dass wir uns damit beschäftigen
müssen, was wir essen und wie wir es essen. Der Kapitalismus hat auch
hier seine Grenzen der Ausbeutung erreicht, und wir brauchen neue Wege,
wie wir Sorge zum Planeten, anderen Tieren und zu uns selbst tragen
können. Veganismus ist nicht die Lösung dafür, aber ein Schritt in eine
andere Richtung.

Ist das ein gutes Schlusswort? Keine Ahnung, aber jetzt muss ich
wirklich mal Pause machen.

P.S:

Zoa und ich waren bei Freund\*innen zu Besuch. Zoa wollte immer ein
Bilderbuch anschauen, in dem verschiedene «Bauernhöfe» dargestellt
werden. Auf diesen Höfen leben die Tiere glücklich und frei miteinander
zusammen. Dazwischen war eine Abbildung welche «tollen» Produkte wir von
den anderen Tieren erhalten. Hier ist ein Problem verwurzelt. Wir lehren
den Kindern, dass Bauernhöfe idyllische Orte sind, an denen Menschen und
andere Tiere friedlich zusammenleben. Wir lehren ihnen, dass die anderen
Tiere uns «ihre» Produkte freiwillig geben. Wir lehren es ihnen, weil es
uns gelehrt wurde. Die Realität sieht anders aus. Die Realität ist
mehrheitlich gewaltvoll und unempathisch. Ich habe nun angefangen, beim
Erzählen von Lebenshöfen zu sprechen, denn das kommt der
Bilderbuchdarstellung näher. Und innerlich schreie ich vor Wut über das,
was wir den anderen Tieren tagtäglich antun.


[^1]: Es ist bewusst die Rede von «andere Tiere», weil das impliziert,
    dass auch wir Tiere sind und weniger einen Unterschied manifestiert,
    den wir durch unsere speziesistische Sprache ansonsten generieren.
    Dieser Begriff wird hier gegenüber «nicht-menschliche Tiere»
    bevorzugt, da dieser vom Mensch als der Norm ausgeht und somit eine
    Hierarchisierung impliziert. Es wird schliesslich bei weiblich
    gelesenen Menschen auch nicht von «nicht-männlicher Mensch»
    gesprochen.

[^2]: Es wird bewusst «tierlich» anstatt «tierisch» verwendet, weil
    «tierisch» grundsätzlich negativ konnotiert ist, analog zu weibisch
    oder kindisch.
