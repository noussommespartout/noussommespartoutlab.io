---
author: "Anonym"
title: "Ich bin eine Nutte"
subtitle: "Ein praktischer Leitfaden für revolutionäre Sexarbeiter*innen"
info: "Im November 2020 für den französischen Sammelband verfasst"
datepub: "Im Herbst 2021 auf Deutsch übersetzt"
categories: ["Feminismus, Genderfragen", "Sexarbeit", "DIY", "Selbstverteidigung"]
tags: ["Liebe, Verliebtheit", "Körper", "Kraft", "Internet, Web", "Macht", "Hure", "Strasse", "Sex", "Theater", "Telefon", "Gesicht", "Auto", "Stimme"]
langtxt: ["de", "fr"]
zone: "Suisse romande"
num: "2"
quote: "ein Wort, das vor Kraft und Vergnügen nur so strotzt"
---

Ich bin eine Nutte.

Weckt das Neugier bei dir? Bringt dich das aus der Ruhe? Widert dich das
an? Hast du Lust auf mich? Vielleicht schockt dich das aber auch ganz
einfach. Doch das ist mir egal. Du fragst dich vielleicht, warum ich das
mache? Es gibt doch viele andere Möglichkeiten, um sein Leben zu
finanzieren -- oder nicht? Ist es, weil es leicht verdientes Geld ist,
weil ich süchtig nach Sex bin oder weil ich keine andere Wahl habe?
Rechenschaft ablegen muss ich sicher nicht. Trotzdem gebe ich dir gerne
einen Einblick in mein Privatleben -- aber nur während der Lektüre
dieses Textes.

Alles fing eigentlich damit an, dass ich Lust hatte, Telefonsex
auszuprobieren. Es hiess, dass sich meine Stimme bestens dafür eignen
würde. Doch ich wusste nicht so recht, wie und wo ich anfangen sollte.
Dann wollte ich mich als Stripperin versuchen. Ich liebe es, zu tanzen.
Das wäre sicher *fun* gewesen. Doch auch da: Ich wusste nicht so recht,
wie und wo ich anfangen sollte. So sagte ich mir, dass es vielleicht
einfacher ist, mich über den Beruf einer Prostituierten schlau zu
machen. Schliesslich erklärte mir eine Freundin einer Freundin, die
selbst als Nutte arbeitet, wie und wo ich anfangen könnte. Ich habe es
dann mit mehreren Freund\*innen besprochen, die ebenfalls Lust hatten,
damit anzufangen. Eines ist sicher: Es gibt mehr Sexarbeiter\*innen°
bzw. zukünftige Sexarbeiter\*innen als du denkst!

Nun, wie wird mensch also Sexarbeiter\*in \[nachfolgend als SA
bezeichnet\]? Was vielleicht nicht auf den ersten Blick sichtbar ist:
Sexarbeit° ist sehr vielfältig. Es gibt zahlreiche Möglichkeiten, sie zu
praktizieren: auf der Strasse, im Internet, über Zeitungsanzeigen, mit
mehreren oder einem einzigen regelmässigen Kunden, bei einmaligen
Treffen, im Auto, in deinem Zimmer, oder beim Kunden zuhause. Ich selbst
möchte euch eine von vielen Methoden vorstellen. So wie ich es eben
mache. Diese Art von Sexarbeit erlaubt es mir, anonym zu bleiben, auch
in meinem Umfeld.

1.  Denke dir einen falschen Namen aus. Wer wollte sich nicht schon
    immer einen anderen Namen zulegen? *Voilà*, die Gelegenheit: Sascha,
    Monika, Giulia, Eva\...

2.  Lege dir eine falsche Identität mit einer erfundenen Geschichte zu.
    Achte darauf, dass du glaubwürdig bleibst. Erfinde deine eigene
    Geschichte und bereite dich auf mögliche Fragen vor. Erzähle lieber
    nicht, dass du Physik studierst, wenn du die Newtonschen Gesetze
    nicht kennst. Kunden mögen es, wenn du ihnen von deinem Studium,
    deinem Job und deinem Leben erzählst. Mir ist es schon passiert,
    dass ich widersprüchliche Dinge betreffend meines Studienjahres und
    meines Alters erzählte. Ich musste dann kurzerhand eine Geschichte
    erfinden. Ich erzählte, ich hätte Klassen übersprungen, nur damit
    alles zeitlich wieder stimmte.

3.  Mache Fotos von deinem Körper. Ein Fotoshooting mit Freund\*innen
    ist doch ganz aufregend. Ich persönlich empfehle, keine Fotos von
    deinem Gesicht zu verbreiten. Damit schützt du dich vor unangenehmem
    *stalking* oder Erpressung.

4.  Überlege dir, wie viel du verdienen möchtest. Ich würde mindestens
    200 Franken pro Stunde oder 400 Franken pro Abend verlangen. Einmal
    verdiente ich an einen Abend sogar 700 Franken. Um anonym zu
    bleiben, solltest du nur Bargeld akzeptieren.

5.  Eröffne beispielsweise auf anibis.ch ein Konto. In der Erotik-Rubrik
    findest du Anzeigen oder kannst selber eine schalten. Beschreibe
    dich kurz und knapp. Sag, was du magst. Und erkläre gut, wo deine
    Grenzen liegen. Kunden ziehen oft gelegentliche SA den
    professionellen vor. Ein Kunde erzählte mir einmal, dass
    professionelle SA ihn einzig und allein als Geldquelle sehen würden.
    Er hatte den Eindruck, dass er einer von vielen Kunden und die Zeit
    knapp kalkuliert war -- der Job wurde wie jede andere Arbeit
    verrichtet. Verachtung ist bei Sexarbeit sehr weit verbreitet.
    Einige sind der Meinung, dass seit langem aktive SA "schmutzig" und
    ihre Scheiden "ausgeweitet" seien.

Bis dahin scheint das Ganze also eher einfach und unkompliziert. Ich
persönlich wurde mir erst später bewusst, wie viel Arbeitsaufwand dieser
Job -- vom Schalten der Anzeige bis zur Bezahlung -- mit sich bringt.

Auf 40 verschickte Nachrichten antworten dir vielleicht 25. Davon sind
nur knapp 15 wirklich interessiert. Und von diesen 15 wollen vielleicht
10 ein Rendez-vous, 7 springen dann in der letzten Minute ab und 2
kommen gar nicht zum Treffen. Bei langem Hin und Her, ohne dass sich ein
konkretes Rendez-vous abzeichnet und du keinen Bock mehr hast, lasse es
lieber sein. Es lohnt sich nicht, sich an die ersten zustande gekommenen
Kontakte zu klammern. Glaub mir: Es kommen noch viele andere.
Regelmässige Kunden sind sehr praktisch. Das schafft Vertrauen und
Sicherheit. Ausserdem erübrigt sich ein mühsamer Mailverkehr. Wenn ich
die Zeit fürs Schreiben, Planen und Reisen mit dem effektiv verdienten
Geld vergleiche, dann ist es -- wenn ich ehrlich bin -- ein nicht sehr
gut bezahlter Job. Warum also weitermachen? Ich finde es therapeutisch.
Es gibt mir das Gefühl, Macht gegenüber Cis-Männern° auszuüben. In
meiner Vergangenheit wurde ich sexuell missbraucht. Erstaunlicherweise
fühlte ich mich dann mit meinem ersten Kunden in Sicherheit. Ich fühlte
mich sicherer als mit jenen Personen, mit denen ich in der Vergangenheit
zusammen war. Und das stimmte mich nachdenklich. Alle sentimentalen
Aspekte beim Sex auszublenden, war für mich ein grosser Schritt, um
besser mit meiner Sexualität umgehen zu können.

Als ich meinen ersten Kunden traf, lag ich auf meinem Rücken, sein Kopf
zwischen meinen Beinen, seine Zunge auf meiner Klitoris. Das gab ihm den
Kick, er verlangte nichts anderes von mir. Und das war mir recht so! An
diesem Abend fühlte ich mich stark. Mich überkam ein ausgeprägtes
Machtgefühl, das mir half, Verletzungen aus der Vergangenheit weiter zu
verarbeiten. Seit dieser Erfahrung und mit jedem neuen Kunden gewinne
ich mehr und mehr die Kontrolle über meine Sexualität. Ich erobere
eigenmächtig das patriarchale System, das mir aufgedrängt wurde -- und
noch immer wird.

Ein paar Anekdoten:

Einmal bin ich mit einem Kunden im Auto in der Nähe meiner Eltern
vorbeigefahren. Ganz plötzlich habe ich mich geduckt, der Kunde war
entsprechend erstaunt. Heute sage ich mir, dass ich meinen Eltern
einfach hätte erzählen können, dass ich am Trampen war.

Einmal hatte ich die Gelegenheit, mit einem Kunden über Anarchismus zu
sprechen. Früher war er Kommunist, heute ist er Kapitalist. Die
Diskussion war konstruktiv und hatte in diesem Kontext ihren Reiz.
Später fragte ich mich aber, ob ich als bekennende Anarchistin nicht
schon zu viel von meiner Identität preisgegeben hatte. Ups.

Einmal, als ein Kunde mir meine Muschi leckte, hatte ich einen Orgasmus
und schrie ganz laut und ohne Absicht: "*PUTAIN[^2]*!" Es war zum
ersten Mal in meinem Leben, dass ich während einem Orgasmus ein
Schimpfwort schrie. Und dies erst noch, um ein Vergnügen auszudrücken.
*Putain*. Das Wort wird ständig falsch verwendet und deshalb verbogen.
Wir sollten aufhören, das Wort als eine Beleidigung oder als Schimpfwort
zu verwenden. Wir sollten ihm eine neue Bedeutung geben. Denn es ist ein
Wort, das vor Kraft und Vergnügen nur so strotzt.

Doch warum eigentlich sollten wir nicht von diesem patriarchalen System
profitieren, das uns aufgezwungen wird? Dieses System, das wir einfach
so ertragen, obwohl wir es gar nicht gewählt haben. Es stellt uns als
sexuelle Objekte dar. Gerade deshalb können wir uns bewusst dafür
entscheiden und uns mit unseren eigenen Werkzeugen dagegen wehren -- und
dabei die bestehenden Machtpositionen umkehren. Entgegen der Meinung
einiger Feministinnen, die gegen Prostitution sind und sie abschaffen
wollen, ist Sexarbeit nicht zwingend etwas, das wir erdulden müssen. Für
einige von uns ist es eine willkommene Möglichkeit, die Kontrolle
zurückzugewinnen. Ein Hilfsmittel, um das Patriarchat° zu bekämpfen, und
manchmal ganz einfach ein Weg, um an Geld für Essen zu kommen.

Als SA verrichten wir eine Arbeit. Und in jedem Arbeitsbereich gibt es
Ungleichheiten, was die Arbeitsbedingungen betrifft. Wenn über
Prostitution und ihre geschlechtsspezifischen Implikationen nachgedacht
wird, wird der Beruf oft auf eine einzige Realität reduziert.
Vorherrschend ist die Meinung, dass dem Nuttendasein eine dunkle,
schmutzige Seite innewohnt. Eher sollten wir aber die Komplexität und
Vielfalt hervorheben, die in diesem Milieu beobachtet werden kann. Und
weil es tatsächlich in der Realität für einige SA schwieriger ist als
für andere, sollten wir Gewerkschaften gründen, unsere
Arbeitsbedingungen verbessern, Demonstrationen organisieren, lauthals
unsere Forderungen kundtun, ohne zu vergessen, dass\...

\...Sexarbeit libertär sein kann. Wir alle haben das Recht auf sexuelle
Befriedigung ohne Liebe.

\...Sexarbeit subversiv sein kann. Denn auf dem "Kampffeld" Sex kann das
Patriarchat destabilisiert werden.

\...Sexarbeit, d. h. das Anbieten von Sex gegen Geld, die bestehenden
Herrschaftsverhältnisse umkehren kann.

\...Sexarbeit auch Sozialarbeit ist.

\...Sexarbeit auch ein bisschen einem Theater gleicht: Du stellst eine
Figur dar, ohne sie in Wirklichkeit zu sein.

\...Sexarbeit die Vorstellung des männlichen Dominierens suggeriert.
Dieses wird aber nicht zwingend reproduziert. Es handelt sich vielmehr
um einen Austausch.

Wollt ihr mich treffen, schreibt mir auf Telegram... Ein Abend kostet
600 Franken 😉.

[^2]: *Putain* bezeichnet auf Französisch eine Hure bzw. eine Nutte und wird im Alltag als derbes Ausrufewort verwendet (auch z. B. *Putain de merde* = verdammte Scheisse).
