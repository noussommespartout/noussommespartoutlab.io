---
author: "Amsel"
title: "Aus Liebe zum Leben"
subtitle: "Aus dem Herzen eines Menschen"
info: "Für den deutschen Sammelband verfasst"
datepub: "Im November 2021 verfasst"
categories: ["Selbstorganisation, kollektives Experimentieren", "Polizei", "Squat, Besetzungen, Wohnen", "Gefängnis, Justiz, Repression", "Gewalt, Gewaltlosigkeit", "Ökologie", "Ökofeminismus"]
tags: [""]
langtxt: ["de"]
zone: "Deutschschweiz"
num: "61"
quote: "Ich bin die Natur, die sich verteidigt!"
---

Aus Liebe zum Leben drohen mir drei Monate Knast. Aus Liebe zum Leben
habe ich den Hügel *Mormont* mit meinem eigenen Körper verteidigt. Aus
Liebe zum Leben sass ich auf einen Baum, meine nackte Brust übergossen
mit roter Farbe, symbolisch für das Blut unzähliger Lebewesen, das
unsere Gesellschaft an den Händen hat. Aus Liebe zum Leben wurde ich von
diesem Baum von der Polizei fortgetragen, in eine Zelle gesteckt,
beschimpft und eingeschüchtert, meine Rechte wurden missachtet. Aus
Liebe zum Leben war ich auf der *ZAD de la Colline*. Aus Liebe zum Leben
nehme ich meine Verantwortung wahr und werde die drei Monate Gefängnis
absitzen, die mir als Strafe auferlegt wird, weil ich das Leben schützen
wollte und dafür des Hausfriedensbruches beschuldigt werde.

Es hiess, dass alle gewonnen hätten in diesem Kampf um den *Mormont*, um
das Leben. Es hiess, die Bewohner\*innen des Hügels hätten der
Gesellschaft die Augen geöffnet, Missstände aufgedeckt, Alternativen
aufgezeigt. Es hiess, dass die Polizei keine Gewalt angewendet und unser
Rechtsstaat sich durchgesetzt habe.

Doch: Wurde wirklich keine Gewalt angewendet? Ist unser Rechtsstaat
wirklich so rechtens? Diese Räumung und dessen Folgen ist das
Gewalttätigste und Traumatisierendste, das ich jemals erlebt habe. Sie
haben zwar nicht scharf geschossen, haben uns keine Knochen gebrochen,
haben uns nach 24 Stunden brav wieder aus dem Gefängnis entlassen. Sie
haben uns aber klar gemacht, dass wir in dieser Gesellschaft nichts zu
sagen haben. Sie haben uns klar gemacht, dass Alternativen keinen Platz
haben. Sie haben uns klar gemacht, dass sie uns alles nehmen können,
wenn sie es wollen. Sie haben uns klar gemacht, dass das Gesetz in jedem
Fall durchgeboxt wird. Sie haben uns klar gemacht, dass wir illegal
handeln. Auch wenn unser Leben auf dem Hügel im Einklang mit unserer
Umwelt und unseren Mitmenschen legitim war. Sie haben uns klar gemacht,
dass sie Träume zerstören können. Sie haben uns klar gemacht, dass die
Wirtschaft und das Gesetz um jeden Preis geschützt werden müssen. Das
Leben, das wir mit unserem ganzen Herzen, mit jeder unserer Zelle so
sehr lieben, ist dabei irrelevant. Es wird von einer Armee schwarzer
Riot-Polizisten[^1] niedergetreten, mit monströsen Maschinen abgetragen,
in Hochöfen verbrannt, im Namen des Gesetzes ausgelöscht. Nochmals: Was
ist Gewalt? Wen schützt der Rechtsstaat? Haben wir der Gesellschaft
wirklich die Augen geöffnet? Anerkennt die Gesellschaft ihre eigenen
Missstände? Will unsere Gesellschaft überhaupt Alternativen?

Ich werde diese drei Monate im Gefängnis absitzen. Ich werde jeden
einzelnen Tag, welcher ich in einer Zelle verbringen werde, an jenen
Baum denken, der mich beherbergt hat und den ich nicht fähig war, zu
schützen. Den Baum, auf dem ich eine Hütte gebaut habe, ohne ihm einen
einzigen Nagel einzuschlagen, ohne ihm einen einzigen Ast abzusägen,
ohne ihm eine einzige Wunde zuzufügen. Den Baum, den ich von ganzem
Herzen liebe und von unserem Rechtsstaat getötet wird.

Es tut mir leid, dass ich dich nicht schützen konnte. Diesen Kampf habe
ich verloren und werde teuer dafür bezahlen. Du wirst noch teurer
bezahlen. Du bezahlst mit deinem Leben. Ich werde aber niemals aufhören
für meine Brüder Bäume und Schwestern Blumen zu kämpfen. Ich werde dies
tun, solange eine Zelle in mir lebendig ist. Das verspreche ich dir. Ich
verteidige nicht die Natur. Ich bin die Natur, die sich verteidigt!

## Was ist los auf dem Hügel *Mormont*?

In Eclépens (VD) will der milliardenschwere Zementhersteller
lafargeholcim den vorhandenen Kalksteinbruch auf das Gebiet, das im
Bundeslandschaftsinventar aufgeführt ist, erweitern. Der Hügel *Mormont*
wird durch diese Erweiterung wortwörtlich in zwei geteilt. Dabei werden
weite Teile ökologisch einzigartiger Flächen vernichtet. Es wird durch
diese Erweiterung nicht nur die Zerstörung von Biodiversität in Kauf
genommen, sondern es wird dadurch auch eine keltische Kulturstätte, die
als europäisches Kulturerbe eingestuft ist, weggesprengt.
Lafargeholcim's zerstörerische Aktionen ziehen sich über den ganzen
Globus. Dem in der Schweiz ansässigen Konzern werden
Menschenrechtsverletzungen, Kulturerbe- und Umweltzerstörungen
vorgeworfen. Zudem ist die Zementindustrie eine der umweltschädlichsten
Industrien überhaupt, sie ist extraktivistisch und verursacht 8 % des
globalen CO~2~-Ausstosses. Um sich diesem Treiben zu widersetzen, wurde
auf dem Hügel *Mormont* die *ZAD de la Colline* errichtet. Eine ZAD
(*zone à défendre*) ist ein Weg, um gegen solche extraktivistische und
zerstörerische Projekte Widerstand mit unseren eigenen Körpern zu
leisten. Darum wurde von Oktober 2020 bis März 2021 der Hügel *Mormont*
besetzt. Am 31. März kam es zur Räumung der *ZAD de la Colline*. Eine
ZAD ist aber auch ein Ort, wo Utopien gelebt werden. Es werden reelle
Alternativen zum herrschenden kapitalistisch-patriarchalem System°
geschaffen. Solche Orte stellen das Leben in den Mittelpunkt, vor Ort
wird im Einklang mit der Flora, Fauna und den Menschen gelebt.
Inklusion, Partizipation, Selbstorganisation° und Horizontalität sind
wichtige Elemente, um neue Arten des Zusammenlebens zu kreieren. Eine
ZAD ist daher nicht nur ein Ort des Widerstandes, sondern auch ein Ort
für soziale Experimente und Träume.

[^1]: Hier wird explizit die männliche Form verwendet, da es
    hauptsächlich Cis-Männer waren, welche die ZAD geräumt haben. Dies
    ist Teil der strukturellen Staatsgewalt, die auch in den Texten
    *Drohnen* \[n°1\] und *They Don't see us* \[n°4\] Thema ist.
