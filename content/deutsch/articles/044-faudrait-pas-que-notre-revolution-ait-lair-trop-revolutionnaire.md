---
author: "Einige Aktivist*innen von XR, die in der Schweiz politisch aktiv sind"
title: "Ganz so revolutionär sollte unsere Revolution doch nicht aussehen"
subtitle: "Selbstorganisierte Diskussion zwischen mehreren Aktivist*innen von Extinction Rebellion (XR)"
info: "Transkribierung einer mündlichen, im Juni 2020 auf Französisch geführten Diskussion"
datepub: "Im Herbst 2021 auf Deutsch übersetzt"
categories: ["Gewalt, Gewaltlosigkeit", "Ökologie", "Selbstorganisation, kollektives Experimentieren"]
tags: ["Selbstkritik", "Wohlwollen", "Witz", "Fressen", "schreien", "Empathie", "Fehler", "Kraft", "Frankreich", "Regierung, regieren", "Maul", "Ungerechtigkeit", "Internet, Web", "Gerechtigkeit", "Logik", "Logo", "Scheisse", "Angst", "Pizza", "Macht", "Wut", "Strasse", "Sabotage, sabotieren", "Stille", "Lösung", "Theorie", "Gewalt", "Abstimmung"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "44"
quote: "oh nein, wir mögen doch keine Gewalt, Gewalt ist Kacke"
---

## Kapitel 1 --- Ganz so revolutionär sollte unsere Revolution doch nicht aussehen

--- Das ganz grosse Thema bei Extinction Rebellion (XR) ist, wie radikal
wir wirklich sein sollen oder wollen, was noch akzeptabel ist und warum
sich andere politisch aktive Bewegungen von uns entfernen.

--- Das ist doch ganz einfach. Im Moment ist XR ein Gruppe von Weissen
Kleinbürgerlichen, denen abgesehen vom Klima vieles egal ist. Eigentlich
ist XR ja gar nicht dafür geeignet, was die Bewegung jetzt tut. Das
Projekt von XR besteht darin, alles Mögliche zu unternehmen, um Menschen
dazu zu bringen, zu rebellieren, beispielsweise durch Ausbildungen im
Bereich des gewaltfreien zivilen Ungehorsams. XR ist eine Bewegung, die
gegenüber anderen Kämpfen und Bewegungen offen sein sollte; uns sollte
mensch sagen können, wenn wir etwas falsch machen. Denn je zahlreicher
wir sind, desto eher verleihen wir unseren Forderungen Gewicht und es
ergibt Sinn, wenn wir alle gemeinsam gegen die Unterdrückung ankämpfen.

--- Oha, du legst ja gleich voll los. (*Lachen*)

--- XR gründet auf drei Forderungen.. Vielleicht müssen wir sie uns in
Erinnerung rufen.

--- Verflixt! (*Lachen*)

--- Der Klimanotstand muss ausgerufen, die Treibhausgasemission müssen
bis 2025 auf Netto-Null reduziert und es müssen unabhängige
Bürger\*innen-Versammlungen zur Klärung der Umweltfrage einberufen
werden. Eigentlich müssten wir eine vierte, soziale Forderung
formulieren, denn aktuell erscheint XR überhaupt nicht wie eine
Bewegung, welche die Gesellschaft verändern will.

--- Aktuell ist XR Schweiz eine Bewegung, die sich in ihrer kleinen
Nische verschanzt und keine Selbstkritik betreibt. XR bleibt in der
Komfortzone des gewaltfreien, ökologischen Ungehorsams ohne auf die
Kritik anderer politisch aktiven Bewegungen einzugehen.

--- Der Teufelskreis besteht darin, dass die Bewegung nur Menschen
anzieht, die sich mit ihren Leitsätzen identifizieren. Und diese Masse
überzeugter XR-Aktivist\*innen erdrückt dann all jene, die
selbstkritisch sind.

--- Unter uns nennen wir das den "*XR Social Club*".

--- Und was ist das bitteschön?

--- So eine Art *Lions Club*. Die Menschen treffen sich, um
freundschaftliche, soziale Beziehungen zu pflegen, was ja grundsätzlich
toll ist, doch so wird es nun mal nicht gegen aussen dargestellt. Es
heisst, es wäre ein radikale Bewegung im Namen des Klimas, doch oft
sieht es in der Realität anders aus: Die Zeit verstreicht und wir
fressen Pizza\...

--- Das entmutigt gerade jene, die sich gerne radikalisieren möchten. Das
hat eine beruhigende Wirkung, obwohl es überhaupt nicht der Moment ist,
um uns ruhig zu verhalten\...

--- In politisch aktiven Kreisen ist das tatsächlich ein Problem. Es wird
etwas lanciert, die Anfangsenergie ist gross, es entsteht etwas und dann
flacht es ab und wird zu etwas, das vorderhand aus Affinität getan wird.
Genoss\*innen werden zu Freund\*innen, weshalb die kämpferische Momente
zu sehr angenehmen Momenten werden. Du gehst mit Freude hin, doch ohne
es zu merken, ist eine Gruppe von Freund\*innen entstanden, die nicht
mehr nach der Logik einer politisch aktiven Gruppe funktioniert, und
deshalb auch ausschliessend ist\... Es ist doch schon so nicht einfach,
sich einer politisch aktiven Gruppe anzuschliessen, geschweige denn,
wenn es eine Gruppe von Menschen ist, in der sich alle super gut kennen.

--- Wir sind unter uns, vertreten die gleichen Meinungen, sind uns einig
--- nichts stellt den *social club* von XR in Frage. Wir gleichen einer
abgeschlossenen Blase. Die Bewegung ist statisch geworden und kann von
sich aus keinen Impuls mehr geben, um sich infrage zu stellen und
weiterzukommen.

--- Was mich persönlich an diesem *social club* schockiert, ist, dass XR
zu deiner Identität geworden ist, du klammerst dich richtiggehend
affektiv an die Bewegung an, du stehst für sie ein, doch deine
Argumentationen werden manchmal irrational.

--- Wird XR kritisiert, dann verteidigst du nicht mehr XR und die Ideen,
sondern dich selbst und deine Freund\*innen. Das ist etwas ganz anderes!

--- Für mich stellen auch die "Prinzipien und Werte" ein Problem dar.
Willst du dich XR anschliessen, musst du sie gutheissen..

--- Wir müssten sie wahrscheinlich kurz zusammenfassen, nicht?

(*Schweigen*)

--- Grundsätzlich kann jede\*r XR für sich in Anspruch nehmen, sofern
er\*sie sich mit den zehn Prinzipien und Werten einverstanden erklärt.
Mensch kann diese Prinzipien und Werte interpretieren wie mensch will,
aber natürlich gibt es eine Art dominierende Interpretation, die daraus
abgeleitet wird und die so schon fast dogmatisch wird sowie unantastbar
und sozial akzeptiert ist. Mit den Aktionsformen ist es genau gleich. In
der Vergangenheit gab es diverse Blockaden von Brücken. Seither machen
wir fast nur noch solche Aktionen. Sie sind zu einer akzeptierten
Aktionsform avanciert. Alles andere kennen wir nicht --- also lassen wir
auch die Finger davon. Wir wollen kein Risiko eingehen. Am Anfang haben
wir mit der Polizei zusammengearbeitet. Ein Jahr später werden
diejenigen blossgestellt, die sich radikalisiert haben und nicht mehr
mit der Polizei zusammenarbeiten wollen oder dies zumindest infrage
stellen.

--- Das ist eine interessante Beobachtung. Denn bleibt eine politische
Bewegung unter sich, dann entdeckt sie keine neuen, anderen politischen
Aktionsformen. Wir wissen, dass radikale direkte Aktionen ---
beispielsweise das Abfackeln von Banken, so wie es die Suffragetten°
anfangs des 20. Jahrhunderts in Grossbritannien für ein allgemeines
Frauenstimmrecht machten --- gut funktioniert haben. Wir sind uns auch
bewusst, dass grosse Bewegungen, die als pazifistisch bezeichnet werden,
wie beispielsweise die indische Entkolonialisierung oder die
Bürgerrechtsbewegung in den USA auch von gewalttätigen Aktionen
begleitet wurden. Bei XR weist eine Mehrheit eine solch radikale
Aktionsform zurück. Es gibt keine Aktionen mit anderen Bewegungen, keine
solidarischen Aktionen mit Gemeinschaften.

--- Doch das liegt auch daran, dass viele Leute die Facebook-Page liken,
einen XR-Button tragen, doch in Wirklichkeit nicht Konkretes
organisieren.

--- Hängt das nicht auch mit den Ursprungsideen von XR zusammen? XR ist
etwas Neues, Interessantes und Problematisches zugleich: eine Rebellion
und eine Bewegung, die pfannenfertig angeboten wird und aus der mensch
Prinzipien und Werte übernimmt, die von anderen Menschen in einem
anderen Kontext geschaffen wurden. Wenn du zu XR stösst, heisst es:
Voilà, das sind unsere Prinzipien und Werte, unsere taktischen Methoden
--- so nach dem Motto: So und so läuft das hier ab und nicht anders! Auf
dem Internet findest du alles: Tutorials, wie neue Leute ausgebildet
werden (sollen), Listen mit Aktionsformen, Ratschläge, pfannenfertige
Argumentarien, Medienmitteilungen. Alles ist schon da! Es ist alles
andere als eine Bewegung, in der mensch Grundsatzdiskussionen führt: Im
Grunde ist es eigentlich ein taktisches Projekt. Wir blockieren die
Städte, versuchen in kürzester Zeit einen kleinen Teil der Bevölkerung
zu mobilisieren, um eine schnelle Veränderung herbeizuführen. Und wenn
das nicht funktioniert, dann müssen wir eben etwas anderes machen.

--- Doch gerade das finde ich auch beeindruckend; weil pfannenfertige
Strategien bereitstehen, konnten sich spontan überall auf der Welt neue
Gruppen organisieren. Es gibt Gruppen in Kalifornien, Indien, Schweden,
Ghana, Japan, Marokko usw. Doch das Problem ist eben, dass die Argumente
und die Taktiken von XR England entwickelt wurden und deshalb nicht
einfach in einen anderen Kontext kopiert werden können: die politischen
und ökologischen Voraussetzungen sind nicht vergleichbar, genauso wenig
wie die Polizeirepression oder die Justizapparate. Das zeigt sich daran,
dass das XR-Logo von Gruppen verwendet wird, die in Südostasien gegen
die Industrialisierung oder Kolonialisierung ankämpfen, die in
Zentralafrika oder auch anderswo feministische Kämpfe führen. Das Logo
ist zu einem globalen Gerechtigkeitssymbol geworden. Das ist doch super!

(*kurzes Schweigen*)

--- Das stimmt, dass bei XR alles ziemlich strukturiert und geregelt
daherkommt. Zumindest hatte ich diesen Eindruck, als ich mich XR
anschloss und die angebotenen Ausbildungen machte. Denselben Eindruck
habe ich, wenn es um grundlegende Auseinandersetzungen geht. Oft hört
mensch dann: "Ja, aber wir haben doch diese übergeordnete Regeln,
deshalb können wir das nicht noch einmal diskutieren, so ist es nun
mal." Es gibt eine vorgegebene Marschrichtung, und die wird als einzig
legitime dargestellt. Vor allem gibt es diesen "Aktionskonsens°": Wenn
du an einer XR-Blockade teilnimmst, erklärst du dich explizit mit
gewissen Regeln einverstanden. Beispielsweise, dass kein Alkohol
getrunken oder die Polizei nicht beleidigt wird. Wenn du dich dem
Aktionskonsens quer stellst, bekommst du Probleme.

--- Eines unserer Probleme ist beispielsweise, wie wir uns gegenüber der
Polizei verhalten. Mensch soll keine polizeifeindlichen Parolen rufen,
die Polizeiarbeit muss respektiert werden. An physische
Auseinandersetzungen mit der Polizei darf schon gar nicht gedacht
werden. Wenn du versuchst, eine Grundsatzdiskussion zu führen, ja dann
sind eben genau die Leute, die diesen dogmatischen "Konsens" prägen,
nicht präsent. In der Schweiz wurden diese Themen einmal auf den Tisch
gelegt. Die Idee war, sie einen Tag lang zu diskutieren. Doch wie immer
kamen am Ende nur die üblich Verdächtigen; jene, die von ihrer
Anti-Polizei-Haltung überzeugt sind. Niemand interessiert sich für diese
Themen, weiter kommen wir so nicht.

--- Ganz viele XR-Aktivist\*innen legen auch grossen Wert auf das
(Selbst-)Bild der Bewegung. Entweder, weil es ihnen um ihre eigene
Person geht und sie keinen Bock haben, mit einer aus ihrer Sicht zu
radikalen Sache in Verbindung gebracht zu werden, oder weil sie am
medialen Bild der netten Bobos°, die nicht zu sehr stören, festhalten
wollen.

--- Natürlich sagen sie das nicht in diesen Worten, schon klar. (*Lachen*)

--- Die Aktionen von XR basieren so sehr auf ihrer Wirksamkeit in den
Medien, dass wir gezwungen sind, nicht von gewissen Mustern in Bezug auf
Argumentationen, Vorgehen und Aktionen abzuweichen, die den Medien als
akzeptabel und gut wahrnehmbar erscheinen.

--- Wir sind medienwirksam geworden.

--- Das stimmt.

--- Bist du bei den Medien beliebt, vertrittst du automatisch eine
reformistische (Lobbying-)Haltung. Du übst zwar Druck auf den Staat aus,
in der Hoffnung, dass der Staat Lösungen bietet, weil du im Grunde daran
glaubst, dass der Staat eine Lösung parat hat. Taktisch gesehen haben
diese Leute eigentlich recht, ihre Logik geht auf. Um in einen Dialog
mit dem Staat zu treten, hilft ein grosser revolutionärer Block, der
alles kurz und klein schlägt und nicht macht, was die Polizei sagt,
nichts: Der Staat wird dir nicht zuhören! Der Staat geht davon aus, dass
er der Wille des Volkes vertritt, jedenfalls versteckt er sich immer
hinter dieser Sichtweise. Es ist ganz einfach: Entweder du spielst das
manipulierte Spiel der Demokratie mit, oder du kommst ins Gefängnis.

--- Das ist gleichzeitig die Stärke und Schwäche von XR: Die Bewegung
will möglichst viele Leute ansprechen. Um die Leute aber zu überzeugen,
darfst du nicht zu radikal sein. So bewegst du dich auf einer ständigen
Gratwanderung --- eine bizarre Halb-Radikalität. Ich war überhaupt nicht
radikal, als ich mich XR angeschlossen habe. Durch den Austausch habe
ich aber viele Dinge gelernt und schon das alleine gibt mir unheimlich
viel Mut. Doch ich glaube, dass wir von dieser Bewegung nichts anderes
erwarten sollten, als dass sie eine Art Eintrittskarte ist, um sich
später anderen Formen von politischem Aktivismus zu widmen.

--- Aus dieser Perspektive sollten radikale Aktivist\*innen bei XR
bleiben, um den Link zu anderen Aktionsformen und radikaleren Bewegungen
zu machen.

--- Genau. Doch das ist gar nicht so einfach.

--- So sicher bin ich da nicht. Die Leute werden sich vielleicht im
Verlauf ihres Werdegangs radikalisieren, doch sicher ist das nicht. Die
Wirkung von XR darf auch nicht überschätzt werden. Schlussendlich stützt
sich die Bewegung doch nur auf ein paar wenige ab. Der grosse Unterbau
von Sympathisant\*innen dagegen zögert jeweils zwischen "auf den Markt
gehen" oder "an einer Aktion mitmachen".

--- Doch eigentlich fördern wir doch das, indem XR auf dem Markt T-Shirts
verkauft.

--- Genau das zeigt, dass wir das Problem nicht wirklich erkannt haben.
Es gibt Hunderte von Menschen, die mit einem XR-T-Shirt in der Stadt
herumlaufen, sich aber weder an Aktionen beteiligen, noch die Bewegung
mitgestalten. Sie werden sich auch nicht radikalisieren, selbst wenn auf
ihrer Brust das Wort "Rebellion" tragen, ohne dabei zu verstehen, was
das wirklich heisst.

--- Das ist verächtlich, was du sagst.

--- Vielleicht, ja. Doch eines ist klar: Wir stecken bereits jetzt tief
in der Scheisse und müssten eigentlich dabei sein, den Staat zu
sabotieren.

--- Ja.

--- Ja.

--- Ja.

--- Genau.

(*kurzes Schweigen*)

## Kapitel 2 --- Raupen, Pandas und die Beziehungen in Herrschaftssystemen

--- Das Problem ist also möglicherweise das Argumentationsmuster, das
durch XR benützt wird.

--- Beispielsweise die "Kurse" zum Thema Klimanotstand sowie der
"*talk*", der dir angeboten wird, wenn du dich der Bewegung anschliessen
willst.

--- Der *talk* ist eine Präsentation von XR-Aktivist\*innen, die allen
offensteht, und in der verschiedene Themen behandelt werden: die Gründe
für den Klimawandel, die wissenschaftlichen Vorhersagen wie
beispielsweise der Bericht des Weltklimarates, die Untätigkeit der
Regierungen, die Grenzen eines verantwortungsvollen, persönlichen
Konsums, aber auch die Strategie von XR und warum gewaltfreier ziviler
Ungehorsam wirkungsvoll sein kann.

--- Wir sollten während diesen *talks* einfach möglichst gut erklären,
was vor sich geht, damit die Leute von sich aus Lust haben, die
Infrastrukturen der Unterdrückung in Flammen zu setzen. In Tat und
Wahrheit dürfte doch eigentlich niemand untätig bleiben. Ich meine, wir
sehen doch, wie verrückt das ist, was da abläuft. All die Leute, die
keine Lust haben, sich fürs Klima einzusetzen, sollten doch eigentlich
vor Angst verrecken. Das ist doch die Realität.

--- Doch das ging daneben. Der *talk*, den XR anbietet, macht eben genau
das nicht.

--- Ich finde diese *talks* und die Kurse ganz gut, aber wir müssen den
wissenschaftlichen und ökologischen Argumenten und Realitäten, denen
immer entgegengehalten wird, mehr politisches Gewicht verleihen. Es wird
beispielsweise kein Wort darüber verloren, wie die Verstrickungen im
Herrschaftssystem sind und wie systemisch° sie sind. Natürlich können
die wissenschaftlichen Argumente nicht abgestritten werden, doch sie
reichen nicht aus, um eine gründliche soziale und politische Analyse der
Situation zu machen.

--- Oft wird aber nur drum herumgeredet. Natürlich gibt es Gandhi, doch
dieser Entkolonialisierungskampf lief in einem gewalttätigen Umfeld ab.
Die Suffragetten haben Banken abgefackelt, um das Stimmrecht zu
erhalten. Luther King hat die Bewaffnung der meisten Aktivist\*innen der
*Black Panther Party* unterstützt. Mandela hat den militärischen Flügel
der ANC[^13], *Umkhonto we Sizwe*, mitbegründet, hunderte
Sabotage-Aktionen angeführt und den bewaffneten Kampf verteidigt.

--- Was die Gewalttätigkeit betrifft: Da geht es um ganz was anderes als
ACAB auf Mauern zu sprayen. Wenn du das während einer XR-Aktion tust,
dann wirst du zurückgepfiffen oder sogar von der Aktion ausgeschlossen.
Sogar wenn du die Polizei ausbuhst, wirst du ermahnt.

--- Diese *talks* müssten XR eigentlich wie ein systemische Bewegung
präsentieren. Denn die Klimakrise hat ihren Ursprung in der Ausbeutung
des Lebendigen und allen anderen Formen von Herrschaft: Kapitalismus,
Patriarchat°, Rassismus und Speziesismus. Zudem gibt es eine natürliche
Konvergenz von sozialen und ökologischen Kämpfen. So sind beispielsweise
die Gewerkschaften oft die ersten, die mit ihrem Arbeitskampf auf
ökologische Gefahren in der Industrie aufmerksam machen.

--- Die Kurse müssten den Staat und das Klima behandeln. Wenn du dir alle
jene Gesetze anschaust, die der Staat geschickt eingeführt hat, merkst
du schnell: Keines wird wirklich respektiert! Das ist das beste Mittel,
um den gewaltfreien zivilen Ungehorsam zu fördern: Aufzuzeigen, dass der
Staat keines seiner eigens verabschiedeten Gesetze respektiert.

--- Im Grossen und Ganzen respektiert der Staat die Gesetze nicht, von
denen das Volk das Gefühl hat, es habe sie selbst gemacht.

--- Was mich auch immer wieder erstaunt, ist, dass sich Umweltbewegungen
und -kämpfe auf die Sichtweise der exakten Wissenschaften wie Biologie,
Klima usw. abstützen, ohne die Sozialwissenschaften beizuziehen, die
beschreiben und erklären, wie die zahlreichen Unterdrückungsmechanismen
miteinander verbandelt sind. Politische Theorien sind weitgehend
verschwunden, die Soziologie wird kaum mehr herangezogen in den
ökologischen Bewegungen. Selbst radikalere Gruppen neigen dazu, einen
grossen Wirbel um das Verschwinden der Gletscher und eine ganze Reihe
von Fakten zu machen, die überzeugend erscheinen. Der Rest verschwindet,
als wären wir davon überzeugt, dass diese gesellschaftspolitische
Diskussion nicht mehr notwendig ist.

--- Ich bin ja so etwas von überzeugt, dass wir nie gewinnen werden, dass
es mir egal ist. Mensch könnte mir alles sagen, mensch könnte mir sagen
"Wir wollen rosarote Kacke", ich würde trotzdem auf die Strasse gehen,
um das geht es nicht\... Aber vielleicht bin ich schon so lange aktiv,
dass ich mir nicht einmal mehr die Frage stelle, was wir eigentlich
einfordern --- und das ist doch schwachsinnig! Aber eben: Aus meiner
Sicht werden wir nie gewinnen. Es ist schlicht zu spät, ich bin nur noch
aktiv, um meine Menschenwürde zu retten.

(*ein Moment lang Stille*)

## Kapitel 3 --- Die ganze Welt hasst... Gewalt

--- Um festzustellen, was nicht funktioniert, müssen viele Dinge
ausprobiert werden. Wenn du direkt mit Ungerechtigkeit konfrontiert
bist, wenn du Gummischrot abbekommst, dann ist das nicht das Gleiche,
wie wenn du darüber in der Zeitung liest. Es gibt viele Leute, die sich
nach einer Verhaftung radikalisieren. Genauer gesagt: Es gibt viele
Arten von Gewalt, die dir als fiktional, irreal oder medial erscheinen,
solange du sie nicht selbst erlebt hast.

--- Daher rührt auch das Paradoxon, dass sich aus der Beziehung zwischen
XR militanten Aktionen ergibt.

--- Du meinst "gewalttätige" Aktionen?

--- Oh nein, wir mögen doch keine Gewalt, Gewalt ist Kacke.

--- XR ist eine Bewegung, die gewaltfreien zivilen Ungehorsam
befürwortet, und es ist kein Problem, diese Taktik zu wählen, sie ist
inklusiv und funktioniert, um staatliche Gewalt sichtbar zu machen. Das
Problem ist doch, dass wir uns nicht einig sind, was gewalttätig ist und
was nicht, insbesondere innerhalb der Bewegung.

--- Diese Debatte findet übrigens gar nicht statt. Gewaltlosigkeit ist
quasi zu einem Zwang geworden. Folglich wird immer abgewogen, alles soll
leicht verdaulich und glatt sein.

--- Während jeder Aktion haben alle eine präzise Aufgabe zu erledigen. So
nehmen Aktivist\*innen während der Aktionen die Rolle der *peacekeeper*
ein. Auch das ist etwas, das von XR (UK) übernommen wurde. Diese
*peacekeeper* überwachen, dass die Aktionen nicht aus dem Ruder laufen.
Sie greifen beispielsweise sofort deeskalierend ein, wenn die Stimmung
gegen die Polizei aggressiv wird oder es mit Passant\*innen zu
Auseinandersetzungen kommt.

--- Solange XR versucht, ihre Aktionen ohne Gewalt zu organisieren,
werden immer weniger Menschen Staatsgewalt am eigenen Leibe erfahren.
Doch genau diese Erfahrung ist doch das Fundament der Taktik des
gewaltlosen Widerstands, nicht?

--- Das ist so paradox.

--- Ganz ehrlich, wir müssen doch keinen Stuss reden, in XR-Blockaden
erfährt mensch nahezu keine Staatsgewalt. Diese habe ich woanders
erfahren. Ich kenne die Theorie des gewaltfreien zivilen Ungehorsams.
Ich bin mir also bewusst, dass es sich um ein taktisches Mittel handelt,
um Staatsgewalt zu provozieren, und sie so erst sichtbar gemacht werden
kann. Doch in Wirklichkeit bekommt mensch, indem er in seiner Position
der Gehorsamkeit und Disziplin verharrt, nur vage Bilder von Menschen
mit, die herumsitzen und von Bullen weggetragen werden.

--- Es wäre falsch zu glauben, dass der Schweizer Staat Gewalt gegenüber
Weissen Personen anwendet, die sich als friedlich bezeichnen. Die
Repression, die wir bei XR erfahren, ist anders. Die Bullen werden dich
verfolgen, dich unter Druck setzen, dich auf dem Polizeiposten demütigen
--- so lange, bis die Justiz dich erdrückt. Doch diese Art von
Gewaltanwendung kommt nicht öffentlich zum Ausdruck. Es war klar ein
Fehler zu glauben, dass uns der Staat und die Polizei spektakulär
behandeln und abführen würde.

--- Ein Beispiel in Frankreich macht klar: Dort hat die Anti-Riot-Polizei
(CRS) einmal ein Sit-in von XR mit Tränengas aufgelöst. Weil die Medien
dieses Vorgehen so stark kritisiert haben, hat die Regierung nun andere,
heimtückische Strategien entwickelt. Sie lässt die Bewegung in ihrem
eigenen inaktiven Ungehorsam sterben.

--- Wenn während XR-Aktionen die Bullen eingreifen, dann gibt es Leute
die meinen: "Die Polizei macht doch nur ihre Arbeit. Das sind ganz nette
Menschen." Manchen mag dieser Eingriff schon gewalttätig vorkommen, doch
die meisten um dich herum, werden sagen, dass es doch nur "normal" ist,
dass uns die Bullen abführen, aber sicher nicht gewalttätig. (*bitter enttäuschtes Lachen*)

--- Bei der Planung von Blockadeaktionen wird alles vorgängig geplant,
sogar der Einsatz der Bullen. Dabei werden auch Personen bestimmt, die
verantwortlich sind, mit den Bullen in den Dialog zu treten. Wenn die
Polizei auftaucht, sind sie nicht im Geringsten verwirrt, sie wissen
bereits an wen sie sich wenden müssen, alles ist vorbereitet, weshalb
eine Aktion zur Routine wird. Es ist immer das Gleiche.

--- Die Polizei braucht nicht einmal mehr Mega- bzw. Mikrofone.

--- Genau, sie braucht quasi nicht mal mehr ihren eigenen Job zu machen..

--- Ich stimme nicht ganz mit dir überein, dass die Polizei niemals
Gewalt gegenüber Weissen, alternativ angehauchten
Wohlstandsbürger\*innen ausübt. Vielleicht nicht bei einer
XR-Brückenblockade, doch wie uns die Vergangenheit gezeigt hat, ist auch
die ökologische Bewegung nicht von Gewalt verschont geblieben. Die
Polizei ist gewalttätig, wenn mensch sie überragt. Doch bei einer
XR-Blockade weiss die Polizei ganz genau, was sie erwartet. Deshalb kann
sie seelenruhig ihre Arbeit verrichten und dabei erst noch geeignete
Bilder fürs Fernsehen liefern.

--- Würden wir aber die Polizei einkesseln und uns an ihre Fahrzeuge
ketten, wenn wir ihnen unseres Zeug aufzwingen würden, ohne physische
Gewalt, dann würde alles etwas anders aussehen. In Wirklichkeit sind
doch Schweizer Bullen nicht mit Massenaktionen vertraut und deshalb auch
schlecht vorbereitet auf solche Aktionen.

--- Das Problem innerhalb der Bewegung ist, dass wir immer wieder in
dieses medienkonforme Bild verfallen. Wir rekrutieren neue
Aktivist\*innen, indem wir ihnen zeigen, wie cool das ist. Mehr machen
wir nicht! Wir fressen Pizza und niemand wird sich dagegen wehren. Doch
wenn du anfängst Steine zu schmeissen, dann geniesst du keine
Unterstützung mehr.

--- Die Bewegung wird automatisch pazifistisch und medienwirksam. Dabei
macht uns die Polizei medienkonform.

--- Ich denke, dass ihr euch in einem Punkt irrt: Die meisten
XR-Aktivist\*innen vertreten nicht die Meinung, dass gewaltfreier
ziviler Ungehorsam dazu da ist, um Staatsgewalt sichtbar zu machen.

--- Doch das wäre eigentlich die Strategie von XR. Auf jeden Fall ist es
das Erste, was mir während des *talks* erzählt wurde.

--- Tja, wenn ich euch nicht getroffen hätte, dann hätte ich diese
Strategie wohl nie verstanden.

--- Innerhalb einer Bewegung gibt es sehr unterschiedliche Werdegänge,
geradezu in einer so grossen Bewegung wie es XR ist. Deshalb kommt es
auch darauf an, auf welche Gruppen mit welchen Affinitäten du triffst.

--- Das ist sogar visuell. Wir beispielsweise setzen uns jedes Mal hin.
Wenn wir stehen würden, wäre das ganz was anderes. Wir müssen ungehorsam
sein, gewaltlos *why not*, doch auf jeden Fall kraftvoll und stark.

--- Meistens sind XR-Aktionen so angedacht, dass sie als interessantes
Schaufenster dienen, um neue Aktivist\*innen zu rekrutieren. In der
Schweiz ist es sicherlich ein strategischer Fehler zu glauben, dass wir
eines Tages zu einer genügend grossen Masse heranwachsen werden, um
wirklich etwas zu bewegen. Hierzulande haben wir keine Vergangenheit von
sozialen Kämpfen wie in Frankreich oder Deutschland. Insofern müssen wir
anders denken.

--- Ich weiss, dass beispielsweise in Lausanne sich die ganze Bewegung
gespaltet hat, nur weil während einer XR-Aktion eine einzige Person ACAB
getagt hat. Ein ACAB-Tag und die ganze Bewegung fängt zu zittern an. Was
für eine solide Grundlage dieser Bewegung\...

(*kurzes Schweigen*)

## Kapitel 4 --- Wenn die Bobos den sozialen Frieden kaufen

--- Bald werden wir diese Diskussion beenden. Nachher haben wir noch eine
andere Sitzung.

--- Als ich politisch aktiv wurde, wäre ich froh gewesen, dass mensch
mich vorgewarnt hätte, dass ich die so oder so schon knappe Zeit, die
mir der Kapitalismus übrig lässt, mit Sitzungen verbringen werde.
(*Lachen*).

--- Über was wollen wir zum Schluss noch sprechen?

--- Vielleicht über das, was XR doch so super macht?

--- Könnten wir, ja. Aber das ist ja klar: seine Wirkungskraft, der Bezug
zu gewaltfreier direkter Aktion°, die Tatsache, dass viele Leute
zusammenkommen und vor allem, dass XR einen Rahmen für Personen bietet,
die zum ersten Mal politisch aktiv sind und nicht wissen, wie sie sich
ausserhalb der Sackgasse von Parteien und NGOs engagieren können.

--- Wir könnten aber auch davon sprechen, warum wir aufhören sollten, uns
wie ein Start-up zu benehmen, das mit Fahnen und seinen Logos sozusagen
*branding*[^14] betreibt?

--- Wir könnten uns dem "Danach" widmen, der Resilienz, und Ideen, anders
zu leben?

--- Das nennen wir bei XR "Kultur der Regeneration".

--- Oje.

--- Die Grundidee ist super: XR organisiert und schafft ein
Kräfteverhältnis, um souveräne, vom Staat emanzipierte
Bürger\*innenversammlungen ins Leben zu rufen. Inzwischen fördert und
schafft die Bewegung eine andere als die herrschende Kultur, eine die
wachstumskritisch, zweckentfremdet, empathisch, sozial und auf
gegenseitiger Hilfe beruht. Nach der Revolution löst sich XR auf. Was
bleibt sind die Bürger\*innenversammlungen und die Kultur der
Regeneration.

--- Auf dem Papier tönt das alles sehr gut.

--- Das Problem ist die Praxis.

--- Genau.

--- Was die Bewegung heute unter der Kultur der Regeneration versteht,
ist doch schlicht ein Witz.

--- Bei XR verwechseln wir wohlwollendes Verhalten bzw. gegenseitige
Hilfe mit Sentimentalität. Wohlwollen bedeutet auch Dinge dann zu sagen,
wenn es nötig ist und die eigenen Privilegien und Fähigkeiten
anzuerkennen, aber auch angriffig zu sein, ohne dass der Aktivismus als
gewalttätig erlebt wird. Wohlwollen bedeutet auch, Sexismus und
Rassismus anzuerkennen und gegen ihn anzukämpfen. Denn dieser wird
unweigerlich in allen Organisationen reproduziert und betrifft auch
Personen in unseren Kämpfen.

--- Heute gibt es sogar Leute, die von Wohlwollen gegenüber den
Chef\*innen und den Bullen sprechen.

--- Natürlich ist das nur eine Minderheit.

--- Ja klar, aber das ist doch symptomatisch für das Problem. Wenn ich
wütend auf das System bin, das alles Lebendige zerstört und ich Lust
habe, dieses zu attackieren, hat mensch mir schon geantwortet: "Du
solltest Hilfe in Anspruch nehmen, um die Wut in dir zu überwinden."
(*Lachen*)

--- Genau du hast doch mal gesagt, dass die Kultur der Regeneration bei
XR für den Moment etwas für neu-buddhistische Bobos ist\... Doch Yoga
vor den Sitzungen, das ist doch kaum auszuhalten

--- Was heute zur Kultur der Regeneration zählt sind: Sich besaufen,
meditieren und im Wald spazieren gehen. Da geht es aber sicher nicht um
Empathie. Einzig allein um Empathie gegenüber sich selbst.

--- Da ist doch genau das Problem all dieser schamanischen
Sich-Wohl-Fühl-Welt: Sie ist auf sich selbst fokussiert, aber nicht auf
die anderen. Ich kämpfe für das Gemeinwohl, nicht für meine Fresse. Vor
allem: Ich bin Weiss und habe einen Job, der von der Gesellschaft
wertgeschätzt wird. Ich brauche keinen Menschen, der sich um mich
kümmert. Ich brauche Menschen, welche jene Strukturen zerstören, die uns
daran hindern, uns kollektiv um einander zu kümmern.

--- Es ist doch einfach bescheuert! Denn die Kultur der Regeneration
sollte uns doch Energie für die Aktionen geben, d. h. sie sollte Raum
schaffen, um unserer Wut gemeinsam freien Lauf zu lassen. Nach unserem
Gespräch fühle ich mich so ruhig, regeneriert und voller Tatendrang wie
nach kaum einem XR-Event, wo es um Regeneration ging.

--- Wir brauchen eine Kultur, die Wut und Regeneration verbindet.

--- Wollen wir wirklich mit so einer hohlen Phrase aufhören?

--- Hat eine\*r etwas Besseres?

(*Schweigen*)

[^13]: Der Afrikanische Nationalkongress (auf engl. *African National Congress, ANC*) ist eine südafrikanische politische Partei, die 1912 gegründet wurde, um die Interessen der Schwarzen Mehrheit gegenüber der Weissen Minderheit zu verteidigen. Während der Apartheid wurde sie von der Nationalen Partei im Jahr 1960 als gesetzeswidrig deklariert, 1990 wurde sie wieder legal. Die USA deklarierte sie zwischen 1986 und 2008 als terroristische Organisation. Bei den ersten allen zugänglichen Wahlen, die 1994 auf der Grundlage des uneingeschränkten allgemeinen Wahlrechts stattfanden, kam die ANC an die Macht und der Parteipräsident Nelson Mandela wurde zum Präsidenten der Republik Südafrika (RSA) gewählt. Seither prägt die ANC das politische Leben in Südafrika.

[^14]: Marketingtechnik zur Förderung von Markenidentitäten und Unternehmen
