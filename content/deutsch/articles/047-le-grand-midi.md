---
author: "Anonym"
title: "Das grosse Mittagessen"
subtitle: "Während einer Woche unterwegs mit der Cantine autogérée, einer Volxküche (VoKü) in Lausanne"
info: "Im Juli 2020 für den Sammelband verfasst"
datepub: "Im Herbst 2021 auf Deutsch übersetzt"
categories: ["Selbstorganisation, kollektives Experimentieren", "Sabotage, direkte Aktion"]
tags: ["Geldstrafe", "Liebe, Verliebtheit", "Verhaftung", "Selbstkritik", "Bank", "Barrikade", "Fressen", "Kaffee", "kleben", "Wut", "Kompost, kompostieren", "fliessend", "kochen, kochen", "Zweifel", "Wasser", "Fehler", "Fussball", "Ameise", "Maul", "Öl", "Öl", "Garten", "Gerechtigkeit", "Logik", "Maschine", "Mail, E-mail", "mittwochs", "Werkzeug", "Angst", "Angst", "Pizza", "Preis", "Treffen", "Salat", "Speicherung", "Zucker", "Schweiss", "Gesicht", "Auto"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "47"
quote: "Hat mensch sich einmal mit dem Schmutz abgefunden"
---

## Montag & Dienstag

Montag und Dienstag ist bei uns eigentlich Ruhetag, doch das ist nicht
immer der Fall. Wenn anfangs Woche irgendwas los ist, dann ist es
meistens eine unvorhergesehene Lieferung; kleine chaotische Unfälle, die
uns zusätzliche Lebensmitteln ins Lager schwemmen. Zu jeder Tageszeit
erhalten wir Nachrichten: "Eh, ich habe zwei Kilo Zucchini, seid ihr
interessiert?" Und meistens sind wir interessiert. Fünfzig Personen
stehen in ständigem Kontakt miteinander. Unter ihnen findet sich immer
eine\*r, um diese zwei Kilo abzuholen und sie bis am Samstag sicher, in
der Frische, zu lagern. Je länger je mehr kommt das Essen aus ganz
verschiedenen Ecken: Weil wir es aber ablehnen, offiziellen, staatlichen
Sammelstellen beizutreten, braucht es viel Improvisationskunst. Denn,
was sich schleichend zeigt, ist, dass gewisse solidarische Initiativen
nicht auf der richtigen Seite der Barrikade stehen.

Einmal hat uns die *Brigade de solidarité populaire*, eine Aktionsgruppe
aus Genf, die sich gegen Faschismus und für Selbstverteidigung einsetzt,
absurde Mengen Schokoriegel geliefert. Sie waren in grossen Kartons
verpackt, keine Ahnung woher sie kamen, wir wollten es auch nicht
wissen. Wochenlang haben wir die Riegel auf der Strasse verteilt. Dann
gibt es auch antispeziesistische Tierheime, einige haben Abmachungen mit
lokalen Geschäften, um noch brauchbare Früchte und Gemüse gratis abholen
zu können. Sie bringen uns vorbei, was die nichtmenschlichen Tiere, die
dort wohnen, nicht essen. So haben wir gelernt, dass sie Brokkoli und
Zitrusfrüchte überhaupt nicht mögen, denn unsere Genoss\*innen bringen
kistenweise davon. Deshalb ist Brokkoli-Pesto zur hauseigenen Marke
geworden.

Vor allem sind wir mit viel Unvorhergesehenem konfrontiert: eine Schule,
die ihre industriellen Konfitüren und Getreide nicht los wurde; ein
Bauernhof in der Nähe, der 7 Kilo Tomaten wegschmeissen will; ein
Restaurant, das schliesst und seine Küchenausrüstung verschenkt; der
Bio-Laden ganz in der Nähe, der uns als Unterstützung Kaffee und Gewürze
offeriert; ein anderer Laden, etwas weiter entfernt, aber von einer
Genossin geführt, die an uns glaubt, sich ins Zeug legt und uns hilft,
Geld oder Bio-Mehl zu sammeln. Freund\*innen, die helfen Pfannen zu
sammeln\...

Einmal hat mensch uns angeboten, Restware aus einem Kino zu holen. Mit
einem Einkaufswagen voll mit cola und m&m's haben wir dann die ganze
Stadt durchquert. Wir hatten den Eindruck, als würden wir industriellen
Zucker verkaufen, direkt auf der Strasse. Das hat uns derart genervt,
dass wir uns nach einer Diskussion im Kollektiv schliesslich
entschieden, dies nicht mehr zu tun.

Dieses spontane Sammeln von Lebensmitteln war anfangs nicht einfach zu
organisieren. Es musste schnell gehen, wir mussten kurzerhand drei
Kühlschränke und eine Tiefkühltruhe besorgten, Flyer und Etiketten für
unsere Konserven druckten, Genoss\*innen zu Hilfe riefen, um unser Lokal
mit Strom zu versorgen, an Ofen und Kühlschränke herumtüftelten, eine
Ecke neu einrichteten, um nicht mehr alles auf den Tischen lagern zu
müssen.

### Wie bin ich in diesem Projekt gelandet? (1)

*Ich war erst seit ein paar Monaten wieder zurück in Lausanne. Ich    
war schon seit jeher aktiv im Bereich der Lebensmittelverarbeitung,   
der Landwirtschaft, auch in der Welt des Handwerks. Ich machte dies   
für mich, alleine, und das ergab für mich nicht viel Sinn. Bislang    
hatte ich keine Bezugsgruppen, mit denen ich Lust hatte, aktiv zu     
sein.*                                                                
                                                                      
*Im April assen wir dann mit zwei Freunden in Lausanne. Auch sie      
waren in verschiedenen Projekten und Aktivitäten engagiert. Wir       
sprachen über die Erfahrungen in VoKüs, die Verteilung von Gemüse und 
selbstgemachtem Brot, und der Frustration, dass es so etwas in        
Lausanne nicht geben würde. Da begann ich zu überlegen, wer motiviert 
sein könnte, um ein Kollektiv zu gründen, das Kochen und Kämpfe       
verbindet.*                                                           
                                                                      
*Zwei Tage später erhielt mein Freund ein SMS eines Kollegen, den er  
jahrelang nicht gesehen hatte. Eine selbstorganisierte Kantine würde  
sich im* Espace Autogéré *seit ein paar Wochen im Aufbau befinden und 
es würden noch Leute gesucht, die sich dem Projekt anschliessen       
wollen. Was für ein Zufall! Am darauffolgenden Samstag gingen wir     
dorthin, um uns ein Bild vor Ort zu machen. Seither kommen wir jeden  
Samstag.*                                                             

## Mittwoch

*Am 25. Juli 1943 erreicht die Nachricht, dass Mussolini festgenommen
worden war, das* Casa Cervi *in Emilia-Romagna, ein Ort, wo
antifaschistischer Widerstand geleistet wird. Alcide Cervi schildert im
Buch* I miei sette figli *die Freude, die an diesem Tag in allen Herzen
war. Er schildert, dass sein Sohn ihm sagte: "Papa, heute offerieren
wir der ganzen Region Pasta." So brachten sie Fässer voll mit Pasta auf
den Dorfplatz, um gemeinsam mit allen und für alle zu kochen. Es gab
380 Kilo Pasta. Was für eine Erfolgsgeschichte, ein hübscher
Antifa-Mythos:* La pastasciutta antifascista. *Wir unsererseits kleben
Etiketten mit der Aufschrift* Pasta antifascista *auf Tüten mit
frischer Pasta und stellen sie in den Gratis-Markt.*

Jeden Mittwoch produzieren wir unsere eigene frische Pasta. Ein Freund
hat eine qualitativ gute Maschine ergattert, die pro Stunde 6 Kilo Pasta
ausspuckt. Zwei oder drei Personen kümmern sich seither jede Woche
darum. Und jedes Mal ist es der Hammer! Wir empfangen die noch heisse
Pasta in unseren Händen wenn sie aus dem Maul dieser Maschine kommen,
zerschneiden und breiten sie dann auf Siebdruck-Trockengestellen aus.
Alle sagen, dass die Pasta schön und wirklich total lecker sei. Seit wir
VoKüs machen, machen wir Pasta. Ein Freund aus Italien hat ganz
besonders daran Gefallen gefunden. Die erste VoKü fand am 25. April
statt. In seinen Augen entspricht dies einem angemessenen Gedenken an
die Aufstände in Mailand, die zur Flucht der faschistischen Truppen am
25. April 1945 führten. Seitdem ist jener Tag in Italien als der "Tag
der Befreiung" bekannt.

### Wie bin ich in diesem Projekt gelandet? (2)

*Ich bin ursprünglich aus Italien und bin etwas zufällig in der       
Schweiz gelandet. Der Lockdown war sehr speziell für mich, die Zeit   
schien stillzustehen. Wie auch ich: Ich befand mich zwischen der      
Erleichterung, nicht bei mir in Italien zu sein, und der Angst,       
woanders, hier, stecken zu bleiben. Umso mehr, als dass ich den       
Eindruck hatte, und zwar seit Jahren, dass ich hier als Tourist sei   
und mich nicht wohlfühlen würde. Nun, ich wusste auch, dass ich mir   
meinen Weggang von meinen Freund\*innen in Italien nur verzeihen      
konnte, wenn ich hier eine Möglichkeit finden würde, in die Stadt, in 
der ich wohne und lebe, so richtig einzutauchen.*                     
                                                                      
*Als engagierter Sänger schloss ich mich dem anarchistischen Chor in  
Lausanne an. Über die Mailingliste des Chors las ich zum ersten Mal   
vom Projekt der selbstorganisierten VoKü: Notverteilung von           
Nahrungsmitteln. Dort suchten sie motivierte Hände, die beim Sammeln  
von Lebensmitteln mithelfen würden, oder aber Menschen mit ganz       
anderen Erfahrungen und Kompetenzen, die zur Unterstützung der*       
Cantine autogerée *beitragen könnten. Ich war planlos und ohne        
Erfahrung, doch es war die Gelegenheit, um mich lokal zu engagieren,  
dem durch die Pandemie verbreiteten Machtlosigkeitsgefühl kollektiv   
entgegenzutreten (ganz speziell vor dem Hintergrund, was in Italien   
abging), und so einen Beitrag zu leisten, um, dort wo ich lebe,       
Solidarität zu leben.*                                                

## Donnerstag

Der Donnerstag ist ein Fixtag für die *Cantine autogérée*. Jeden Samstag
planen wir die Rundgänge zum Sammeln der Lebensmittel für den
Donnerstagabend. In der Regel machen wir das zu zweit mit einem Auto.
Falls du in deinem Kaff noch nie eine Container-Tour gemacht hast, hier
eine Anleitung, wie du vorgehen musst:

Zuerst muss mensch die Lage vor Ort analysieren und die Zielscheiben,
die Supermärkte, ausfindig machen. Dank einem ersten Besuch können jene
Geschäfte bestimmt werden, die einen idealen Zugang haben, d. h. wo die
Abfallbehälter gut zugänglich sowie offen sind und die Nahrungsmittel
weder zerdrückt noch mit Javelwasser übergossen werden Wenn wir ein Auto
haben, ziehen wir oft jene Supermärkte vor, die sich in den
Industriezonen befinden. Die Abfallbehälter in den Zentren, die zu Fuss
und mit dem Velo zugänglich sind, werden nämlich meist schon geleert.
Zudem finden wir es fair, diese jenen Menschen zu überlassen, die in der
Stadt wohnen und kein Auto haben. In der Schweiz verdienen coop und
migros ein ganz besonderes Lob für ihre Sicherheitspolitik: Was mit den
Abfällen gemacht wird, geschieht hinter verschlossenen Türen --- und zwar
rigoros. Die Container sind meistens in doppelt verriegelten
Lagerhallen, oder direkt in Lastwagen, die den Abfall zur Mülldeponie
fahren, gelagert. Manchmal kann mensch in die Lastwagen gelangen, doch
die Bussen dafür können einem ganz schön weh tun. Hier wird nicht mit
abgelaufenen Joghurts und zerrissenen Bananenpackungen gespasst.

Ist die Liste mit den Supermärkten einmal gemacht, wird erkundet, wo die
Abfalleimer sind, wo mensch parkieren kann, ohne dass das Nummernschild
oder Gesichter auf den Überwachungskameras sichtbar sind. Wenn mensch
keine Spuren von seinem Besuch hinterlässt, gibt es kaum Gründe, warum
die Videoüberwachung von Supermärkten angeschaut wird. Ja, im
Kapitalismus ist auch dein Müll Privatbesitz, und wenn sich andere davon
bedienen, um sich zu ernähren, wird das als Diebstahl betrachtet. Einige
Lebensmittelläden ist dies ganz egal. Sie sind sehr offen für
"Diebstahl" dieser Art von Privatbesitz, der sie eher belastet. Am
besten macht mensch eine genaue Karte mit den Standorten der Kameras und
den Parkplätzen. Das scheint vielleicht auf den ersten Blick *too much*,
doch wenn regelmässig immer neue Personen sich darum kümmern, ist das
ganz praktisch! Damit sich alle wohl fühlen, empfehlen wir, freundliche
Läden zu bevorzugen. Dann muss mensch sich auch nicht *à la Black bloc*
anziehen, um ein paar Tomaten zu containern.

Auf einer Container-Tour soll jede\*r selber entscheiden, ob er sich
vermummen will oder nicht. Was aber ganz besonders empfehlenswert ist,
sind wasserdichte Schuhe, Kleider, die dir nicht so viel wert sind,
Latex- oder sogar Bauhandschuhe, eine Stirnlampe, Kisten und
Plastiksäcke. Und dann ab ins Planschbecken! Um für eine VoKü genug zu
haben, muss so viel wie möglich herausgeholt werden. Das bedeutet, dass
mensch manchmal bis zuunterst in die Müllcontainer tauchen muss.
Freund\*innen haben sich schon in wahre Müll-Speläolog\*innen
verwandelt, haben mehrere Schichten Plastik oder zerdrücktes Mus
durchforstet, um --- erst dann --- an Schätze zu gelangen. Manchen
Menschen fällt das schwer: Wir alle sind auf eine gewisse Abneigung
gegen alles konditioniert, was nicht kalibriert, desinfiziert,
standardisiert bzw. durchbohrt, asymmetrisch, fliessend oder zähflüssig
ist. Hat mensch sich einmal mit dem Schmutz abgefunden, wird das Sammeln
zu DER Aktivität an einem Donnerstagabend --- eine Art Schatzsuche, die
sich für Familien eignen würde. Es ist schwer, die kollektive und
triumphierende Freude auszudrücken, wenn mensch endlich Dutzende von
tadellosen Aprikosen entdeckt, die ihrem katastrophalen industriellen
Schicksal entrissen wurden, um zu Konfitüre verarbeitet zu werden.

Am Ende des Abends zieht mensch von dannen und versucht, dem Müllgestank
der zwanzig Kisten, die in den Kofferraum gepfercht sind, keine
Beachtung zu schenken. Wir fahren dann direkt in unser Lagerlokal, wo
uns Genoss\*innen bereits erwarten. Unter lauter Musik reinigen wir mit
Wasser und Natriumbikarbonat alles, was wir gesammelt haben und lagern
es. Wenn der Schatz im Licht angeschaut wird, stellen wir manchmal fest,
dass wir zu optimistisch waren. Der Fenchel, der sich mit der Stirnlampe
und durch die Handschuhe frisch und saftig anfühlte, ist in Wahrheit
halb verfault. Egal, auf den Kompost, das war je eh sein Schicksal. Der
industrielle Kapitalismus wollte nichts davon wissen, die Igel und
Ameisen werden sich freuen. Sie werden dann wiederum auf die Salate aus
dem Garten scheissen; wir hoffen, sie eines Tages in der VoKü servieren
zu können. Der Kreis schliesst sich! Mit den Lebensmitteln, die drei
Läden in einem Tag wegschmeissen, können ungefähr 100 Personen jeden
Samstag ernährt werden, zudem können sich rund zwanzig Personen im
Gratis-Markt bedienen.

### Wie bin ich in diesem Projekt gelandet? (3)

*Mein politisches Engagement hat in einer Student\*innen-Gewerkschaft 
begonnen. Ich fühlte mich in einem unterdrückenden Schulumfeld unwohl 
und trug viel Wut in mir. Die Gewerkschaftsbewegung hat sich für mich 
als Werkzeug erwiesen, um diese Wut in einen kollektiven Kampf        
umzuwandeln. Es lag für mich auf der Hand, mich dort zu engagieren.   
Ich tat es so intensiv, dass ich im Folgejahr ein Burn-out erlitt. So 
begann ich mein politisches Engagement infrage zu stellen, und        
merkte, dass es mir nicht das Erhoffte gab. Mit Genoss\*innen         
begannen wir deshalb von Genossenschaften zu träumen, die Land        
bebauen, ihr Brot machen und VoKüs organisieren. Wir haben uns viel   
dazu ausgetauscht, entstanden ist aber nichts, zu fest waren wir mit  
eigenen Konflikten beschäftigt und auf der Suche nach neuen Kräften.  
Eine grosse Enttäuschung. Ich schielte neugierig auf autonome         
Milieus, mit denen sich meine Gewerkschaftsgenoss\*innen überhaupt    
nicht identifizieren. Ich spürte, dass es Zeit war, etwas zu          
verändern in meinem Leben, neue Leute kennen zu lernen, neue Dinge in 
einer neuen Umgebung auszuprobieren. Genau dann fiel mir der Flyer    
der selbstorganisierten VoKü in die Hände.*                           
                                                                      
*Am nächsten Samstag bin ich, die Arme voll mit 5 kg Brot, das ich    
frühmorgens gebacken hatte, in der* Cantine autogérée *angekommen.    
Was folgte, war viel Liebe. Ich habe den Eindruck, eine grosse        
Familie gefunden zu haben, in der ich mich sehr wohl fühle. Wir       
lassen uns Zeit, bauen behutsam Beziehungen auf, welche die Basis für 
immer mehr kollektive Projekte schaffen. Seine Hände als politische   
Waffe zu benützen, bereitet einfach viel Freude. Und egal wer du bist 
und was du für eine Vergangenheit hast, deine Person wird nicht       
ständig gewertet, mensch kann kommen und sich selbst sein. Für mich   
ist es sehr aufbauend zu realisieren, dass das Essen nicht mehr eine  
leidende, persönliche Angelegenheit, sondern eine kollektive,         
solidarische und politische Freude ist. Das tut unheimlich gut!*      

## Freitag

Am Freitag ist eigentlich auch vorgesehen, dass wir uns erholen --- das
ist aber nicht immer der Fall. Manchmal wurden am Vorabend nicht
genügend Lebensmittel gesammelt und es braucht einen zweiten
Sammelrundgang. Wenn wir am Freitag etwas machen, dann geht es eher
darum, etwas langfristig zu organisieren. Denn beim Essen-Servieren am
Samstag knüpfen wir Kontakte. Essen verbindet, hilft, sich zu
organisieren, und kein Mensch kann sich vorstellen, dass eine
selbstverwaltete VoKü die Kämpfe um sie herum einfach so ignorieren
kann. Ein Projekt, das kollektiv getragen wird, ist jenes einer mobilen
VoKü. Natürlich braucht es Kohle, um so etwas aufzubauen. Und was Kohle
angeht, sind wir nicht sehr bewandert. Wir haben uns einmal bemüht,
einen Pizzatag zu organisieren. Ursprünglich wollten wir ihn "Pizzacab"
nennen, aber angesichts unserer eigenen grausamen Fantasielosigkeit
haben wir uns für "Pizz\@more" entschieden. Das ist besser, denn es gibt
nichts, das revolutionärer ist als die Liebe. Wir haben Backöfen, Teig
und Saucen organisiert. Die Bullen haben unsere riesigen Backöfen ganz
genau angeschaut und meinten: "Wem gehören denn diese brennenden,
riskanten Dinge?" Die Kochenden antworteten: "Das wissen wir nicht, wenn
ihr einen Gabelstapler habt, könnt ihr sie gerne woanders hinstellen,
bis dahin machen wir weiter Pizzas." Daraufhin haben sie sich verpisst.
Gut 100 Personen sind zum Essen gekommen, zudem verteilten wir auf der
Strasse rund 30 Calzone. Die VoKü fand zum zehnten Mal statt, ein
grosser Moment. Von allen Leuten der *Cantine autogérée* hat aber kein
Mensch daran gedacht, ein Kollekte°-Kässeli aufzustellen. Deshalb haben
wir nicht wirklich Geld gesammelt.

Während den grossen, antisexistischen Demonstrationen im Juni haben wir
einen Einkaufswagen so ausgestattet, dass wir mit ihm in der Demo
herumlaufen konnten. Mit unseren selbst gemachten, antifaschistischen,
violetten (!) Pasta, haben wir so etwas Geld gesammelt. Einmal, während
*Sainte-Sorcière*[^15], haben wir einen Pasta-Tag organisiert. Ein
anderes Mal haben wir, die *Cantine autogérée* und eine Gruppen von
Menschen, die sich gegen Repression von Aktivist\*innen einsetzt -
spontan eine Pizzeria auf der *Promenade de la Solitude* aufgemacht. Den
Event nannten wir "Contre la répression judiciaire, pizzas solidaires"
(dt.: Mit Pizzas solidarisch gegen Strafverfolgung). Und dabei ist ---
endlich einmal --- auch etwas Geld zusammengekommen.

Und wieder ein anderes Mal haben wir Falafel gemacht. Dabei ist uns kein
schlechteres Wortspiel in den Sinn gekommen als "Antifa-lafel". Wir
kochen für die Antirep-Solikassen, um zu helfen, Bussen von jenen zu
zahlen, die verlassene Häuser besetzt haben; für jene, die sich dem
Revolutionären Block angeschlossen haben und dabei riskieren, aus dem
Land oder ihrer Unterkunft ausgewiesen zu werden; für jene, die keine
Krankenversicherung haben. Natürlich kotzt es den Staat an, dass wir
viele Menschen umsonst ernähren, dass wir ihre Zustimmung nicht
brauchen, um zu existieren, um Nachschub zu bekommen und um öffentliche
Parks zu bespielen, ohne irgendwelche Normen oder Regeln, ausser denen,
die wir uns selber auferlegen.

Die Verwaltung legt uns so viele Steine in den Weg wie sie nur kann, für
den Moment beklagen wir uns aber nicht. Einmal war ein Auto eines
Vereins falsch parkiert während eine\*r drei Kisten Gemüse bei der
*Cantine autogérée* abholte, um sie am nächsten Tag an Personen zu
verteilen, die wegen Covid-19 ihren Job verloren hatten. Die
herumstreifenden Bullen ergriffen prompt die Gelegenheit, und sahen,
dass ein Autopneu eine weisse Linie überschritt und gaben uns eine Busse
über 200 Franken.

Wir wünschen uns, dass unsere mobile VoKü sehr agil ist, dass sie in nur
wenigen Minuten dort sein kann, wo besetzt, demonstriert, verhaftet
wird, vor dem Kommissariat, wo die Genoss\*innen festgehalten werden. Je
mehr die Leute zum Essen haben, desto länger bleiben sie. Mit einem
veganen Crêpes-Stand und zwei Lautsprechern verdoppelt sich die Anzahl
Personen, die zur Unterstützung vor einen Bullenposten kommen. Zudem
bleiben die Leute auch viel länger!

### Wie bin ich in diesem Projekt gelandet? (4)

*Auch wenn ich bereits an Demonstrationen und Vollversammlungen
teilgenommen habe, so wirklich politisch aktiv war ich noch nie. Eine
Zeit lang habe ich gewählt, dann nicht mehr, dann habe ich leere
Stimmzettel eingelegt, so richtig war ich aber nie irgendwo engagiert.
In der Schweiz habe ich nie ein Netzwerk gefunden, in dem ich mich wohl
fühlte. So investierte ich meine Energie anderswo und fragte mich, wo
die "anderen" wären, jene Menschen, mit denen ich mich wohlfühlen
würde.*

*Während des Lockdowns hatte ich das Verlangen, etwas Konkretes zu tun
als dem Geschehen von weitem zuzuschauen und alleine darüber
nachzudenken. Ich war wütend und empfindlich gegenüber all diesen
hinterhältigen Herrschaftsansprüchen, die so fürchterlich gewöhnlich
sind. Zudem habe ich letzthin damit begonnen, mich mit Anarchismus
auseinanderzusetzen. Diese Ideen haben mir eingeleuchtet. Und während
diese Ideen sich zu festigen begannen, bin ich auf die
selbstorganisierte VoKü aufmerksam geworden.*

*An einem Samstagmorgen, gegen 9 Uhr, ging ich dann ganz einfach
dorthin. Bislang kam es mir nicht in den Sinn, einen politischen Kampf
mit dem Zubereiten und Servieren von Essen zu führen. Erst im
Nachhinein, nachdem ich kochend und essend diese Art von Aktion mit dem
Kollektiv der* Cantine autogérée *entdeckt hatte, erschien es mir als
eine geniale und grundlegende Idee. Und obwohl das Sinn macht, ändert
es aber nichts an der Tatsache, dass sich nichts ändert, ausser den
Gesichtern der Narren auf den Geldscheinen. Trotz allem machen wir
weiter.*

## Samstag

Am Samstag ist dann endlich das grosse Mittagessen. Doch wie viele Leute
kommen da eigentlich? Das würde natürlich die Clowns in den neonblauen
Uniformen interessieren; sie, die das legitimierte Gewaltmonopol haben
--- sagen wir also: so zwischen einer Person und tausend. Dieser sehr
heterogene, mobile Verbund von Individuen trifft sich morgens, trinkt
einen Kaffee und holt die Kisten, in denen das am Donnerstag gesammelte
Essen aufbewahrt wurde. Wir machen so etwas wie einen Haufen, eine
Pyramide aus grünen Kisten, gefüllt mit hausgemachter Pasta, Früchten,
Gemüse und verpackten Produkten. Im Kollektiv erarbeiten wir dann ein
Menu. Was am Verderben ist, muss unbedingt verarbeitet werden. Die am
besten erhaltenen Lebensmittel kommen in den Gratis-Markt. Jeden
Samstagmorgen besprechen wir uns kurz und verteilen die Rollen: Es
braucht eine Kontaktperson für den Fall, dass wir von der Polizei Besuch
erhalten; eine Person, die den Gratis-Markt im Auge behält; eine Gruppe
von Personen, die sich bereit erklärt, das Essen auf der Strasse zu
verteilen; und eine Person, die im Notfall Zusatzeinkäufe tätigen könnte
(an gewisse Dinge wie vor allem Salz, Zucker und Öl kommen wir mit beim
Containern nur schwierig ran).

Dann fangen wir an zu schneiden. Natürlich könnten wir einen Arbeitsplan
erstellen, Gruppen bilden, Verantwortlichkeiten verteilen und die
verschiedenen Arbeiten zwischen den Kochherden, Schneidebrettern, dem
Rohen und Gekochten aufteilen. Alles ergibt sich aber auf eine sehr
organische Art und Weise. Bist du gut drauf? Geh an den Herd. Hast du
Lust zum Quatschen? Schnipsle auf dem grossen Tisch das Gemüse. Brauchst
du Ruhe? Putze die Kühlschränke und die Lagervorräte. Hast du Lust
draussen zu sein? Stelle die Tische auf oder leere den Kompost. Hast du
Lust zu faulenzen? Spiele Tischfussball, schaue, dass genügend Kaffee
für alle da ist, oder streiche die Nägel deiner Genoss\*innen.

Nach ein, zwei Stunden fangen wir draussen mit dem Aufbau der VoKü an,
sofern es denn schönes Wetter ist, ansonsten bleiben wir drinnen. Auf
der einen Seite des Gebäudes wird das warme Essen serviert, dort gibt es
genügend Stühle und Tische für 30-40 Personen. Auf der anderen Seite des
Gebäudes machen wir einen grossen Selbstbedienungsladen auf, ein
Gratis-Markt oder auf Kollektenbasis, offen für alle, mit Lebensmitteln
zum Selberkochen. Die Idee eines Gratis-Markts ist erst später
entstanden. Wir haben festgestellt, dass eine gemeinschaftliche,
Solidarität stiftende Initiative vielfältig sein muss: Es gibt Menschen,
die eine warme Mahlzeit wollen oder brauchen, weil sie nicht die
Möglichkeit haben, selbst zu kochen oder weil sie einfach Lust haben,
sich im Freien mit anderen auszutauschen. Andere wiederum haben Lust und
das Verlangen, für sich und ihre Angehörigen zu kochen; diese Menschen
kommen in den Gratis-Markt. Wir mögen ja nicht alle die gleichen Dinge,
kochen nicht auf die gleiche Art oder geben nicht die gleichen Kräuter
oder Gewürze in unsere Mahlzeiten. Zudem haben wir nicht alle die
gleichen Essgewohnheiten, und sind nicht alle gegen die gleichen
Lebensmittel allergisch oder intolerant.

Manchmal ziehen jene, die regelmässig in den Gratis-Markt kommen, gleich
wieder weiter, manchmal bleiben sie einen Moment und verweilen im Park
zum Essen. Wir laden immer alle dazu ein, am nächsten Samstag bereits am
Morgen zu kommen, um in der Küche und bei der Gestaltung des Menus
mitzuhelfen. Die Leute kommen öfters wieder. In den Sitzungen der
*Cantine autogérée* gibt es das gemeinsame Verlangen, einen Ort in der
Nachbarschaft zu schaffen, welcher anti-kapitalistische Werte vertritt.
Dort soll soziale Durchmischung gelebt werden und mensch soll
selbstkritisch bleiben und versuchen, keine unterdrückenden Mechanismen
zu reproduzieren. Das heisst nicht, dass uns dies gelingt, aber wir
versuchen es. Klar, es soll ein politisch engagierter Ort sein. Wir sind
uns bewusst: Die Sozialisierungen° und die Beziehungen, die mensch
pflegt, begünstigen manchmal das Unter-sich-sein-wollen. Doch das wird
auch immer wieder infrage gestellt.[^16]

Wenn es dann auf den Mittag zugeht, steht das Essen bereit: Pasta,
Tortillas, gemischte Salate, Couscous, angebratenes Gemüse,
Fruchtsalate, Smoothies, alles ist möglich. Manchmal bringen
Genoss\*innen sogar geklaute Trüffel-Öle, Morcheln oder andere teure
Sachen mit. Sachen, die aus dem Jenseits kommen.

Egal was für ein Menü wir kochen, bevor wir vor Ort servieren, bereiten
wir die Essensverteilung vor. Zwischen 30 und 40 Mahlzeiten werden auf
der Strasse verteilt: ein warmes Essen, Besteck und ein Fruchtsalat.
Diese Mahlzeiten sind für jene, die den Weg zur *Cantine autogérée*
nicht auf sich nehmen oder sich nicht mit anderen unterhalten wollen.
Die Stunden danach sind die schönsten: Wir essen alle gemeinsam. In
politisch aktiven Kreisen und politisch engagierten Vereinen hat die
Info von der VoKü die Runde gemacht; sie ist sogar bis zu einigen
staatlichen Sozialdiensten durchgedrungen.

Einmal hat uns eine Person, die regelmässig in den Gratis-Markt kommt,
mehrere Pfannen eines Gerichts mitgebracht, das sie bei sich zuhause
zubereitet hatte.\
Einmal hat eine Stammkundin des Gratis-Markts mit CBD "bezahlt".\
Einmal hat eine selbstständig arbeitende Schneiderin mit uns gegessen
und uns Hygiene-Masken mitgebracht, die sie von Hand für die *Cantine
autogérée* genäht hat. Umso besser, denn Masken und der Desinfektionsgel
sind teuer.\
Einmal ist eine international anerkannte Anarchistin vorbeigekommen. Wir
wollten ihr Spalierstehen. Sie sagte: "Ich habe euch Tupperware
mitgebracht. Gibt es Pasta?" Danach hat sie sich ins Gras gesetzt.\
Einmal kam eine Person von einem anderen Verein, um aus all unseren
Früchten Konfitüren und Sirupe zu machen; am nächsten Samstag stellten
wir sie in den Gratis-Markt.\
Einmal erzählte uns eine Aktivistin am Tisch, wie sie Evo Morales
kennengelernt hatte, als er noch ein junger indigener Aktivist war, und
bevor er bolivianischer Präsident wurde und den USA die Stirn bot.\
Einmal bemerkten wir, dass eine Person Geld aus der Kasse des
Gratis-Markts genommen hatte. Wir haben dann lange diskutiert, ob die
Kasse auch zum Selbstbedienungsladen gehören würde. Geld zu einem freien
Preis anzubieten, ist doch ein gutes Konzept!\
Einmal erfuhren wir, dass die Geflüchteten im Quartier nur wenig
Hygiene-Produkte zur Verfügung hatte. So haben wir einen Workshop
organisiert, um Waschmittel herzustellen.\
Einmal hat eine Person im Park spontan einen Flohmarkt organisiert.\
Einmal haben uns Freund\*innen eines besetzten Hauses 200 Schoggihasen
an je 500 Gramm vorbeigebracht. 100 Kilo Schokolade. Wir haben sie
verteilt, gegessen, geschmolzen und sie x-mal während Wochen von einem
ins nächste Lager deplatziert.\
Einmal, also eigentlich mehrmals, haben uns Nachbar\*innen ihre
Nahrungsmittel vorbeigebracht, die bei ihrem Umzug übrigblieben.\
Einmal hat uns ein italienischer Genosse erklärt, dass wir alle grünen
Salate auch in einer Pfanne anbraten könnten, mit Knoblauch. Erst hat
sich die Mehrheit ein wenig lustig darüber gemacht, aber dann hat die
Mehrheit den gekochten Salat probiert. Seither servieren wir fast jede
Woche davon.\
Einmal hat ein Genosse versehentlich den Biervorrat, den wir uns für die
Sitzung am Nachmittag reservieren, in den Gratis-Markt gestellt. Wären
wir für eine Strafjustiz, hätte er einen üblen Tag verbracht. Doch wir
scheissen drauf. An der Sitzung tranken wir Fruchtsäfte. Seither trinken
wir übrigens kein Bier mehr während der Sitzungen.\
Einmal, also eigentlich mehrmals, haben die Bullen ihren Wagen unweit
von uns parkiert. Sie tun das, um den Menschen, die zum Essen kommen,
Angst zu machen. Die Polizei will, dass die *Cantine autogérée* nicht
ein sicherer Ort ist für Leute, für die eine Identitätskontrolle ein
Sicherheitsrisiko darstellt.[^17] Die Strategie ist klar: Angst
verbreiten und einen Graben zwischen die Menschen ziehen. Je mehr die
Masse zusammen isst, desto mehr wird diskutiert und desto mehr
organisiert sich mensch, und das ist nicht gut für alle. Wenn es heiss
ist und wir draussen essen, dann können wir nicht viel gegen die
Überwachung machen. Ist es dagegen kalt, und wir essen drinnen, dann
sind wir unbesorgt.\
Jedes Mal, wenn das Material versorgt ist, kommen wir zusammen: es ist
das Treffen der *Cantine autogérée*. Wir besprechen nach, planen die
nächste Woche, zählen das Geld und schlagen Projekte für die Zukunft
vor.\
Jedes Mal *werden* die Anwesenden zur *Cantine autogérée* und verbessern
sie fortlaufend, gemeinsam und auf einfache Art und Weise.\
Oft fällt es uns am Samstagabend schwer, uns zu verabschieden, wenn wir
verschwitzt und fröhlich sind. Wir bleiben da und machen Konserven oder
Konfitüren, waschen mehr als nötig ab, um zusammenbleiben zu können,
machen gemeinsam Nickerchen, Tischfussball-Turniere, Zirkus- oder
Kosmetikateliers oder saufen ganz einfach.

## Sonntag

Das Essen, das vom Gratis-Markt übriggeblieben ist, wird von einem
Verein abgeholt, der jeweils sonntags eine Essensverteilung in der
Nachbarstadt organisiert. Von den Früchten und dem Gemüse, das die
Gesellschaft für den Müll reserviert hat, werfen wir eigentlich nichts
weg.

Wenn bei uns etwas am Sonntag läuft, dann ist es oft eine Demo, eine
Blockade oder eine andere militante Aktion. Es kommt vor, dass wir am
Samstag bis spät bleiben, oder wir uns früh am Sonntagmorgen wieder
treffen, um Essen vorzubereiten. Wenn wir wissen, dass ein
revolutionärer Block° sich irgendwo vorbereitet, dann bringen wir den
Leuten manchmal das *Zvieri*. Es lässt einen nicht kalt, zu sehen, wie
eine Truppe die Sturmmasken auszieht, sich nach einer Aktion erholt und
kiloweise Bananen oder Schoggi geradezu verschlingt. Und wenn nach einer
unbewilligten Demo, die ausgeartet ist, Genoss\*innen auf dem
Polizeiposten landeten, dann tauchen wir eben später auf, um das Dessert
zu servieren. Wir haben in Delsberg gestrichene Brötchen serviert, um
die *Cantine* zu unterstützen (es handelte sich um ein
selbstorganisiertes Zentrum, dass leider beim Erscheinen dieses Textes
nicht mehr existiert). Wir möchten VoKüs organisieren, bis alle
Herrschaftssysteme definitiv zusammenbrechen. Wir hoffen, wir werden
genügend Leute sein.

Wir hoffen, dass immer neue Menschen zu uns stossen.

Wir hoffen, dass wir eines Tages überall sein werden.

## Rezepte

### Brokkoli-Pesto

Zutaten: Brokkoli, viel Knoblauch, Mandeln, Pfeffer, Salz

Brokkoli gut waschen. An den Brokkoli riechen, um zu überprüfen, dass
sich kein Müllsaft im dichten Grün verloren hat.

Die Brokkoli in grosszügige Stücke schneiden und diese im gesalzenen
Wasser kochen.\
Am besten nur so lange, bis sie *al dente* sind. Danach alle Zutaten in
eine grosse Schüssel geben, und das Ganze mixen bis es jene Konsistenz
hat, die euch gefällt. Nach Belieben salzen und pfeffern!

### Gebratener Salat

Am besten eignet sich ein alter, zerfledderter Salat und Paniermehl aus
hartem Brot.

Zutaten: Salat, Olivenöl, Knoblauch, Zwiebeln, Pfeffer, Salz; Tipp:
Paniermehl.

Gut waschen. An den Salaten riechen, um zu überprüfen, dass sich kein
Müllsaft zwischen den Blättern verloren hat. Den Salat sehr grosszügig
schneiden und ihn gut waschen.

Den Knoblauch hacken und die Zwiebeln in Lamellen, Scheiben, dünne
Streifen schneiden; so wie es euch eben gefällt.

Knoblauch und Zwiebeln anbraten und dann den Salat dazugeben. Es braucht
grosse Behälter: Am Anfang hatten wir das Gefühl, dass wir Salat für
300 Personen zubereiten würden, das Volumen verringert sich dann aber
beim Kochen ganz von allein (wie bei Spinat).

Warten bis das Wasser, das der Salat abgibt, sich etwas reduziert hat,
um dann das Paniermehl am Ende der Garzeit hinzuzufügen.

### Kompost-Bomben (zum Werfen auf Bullen)[^18]

Aus Brennnessel- oder Schneckenjauche entstehen die besten Bomben. Das
stinkt wie die Pest!

[^15]: Die *Cantine autogérée* hat den 15. August (eigentlich Maria Himmelfahrt) in *Sainte-Sorcière* (dt.: die heilige Hexe) umgetauft.

[^16]: ... eine manchmal schwierige Fragestellung; lies den Text *Ganz so revolutionär sollte unsere Revolution doch nicht aussehen* \[n. 44\].

[^17]: Der Text *They don't see us* \[n. 4\] erzählt von der Verbissenheit der Polizei, Sans-Papiers zu kontrollieren.

[^18]: Um ein anderes Anti-Bullen-Geschoss kennen zu lernen, lies den Text *Drohnen* \[n. 1\].
