---
author: "Emmathegreat"
title: "Drop gun & pick coal"
subtitle: "Gedicht"
info: "Für den französischen Sammelband am 18. Januar 2021 verfasst"
datepub: "Im Herbst 2021 auf Deutsch übersetzt"
categories: ["Antirassismus", "Selbstreflexion zum Aktivismus", "Migration und Kampf"]
tags: ["Mut", "Feuer", "Macht", "Blut", "Gewalt"]
langtxt: ["de", "fr"]
zone: "Suisse romande"
num: "9"
quote: "Legt die Waffen nieder und sammelt Kohle"
---

Drop guns and pick coal. Stop shooting, but save soul.

I'm not stronger, but I've just been bold to regain my mentality that
has been sold.

And to share some piece of it with you, maybe together we can grow old.

Because you all know we've lost so many young souls, because they chose
colors, guns and not coal.

I've been to places where they have different races. Where I never
heard a gunshot in nine years.

Which got me thinking about my place, where I see blood so many times in
one year.

All by violence.

Many by gunshot.

I wish I had some other ways to reach you. I really wish I had some
other ways to show you.

I imagine your feet, fitting in my shoes, they can't.

So all I can do is to tell you my shoe size, maybe we can have a year
with few suicide.

By violence or by gunshot, at least a few lost.

“Black lives matter”, I don't know if that includes us. Or should we
say “African lives matter”, just to gain trust.

I think I'm strong enough to carry more than my own cross. You speak
for yourself, they speak for themselves, I speak for us.

So I say “Drop guns and pick coal. Stop shooting, but save soul.”

## Legt die Waffen nieder und sammelt Kohle

Legt die Waffen nieder und sammelt Kohle. Nicht schiessen, sondern
Seelen retten.

Ich bin nicht stärker, aber ich hatte den Mut, meine verkaufte Seele
zurückzuerobern.

Einen Teil davon teile ich mit dir, vielleicht können wir gemeinsam alt
werden.

Wir wissen alle, dass wir viele junge Seelen verloren haben, weil sie
sich für eine Gang, für Waffen und nicht für Kohle entschieden haben.

Ich war schon an Orten, wo es verschiedene Rassen gibt. In neun Jahren
habe ich nie einen Schuss gehört.

Das erinnert mich an meine Heimat, wo ich so oft im Jahr Blut sehe.

Immer wegen Gewalt.

Oft wegen Waffen.

Ich wünsche mir, ich könnte euch anders erreichen. Ich wünsche mir ganz
fest, ich könnte es euch anders zeigen.

Ich stelle mir vor, wie ihr in meinen Schuhen seid --- ohne reinzupassen.

Alles, was ich tun kann, ist, euch meine Schuhgrösse mitzuteilen;
vielleicht können wir ein Jahr mit weniger Selbstmorden erleben.

Wegen Gewalt oder Kugeln, zumindest einige gehen verloren.

"Black lives matter", ich weiss nicht, ob wir da mitgemeint sind. Oder
sollten wir sagen: "African lives matter", nur um Vertrauen zu gewinnen.

Ich denke, ich bin stark genug, um mehr als nur mein eigenes Kreuz zu
tragen. Du sprichst für dich selbst, sie sprechen für sich selbst, ich
spreche für uns.

Und sage: "Legt die Waffen nieder und sammelt Kohle. Nicht schiessen,
sondern Seelen retten."
