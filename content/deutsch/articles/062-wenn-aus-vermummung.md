---
author: "Kollektiv *NO WEF Winterquartier*"
title: "Wenn aus Vermummung Maskenplicht wird"
subtitle: "Frühlingsquartier 2021"
info: "Für den deutschen Sammelband verfasst"
datepub: "Im November 2021 verfasst"
categories: ["Selbstorganisation, kollektives Experimentieren", "Selbstreflexion zum Aktivismus"]
tags: [""]
langtxt: ["de"]
zone: "Deutschschweiz"
num: "62"
quote: "Es ist der Kampf gegen den Kapitalismus."
---

Seit einigen Jahren ist das *NO WEF Winterquartier* fester Bestandteil
der Berner Mobilisierung gegen das world economic forum (*wef*). Jedes
Jahr finden sich zu dem Anlass diverse Aktivist\*innen aktueller Kämpfe
und lokaler Projekte sowie Expert\*innen politischer Themen in der
Berner Reitschule ein. Mit Vorträgen, Filmen und Ausstellungen wird der
Widerstand gegen das herrschende System ein Wochenende lang sichtbar
gemacht und diskutiert. Das *wef* und die Akteure, die es anzieht, sind
der ideale Anlass für eine breite Gegenmobilisierung. Da von
Vertreter\*innen von Nationalstaaten bis zu Grosskapitalist\*innen alle
im Januar in Davos zusammenkommen, sind für kurze Zeit die direkten
Feinde der meisten Widerstandsbewegungen an einem Ort. Aus diesem Grund
sehen wir die NO WEF Mobilisierung in einer wichtigen Rolle, denn an ihr
lässt sich aufzeigen, was all die globalen und lokalen Protestbewegungen
verbindet: Es ist der Kampf gegen den Kapitalismus.

Auch im Winter 2020 hatte sich darum wieder eine grössere Gruppe
zusammengefunden, um für das Jahr 2021 das Winterquartier in Bern zu
planen. Diese grössere Gruppe ist, wie es oft läuft, mit der Zeit
zusammengeschrumpft. Die verbleibenden drei Frauen mussten angesichts
der Pandemielage flexibel bleiben, sowohl aufgrund der Verschiebung des
*wef* an sich als auch wegen Unsicherheiten bezüglich Einschränkungen
von Veranstaltungen. So wurde das geplante Winterquartier zum
Frühlingsquartier. Da sich die Situation in Bezug auf Covid nur schlecht
einschätzen liess, einigten wir drei uns darauf, internationale Kämpfe
in der Form von Interviews und Videos zu porträtieren und lokale Kämpfe
in Form von Online-Veranstaltungen vorzustellen. Die Unsicherheit, ob es
für internationale Gäst\*innen möglich sein würde, einzureisen, war zu
gross.

Ähnlich gross war der Aufwand, die Covid-bedingten Hürden zu meistern ---
der Ertrag oder das Interesse der Leute in Bern schwindend klein. Wohl
aus drei Gründen: Erstens ist die linksradikale politische Landschaft
spätestens seit der Pandemie karg und statisch, zweitens war nach einem
Jahr Home-Office die Motivation für weitere Online-Veranstaltungen
aufgebraucht und drittens war am betreffenden Wochenende auch noch
wunderschönes Wetter. Dies hätte dann aber zur Ausstellung laden können,
die draussen stattfand. An Stehtafeln konnten QR-Codes eingelesen
werden, die zu Interviews und Videos führten. So konnte mensch die
Stimmen von Aktivist\*innen aus verschiedenen Ländern auf dem eigenen
Smartphone anhören. Die Ausstellung führte nach Südamerika, konkret nach
Kolumbien und Ecuador, wo Menschen unentwegt und trotz der Pandemie
gegen neoliberale Regimes kämpfen. Der Beitrag von *Medina*[^1] führte
die Besucher\*innen auf die wenige hundert Meter entfernte
Schützenmatte, wo die während dem Notstand so oft beschworene
Solidarität praktisch gelebt wurde --- unter anderem durch eine warme
Mahlzeitenabgabe. Sie führte zu unseren Genoss\*innen in Lyon, die neben
konstanter Antifa-Arbeit während der Pandemie auch zunehmend
Nachbarschaftshilfe betrieben. Ein Beitrag nahm die Besucher\*innen nach
Belarus mit, wo die Pandemie im Gegensatz zu vielen anderen Ländern die
linksradikale Bewegung nicht blockierte, sondern auftrieb. Und sie
begleitete Menschen in Bosnien und Griechenland, wo das Leben der
Geflüchteten und die Arbeit derer, die sie unterstützen, durch die
Pandemie zusätzlich erschwert wurde.

Doch auch diese vielfältigen und beeindruckenden Beiträge haben kaum
Leute angezogen. Für uns als organisierende Gruppe war dies sicher eine
Enttäuschung. Doch der Kontakt zu den verschiedenen Gruppen, die zu
Interviews, Videos und Onlineveranstaltungen beigetragen hatten, war
sehr inspirierend. Es konnte festgestellt werden, dass viele Gruppen
während der Coronakrise von ihrem eigentlichen Fokus wegkamen und sich
der Situation in ihren Städten oder Quartieren anpassten. Es gab
Nachbarschaftshilfen, Radioprojekte oder andere einfallsreiche
Gruppenaktivitäten. Es war schön zu sehen, dass nicht alle in einer
Schockstarre verharrten und auf bessere, geimpfte und
zertifikatspflichtige Zeiten warteten, sondern stattdessen kreativ
wurden.

Auch die Online-Vorträge von lokalen Gruppen wie *500k.ch/Basel
Nazifrei*[^2]*, Seebrücke Bern* und *8. März Unite* zeigten diverse
Ansätze, wie während der Pandemie weiterhin revolutionäre Arbeit
geleistet werden kann --- einfach auf andere Art und Weise: Aus
Solikonzerten wurde eine Onlinekampagne, aus Vermummung Maskenpflicht.
Umso grösser war unser Frust darüber, dass nur wenige Menschen mit
diesen wertvollen Diskussionsbeiträgen erreicht wurden.

So verschieden die Kämpfe waren, so einte alle eines: der Wunsch sich
auszutauschen, Kämpfe zu verbinden, voneinander zu lernen und
Solidarität zu leben. Es war erstaunlich, wie sehr sich die Gruppen
gefreut haben, dass sich wer für sie, für ihren Alltag und ihre Kämpfe
interessierte. Es gab nicht viel, was wir in der Situation machen
konnten, aber es war wichtig, unseren Genoss\*innen rund um die Welt
zuzuhören. Es ist klar, dass die Pandemie dazu führte, dass laufende
Kämpfe vergessen wurden und sich die einzelnen Bewegungen um ihre lokale
Situation kümmern mussten. Weil der Kontakt und der Austausch erschwert
wurde, waren lokale Gruppen auf sich allein gestellt. Alle waren
gezwungen, ihre eigenen Lehren aus der Pandemie zu ziehen. Teilweise
schien es fast so, als würde die Welt stillstehen. Doch wenn mensch sich
die Mühe machte und hinsah, gab es so viele Leute, die sich um andere
Menschen kümmerten, die sich zu nachbarschaftlicher Hilfe
zusammenschlossen, die sich weiterhin gegen Ungleichheit zur Wehr
setzen. Für die linke Bewegung ist das unserer Ansicht nach zentral.
Denn wenn wir hinschauen, wie unsere Genoss\*innen in anderen Ländern
ähnliche Probleme lösen, können wir daraus etwas für unsere Praxis
lernen und unsere Methoden den Umständen anpassen. Das wäre der
technische Aspekt. Genauso wichtig ist aber die emotionale Komponente:
Isolation macht hoffnungslos. Nur durch Austausch mit anderen Gruppen
können wir spüren, dass wir nicht alleine sind im Kampf für eine bessere
Welt. Der Kontakt mit unseren Genoss\*innen ermöglicht es uns, trotz
ständiger Repression und Ausbeutung den Blick für die Revolution nicht
zu verlieren.

Für uns ist klar, dass es im Januar 2022 wieder ein Winterquartier geben
wird. Weil wir durch unsere Arbeit und durch die Vernetzung mit den
verschiedenen Gruppen sehen, wie wichtig es ist, dass wir von anderen
lernen können, um das eine oder andere auch in die eigenen Kämpfe zu
integrieren. Die Motivation und der Kampfgeist sollen bis zu uns kommen
und von uns wieder zurück, damit die bestehenden Kämpfe dieser Welt noch
stärker werden. Die beteiligten Menschen sollen spüren, dass sich
immerhin drei Frauen für sie, ihren Kampf und ihren Alltag
interessieren. *La lucha continua*!

[^1]: Der Text *Für eine Welt, in die viele Welten passen* \[n°63\] gibt
    noch mehr Einblicke in das Projekt *Medina*.

[^2]: Was dort genau ab- und schiefging beleuchtet der Text *Wir sind
    alle antifaschistisch* \[n°65\]. Der Text *Solidarität, Solidarität,
    Solidarität* \[n°66\] gibt Einblicke in Theorie und Praxis eines
    politischen Prozesses.
