---
author: "Loïc Valley"
title: "Humus für die Blumen der Revolution"
subtitle: "Nicht-binäre Wut"
info: "Im Juni 2020 für den französischen Sammelband verfasst"
datepub: "Im Herbst 2020 auf Deutsch übersetzt"
categories: ["Feminismus, Genderfragen", "Trans-Schwul-Lesbisch, Queer", "Selbstreflexion zum Aktivismus"]
tags: ["Liebe, Verliebtheit", "Wut", "Straftat", "zerstören", "Maul", "Scheisse", "Mutter", "Nacht, Dunkelheit", "lachen, trinken", "Strasse", "Fabrik", "Stimme"]
langtxt: ["de", "fr"]
zone: "Suisse romande"
num: "52"
quote: "aber lassen wir unsere Wut nicht verstummen"
---

{{%epigraphe%}}
Die Revolution (ihre, unsere) verlangt von uns, immer, mitten in der
Nacht aufzuwachen: Das Bewusstsein muss gerade dann aktiviert werden,
wenn es ausgeschaltet werden sollte.

--- Paul B. Preciado
{{%/epigraphe%}}

Ich bin Schweizer. Ich bin hier geboren, meine schulische Bildung habe
ich in der Schweiz genossen. Ich wurde dazu gezwungen, dem Land zu
dienen, in dem ich geboren wurde. Mir wurde die vermeintliche
Grossartigkeit der Schweizer Institutionen eingetrichtert, die ach so
perfekte Demokratie, die legendäre Neutralität, welche die Schweiz
während all den Weltkonflikten seit ihrer Gründung zu bewahren wusste.
Ich wurde mit der sterilen Ruhe eines Landes gefüttert, dessen grösster
Fehler, den mensch sich überhaupt vorstellen kann, jener ist, dass
mensch zu laut spricht. "Verstehen Sie, das wäre unhöflich\..."

So hat mir die Schweiz während Lehrjahren dumpfer Nachahmung ganz
selbstverständlich beigebracht, dass Schwule, Lesben und alle anderen,
die sich nicht für Heterosexualität einsetzen, innerhalb der
Landesgrenzen ganz rein gar nichts zu heiraten hätten. Sie hat mir auch
beigebracht, dass Homophobie (geschweige denn Transphobie) kein
Verbrechen sei, dass ich aufgrund meiner Anatomie nicht vergewaltigt
werden könnte und vor allem, dass sich dies nicht so schnell ändern
würde. "Verstehen Sie, alles braucht seine Zeit..."

So merkte ich beiläufig zwischen zwei Schuljahren, dass Heterosexualität
mich genauso langweilte wie Seilbahnen, und ich beim Wort
Nicht-Binarität weinen musste, weil ich den Eindruck hatte, dass es für
mich erfunden wurde. Doch erstaunt das, wenn dein\*e
möchtegern-subversive\*r Lehrer\*in den Schulplan buchstabengetreu
umsetzt, dass mensch ihn heimlich "11 Jahre Klappe halten zur
biopolitischen Zukunft der Mutter Schweiz" nennen könnte?

Und jetzt?

Was machen wir?

Ich stelle die Frage noch einmal, weil sie grundlegend ist. Was machen
wir, wenn wir uns in der urbanen und materiellen Vervielfältigung der
Definition der "Ruhe" befinden und merken, dass unsere Identität diese
Ruhe bedroht?

Ich kann euch versichern, die meisten queeren° Menschen machen im Alter
von 15 Jahren genau eines: Sie unternehmen alles, um so zu sein wie die
anderen. Sie machen diskriminierende Witze über sich selbst. Denn das
bringt die anderen Menschen zum Lachen, sie finden uns "total
selbstironisch".

Mensch spielt die Tucke, weil mensch so genannt wird. Mensch äppelt
den\*die Warmduscher\*in nach, weil gesagt wird, mensch sei so eine\*r.
In der Strasse knackt mensch mit dem Handgelenk, damit die Heteros ganz
sicher kapieren, dass wir nicht auf ihrer Seite sind, aber dass wir uns
gleichzeitig anpassen und motiviert sind, damit alles normal
funktioniert. "Sie verstehen doch, *Herr* Valley, Motivation ist etwas
wichtiges."

Ich schwöre euch, wenn du jahrelang dem Establishment und dem System in
den Arsch kriechst und merkst, dass sie nicht wollen, dass du
existierst, dann politisiert dich das automatisch.

Dann macht es Klick.

Und das prägt.

So habe ich meine beste Mitstreiterin, aber auch meine ärgste Gegnerin
kennengelernt. Mit ihr hatte ich eine Liebesgeschichte --- eine von
jenen, die ganz intensiv sind und dich weit tragen. Ich brauchte lange,
bis ich verstand, dass sie mir nur Gutes wollte und sie mir dies zu
verstehen gab, wie sie eben konnte, ganz plötzlich, aber ohne etwas
durchgehen zu lassen. Ich merkte, dass sie immer für mich da war,
jederzeit, überall, egal in welchem Kontext. Und immer wenn sie mich
wieder aufbaute, zauberte sie mir auch ein Lächeln aufs Gesicht.

So habe ich meine Wut kennen gelernt.

Das brauchte Zeit, weil der Wut gelernt wurde, nicht zu existieren, sich
in eine Ecke zu stellen. Doch die Schweiz hat Scheisse gebaut. Es ist
nicht ungewöhnlich, dass die Schweiz Scheisse baut. Doch hier ging
eindeutig etwas schief. Sie hat die Fabrik ihrer eigenen Zerstörung
gebaut. Lebewesen, die sie nicht liebt, nur toleriert. Deren Wut will
sie mit Ideen, eine raffinierter als die andere, zum Schweigen bringen
und merkt dabei nicht, dass sie Vulkane formt, die eines Tages --- mit
Verspätung --- ausbrechen werden.

Die Schweiz "erschafft" Kinder, welche die Grabstätte der Schweiz
schaufeln. Die Schweiz ist eine heroische, griechische Tragödie: Sie
preist den Olymp, damit wir sie nicht fallen lassen, um sich dann
bewusst zu werden, dass sie ein verlogenes, monströses Wesen ist und sie
in einer Welt, die *sich entwickeln* will, nicht leben kann, sondern
aufwachen muss, um nicht zu sterben.

{{%ligneblanche%}}

Schweiz, Mutter meiner Wut. Mutter der Wut der nicht-binären Queer
Trans\* Schwulen Dicken Hyperaktiven und Hochbegabten Person, die ich
bin.

{{%ligneblanche%}}

Schweiz, Mutter meiner Wut. Mutter der Wut der nicht-binären Queer
Trans\* Schwulen Dicken Hyperaktiven und Hochbegabten Person, die ich
bin.

{{%ligneblanche%}}

Wie in einer Tragödie muss es x-mal wiederholt werden, um den Olymp
vorzuwarnen, von dem, das kommt, und damit die Zuschauer\*innen wissen,
dass im letzten Akt ganz viel Blut spritzen wird.

{{%ligneblanche%}}

Schweiz, Mutter meiner Wut. Mutter der Wut der nicht-binären Queer
Trans\* Schwulen Dicken Hyperaktiven und Hochbegabten Person, die ich
bin.

{{%ligneblanche%}}

Wie ein Ritual, dass unendlich lange wiederholt wird.

Schweiz, Mutter meiner Wut. Mutter der Wut der nicht-binären Queer
Trans\* Schwulen Dicken Hyperaktiven und Hochbegabten Person, die ich
bin.

{{%ligneblanche%}}

Ist dies getan, erinnern wir uns, dass die Wut uns nur Gutes will. Dass
sie nur jenen Personen und Institutionen weh tun will, die uns weh tun.

{{%ligneblanche%}}

Fördern wir unsere Wut.

Geben wir ihr eine Form, damit sie stark wird.

Versuchen wir ihren Nutzen zu verstehen,

ihre Notwendigkeit zu akzeptieren.

Tolerieren wir nichts. Nie und nimmer.

Tolerieren wir nur dann, wenn wir uns selbst schützen wollen.

Aber lassen wir unsere Wut nicht verstummen.

Kämpfen.

Sich vernetzen, gemeinsam organisieren.

Gegen alles Empörende revoltieren.

Seien wir radikal.

Machen wir keine Kompromisse.

Sich vernetzen, gemeinsam organisieren.

Schreien wir in die Nacht hinaus.

So laut, dass am Tag, wenn wir wieder wach sind, die Echos unserer
Stimmen noch immer zu hören sind.

Wenden wir uns dem Anarchismus zu, und fragen wir uns, wie wir ihn
anwenden können.

Seien wir sanft und freundlich mit uns selbst.

Vergessen wir nicht, dass wir viele (und nicht alleine) sind und wir
immer "mehr" werden.

Halten wir die Augen offen. *Wir Sind Überall*.

{{%ligneblanche%}}

Und verwandeln wir unsere Wut in den Humus für die Blumen der
Revolution.

