---
author: "Anonym"
title: "Kill the hippie in your head"
subtitle: "Wie ich dem Pazifismus den Rücken kehrte"
info: "Im September 2020 für den Sammelband verfasst"
datepub: "Im Herbst 2021 auf Deutsch übersetzt"
categories: ["Demonstration, Riot", "Selbstreflexion zum Aktivismus"]
tags: ["Adrenalin", "Wut", "Dissonanz", "Feuer", "Maul", "Horde", "Ungerechtigkeit", "Scheisse", "Angst", "Macht", "Wut", "lachen, trinken", "Sonne", "Theorie", "Gewalt", "Gesicht", "Stimme"]
langtxt: ["de", "fr", "it", "en"]
zone: "Suisse romande"
num: "48"
quote: "Blumen in die Kanonen"
---

{{%epigraphe%}}
Hippie: junge\*r Anhänger\*in,
der\*die Konsumgesellschaft ablehnt
und dies durch eine gewaltlose Haltung
und einen unkonventionellen Lebensstil zum Ausdruck bringt;
ursprünglich aus den Vereinigten Staaten, später Westeuropa.
Der\*die Hippie ist apolitisch.
{{%/epigraphe%}}

{{%ligneblanche%}}

Es war an einer Demo gegen monsanto. Es waren ganz viele Bobos° und
Hippies da, Kinder und Alte --- Menschen aus allen sozialen Schichten.
Vermummt war kein Mensch, es flogen auch keine Molotowcocktails[^19].
Soweit so gut. Ich möchte diese Geschichte erzählen, weil ich denke,
dass ich dadurch das aufständische Potenzial entdeckte. Seither hat sich
mein Aktivismus inhaltlich und in seiner Form verändert. An diesem Tag
aber habe ich verstanden, dass mensch gegen das todbringende Handeln der
Wirtschaft und der Unterdrückenden protestieren kann, selbst wenn Erfolg
nicht garantiert ist und womöglich im Wissen, dass das alles sowieso zu
(fast) nichts führt. Ich möchte diese Geschichte aber auch erzählen,
weil ich denke, dass es Menschen gibt, die dieses Buch lesen werden und
denen diese Geschichte vielleicht nützlich sein könnte...

Ich nahm also an dieser Demo gegen monsanto teil. Das Polizeiaufgebot,
um die Demonstrant\*innen vom Gebäude fernzuhalten, war ziemlich
eindrücklich. Vor allem, weil wir uns in einer relativ kleinen Stadt mit
einer schlaffen, bürgerlichen Bevölkerung befanden; eine von diesen
"sozialistischen" Städten, die monsanto ohne grössere ethische Konflikte
zu beherbergen weiss.

Für mich waren amerikanische Filme der einzige Anhaltspunkt, weshalb ich
das Polizeiaufgebot nicht besonders schockierend fand. Die Leute, die
ebenfalls da waren, und mehr Erfahrung als ich auf dem Buckel hatten,
waren erstaunt über das grosse Polizeiaufgebot. In der Theorie müsste
dieses Staatsorgan die öffentlichen Interessen verteidigen. Hier war es
aber zum Schutz eines privaten Unternehmens im Einsatz, und die Bullen
kamen aus allen umliegenden Gemeinden.

Mit Helm und Waffen ausgestattet befand sich die Polizei im Schatten des
Gebäudeeingangs. Herausgelockt wurde sie erst, als wir die wackeligen
Barrieren durchbrachen. Von weitem sah ich, wie einige Menschen Eier und
andere Dinge auf geschlossene Schaufenster warfen. Ich glaube, ich hätte
mich ihnen gerne angeschlossen. Es löste in mir ein berauschendes Gefühl
aus. Ich hatte sowas noch nicht oft gemacht. Ich erinnere mich, dass ich
unheimlich schwitzen musste. Es rann mir nur so den Rücken herunter,
unter den Achseln war ich ganz nass. Rückblickend bin ich an diesem Tag
einige eher blöde Risiken eingegangen. Ich war weder vermummt, noch
begleitet.

Ich fühlte Wut, aber eher vage. Die Robocops, die mir gegenüberstanden,
gingen mir ebenso auf die Nerven, wie sie mich neugierig machten. Ich
empfand es als sehr ungerecht, dass sie sich hier --- zwischen uns und
diesem Scheiss-Unternehmen --- aufhielten. Einer von ihnen schien mir,
hinter seinem Visier, ganz jung zu sein. Ich wollte ihn aus der Nähe
beäugen, vor allem wollte ich herausfinden, wie nahe ich an sein Gesicht
kommen konnte. Natürlich war ich provokativ, aber auch neugierig. Ich
wollte herausfinden, ob der so unnahbar scheinende Raum es tatsächlich
auch war. Angst machte ich ihm wohl kaum, denn er räusperte nur
annähernd, als ich mich direkt vor seine Fresse hinstellte. Weil er kaum
einen Wank machte, war ich, glaube ich, etwas beleidigt. So entschied
ich, einige Stunden vor ihm auszuharren.

Und das dauerte eine Weile.

Irgendwann fühlte sich der Bulle dann aber doch herausgefordert von mir,
diesem Kind, das fünf Zentimeter von seiner kugelsicheren Weste und
seinem Sturmgewehr entfernt wie angewurzelt dastand. Ich hatte den
Eindruck, wir würden "Wer zuerst blinzelt, hat verloren" spielen. Er
zwinkerte mir zu, ich machte Grimassen. Dann, nach fast zwei Stunden,
fragte ihn sein Kommandant, ob er nicht den Platz wechseln möchte. Er
lehnte dankend ab und meinte nur, es wäre für ihn okay, vor einem "so
hübschen Mädchen" zu verweilen. Ich begann ernsthaft an meiner Aktion zu
zweifeln. Offenbar fand der Typ das angenehm, und das war ja wirklich
überhaupt nicht das erhoffte Ziel. Dennoch hatte ich den Eindruck, dass
er lieber woanders gewesen wäre, vor allem, weil die Fotograf\*innen die
Szene fotografierten. Ich hätte ihn anspucken sollen, das hätte sicher
etwas bewirkt. Ich war ja eigentlich nahe genug an seinem Gesicht.
Einerseits war ich angeekelt, andererseits begann ich ernsthaft, mich zu
langweilen. Ich schwitzte immer noch, unterdessen aber nicht mehr wegen
des Adrenalins, sondern wegen der Sonne. Ich hätte ihn zumindest
auslachen, ihm den Stinkefinger zeigen und ihm sagen sollen, dass er ein
Stück Scheisse sei. Das habe ich verpasst. Ich blieb wie angewurzelt
stehen , wie eine Art nutzlose Idealistin.

Zum einen, so glaube ich, war ich nach wie vor von dieser Vorstellung
besessen, dass die Revolution friedlich ablaufen müsste. Dieses
Hippie-Ideal, wo du dir vorstellst, wie mensch Blumen in die Kanonen
steckt, den Panzer von seinem Kurs abbringt und dabei stoisch und stolz
dasteht. Doch die Wahrheit ist, dass dich der Panzer über den Haufen
fährt. Diese Bilder sind wunderbar, klar, doch ich glaube, ich habe
begriffen, dass das nicht so abläuft. So einfach ist es eben nicht, die
Autorität zu beeindrucken und mit einfachen und symbolischen Gesten ins
Wanken zu bringen. Es sind beeindruckende Bilder, sie prägen eine
akzeptierbare Denkweise, die herzergreifend und leicht zu vermitteln
ist. Die Wahrheit ist aber, dass durch gewaltlosen Widerstand und einen
weichen Konsens, der durch das öffentliche Anpreisen von
Held\*innentaten gegenüber einer kompromissbereiten Regierung zustande
gekommen ist, keine sozialen Veränderungen ausgelöst werden, oder nur
minimale. Oft waren es (bewaffnete) Kämpfe im Verborgenen, die mit
gesättigter und tauber Stimme einige Fundamente zum Beben, manchmal
sogar die Pflastersteine zum Bersten brachten, damit sie eine
Menschenmenge einzeln aufliest und sie auf das mit Helm geschützte
Gesicht der Macht wirft. Was privilegierte Menschen wie ich von den
Kämpfen jeweils wahrnehmen, ist vielfach nur die sichtbare Spitze des
Eisbergs. Mensch ist häufig aus Ignoranz erstaunt, ja gar empört von der
Reaktion der Unterdrückten. Es scheint übertrieben, zu radikal, zu
gewalttätig.

Der andere Aspekt, den es im Zusammenhang mit dem Problem der
Spektakularisierung von Revolten zu bedenken gilt, ist, dass sie allzu
oft dazu führt, dass die Kämpfe von Personen vereinnahmt werden, die
nicht oder kaum betroffen sind. Das muss ich womöglich etwas ausführen.
Die Geschichte, die ich soeben geschildert habe, wurde auch mithilfe
eines Fotos erzählt. Ein Foto, das vor nicht allzu langer Zeit wieder
herausgekramt wurde, um irgendetwas zu illustrieren --- was genau, weiss
ich auch nicht mehr. Dabei geht ganz vergessen, dass ich erst dank
meinem privilegierten Status diese Haltung einnehmen und stundenlang vor
diesem Robocop ausharren konnte. So geht das eben mit aufsehenerregenden
Bildern wie diesem. Einerseits werden kollektive Kämpfe durch solche
Bilder individualisiert und andererseits rücken sie immer die gleichen
Kämpfe und Aktivist\*innen in den Vordergrund: Weisse Aktivist\*innen,
CIS-Menschen, Menschen mit Rechten, gültigen Papieren und einer grossen
Klappe sowie Bobos. Von Rassismus betroffene Menschen, Transmenschen°,
nicht-binäre, behinderte oder verarmte Menschen und Sans Papiers werden
ungerechterweise nicht in den Vordergrund gerückt. Und dies obwohl sie
ihre Rechte verteidigen.

Doch dazumal wusste ich noch nicht, dass dieser stumpfsinnige Bulle mir
einzig deshalb nicht die Fresse einschlug, weil ich eine junge Weisse
privilegierte Person bin, die eine Überzeugung vertritt, die politisch
akzeptabel ist und vereinnahmt werden kann. Ich wusste noch nicht, dass
Bullen töten, dass andere Menschen aufgrund der von ihr ausgeübten
Gewalt sterben und sie dafür nicht bestraft werden.[^20] Das wusste ich
noch nicht. Ich wusste noch nicht, dass alle Bullen Schweine sind
(ACAB°). Ich realisierte zudem auch nicht, dass es problematisch sein
könnte, wenn ich mich so stark in den Vordergrund dränge.

So war das nun mal. Das sind Dinge, die mensch lernt, wenn er mit
anderen gemeinsam Aktionen organisiert, Erzählungen zuhört, Bilder
sieht, die Polizeigewalt zeigen und so Wut im Bauch auslösen. Genau, wir
lernen doch am besten, wenn wir einander zuhören. Für einmal Klappe zu
und dem Gegenüber zuhören. Wir schauen, wie wir nützlich sein können.
Manchmal ist das Beste, was mensch machen kann, im Hintergrund zu
bleiben, Essen zuzubereiten[^21], den Raum anderen zu überlassen, die
Bullen zu filmen, vor einem Polizeiposten auf die freigelassenen
Menschen zu warten oder Soli-Aktionen zu organisieren.

Kurzum: Unsere Privilegien für einmal denjenigen Menschen zur Verfügung
stellen, die es nötig haben. Wir privilegierte Menschen sollten nicht
meinen, wir müssten immer in der vordersten Reihe stehen. Auch sollten
wir aufhören, uns selber zu erzählen, wie ach so spektakulär unser Kampf
ist. Da ist nichts Wahres dran. Wir müssen jenen Teil in uns zum
Schweigen bringen, der mit einer reformistischen, universalistischen,
gewaltfreien Vision genährt wurde, jedoch von der sozialen Realität
abgeschnitten ist. Wenn wir die Härte der Ungerechtigkeit realisieren,
wenn wir jene Menschen kennen lernen, die täglich Gewalt ausgesetzt
sind, sei es durch ihre Aktionsform oder ihre Identität, dann verstehen
wir etwas besser, dass es hier nicht *about us* geht. Das hoffe ich
jedenfalls. Den Hippie in unserem Kopf lassen wir dann auf kleiner
Flamme krepieren.

Beim nächsten Mal wird es vielleicht ein bisschen weniger bedrückend,
weniger problematisch, weniger lahm sein.

Beim nächsten Mal werden wir zurückkehren.

Vermummt.

Im hinteren Teil der Demo.

Vielleicht mit Pflastersteinen in den Händen.

Ganz sicher aber gemeinsam mit unseren Adelphen°.

Und wir werden als Masse, mit unseren unterschiedlichen Vorstellungen
und unseren ungleichen Privilegien hingehen --- aber wir werden es
versuchen.

Wir werden eine Horde, vielfältig und gesichtslos sein

Wir werden es versuchen, ganz bestimmt.

[^19]: Im Text *Drohnen* \[n. 1\] kommen militante Aktivist\*innen zu Wort.

[^20]: Apropos unbestrafte Polizeigewalt: *They don't see us* \[n. 4\].

[^21]: Der Text *Das grosse Mittagessen* \[n. 47\] beschreibt praktische Erfahrungen einer VoKü.
