---
title: Sprachleitfaden
meta: true
num: "0-2"
metatext: |
  Ein Buch für alle. Gemeinsam mit den Aktivist*innen haben die Herausgeber*innen-Kollektive eine Reihe typografischer Konventionen beschlossen, die auf alle Texte angewendet werden. Die meisten von ihnen verwenden eine nicht-binäre inklusive Schreibweise, die sich für das mündliche, laute Vorlesen eignet.
---

Das Deutschschweizer Redaktionskollektiv hat im Rahmen der übersetzten
Texte einen Sprachleitfaden für die deutsche Textsammlung von *Wir sind
überall* erarbeitet.

## Gendergerechte Sprache

### Gendersternchen \*

Mit dem Gendersternchen, auch als Asterix bezeichnet, wird die ganze
Vielfalt der Geschlechter sichtbar gemacht (männlich, divers, weiblich).
Das Sternchen symbolisiert alle Geschlechter, die keine grammatikalische
Entsprechung haben. Es wird zwischen der maskulinen und femininen
Wortendung platziert, z. B. Aktivist\*innen oder er\*sie.

Mündlich wird das Gendersternchen als eine kurze Pause ausgesprochen.

### Anderes

Anstelle des Pronomens «man», das in der feministischen Sprachkritik ein
No-Go ist, wird das neutrale «mensch» verwendet, dass alle mit meint,
unabhängig vom Geschlecht.

Steht im Text einzig die männliche Form, z. B. Polizisten, ist dies ein
bewusster, absichtlicher Entscheid der Autor\*innen, um auf die
strukturelle, maskuline Staatsgewalt und deren Herrschaftsposition
hinzuweisen.

## Terminologie

Alle Anfangsbuchstaben von kapitalistischen Grossunternehmen werden mit
einem kleinen Anfangsbuchstaben geschrieben, z. B. heineken, um ihnen
nicht noch mehr Beachtung zuschenken, als dass sie ohnehin schon haben.

## Kursive Begriffe

*Kursiv* geschrieben werden alle Anglizismen, sofern sie nicht bereits
vom duden, dem Standard-Wörterbuch der deutschen Sprache, aufgenommen
wurden.

Ebenfalls kursiv geschrieben werden andere fremdsprachige Begriffe wie
*Voilà*, feste Namen von Kollektiven wie *Jean Dutoit*, Verweise auf
geografische Orte wie *Quartier de Fourmi* sowie gezielt ausgewählte
Wörter wie *wirklich*, die so beudeutungssprachlich hervorgehoben
werden.

## Anderes

Die Begriffe «Schwarz» und «Weiss» werden bewusst gross geschrieben. Sie
beziehen sich nicht auf die reelle Hautfarbe, sondern vielmehr auf den
sozialen, politischen und kulturellen Kontext. Die
Antirassismus-Bewegung nutzt die Bezeichnungen, um Rassismus in der
Sprache entgegenzutreten. Der Grossbuchstabe bei «Weiss» weist auf die
Privilegien von Menschen hin, die nicht Rassismus ausgesetzt sind und
sich deshalb in einer machtvolleren Position innerhalb unserer
Gesellschaft befinden. «Schwarze Menschen» ist eine selbstgewählte,
positiv konnotierte Bezeichnung von Menschen, die auf ihre gemeinsame
Betroffenheit durch rassistische Diskriminierung verweist.
