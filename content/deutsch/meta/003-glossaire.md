---
title: Glossar
meta: true
withoutindent: true
num: "0-3"
metatext: |
  Die im Glossar definierten Wörter sind in den Texten mit einem Gradzeichen (°) markiert.
---

<div class="glossary">

## ACAB, 1312

ACAB ist ein Akronym aus dem Englischen *All Cops Are Bastards* (Alle
Polizisten sind Schweine) und ein Anti-Polizei-Slogan. Das Akronym wird
manchmal durch die Zahl 1312 ersetzt, die auf die Position der
Buchstaben A, C, A, B im lateinischen Alphabet verweisen. Es kam bereits
vor, dass Personen, die es verwendeten, wegen Polizistenbeleidigung
verurteilt wurden. Mehrere Abwandlungen existieren -- um die Zensur zu
umgehen (*All Cats Are Beautiful*) oder andere Kämpfe einzubeziehen
(*All Colors Are Beautiful*; *All Clitoris Are Brave* usw.).

## Adelphität / Adelphen

Es handelt sich um einen Neologismus, ein Kunstwort, das von der
griechischen Wurzel *adelph* stammt. Er bezeichnet die Verbindung
zwischen Personen mit denselben Eltern, unabhängig vom Geschlecht.

Der Begriff wird als geschlechtsneutrales Wort verwendet, mit dem mensch
sowohl «Schwesterlichkeit» als auch «Brüderlichkeit» ausdrücken kann. Im
Queer°feminismus wird er verwendet, um auch nicht-binäre Menschen
miteinzubeziehen.

## Aktionskonsens

Ein Aktionskonsens sind die Rahmenbedingungen, die von
Organisator\*innen und/oder Teilnehmer\*innen einer Aktion festgelegt
werden, um die theoretischen und praktischen Modalitäten einer Aktion zu
definieren. Er kann z. B. den Ablauf der Aktion, das erwünschte
Verhalten der Teilnehmer\*innen, den Einsatz von illegalen/gewalttätigen
Aktionen, die rechtlichen Risiken für die Teilnehmer\*innen oder die
Kommunikations- und Interaktionsstrategie mit den Medien festlegen. Ein
Aktionskonsens legt den Rahmen fest, in dem jede\*r Aktivist\*in weiss,
was ihn\*sie erwartet, damit er\*sie sich zweckmässig sowie
mental/psychisch auf die Aktion vorbereiten kann.

## Basisarbeit

Bezeichnet die Arbeit mit den Menschen und ihren Realitäten. Politische
Basisarbeit hat zum Ziel, Menschen zusammenzubringen, sie weiterzubilden
und zu organisieren. Dabei sollen zum einen Erkenntnisse über
Systemzusammenhänge gewonnen werden, zum anderen aber auch die Menschen
in ihrer Lebensrealität abgeholt und sie dazu eingeladen werden, sich
kollektiv gegen Diskriminierung und Ausbeutung zur Wehr zu setzen.
Politische Basisarbeit kann z. B. ein kostenloses, juristisches
Unterstützungsangebot oder ein gruppeninterner Lesekreis sein.

## Black Block / Schwarzer Block / Radikaler Block / Revolutionärer Block

Der *black block*, auch Schwarzer Block genannt, ist eine kollektive
Organisationsform, die hauptsächlich bei Demonstrationen in Erscheinung
tritt. Der Name leitet sich von der schwarzen Kleidung ab, die
beabsichtigt, die Teilnehmer\*innen zu vereinheitlichen, um schwieriger
identifizierbar zu sein. Häufig tritt der Block innerhalb von
friedlichen Demonstrationszügen als «gewaltbereiter» und
entschlossenerer Teil in Erscheinung. Dieser taktische Entscheid stellt
das vom Staat legitimierte Gewaltmonopol infrage und gibt den
Aktivist\*innen die Gelegenheit, sich aktiv gegen Repression zu wehren;
beispielsweise indem sie ihre Personalien nicht überprüfen oder sich
verhaften lassen. In militanten Milieus beinhalten Begriffe wie
«radikaler Block» oder «revolutionärer Block» eine ähnliche Idee; zudem
gibt es thematische Variationen wie *green bloc* (bei Umwelt-Demos),
*pink bloc* (Feminismus/Queer°) oder *clown bloc*.

## Bobos / Bobo

Neologismus und Akronym, setzt sich aus den beiden französischen Wörtern
*bourgeois* und *bohémien* zusammen. Gemeint ist ein\*e Angehörige\*r
einer modernen, meist jungen, alternativ lebenden, verbürgerlichten,
urbanen Gesellschaftsschicht. Geprägt wurde der Begriff durch das Buch
*Bobos in Paradise* von David Brooks. Darin bezeichnet er die
US-amerikanische Oberschicht am Ende der 90er Jahre als «Konservative in
Jeans» und «Kapitalisten der Gegenkultur». Dieser Gesellschaftsschicht
wird vorgeworfen, dass sie die Gentrifizierung beschleunige.

## Care

*Care* ist ein englischer Begriff, den mensch auf verschiedene Weise auf
Deutsch übersetzen und interpretieren kann: Pflege, sich kümmern,
Fürsorge, Wohlwollen. Auf Deutsch spricht mensch auch von Care-Arbeit.
Sie beinhaltet u. a. Pflegearbeiten von abhängigen Personen, Erziehung,
psychologische Unterstützung usw. Der Begriff entstand in feministischen
Kreisen, die den Utilitarismus in der Psychologie infrage stellten.
Seitdem hat er sich auch auf andere Disziplinen ausgeweitet
(Care-Ökonomie, Care-Ethik usw.). Er wird sehr unterschiedlich
verwendet, um z. B. Arbeitsplätze im Pflegebereich oder die Beziehung zu
sich selbst und zu den anderen Menschen im Kollektiv zu bezeichnen.
Allgemein wird er häufig mit dem Verlangen, bestimmte kapitalistische,
patriarchale° und modernistische Werte wie Effizienz, Produktivität,
Männlichkeit bis zum Geht-nicht-mehr zu bekämpfen.

## Cis-Männer / Cis-Mann / cisgender

Das Adjektiv cisgender, abgekürzt «cis», ist ein Neologismus, der eine
Geschlechtsidentität° bezeichnet, bei der das empfundene Geschlecht
einer Person mit dem bei der Geburt zugewiesenen Geschlecht
übereinstimmt. Das Wort steht als Gegensatz zu Transgender°. Wenn von
Cis-Typen oder Cis-Männern die Rede ist, sind Personen gemeint, denen
bei der Geburt das männliche Geschlecht zugewiesen wurde und die sich in
diesem Geschlecht wiedererkennen. Da es sich um eine dominante Position
in patriarchalen Systemen° handelt, geniessen Cis-Männer Privilegien
gegenüber anderen Geschlechtsidentitäten.

## Direkte Aktion

Im aktivistischen Sprachgebrauch bezeichnet direkte Aktion das unmittelbare Einwirken auf den Verlauf der Dinge im Gegensatz zu institutionellen, repräsentativen und staatlichen politischen Wegen, die «indirekte» Aktionen darstellen. Direkte Aktion ist ein dehnbarer Begriff und wird je nach Auslegung anders interpretiert: kurz- und längerfristige Besetzungen, ziviler Ungehorsam, Anbringen von Plakaten mit radikalem politischem Inhalt, Sabotage, Arbeiter\*innen-Kollektive, selbstverwaltete Kollektive, kostenlose juristische Sprechstunden usw. Je nach Interpretation werden damit einzig illegale und/oder als gewalttätig geltende Aktionen bezeichnet.

## FLINTA\*

Das Akronym steht für **F**rauen (meist Cis-Frauen), **L**esben,
**I**nter Menschen, **N**icht-binäre Menschen, **T**rans Menschen,
**A**gender. Der Stern \* steht für alle, die sich in der Bezeichnung in
keinem der Buchstaben wiederfinden, aber genauso vom Patriarchat°
unterdrückt werden.

## Geschlechtsidentität

Die Geschlechtsidentität ist ursprünglich ein Konzept aus der
Soziologie, die das Gefühl der Zugehörigkeit zu einem Geschlecht
ausserhalb jeglicher biologischer Überlegungen bezeichnet. Der Begriff
ermöglicht es, sich von der Biologie zu lösen, die Geschlechter als
soziale Konstrukte zu betrachten und sich somit auf das Gefühl einer
Person zu stützen, um ihr Geschlecht zu definieren. Die
Geschlechtsidentität ist von der sexuellen Orientierung zu
unterscheiden, die das Geschlecht der Personen bezeichnet, zu denen sich
mensch sexuell und/oder aus Liebe hingezogen fühlt.

## Intersektionalität

Intersektionalität ist ein Begriff, der in der Soziologie und der
politischen Theorie verwendet wird, um die Situation von Menschen zu
bezeichnen, die in einer Gesellschaft gleichzeitig mehrere Formen von
Diskriminierung hinnehmen müssen. Der Begriff wurde 1989 von der
US-amerikanischen Wissenschafterin und afrofeministischen Aktivistin
Kimberlé Williams Crenshaw vorgeschlagen, um insbesondere die
Überschneidung von Sexismus und Rassismus zu thematisieren, die
afroamerikanische Frauen erleiden. Er hilft auch zu erklären, warum
diese Frauen in den damaligen feministischen Auseinandersetzungen nicht
berücksichtigt wurden.

## Kollekte

Bei der Kollekte wird der Preis durch diejenigen Personen festgelegt,
die etwas kaufen, mieten oder eine Dienstleistung in Anspruch nehmen.
Achtung: nicht gleichzusetzen mit kostenlos, unentgeltlich. Der Preis
des Gegenstands, Produkts oder der Dienstleistung wird also nicht von
der Person festgelegt, die ihn anbietet. Die Kollekte eröffnet den
Zugang zu etwas, das den Möglichkeiten eines\*r Käufers\*in entspricht;
er richtet sich nicht nach der Marktlogik. Manchmal wird ein
Selbstkostenpreis oder eine Preisempfehlung angegeben. Bei der Kollekte
besteht die Möglichkeit, dass der Gegenstand, das Produkt oder die
Dienstleistung kostenlos angeboten wird oder nicht.

## LGBTQIA+

LGBTQIA+ ist ein Akronym, das verschiedene sexuelle Orientierungen,
Geschlechtsidentitäten° und Gemeinschaften beschreibt, die unser
heteronormatives und patriarchales° Gesellschaftsmodell nicht
miteinbezieht: **L**esbian (lesbisch), **G**ay (schwul), **B**isexual
(bisexuell), **T**ransgender (transgender), **Q**ueer (queer),
**I**ntersexual (intersexuell), **A**sexual (asexuell). Das Zeichen «+»
dient dazu, die Vielfalt der sexuellen Orientierungen und
Geschlechtsidentitäten zu bezeichnen: Pansexualität, agender,
genderqueer, bigender, genderfluide Menschen usw.

## Mitstreiter\*innen

Mitstreiter\*innen sind Menschen, die Kämpfe gegen verschiedene Formen
von Unterdrückung unterstützen, ohne davon betroffen zu sein. Der
Begriff Mitstreiter\*innen hilft zu verdeutlichen, dass die von einer
Unterdrückung betroffenen Menschen ihren Kampf selbst führen sollen, und
dass es ihre Aufgabe ist, sich mitzuteilen, selbst zu bestimmen und zu
entscheiden, was sie brauchen. Ein Cis-Mann° kann beispielsweise
zuhören, sich informieren, einen Dekonstruktionsprozess einleiten,
unterstützen und Hilfe leisten, je nachdem, welche Bedürfnisse von
feministischen Gruppen geäussert werden. Es ist aber nicht an ihm, zu
bestimmen, wie feministische Kämpfe geführt werden sollen. Dasselbe gilt
für Weisse Menschen gegenüber antirassistischen Kämpfen sowie
cis-heterosexuelle Menschen gegenüber LGBTQIA+°-Kämpfen, Chef\*innen
gegenüber Gewerkschafts- und Arbeiter\*innenkämpfen, nichtbehinderte
Menschen gegenüber behinderten Menschen usw.

## Mixité choisie oder non-mixité

Für den Begriff gibt es auf Deutsch kein exaktes Äquivalent. *Mixité
choisie* bezeichnet eine Form der Organisation, bei der Menschen
zusammenkommen, die einer oder mehreren unterdrückten oder
diskriminierten sozialen Gruppen angehören. Um nicht die üblichen
sozialen Schemas zu reproduzieren, werden Menschen ausgeschlossen, die
einer Gruppe angehören, die strukturell diskriminierend oder
unterdrückend ist. Diese Praxis wird von Gruppen verwendet, die sich dem
Feminismus oder dem Antirassismus sowie Bewegungen um LGBTQIA+,
Gender-Minoritäten oder behinderter Menschen zuordnen. Siehe auch *safe
space°*.

## Nicht-Binarität / Nicht-binäre Personen

Nicht-Binarität ist ein Konzept, das verwendet wird, um die
Kategorisierung von Menschen zu bezeichnen, die als nicht-binär oder
*genderqueer* bezeichnet werden. Ihre Geschlechtsidentität passt nicht
in die binäre Norm, d. h. die Person fühlt sich weder als Mann noch als
Frau, sondern als etwas dazwischen, eine «Mischung» aus beidem oder
keines von beidem. Diese Identität widersetzt sich der
Geschlechterbinarität und der damit verbundenen Geschlechterhierarchie
in einem patriarchalen System°. Sie stellt auch die sexuelle Zuweisung
zu einem bestimmten Geschlecht infrage.

## Ökodzid

Begriff, der all jene Handlungen bezeichnet, die das/die Ökosystem/e
zerstören. Die Formulierung auf -zid lehnt sich an den Begriff Genozid
an und verweist darauf, dass diese Handlungen explizit kriminell
und/oder mörderisch konnotiert sind.

## Patriarchat

Früher eher zur Bezeichnung der Herrschaft des Patriarchen über
den Rest der Familie (Frau, Kinder, Sklav\*innen, Angestellte usw.)
verwendet; bezeichnet der Begriff heute allgemeiner die soziale,
politische und rechtliche Organisation, die auf Autorität, Macht und die
damit verbundenen Formen der Diskriminierung beruht und von Cis-Männern
ausgeübt wird.

In feministischen Theorien wurde der Begriff verwendet, um die
Besonderheit der Unterdrückung von Frauen im Vergleich zu anderen Arten
der Unterdrückung (Klassismus, Rassismus usw.) aufzuzeigen und zu
analysieren. Die zeitgenössischen intersektionalen° feministischen
Bewegungen betonen die Verflechtung des Patriarchats mit anderen Formen
der Herrschaft.

## People of Color (PoC) / Person of Color (PoC) / Black and People of Color (BPoC) / Black, Indigenous and People of Color (BIPoC)

Menschen, die (systemischen) Rassismus erfahren, verwenden den Begriff
PoC, um sich selbst zu bezeichnen. Er ist positiv konnotiert.
Erweiterungen des Begriffs sind BPoC , womit ausdrücklich auch Schwarze
Menschen miteinschlossen werden und BIPoC, womit explizit auch indigene
Menschen mitgemeint sind.

## Prekariat / prekarisierte Menschen / Prekarisierung

Begriff um jene Klasse von Menschen zu beschreiben, die in prekären
Verhältnissen leben. Manchmal sagt mensch, dass er in zeitgenössischen
westlichen Demokratien den Begriff Proletariat ersetzt hat. In diesem
Sinne bezeichnet der Begriff eine eigene Realität: Während die
Mittelschicht wächst, entsteht eine prekäre Klasse, die nicht so homogen
ist (z. B. Arbeitsort, Lebensbedingungen usw.) wie es die
Arbeiterklasse, d. h. das Proletariat, war. Der Begriff des Prekariats
wird im Allgemeinen für eine Reihe sehr unterschiedlicher Realitäten
verwendet: Student\*innen, Sans-Papiers, uberisierte Arbeiter\*innen
usw.

## Queer

Queer ist ein Anglizismus, der wörtlich übersetzt «seltsam» oder
«sonderbar» bedeutet und war ursprünglich ein homophobes und transphobes
Schimpfwort. Anfangs der 1990er Jahre wurde es von den betroffenen
Minderheiten zurückerobert und positiv konnotiert. Es wird verwendet, um
alle Personen zu bezeichnen, die eine andere sexuelle Orientierung als
die Heterosexualität haben oder sich zu einer Geschlechtsidentität°
bekennen, die sich von der Cis-Identität° unterscheidet.

## Safe space

Wörtlich übersetzt heisst *safe space* ein «sicherer Raum»; es ist ein
Ort, der es marginalisierten und unterdrückten Menschen ermöglicht, sich
in einer sicheren und gutgesinnten Umgebung zu treffen, die frei von den
üblichen Herrschaftsformen ist. Die Idee des *safe space* bezeichnet oft
einen ständigen Prozess der Selbstkritik, bei dem darüber nachgedacht
wird, wie Unterdrückung in vorübergehenden oder andauernden Räumen
reproduziert wird. *Safe spaces* entstanden in den 1960er Jahren in den
USA innerhalb der LGBTQIA+-Szene; die Idee dahinter ist eng mit jener
von *mixité choisie°* verbunden. Viele Kollektive sind sich einig, dass
der *safe space* eher ein kritisches Ideal ist, das es anzustreben gilt,
als eine objektiv beobachtbare Realität, weshalb manchmal auch von
*safer space* («sicherere Räume») gesprochen wird. Die Idee eines
«sicheren Raumes» tendiert dazu, die Erfahrungen aller Menschen zu
vereinheitlichen: Ein Raum kann für Weisse Cis-Frauen sicher sein, aber
nicht für Schwarze Cis-Frauen.

## Selbstorganisation / Selbstverwaltung

Selbstverwaltung bedeutet, dass sich eine Gruppe von Personen oder ein
Gefüge von Menschen selbst kollektiv verwaltet, ohne dass dabei eine
Regierung oder andere hierarchische Strukturen Entscheidungsgewalt
besitzen. Selbstverwaltung ist eine Organisationsform, die insbesondere
die Abschaffung jeglicher Form von Hierarchie, die Suche nach Konsens
beim Entscheidungsfindungsprozess und die Kollektivierung von Reichtum,
Wissen, Rechten und Pflichten beinhaltet. Theoretisch und praktisch
bedeutet Selbstverwaltung häufig, dass repräsentative Funktionen in
einer Organisation abgelehnt oder zumindest reduziert werden.

## Sexarbeit / Sexarbeiter\*innen

Sexarbeit (engl. *sex work*) ist ein Synonym für Prostitution, kann aber
auch andere Berufe bezeichnen, die mit der Sexindustrie in Verbindung
stehen: Pornodarsteller\*innen, Escort-Girls/-Boys, Camgirls/-boys,
Sexualassistent\*innen usw. Er wurde erstmals von der aktivistischen,
feministischen und sexpositiven Sexarbeiterin Carol Leigh mit dem Ziel
verwendet, das Wort «Prostituierte» zu entdämonisieren. Dieser Begriff
ermöglicht es auch, die Rechte, soziale Garantien und materielle
Bedingungen zu betonen, die mit der Sexarbeit verbunden sein sollten,
wie sie es in allen anderen Berufen auch sind.

## Sozialisation, sozialisiert

In der Soziologie bezeichnet der Begriff jene Prozesse, durch die ein
Individuum die Normen, Rollen und Werte, die das soziale Leben
strukturieren, erwirbt und verinnerlicht und dadurch seine soziale und
psychologische Identität konstruiert. Die Sozialisation erfolgt über
verschiedene Instanzen (z. B. die Erziehung oder der Kontakt mit
Gleichaltrigen). Das Konzept der geschlechtsspezifischen Sozialisation
ermöglicht die Weitergabe und Verinnerlichung von Normen und sozialen
Codes in Bezug auf männlich und weiblich sowie die Entwicklung der damit
verbundenen Geschlechtsidentitäten zu erklären.

## Suffragetten

Der Begriff Suffragetten bezeichnet die Bewegungen des frühen 20. Jahrhunderts
für das Frauenwahlrecht im Vereinigten Königreich und
in den USA. Der Begriff hat seinen Ursprung in der *National Union of
Woman\'s Suffrage Societies (NUWSS)*, die 1897 von Millicent Fawcett in
England gegründet wurde. Die Bewegung, die vorwiegend aus der Weissen
bürgerlichen Schicht stammt, ist in verschiedene soziale Schichten und
für unterschiedliche Zwecke eigetreten», wobei der gemeinsame Nenner der
Kampf für das Frauenwahlrecht war. In der Bewegung sind zwei
Hauptrichtungen auszumachen: der gemässigte, verfassungsrechtliche
Ansatz der *NUWSS* und die direkte Aktion°, die insbesondere von der
*Women\'s Social and Political Union (WSPU)* von Emmeline, Christabel
und Sylvia Pankhurst vertreten wird.

## Systemisch

Begriff aus den holistischen/strukturalistischen Ansätzen der
Sozialwissenschaften, die den Einfluss sozialer, kultureller und
politischer Strukturen auf Individuen und Praktiken, aber auch

auf Institutionen und gemeinsame Vertretungen unterstreichen. In der
Sprache der Aktivist\*innen wird der Begriff häufig verwendet, um eine
Frage von der individuellen Problematisierungsebene zu lösen und sie in
umfassendere soziale Gegebenheiten einzubetten (systemisch, durch ein
System und nicht durch individuelle Merkmale oder Bestimmungen
verursacht).

## Trans / Transgender / Trans-Identitäten

Mensch spricht von Transidentität(en), um Identitäten zu bezeichnen, bei
denen im Gegensatz zur Cis°-Identität die gelebte Geschlechtsidentität°
und das bei der Geburt zugewiesene Geschlecht nicht übereinstimmen.

## Verbalradikalismus

Bezeichnet die Form der Verbreitung von politisch radikalen Ideen, die
sich ausschliesslich auf Sprechhandlungen bzw. sprachliche Äusserungen
bezieht. Oft wird der Begriff verwendet, um die tatsächliche Untätigkeit
von Menschen zu kritisieren, die jedoch oft und gern von radikalen
Handlungen und Konzepten sprechen und diese fast schon beschwören.

</div>

