//
// Search Engine with Lunrjs
// Inspired by Hugo Lithium Theme - Janik von Rotz - MIT License
// https://github.com/janikvonrotz/hugo-lithium-theme/blob/master/exampleSite/content/page/search.md
//
// Plus search script inside init function
//
// define globale variables
let idx;
let results;
let searchInput;
let searchResults = null;
let documents;
let mediaQuery = window.matchMedia('(min-width: 50em)');
let searchDataURL;
let indexJson;
if (window.location.href.indexOf("/de/") != -1) {
  searchDataURL = '/de/searchdata.json';
  indexJson = '/de/index.json';
}
else if (window.location.href.indexOf("/en/") != -1) {
  searchDataURL = '/en/searchdata.json';
  indexJson = '/en/index.json';
}
else if (window.location.href.indexOf("/it/") != -1) {
  searchDataURL = '/it/searchdata.json';
  indexJson = '/it/index.json';
}
else {
  searchDataURL = '/searchdata.json';
  indexJson = '/index.json';
}
let currentContent;
let metaHome;
// if (mediaQuery.matches) {
//   currentContent = document.querySelectorAll('.search__results ~ *');
// } else {
//   currentContent = document.querySelectorAll('.search__results ~ *, .meta--home');
// }

let tableOpen = false;
let tableDiv = document.querySelector('.table');

function renderSearchResults(results) {
  if (tableDiv.classList.contains('show')) {
    tableOpen = true;
  }

  if (results.length > 0) {
    // show max 10 results
    // if (results.length > 9){
    //     results = results.slice(0,10)
    // }

    // reset search results
    searchResults.innerHTML = '';

    if (tableOpen) {
      tableDiv.classList.remove('show');
    }

    // append results
    results.forEach((result) => {
      // create result item
      let article = document.createElement('article');
      article.classList.add('search__result');
      article.innerHTML = `
        <h2 class="search__title search__title--result"><a href="${result.ref}">${documents[result.ref].title}</a></h2>
        <div class="search__subtitle">${documents[result.ref].subtitle}</div>
        <div class="search__authors">${documents[result.ref].authors}</div>
        <div class="search__lang">${documents[result.ref].lang}</div>
        <div class="search__categories">${documents[result.ref].categories}</div>
        `;
      searchResults.appendChild(article);
    });

    // if results are empty
  } else {
    searchResults.innerHTML = '<p class="search__noresult">'+ document.querySelector('.search__noresult--textinvisible').innerHTML +'</p>';
  }
}

function registerSearchHandler() {
  // register on input event
  if (window.location.href.indexOf("/de/") != -1) {
    searchDataURL = '/de/searchdata.json';
    indexJson = '/de/index.json';
  }
  else if (window.location.href.indexOf("/en/") != -1) {
    searchDataURL = '/en/searchdata.json';
    indexJson = '/en/index.json';
  }
  else if (window.location.href.indexOf("/it/") != -1) {
    searchDataURL = '/it/searchdata.json';
    indexJson = '/it/index.json';
  }
  else {
    searchDataURL = '/searchdata.json';
    indexJson = '/index.json';
  }
  searchInput.oninput = (event) => {
    // remove search results if the user empties the search input field
    if (searchInput.value == '') {
      searchResults.innerHTML = '';

      searchResults.style.display = '';

      currentContent.forEach((el) => {
        el.style.display = '';
      });
      if (metaHome) {
        metaHome.classList.remove('meta--search');
      }
      // if (tableOpen) {
      //   tableDiv.classList.add('show');
      // }
    } else {
      searchResults.style.display = 'block';

      // Hide current content
      currentContent.forEach((el) => {
        el.style.display = 'none';
      });
      if (metaHome) {
        metaHome.classList.add('meta--search');
      }

      // get input value
      let query = event.target.value;

      var allChars = {
                   À: 'A',
                   Á: 'A',
                   Â: 'A',
                   Ã: 'A',
                   Ä: 'A',
                   Å: 'A',
                   Æ: 'AE',
                   Ç: 'C',
                   È: 'E',
                   É: 'E',
                   Ê: 'E',
                   Ë: 'E',
                   Ì: 'I',
                   Í: 'I',
                   Î: 'I',
                   Ï: 'I',
                   Ð: 'D',
                   Ñ: 'N',
                   Ò: 'O',
                   Ó: 'O',
                   Ô: 'O',
                   Õ: 'O',
                   Ö: 'O',
                   Ő: 'O',
                   Ø: 'O',
                   Ù: 'U',
                   Ú: 'U',
                   Û: 'U',
                   Ü: 'U',
                   Ű: 'U',
                   Ý: 'Y',
                   Þ: 'TH',
                   ß: 'ss',
                   à: 'a',
                   á: 'a',
                   â: 'a',
                   ã: 'a',
                   ä: 'a',
                   å: 'a',
                   æ: 'ae',
                   ç: 'c',
                   è: 'e',
                   é: 'e',
                   ê: 'e',
                   ë: 'e',
                   ì: 'i',
                   í: 'i',
                   î: 'i',
                   ï: 'i',
                   ð: 'd',
                   ñ: 'n',
                   ò: 'o',
                   ó: 'o',
                   ô: 'o',
                   õ: 'o',
                   ö: 'o',
                   ő: 'o',
                   ø: 'o',
                   ù: 'u',
                   ú: 'u',
                   û: 'u',
                   ü: 'u',
                   ű: 'u',
                   ý: 'y',
                   þ: 'th',
                   ÿ: 'y',
                   ẞ: 'SS'
                  };
      var fewChars = {
                   È: 'E',
                   è: 'e',
                  };

      var re = new RegExp(Object.keys(allChars).join("|"),"gi");
      query = query.replace(re, function(matched){
        return allChars[matched];
      });

      // run fuzzy search
      if (idx === undefined || documents === undefined) {
        searchResults.innerHTML = '<p class="search__noresult">'+ document.querySelector('.search__noresult--wait').innerHTML +'</p>';
      } else {
        if (query.charAt(0) == '*') {
          results = '';
        }
        else if (query.charAt(0) == '^') {
          results = '';
        }
        else if (query.includes('+')
            || query.includes('\\')
            || query.includes('~')) {
          results = '';
        }
        else if (query.slice(-1) == ' ') {
          query = query.slice(0, -1);
          results = idx.search(query + '*');
        }
        else if (query.slice(-1) == '^') {
          query = query.slice(0, -1);
          results = idx.search(query + '*');
        }
        else {
          results = idx.search(query + '*');
        }
        renderSearchResults(results);
      }
    }
  };
}

//
// Load the pre-built index in idx variable
//
const requestSearch = async () => {
  const response = await fetch(searchDataURL);
  const json = await response.json();
  idx = lunr.Index.load(json);
  // Erase if search attempt before loading
  searchInput.value = '';
  searchResults.innerHTML = '';
  searchResults.style.display = '';
  currentContent.forEach((el) => {
    el.style.display = '';
  });
  if (metaHome) {
    metaHome.classList.remove('meta--search');
  }
}
requestSearch();

//
// Make an array (variable 'documents') with the JSON
// to display the result
//
const requestJson = async () => {
    const response = await fetch(indexJson);
    const jsonSearchFile = await response.json();
    documents = [];
  jsonSearchFile.forEach(doc => {
        documents[doc.url] = {
          title: doc.title,
          subtitle: doc.subtitle,
          lang: doc.lang,
          categories: doc.categories,
          authors: doc.authors,
        };
  });
  // Erase if search attempt before loading
  searchInput.value = '';
  searchResults.innerHTML = '';
  searchResults.style.display = '';
  currentContent.forEach((el) => {
    el.style.display = '';
  });
  if (metaHome) {
    metaHome.classList.remove('meta--search');
  }
}
requestJson();
