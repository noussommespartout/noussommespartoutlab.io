// Scripts

// Variables
let simpleBarContent;
let simpleBarTableofcontents;
let simpleBarMeta;

// Marquee
function Marquee(selector) {
  const  allSelectors = document.querySelectorAll(selector);
  let counter = 1;
  allSelectors.forEach((el) => {
    let marqueeElement = el.querySelector('.marquee__element');
    let parentWidth = el.parentNode.clientWidth;
    let elWidth = el.clientWidth;
    let repeatLineLength = 3;
    let repeatClone = Math.floor(parentWidth / elWidth) * repeatLineLength;
    if (repeatClone == 0) {
      repeatClone += 1;
    }
    for (let i = 0; i < repeatClone; i++) {
      let clone = marqueeElement.cloneNode(true);
      let space = document.createTextNode('\u00A0');
      clone.prepend(space);
      marqueeElement.insertAdjacentElement('afterend', clone);
    }
    // let timer = 60 + 's';
    // el.style.animationDuration = timer;

    let direction;

    if (counter % 2) {
      direction = 'normal';
    } else {
      direction = 'reverse';
    }

    el.style.animationDirection = direction;
    counter += 1;
  });
}
  Marquee('.marquee');

//
// Menu top
//
const menuTaxo = document.querySelector('.menu--taxo');
const buttonTaxo = document.querySelector('.button--taxo');
const menuAbout = document.querySelector('.menu--about');
const buttonAbout = document.querySelector('.button--about');
const menuLang = document.querySelector('.menu--lang');
const buttonLang = document.querySelector('.button--lang');
buttonAbout.addEventListener('click', () => {
  menuAbout.classList.toggle('show--menu');
  Marquee('.marquee');
  menuTaxo.classList.remove('show--menu');
  menuLang.classList.remove('show--menu');
  buttonAbout.blur();
});
buttonTaxo.addEventListener('click', () => {
  menuTaxo.classList.toggle('show--menu');
  Marquee('.marquee');
  menuAbout.classList.remove('show--menu');
  menuLang.classList.remove('show--menu');
  buttonTaxo.blur();
});
buttonLang.addEventListener('click', () => {
  menuLang.classList.toggle('show--menu');
  Marquee('.marquee');
  menuTaxo.classList.remove('show--menu');
  menuAbout.classList.remove('show--menu');
  buttonAbout.blur();
});

////
//// Shuffle article
////
const shuffleLink = document.querySelector('.shuffle--link');
const shuffleList = document.querySelector('.shuffle--list');

function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

function ShuffleArticle() {
  let listArticles = document.querySelectorAll('.table__item-title a');
  let listURL = [];
  listArticles.forEach((el) => {
    listURL.push(el.href);
  });
  shuffleArray(listURL);
  shuffleLink.href = listURL[0];
}
function ShuffleList() {
  const list = document.querySelector('.table__list');
  for (let i = list.children.length; i >= 0; i--) {
    list.appendChild(list.children[Math.random() * i | 0]);
  }
}

ShuffleList();
ShuffleArticle();

//
// Table of contents
//
const tableOfContents = document.querySelector('.table');
const buttonTable = document.querySelector('.button--table');
if (tableOfContents) {
  buttonTable.addEventListener('click', () => {
    if (!tableOfContents.classList.contains('show')) {
      menuAbout.classList.remove('show--menu');
      menuLang.classList.remove('show--menu');
      menuTaxo.classList.remove('show--menu');
    }
    document.querySelector('.table__list').scrollIntoView();
    ShuffleList();
    ShuffleArticle();
    tableOfContents.classList.toggle('show');
    buttonTable.blur();
  });
  shuffleList.addEventListener('click', () => {
    ShuffleList();
  });
}

// Begin init for swup
function init() {
  // Close menus
  tableOfContents.classList.remove('show');
  menuAbout.classList.remove('show--menu');
  menuLang.classList.remove('show--menu');
  menuTaxo.classList.remove('show--menu');

  //
  // Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
  //
  // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
  let vh = window.innerHeight * 0.01;
  // Then we set the value in the --vh custom property to the root of the document
  document.documentElement.style.setProperty('--vh', `${vh}px`);

  // We listen to the resize event
  window.addEventListener('resize', () => {
    // We execute the same script as before
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
  });

  //
  // Baffle headers
  //
  let b = baffle('.baffle')
    .start()
    .set({
      characters: 'noussommespartout',
      speed: 150,
    })
    .reveal(500, 500);

  //
  // Height of the title (for content margin)
  //
  const articleNav = document.querySelector('.article__nav');
  let th;
  if (articleNav) {
    th = articleNav.offsetHeight;
    document.documentElement.style.setProperty('--th', `${th}px`);
  }
  // We listen to the resize event
  window.addEventListener('resize', () => {
    if (articleNav) {
      th = articleNav.offsetHeight;
      document.documentElement.style.setProperty('--th', `${th}px`);
    }
  });

  //
  // Scrollbar with simplebar.js
  //
  simpleBarContent = new SimpleBar(document.querySelector('.content'));
  simpleBarTableofcontents = new SimpleBar(document.querySelector('.tableofcontents'));
  if (document.querySelector('.article__nav--metapage.article__nav--wrap')) {
    simpleBarMeta = new SimpleBar(document.querySelector('.article__nav--metapage.article__nav--wrap'));
  }

  // Remove menu when reload
  // menu.classList.remove('header__nav--show');

  //
  // Site Title
  //
  const siteTitle = document.querySelector('.header__home a');
  siteTitle.addEventListener('click', () => {
    if (window.location.pathname === '/') {
      swup.loadPage({url: window.location.pathname});
      // window.location.reload();
    }
    siteTitle.blur();
    btnHack.classList.remove('button--anim');
  });

  //
  // Reload page if same page link
  //
  const links = document.querySelectorAll('a:not([href^="#"])');
  links.forEach((link) => {
    link.addEventListener('click', () => {
      if (link.pathname === window.location.pathname) {
        swup.loadPage({url: window.location.pathname});
        // window.location.reload();
      }
      link.blur();
    });
  });

  //
  // Menu article
  //
  const articleMenu = document.querySelector('.article__menu');
  const articleInfo = document.querySelector('.article__info');
  const articleCategories = document.querySelector('.article__categories');
  const articleDownload = document.querySelector('.article__download');
  if (articleMenu) {
    articleMenu.addEventListener('click', () => {
      if(articleInfo) {
        articleInfo.classList.toggle('show');
        articleNav.classList.toggle('article__nav--border');
      }
      if(articleCategories) articleCategories.classList.toggle('show');
      if(articleDownload) articleDownload.classList.toggle('show');
      articleMenu.blur();
    });
  }

  // Random numbers
  function randomNb(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  //
  // Button Hack
  //
  let piraterie;
  if (window.location.href.indexOf("/de/") != -1) {
    piraterie = ['wir', 'sind', 'überall'];
  }
  else if (window.location.href.indexOf("/en/") != -1) {
    piraterie = ['we', 'are', 'everywhere'];
  }
  else if (window.location.href.indexOf("/it/") != -1) {
    piraterie = ['siamo', 'ovunque'];
  }
  else {
    piraterie = ['nous', 'sommes', 'partout'];
  }

  const btnHack = document.querySelector('.button--hack');
  let btnHackActive = false;
  // const hack = document.createElement('div');
  let hack;
  hack = document.querySelector('.hack');
  hack.classList.add('none');
  hack.style.zIndex = '900';
  hack.style.position = 'fixed';
  hack.style.top = 'calc(var(--header-height))';
  hack.style.height = 'calc(100% - var(--header-height) - var(--footer-height))';
  hack.style.left = '0';
  hack.style.width = '100%';
  hack.style.overflow = 'hidden';
  // const mediaQuery = window.matchMedia('(min-width: 50em)');
  // if (mediaQuery.matches) {
  // } else {
  // }
  let hackTime;
  btnHack.addEventListener('click', (event) => {
    event.preventDefault();
    if (!btnHackActive) {
      // document.body.appendChild(hack);
      hack.classList.toggle('none');
      btnHack.classList.toggle('button--anim');
      btnHackActive = !btnHackActive;
      hackTime = setInterval(() => {
        shuffleArray(piraterie);
        let popup = document.createElement('div');
        popup.style.position = 'absolute';
        popup.style.top = randomNb(0, 100) + '%';
        popup.style.left = randomNb(0, 100) + '%';
        popup.style.transform = 'translate(-50%,-50%)';
        popup.style.fontSize = 'var(--title-size)';
        popup.style.fontWeight = 'bold';
        popup.style.color = 'var(--color-fg)';
        popup.style.whiteSpace = 'nowrap';
        popup.innerHTML = piraterie[0];
        hack.appendChild(popup);
      }, 100);
    } else {
      hack.innerHTML = '';
      hack.classList.toggle('none');
      btnHack.classList.toggle('button--anim');
      btnHackActive = !btnHackActive;
      clearInterval(hackTime);
    }
    btnHack.blur();
  });

  //
  // For search page
  //
  // get dom elements
  searchInput = document.querySelector('.search__input');
  searchResults = document.querySelector('.search__results');
  currentContent = document.querySelectorAll('.search__results ~ *');
  metaHome = document.querySelector('.meta--home');

  registerSearchHandler();
  requestSearch();
  requestJson();

  //
  // Go top of page when change
  //
  if (window.location.hash) {
    const elScroll = document.querySelector(""+decodeURI(window.location.hash)+"");
    if (elScroll) {
      window.scrollTo(0, 0);
      elScroll.scrollIntoView();
    }
  }
  else {
    const elScroll = document.querySelector('.article__content');
    if (elScroll) {
      window.scrollTo(0, 0);
      elScroll.scrollIntoView({block: "start", inline: "start"})
    }
  }

}
// End init for swup


//
// Dark mode
//
const btnDark = document.querySelector('.button--dark-mode');
let darkModeToggle = false;
const darkModeTheme = localStorage.getItem('theme');

if (darkModeTheme) {
  document.documentElement.setAttribute('data-theme', darkModeTheme);

  if (darkModeTheme === 'dark') {
    darkModeToggle = true;
  }
}

function darkModeSwitch() {
  if (!darkModeToggle) {
    // document.querySelector('.carre-nb').classList.remove('show');
    // document.querySelector('.carre-bn').classList.add('show');
    document.documentElement.setAttribute('data-theme', 'dark');
    localStorage.setItem('theme', 'dark');
    darkModeToggle = true;
  } else {
    // document.querySelector('.carre-bn').classList.remove('show');
    // document.querySelector('.carre-nb').classList.add('show');
    document.documentElement.setAttribute('data-theme', 'light');
    localStorage.setItem('theme', 'light');
    darkModeToggle = false;
  }
  btnDark.blur();
}

btnDark.addEventListener('click', darkModeSwitch);


//
// Swup - Page transition
//
const options = {
  containers: ['.swup'],
  cache: true,
  linkSelector:
    'a[href^="' +
    window.location.origin +
  '"]:not([data-no-swup]), .button--shuffle, a[href^="/"]:not([data-no-swup])',
};

const swup = new Swup(options);
init();
swup.on('contentReplaced', init);

// Disable scroll simplebar for print
window.addEventListener('beforeprint', (event) => {
  simpleBarContent.unMount();
  simpleBarTableofcontents.unMount();
  if (document.querySelector('.article__nav--metapage.article__nav--wrap')) {
    simpleBarMeta.unMount();
  }
});

window.addEventListener('afterprint', (event) => {
  simpleBarContent = new SimpleBar(document.querySelector('.content'));
  simpleBarTableofcontents = new SimpleBar(document.querySelector('.tableofcontents'));
  if (document.querySelector('.article__nav--metapage.article__nav--wrap')) {
    simpleBarMeta = new SimpleBar(document.querySelector('.article__nav--wrap'));
  }
});
