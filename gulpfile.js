// Include Gulp
import gulp from 'gulp';
import browserSync from 'browser-sync';
import critical from 'critical';
import del from 'del';
import cache from 'gulp-cache';
import gulpif from 'gulp-if';
import imageResize from 'gulp-image-resize';
import changed from 'gulp-changed';
import rename from 'gulp-rename';
import imagemin from 'gulp-imagemin';
import imageminMozjpeg from 'imagemin-mozjpeg';
import imageminGifsicle from 'imagemin-gifsicle';
import imageminSvgo from 'imagemin-svgo';
import imageminOptipng from 'imagemin-optipng';
import { exec } from 'child_process'; // For running a local machine task

// Compress and transform all images
gulp.task('images', function(done) {

  [2400,1200,800,400].forEach(function (size) {
    gulp.src( ['src/img/**/*.{png,jpg}', '!src/img/**/*.gif', '!src/img/**/*.mp3', '!src/img/**/*.mp4'])
      .pipe(changed('static/img/'))
      .pipe(imageResize({ width: size }))
      .pipe(cache(imagemin([
        imageminGifsicle({interlaced: true, optimizationLevel: 3}),
        imageminMozjpeg({quality: 70, progressive: true}),
        imageminOptipng({optimizationLevel: 5}),
        imageminSvgo({
            plugins: [
                {removeViewBox: true},
                {cleanupIDs: false}
            ]
        })
      ], {
          verbose: true
      }
      )))
      .pipe(gulpif(size == 2400, rename(function (path) { path.basename = `${path.basename}`; })))
      .pipe(gulpif(size == 1200, rename(function (path) { path.basename = `${path.basename}-large`; })))
      .pipe(gulpif(size == 800, rename(function (path) { path.basename = `${path.basename}-medium`; })))
      .pipe(gulpif(size == 400, rename(function (path) { path.basename = `${path.basename}-small`; })))
      .pipe(gulp.dest('static/img/'));
    });
  gulp.src( ['src/img/**/*.mp4','src/img/**/*.gif','src/img/**/*.mp3'])
     .pipe(gulp.dest('static/img/'));
  done();
});

// Clean : clear cache and delete images
gulp.task('clean', function () {
  return del(['static/img/*']);
  cache.clearAll();
});

gulp.task('lunr-index', function (cb) {
  exec("cp public-dev/index.json public-dev/index-ascii.json && sed -i -e 'y/āáǎàäâēéěèêëīïíǐìöōóǒòôūúǔùǖǘǚǜüûÿĀÁǍÀÄÂĒÉĚÈÊËĪÏÍǏÌÖŌÓǑÒÔŪÚǓÙǕǗǙǛÜÛŸ/aaaaaaeeeeeeiiiiioooooouuuuuuuuuuyAAAAAAEEEEEEIIIIIOOOOOOUUUUUUUUUUY/' public-dev/index-ascii.json && cat public-dev/index-ascii.json | node tools/build-search-index.cjs > static/searchdata.json", function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});

gulp.task('lunr-index-de', function (cb) {
  exec("cp public-dev/de/index.json public-dev/de/index-ascii.json && sed -i -e 'y/āáǎàäâēéěèêëīïíǐìöōóǒòôūúǔùǖǘǚǜüûÿĀÁǍÀÄÂĒÉĚÈÊËĪÏÍǏÌÖŌÓǑÒÔŪÚǓÙǕǗǙǛÜÛŸ/aaaaaaeeeeeeiiiiioooooouuuuuuuuuuyAAAAAAEEEEEEIIIIIOOOOOOUUUUUUUUUUY/' public-dev/de/index-ascii.json && cat public-dev/de/index-ascii.json | node tools/build-search-index.cjs > static/de/searchdata.json", function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});

gulp.task('lunr-index-en', function (cb) {
  exec("cp public-dev/en/index.json public-dev/en/index-ascii.json && sed -i -e 'y/āáǎàäâēéěèêëīïíǐìöōóǒòôūúǔùǖǘǚǜüûÿĀÁǍÀÄÂĒÉĚÈÊËĪÏÍǏÌÖŌÓǑÒÔŪÚǓÙǕǗǙǛÜÛŸ/aaaaaaeeeeeeiiiiioooooouuuuuuuuuuyAAAAAAEEEEEEIIIIIOOOOOOUUUUUUUUUUY/' public-dev/en/index-ascii.json && cat public-dev/en/index-ascii.json | node tools/build-search-index.cjs > static/en/searchdata.json", function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});

gulp.task('lunr-index-it', function (cb) {
  exec("cp public-dev/it/index.json public-dev/it/index-ascii.json && sed -i -e 'y/āáǎàäâēéěèêëīïíǐìöōóǒòôūúǔùǖǘǚǜüûÿĀÁǍÀÄÂĒÉĚÈÊËĪÏÍǏÌÖŌÓǑÒÔŪÚǓÙǕǗǙǛÜÛŸ/aaaaaaeeeeeeiiiiioooooouuuuuuuuuuyAAAAAAEEEEEEIIIIIOOOOOOUUUUUUUUUUY/' public-dev/it/index-ascii.json && cat public-dev/it/index-ascii.json | node tools/build-search-index.cjs > static/it/searchdata.json", function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});

gulp.task('lunr-index-deploy', function (cb) {
  exec("cp public/index.json public/index-ascii.json && sed -i -e 'y/āáǎàäâēéěèêëīïíǐìöōóǒòôūúǔùǖǘǚǜüûÿĀÁǍÀÄÂĒÉĚÈÊËĪÏÍǏÌÖŌÓǑÒÔŪÚǓÙǕǗǙǛÜÛŸ/aaaaaaeeeeeeiiiiioooooouuuuuuuuuuyAAAAAAEEEEEEIIIIIOOOOOOUUUUUUUUUUY/' public/index-ascii.json && cat public/index-ascii.json | node tools/build-search-index.cjs > static/searchdata.json", function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});

gulp.task('lunr-index-de-deploy', function (cb) {
  exec("cp public/de/index.json public/de/index-ascii.json && sed -i -e 'y/āáǎàäâēéěèêëīïíǐìöōóǒòôūúǔùǖǘǚǜüûÿĀÁǍÀÄÂĒÉĚÈÊËĪÏÍǏÌÖŌÓǑÒÔŪÚǓÙǕǗǙǛÜÛŸ/aaaaaaeeeeeeiiiiioooooouuuuuuuuuuyAAAAAAEEEEEEIIIIIOOOOOOUUUUUUUUUUY/' public/de/index-ascii.json && cat public/de/index-ascii.json | node tools/build-search-index.cjs > static/de/searchdata.json", function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});

gulp.task('lunr-index-en-deploy', function (cb) {
  exec("cp public/en/index.json public/en/index-ascii.json && sed -i -e 'y/āáǎàäâēéěèêëīïíǐìöōóǒòôūúǔùǖǘǚǜüûÿĀÁǍÀÄÂĒÉĚÈÊËĪÏÍǏÌÖŌÓǑÒÔŪÚǓÙǕǗǙǛÜÛŸ/aaaaaaeeeeeeiiiiioooooouuuuuuuuuuyAAAAAAEEEEEEIIIIIOOOOOOUUUUUUUUUUY/' public/en/index-ascii.json && cat public/en/index-ascii.json | node tools/build-search-index.cjs > static/en/searchdata.json", function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});

gulp.task('lunr-index-it-deploy', function (cb) {
  exec("cp public/it/index.json public/it/index-ascii.json && sed -i -e 'y/āáǎàäâēéěèêëīïíǐìöōóǒòôūúǔùǖǘǚǜüûÿĀÁǍÀÄÂĒÉĚÈÊËĪÏÍǏÌÖŌÓǑÒÔŪÚǓÙǕǗǙǛÜÛŸ/aaaaaaeeeeeeiiiiioooooouuuuuuuuuuyAAAAAAEEEEEEIIIIIOOOOOOUUUUUUUUUUY/' public/it/index-ascii.json && cat public/it/index-ascii.json | node tools/build-search-index.cjs > static/it/searchdata.json", function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});

// Generate & Inline Critical-path CSS
gulp.task('critical', function (done) {
  critical.generate({
  base: 'public-dev/',
  src: 'index.html',
  css: ['public-dev/css/style.*.css'],
  target: {
    css: '../assets/css/critical.css',
  },
  dimensions: [
    {
      height: 600,
      width: 300,
    },
    {
      height: 800,
      width: 1300,
    },
  ],
});
  done();
});

// Hugo
gulp.task(
  'hugo',
  gulp.series(
  function (cb) {
  exec("GOMAXPROCS=1 hugo --minify --cleanDestinationDir --environment development", function (err, stdout, stderr) {
  // exec("GOMAXPROCS=1 hugo --cleanDestinationDir --environment development", function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
  })
);

gulp.task(
  'hugo-prod',
  gulp.series(
  function (cb) {
  exec("GOMAXPROCS=1 hugo --minify --cleanDestinationDir --environment production", function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
  })
);

// Browser-sync
gulp.task('browser-sync', function(cb) {
  browserSync({
    server: {
      baseDir: "public-dev"
    },
    // https: true,
    open: false
  }, cb);
});

function reload(done) {
  browserSync.reload();
  done();
}

gulp.task('pdf-compare', function (cb) {
  exec('sleep 5 && ./tools/compare.sh', function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
})

// Watch
gulp.task('watch', function () {
  gulp.watch([
    'content/**',
    'assets/**',
    'data/**',
    'layouts/**',
    'static/**',
    'archetypes/**',
    'config/**'
  ], gulp.series('hugo', reload));
  gulp.watch('src/img/**/*', gulp.series('images', reload));
});

gulp.task(
  'default',
  gulp.series('images', 'hugo', 'critical', 'lunr-index', 'lunr-index-de', 'lunr-index-it', 'lunr-index-en', 'hugo', 'browser-sync', 'watch')
);

gulp.task(
  'deploy',
  gulp.series('images', 'hugo-prod', 'lunr-index-deploy', 'lunr-index-de-deploy', 'lunr-index-it-deploy', 'lunr-index-en-deploy', 'hugo-prod', 'pdf-compare', 'watch')
);
