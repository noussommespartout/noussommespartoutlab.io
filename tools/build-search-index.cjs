const lunr = require('lunr');
// require("lunr-languages/lunr.stemmer.support")(lunr)
// require("lunr-languages/lunr.fr")(lunr)
const stdin = process.stdin;
const stdout = process.stdout;
let buffer = [];

stdin.resume()
stdin.setEncoding('utf8')

stdin.on('data', function (data) {
  buffer.push(data)
})



stdin.on('end', function () {
  var documents = JSON.parse(buffer.join(''))

  var idx = lunr(function () {
    // With language
    // this.use(lunr.fr);
    // issue with wildcard
    // https://github.com/olivernn/lunr.js/issues/454
    this.pipeline.remove(lunr.stemmer);
    this.pipeline.remove(lunr.stopWordFilter);

    this.ref('url');
    this.field('title', {boost: 6});
    this.field('subtitle', {boost: 5});
    this.field('tags');
    this.field('lang', {boost: 4});
    this.field('categories', {boost: 3});
    this.field('content');
    this.field('authors', {boost: 2});

    documents.forEach(function (doc) {
      this.add(doc)
    }, this)
  })

  stdout.write(JSON.stringify(idx))
})

