#!/usr/bin/env bash

INPUT=$1
LANG=$2

for i in $INPUT/*.md; do
  sed -i -E 's/href="\//href="\/'"${LANG}"'\//g' $i
done
