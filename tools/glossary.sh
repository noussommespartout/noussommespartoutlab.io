#!/usr/bin/env bash

INPUT=$1

for i in $INPUT/*.md; do
  sed -i -E 's/(\<[A-zÀ-ÿ]*\>[*]?)(°)/<a href="\/glossaire\/\#\1" class="glossary-link" target="_blank" rel="noopener" data-no-swup>\1\2<\/a>/g' $i
done
