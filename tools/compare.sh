#!/bin/bash

ARTICLES=`ls -1q ./content/francais/articles/* | wc -l`
PDFS=`ls -1q ./static/etc/fr/*.pdf | wc -l`
PDFS=$((PDFS - ARTICLES - ARTICLES))
if [[ $ARTICLES != $PDFS ]]; then
  echo "---------------------------------"
  echo "---------------------------------"
  echo "                              "
  echo " Attention ! Il manque $((ARTICLES - PDFS)) PDF !"
  echo "                              "
  echo "---------------------------------"
  echo "---------------------------------"
fi

exit 0
