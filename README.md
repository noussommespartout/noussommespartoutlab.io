﻿# Nous sommes partout

## Projet

*Nous sommes partout* collecte et partage des voix antifascistes, féministes, anticapitalistes, antiracistes, antispécistes, des paroles de hackeureuxses, des voix en lutte pour les droits des migranxtes, contre toutes les formes d'oppression de nos sociétés, pour les droits LGBTQIA+, contre les écocides, pour les droits des travailleureuxses du sexe, contre les violences policières et la répression juridique, pour les droits des sans-papièrexs, pour l'autodétermination et l'émancipation de touxtes les travailleureuxses, contre la précarisation, contre le système carcéral et pour les ZAD.

Tous les textes sont hébergés sur [www.noussommespartout.org](https://www.noussommespartout.org). Ce livre est une première sauvegarde de cette base de données en ligne, l’export fait à l’été 2021 d’une collecte qui se poursuit. Pour cette première édition de *Nous sommes partout*, les récits et témoignages émanent tous du même territoire géographique : la Suisse romande.

## Groupe éditorial

*Nous sommes partout* est un groupe éditorial qui s'organise autour de son geste premier : [accueillir des voix](https://www.noussommespartout.org). Son intention est de socialiser des réflexions sur les pratiques militantes dans les termes choisis par les très nombreuxses auteurixes des textes édités, dont la collecte est essentiellement protéiforme, comme une interrogation sociale, critique et historique sur les combats animant la société et sur la manière de lutter pour changer le réel. *Nous sommes partout* croit en la nécessité de l'échange et en l'agentivité des lecteurixces. Sa volonté est de faire somme de paroles, dans l'espoir de relater l'immédiateté des luttes contemporaines par la mise en texte de vécus pratiques et émotionnels. *Nous sommes partout* s'inscrit dans un terrain expérientiel, un terrain géographique et un terrain idéologique. Centré sur l'expérience de l'action politique radicale, le groupe éditorial ne se structure pas autour de perspectives idéologiques prédéfinies. Cela dit, par souci d'accueillir des voix, des idées et des modes d'action peu présents dans l'interdiscours dominant, un certain environnement conceptuel s'impose de lui-même. Il est possible d'essayer de le saisir par la constellation d'adjectifs utilisés par les contributeurixes elleux-mêmes pour se décrire : non institutionnel, radical, libertaire, intersectionnel, anticapitaliste, antifasciste, antiétatique.

## Technique

*Nous sommes partout* s'appuie sur la puissance de [l'outil libre Git](https://gitlab.com/noussommespartout/noussommespartout.gitlab.io) pour diffuser ses textes en ligne.

*Nous sommes partout* utilise les outils libres Pandoc et LuaTeX, ainsi que quelques scripts Bash, Lua et JavaScript, pour produire ses formes statiques (*antizines* et *antilivres*). Les fichiers *TeX* qui permettent la fabrique de ces *antizines* et de ces *antilivres* se trouvent dans le dossier [fabrique](/fabrique).

## Liberté

*Nous sommes partout* place ses textes sous une [lecture libre](https://abrupt.cc/partage) de la licence Creative Commons Attribution — Pas d’Utilisation Commerciale — Partage dans les Mêmes Conditions 4.0 International (CC&nbsp;BY-NC-SA&nbsp;4.0).

Le [code source](https://gitlab.com/noussommespartout/noussommespartout.gitlab.io) du site, tout comme le [code source](/fabrique) qui permet de produire les formes statiques de *Nous sommes partout* (*antizines* et *antilivres*), est quant à lui placé sous [licence MIT](LICENSE-TOOLS).

(*L'information veut être libre.*)
